//
//  HGRating.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "JSONModel.h"
#import "HGReviewer.h"

@class HGReviewer;

@interface HGRating : JSONModel


@property (nonatomic, strong) NSString* uid;  ///reivew information uid;
@property (nonatomic, strong) HGReviewer * reviewer;
@property (nonatomic, assign) CGFloat score;
@property (nonatomic, strong) NSString * comment;
@property (nonatomic, strong) NSString* reply_content;
@property (nonatomic, assign) NSUInteger created_at;


@property (nonatomic, assign) BOOL reported;
///==================local var ============
@property (nonatomic, assign) float commentHeight;
@property (nonatomic, assign) float replyHeight;

- (NSDate *)timestamp;

@end

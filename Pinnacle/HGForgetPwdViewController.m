//
//  HGForgetPwdViewController.m
//  Pinnacle
//
//  Created by Alex on 14-9-30.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGForgetPwdViewController.h"
#import "HGAppData.h"
#import "HGUtils.h"

#import "NSString+EmailAddresses.h"
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <SVProgressHUD.h>
@interface HGForgetPwdViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;

@property (nonatomic,assign) CGFloat yKeyboardShow;
@property (nonatomic,strong) UITapGestureRecognizer *gestTapOutOfKeyboard;

@end

@implementation HGForgetPwdViewController

#define Y_OFFSET_KEYBOARD_SHOW ([UIScreen mainScreen].bounds.size.height - 500)

- (void)viewDidLoad {
    [super viewDidLoad];

    self.yKeyboardShow = self.view.frame.origin.y + Y_OFFSET_KEYBOARD_SHOW;
    
    self.tfEmail.delegate = self;

    // Configure text
    self.lbDescription.text = NSLocalizedString(@"Please enter the email you used to sign into 5miles.", nil);
    [self.btnReset setTitle:NSLocalizedString(@"Reset password", nil) forState:UIControlStateNormal];

    
    // Configure font
    self.lbDescription.font = [UIFont systemFontOfSize:13];
    self.tfEmail.font = [UIFont systemFontOfSize:15];
    self.btnReset.titleLabel.font = [UIFont systemFontOfSize:18];
    
    
    // Configure placeholder for text label
    [[HGUtils sharedInstance ]addHeadPadding: 12.0 forTextField: self.tfEmail];
    
    [self.tfEmail setPlaceholder: NSLocalizedString(@"Email address", nil)];
    

    
    
    // Configure style
    self.lbDescription.textColor = FANCY_COLOR(@"242424");
    self.lbDescription.textAlignment = NSTextAlignmentLeft;
    
    self.tfEmail.backgroundColor = FANCY_COLOR(@"f0f0f0");
    self.tfEmail.layer.cornerRadius = self.btnReset.layer.cornerRadius = 3.0;
    
    self.btnReset.backgroundColor = FANCY_COLOR(@"ff8830");
    [self.btnReset setTitleColor:FANCY_COLOR(@"ffffff") forState:UIControlStateNormal];
    
    
    // Do any additional setup after loading the view.
    self.tfEmail.delegate = self;
    [self.tfEmail setReturnKeyType:UIReturnKeyDone];
    [self.tfEmail addTarget:self
                       action:@selector(textFieldFinished:)
             forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    
    
    if (self.suggestUserEmail!=nil && self.suggestUserEmail.length > 0) {
        self.tfEmail.text = self.suggestUserEmail;
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

- (IBAction)textFieldFinished:(id)sender
{
    // [sender resignFirstResponder];
    [self Reset:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Reset:(id)sender {
    
    NSString* emailContentString = self.tfEmail.text;
    if (![emailContentString isValidEmailAddress]) {
        PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"This email doesn't seem to work.", nil) cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            
        }];
        [view useDefaultIOS7Style];
        
        [view setMessageFont:[UIFont systemFontOfSize:13]];
        [view setMessageColor: FANCY_COLOR(@"242424")];
        return;
    }
    
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"signin_view" action:@"resetpassword" label:nil value:nil];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [[HGAppData sharedInstance].guestApiClient FIVEMILES_GET:@"forget_password/" parameters:@{@"email": emailContentString}
      success:^(NSURLSessionDataTask *task, id responseObject)
     {
                                            [SVProgressHUD dismiss];
         
        NSString *message = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"An email has been sent to",nil), emailContentString, NSLocalizedString(@"with a link to reset your password",nil)];
        PXAlertView* view =[PXAlertView showAlertWithTitle:nil
                                        message:message
                                        cancelTitle:NSLocalizedString(@"OK", nil)
                                        completion:^(BOOL cancelled, NSInteger buttonIndex) {
                                            if(cancelled){
                                                 [self.navigationController popViewControllerAnimated:YES];
                                            }
                            }];
         
         [view useDefaultIOS7Style];
         
         [view setMessageFont:[UIFont systemFontOfSize:13]];
         [view setMessageColor: FANCY_COLOR(@"242424")];
         
       
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"Forget password failed : %@", [error.userInfo objectForKey:@"body"]);                
    }];
}

#pragma mark - helpers

- (void)keyboardWillShow: (NSNotification *)notification{
    
    if( self.view.frame.origin.y > self.yKeyboardShow){
        [UIView animateWithDuration:0.3f animations:^{
            CGRect rect = self.view.frame;
            rect.origin.y = self.yKeyboardShow;
            self.view.frame = rect;
        }];
    }
    
    self.gestTapOutOfKeyboard = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapOutOfKeyboard:)];
    [self.view addGestureRecognizer:self.gestTapOutOfKeyboard];
}

- (void)keyboardWillHide: (NSNotification *)notification{
    
    if( self.view.frame.origin.y == self.yKeyboardShow){
        [UIView animateWithDuration:0.3f animations:^{
            CGRect rect = self.view.frame;
            rect.origin.y = self.yKeyboardShow - Y_OFFSET_KEYBOARD_SHOW;
            self.view.frame = rect;
        }];
    }
    
    [self.view removeGestureRecognizer:self.gestTapOutOfKeyboard];
}


- (void) onTapOutOfKeyboard: (UITapGestureRecognizer *)gesture{

    [self.view endEditing:YES];
}


@end

//
//  HGResourceCollectionController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-28.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <JDStatusBarNotification.h>
#import "SVPullToRefresh.h"

@interface HGResourceCollectionController : UICollectionViewController

@end

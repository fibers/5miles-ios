//
//  HGHomeItemCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGHomeItemCell.h"
#import "HGShopItem.h"
#import "HGUtils.h"
#import "HGAppData.h"
#import "UIImage+pureColorImage.h"
#import <QuartzCore/QuartzCore.h>

@implementation HGHomeItemCell
#define CELL_FONT_COLOR_1 [UIColor colorWithRed:0x24/255.0 green:0x24/255.0 blue:0x24/255.0 alpha:1.0];


#define CELL_SUGGEST_FONT_SIZE 11

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _setupCell];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setupCell];
}

- (void)_setupCell
{
    self.contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.contentView.layer.borderWidth = 0.0f;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    
    self.bottomShadow = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:self.bottomShadow];
    self.rightShadow = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:self.rightShadow];
    
    self.rightShadow.backgroundColor = self.bottomShadow.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.10];
    
    
    
    self.imageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    [self.contentView addSubview:self.imageView];
    
    
     
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.imageView.backgroundColor = [UIColor grayColor];
    
    
    self.ItemNewIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 66/2, 44/2)];
    self.ItemNewIcon.image = [UIImage imageNamed:@"home_item_new"];
    self.ItemNewIcon.hidden = NO;
    [self.imageView addSubview:self.ItemNewIcon];
    
    
    self.lbDistance = [[UILabel alloc] initWithFrame:CGRectZero];
    self.lbDistance.backgroundColor = [UIColor clearColor];
    self.lbDistance.font = [UIFont systemFontOfSize:CELL_SUGGEST_FONT_SIZE];
    self.lbDistance.textColor = CELL_FONT_COLOR_1;
    [self.contentView addSubview:self.lbDistance];
    
    self.lbPrice = [[UILabel alloc] initWithFrame:CGRectZero];
    self.lbPrice.backgroundColor = [UIColor clearColor];
    self.lbPrice.textAlignment = NSTextAlignmentLeft;
    self.lbPrice.font = [UIFont fontWithName:@"Helvetica-bold" size:CELL_SUGGEST_FONT_SIZE];;
    self.lbPrice.textColor = CELL_FONT_COLOR_1;
    [self.contentView addSubview:self.lbPrice];
    
    self.originalPrice = [[StrikeThroughLabel alloc] initWithFrame:CGRectZero];
    self.originalPrice.backgroundColor = [UIColor clearColor];
    self.originalPrice.textAlignment = NSTextAlignmentLeft;
    self.originalPrice.textColor = self.originalPrice.strikeColor = FANCY_COLOR(@"818181");
    self.originalPrice.font = [UIFont fontWithName:@"Helvetica" size:CELL_SUGGEST_FONT_SIZE];;
    self.originalPrice.strikeThroughEnabled = YES;
    [self.contentView addSubview:self.originalPrice];
    
    self.soldIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-sold"]];
    [self.contentView addSubview:self.soldIconView];
    
    
    self.locationIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-location"]];
    [self.contentView addSubview:self.locationIcon];
    
    
    
    self.seperatorView = [[UIView alloc] init];
    self.seperatorView.backgroundColor = [UIColor colorWithRed:0xd3/255.0 green:0xd3/255.0 blue:0xd3/255.0 alpha:1.0];
    [self.contentView addSubview:self.seperatorView];
    
    self.itemTitle = [[UILabel alloc] init];
    self.itemTitle.font = [UIFont fontWithName:@"Helvetica" size:CELL_SUGGEST_FONT_SIZE];
    [self.contentView addSubview: self.itemTitle];
    self.itemTitle.numberOfLines = 1;
    self.itemTitle.textColor = CELL_FONT_COLOR_1;
    
    self.homeAudioMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 19, 19)];
    self.homeAudioMark.image = [UIImage imageNamed:@"home_audio_mark"];
    [self.contentView addSubview:self.homeAudioMark];

}

- (void)layoutSubviews
{
    
    self.contentView.frame = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width-1, self.bounds.size.height-1) ;
    
    self.rightShadow.frame = CGRectMake(self.bounds.size.width-1, 1, 1, self.bounds.size.height-1) ;
    self.bottomShadow.frame = CGRectMake(1, self.bounds.size.height - 1, self.bounds.size.width -2, 1);
    
    
    
    
    self.imageView.frame = CGRectMake(0, 0, CGRectGetWidth(self.contentView.bounds), CGRectGetHeight(self.contentView.bounds) - WATER_FLOW_CELL_EXTEND_HEIGHT);
     self.ItemNewIcon.frame = CGRectMake(self.imageView.frame.size.width - 6 - 66/2, self.imageView.frame.size.height - 6 - 44/2, 66/2, 44/2);
    [self.imageView addSubview:self.ItemNewIcon];
    
    self.itemTitle.frame = CGRectMake(8, CGRectGetMaxY(self.imageView.frame) ,CGRectGetWidth(self.contentView.bounds)-8, 22 );
    
    self.seperatorView.frame = CGRectMake(0, CGRectGetMaxY(self.imageView.frame)+22, CGRectGetWidth(self.contentView.bounds), 0.5);
    
    self.soldIconView.frame = CGRectMake(0, 0, 60, 60);
    self.soldIconView.center = self.imageView.center;
    //=================================================================================
    self.locationIcon.frame = CGRectMake(8 ,  CGRectGetMaxY(self.imageView.frame) + 22 + 19, 7, 10);
    self.lbDistance.frame = CGRectMake(18, CGRectGetMaxY(self.imageView.frame) + 15 +19,  CGRectGetWidth(self.imageView.bounds) * 1 - 20, 24);
   
    {
        self.lbPrice.frame = CGRectMake(8, CGRectGetMaxY(self.imageView.frame) + 22, CGRectGetWidth(self.bounds) * 1 - 4, CGRectGetHeight(self.lbDistance.frame));
        
        self.originalPrice.frame = CGRectMake(self.lbPrice.frame.size.width, CGRectGetMaxY(self.imageView.frame) + 22,  80, 24);
        if (self.lbPrice.text.length > 0) {
            CGRect lbPriceRect = [self.lbPrice.text boundingRectWithSize: CGSizeMake(300, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lbPrice.font} context:NULL];
            self.lbPrice.frame = CGRectMake(self.lbPrice.frame.origin.x, self.lbPrice.frame.origin.y, lbPriceRect.size.width , self.lbPrice.frame.size.height);
            
            int max_PriceWidth = MIN(CGRectGetWidth(self.bounds) * 1 - 13 - lbPriceRect.size.width, 80);
            self.originalPrice.frame = CGRectMake(12+self.lbPrice.frame.size.width, CGRectGetMaxY(self.imageView.frame) + 22, max_PriceWidth, 24);

        }
    }
   
    //self.originalPrice.frame = CGRectMake(self.lbPrice.frame.size.width + 2, CGRectGetMinY(self.lbDistance.frame), CGRectGetWidth(self.bounds) * 0.5 - 4, CGRectGetHeight(self.lbDistance.frame));
    self.homeAudioMark.frame = CGRectMake(120,  CGRectGetMaxY(self.imageView.frame) + 30, 19, 19);
}



- (void)configWithItem:(HGShopItem *)item
{
    HGItemImage * coverImage = [item.images firstObject];
    NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:coverImage.imageLink width:300 height:0];
    
    
    
    @try {
        UIColor* color = [[HGAppData sharedInstance].AppInitDataSingle.default_placehold_colors objectAtIndex:rand()%24];
        //UIImage*fancyImage = [UIImage imageNamed:@"icon-empty"];
        UIImage*fancyImage = [UIImage imageWithColor:color];
        
        //self.imageView.backgroundColor = color;

        [self.imageView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:fancyImage
                                 options:SDWebImageRetryFailed
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error) {
                NSLog(@"load image error: %@", error);
            }
        }];
    }
    @catch (NSException *exception) {
        [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
    }
   
    
    if (item.is_new == 1) {
        if (item.state == HGItemStateListing) {
            self.ItemNewIcon.hidden = NO;
        }
        else{
            self.ItemNewIcon.hidden = YES;
        }
    }
    else{
        self.ItemNewIcon.hidden = YES;
    }
    
    self.itemTitle.text = item.title;
    
    self.soldIconView.hidden = item.state == HGItemStateListing;
    if (item.state >= HGItemStateOfferAccepted) {
        self.soldIconView.hidden = NO;
    }
    if (item.state == HGItemStateUnapproved) {
        self.soldIconView.hidden = NO;
        self.soldIconView.image = [UIImage imageNamed:@"icon-Unapproved"];
    }
    if (item.state == HGItemStateUnavailable) {
        self.soldIconView.hidden = NO;
        self.soldIconView.image = [UIImage imageNamed:@"icon-Unavailable"];
        
    }
    
    //address name has higher priority
    NSString *displayPlace;
    NSString *address = [item formatAddress];
    if (address.length > 0) {
        displayPlace = address;
    }
    else
    {
        NSString *distance = [item formatDistance];
        if ( distance.length == 0) {
            //user close location
            displayPlace = NSLocalizedString(@"unknown", nil);
        }else{
            displayPlace = distance;
        }
    }
    self.lbDistance.text = displayPlace;
    self.lbPrice.text = [item priceForDisplay];
    
   
    if (self.lbPrice.text.length > 0) {
        CGRect lbPriceRect = [self.lbPrice.text boundingRectWithSize: CGSizeMake(300, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lbPrice.font} context:NULL];
        self.lbPrice.frame = CGRectMake(self.lbPrice.frame.origin.x, self.lbPrice.frame.origin.y, lbPriceRect.size.width , self.lbPrice.frame.size.height);
        
        
        int max_PriceWidth = MIN(CGRectGetWidth(self.bounds) * 1 - 13 - lbPriceRect.size.width, 80);
        self.originalPrice.frame = CGRectMake(12+self.lbPrice.frame.size.width, CGRectGetMaxY(self.imageView.frame) + 22, max_PriceWidth, 24);
        
    }
    
    self.originalPrice.text = [item originalPriceForDisplay];  
    
    
    if (item.mediaLink.length > 0) {
        //close home audio mark
        //remove ,home audiomark temply.
        self.homeAudioMark.hidden = YES;
    }
    else{
        self.homeAudioMark.hidden = YES;
    }
}

#pragma mark - Helpers


@end

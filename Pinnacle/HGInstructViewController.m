//
//  HGInstructViewController.m
//  Pinnacle
//
//  Created by Alex on 14-10-21.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGInstructViewController.h"
#import "HGConstant.h"

@interface HGInstructViewController ()

@property (nonatomic, strong) UIButton *btnGotIt;

@end

@implementation HGInstructViewController


#pragma mark Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.AdditionalViewType==HGInstructViewType_VerifyIntro)
    {
        [self showVerifyInstruction];
    }
    else if(self.AdditionalViewType == HGInstructViewType_SellerChatIntro){
        // Seller
        [self showChatInstruction:NSLocalizedString(@"Answering the buyer quickly will help you sell faster. Keep chatting to agree on safe methods of payment and delivery.", nil)];
    }else if(self.AdditionalViewType == HGInstructViewType_BuyerChatIntro){
        // Buyer
        [self showChatInstruction:NSLocalizedString(@"Ask the seller for details here. Keep chatting to agree on safe methods of payment and delivery.", nil)];
    }
    
    
}


- (void)showChatInstruction:(NSString *)instruction{
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.8f];
    
    UIImageView *ivBubble = [[UIImageView alloc] initWithFrame:CGRectMake(18.0f, 148.0f, 283.0f, 222.0f)];
    ivBubble.image = [UIImage imageNamed:@"chat-instruction-bubble"];
    [self.view addSubview:ivBubble];
    
    UILabel *lbInstruction = [[UILabel alloc]initWithFrame:CGRectMake(65, 120, 200, 200)];
    lbInstruction.numberOfLines = 0;
    lbInstruction.lineBreakMode = NSLineBreakByWordWrapping;
    lbInstruction.font = [UIFont systemFontOfSize:14.0f];
    lbInstruction.textColor = [UIColor whiteColor];
    lbInstruction.textAlignment = NSTextAlignmentLeft;
    lbInstruction.text = instruction;
    [self.view addSubview:lbInstruction];
    
    self.btnGotIt = [[UIButton alloc]initWithFrame:CGRectMake(108.0f, 275.0f, 98.0f, 26.0f)];
    [self.btnGotIt setTitle:NSLocalizedString(@"Got it", nil) forState:UIControlStateNormal];
    self.btnGotIt.clipsToBounds = YES;
    self.btnGotIt.layer.cornerRadius = 4.0f;
    self.btnGotIt.layer.borderWidth = 1.0f;
    self.btnGotIt.enabled = NO;
    self.btnGotIt.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
    self.btnGotIt.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.btnGotIt setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [self.btnGotIt setTitleColor:FANCY_COLOR(@"b9b9b9") forState:UIControlStateDisabled];
    [self.btnGotIt addTarget:self action:@selector(closeChatInstruction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.btnGotIt];
    
    
    
}

- (void)closeChatInstruction{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onClickGotIt)])  {
        [self.delegate onClickGotIt];
    }
}

- (void)enableBtnGotIt{
    self.btnGotIt.enabled = YES;
    self.btnGotIt.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
}

-(void)showVerifyInstruction
{
    //todo. ..check if this is still in work.
    self.view.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    UIImageView* avatarView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 72, 100/2, 100/2)];
    avatarView.image =[UIImage imageNamed:@"verify_introduction_avatar"];
    [self.view addSubview:avatarView];
    
    UIImageView* renewIcon = [[UIImageView alloc] initWithFrame:CGRectMake(8, 430, 211/2,81/2)];
    renewIcon.image = [UIImage imageNamed:@"verify_introduction_renew"];
    [self.view addSubview:renewIcon];
    
    
    
    UIImageView* bubleView1= [[UIImageView alloc] initWithFrame:CGRectMake(66, 70, 464/2, 252/2)];
    bubleView1.image = [UIImage imageNamed:@"verify_introduction_bubble1"];
    [self.view addSubview:bubleView1];
    
    UILabel* label1 = [[UILabel alloc] initWithFrame:CGRectMake(35, 15, 464/2-44, 252/4)];
    label1.textColor = [UIColor whiteColor];
    label1.font = [UIFont systemFontOfSize:13];

    label1.text = NSLocalizedString(@"If you haven't been verified yet, become a verified seller to let buyers know they can trust you.",nil);
    label1.numberOfLines = 4;
    [bubleView1 addSubview:label1];
    
    UIButton* verifyButton =[[UIButton alloc] initWithFrame:CGRectMake(35, 88, 98, 26)];
    verifyButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    verifyButton.layer.borderWidth = 1;
    [verifyButton setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [verifyButton setTitle:NSLocalizedString(@"Go to verify",nil) forState:UIControlStateNormal];
    verifyButton.layer.cornerRadius = 4;
    verifyButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [verifyButton addTarget:self action:@selector(startVerifyAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    bubleView1.userInteractionEnabled = YES;
    [bubleView1 addSubview:verifyButton];
    
    UIImageView* bubleView2 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 326, 370/2, 186/2)];
    bubleView2.image = [UIImage imageNamed:@"verify_introduction_bubble2"];
    [self.view addSubview:bubleView2];
    
    UILabel* label2 = [[UILabel alloc] initWithFrame:CGRectMake(22, 18, 370/2 -39, 40)];
    label2.textColor = [UIColor whiteColor];
    label2.font = [UIFont systemFontOfSize:13];
    label2.text = NSLocalizedString(@"Renewing your listing will make it more visible",nil);
    label2.numberOfLines = 2;
    [bubleView2 addSubview:label2];
    
    
    
    UITapGestureRecognizer *tapAvatar = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeInstructView)];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:tapAvatar];
}
-(void)startVerifyAction
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onStartVerifyAction:)])  {
        [self.delegate onStartVerifyAction:self];
    
    }
}
-(void)closeInstructView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onClickOutOfButton:)]) {
        [self.delegate onClickOutOfButton:self];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end

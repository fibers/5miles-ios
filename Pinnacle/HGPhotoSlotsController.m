//
//  HGPhotoSlotsController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-16.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGPhotoSlotsController.h"
#import "HGAddPhotoCell.h"
#import "HGPhotoSlotCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGUtils.h"
#import <Raven/RavenClient.h>
@implementation HGPhotoSlotsController

#define MAX_SLOT_CAPACITY   6

- (id)init
{
    self = [super init];
    if (self) {
        self.images = [@[] mutableCopy];
    }
    return self;
}

#pragma mark - UITableView Datasource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return MIN(MAX_SLOT_CAPACITY, self.images.count + 1) ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 74;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * AddPhotoCellID = @"AddPhotoCell";
    static NSString * PhotoSlotCellID = @"PhotoSlotCell";
    if (indexPath.row == self.images.count && self.availableSlotNum > 0) {
        HGAddPhotoCell * cell = [tableView dequeueReusableCellWithIdentifier:AddPhotoCellID];
        if (!cell) {
            NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"HGAddPhotoCell" owner:self options:nil];
            cell = nib.count > 0 ? [nib firstObject] : nil;
        }
        
        return cell;
    } else if (indexPath.row < self.images.count) {
        HGPhotoSlotCell * cell = [tableView dequeueReusableCellWithIdentifier:PhotoSlotCellID];
        if (!cell) {
            NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"HGPhotoSlotCell" owner:self options:nil];
            cell = nib.count > 0 ? [nib firstObject] : nil;
        }
        
        NSDictionary* imageDict = [self.images objectAtIndex:indexPath.row];
        NSString* remoteURlString = [imageDict objectForKey:@"remote_url"];
       
        cell.thumbnailView.contentMode = UIViewContentModeScaleAspectFill;

        cell.thumbnailView.clipsToBounds = YES;
        if ( remoteURlString != nil && remoteURlString.length > 1) {
            
            int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:cell.thumbnailView.frame.size.width*2];
             NSString* image_url = [[HGUtils sharedInstance] cloudinaryLink:remoteURlString  width:suggestWidth height:suggestWidth];
            
            [cell.thumbnailView sd_setImageWithURL:[NSURL URLWithString:image_url]];
        }
        else
        {
            cell.thumbnailView.image = [imageDict objectForKey:@"image"];
        }
       
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.images.count) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(handleTapAddPhoto)]) {
            [self.delegate handleTapAddPhoto];
        }
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(handleTapPhotoImageAtSlot:)]) {
            [self.delegate handleTapPhotoImageAtSlot:indexPath.row];
        }
    }
}

#pragma mark - Others

- (NSUInteger)availableSlotNum
{
    return self.images.count >= MAX_SLOT_CAPACITY ? 0 : MAX_SLOT_CAPACITY - self.images.count;
}

- (BOOL)addImage:(UIImage *)image withUUID:(NSString *)imgUUID withRomoteUrl:(NSString*)remoteUrl
{
    if (self.images.count < MAX_SLOT_CAPACITY) {
        [self.images addObject:@{@"image": image, @"uuid":imgUUID, @"remote_url":remoteUrl}];
        return YES;
    }
    return NO;
}

-(BOOL)addImage:(NSString*)fakeImageUUID withRemoteUrl:(NSString*)remoteUrl
{
    if (self.images.count < MAX_SLOT_CAPACITY) {
        [self.images addObject:@{ @"uuid":fakeImageUUID, @"remote_url":remoteUrl}];
        return YES;
    }
    return NO;
}

- (NSString *)removeImageAtIndex:(NSUInteger)index
{
    @try {
        NSString * imgUuid = [[self.images objectAtIndex:index] objectForKey:@"uuid"];
        [self.images removeObjectAtIndex:index];
        return imgUuid;
    }
    @catch (NSException *exception) {
         [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];

        NSLog(@"remove image failed: %@", exception.description);
        return nil;
    }
}
- (void)insertImageAsFirstCover:(NSMutableDictionary*)imgUUID;
{
    [self.images insertObject:imgUUID atIndex:0];
}

@end

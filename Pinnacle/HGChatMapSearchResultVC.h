//
//  HGChatMapSearchResultVC.h
//  Pinnacle
//
//  Created by Alex on 15/5/7.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGUITextField.h"
#import "HGConstant.h"
#import <MapKit/MapKit.h>
@interface HGChatMapSearchResultVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;

//@property (nonatomic, strong) HGUITextField* searchTitleView;
@end

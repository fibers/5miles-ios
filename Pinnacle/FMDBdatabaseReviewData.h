//
//  FMDBdatabaseReviewData.h
//  Pinnacle
//
//  Created by Alex on 15-4-20.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDatabaseQueue.h>


//////warning!!!!!!!!!
///////todo ,,,!!!!!important, if we are using more and more FMDB SQLITE.
/////// WE NEED TO IMPORT FMDBQUEUE to keep thread safe.....by alex


@interface FMDBdatabaseReviewData : NSObject

+ (void)initDataBase;
+ (BOOL)insertReviews:(NSDictionary*)dict;
+ (NSMutableDictionary *)getReview:(NSString *)review_keystring;
+ (BOOL)deleteReview:(NSString *)review_keystring;
+ (BOOL)updateReview:(NSDictionary *)dict;
@end

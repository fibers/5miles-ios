//
//  HGChatSupplementaryReusableView.m
//  Pinnacle
//
//  Created by shengyuhong on 15/5/19.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatSupplementaryReusableView.h"
#import "TTTAttributedLabel.h"

NSInteger const TopConstraintDescription = 14;
NSInteger const TrailingConstraintDescription = 28;
NSInteger const BottomConstraintDescription = 20;
NSInteger const LeadingConstraintDescription = 28;

NSInteger const TopPaddingDescription = 16;
NSInteger const BottomPaddingDescription = 16;


@interface HGChatSupplementaryReusableView ()

@property (nonatomic, strong) TTTAttributedLabel *lbDescription;

@end


@implementation HGChatSupplementaryReusableView



- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self) {
        
        self.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.lbDescription = [[TTTAttributedLabel alloc] init];
        self.lbDescription.translatesAutoresizingMaskIntoConstraints = NO;
        self.lbDescription.clipsToBounds = YES;
        self.lbDescription.layer.cornerRadius = 12;
        self.lbDescription.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.lbDescription.numberOfLines = 0;
        self.lbDescription.lineBreakMode = NSLineBreakByWordWrapping;
        self.lbDescription.font = [UIFont systemFontOfSize:13];
        self.lbDescription.textColor = [UIColor whiteColor];
        self.lbDescription.textInsets = UIEdgeInsetsMake(TopPaddingDescription, 0, BottomPaddingDescription, 0);
        
        [self addSubview:self.lbDescription];
        
        [self configConstraints];
    }
    
    return self;

}


- (void)configText:(NSString *)text style:(NSParagraphStyle *)style boldFirstSentence:(BOOL)bold{
    [self.lbDescription setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        if (bold) {
            NSError *error = nil;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\n(.+)\n" options:NSRegularExpressionCaseInsensitive error:&error];
            
            NSTextCheckingResult *result = [regex firstMatchInString:[mutableAttributedString string] options:0 range:NSMakeRange(0, [mutableAttributedString length])];
            
            [mutableAttributedString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:[result rangeAtIndex:1]];
            
        }
        [mutableAttributedString addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, [mutableAttributedString length])];
        
        return mutableAttributedString;
        
    }];

}


- (void)configConstraints{
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lbDescription attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:TopConstraintDescription]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lbDescription attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:0-TrailingConstraintDescription]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lbDescription attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0-BottomConstraintDescription]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lbDescription attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:LeadingConstraintDescription]];
}




@end

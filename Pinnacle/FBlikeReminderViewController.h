//
//  FBlikeReminderViewController.h
//  Pinnacle
//
//  Created by Alex on 14-12-5.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FBLikerReminderCloseDelegate;

@interface FBlikeReminderViewController : UIViewController

@property (nonatomic, assign) id<FBLikerReminderCloseDelegate> delegate;
@property (nonatomic, strong) UIView* boardview;

@property (nonatomic, copy) NSString *alertTitle;

@end

@protocol FBLikerReminderCloseDelegate<NSObject>

- (void)touchedCloseButtonWithFblikeReminder:(FBlikeReminderViewController *)viewController;

- (void)touchedLikeButtonWithFblikeReminder:(FBlikeReminderViewController *)viewController;

@end

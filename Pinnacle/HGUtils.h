//
//  HGUtils.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-6.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cloudinary/Cloudinary.h"
#import "HGConstant.h"
#import "CMPopTipView.h"


@interface HGUtils : NSObject

@property (nonatomic, strong) CLCloudinary * cloudinary;

+ (HGUtils *)sharedInstance;

/* Upload to Cloudinary */
- (NSString *)cloudinaryLink:(NSString *)sourceLink width:(int)idealWidth height:(int)idealHeight;


- (void)uploadPhoto:(UIImage *)image delegate:(id<CLUploaderDelegate>)delegate;
- (CLUploader*)uploadPhoto:(UIImage *)image withSignedOptions:(NSDictionary *)options delegate:(id<CLUploaderDelegate>)delegate;

/* Get system paths */
- (NSString *)documentDirectoryPath;

/* CGRect manipulate */
- (CGRect)changeFrameHeight:(CGRect)originalFrame toHeight:(CGFloat)newHeight;
- (CGRect)changeFrameHeight:(CGRect)originalFrame offset:(CGFloat)offset;

/* Number format */
- (NSString *)formatInteger:(NSInteger)value;


/* GA Tracking */
- (void)gaTrackViewName:(NSString *)viewName;
- (void)gaSendEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value;



- (UIImage *)centerCropImage:(UIImage *)image;
-(UIImage*)thumbnailOfImage:(UIImage*)image withSize:(CGSize)aSize;
-(UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2;




- (NSString *)getIPAddress;
-(UIColor*)FancyColorWithString:(NSString*)colorvalue;

-(NSString *) md5HexDigest:(NSString*)input;
-(NSString*)getBranchUrl:(NSMutableDictionary*)params;
- (void) addHeadPadding:(CGFloat)padding forTextField:(UITextField*) textField;

- (void)showSimpleAlert:(NSString *)alertString;
- (void)showSimpleAlert:(NSString *)alertString withTitle:(NSString *)title;
- (void)showSimpleAlert:(NSString *)alertString withTitle:(NSString *)title completion:(void(^)())completion;
- (void)showSimpleAlert:(NSString *)alertString withTitle:(NSString *)title cancelTitle:(NSString *)cancelTitle otherTitle:(NSString *)otherTitle completion:(void(^)(BOOL cancelled, NSInteger buttonIndex))completion;
- (void)showSimpleNotify:(NSString *)alertString onView:(UIView *)view;

-(int)getImageSuggestEdgeLength:(int)edgeLength;

-(NSString *)friendlyDate:(NSDate *)date;

- (UIImage*) mergedImageOnMainImage:(UIImage *)mainImg WithImageArray:(NSArray *)imgArray AndImagePointArray:(NSArray *)imgPointArray;
- (NSString *)prettyDate:(NSInteger)timestamp;

- (NSString *)getDistanceKm:(CGFloat)distance;
- (NSString *)getDistanceMiles:(CGFloat)distance;

- (NSString *)deviceString;

- (NSString *)advertisingIdentifier;
-(int)checkPushNotificationStatus;
- (void)presentLoginGuide:(UIViewController *)vc selector:(SEL)sel;
- (void)presentLoginGuide:(UIViewController *)vc;

- (CMPopTipView *)showTipsWithMessage:(NSString *)message atView:(UIView *)atView inView:(UIView *)inView animated:(BOOL)animated withOffset:(CGPoint)offset withSize:(CGSize)size withDirection:(PointDirection)direction;

- (CMPopTipView *)showTipsWithMessage:(NSString *)message atBarButtonItem:(UIBarButtonItem *)barButtonItem animated:(BOOL)animated withOffset:(CGPoint)offset withSize:(CGSize)size;

- (NSString *)apiErrorMessageFrom:(NSError *)error;
-(void)startRatingAppRemind:(UIViewController*)viewController;

-(void)startApplicationSetting;
-(NSString*)getReviewImageNameByScore:(float)score;

-(double)getPreferLat;
-(double)getPreferLon;

- (NSString *)decimalToPercentage:(CGFloat)percentage;

- (UIImage *)compressImage:(UIImage *)image toSize:(CGSize)size withCompressionQuality:(CGFloat)quality;

-(NSString*)getPreferLanguage;
-(float)getSuggestSeperatorLineHeight;

@end

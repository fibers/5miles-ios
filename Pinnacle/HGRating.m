//
//  HGRating.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGRating.h"
#import "HGUser.h"

@implementation HGRating

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid"}];
}
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    
    
    NSArray * names = @[@"reviewer", @"comment",@"score",@"created_at", @"commentHeight", @"replyHeight", @"uid", @"reported", @"reply_content"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    
    return NO;
}
- (NSDate *)timestamp
{
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:self.created_at];
    return date;
}
@end

//
//  UIStoryboard+PinnacleViewController.h
//  Pinnacle
//
//  Created by shengyuhong on 15/2/26.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (Pinnacle)

+ (UIStoryboard *)mainStoryboard;
+ (UIStoryboard *)loginStoryboard;
+ (UIStoryboard *)searchStoryboard;
+ (UIStoryboard *)uploadStoryboard;
+ (UIStoryboard *)messageStoryboard;
+ (UIStoryboard *)profileStoryboard;

@end

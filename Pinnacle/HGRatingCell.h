//
//  HGRatingCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HGRating;

@protocol HGRatingCellDelegate <NSObject>

- (void)onTapReportIcon:(HGRating *)rating;
-(void)onTapReplyIcon:(HGRating*)rating;

@end

@interface HGRatingCell : UITableViewCell

@property (nonatomic, assign) id<HGRatingCellDelegate> delegate;
@property (nonatomic, strong) HGRating * rating;
@property (nonatomic, strong) UIView* SeperatorView;
@property (nonatomic, strong) UILabel* ratingUserName;

@property (nonatomic, strong) NSString* nickname;


+ (CGFloat)cellHeightForRating:(HGRating *)rating boundingWidth:(CGFloat)width;

- (void)configWithEntity:(HGRating *)rating isMyRatingView:(BOOL)bIsMyRatingView;

@end

@interface HGRatingCellDrawingView : UIView

@property (nonatomic, weak) HGRatingCell * ownerCell;

- (id)initWithFrame:(CGRect)frame ownerCell:(HGRatingCell *)cell;

@end
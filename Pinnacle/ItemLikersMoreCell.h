//
//  ItemLikersMoreCell.h
//  Pinnacle
//
//  Created by Alex on 14-12-3.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemLikersMoreCell : UITableViewCell<UIActionSheetDelegate>
@property (strong, nonatomic)  UIImageView *avatar;
@property (strong, nonatomic)  UILabel *userName;
@property (strong, nonatomic) UIView* SeperatorView;

@property (nonatomic, strong)UIImageView* userAvatar_verifyMark;


@property (nonatomic, strong) UIButton* followButton;
@property (nonatomic, strong) UIImageView* followIcon;

@property (nonatomic, strong) NSString* uid;
@property (nonatomic, assign) BOOL following;



-(void)configCell:(NSObject*)object;
@end

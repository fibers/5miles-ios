//
//  HGAppDelegate.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-10.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//
 
#import <UIKit/UIKit.h>
#import "GexinSdk.h"
#import "HGTabBarViewController.h"
#import <MapKit/MapKit.h>
#import "AppsFlyerTracker.h"

@interface HGAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) HGTabBarViewController * tabController;
 

- (void)logUserOut;

@end

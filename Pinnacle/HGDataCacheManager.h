//
//  HGDataCacheManager.h
//  Pinnacle
//
//  Created by Alex on 15-3-2.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


#define MSG_SYSTEM_KEY @"msg_system_key"
#define MSG_BUYING_KEY @"msg_buying_key"
#define MSG_SELLING_KEY @"msg_selling_key"

@interface HGDataCacheManager : NSObject
+ (HGDataCacheManager *)sharedInstance;


-(void)updateCacheData:(NSString*)keyString withValueString:(NSString*)valueString;
-(NSString*)getCacheData:(NSString*)keyString;
@end

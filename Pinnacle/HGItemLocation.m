//
//  HGItemLocation.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-19.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemLocation.h"
#import "HGShopItem.h"
#import "HGUser.h"

@interface HGItemLocation ()

@property (nonatomic, strong) HGShopItem * item;
@property (nonatomic, assign) CLLocationCoordinate2D itemPos;

@end

@implementation HGItemLocation

- (id)initWithItem:(HGShopItem *)item
{
    self = [super init];
    if (self) {
        self.item = item;
        self.itemPos = CLLocationCoordinate2DMake(self.item.geoLocation.lat, self.item.geoLocation.lon);
    }
    return self;
}

- (NSString *)title
{
    return self.item.title;
}

- (NSString *)subtitle
{
    return self.item.seller.displayName;
}

- (CLLocationCoordinate2D)coordinate
{
    return self.itemPos;
}

@end

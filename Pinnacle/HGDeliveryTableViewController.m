//
//  HGDeliveryTableViewController.m
//  Pinnacle
//
//  Created by Alex on 14-11-12.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGDeliveryTableViewController.h"
#import "HGConstant.h"
#import "HGAppData.h"
#import "HGUtils.h"
@interface HGDeliveryTableViewController ()
@property (nonatomic, strong) UIImageView* selected_image;
@end

@implementation HGDeliveryTableViewController

- (void)viewDidLoad {
    
     [super viewDidLoad];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"delivery_select" label:nil value:nil];
    
     [[HGUtils sharedInstance] gaTrackViewName:@"deliveryselect_view"];
    bUserMakeSelect = NO;
   

    self.navigationItem.title = NSLocalizedString(@"Select delivery",nil);
    
    self.selected_image = [[UIImageView alloc] initWithFrame:CGRectMake(280, 14, 65/4, 51/4)];
    self.selected_image.image = [UIImage imageNamed:@"cell-selected-icon"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *TableSampleIdentifier = @"TableDeliveryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             TableSampleIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:TableSampleIdentifier];
    }
    cell.textLabel.textColor  = SYSTEM_DEFAULT_FONT_COLOR_1;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = NSLocalizedString(@"Local exchange", nil);
            
            break;
            
        case 1:
            cell.textLabel.text = NSLocalizedString(@"Shipping", nil);
            
            break;
        case 2:
            cell.textLabel.text = NSLocalizedString(@"Local exchange or shipping", nil);
            
            break;
        default:
            break;
    };
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"deliveryselect_view" action:@"delivery_chagne" label:nil value:nil];
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    //[self.selected_image removeFromSuperview];
    [cell addSubview:self.selected_image];

    bUserMakeSelect = YES;
    [HGAppData sharedInstance].sellingItemDeliveryString = cell.textLabel.text;
    [HGAppData sharedInstance].sellingItemDeliveryType =(int) indexPath.row + 1;
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
}
static BOOL bUserMakeSelect = NO;
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!bUserMakeSelect) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"deliveryselect_view" action:@"delivery_back" label:nil value:nil];
    }
}
@end

//
//  HGMyItemsController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGMyItemsController.h"
#import "HGAppData.h"
#import "HGUtils.h"
#import "HGPurchaseItemCell.h"
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <SVProgressHUD.h>
#import "HGItemDetailController.h"
#import "MGSwipeButton.h"
#import "HGAppDelegate.h"
#import "BDKNotifyHUD.h"
#import "UIStoryboard+Pinnacle.h"
#import "UIApplication+Pinnacle.h"
#import "HGProfilePhoneViewController.h"

@interface HGMyItemsController ()
@property (nonatomic, strong) NSMutableArray * items;
@property (nonatomic, strong) NSString * nextLink;
@property (nonatomic, strong) UIView* noMoreHintView;
@property (nonatomic, assign) BOOL bHasUserClickActionBeforeReturn;
@end

@implementation HGMyItemsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    [self.tableView addSubview:self.noMoreHintView];
    [self.noMoreHintView setHidden:YES];
}
-(void)customizeBackButton
{
    
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
}
- (void)viewDidLoad
{
    
    self.bHasUserClickActionBeforeReturn = NO;
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self customizeBackButton];
    // Do any additional setup after loading the view.
    self.items = [@[] mutableCopy];
    self.nextLink = @"";
    self.tableView.delegate = self;
    [self addNoMoreHintView];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"My Listings", nil);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMyItemDetailViewDelete:) name:NOTIFICATION_USER_ITEMS_DELETE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMyItemStatusChange:) name:NOTIFICATION_USER_ITEMS_STATUS_CHANGE object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onPhoneVerifySuccess:)
                                                 name:HGProfilePhoneViewControllerVerifySuccessNotification
                                               object:nil];
    
    
    
    [self getItems];
    
}
-(void)handleMyItemDetailViewDelete:(NSNotification*)notification
{
    NSDictionary* dict =  [notification object];
    NSString* itemid = [dict objectForKey:@"item_id"];
    
    for (int i = 0; i<self.items.count; i++) {
        HGShopItem* item = [self.items objectAtIndex:i];
        if ([item.uid isEqualToString:itemid]) {
            [self.items removeObjectAtIndex:i];
            [self.tableView reloadData];
        }
    }
    
}
-(void)handleMyItemStatusChange:(NSNotification*)notification
{
    NSDictionary* dict = [notification object];
    NSString* itemid = [dict objectForKey:@"item_id"];
    int newItemStatus = ((NSNumber*)[dict objectForKey:@"new_item_status"]).intValue;
    
    for (int i = 0; i<self.items.count; i++) {
        HGShopItem* item = [self.items objectAtIndex:i];
        if ([item.uid isEqualToString:itemid]) {
            item.state = newItemStatus;
            [self.tableView reloadData];
        }
    }
}

- (void)onPhoneVerifySuccess:(NSNotification *)notification {
    [self getItems];
}

-(void)getItems
{
    __weak HGMyItemsController * weakSelf = self;
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"my_sellings/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        
        [self.items removeAllObjects];
        for (id dictObj in results) {
            NSError * error = nil;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                [self.items addObject:item];
            } else {
                NSLog(@"shop item creation error: %@", error);
            }
        }
        
        [self.tableView reloadData];
        
        if (self.items.count == 0) {
            [self addDefaultEmptyView];
        }
        
        if (![self.nextLink isEqual:[NSNull null]]) {
            if (!self.tableView.showsInfiniteScrolling) {
                self.tableView.showsInfiniteScrolling = YES;
            }
            
            [self.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf _loadNextPage];
            }];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        
        
    }];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"myitems_view"];

    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.tabController.tabBar.hidden =YES;
    self.tabBarController.tabBar.hidden = YES;
}

-(void)handleMyItemEdit:(NSNotification *)notification
{
    
    
    
    __weak HGMyItemsController * weakSelf = self;
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"my_sellings/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        
        [self.items removeAllObjects];
        for (id dictObj in results) {
            NSError * error = nil;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                [self.items addObject:item];
            } else {
                NSLog(@"shop item creation error: %@", error);
            }
        }
      
        [self.tableView reloadData];
        
        if (![self.nextLink isEqual:[NSNull null]]) {
            [self.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf _loadNextPage];
            }];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"get my selling listings error: %@", error);
    }];
}
-(void)addDefaultEmptyView
{
    int iconYoffset = 144;
    int lineOffset = 10;

    self.view.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];

    CGRect rx = [ UIScreen mainScreen ].bounds;
    int iphone4_Y_offset_config_Value = 0;
    if (rx.size.height <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    CGPoint iconCenter  = CGPointMake(rx.size.width/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [self.view addSubview:icon];
    
    
    CGPoint centerPoint = CGPointMake(rx.size.width/2, CGRectGetMaxY(icon.frame)+ lineOffset + 60/2);
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 78)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"Cash up now by publish your first listing and become a super seller! You can come back here at anytime to view, edit or delete.", nil);
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [self.view addSubview:textLabel];
    
    
    CGPoint buttonCenter = CGPointMake(rx.size.width/2, CGRectGetMaxY(textLabel.frame)+lineOffset+ 36/2);
    UIButton* button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 200, 36)];
    button.layer.cornerRadius = 4.0;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = DEFAULT_EMPTY_BOARDER_COLOR;
    UILabel* buttonTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    buttonTitle.textAlignment = NSTextAlignmentCenter;
    buttonTitle.text = NSLocalizedString(@"Publish your first listing!", nil);
    buttonTitle.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [button addSubview:buttonTitle];
    button.center = buttonCenter;
    [self.view addSubview:button];
    [button addTarget:self action:@selector(defaultEmptyAction) forControlEvents:UIControlEventTouchUpInside];
}
-(void)defaultEmptyAction
{
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"myitems_view" action:@"item_empty" label:nil value:nil];
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    {
        UINavigationController *navUpload = (UINavigationController *)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"NavUpload"];
        [appDelegate.tabController presentViewController:navUpload animated:YES completion:nil];
    }
    
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    
    
    if(!self.bHasUserClickActionBeforeReturn)
    {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myitems_view" action:@"myitems_back" label:nil value:nil];
    }
}

-(void)dealloc
{
    //[super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Notification.Item.Edit" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_USER_ITEMS_DELETE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_USER_ITEMS_STATUS_CHANGE object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)checkRemoteNotification
{
    if (![UIApplication isEnableRemoteNotification]) {
        
        NSString* const kShowTurnOnNotificationKey = @"ShowTurnOnNotifications";
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowTurnOnNotificationKey]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Turn on notifications", nil)
                                                            message:NSLocalizedString(@"This way, you will see new messages on your iPhone instantly. And you can close some types of notifications in Profile>Setting", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kShowTurnOnNotificationKey];
        }
    }
}

#pragma mark - UITableView Datasource & Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGPurchaseItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PurchaseItemCell" forIndexPath:indexPath];

    // Configure the cell...
    cell.delegate = self;
    cell.CellType = HGItemCellType_MyItem;
    
    HGShopItem * item = [self.items objectAtIndex:indexPath.row];
    
    if (item.state == HGItemStateListing) {
        [self checkRemoteNotification];
    }
    
    [cell configWithEntity:item];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myitems_view" action:@"myitems_product" label:nil value:nil];
    
    self.bHasUserClickActionBeforeReturn = YES;
    
    @try {
        HGShopItem * item = [self.items objectAtIndex:indexPath.row];
        
        HGItemDetailController *vcItemDetail = (HGItemDetailController *)[[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:@"VCItemDetail"];
        vcItemDetail.item = item;
        
        [self.navigationController pushViewController:vcItemDetail animated:YES];

    }
    @catch (NSException *exception) {
        
    }
}



-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    swipeSettings.transition = MGSwipeTransitionStatic;
    
    if (direction == MGSwipeDirectionLeftToRight) {
        /*expansionSettings.buttonIndex = -1;//data.leftExpandableIndex;
        expansionSettings.fillOnTrigger = NO;
        return [self createLeftButtons:3];
         */
        return 0;
    }
    else {
        expansionSettings.buttonIndex = -1;//data.rightExpandableIndex;
        expansionSettings.fillOnTrigger = YES;
        return [self createRightButtons:2];
    }
}





-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    NSIndexPath * path = [self.tableView indexPathForCell:cell];

    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        //delete button
        
        HGShopItem* currentShopItem = [self.items objectAtIndex:path.row];
        
        if(currentShopItem.deletable == 0)
        {
            [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"The listing has offers or has been sold.",nil) withTitle:NSLocalizedString(@"It can't be deleted.",nil)];
            
        }
        else{
            [self delistItem:(int)(path.row)];
            [self.items removeObjectAtIndex:path.row];
            [self.tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationLeft];
        }
        
         
    }
    /*if(direction == MGSwipeDirectionRightToLeft && index == 1)
    {
        //edit button
        [self editItem:(int)path.row];
    }*/
    return YES;
}


#pragma mark - Helpers

- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.tableView.infiniteScrollingView stopAnimating];
        self.tableView.showsInfiniteScrolling = NO;
        [self.noMoreHintView setFrame:CGRectMake(0, self.tableView.contentSize.height, self.tableView.frame.size.width, 40)];
        if(self.items.count!=0)
        {
            [self.noMoreHintView setHidden:NO];
        }
    } else {
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myitems_view" action:@"myitems_loadmore" label:nil value:nil];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[NSString stringWithFormat:@"my_sellings/%@", self.nextLink] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.items addObject:item];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            [self.tableView reloadData];
            [self.tableView.infiniteScrollingView stopAnimating];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self.tableView.infiniteScrollingView stopAnimating];
            NSLog(@"get home items failed: %@", error.localizedDescription);
        }];

    }
}

-(NSArray *) createLeftButtons: (int) number
{
    NSMutableArray * result = [NSMutableArray array];
    UIColor * colors[3] = {[UIColor greenColor],
        [UIColor colorWithRed:0 green:0x99/255.0 blue:0xcc/255.0 alpha:1.0],
        [UIColor colorWithRed:0.59 green:0.29 blue:0.08 alpha:1.0]};
    UIImage * icons[3] = {[UIImage imageNamed:@"check.png"], [UIImage imageNamed:@"fav.png"], [UIImage imageNamed:@"menu.png"]};
    for (int i = 0; i < number && i<3; i++)
    {
        UIColor* currentColor = colors[i];
        UIImage* currentIcon = icons[i];
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:@"" icon:currentIcon backgroundColor:currentColor padding:15 callback:^BOOL(MGSwipeTableCell * sender){
            NSLog(@"Convenience callback received (left).");
            return YES;
        }];
        [result addObject:button];
    }
    return result;
}


-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray * result = [NSMutableArray array];
    NSString* titles[1] = {NSLocalizedString(@"Delete",nil)};
    UIColor * colors[1] = {[UIColor redColor]};
    for (int i = 0; i < number&&i<1; ++i)
    {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:titles[i] backgroundColor:colors[i] callback:^BOOL(MGSwipeTableCell * sender){
            NSLog(@"Convenience callback received (right).");
            return YES;
        }];
        [result addObject:button];
    }
    return result;
}


- (void)delistItem:(int)index
{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myitems_view" action:@"myitems_delete" label:nil value:nil];
    if (self.items.count > index)
    {
         HGShopItem* currentShopItem = [self.items objectAtIndex:index];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"delete_item/" parameters:@{@"item_id": currentShopItem.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
            
            [self alertInformation:NSLocalizedString(@"Item has been deleted.", nil)];
            
            NSLog(@"delete item ok: %@", responseObject);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"delete item failed: %@", error.userInfo);
        }];
    }
}


-(void)alertInformation:(NSString*)alertString
{
    
    BDKNotifyHUD* hud = [BDKNotifyHUD notifyHUDWithView:nil text:alertString withFrameWith:220 withFrameHeight:100];
    
    hud.center = CGPointMake(self.view.center.x, self.view.center.y - 20);
    
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.window addSubview:hud];
    [hud presentWithDuration:1.5f speed:0.5f inView:self.view completion:^{
        [hud removeFromSuperview];
    }];
}

#pragma mark- UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (1 == buttonIndex) {
        [UIApplication gotoSystemSetting];
    }
}

@end

//
//  HGSysMessage.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-28.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "JSONModel.h"

typedef NS_ENUM(NSInteger, HGSysMessageType){
    HGSysMessageTypeCommon,
    HGSysMessageTypeReview
};


@protocol HGSysMessage <NSObject>

@end

@interface HGSysMessage : JSONModel

@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * text; //title
@property (nonatomic, assign) BOOL unread;
@property (nonatomic, assign) NSTimeInterval timestamp;
@property (nonatomic, strong) NSString* action;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* desc;

@property (nonatomic, assign) int image_type; //// 0: item image, 1: user portrait
@property (nonatomic, assign) HGSysMessageType smType;  ////0: commom , 1: reviewe;
@property (nonatomic, assign) float review_score;
- (NSString *)senderName;

@end

@interface HGSysMessageResponseMeta : JSONModel

@property (nonatomic, strong) NSString * next;
@property (nonatomic, strong) NSString * previous;
@property (nonatomic, assign) int limit;
@property (nonatomic, assign) int offset;
@property (nonatomic, assign) int totalCount;
+(void)updateSystemMessageResponseMeta:(HGSysMessageResponseMeta*)destMeta withSourceDeta:(HGSysMessageResponseMeta*)sourceMeta;
@end

@interface HGSysMessageResponse : JSONModel

@property (nonatomic, strong) HGSysMessageResponseMeta * meta;
@property (nonatomic, strong) NSMutableArray<HGSysMessage> * objects;

@end
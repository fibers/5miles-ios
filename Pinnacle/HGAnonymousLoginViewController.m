////
////  HGAnonymousLoginViewController.m
////  Pinnacle
////
////  Created by shengyuhong on 15/3/9.
////  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
////
//
//#import "HGAnonymousLoginViewController.h"
//#import "HGUtils.h"
//#import "HGAppDelegate.h"
//#import "HGAppData.h"
//#import <SVProgressHUD.h>
//#import "HGFBUtils.h"
//
//@interface HGAnonymousLoginViewController ()
//
//@property (weak, nonatomic) IBOutlet UIImageView *imgCancel;
//@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
//@property (weak, nonatomic) IBOutlet UILabel *lbSlogan;
//@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
//@property (weak, nonatomic) IBOutlet UILabel *lbSignTitle;
//@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
//@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
//
//@end
//
//@implementation HGAnonymousLoginViewController
//
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//    
//    [[HGUtils sharedInstance] gaTrackViewName:@"Login_view"];
//
//    [self initContentAndStyle];
//    [self initAction];
//
//}
//
//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
//}
//-(void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    [SVProgressHUD dismiss];
//}
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//#pragma mark - herlpers
//
//- (void)initContentAndStyle{
//    
//    self.imgCancel.image = [UIImage imageNamed:@"cancel.png"];
//    self.imgLogo.image = [UIImage imageNamed:@"sign-logo.png"];
//    
//    self.lbSlogan.text = NSLocalizedString(@"Your Mobile Marketplace", nil);
//    self.lbSlogan.font = [UIFont fontWithName:@"Museo-700" size:15.0f];
//    self.lbSlogan.textColor = FANCY_COLOR(@"818181");
//    self.lbSlogan.textAlignment = NSTextAlignmentCenter;
//    
//    self.lbSignTitle.text = NSLocalizedString(@"or with Email", nil);
//    self.lbSignTitle.font = [UIFont fontWithName:@"Museo-300" size:15.0f];
//    self.lbSignTitle.textColor = FANCY_COLOR(@"818181");
//    self.lbSignTitle.textAlignment = NSTextAlignmentCenter;
//    
//    self.btnFacebook.clipsToBounds = YES;
//    self.btnFacebook.layer.cornerRadius = 4.0f;
//    self.btnFacebook.titleLabel.font = [UIFont systemFontOfSize:18.0f];
//    self.btnFacebook.backgroundColor = FANCY_COLOR(@"4761af");
//    [self.btnFacebook setBackgroundImage:[UIImage imageNamed:@"facebook-login"] forState:UIControlStateNormal];
//    
//    NSString *titleBtnFacebook = NSLocalizedString(@"Login with Facebook", nil);
//    NSRange  range = [titleBtnFacebook rangeOfString:@"Facebook"];
//    if (range.length > 0 ) {
//        UIFont *font = [UIFont fontWithName:@"Facebook Letter Faces" size:(18.0f)];
//        UIColor *fontcolor = [UIColor whiteColor];
//        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:titleBtnFacebook];
//        [attrString addAttribute:NSFontAttributeName value:font range:range];
//        [attrString addAttribute:NSForegroundColorAttributeName value:fontcolor range:NSMakeRange(0, titleBtnFacebook.length)];
//        [self.btnFacebook setAttributedTitle:attrString forState:UIControlStateNormal];
//        [self.btnFacebook setAttributedTitle:attrString forState:UIControlStateHighlighted];
//    }
//    
//    self.btnSignUp.clipsToBounds = YES;
//    self.btnSignUp.layer.cornerRadius = 4.0f;
//    self.btnSignUp.titleLabel.font = [UIFont systemFontOfSize:18.0f];
//    self.btnSignUp.backgroundColor = FANCY_COLOR(@"f2f2f2");
//    [self.btnSignUp setTitle:NSLocalizedString(@"Sign up", nil) forState:UIControlStateNormal];
//    [self.btnSignUp setTitleColor:FANCY_COLOR(@"242424") forState:UIControlStateNormal];
//    
//    self.btnSignIn.clipsToBounds = YES;
//    self.btnSignIn.layer.cornerRadius = 4.0f;
//    self.btnSignIn.titleLabel.font = [UIFont systemFontOfSize:18.0f];
//    self.btnSignIn.backgroundColor = FANCY_COLOR(@"f2f2f2");
//    [self.btnSignIn setTitle:NSLocalizedString(@"Sign in", nil) forState:UIControlStateNormal];
//    [self.btnSignIn setTitleColor:FANCY_COLOR(@"242424") forState:UIControlStateNormal];
//
//}
//
//- (void)initAction{
//    
//    UITapGestureRecognizer *cancelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapCancel:)];
//    self.imgCancel.userInteractionEnabled = YES;
//    [self.imgCancel addGestureRecognizer:cancelGesture];
//
//}
//
//- (void) onTapCancel: (UITapGestureRecognizer* )gesture{
//    [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"close" label:nil value:nil];
//
//    [self dismissViewControllerAnimated:YES completion:^{
//        [[NSNotificationCenter defaultCenter] removeObserver:self.tempPresentingVC name:NOTIFICATION_USER_LOGIN_WITH_ACTION object:nil];
//    }];
//}
//
//
//- (IBAction)onTouchSignUp:(id)sender {
//    [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"signup" label:nil value:nil];
//    [self performSegueWithIdentifier:@"AnonymousLoginToSignUp" sender:nil];
//}
//
//- (IBAction)onTouchSignIn:(id)sender {
//    
//    
//    [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"signin" label:nil value:nil];
//    [self performSegueWithIdentifier:@"AnonymousLoginToSignIn" sender:nil];
//}
//
//- (IBAction)onTouchFacebook:(id)sender {
//    
//    if (![[HGAppData sharedInstance].guestApiClient checkNetworkStatus]) {
//        return;
//    }
//    
//    [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"loginwithfacebook" label:nil value:nil];
//    
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
//    [[HGFBUtils sharedInstance] loginAccountWithSuccess:^(NSDictionary* response){
//        
//        [SVProgressHUD dismiss];
//        
//        [[HGUserData sharedInstance] updateUserLoggedInfo:response withAnonymousSignup:NO withAccountType:HGAccountTypeFacebook];
//        
//        [self dismissViewControllerAnimated:NO completion:^{
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_LOGIN_WITH_ACTION object:self];
//        }];
//        
//    } withFailure:^(NSError* error){
//        [SVProgressHUD dismiss];
//        NSLog(@"Error: %@", error);
//    }];
//
//}
//
//@end

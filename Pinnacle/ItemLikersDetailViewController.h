//
//  ItemLikersDetailViewController.h
//  Pinnacle
//
//  Created by Alex on 14-11-19.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemLikersDetailViewController : UITableViewController
@property (nonatomic, strong)NSString* itemIdString;
@property (nonatomic, strong)NSMutableArray *likersArray;
@property (nonatomic, strong)NSString* nextLink;
@end

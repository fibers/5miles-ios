//
//  HGSystemThemeController.h
//  Pinnacle
//
//  Created by Alex on 14-12-16.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGShopItem.h"
#import "StrikeThroughLabel.h"
#import "HGSystemThemeItemCell.h"

@interface HGSystemThemeController : UITableViewController


@property (nonatomic, strong) UIView* fakeNavigationBar;
@property (nonatomic, strong) UIButton* backButton;
@property (nonatomic, strong) UIButton* bigNaviBackButton;

@property (nonatomic, strong) HGShopItem* itemDetail;



@property (nonatomic, strong) UIImageView* head_theme_image;

@end

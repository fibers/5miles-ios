//
//  HGHomeItemCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <SDWebImage/UIImageView+WebCache.h>
#import "StrikeThroughLabel.h"

@class HGShopItem;

@interface HGHomeItemCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView * imageView;
@property (nonatomic, strong) UIImageView * soldIconView;
@property (nonatomic, strong) UIImageView * locationIcon;

@property (nonatomic, strong) UILabel* itemTitle;
@property (nonatomic, strong) UIView* seperatorView;

@property (nonatomic, strong) UILabel * lbDistance;
@property (nonatomic, strong) UILabel * lbPrice;
@property (nonatomic, strong) StrikeThroughLabel * originalPrice;
@property (nonatomic, strong) UIImageView* homeAudioMark;


@property (nonatomic, strong) UIView* bottomShadow;
@property (nonatomic, strong) UIView* rightShadow;



@property (nonatomic, strong) UIImageView* ItemNewIcon;
- (void)configWithItem:(HGShopItem *)item;

@end

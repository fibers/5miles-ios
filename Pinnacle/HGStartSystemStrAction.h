//
//  HGStartSystemStrAction.h
//  Pinnacle
//
//  Created by Alex on 15-4-9.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HGConstant.h"
@interface HGStartSystemStrAction : NSObject
+ (HGStartSystemStrAction *)sharedInstance;


#define SYSTEM_ACTION_SOURCE_UNDEFINE 0
#define SYSTEM_ACTION_SOURCE_MESSAGECENTER 1

-(void)startHomeBannerAction:(NSString*)actionString withController:(UIViewController*)controller withActionSource:(int)messagesource;

@end

//
//  HGSettingsViewController.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGSettingsViewController : UITableViewController<UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

- (IBAction)onTouchShare:(id)sender;

@property (weak, nonatomic) IBOutlet UITableViewCell *version_cell;

@property (nonatomic, strong) UILabel* versionAutoCheckHintLabel;

@property (weak, nonatomic) IBOutlet UITableViewCell *currencyCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *NotificationCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *AboutUsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *supportCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *PrivacyPolicyCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *TermsOfUseCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *LeaveFeedBackCell;





@end

//
//  HGChatMessageCell.m
//  Pinnacle
//
//  Created by shengyuhong on 15/1/23.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatMessageCell.h"
#import "HGOffer.h"
#import "HGOfferLine.h"
#import "HGShopItem.h"
#import "HGUser.h"
#import "HGUtils.h"
#import <MHPrettyDate.h>
#import "JSQMessagesBubbleImageFactory.h"
#import <UIImageView+WebCache.h>
#import "UIColor+MLPFlatColors.h"
#import "HGAppData.h"


@interface HGChatMessageCell ()

@property (weak, nonatomic) IBOutlet UIImageView *outgoingAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *incomingAvatar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintIncomingAvatarTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintOutgoingAvatarTop;

@property (nonatomic, strong) NSLayoutConstraint *constraintIncomingAvatarMarkTop;
@property (nonatomic, strong) NSLayoutConstraint *constraintOutgoingAvatarMarkTop;

@property (nonatomic, strong) UIImageView *incomingBubble;
@property (nonatomic, strong) UIImageView *outgoingBubble;
@property (nonatomic, strong) UIImageView *incomingAvatarMark;
@property (nonatomic, strong) UIImageView *outgoingAvatarMark;

@property (nonatomic, strong) TTTAttributedLabel *lbTime;

@end


@implementation HGChatMessageCell


- (void)awakeFromNib{
    [super awakeFromNib];
    [self setup];
}

// Setup the style, ignore the position.
- (void)setup{
    
    self.backgroundColor = [UIColor clearColor];
    
    self.incomingAvatar.clipsToBounds = YES;
    self.incomingAvatar.layer.cornerRadius = CGRectGetWidth(self.incomingAvatar.bounds) * 0.5;
    self.incomingAvatar.layer.borderWidth = 2.0f;
    self.incomingAvatar.layer.borderColor = COLOR_AVATAR_BORDER.CGColor;
    self.incomingAvatar.userInteractionEnabled = YES;
    
    self.outgoingAvatar.clipsToBounds = YES;
    self.outgoingAvatar.layer.cornerRadius = CGRectGetWidth(self.outgoingAvatar.bounds) * 0.5;
    self.outgoingAvatar.layer.borderWidth = 2.0f;
    self.outgoingAvatar.layer.borderColor = COLOR_AVATAR_BORDER.CGColor;
    self.outgoingAvatar.userInteractionEnabled = YES;
    
    self.incomingAvatarMark = [[UIImageView alloc] init];
    self.incomingAvatarMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    self.incomingAvatarMark.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview: self.incomingAvatarMark];
    
    [self.incomingAvatarMark addConstraint:[NSLayoutConstraint constraintWithItem:self.incomingAvatarMark attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:self.incomingAvatar.frame.size.width/3]];
    [self.incomingAvatarMark addConstraint:[NSLayoutConstraint constraintWithItem:self.incomingAvatarMark attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:self.incomingAvatar.frame.size.height/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.incomingAvatarMark attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0f constant:(self.incomingAvatar.frame.size.width * 2 / 3 + PADDING_SYSTEM_RECOMMENDED)]];
    
    self.constraintIncomingAvatarMarkTop = [NSLayoutConstraint constraintWithItem:self.incomingAvatarMark attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:(HEIGHT_TIME + self.incomingAvatar.frame.size.height * 2 / 3 + PADDING_SYSTEM_RECOMMENDED)];
    [self addConstraint: self.constraintIncomingAvatarMarkTop];

    self.outgoingAvatarMark = [[UIImageView alloc] init];
    self.outgoingAvatarMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    self.outgoingAvatarMark.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview: self.outgoingAvatarMark];
    
    [self.outgoingAvatarMark addConstraint:[NSLayoutConstraint constraintWithItem:self.outgoingAvatarMark attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:self.outgoingAvatar.frame.size.width/3]];
    [self.outgoingAvatarMark addConstraint:[NSLayoutConstraint constraintWithItem:self.outgoingAvatarMark attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:self.outgoingAvatar.frame.size.height/3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.outgoingAvatarMark attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0f constant:(0 - PADDING_SYSTEM_RECOMMENDED)]];
    
    self.constraintOutgoingAvatarMarkTop = [NSLayoutConstraint constraintWithItem:self.outgoingAvatarMark attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:(HEIGHT_TIME + self.outgoingAvatar.frame.size.height * 2 / 3 + PADDING_SYSTEM_RECOMMENDED)];
    [self addConstraint: self.constraintOutgoingAvatarMarkTop];
    
    JSQMessagesBubbleImageFactory *factory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage imageNamed:@"bubble_self_defined"] capInsets:UIEdgeInsetsZero];
    
    self.incomingBubble = [[UIImageView alloc] initWithImage:[factory incomingMessagesBubbleImageWithColor:[UIColor whiteColor]].messageBubbleImage];
    [self addSubview: self.incomingBubble];
    
    self.outgoingBubble = [[UIImageView alloc] initWithImage:[factory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.996f green:0.808f blue:0.698f alpha:1.00f]].messageBubbleImage];
    [self addSubview: self.outgoingBubble];

    
    self.lbTime = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), HEIGHT_TIME)];
    self.lbTime.clipsToBounds = YES;
    self.lbTime.layer.cornerRadius = 4.0f;
    self.lbTime.textColor = [UIColor whiteColor];
    self.lbTime.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
    self.lbTime.font = [UIFont systemFontOfSize:FONT_SIZE_TIME];
    self.lbTime.textAlignment = NSTextAlignmentCenter;
    self.lbTime.numberOfLines = 1;
    self.lbTime.textInsets = UIEdgeInsetsMake(2, 10, 2, 10);
    [self addSubview: self.lbTime];
    
    
    self.lbMessage = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width/2, self.frame.size.height)];
    self.lbMessage.userInteractionEnabled = YES;
    self.lbMessage.textColor = [UIColor darkTextColor];
    self.lbMessage.font = [UIFont systemFontOfSize:FONT_SIZE_BUBBLE];
    self.lbMessage.enabledTextCheckingTypes = NSTextCheckingTypeLink | NSTextCheckingTypePhoneNumber;
    self.lbMessage.textAlignment = NSTextAlignmentLeft;
    self.lbMessage.numberOfLines = 0;
    self.lbMessage.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview: self.lbMessage];

}


// Call this after the cell height is set.
- (void)configWithOffer:(HGOffer *)offer withOfferLineMeta:(HGOfferLineMeta *)offerLineMeta{
    
    self.offer = offer;
    NSString *currentAvatarLink;
    BOOL currentUserVerified;
    
    // Shit code due to the damned data structure and api logic.
    if(offerLineMeta == nil){
        // if it is the first chat.
        currentAvatarLink = [HGUserData sharedInstance].userAvatarLink;
        currentUserVerified = [HGUserData sharedInstance].bUserAvatarVerified;
    }else{
        HGUser *fromUser = offer.fromBuyer ? offerLineMeta.buyer : offerLineMeta.item.seller;
        currentAvatarLink = fromUser.portraitLink;
        currentUserVerified = fromUser.verified.boolValue;
    }
    
    UIImageView *currentAvatar = nil;
    UIImageView *currentAvatarMark = nil;
    
    CGRect textRect = [offer.text boundingRectWithSize:CGSizeMake(self.frame.size.width/2, CGFLOAT_MAX) options:NSLineBreakByWordWrapping|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE_BUBBLE]} context:nil];
    textRect.size.width = ceilf(textRect.size.width) + WIDTH_TEXT_SUPPLEMENT;
    textRect.size.height = ceilf(textRect.size.height) + HEIGHT_TEXT_SUPPLEMENT;
    
    CGFloat bubbleWidth = textRect.size.width + 2 * X_PADDING_BUBBLE + WIDTH_BUBBLE_TAIL;
    CGFloat bubbleHeight = textRect.size.height + 2 * Y_PADDING_BUBBLE;
    
    NSDate *now = [NSDate date];
    NSString *strDate = [now timeIntervalSinceDate:offer.timestamp] >= 5*60 ? [[HGUtils sharedInstance]friendlyDate:offer.timestamp] : @"";
    
    CGFloat heightLabelTime;
    if( strDate != nil && strDate.length > 0 && offer.bShowTimestamp == 1 ){
        heightLabelTime = HEIGHT_TIME;
        self.lbTime.hidden = NO;
        self.lbTime.text = strDate;
        [self.lbTime sizeToFit];
        self.lbTime.center = CGPointMake(self.frame.size.width/2, HEIGHT_TIME/2);
        self.constraintIncomingAvatarTop.constant = 20.0f + PADDING_SYSTEM_RECOMMENDED;
        self.constraintOutgoingAvatarTop.constant = 20.0f + PADDING_SYSTEM_RECOMMENDED;
        self.constraintIncomingAvatarMarkTop.constant = HEIGHT_TIME + self.incomingAvatar.frame.size.height * 2 / 3 + PADDING_SYSTEM_RECOMMENDED;
        self.constraintOutgoingAvatarMarkTop.constant = HEIGHT_TIME + self.incomingAvatar.frame.size.height * 2 / 3 + PADDING_SYSTEM_RECOMMENDED;
    }else{
        heightLabelTime = 0.0f;
        self.lbTime.hidden = YES;
        self.lbTime.text = @"";
        self.constraintIncomingAvatarTop.constant = PADDING_SYSTEM_RECOMMENDED;
        self.constraintOutgoingAvatarTop.constant = PADDING_SYSTEM_RECOMMENDED;
        self.constraintIncomingAvatarMarkTop.constant = self.outgoingAvatar.frame.size.height * 2 / 3 + PADDING_SYSTEM_RECOMMENDED;
        self.constraintOutgoingAvatarMarkTop.constant = self.outgoingAvatar.frame.size.height * 2 / 3 + PADDING_SYSTEM_RECOMMENDED;
    }
    
    self.lbMessage.text = offer.text;
    
    if(offer.fromMe){
        
        self.outgoingBubble.frame = CGRectMake(
            self.frame.size.width - PADDING_SYSTEM_RECOMMENDED - self.outgoingAvatar.frame.size.width - PADDING_SYSTEM_RECOMMENDED - WIDTH_BUBBLE_TAIL - textRect.size.width - 2 * X_PADDING_BUBBLE,
            heightLabelTime + Y_MARGIN_BUBBLE,
            bubbleWidth,
            bubbleHeight
        );
        
        self.lbMessage.frame = CGRectMake(
            self.outgoingBubble.frame.origin.x + X_PADDING_BUBBLE,
            self.outgoingBubble.frame.origin.y + Y_PADDING_BUBBLE,
            textRect.size.width,
            textRect.size.height
        );
        
        self.incomingAvatar.hidden = YES;
        self.incomingAvatarMark.hidden = YES;
        self.incomingBubble.hidden = YES;
        self.outgoingAvatar.hidden = NO;
        self.outgoingAvatarMark.hidden = NO;
        self.outgoingBubble.hidden = NO;
        
        currentAvatar = self.outgoingAvatar;
        currentAvatarMark = self.outgoingAvatarMark;
        
    }else{
        
        self.incomingBubble.frame = CGRectMake(
            PADDING_SYSTEM_RECOMMENDED + self.outgoingAvatar.frame.size.width + PADDING_SYSTEM_RECOMMENDED,
            heightLabelTime + Y_MARGIN_BUBBLE,
            bubbleWidth,
            bubbleHeight
        );
        
        self.lbMessage.frame = CGRectMake(
            self.incomingBubble.frame.origin.x + X_PADDING_BUBBLE + WIDTH_BUBBLE_TAIL,
            self.incomingBubble.frame.origin.y + Y_PADDING_BUBBLE,
            textRect.size.width,
            textRect.size.height
        );
        
        self.incomingAvatar.hidden = NO;
        self.incomingAvatarMark.hidden = NO;
        self.incomingBubble.hidden = NO;
        self.outgoingAvatar.hidden = YES;
        self.outgoingAvatarMark.hidden = YES;
        self.outgoingBubble.hidden = YES;
        
        currentAvatar = self.incomingAvatar;
        currentAvatarMark = self.incomingAvatarMark;
    }
    
    NSString* avatarLink = [[HGUtils sharedInstance] cloudinaryLink:currentAvatarLink width:currentAvatar.frame.size.width*2 height:0];
    [currentAvatar sd_setImageWithURL:[NSURL URLWithString:avatarLink] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    
    if (currentUserVerified){
        currentAvatarMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        currentAvatarMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }

    UITapGestureRecognizer *tapAvatar = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapAvatar:)];
    [currentAvatar addGestureRecognizer:tapAvatar];
    
}

- (void)onTapAvatar:(UITapGestureRecognizer *)tapGesture{
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onTouchChatAvatar:)]){
        [self.delegate onTouchChatAvatar: self.offer];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

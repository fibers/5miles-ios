//
//  HGZipCodeVC.m
//  Pinnacle
//
//  Created by Alex on 15/5/12.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGZipCodeVC.h"
#import "HGUITextField.h"
#import "HGConstant.h"
#import "HGUserData.h"
#import "HGAppData.h"
#import <SVProgressHUD.h>
#import "PXAlertView+Customization.h"
#import "HGAppDelegate.h"
#import "HGHomeViewController.h"
#import "HGUtils.h"



@interface HGZipCodeVC ()
@property (nonatomic, strong)UIWindow* myKeyBoard;
@property (nonatomic, strong)UIButton* fakeKeyBoardSwitch;


@property (nonatomic, strong)HGUITextField* zipCodeField;
@property (nonatomic, strong)UILabel* hintLabel;
@property (nonatomic, strong)UILabel* hintLabel2;
@property (nonatomic, strong)UIButton* useGPSButton;



@property (nonatomic, strong)UIButton* btnSend;

//@property (nonatomic, strong)UIButton* confirmButton;


@property (nonatomic, strong) UIView* fakeNavigatinoView;
@property (nonatomic, strong) UIButton* closeButton;
@property (nonatomic, strong) NSString* deviceCountryCode;
@end

@implementation HGZipCodeVC




-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
    if (bUserTouchLeave) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"" action:@"" label:nil value:nil];
    }
    
   
}

static BOOL bUserTouchLeave = YES;

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ////TextField has a strange bug, from 8.3..view re show will clear the content.

    self.zipCodeField = [[HGUITextField alloc] initWithFrame:CGRectMake(0, 74, self.view.frame.size.width,44)];
    [self.view addSubview:self.zipCodeField];
    self.zipCodeField.font = [UIFont systemFontOfSize:15];
    self.zipCodeField.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.zipCodeField.delegate = self;
    self.zipCodeField.textAlignment = NSTextAlignmentLeft;
    
    self.zipCodeField.backgroundColor = [UIColor whiteColor];
    self.zipCodeField.edgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    
    self.zipCodeField.keyboardType = UIKeyboardTypeNamePhonePad;

    self.zipCodeField.placeholder = NSLocalizedString(@"Zipcode", nil);
    
    
    if ([HGUserData sharedInstance].zipcodeString.length > 0) {
        self.zipCodeField.text = [HGUserData sharedInstance].zipcodeString;
        if (self.currentViewType == zipViewTypeEditProfile) {
            [self enableSendButton];

        }
    }
    self.zipCodeField.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [[HGUtils sharedInstance] gaTrackViewName:@"changeemail_view"];
    bUserTouchLeave = YES;
    
    
    
    NSLocale *currentLocale = [NSLocale currentLocale];

    NSLog(@"Country Code is %@", [currentLocale objectForKey:NSLocaleCountryCode]);
    self.deviceCountryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    self.navigationItem.title = NSLocalizedString(@"Location",nil);
    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    
    
    self.currentViewType = zipViewTypeUserRegister;
    
    if (self.currentViewType == zipViewTypeEditProfile)
    {
        [self initWithTypeProfile];
    }
    else
    {
        [self initWithTypeRegister];
    }
   
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.zipCodeField becomeFirstResponder];
}

-(void)initWithTypeRegister
{
    
    [[HGUtils sharedInstance] gaTrackViewName:@"zipcodepop_view"];
    self.hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 70+44+5, self.view.frame.size.width-30, 100)];
    [self.view addSubview:self.hintLabel];
    self.hintLabel.textColor = FANCY_COLOR(@"818181");
    self.hintLabel.font = [UIFont systemFontOfSize:14];
    
    self.hintLabel.text = NSLocalizedString(@"5miles needs your location! Please enter a zipcode instead of using your GPS location. This will set the location of your listings, item discovery and calculate distance from other 5milers.", nil);
    
    self.hintLabel.numberOfLines = 0;
    
    
   
    self.navigationController.navigationBar.hidden = YES;
    
    [self addFakeNavigatinoView];
    
    if (self.zipCodeField.text.length >0) {
        [self enableSendButton];
    }
    else{
        [self disableSendButton];
    }
}
-(void)addFakeNavigatinoView
{
    if(self.fakeNavigatinoView==nil)
    {
        self.fakeNavigatinoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 66)];
        [self.view addSubview:self.fakeNavigatinoView];
        self.fakeNavigatinoView.backgroundColor = [UIColor whiteColor];
        
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 130, 30)];
        [self.fakeNavigatinoView addSubview:label];
        label.text = self.navigationItem.title;
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        label.font = [UIFont systemFontOfSize:18];
        label.center= CGPointMake(self.fakeNavigatinoView.center.x, self.fakeNavigatinoView.center.y+10);
        label.backgroundColor = [UIColor clearColor];
        
        float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
        UIView* seperatorLine  = [[UIView alloc] initWithFrame:CGRectMake(0, self.fakeNavigatinoView.frame.size.height-fSeperatorHeight, self.view.frame.size.width, fSeperatorHeight)];
        [self.fakeNavigatinoView addSubview:seperatorLine];
        seperatorLine.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        
        
        self.closeButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 25, 148/2, 60/2)];
        self.closeButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sell_closeIcon"]];
        [self.fakeNavigatinoView addSubview:self.closeButton];
        self.closeButton.hidden = YES;
        [self.closeButton addTarget:self action:@selector(onTouchCloseButton) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIView* rightBackView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 90, 30, 70, 30)];
        
        self.btnSend = [[UIButton alloc] initWithFrame:CGRectMake(13, 0, 65, 30)];
        self.btnSend.clipsToBounds = YES;
        self.btnSend.layer.cornerRadius = 3;
        self.btnSend.layer.borderWidth = 1;
        self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
        self.btnSend.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self.btnSend setTitle:NSLocalizedString(@"Save",nil) forState:UIControlStateNormal];
        [self.btnSend setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
        [self.btnSend setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [self.btnSend addTarget:self action:@selector(onTouchSend) forControlEvents:UIControlEventTouchUpInside];
        
        [rightBackView addSubview:self.btnSend];
        [self.fakeNavigatinoView addSubview:rightBackView];
        
    }
   
    
}
-(void)onTouchCloseButton
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"zipcodepop_view" action:@"zipcodeclose" label:nil value:nil];
    
    [HGUserData  sharedInstance].bUserForceCloseZipView = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initWithTypeProfile
{
    
    
    [[HGUtils sharedInstance] gaTrackViewName:@"changezipcodet_view"];
    self.hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 70+44+5, self.view.frame.size.width-20, 60)];
    [self.view addSubview:self.hintLabel];
    self.hintLabel.textColor = FANCY_COLOR(@"636363");
    self.hintLabel.font = [UIFont systemFontOfSize:13];
    self.hintLabel.numberOfLines = 0;
     self.hintLabel.text = NSLocalizedString(@"Use a Zipcode to confirm your location. This will set the location of your listings, item discovery and calculate distance from other 5milers.", nil);
    self.hintLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(15,  CGRectGetMaxY(self.hintLabel.frame) , self.view.frame.size.width-30, 20)];
    self.hintLabel2.textColor = FANCY_COLOR(@"818181");
    self.hintLabel2.numberOfLines = 2;
    self.hintLabel2.font = [UIFont systemFontOfSize:14];
    self.hintLabel2.text = NSLocalizedString(@"Or", nil);
    self.hintLabel2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.hintLabel2];

    self.useGPSButton = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(self.hintLabel.frame) + 30, self.view.frame.size.width -30, 36)];
    self.useGPSButton.layer.cornerRadius = 4;
    self.useGPSButton.backgroundColor = FANCY_COLOR(@"cccccc");
    self.useGPSButton.titleLabel.textColor = [UIColor whiteColor];
    [self.useGPSButton setTitle:NSLocalizedString(@"Current location", nil) forState:UIControlStateNormal];
    
    [self.useGPSButton addTarget:self action:@selector(onTouchUseGPS) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.useGPSButton];
    [self enalbGPSButton];
    [self addRightButton];
}
-(void)enalbGPSButton
{
    self.useGPSButton.userInteractionEnabled = YES;
    self.useGPSButton.backgroundColor = FANCY_COLOR(@"ff8830");
}
-(void)disableGPSButton
{
    self.useGPSButton.userInteractionEnabled=NO;
    self.useGPSButton.backgroundColor = FANCY_COLOR(@"cccccc");
}

-(void)GPSFailHandle
{
    
    NSString* titleString =  NSLocalizedString(@"5miles requires access to your location to discover great listings nearby. Please allow access to Location Services in your settings", nil);
    NSAttributedString* attriSting = [[NSAttributedString alloc] initWithString:titleString];
    if (IOS_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        PXAlertView *alert = [PXAlertView showAlertWithTitle:nil messageWithAttribute:attriSting cancelTitle:NSLocalizedString(@"Cancel", nil) otherTitle:NSLocalizedString(@"Setting", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            if(!cancelled){
                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
            else{
                [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Without access to your current location, you must use the zipcode.", nil)];
            }
        }];
        
        
        [alert useDefaultIOS7Style];
        
    }
    else{
        PXAlertView *alert = [PXAlertView showAlertWithTitle:nil messageWithAttribute:attriSting cancelTitle:NSLocalizedString(@"OK", nil) otherTitle:nil completion:^(BOOL cancelled, NSInteger buttonIndex) {
        }];
        
        [alert useDefaultIOS7Style];
    }
}
-(void)onTouchUseGPS
{
   
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusDenied && [HGUserData sharedInstance].zipcodeString.length == 0)
    {
        [self GPSFailHandle];
        
    }
    else{
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"zipcodepop_view" action:@"people_gps" label:nil value:nil];
        ///clear local history
        [HGUserData sharedInstance].zipcodeString = @"";
        
        HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController* Navi = appDelegate.tabController.viewControllers[0];
        HGHomeViewController* homeViewcontroller= Navi.viewControllers[0];
        [homeViewcontroller freshHomeItemSilent];
        
       
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)onTouchSend
{
    if (self.currentViewType == zipViewTypeUserRegister) {
         [[HGUtils sharedInstance] gaSendEventWithCategory:@"zipcodepop_view" action:@"zipcodeconfirm" label:nil value:nil];
    }
    else{
         [[HGUtils sharedInstance] gaSendEventWithCategory:@"changezipcodet_view" action:@"zipcodesave" label:nil value:nil];
    }
   
    
    NSString* nameString = [self.zipCodeField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (nameString.length == 0) {
        PXAlertView * view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Please input a valid name.", nil)];
        [view useDefaultIOS7Style];
        self.zipCodeField.text = [HGUserData sharedInstance].userDisplayName;
        return;
    }
    
    
    @try {
        [SVProgressHUD show];
        [self getZipCodeAddress:self.zipCodeField.text];

    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
          [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
    }
   
    
    
}
-(void)addRightButton
{
    UIView* rightBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    
    self.btnSend = [[UIButton alloc] initWithFrame:CGRectMake(13, 0, 65, 30)];
    self.btnSend.clipsToBounds = YES;
    self.btnSend.layer.cornerRadius = 3;
    self.btnSend.layer.borderWidth = 1;
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnSend.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.btnSend setTitle:NSLocalizedString(@"Save",nil) forState:UIControlStateNormal];
    [self.btnSend setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [self.btnSend setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.btnSend addTarget:self action:@selector(onTouchSend) forControlEvents:UIControlEventTouchUpInside];
    
    [rightBackView addSubview:self.btnSend];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBackView];
    
    if (self.zipCodeField.text.length == 0) {
        [self disableSendButton];

    }
    else
    {
        [self enableSendButton];
    }
    
}

- (void)enableSendButton{
    self.btnSend.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.btnSend.enabled = YES;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)disableSendButton{
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnSend.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString* keyString = [newString stringByReplacingOccurrencesOfString:@" "  withString:@""];
    if (keyString.length==0) {
        if (self.currentViewType == zipViewTypeUserRegister) {
            [self disableSendButton];
        }
        else{
            [self disableSendButton];
            [self enalbGPSButton];
        }
    }
    else{
        if (self.currentViewType == zipViewTypeUserRegister) {
            [self enableSendButton];
        }
        else{
            [self enableSendButton];
            [self disableGPSButton];
        }
    }
    
    
    return YES;
}


-(void)getZipCodeAddress:(NSString*)zipcode
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    
    NSString* baseUrl = [@"http://maps.google.com/maps/api/geocode/json?components=postal_code:" stringByAppendingFormat:@"%@", zipcode] ;
    baseUrl = [baseUrl stringByAppendingString:@"&language=en&sensor=false"];
    
    [request setURL:[NSURL URLWithString:baseUrl]];
    
    //Send an asynchronous request so it doesn't pause the main thread
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        
        NSHTTPURLResponse *responseCode = (NSHTTPURLResponse *)response;
        if ([responseCode statusCode] == 200)
        {
            if (data == nil) {
                return;
            }
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"the zipcode address dict is %@", dict);
            
            if (![[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
                [SVProgressHUD dismiss];
                [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Sorry, we can't recognize this zipcode.", nil)];
                if (self.currentViewType == zipViewTypeEditProfile) {
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"changezipcodet_view" action:@"zipcodewrong" label:nil value:nil];
                }
                else{
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"zipcodepop_view" action:@"zipcodewrong" label:nil value:nil];
                    if (self.closeButton!=nil) {
                        self.closeButton.hidden = NO;
                    }
                }
                return;
            }
            NSArray* addressArray = [dict objectForKey:@"results"];
           
            int nBestAddressIndex =[self getBestMatchAddressIndex:addressArray];
            
            NSDictionary* topAddress = [addressArray objectAtIndex:nBestAddressIndex];
            
            NSArray* address_components =[topAddress objectForKey:@"address_components"];
            
            
            ///clear local history
            {
                [HGUserData sharedInstance].zipCountry = @"";
                [HGUserData sharedInstance].zipCity = @"";
                [HGUserData sharedInstance].zipRegion = @"";
            }
            ///end of clear local history
            for (int i =0; i<address_components.count; i++) {
                NSDictionary* address_component = [address_components objectAtIndex:i];
                NSArray* types = [address_component objectForKey:@"types"];
                NSString* topType = [types objectAtIndex:0];
                if ([topType isEqualToString:@"country"]) {
                    [HGUserData sharedInstance].zipCountry = [address_component objectForKey:@"long_name"]!=nil?[address_component objectForKey:@"long_name"]:@"" ;
                }
                if ([topType isEqualToString:@"locality"]) {
                    [HGUserData sharedInstance].zipCity = [address_component objectForKey:@"short_name"]!=nil?[address_component objectForKey:@"short_name"]:@"";
                }
                if ([topType isEqualToString:@"administrative_area_level_1"]) {
                    [HGUserData sharedInstance].zipRegion = [address_component objectForKey:@"short_name"]!=nil? [address_component objectForKey:@"short_name"]:@"";
                }
            }
            
            NSDictionary* geometry = [topAddress objectForKey:@"geometry"];
            NSDictionary* location = [geometry objectForKey:@"location"];
            [HGUserData sharedInstance].zipLat = [(NSNumber*)[location objectForKey:@"lat"] stringValue];
            [HGUserData sharedInstance].zipLon = [(NSNumber*)[location objectForKey:@"lng"] stringValue];
            
            BOOL bHasZipCodeUpdated = NO;
            if (![[HGUserData sharedInstance].zipcodeString isEqualToString:self.zipCodeField.text]) {
                bHasZipCodeUpdated = YES;
            }
            [HGUserData sharedInstance].zipcodeString = self.zipCodeField.text;
            
            
            [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"fill_profile/" parameters:@{@"zipcode":self.zipCodeField.text,
                @"lat":[HGUserData sharedInstance].zipLat,
                @"lon":[HGUserData sharedInstance].zipLon,
                @"country":[HGUserData sharedInstance].zipCountry,
                @"region":[HGUserData sharedInstance].zipRegion,
                @"city":[HGUserData sharedInstance].zipCity
                }
                success:^(NSURLSessionDataTask *task, id responseObject) {
                
                
                
                [SVProgressHUD dismiss];
                    
                    
                if (self.currentViewType == zipViewTypeEditProfile) {
                    [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"We've successfully set your zipcode.", nil) onView:self.view];
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"changezipcodet_view" action:@"zipcoderight" label:nil value:nil];
                }
                else{
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"zipcodepop_view" action:@"zipcoderight" label:nil value:nil];
                }
                
                if (bHasZipCodeUpdated) {
                     HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
                     UINavigationController* Navi = appDelegate.tabController.viewControllers[0];
                     HGHomeViewController* homeViewcontroller= Navi.viewControllers[0];
                     [homeViewcontroller freshHomeItemSilent];
                }
                    
                    
                bUserTouchLeave = NO;
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                
               
                [SVProgressHUD dismiss];
                ///temply keep
                PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Sorry, we can't recognize this zipcode.", nil) message:error.localizedDescription];
                [view useDefaultIOS7Style];
            }];
            
        }
        else{
            [SVProgressHUD dismiss];
        }
        
        
        
    }];
    

}

-(int)getBestMatchAddressIndex:(NSArray*)resutls
{
    int nBestIndex = 0;
    //int USAIndex = 0;
    for (int i = 0; i< resutls.count; i++) {
        NSDictionary* address = [resutls objectAtIndex:i];
        NSArray* address_components =[address objectForKey:@"address_components"];
        
        for (int j =0; j<address_components.count; j++) {
            NSDictionary* address_component = [address_components objectAtIndex:j];
            NSArray* types = [address_component objectForKey:@"types"];
            NSString* topType = [types objectAtIndex:0];
            if ([topType isEqualToString:@"country"]) {
                NSString* countryShortName  = [address_component objectForKey:@"short_name"]!=nil?[address_component objectForKey:@"short_name"]:@"" ;
                if ([countryShortName isEqualToString:self.deviceCountryCode]) {
                    if (nBestIndex==0) {
                        //only using the first country match.
                        nBestIndex = i;
                    }
                }
            }
        }

    }
    
    
    if (!(nBestIndex<resutls.count)) {
        nBestIndex = 0;
    }
    return nBestIndex;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}

@end

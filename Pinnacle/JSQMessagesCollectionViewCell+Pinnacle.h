//
//  JSQMessagesCollectionViewCell+Pinnacle.h
//  Pinnacle
//
//  Created by shengyuhong on 15/5/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSQMessagesCollectionViewCell.h"
#import "JSQMessage.h"

extern const NSInteger TagIndicatorFailed;

@interface JSQMessagesCollectionViewCell (Pinnacle)

- (void)configAvatar:(NSURL *)url verified:(BOOL)verified;
- (void)configTimeText:(NSString *)timeText;
- (void)configSendingIndicator:(JSQMessage *)message;
- (void)configFailedIndicator:(JSQMessage *)message;

@end

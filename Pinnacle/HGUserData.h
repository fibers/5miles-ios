//
//  HGUserData.h
//  Pinnacle
//
//  Created by x4 on 15/5/4.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//
//  The user data manger.
//  All public variables in this class are stored to NSUserDefaults automatically after set.
//  When add a new variable, please make sure you comply with the following steps.
//  Note:
//      1. Add the variable declaration in normal way. (Custome type is not allowed. Try to use
//         NSDictionary instead of self-defined class).
//      2. If you want to initialize a different default value from the one which objc provided. Please
//         add a entry in function : (NSDictionary *)defaultValues. Otherwise it will use the default
//         value that defined in (id)getDefaultEmptyValue:(NSString *)type
//      3. If the added variable is a global variable, add a item in (NSArray *)globalData.
//         If the added variable is a local variable, add a item in (NSArray *)localData.
//      4. If there is a different nil value strategy rather than using the default value, please add a
//         function named with "- (void){Variable Name}Init"
//      5. If there is some logic need to be execute after the value changed, please add a function named
//         with "- (void){Variable Name}ChangedAction:(Variable Type:)newValue

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "HGUser.h"

typedef NS_ENUM(NSInteger, HGAccountType) {
    HGAccountTypeEmail = 0,
    HGAccountTypeFacebook
};


@interface HGUserData : NSObject

/**************** User global key ********************/

@property (nonatomic, strong) NSString *fmUserID;
@property (nonatomic, strong) NSString *fmUserToken;
@property (nonatomic, strong) NSString *userDisplayName;
@property (nonatomic, strong) NSString *userAvatarLink;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, assign) NSInteger accountType;

@property (nonatomic, assign) BOOL bLoggedIn;
@property (nonatomic, assign) BOOL bAnonymous;
@property (nonatomic, assign) BOOL bUserAvatarVerified;
@property (nonatomic, assign) BOOL bUserEmailVerified;
@property (nonatomic, assign) BOOL bUserFBVerified;
@property (nonatomic, assign) BOOL bUserPhoneVerified;
@property (nonatomic, assign) BOOL bUserForceCloseZipView;



@property (nonatomic, strong) NSString *facebookUserID;
@property (nonatomic, strong) NSString *facebookAccessToken;
@property (nonatomic, strong) NSDate *facebookTokenExpirationDate;


/**************** User local key ********************/

@property (nonatomic, strong) NSString* zipcodeString;
@property (nonatomic, strong) NSString* zipLat;
@property (nonatomic, strong) NSString* zipLon;
@property (nonatomic, strong) NSString* zipCountry;
@property (nonatomic, strong) NSString* zipRegion;
@property (nonatomic, strong) NSString* zipCity;


@property (nonatomic, assign) BOOL bShowVerifyRemind;
@property (nonatomic, assign) BOOL bSellingFirstTime;

@property (nonatomic, assign) BOOL serviceSellingFirstTime;

@property (nonatomic, assign) BOOL firstClosedProfilePhonePage;

@property (nonatomic, assign) BOOL showServiceVerifyRemind;

@property (nonatomic, assign) BOOL bNeedProfileIntro;
@property (nonatomic, assign) BOOL bNeedShakingIntro;

@property (nonatomic, assign) BOOL bHomeShowFBLike;
@property (nonatomic, assign) BOOL bHomeShowRateApp;
@property (nonatomic, assign) BOOL bHomeShowUserNearby;
@property (nonatomic, assign) BOOL bHomeShowFollowInstruction;

@property (nonatomic, assign) BOOL bOtherUserViewShowRatingInstruction;

@property (nonatomic, assign) BOOL bMessageTabShowInstruction;
@property (nonatomic, assign) BOOL bChatShowReviewTip;
@property (nonatomic, assign) BOOL bChatShowSendMediaTip;

@property (nonatomic, assign) BOOL bShowRatingAppRemind_MarkAsSold;
@property (nonatomic, assign) BOOL bShowRatingAppRemind_FirstReview5star;

@property (nonatomic, assign) BOOL bSellingFBShare;
@property (nonatomic, assign) BOOL bPromptFBShare;

@property (nonatomic, assign) NSInteger enterChatCounter;
@property (nonatomic, assign) NSInteger userLaunchCounter;
@property (nonatomic, strong) NSDictionary *userLocalProfile;
@property (nonatomic, strong) NSArray *userLocalBrand;
@property (nonatomic, strong) NSArray* userLocalMapSearchPOIs;
@property (nonatomic, strong) NSString *preferredCurrency;


+ (HGUserData *)sharedInstance;

- (BOOL)isLoggedIn;
- (void)clearUserInfo;

- (void)updateUserLoggedInfo:(NSDictionary *)info withAnonymousSignup:(BOOL)isAnonymousSignup withAccountType:(HGAccountType)accountType;
- (void)updateFacebookToken:(FBSDKAccessToken *)fbAccessToken;
- (void)updateFacebookProfile:(NSDictionary *)response;

- (HGUser *)currentUser;



@end

//
//  HGRatingsController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGRatingsController.h"
#import "HGRating.h"
#import "HGUtils.h"
#import "HGAppDelegate.h"
#import "HGUser.h"
#import "HGUserViewController.h"
#import "UIStoryboard+Pinnacle.h"
#import <SVProgressHUD.h>
#import "HGAppData.h"
#import "UserReportTableViewController.h"
#import "HGRatingTopCell.h"

@interface HGRatingsController ()

@property(nonatomic, strong)NSMutableArray * objects;
@property(nonatomic, strong)NSString* nextLink;
@property(nonatomic, strong)UIView* noMoreHintView;

@property (nonatomic, assign) BOOL isMyRatingView;
@property(nonatomic, assign) float reviewScore;
@property(nonatomic, assign) int reviewCount;

@property(nonatomic, strong) HGRating* currentTouchRating;


@property (nonatomic, strong) BHInputToolbar *inputToolbar;
@end


#define kStatusBarHeight 20
#define kDefaultToolbarHeight 44
#define kKeyboardHeightLandscape 140

////// feedback as seller.
@implementation HGRatingsController

-(void)addInputToolBar
{
    /* Calculate screen size */
    //self.view.backgroundColor = [UIColor clearColor];
    /* Create toolbar */
    self.inputToolbar = [[BHInputToolbar alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - kDefaultToolbarHeight, [UIScreen mainScreen].bounds.size.width, kDefaultToolbarHeight)];
    [self.view addSubview:self.inputToolbar];
    self.inputToolbar.inputDelegate = self;
    self.inputToolbar.textView.placeholder = @"Placeholder";

    self.inputToolbar.textView.backgroundColor = [UIColor whiteColor];
    self.inputToolbar.textView.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.inputToolbar.textView.font = [UIFont systemFontOfSize:14.0];
    self.inputToolbar.textView.layer.cornerRadius = 4.0;
    self.inputToolbar.textView.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
    self.inputToolbar.textView.layer.borderWidth = 1.0;
    self.inputToolbar.textView.clipsToBounds = YES;
    self.inputToolbar.backgroundColor = FANCY_COLOR(@"f4f4f4");
    
    self.inputToolbar.inputButton.title = NSLocalizedString(@"Send", nil);
    self.inputToolbar.inputButton.tintColor = [UIColor grayColor];
    self.inputToolbar.hidden = YES;
    //[self.inputToolbar.textView becomeFirstResponder];
   
}
-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    [self.tableView addSubview:self.noMoreHintView];
    [self.noMoreHintView setHidden:YES];
}
-(void)customizeBackButton
{
    
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
    NSLog(@"the navigation back button frame is %@", backbutton);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        self.navigationItem.backBarButtonItem = backbutton;
        //using the customize back  image;
        // naviBack_icon2 naviBack
        UIImage *backButtonImage = [[UIImage imageNamed:@"naviBack"]resizableImageWithCapInsets:UIEdgeInsetsMake(0, 18, 0, 21)];
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    }
    else{
        //todo for ios7.0
        [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"naviBack"]];
        [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"naviBack"]];
    }
}
-(void)createTopCellView
{
    if (self.headView == nil) {
        self.headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, headViewHeight)];
        self.headView.backgroundColor = [UIColor whiteColor];
        UIImageView* icon1 = [[UIImageView alloc] initWithFrame:CGRectMake(14, 14, 51, 51)];
        icon1.image = [UIImage imageNamed:@"reviews-head-icon"];
        [self.headView addSubview:icon1];
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(81, 16, 200, 25)];
        titleLabel.numberOfLines = 1;
        titleLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        titleLabel.font = [UIFont systemFontOfSize:16];
        titleLabel.text = NSLocalizedString(@"Average rating:",nil);
        [self.headView addSubview:titleLabel];
        
        
        self.ratingResultView = [[UIImageView alloc] initWithFrame:CGRectMake(81, 45, 116, 15)];
        self.ratingResultView.image = [UIImage imageNamed:@"icon-feedback-score-0"];
        [self.headView addSubview:self.ratingResultView];
        
        self.ratingCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(81 + 12 + 115, 40, 60, 25)];
        self.ratingCountLabel.textColor = FANCY_COLOR(@"818181");
        self.ratingCountLabel.font = [UIFont systemFontOfSize:13];
        self.ratingCountLabel.text = @"(0)";
        [self.headView addSubview: self.ratingCountLabel];
        
        UIView* seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0,76 , self.view.frame.size.width, 14)];
        [self.headView addSubview: seperatorView];
        seperatorView.backgroundColor = FANCY_COLOR(@"eeeeee");
        
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];
    self.objects = [@[] mutableCopy];
    self.nextLink = @"";
    self.currentDisplayUserName = @"";
    [self addNoMoreHintView];
    if ([self.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        [[HGUtils sharedInstance] gaTrackViewName:@"myreviews_view"];
        self.isMyRatingView = YES;
    }
    else{
        //sellerreview_view
         [[HGUtils sharedInstance] gaTrackViewName:@"sellerreview_view"];
    }

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.title = NSLocalizedString(@"Reviews", nil);
    [self customizeBackButton];
    [self createTopCellView];
    
    [self addInputToolBar];

}
-(void)getUserRatingData
{
    __weak HGRatingsController * weakSelf = self;
    if ( !(self.uid != nil && self.uid.length > 0)) {
        return;
    }
    
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"reviews/" parameters:@{@"user_id":self.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        
        [self.objects removeAllObjects];
        for (id dictObj in results) {
            NSError * error = nil;
            HGRating * item = [[HGRating alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                [self.objects addObject:item];
            } else {
                NSLog(@"shop item creation error: %@", error);
            }
        }
        
       
        
        
        if (self.objects.count == 0) {
            if ([self.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
                [self addDefaultEmptyView];
            }
            else{
                [self addDefaultEmptyViewOtherUser];
            }
        }
        
        
        [self.tableView reloadData];
        NSDictionary* res_ext= [responseObject objectForKey:@"res_ext"];
        self.currentDisplayUserName = [res_ext objectForKey:@"nickname"];
        self.reviewScore = ((NSNumber*)[res_ext objectForKey:@"review_score"]).floatValue;
        self.reviewCount = ((NSNumber*)[res_ext objectForKey:@"review_num"]).intValue;
        
        if (![self.nextLink isEqual:[NSNull null]]) {
            [self.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf _loadNextPage];
            }];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"get me(%@) likes failed:%@", [HGUserData sharedInstance].fmUserID, error.userInfo);
    }];

}
-(void)configReviewHead
{
  
    NSString* image_index = [[HGUtils sharedInstance] getReviewImageNameByScore:self.reviewScore];
    
    self.ratingResultView.image = [UIImage imageNamed:image_index];
    
    self.ratingCountLabel.text = [@"" stringByAppendingFormat:@"(%d)",self.reviewCount];
    

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getUserRatingData];
     self.tabBarController.tabBar.hidden = YES;
    [[HGUtils sharedInstance] gaTrackViewName:@"mysellfeedback_view"];
    /* Listen for keyboard */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

-(void)addDefaultEmptyViewOtherUser
{
    int iconYoffset = 144;
    int lineOffset = 10;
    
    CGRect rx = [ UIScreen mainScreen ].bounds;
    int iphone4_Y_offset_config_Value = 0;
    if (rx.size.height <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    CGPoint iconCenter  = CGPointMake(rx.size.width/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [self.view addSubview:icon];
    
    CGPoint centerPoint = CGPointMake(rx.size.width/2, CGRectGetMaxY(icon.frame)+ lineOffset + 60/2);
    
    
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 60)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"No reviews \n Reviews are from both buyers and sellers. You can leave reviews via the Chat view.", nil);
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [self.view addSubview:textLabel];
}
-(void)addDefaultEmptyView
{
    int iconYoffset = 144;
    int lineOffset = 10;

    CGRect rx = [ UIScreen mainScreen ].bounds;
    int iphone4_Y_offset_config_Value = 0;
    if (rx.size.height <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    CGPoint iconCenter  = CGPointMake(rx.size.width/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [self.view addSubview:icon];
    
    CGPoint centerPoint = CGPointMake(rx.size.width/2, CGRectGetMaxY(icon.frame)+ lineOffset + 60/2);
    
    
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 80)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"Reviews are from both buyers and sellers and are valuable to help you secure your next deal! List more listings now to get a good rating", nil);
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [self.view addSubview:textLabel];
    
    CGPoint buttonCenter = CGPointMake(rx.size.width/2, CGRectGetMaxY(textLabel.frame)+lineOffset+ 36/2);
    
    UIButton* button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 200, 36)];
    button.layer.cornerRadius = 4.0;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = DEFAULT_EMPTY_BOARDER_COLOR;
    UILabel* buttonTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    buttonTitle.textAlignment = NSTextAlignmentCenter;
    buttonTitle.text = NSLocalizedString(@"List New listing", nil);
    buttonTitle.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [button addSubview:buttonTitle];
    button.center = buttonCenter;
    [self.view addSubview:button];
    [button addTarget:self action:@selector(defaultEmptyAction) forControlEvents:UIControlEventTouchUpInside];
}
-(void)defaultEmptyAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myreviews_view" action:@"myreview_empty" label:nil value:nil];
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    {
        UINavigationController *navUpload = (UINavigationController *)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"NavUpload"];
        [appDelegate.tabController presentViewController:navUpload animated:YES completion:nil];
    }
    
    
}

#pragma mark - Overrides

- (void)fillObjectsWithServerDataset:(NSArray *)dataset
{
    for (NSDictionary * dictObj in dataset) {
        NSError * error = nil;
        HGRating * rating = [[HGRating alloc] initWithDictionary:dictObj error:&error];
        if (!error) {
            [self.objects addObject:rating];
        } else {
            NSLog(@"rating object creation error: %@", error.userInfo);
        }
    }
    if (self.objects.count == 0 ) {
       
        if (self.isMyRatingView) {
            [self addDefaultEmptyView];
        }
        else{
            [self addDefaultEmptyViewOtherUser];
        }
   }
}



#pragma mark - Table view data source
static int headViewHeight = 90;

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return headViewHeight;
    }
    else{
        HGRating * rating = [self.objects objectAtIndex:indexPath.row - 1];
        CGFloat lineHeight = [HGRatingCell cellHeightForRating:rating boundingWidth:CGRectGetWidth(self.view.bounds)];
        return lineHeight ;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.objects.count == 0) {
        return 0;
    }
    else
    {
        return self.objects.count + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        HGRatingTopCell* cell = [tableView dequeueReusableCellWithIdentifier:@"RatingTopCell" forIndexPath:indexPath];
        
        
        if (self.headView != nil) {
            [self configReviewHead];
        }
        [cell addSubview:self.headView];
        
        //cell.backgroundColor = [UIColor redColor];
        return cell;
    }
    else{
        HGRatingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RatingCell" forIndexPath:indexPath];
        cell.delegate = self;
        // Configure the cell...
        
        int contentIndex = (int)indexPath.row -1;
        
        HGRating * rating = [self.objects objectAtIndex:contentIndex];
        cell.nickname = self.currentDisplayUserName;
        [cell configWithEntity:rating isMyRatingView:self.isMyRatingView];
        
        return cell;

    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ///top cell click : do nothing
    if (indexPath.row == 0) {
        return;
    }
    
    if (!self.isMyRatingView) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerreview_view" action:@"click_sellerreview" label:nil value:nil];
    } else {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myreviews_view" action:@"click_myreview" label:nil value:nil];
    }
    
    int contentIndex = (int)indexPath.row - 1;
    HGRating * rating = [self.objects objectAtIndex:contentIndex];
    
    HGUserViewController *vcUser = (HGUserViewController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCUser"];
    
    HGUser* user = [[HGUser alloc] init];
    user.uid = rating.reviewer.uid;
    vcUser.user = user;
    
    [self.navigationController pushViewController:vcUser animated:YES];
}



- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.tableView.infiniteScrollingView stopAnimating];
        self.tableView.showsInfiniteScrolling = NO;
        
        [self.noMoreHintView setFrame:CGRectMake(0, self.tableView.contentSize.height, self.tableView.frame.size.width, 40)];
        
        if(self.objects.count != 0)
        {
            [self.noMoreHintView setHidden:NO];
        }
        
    } else {
        
        if (self.isMyRatingView) {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"myreviews_view" action:@"myreivews_loadmore" label:nil value:nil];
        }
        else
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerreview_view" action:@"sellerreview_loadmore" label:nil value:nil];
        }
        [SVProgressHUD show];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[NSString stringWithFormat:@"reviews/%@", self.nextLink] parameters:@{@"user_id":self.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            for (id dictObj in results) {
                NSError * error = nil;
                HGRating * item = [[HGRating alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.objects addObject:item];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            [self.tableView reloadData];
            [self.tableView.infiniteScrollingView stopAnimating];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [SVProgressHUD dismiss];
            [self.tableView.infiniteScrollingView stopAnimating];
            NSLog(@"get home items failed: %@", error.localizedDescription);
        }];
        
    }
}

-(void)startReportView:(HGRating*)rating
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myreviews_view" action:@"report_review" label:nil value:nil];
    
    UserReportTableViewController *vcUserReport = [[UserReportTableViewController alloc] initWithNibName:@"UserReportTableViewController" bundle:nil];
    vcUserReport.reportType = ReportReasonTypeReview;
    vcUserReport.reviewID = rating.uid;
    [self.navigationController pushViewController:vcUserReport animated:YES];
}
-(void)startRelyToReview:(HGRating*)rating withContent:(NSString*)contentText;
{
    NSString* reviewID = rating.uid;
    NSString* reviewContent =contentText;
    
    if (reviewID==nil || reviewContent == nil) {
        return;
    }
    
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"post_review_reply/" parameters:@{@"review_id":reviewID, @"reply_content":reviewContent} success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        rating.reply_content = contentText;
        [self.tableView reloadData];
        [[HGUtils sharedInstance] showSimpleNotify: NSLocalizedString(@"reply to Review success",nil) onView:self.view];
        
    }
    failure:^(NSURLSessionDataTask *task, NSError *error) {
        [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"reply to Review fail",nil) onView:self.view];
        [SVProgressHUD dismiss];
    }];
}
- (void)onTapReportIcon:(HGRating *)rating{
    
    if (rating) {
        self.currentTouchRating = rating;
        [self startReportView:self.currentTouchRating];

    }
    
    
   
}

-(void)onTapReplyIcon:(HGRating*)rating
{
    if (rating) {
        
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myreviews_view" action:@"reply_review" label:nil value:nil];
        self.currentTouchRating = rating;
        self.inputToolbar.hidden = NO;
        self.inputToolbar.textView.placeholder = [NSLocalizedString( @"Reply",nil) stringByAppendingFormat:@" %@:", rating.reviewer.nickname]; 
        
        [self.inputToolbar.textView.internalTextView becomeFirstResponder];

    }
}





- (void)keyboardWillChange:(NSNotification *)noti
{
    CGRect keyboardEndFrame = [noti.userInfo[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    CGFloat duration = [noti.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        if (keyboardEndFrame.origin.y == [UIScreen mainScreen].bounds.size.height) {
            CGRect fm = self.inputToolbar.frame;
            fm.origin.y = [UIScreen mainScreen].bounds.size.height - fm.size.height;
            self.inputToolbar.frame = fm;
        } else {
            CGRect fm = self.inputToolbar.frame;
            fm.origin.y = -(keyboardEndFrame.size.height + self.inputToolbar.frame.size.height - self.view.frame.size.height);
            self.inputToolbar.frame = fm;
        }
    }];
}


#pragma mark -
#pragma mark Notifications

-(void)inputButtonPressed:(NSString *)inputText
{
    /* Called when toolbar button is pressed */
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myreviews_view" action:@"reply_reivewsend" label:nil value:nil];
    NSLog(@"Pressed button with text: '%@'", inputText);
    self.inputToolbar.hidden = YES;
    [self.inputToolbar.textView resignFirstResponder];
    [self startRelyToReview:self.currentTouchRating withContent:inputText];
    
}
@end

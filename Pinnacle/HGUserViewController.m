//
//  HGUserViewController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-2.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGUserViewController.h"
#import "HGUserSummaryView.h"
#import "HGUser.h"
#import "HGAppData.h"
#import "HGFollowingViewController.h"
#import "HGFollowersViewController.h"
#import "HGItemDetailController.h"
#import "HGShopItem.h"
#import "HGHomeItemCell.h"
#import "HGUtils.h"
#import "HGRatingsController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "UIColor+FlatColors.h"
#import "SVPullToRefresh.h"
#import "HGAppDelegate.h"
#import "UIStoryboard+Pinnacle.h"
#import "UserReportTableViewController.h"


@interface HGUserViewController () <CHTCollectionViewDelegateWaterfallLayout,UIScrollViewDelegate>

@property (nonatomic, assign) BOOL detailLoaded;
@property (nonatomic, assign) BOOL followed;
@property (nonatomic, assign) BOOL isMe;
@property (nonatomic, assign) int itemsCount;
@property (nonatomic, assign) int followersCount;
@property (nonatomic, assign) int followingCount;



@property (nonatomic, assign) int reviewCount;
@property (nonatomic ,assign) float reviewScore;


@property (nonatomic, assign) BOOL bBlockedUser;

@property (nonatomic, strong) NSMutableArray * sellingItems;
@property (nonatomic, strong) NSString * nextLink;
@property (nonatomic, strong) UIView* UserRatingView;
@property (nonatomic, strong) UIImageView* ratingScoreImage;
@property (nonatomic, strong) UILabel* ReviewTitle;
@property (nonatomic, strong) UILabel* counterRatings;

@property (nonatomic, strong) HGUserVerifyHintView *userVerifyInfoView;
@property (nonatomic, strong) UIButton *btnVerifyInfoMask;

@property (nonatomic, strong) HGUserSummaryView *userSummaryView;

@property (nonatomic, strong) UIView* emptyView;

@property (nonatomic, strong) CMPopTipView *popTipReviewStar;

@property (nonatomic, assign) float extraXoffset_Iphone6;

@end

#define HEADER_IDENTIFIER @"UserHeader"
#define TAG_ACTION_SHEET_OPTIONS 1000

@implementation HGUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


static int layoutHeadHeight = 0;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.extraXoffset_Iphone6 = 0;
    if (SCREEN_WIDTH==320) {
        //iphone4.5
        self.extraXoffset_Iphone6 = 0;
    }
    else{
        //iphone6; UI improve
        self.extraXoffset_Iphone6 = 20;
    }

    TouchAction_simpleLock = 0;
    
    // Do any additional setup after loading the view.
    self.navigationItem.title = self.user.displayName;
    UIImageView *ivOptions = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seller_profile_options"]];
    UITapGestureRecognizer *tapGestureOptions = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapOptions:)];
    [ivOptions addGestureRecognizer:tapGestureOptions];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:ivOptions];
    
    self.sellingItems = [@[] mutableCopy];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    //self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);

    // Custom collection view layout
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    if([self.user.uid isEqualToString:[HGUserData sharedInstance].fmUserID])
    {
        //it is user self
        layout.headerHeight = 230 - 60;
    }
    else{
        layout.headerHeight = 230;
    }
    
    if(![HGAppData sharedInstance].bOpenReviewFunctions)
    {
        layout.headerHeight = layout.headerHeight - 40 -10;
    }
    layoutHeadHeight = layout.headerHeight;
    
    layout.sectionInset = UIEdgeInsetsMake(10, 5, 5,4 );
    layout.minimumColumnSpacing = 5;
    layout.minimumInteritemSpacing = 7.5;
    self.collectionView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    UINib *headNib = [UINib nibWithNibName:@"HGUserSummaryView" bundle:nil];
    [self.collectionView registerNib:headNib forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:HEADER_IDENTIFIER];
    self.collectionView.collectionViewLayout = layout;
    
    [self addItemEmptyView];
    [self getUserDetail];
    
    [self customizeBackButton];
    [self addUserVerifyInfoView];
   
}


-(void)getUserDetail
{
    if (self.user.uid==nil || self.user.uid.length==0) {
        return;
    }
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"user_detail/" parameters:@{@"user_id": self.user.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        self.detailLoaded = YES;
        self.followed = [[responseObject objectForKey:@"followed"] boolValue];
        
        if ([self.user.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
            self.isMe = YES;
        }
        else{
            self.isMe = NO;
        }
        self.navigationItem.title = [responseObject objectForKey:@"nickname"];
        self.user.portraitLink = [responseObject objectForKey:@"portrait"];
        //[[responseObject objectForKey:@"isMe"] boolValue];
        self.itemsCount = [[responseObject objectForKey:@"count_sellings"] intValue];
        self.followersCount = [[responseObject objectForKey:@"count_followers"] intValue];
        self.followingCount = [[responseObject objectForKey:@"count_following"] intValue];

        self.reviewCount = [[responseObject objectForKey:@"review_num"] intValue];
        self.reviewScore = [[responseObject objectForKey:@"review_score"] floatValue];
        
        self.bBlockedUser = [[responseObject objectForKey:@"user_blocked"] boolValue];
        [self updateRatingDisplay:self.reviewScore withReviewCount:self.reviewCount];
        
        [self configVerifyInfo:responseObject];
        [self configUserLocation:responseObject];
        
      
        [self.collectionView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get user detail error: %@", error);
    }];
    
    __weak HGUserViewController * weakSelf = self;
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"user_sellings/" parameters:@{@"seller_id": self.user.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        
        [self.sellingItems removeAllObjects];
        for (id dictObj in results) {
            NSError * error = nil;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                [self.sellingItems addObject:item];
            } else {
                NSLog(@"shop item creation error: %@", error);
            }
        }
        if (![self.nextLink isEqual:[NSNull null]]) {
            [self.collectionView addInfiniteScrollingWithActionHandler:^{
                [weakSelf _loadNextPage];
            }];
        }
        [self.collectionView reloadData];
        if (self.sellingItems.count == 0) {
            self.emptyView.hidden = NO;
            
        }
        else{
            self.emptyView.hidden = YES;
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get user selling listings error: %@", error);
    }];
}


-(void)configVerifyInfo:(NSDictionary*)responseObject
{
    /////avatar/////////////
    NSNumber* markValueAvatar = [responseObject objectForKey:@"verified"];
    if (markValueAvatar == nil || markValueAvatar.integerValue == 0) {
        self.user.verified = [[NSNumber alloc] initWithInt:0];
    }
    else{
        self.user.verified = [[NSNumber alloc] initWithInt:1];
    }
    
    /////email////////////////
    NSNumber* markValueEmail = [responseObject objectForKey:@"email_verified"];
    if (markValueEmail == nil || markValueEmail.integerValue == 0) {
        self.user.email_verified = [[NSNumber alloc] initWithInt:0];
      }
    else{
        self.user.email_verified = [[NSNumber alloc] initWithInt:1];

    }
    ////facebook/////////////////////
    NSNumber* markValueFacebook = [responseObject objectForKey:@"facebook_verified"];
    if (markValueFacebook == nil || markValueFacebook.integerValue == 0) {
        self.user.facebook_verified = [[NSNumber alloc] initWithInt:0];
    }
    else{
        self.user.facebook_verified = [[NSNumber alloc] initWithInt:1];

    }
    
    /////////phone///////////////////
    NSNumber* markValuePhone = [responseObject objectForKey:@"mobile_phone_verified"];
    if (markValuePhone == nil || markValuePhone.integerValue == 0) {
        self.user.mobile_phone_verified = [[NSNumber alloc] initWithInt:0];
    }else{
        self.user.mobile_phone_verified = [[NSNumber alloc] initWithInt:1];
    }
    
}
-(void)configUserLocation:(NSDictionary*)responseObject
{
    if ([responseObject objectForKey:@"country"] != nil) {
        self.user.country = [responseObject objectForKey:@"country"];
    }
    if ([responseObject objectForKey:@"city"] != nil) {
        self.user.city = [responseObject objectForKey:@"city"];
    }
    if ([responseObject objectForKey:@"region"] != nil) {
        self.user.region = [responseObject objectForKey:@"region"];
    }
    
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"sellerProfile_view"];
    HGAppDelegate* appDelegate = (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.tabController.tabBar.hidden =YES;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([HGUserData sharedInstance].bOtherUserViewShowRatingInstruction && [HGAppData sharedInstance].bOpenReviewFunctions){
    
        [HGUserData sharedInstance].bOtherUserViewShowRatingInstruction = NO;
        self.popTipReviewStar = [[HGUtils sharedInstance] showTipsWithMessage:@"Reviews are from both buyers and sellers. Give a review now in the Chat view." atView:self.ratingScoreImage inView:self.view animated:NO withOffset:CGPointMake(5, 5) withSize:CGSizeMake(166, 100) withDirection:PointDirectionUp];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.popTipReviewStar dismissAnimated:NO];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowSingleItem"]) {
        HGItemDetailController * vcDest = segue.destinationViewController;
        vcDest.item = sender;
    } else if ([segue.identifier isEqualToString:@"ShowSellerRatings"]) {
        HGRatingsController * vcDest = segue.destinationViewController;
        vcDest.uid = self.user.uid;
    }
    
}
-(void)addUserVerifyInfoView
{
   
    self.userVerifyInfoView = [[HGUserVerifyHintView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.userSummaryView.user_verifyInfoButton.frame)-201 ,65, (452+20)/2, (130+74*2+90)/2)];
    self.btnVerifyInfoMask = [[UIButton alloc] initWithFrame:self.view.frame];
    self.btnVerifyInfoMask.layer.opaque = YES;
    self.btnVerifyInfoMask.hidden = YES;
    [self.btnVerifyInfoMask addTarget:self action:@selector(hideUserVerifyInfoView) forControlEvents:UIControlEventTouchUpInside];
    [self.btnVerifyInfoMask addSubview:self.userVerifyInfoView];
    
    [self.view addSubview:self.btnVerifyInfoMask];
    

}

-(void)viewDidLayoutSubviews
{
    
    UIView* infoButtonView= self.userSummaryView.user_verifyInfoButton;
    CGPoint pointInWindow = [infoButtonView.superview convertPoint:infoButtonView.center toView:self.view];
    
    
    
    self.userVerifyInfoView.frame = CGRectMake(CGRectGetMinX(self.userSummaryView.user_verifyInfoButton.frame)-201 , pointInWindow.y+5, (452+20)/2, (130+74*2+90)/2);
}

#pragma mark - CollectionView Datasource & Delegate

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader])
    {
        HGUserSummaryView * view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"UserHeader" forIndexPath:indexPath];
       
        view.delegate = self;
        view.backgroundColor = [UIColor whiteColor];
        [view configWithEntity:self.user];
        
       
        
        
        view.avatarView.userInteractionEnabled = YES;
        UITapGestureRecognizer * tapGesture_onAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUserViewTouchAuthorAvatar:)];
        
        [view.avatarView addGestureRecognizer:tapGesture_onAvatar];
        
        [view.btnItemNum setTitle:[[HGUtils sharedInstance] formatInteger:self.itemsCount] forState:UIControlStateNormal];
        [view.btnFollowerNum setTitle:[[HGUtils sharedInstance] formatInteger:self.followersCount] forState:UIControlStateNormal];
        [view.btnFollowingNum setTitle:[[HGUtils sharedInstance] formatInteger:self.followingCount] forState:UIControlStateNormal];
        if (self.detailLoaded) {
            view.btnAction.displayType = self.followed ? HGFollowButtonTypeFollowing : HGFollowButtonTypeNotFollowing;
            view.btnAction.hidden = self.isMe;
        }
        view.itemNumUnit.text = NSLocalizedString(@"Listings", nil);
        view.followerUnit.text = NSLocalizedString(@"Followers", nil);
        view.followingUnit.text = NSLocalizedString(@"Following", nil);
        [view.btnFollowerNum addTarget:self action:@selector(onTouchFollowerButton:) forControlEvents:UIControlEventTouchUpInside];
        [view.btnFollowingNum addTarget:self action:@selector(onTouchFollowingButton:) forControlEvents:UIControlEventTouchUpInside];
        [view.btnAction addTarget:self action:@selector(onTouchFollowButton:) forControlEvents:UIControlEventTouchUpInside];
        self.userSummaryView = view;
        
        if([HGAppData sharedInstance].bOpenReviewFunctions)
        {
            [self addRatingView:view];
        }
        
        
        
        return view;

    }
    return nil;
    
}
-(void)addRatingView:(UIView*)view
{
    if([self.user.uid isEqualToString:[HGUserData sharedInstance].fmUserID])
    {
        self.UserRatingView = [[UIView alloc] initWithFrame:CGRectMake(0, 185-60, view.frame.size.width, 40)];
    }
    else
    {
        self.UserRatingView = [[UIView alloc] initWithFrame:CGRectMake(0, 185, view.frame.size.width, 40)];
    }
    
    
    
    self.UserRatingView.backgroundColor = [UIColor whiteColor];
    UIImageView * iconStar = [[UIImageView alloc] initWithFrame:CGRectMake(15, 11, 36/2, 36/2)];
    iconStar.image = [UIImage imageNamed:@"icon-star"];
    [self.UserRatingView addSubview:iconStar];
    
    self.ReviewTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(iconStar.frame) + 10, 11, 240, 30)];
    self.ReviewTitle.textColor= SYSTEM_DEFAULT_FONT_COLOR_1;
    self.ReviewTitle.font = [UIFont systemFontOfSize:15];
    self.ReviewTitle.text = NSLocalizedString(@"Reviews", nil);
    
    [self.UserRatingView addSubview:self.ReviewTitle];
    
    
    [self.ReviewTitle sizeToFit];
    
    
    
    self.ratingScoreImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-80-144/2, 15, 144/2, 20/2)];
    
   
    [self.UserRatingView addSubview:self.ratingScoreImage];
    
    
    self.counterRatings = [[UILabel alloc] initWithFrame: CGRectMake(CGRectGetMaxX(self.ratingScoreImage.frame)+5, 5, 35, 30)];
    self.counterRatings.font = [UIFont systemFontOfSize:15.0];
    self.counterRatings.textAlignment = NSTextAlignmentRight;
    
    [self.UserRatingView addSubview:self.counterRatings];
    [self updateRatingDisplay: self.reviewScore withReviewCount:self.reviewCount];
    
    self.UserRatingView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTouchUserRatingView)];
    [self.UserRatingView addGestureRecognizer:tapGesture];
    [view addSubview:self.UserRatingView];
    
    UIImageView*accView = [[UIImageView alloc] initWithFrame:CGRectMake( self.view.frame.size.width-12-17/2, 12.5, 17/2, 30/2)];
    accView.image = [UIImage imageNamed:@"uitableview_cell_access"];
    
    [self.UserRatingView addSubview:accView];
    
}

-(void)updateRatingDisplay:(float)ReviewScore withReviewCount:(int)count
{
    
    NSString* image_index = [[HGUtils sharedInstance] getReviewImageNameByScore:ReviewScore];
    
    self.ratingScoreImage.image = [UIImage imageNamed:image_index];
    
    self.counterRatings.text = [@"" stringByAppendingFormat:@"%d", count];
}
-(void)showUserVerifyInfoView
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"itsverifyinfoicon" label:nil value:nil];
    self.btnVerifyInfoMask.hidden = NO;
}
-(void)hideUserVerifyInfoView
{
    self.btnVerifyInfoMask.hidden = YES;
}
#pragma mark - Tap Gesture Handler

- (void)onUserViewTouchAuthorAvatar:(UITapGestureRecognizer *)gesture
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"itsphoto" label:nil value:nil];
}


-(void)onTouchUserRatingView
{
     //[self performSegueWithIdentifier:@"ShowSellerRatings" sender:self];
   
    
    
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"seller_review" label:nil value:nil];
    HGRatingsController *ratingControlelr = (HGRatingsController *)[[UIStoryboard profileStoryboard]instantiateViewControllerWithIdentifier:@"SellerRatings"];
   
    ratingControlelr.uid = self.user.uid;
    
    [self.navigationController pushViewController:ratingControlelr animated:YES];
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return self.sellingItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGHomeItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SellItemCell" forIndexPath:indexPath];
    
    HGShopItem * item = [self.sellingItems objectAtIndex:indexPath.row];
    [cell configWithItem:item];
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"itsitem" label:nil value:nil];
    HGShopItem * item = [self.sellingItems objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ShowSingleItem" sender:item];
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.popTipReviewStar dismissAnimated:NO];
    self.popTipReviewStar = nil;
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    HGShopItem * item = [self.sellingItems objectAtIndex:indexPath.row];
    HGItemImage * coverImage = [item.images firstObject];
    CGFloat wd =  CGRectGetWidth(self.view.frame) * 0.5 - 20;
    CGSize cellSize = CGSizeMake(wd, ceil(coverImage.height * wd * 1.0/ coverImage.width) + WATER_FLOW_CELL_EXTEND_HEIGHT);
    
    return cellSize;
}

#pragma mark - UI Control Actions

- (IBAction)onTouchFollowerButton:(id)sender {
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"itsfollowers" label:nil value:nil];
    
    HGFollowersViewController *vcFollowers = (HGFollowersViewController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCFollowers"];
    vcFollowers.userID = self.user.uid;
    
    [[HGUtils sharedInstance] gaTrackViewName:@"sellerfollowers_view"];
    [self.navigationController pushViewController:vcFollowers animated:YES];
    
}

- (IBAction)onTouchFollowingButton:(id)sender {
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"itsfollowing" label:nil value:nil];
    
    HGFollowingViewController *vcFollowing = (HGFollowingViewController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCFollowing"];
    vcFollowing.userID = self.user.uid;
    
    [[HGUtils sharedInstance] gaTrackViewName:@"sellerfollowing_view"];
    [self.navigationController pushViewController:vcFollowing animated:YES];
}

static int TouchAction_simpleLock = 0;
- (IBAction)onTouchFollowButton:(id)sender {
    
    if( [[HGUserData sharedInstance] isLoggedIn]){
        [self startFollowAction];
    }else{
        if (self.followed)
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"unfollow" label:nil value:nil];
            
        }
        else
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"follow" label:nil value:nil];
            
        }
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"follow_login" label:nil value:nil];
        
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startFollowAction)];
    }
    
}

- (void)startFollowAction{
    if (TouchAction_simpleLock == 1) {
        return;
    }
    
    if (self.followed) {
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"unfollow" label:nil value:nil];
        UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:
                                      NSLocalizedString(@"Unfollow", nil),
                                      nil];
        actionSheet.tag = 0;
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showInView:self.view];
        
        
    } else {
        
        
        TouchAction_simpleLock = 1;
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"follow/" parameters:@{@"user_id": self.user.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"follow" label:nil value:nil];
            self.followed = YES;
            self.userSummaryView.btnAction.displayType = HGFollowButtonTypeFollowing;
            self.followersCount += 1;
            [self.userSummaryView.btnFollowerNum setTitle:[NSString stringWithFormat:@"%d", self.followersCount] forState:UIControlStateNormal];
            TouchAction_simpleLock = 0;
            //[self alertInformation:@"follow operation success."];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            TouchAction_simpleLock = 0;
            NSLog(@"follow failed: %@", error.localizedDescription);
            //[[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"operation fail , try again later",nil)];
        }];
    }
    

}

-(void)startUnfollowAction
{
    TouchAction_simpleLock = 1;
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unfollow/" parameters:@{@"user_id": self.user.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        self.followed = NO;
        self.userSummaryView.btnAction.displayType = HGFollowButtonTypeNotFollowing;
        self.followersCount -= 1;
        [self.userSummaryView.btnFollowerNum setTitle:[NSString stringWithFormat:@"%d", self.followersCount] forState:UIControlStateNormal];
        TouchAction_simpleLock = 0;
        //[self alertInformation:@"unfollow operation success."];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        TouchAction_simpleLock = 0;
        //[[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"operation fail , try again later",nil)];
        NSLog(@"unfollow failed: %@", error.localizedDescription);
    }];
}
#pragma mark - UIActionsheet Delegate
- (void)actionSheetCancel:(UIActionSheet *)actionSheet{
  
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if (actionSheet.tag == 0)
        {
            switch (buttonIndex) {
                case 0:
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"unfollow_yes" label:nil value:nil];
                    [self startUnfollowAction];
                    break;
                default:
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"unfollow_no" label:nil value:nil];
                    break;
            }
        }else if(actionSheet.tag == TAG_ACTION_SHEET_OPTIONS){
            switch (buttonIndex) {
                case 0:
                {
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"seller_reportseller" label:nil value:nil];

                    UserReportTableViewController *vcUserReport = [[UserReportTableViewController alloc] initWithNibName:@"UserReportTableViewController" bundle:nil];
                    vcUserReport.reportType = ReportReasonTypeSeller;
                    vcUserReport.userID = self.user.uid;
                    [self.navigationController pushViewController:vcUserReport animated:YES];
                    break;
                }

                case 1:
                {
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:blockActionGAstring label:nil value:nil];
                    if( self.bBlockedUser){
                        [self unblockUser];
                    }else{
                        [self blockUser];
                    }
                    break;
                }
                default:
                {
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"sellermore_cancel" label:nil value:nil];
                    break;
                }
            }
        }
}
#pragma mark - Helpers 

- (void)blockUser{
    
    NSString* prefixString = NSLocalizedString(@"Are you sure you want to block them?", nil);
    NSString* CompleteString = NSLocalizedString(@"Are you sure you want to block them?\nThis 5miler will no longer be able to connect with you.",nil);
    NSRange  range = [CompleteString rangeOfString:prefixString];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:CompleteString];
    if (range.length > 0 ) {
        UIFont* font = [UIFont boldSystemFontOfSize:15];
        [attrString addAttribute:NSFontAttributeName value:font range:range];
    }
    
    PXAlertView *alert = [PXAlertView showAlertWithTitle:nil messageWithAttribute:attrString cancelTitle:NSLocalizedString(@"Cancel", nil) otherTitle:NSLocalizedString(@"Confirm", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
        
        if(!cancelled){
            [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"block/" parameters:@{@"target_user":self.user.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
                self.bBlockedUser = YES;
                [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"You have successfully blocked them.", nil) onView:self.view];
                 [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"seller_blockyes" label:nil value:nil];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {}];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"seller_blockno" label:nil value:nil];
        }
    }];
    
    [alert useDefaultIOS7Style];
}


- (void)unblockUser{
    
    NSString* prefixString = NSLocalizedString(@"Are you sure you want to unblock them?", nil);
    NSString* CompleteString =NSLocalizedString(@"Are you sure you want to unblock them?\nThis 5miler will now be able to connect with you.",nil);
    NSRange  range = [CompleteString rangeOfString:prefixString];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:CompleteString];
    if (range.length > 0 ) {
        UIFont* font = [UIFont boldSystemFontOfSize:15];
        [attrString addAttribute:NSFontAttributeName value:font range:range];
    }
    
    PXAlertView *alert = [PXAlertView showAlertWithTitle:nil messageWithAttribute:attrString cancelTitle:NSLocalizedString(@"Cancel", nil) otherTitle:NSLocalizedString(@"Confirm", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
        
        if(!cancelled){
            [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unblock/" parameters:@{@"target_user":self.user.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
                self.bBlockedUser = NO;
                [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"You have successfully unblocked them.", nil) onView:self.view];
                 [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"seller_unblockyes" label:nil value:nil];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {}];
        }
        else{
             [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"seller_unblockno" label:nil value:nil];
        }
    }];
    
    [alert useDefaultIOS7Style];
}

- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.collectionView.infiniteScrollingView stopAnimating];
    } else {
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[NSString stringWithFormat:@"user_sellings/%@", self.nextLink] parameters:@{@"seller_id": self.user.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.sellingItems addObject:item];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            [self.collectionView reloadData];
            [self.collectionView.infiniteScrollingView stopAnimating];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self.collectionView.infiniteScrollingView stopAnimating];
            NSLog(@"get user selling listings failed: %@", [error.userInfo objectForKey:@"body"]);
        }];
    }
}

static NSString* blockActionGAstring = @"seller_block";
- (void)onTapOptions:(UITapGestureRecognizer *)tapGesture{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"sellermore" label:nil value:nil];

    
    NSString *blockTitle = self.bBlockedUser ? NSLocalizedString(@"Unblock this Person",nil):NSLocalizedString(@"Block this Person",nil);
    if (self.bBlockedUser) {
        blockActionGAstring = @"seller_unblock";
    }
    
    UIActionSheet *asOptions = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Report this Person", nil), blockTitle, nil];
    
    asOptions.tag = TAG_ACTION_SHEET_OPTIONS;
    [asOptions showInView:self.view];
}




-(void)customizeBackButton
{
    
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
    
   
    
}
-(void)addItemEmptyView
{
    self.emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, layoutHeadHeight+60, self.view.frame.size.width, 400)];
    [self.view addSubview:self.emptyView];
    self.emptyView.hidden = YES;
    //self.emptyView.backgroundColor =[UIColor redColor];
    
    int iconYoffset = 80;
    int lineOffset = 10;
    
    self.emptyView.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];
    
    int iphone4_Y_offset_config_Value = 0;
    if (SCREEN_HEIGHT <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    CGPoint iconCenter  = CGPointMake(SCREEN_WIDTH/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [self.emptyView addSubview:icon];
    
    
    CGPoint centerPoint = CGPointMake(SCREEN_WIDTH/2, CGRectGetMaxY(icon.frame)+lineOffset + 30/2);
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 20)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 1;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"No listings right now", nil);
    textLabel.font = [UIFont systemFontOfSize:13];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [self.emptyView addSubview:textLabel];
    //[textLabel sizeToFit];
    
}
@end

//
//  HGMessage.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-4.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "JSONModel.h"

@class HGUser, HGItemImage;

@interface HGMessage : JSONModel

@property (nonatomic, strong) NSString * uid; //message id
@property (nonatomic, strong) HGUser * from;
@property (nonatomic, strong) HGUser * to;
@property (nonatomic, strong) NSString * itemID;
@property (nonatomic, strong) HGItemImage * itemImage;
@property (nonatomic, strong) NSString * threadID;  //offer line id
@property (nonatomic, strong) NSString * badgeCount;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, assign) BOOL unread;
@property (nonatomic, assign) NSTimeInterval timestamp;
@property (nonatomic, strong) NSString* item_title;
@property (nonatomic, strong) NSString* owner_nickname;


- (NSString *)prettyDate;
- (BOOL)fromMe;

@end

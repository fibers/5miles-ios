//
//  HGZipCodeVC.h
//  Pinnacle
//
//  Created by Alex on 15/5/12.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
   zipViewTypeUserRegister = 1,
   zipViewTypeEditProfile = 2,
} zipViewType;
@interface HGZipCodeVC : UIViewController<UITextFieldDelegate, UINavigationControllerDelegate>


@property (nonatomic, assign)zipViewType currentViewType;
@end

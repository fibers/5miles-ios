//
//  HGSearchViewController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-19.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSearchViewController.h"
#import "HGAppData.h"
#import "HGCategoryCell.h"
#import "HGSearchResultsController.h"
#import "HGUtils.h"
#import "HGAppDelegate.h"
#import <Crashlytics/Crashlytics.h>
#import "HGCategory.h"
#import "HGAppInitData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+pureColorImage.h"
@interface HGSearchViewController () <UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) NSMutableDictionary* level2CategoryDict;
@property (nonatomic, strong) NSMutableArray* level1CategoryArray;
@property (nonatomic, strong) NSString* currentSelectedCategoryLevel1Title;



@property (nonatomic, strong) NSMutableArray* leftCategoryButtons;

@property (nonatomic, assign) int currentSelectCateogrylevel1Index;
@property (nonatomic, strong) NSMutableArray* currentSelectedCategories;
@end

@implementation HGSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(void)initCategoryfromLocal
{
    if ([HGAppInitData sharedInstance].categoryArray_c1) {
        [self.level1CategoryArray removeAllObjects];
        [self.level1CategoryArray addObjectsFromArray:[HGAppInitData sharedInstance].categoryArray_c1];
        for (int i= 0; i<self.level1CategoryArray.count; i++) {
            HGCategory* c = [self.level1CategoryArray objectAtIndex:i];
            
            if (i==0) {
                self.currentSelectedCategoryLevel1Title = c.title;
            }
            [self.level2CategoryDict setObject:c.subCategories forKey:c.title];
        }
        //[self.myCategories reloadData];
    }
}
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
static int leftSearchTypeButtonWidth = 80;
static int leftSearchTypeButtonHeight = 89;


static int btn_right_triangle = 9997;
static int btn_icon_tag = 9998;
static int btn_label_tag = 9999;
-(void)addLeftSearchTypeButton
{
   
    
    UIScrollView* leftTypeBoard = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, leftSearchTypeButtonWidth, self.view.frame.size.height)];
    
    UIView* rightLine = [[UIView alloc] initWithFrame:CGRectMake(leftTypeBoard.frame.size.width -1, 0, 1, leftTypeBoard.frame.size.height)];
    rightLine.backgroundColor = FANCY_COLOR(@"c8c8c8");
    [leftTypeBoard addSubview:rightLine];
    
    [self.view addSubview:leftTypeBoard];
    leftTypeBoard.backgroundColor = FANCY_COLOR(@"f4f4f4");
    
    leftTypeBoard.contentSize = CGSizeMake(leftSearchTypeButtonWidth, leftSearchTypeButtonHeight* [HGAppInitData sharedInstance].categoryArray_c1.count + 100);
    for (int i = 0; i<[HGAppInitData sharedInstance].categoryArray_c1.count; i++) {        HGCategory* category = [[HGAppInitData sharedInstance].categoryArray_c1 objectAtIndex:i];
        
        UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0, leftSearchTypeButtonHeight*i, leftSearchTypeButtonWidth, leftSearchTypeButtonHeight)];
        btn.tag = i;
        [leftTypeBoard addSubview:btn];
        UIImageView* iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        iconView.center = CGPointMake(leftSearchTypeButtonWidth/2, leftSearchTypeButtonHeight/2-5);
        iconView.tag = btn_icon_tag;
        [btn addSubview:iconView];
        
        
        NSString* localIconName = [[HGAppInitData sharedInstance].category_level1_localIcon objectAtIndex:i%[HGAppInitData sharedInstance].category_level1_localIcon.count];
        
        [iconView sd_setImageWithURL:[NSURL URLWithString:category.icon] placeholderImage:[UIImage imageNamed:localIconName]];
        
        UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
        title.numberOfLines = 1;
        title.font = [UIFont fontWithName:@"Museo-300" size:11.0f];
        title.textColor  = FANCY_COLOR(@"656565");
        title.text = [category.title uppercaseString];
        title.textAlignment = NSTextAlignmentCenter;
        CGPoint center = iconView.center;
        center.y = center.y + 25;
        title.center = center;
        title.tag = btn_label_tag;
        [btn addSubview:title];
        
        btn.backgroundColor = FANCY_COLOR(@"f4f4f4");
        UIView* leftColor = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2, btn.frame.size.height)];
        
        leftColor.backgroundColor = FANCY_COLOR(@"ff8830");
        
        UIView* bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, btn.frame.size.height-1, btn.frame.size.width, 1)];
        bottomLine.backgroundColor = FANCY_COLOR(@"c8c8c8");
        [btn addSubview:bottomLine];
        [btn addSubview:leftColor];
        
        UIView* rightLine = [[UIView alloc] initWithFrame:CGRectMake(btn.frame.size.width -1, 0, 1, btn.frame.size.height)];
        rightLine.backgroundColor = FANCY_COLOR(@"c8c8c8");
        [btn addSubview:rightLine];
        
        
        UIImageView* rightTriangle = [[UIImageView alloc] initWithFrame:CGRectMake(leftSearchTypeButtonWidth-16/2, leftSearchTypeButtonHeight/2-19/4, 16/2, 19/2)];
        rightTriangle.image = [UIImage imageNamed:@"searchRightCategoryTriangle"];
        [btn addSubview:rightTriangle];
        rightTriangle.tag = btn_right_triangle;
        rightTriangle.hidden = YES;
        
        [btn addTarget:self action:@selector(onTouchSearhMenuChange:) forControlEvents:UIControlEventTouchUpInside];
        [self.leftCategoryButtons addObject:btn];
        
        
        [self.currentSelectedCategories addObject:[[NSNumber alloc] initWithInt:-1]];
    }
    
    if(self.leftCategoryButtons.count > 0)
    {
        [self onTouchSearhMenuChange:[self.leftCategoryButtons objectAtIndex:0]];

    }

}

-(void)resetLeftCateogryButton
{
    for (int i =0; i<self.leftCategoryButtons.count; i++) {
        UIButton* btn = [self.leftCategoryButtons objectAtIndex:i];
        btn.backgroundColor = FANCY_COLOR(@"f4f4f4");
        
        if (self.level1CategoryArray && i < self.level1CategoryArray.count) {
            HGCategory* c =[self.level1CategoryArray objectAtIndex:i];
            
            UIImageView* iconView = (UIImageView*)[btn viewWithTag:btn_icon_tag];
            if ([iconView isKindOfClass:[UIImageView class]]) {
                NSString* localIconName = [[HGAppInitData sharedInstance].category_level1_localIcon objectAtIndex:i%[HGAppInitData sharedInstance].category_level1_localIcon.count];
                
                [iconView sd_setImageWithURL:[NSURL URLWithString:c.icon] placeholderImage:[UIImage imageNamed:localIconName]];
                
            }

        }
        UILabel* title = (UILabel*)[btn viewWithTag:btn_label_tag];
        if ([title isKindOfClass:[UILabel class]]) {
            title.textColor = FANCY_COLOR(@"656565");
        }
        UIImageView* trangle = (UIImageView*)[btn viewWithTag:btn_right_triangle];
        if ([trangle isKindOfClass:[UIImageView class]]) {
            trangle.hidden = YES;
        }
    }
}
-(void)onTouchSearhMenuChange:(id)sender
{
    [self resetLeftCateogryButton];
    UIButton * btn = (UIButton*)sender;
    int index = (int)btn.tag;
    self.currentSelectCateogrylevel1Index = index;
    if (index >= self.level1CategoryArray.count) {
        return;
    }
    
    HGCategory* c =[self.level1CategoryArray objectAtIndex:index];
    
    btn.backgroundColor = FANCY_COLOR(@"ff8830");
    
    UIImageView* iconView = (UIImageView*)[btn viewWithTag:btn_icon_tag];
    if ([iconView isKindOfClass:[UIImageView class]]) {
        NSString* localIconName = [[HGAppInitData sharedInstance].category_level1_localIcon objectAtIndex:index%[HGAppInitData sharedInstance].category_level1_localIcon.count];
        localIconName = [localIconName stringByAppendingString:@"_psd"];
        
        [iconView sd_setImageWithURL:[NSURL URLWithString:c.hover_icon] placeholderImage:[UIImage imageNamed:localIconName]];

    }
    
    UILabel* title = (UILabel*)[btn viewWithTag:btn_label_tag];
    if ([title isKindOfClass:[UILabel class]]) {
        title.textColor = FANCY_COLOR(@"ffffff");
    }
    
    UIImageView* trangle = (UIImageView*)[btn viewWithTag:btn_right_triangle];
    if ([trangle isKindOfClass:[UIImageView class]]) {
        trangle.hidden = NO;
    }
    
    self.currentSelectedCategoryLevel1Title = c.title;
    self.myCategories.dataSource =self;
    self.myCategories.delegate = self;
    [self.myCategories reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.level2CategoryDict = [[NSMutableDictionary alloc] init];
    self.level1CategoryArray = [[NSMutableArray alloc] init];
    self.leftCategoryButtons = [[NSMutableArray alloc] init];
    self.currentSelectedCategories = [[NSMutableArray alloc] init];
    
    
    
    [self initCategoryfromLocal];
    [self addLeftSearchTypeButton];
    
    [self customSearchBar];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
   
    self.navigationController.delegate = self;
    [self customizeBackButton];

    self.myCategories.frame = CGRectMake(self.myCategories.frame.origin.x + 80, self.myCategories.frame.origin.y, self.myCategories.frame.size.width, self.myCategories.frame.size.height);
}

-(void)customSearchBar
{
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(5, 0, 280, 35)];;
    
    self.searchBar.barStyle = UIBarStyleDefault;
    self.searchBar.placeholder = NSLocalizedString(@"Search listings in all categories", nil);
    self.searchBar.delegate = self;
    //self.searchBar.barTintColor = FANCY_COLOR(@"f0f0f0");
    for (UIView *subView in self.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                searchBarTextField.backgroundColor = FANCY_COLOR(@"ededed");
            }
        }
    }
    
    
    self.searchDisplayController.searchResultsTableView.showsHorizontalScrollIndicator = NO;
    self.navigationItem.titleView = self.searchBar;

}
-(void)customizeBackButton
{
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    self.navigationItem.backBarButtonItem = backbutton;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
     self.searchBar.text = @"";
    if ([HGAppData sharedInstance].jumpToSearchFromHomeRightButton) {
        [HGAppData sharedInstance].jumpToSearchFromHomeRightButton = NO;
        [self.searchBar becomeFirstResponder];
    }
    else
    {
        self.searchBar.showsCancelButton = NO;
        [self.searchBar resignFirstResponder];
    }
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
    [[HGUtils sharedInstance] gaTrackViewName:@"discover_view"];
  
}
-(void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionView Datasource & Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    NSArray* categories = [self.level2CategoryDict objectForKey:self.currentSelectedCategoryLevel1Title];
    return categories.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGCategoryCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCell" forIndexPath:indexPath];
    
    NSArray* categories = [self.level2CategoryDict objectForKey:self.currentSelectedCategoryLevel1Title];
    HGCategory* category = [categories objectAtIndex:indexPath.row];
    
    
    NSNumber* currentSelectedIndex = [self.currentSelectedCategories objectAtIndex:self.currentSelectCateogrylevel1Index];
    if (currentSelectedIndex.intValue == indexPath.row) {
        
        cell.lbTitle.textColor = FANCY_COLOR(@"242424");
        //高亮上次选中的分类
        //cell.lbTitle.textColor = FANCY_COLOR(@"ff8830");
    }
    else
    {
        cell.lbTitle.textColor = FANCY_COLOR(@"242424");
    }

    
    [cell configCategoryCell:category];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
      [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"category" label:nil value:nil];
     NSArray* categories = [self.level2CategoryDict objectForKey:self.currentSelectedCategoryLevel1Title];
    HGCategory* category = [categories objectAtIndex:indexPath.row];
    
    [self.searchBar resignFirstResponder];
    
    [self.currentSelectedCategories replaceObjectAtIndex:self.currentSelectCateogrylevel1Index withObject:[[NSNumber alloc] initWithInt:(int)indexPath.row]];
    
    //for GA
    {
        
        switch (self.currentSelectCateogrylevel1Index) {
            case 0:
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"forsale_category" label:nil value:nil];
                break;
            case 1:
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"services_category" label:nil value:nil];
                break;
            case 2:
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"housing_category" label:nil value:nil];
                break;
            case 3:
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"jobs_category" label:nil value:nil];
                break;
            default:
                break;
        }
    }
    //end of GA
    
    [self.myCategories reloadData];
    [self performSegueWithIdentifier:@"ShowCategorySearchResults" sender:@{@"catID": [NSNumber numberWithInteger:category.index], @"catTitle": category.title}];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //52 is the default collection cell height;
    return CGSizeMake(self.view.frame.size.width , 45);
}
#pragma mark - SearchBar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"search_start" label:nil value:nil];

    searchBar.showsCancelButton = YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"search_action" label:nil value:nil];
    NSString * keyword = searchBar.text;
    [searchBar resignFirstResponder];
    [self performSegueWithIdentifier:@"ShowKeywordSearchResult" sender:@{@"keyword": keyword}];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"search_cancel" label:nil value:nil];
    searchBar.text = @"";
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowCategorySearchResults"]) {
        HGSearchResultsController * vcDest = segue.destinationViewController;
        vcDest.title = [sender objectForKey:@"catTitle"];
        vcDest.rf_tag_from_push_payload = [sender objectForKey:@"rf_tag"];
        vcDest.searchMode = HGSearchModeCategory;
        vcDest.searchPayload = sender;
    } else if ([segue.identifier isEqualToString:@"ShowKeywordSearchResult"]) {
        HGSearchResultsController * vcDest = segue.destinationViewController;
        
        vcDest.rf_tag_from_push_payload = [sender objectForKey:@"rf_tag"];
        vcDest.searchMode = HGSearchModeKeyword;
        vcDest.searchPayload = sender;
        vcDest.title = [vcDest.searchPayload valueForKey:@"keyword"];
    }
}



-(void)performSegue:(id)sender withSegueName:(NSString*)segueName
{
    if ([segueName isEqualToString:@"ShowKeywordSearchResult"])
    {
        NSDictionary* dict = (NSDictionary*)sender;
        [self performSegueWithIdentifier:@"ShowKeywordSearchResult" sender:dict];
    }
    if ([segueName isEqualToString:@"ShowCategorySearchResults"]) {
        
        
        NSDictionary* dict = (NSDictionary*)sender;
        NSString* categoryKey = [dict objectForKey:@"categoryKey"];
         NSString * title = @"";
        
         NSArray* categories = [self.level2CategoryDict objectForKey:self.currentSelectedCategoryLevel1Title];
        for (HGCategory *currentCAT in categories)
        {
            int indexValue = (int)currentCAT.index;
            if (indexValue == categoryKey.integerValue) {
                title = currentCAT.title;
                break;
            }
        }
        NSString* rf_tagString = [dict objectForKey:@"rf_tag"];
        [self performSegueWithIdentifier:@"ShowCategorySearchResults" sender:@{@"catID": [NSNumber numberWithInteger:categoryKey.integerValue], @"catTitle": title, @"rf_tag":rf_tagString}];
    }
}

- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSLog(@"navigationcontroller will show view in search center");
     HGAppDelegate* myappdelegate = (HGAppDelegate*)[UIApplication sharedApplication].delegate;
    UIViewController* homeview =  [navController.viewControllers objectAtIndex:0];
    
    if (viewController == homeview) {
        myappdelegate.tabController.fmTabbarView.hidden = NO;
    }
    else{
        myappdelegate.tabController.fmTabbarView.hidden = YES;
    }
    //appDelegate.tabController.fmTabbarView.hidden = YES;
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

@end

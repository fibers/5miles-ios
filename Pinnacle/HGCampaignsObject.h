//
//  HGCampaignsObject.h
//  Pinnacle
//
//  Created by Alex on 14-12-9.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
@interface HGCampaignsObject : JSONModel

@property (nonatomic, assign)NSUInteger begin_at;
@property (nonatomic, assign)NSUInteger end_at;
@property (nonatomic, assign)NSUInteger image_width;
@property (nonatomic, assign)NSUInteger image_height;

@property (nonatomic, strong)NSString* name;
@property (nonatomic, strong)NSString* image_url;
@property (nonatomic, strong)NSString* click_action;
@property (nonatomic, assign)NSUInteger CampaignObjectID;

@end

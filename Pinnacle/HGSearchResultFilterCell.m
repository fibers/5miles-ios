//
//  HGSearchResultFilterCell.m
//  Pinnacle
//
//  Created by Alex on 15/7/23.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGSearchResultFilterCell.h"
#import "HGConstant.h"

@implementation HGSearchResultFilterCell

- (void)awakeFromNib {
    // Initialization code
    [self commonSetup];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self commonSetup];
    }
    return self;
}

-(void)commonSetup
{
    self.titleString = [[UILabel alloc] initWithFrame:CGRectMake(100, 5, 200, 30)];
    [self addSubview:self.titleString];
    self.backgroundColor = [UIColor whiteColor];
    
    
    self.titleString.font =[UIFont systemFontOfSize:12];
    
}



-(void)configCell
{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

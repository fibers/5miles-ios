//
//  HGSysMessageCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-28.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSysMessageCell.h"
#import "HGSysMessage.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGUtils.h"
#import <CoreText/CoreText.h>
#import "UIImage+pureColorImage.h"
#import "HGAppData.h"

@implementation HGSysMessageCell

#define AVATAR_SIZE     34
#define SYSTEM_MESSAGE_IMAGE_HEIGHT 130

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _commonSetup];
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self _commonSetup];
}

- (void)_commonSetup
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.avatarView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"teamicon"]];
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatarView.layer.borderWidth = 0;
    self.avatarView.layer.cornerRadius = AVATAR_SIZE * 0.5;
    self.avatarView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.avatarView];
    
    self.canvasView = [HGSysMessageDrawingView new];
    self.canvasView.ownerCell = self;
    self.canvasView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.canvasView];
    
    
    self.itemImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 34, 34)];
    
    self.itemImageView.layer.borderWidth = 0;
    
    self.itemImageView.layer.cornerRadius = 3;
    self.itemImageView.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
    self.itemImageView.clipsToBounds = YES;
    self.itemImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.itemImageView];
    
    self.SeperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, self.contentView.frame.size.height-1, CGRectGetWidth(self.bounds), 0.5)];
    self.SeperatorView.backgroundColor = [UIColor colorWithRed:0xcc/255.0 green:0xcc/255.0 blue:0xcc/255.0 alpha:1.0];
    [self.contentView addSubview:self.SeperatorView];
    
    self.urlAcionIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24/2, 23/2)];
    self.urlAcionIcon.image = [UIImage imageNamed:@"icon-url-link"];
    [self.contentView addSubview:self.urlAcionIcon];
    
    self.systemMessageImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 240, 130)];
    [self.contentView addSubview:self.systemMessageImage];
    
}

#define CONTENT_Y_OFFSET 3
- (void)layoutSubviews
{
    self.contentView.frame = self.bounds;
    
    self.avatarView.frame = CGRectMake(8, 8, AVATAR_SIZE, AVATAR_SIZE);
    self.avatarView.layer.cornerRadius = AVATAR_SIZE/2;
    if (self.systemMessageImage.hidden) {
        self.systemMessageImage.frame = CGRectMake(CGRectGetMaxX(self.avatarView.frame) + 10, CGRectGetMinY(self.avatarView.frame), 240, 0) ;
        self.canvasView.frame = CGRectMake(CGRectGetMaxX(self.avatarView.frame) + 10, CGRectGetMinY(self.avatarView.frame), CGRectGetWidth(self.frame) - CGRectGetMaxX(self.avatarView.frame) - 20, CGRectGetHeight(self.frame) - CGRectGetMinY(self.avatarView.frame));
    }
    else{
        self.systemMessageImage.frame = CGRectMake(CGRectGetMaxX(self.avatarView.frame) + 10, CGRectGetMinY(self.avatarView.frame), 240, 130) ;
        self.canvasView.frame = CGRectMake(CGRectGetMaxX(self.avatarView.frame) + 10, CGRectGetMinY(self.avatarView.frame)+130, CGRectGetWidth(self.frame) - CGRectGetMaxX(self.avatarView.frame) - 20, CGRectGetHeight(self.frame) - CGRectGetMinY(self.avatarView.frame) - 130);
    }
    
    
    
    
    self.SeperatorView.frame = CGRectMake(10, self.contentView.frame.size.height-1, CGRectGetWidth(self.bounds), 0.5);
    
    self.urlAcionIcon.frame = CGRectMake(self.bounds.size.width-29, self.bounds.size.height-18, self.urlAcionIcon.frame.size.width, self.urlAcionIcon.frame.size.height);
   
}

- (void)configWithSysMessage:(HGSysMessage *)message
{
    self.message = message;
    if (self.message.image != nil && self.message.image.length > 0) {
        float width = self.itemImageView.frame.size.width;
        float height =self.itemImageView.frame.size.height;
        NSString * itemImageLink = [[HGUtils sharedInstance] cloudinaryLink:self.message.image width:width * 2 height:height * 2];
        [self.itemImageView sd_setImageWithURL:[NSURL URLWithString:itemImageLink]  placeholderImage:[UIImage imageWithColor:FANCY_COLOR(@"eaeaea")]];
        //self.itemImageView.layer.cornerRadius = 17;
        if (self.message.image_type == 1) {
            self.itemImageView.layer.cornerRadius = width/2;
            self.itemImageView.layer.borderWidth = 1;
            self.itemImageView.layer.borderColor = SYSTEM_MY_ORANGE.CGColor;
        }
        else{
            self.itemImageView.layer.cornerRadius = 2.0;
            self.itemImageView.layer.borderWidth = 0;
        }
        
        //NSLog(@"the image link is %@", self.message.image);
        [self.systemMessageImage sd_setImageWithURL:[NSURL URLWithString:itemImageLink]  placeholderImage:[UIImage imageWithColor:FANCY_COLOR(@"eaeaea")]];
        
        self.systemMessageImage.hidden = YES;
        self.itemImageView.hidden = NO;
        self.avatarView.hidden = YES;
    }
    else{
        self.systemMessageImage.hidden =YES;
        self.itemImageView.hidden = YES;
        self.avatarView.hidden = NO;
    }
    
    
    [self.canvasView setNeedsDisplay];
    //[self.canvasView configuration];
    
    
    
    self.SeperatorView.backgroundColor = [UIColor colorWithRed:0xcc/255.0 green:0xcc/255.0 blue:0xcc/255.0 alpha:1.0];

   
    self.SeperatorView.hidden = NO;
    
    
    if (message.action != nil && message.action.length > 0) {
        if( message.smType == HGSysMessageTypeReview && ![HGAppData sharedInstance].bOpenReviewFunctions){
            self.urlAcionIcon.hidden = YES;
        }else{
            self.urlAcionIcon.hidden = NO;
        }
    }
    else{
        self.urlAcionIcon.hidden = YES;
    }
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGRect rect = [self.message.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.bounds) - 60, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0]} context:nil];
    
    return CGSizeMake(CGRectGetWidth(self.bounds), CGRectGetHeight(rect) + 36) ;
}

@end



@interface  HGSysMessageDrawingView()

@property (nonatomic, assign)CGRect rectTitle;
@property (nonatomic, assign)NSUInteger titleLength;
@property (nonatomic, assign)CTFramesetterRef framesetter;
@property (nonatomic, strong)NSAttributedString* attributeString;
@property (nonatomic, strong)NSMutableArray* links;

@property (nonatomic, strong) UIImageView* ratingImage;
@end


@implementation HGSysMessageDrawingView

- (void)drawRect:(CGRect)rect
{
    // Draw title
    //
   
    
    [self normalTextDraw:rect];
    
    
}
-(void)configuration
{
    [self AddTextUsingLabel:self.frame];
}

-(void)AddTextUsingLabel:(CGRect)rect
{
     CGRect rect1 = [self.ownerCell.message.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(rect) - 60, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0]} context:nil];
    self.titleLabel.frame = CGRectMake(0, 0, rect.size.width, rect1.size.height);
    self.titleLabel.text = self.ownerCell.message.text;
  
    
    self.descLabel.frame = CGRectMake(0, rect1.size.height -10 , rect.size.width, rect.size.height-rect1.size.height);
    self.descLabel.text = self.ownerCell.message.desc;
    
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:self.ownerCell.message.timestamp];
    NSString * dateString = [[HGUtils sharedInstance] friendlyDate:date];

    self.dateLabel.frame = CGRectMake(0, rect.size.height - 20, 100, 20);
    self.dateLabel.text = dateString;
    
   
}
-(NSArray*)getUrlStringArrray:(NSString*)inputString
{
    NSMutableArray* urlstringArray = [[NSMutableArray alloc] init] ;
    NSError *error;
    NSString *regulaStr = @"\\bhttps?://[a-zA-Z0-9\\-.]+(?::(\\d+))?(?:(?:/[a-zA-Z0-9\\-._?,'+\\&%$=~*!():@\\\\]*)+)?";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *arrayOfAllMatches = [regex matchesInString:inputString options:0 range:NSMakeRange(0, [inputString length])];
    
    for (NSTextCheckingResult *match in arrayOfAllMatches)
    {
        NSString* substringForMatch = [inputString substringWithRange:match.range];
        NSLog(@"substringForMatch");
        [urlstringArray addObject:substringForMatch];
    }
    return urlstringArray;
}
-(void)setAtrributionForString:(NSMutableAttributedString*)inputAttributeString withOriginalString:(NSString*)originalString withKeyStrings:(NSArray*)keyStrings
{
    NSRange searchRange = NSMakeRange(0, originalString.length);
    for (int i = 0 ; i< keyStrings.count; i++) {
        NSString*currrentKeyString = [keyStrings objectAtIndex:i];
        NSRange  range = [originalString rangeOfString:currrentKeyString options:NSCaseInsensitiveSearch range:searchRange];
        if (range.length > 0 ) {
           
            
            searchRange.location = range.location + range.length;
            searchRange.length = originalString.length - searchRange.location;
            UIFont *font = [UIFont systemFontOfSize:15.0f];
            UIColor* fontcolor = [UIColor blueColor];
            [inputAttributeString addAttribute:NSFontAttributeName value:font range:range];
            [inputAttributeString addAttribute:NSForegroundColorAttributeName value:fontcolor range:range];
            [inputAttributeString addAttribute: NSLinkAttributeName value:currrentKeyString range: range];
            
            [self addLinkToURL:[NSURL URLWithString:currrentKeyString] withRange:range];
            //[inputAttributeString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:range];
        }

    }
    
}
-(void)normalTextDraw:(CGRect)rect
{
    CGRect rect1 = [self.ownerCell.message.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(rect) , CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading   attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0]} context:nil];
    
    {
        if (self.ownerCell.message.text == nil) {
            self.ownerCell.message.text = @"";
        }
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.ownerCell.message.text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSForegroundColorAttributeName:SYSTEM_DEFAULT_FONT_COLOR_1}];
        
        NSArray* urls = [self getUrlStringArrray:self.ownerCell.message.text];
        [self setAtrributionForString:attributedText withOriginalString:self.ownerCell.message.text withKeyStrings:urls];
        
        
        //[attributedText drawInRect:CGRectMake(rect.origin.x,0, rect.size.width, rect1.size.height)];
        
        [attributedText drawWithRect:CGRectMake(rect.origin.x,0, rect.size.width, rect1.size.height) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
        self.rectTitle =rect1;
        self.titleLength = self.ownerCell.message.text.length;
        
        
        self.framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)attributedText);
        self.attributeString = attributedText;
        
    }
    
    if (self.ownerCell.message.smType == HGSysMessageTypeReview && [HGAppData sharedInstance].bOpenReviewFunctions) {
        //review message
        self.ratingImage.hidden = NO;
         self.ratingImage.frame = CGRectMake(0, CGRectGetMaxY(self.rectTitle)+3, 91, 12);
        
       NSString* image_index = [[HGUtils sharedInstance] getReviewImageNameByScore:self.ownerCell.message.review_score];
        
      
        
        self.ratingImage.image = [UIImage imageNamed:image_index];
    }
    else{
        self.ratingImage.hidden = YES;
    }
    
    
    // Draw desc
    //CGRect rect2 = [self.ownerCell.message.desc boundingRectWithSize:CGSizeMake(CGRectGetWidth(rect) , CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13.0]} context:nil];
    {
        
        if (self.ownerCell.message.desc == nil) {
            self.ownerCell.message.desc = @"";
        }
        NSMutableAttributedString *attributedText_Describ = [[NSMutableAttributedString alloc] initWithString:self.ownerCell.message.desc attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSForegroundColorAttributeName:FANCY_COLOR(@"818181")}];
        
        NSArray* urls_des = [self getUrlStringArrray:self.ownerCell.message.desc];
        [self setAtrributionForString:attributedText_Describ withOriginalString:self.ownerCell.message.desc withKeyStrings:urls_des];
        
        //[attributedText_Describ drawInRect:CGRectMake(rect.origin.x, rect1.size.height  , rect.size.width, rect.size.height-rect1.size.height) ];
        float rect2Yoffset = rect1.size.height;
        if (!self.ratingImage.hidden) {
            rect2Yoffset = rect2Yoffset + 18;
        }
        
        
        [attributedText_Describ drawWithRect:CGRectMake(rect.origin.x, rect2Yoffset  , rect.size.width, rect.size.height-rect1.size.height -20) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine
                                     context:nil];

    }
   
    
    
    // Draw date
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:self.ownerCell.message.timestamp];
    NSString * dateString = [[HGUtils sharedInstance] friendlyDate:date];
    UIFont * dateFont = [UIFont systemFontOfSize:13.0];
    //CGRect dateRect = [dateString boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, dateFont.lineHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:dateFont} context:nil];
    [dateString drawAtPoint:CGPointMake(rect.origin.x  ,rect.size.height -20) withAttributes:@{NSFontAttributeName: dateFont, NSForegroundColorAttributeName: FANCY_COLOR(@"818181")}];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _commonSetup];
    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self _commonSetup];
}

-(void)_commonSetup
{
    /*self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self addSubview:self.titleLabel];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.titleLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    
    
    self.descLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self addSubview:self.descLabel];
    self.descLabel.textColor = FANCY_COLOR(@"818181");
    self.descLabel.font = [UIFont systemFontOfSize:13.0];
    
   
    UIFont * dateFont = [UIFont systemFontOfSize:12.0];
    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.dateLabel.textColor = FANCY_COLOR(@"818181");
    self.dateLabel.font = dateFont;
    [self addSubview:self.dateLabel];
    
    self.titleLabel.textAlignment = self.descLabel.textAlignment = self.dateLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.numberOfLines = self.descLabel.numberOfLines = self.dateLabel.numberOfLines = 0;
     */
    self.userInteractionEnabled = YES;
    self.ratingImage = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self addSubview:self.ratingImage];
    
}
-(void)layoutSubviews
{
    
}

#pragma mark - UIResponder

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)canPerformAction:(SEL)action
              withSender:(__unused id)sender
{
    return (action == @selector(copy:));
}
- (CFIndex)characterIndexAtPoint:(CGPoint)p {
    if (!CGRectContainsPoint(self.bounds, p)) {
        return NSNotFound;
    }
    
    
    if (!CGRectContainsPoint(self.rectTitle, p)) {
        return NSNotFound;
    }
    
    // Offset tap coordinates by textRect origin to make them relative to the origin of frame
    p = CGPointMake(p.x - self.rectTitle.origin.x, p.y - self.rectTitle.origin.y);
    // Convert tap coordinates (start at top left) to CT coordinates (start at bottom left)
    p = CGPointMake(p.x, self.rectTitle.size.height - p.y);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, self.rectTitle);
    CTFrameRef frame = CTFramesetterCreateFrame(self.framesetter, CFRangeMake(0, (CFIndex)self.titleLength), path, NULL);
    if (frame == NULL) {
        CFRelease(path);
        return NSNotFound;
    }
    
    CFArrayRef lines = CTFrameGetLines(frame);
    NSInteger numberOfLines =  CFArrayGetCount(lines) ;
    if (numberOfLines == 0) {
        CFRelease(frame);
        CFRelease(path);
        return NSNotFound;
    }
    
    CFIndex idx = NSNotFound;
    
    CGPoint lineOrigins[numberOfLines];
    CTFrameGetLineOrigins(frame, CFRangeMake(0, numberOfLines), lineOrigins);
    
    for (CFIndex lineIndex = 0; lineIndex < numberOfLines; lineIndex++) {
        CGPoint lineOrigin = lineOrigins[lineIndex];
        CTLineRef line = CFArrayGetValueAtIndex(lines, lineIndex);
        
        // Get bounding information of line
        CGFloat ascent = 0.0f, descent = 0.0f, leading = 0.0f;
        CGFloat width = (CGFloat)CTLineGetTypographicBounds(line, &ascent, &descent, &leading);
        CGFloat yMin = (CGFloat)floor(lineOrigin.y - descent);
        CGFloat yMax = (CGFloat)ceil(lineOrigin.y + ascent);
        
        // Check if we've already passed the line
        if (p.y > yMax) {
            break;
        }
        // Check if the point is within this line vertically
        if (p.y >= yMin) {
            // Check if the point is within this line horizontally
            if (p.x >= lineOrigin.x && p.x <= lineOrigin.x + width) {
                // Convert CT coordinates to line-relative coordinates
                CGPoint relativePoint = CGPointMake(p.x - lineOrigin.x, p.y - lineOrigin.y);
                idx = CTLineGetStringIndexForPosition(line, relativePoint);
                break;
            }
        }
    }
    
    CFRelease(frame);
    CFRelease(path);
    
    return idx;
}
- (NSTextCheckingResult *)linkAtCharacterIndex:(CFIndex)idx {
    NSEnumerator *enumerator = [self.links reverseObjectEnumerator];
    NSTextCheckingResult *result = nil;
    while ((result = [enumerator nextObject])) {
        if (NSLocationInRange((NSUInteger)idx, result.range)) {
            return result;
        }
    }
    
    return nil;
}
- (NSTextCheckingResult *)linkAtPoint:(CGPoint)point {
    return [self linkAtCharacterIndex:[self characterIndexAtPoint:point]];
}
- (void)touchesBegan:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    self.activeLink = [self linkAtPoint:[touch locationInView:self]];
    
    if (!self.activeLink) {
        [super touchesBegan:touches withEvent:event];
    }
}

- (void)touchesMoved:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    if (self.activeLink) {
        UITouch *touch = [touches anyObject];
        
        if (self.activeLink != [self linkAtPoint:[touch locationInView:self]]) {
            self.activeLink = nil;
        }
    } else {
        [super touchesMoved:touches withEvent:event];
    }
}

- (void)touchesEnded:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    if (self.activeLink) {
        NSTextCheckingResult *result = self.activeLink;
        self.activeLink = nil;
        switch (result.resultType) {
                
            case NSTextCheckingTypeLink:
            {
                if (self.delegate && [self.delegate respondsToSelector:@selector(clickUrlAction:)]) {
                    [self.delegate clickUrlAction:result.URL];
                }
                break;
            }
            default:
                break;
                
        }
    } else {
        [super touchesEnded:touches withEvent:event];
    }
}

- (void)touchesCancelled:(NSSet *)touches
               withEvent:(UIEvent *)event
{
    if (self.activeLink) {
        self.activeLink = nil;
    } else {
        [super touchesCancelled:touches withEvent:event];
    }
}
- (void)addLinkWithTextCheckingResult:(NSTextCheckingResult *)result
                           attributes:(NSDictionary *)attributes
{
    [self addLinksWithTextCheckingResults:[NSArray arrayWithObject:result] attributes:attributes];
}

- (void)addLinksWithTextCheckingResults:(NSArray *)results
                             attributes:(NSDictionary *)attributes
{
    NSMutableArray *mutableLinks = [NSMutableArray arrayWithArray:self.links];
    if (attributes) {
        NSMutableAttributedString *mutableAttributedString = [self.attributeString mutableCopy];
        for (NSTextCheckingResult *result in results) {
            [mutableAttributedString addAttributes:attributes range:result.range];
        }
        
        self.attributeString = mutableAttributedString;
        [self setNeedsDisplay];
    }
    [mutableLinks addObjectsFromArray:results];
    
    self.links = [NSMutableArray arrayWithArray:mutableLinks];
}

- (void)addLinkWithTextCheckingResult:(NSTextCheckingResult *)result {
    [self addLinkWithTextCheckingResult:result attributes:nil];
}

- (void)addLinkToURL:(NSURL *)url
           withRange:(NSRange)range
{
    [self addLinkWithTextCheckingResult:[NSTextCheckingResult linkCheckingResultWithRange:range URL:url]];
}


@end

//
//  HGSectionTitleReusableView.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-21.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSectionTitleReusableView.h"
#import "HGConstant.h"
@implementation HGSectionTitleReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, CGRectGetWidth(frame)-15, CGRectGetHeight(frame))];
        self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
        self.titleLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.titleLabel.text = nil;
}

@end

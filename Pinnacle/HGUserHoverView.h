//
//  HGUserHoverView.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HGUser;
@class HGUserHoverBkgView;

@interface HGUserHoverView : UIView

@property (nonatomic, strong) UIImageView * avatarView;
@property (nonatomic, strong) HGUserHoverBkgView * bkgView;
@property (nonatomic, strong) HGUser * user;

@property (nonatomic, strong) UIImageView*  avatarCover;
@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;
@end

@interface HGUserHoverBkgView : UIView

@end

//
//  HGItemDetailLayout.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-20.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemDetailLayout.h"
#import "HGConstant.h"

NSString * const HGCollectionElementKindGlobalHeader = @"HGCollectionElementKindGlobalHeader";
NSString *const HGCollectionElementKindGlobalFooter = @"HGCollectionElementKindGlobalFooter";

@interface HGItemDetailLayout ()

@property (nonatomic, strong) NSMutableDictionary *headersAttributes;
@property (nonatomic, strong) NSMutableDictionary *footersAttributes;


@property (nonatomic, strong) NSMutableArray *sectionItemAttributes;
@property (nonatomic, strong) NSMutableArray *allItemAttributes;
@property (nonatomic, strong) NSMutableArray *columnHeights;
@property (nonatomic, strong) NSMutableArray *unionRects;

@property (nonatomic, assign) float headCellBottomPosition;
@property (nonatomic, assign) float offerCellBottomPosition;
@end

@implementation HGItemDetailLayout
{
    CGFloat _height;                        // height of content area
}

const NSInteger UNIONSIZE = 20;

- (id)init {
    if (self = [super init]) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    _columnCount = 2;
    _minimumColumnSpacing = 10;
    _minimumInteritemSpacing = 10;
    _headerHeight = 0;
    _FooterHeigth = 0;
    _sectionInset = UIEdgeInsetsZero;
    
    _height = 0;
    
    self.headersAttributes = [@{} mutableCopy];
    self.footersAttributes = [@{} mutableCopy];
    
    self.sectionItemAttributes = [@[] mutableCopy];
    self.allItemAttributes = [@[] mutableCopy];
    self.columnHeights = [@[] mutableCopy];
    self.unionRects = [@[] mutableCopy];
}

- (void)prepareLayout
{
    [super prepareLayout];
    
    _height = 0;
    [self.headersAttributes removeAllObjects];
    [self.footersAttributes removeAllObjects];
    
    [self.unionRects removeAllObjects];
    [self.columnHeights removeAllObjects];
    [self.allItemAttributes removeAllObjects];
    [self.sectionItemAttributes removeAllObjects];
    
    NSInteger numberOfSections = self.collectionView.numberOfSections;
    for (NSInteger section = 0; section < numberOfSections; ++section) {
        // Section Header
        CGFloat headerHeight;
        if (self.delegate && [self.delegate respondsToSelector:@selector(collectionView:layout:heightForHeaderInSection:)]) {
            headerHeight = [self.delegate collectionView:self.collectionView layout:self heightForHeaderInSection:section];
        } else {
            headerHeight = self.headerHeight;
        }
        if (headerHeight > 0) {
            UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader withIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]];
            attributes.frame = CGRectMake(0, _height, self.collectionView.frame.size.width, headerHeight);
            
            self.headersAttributes[@(section)] = attributes;
            [self.allItemAttributes addObject:attributes];
            
            _height += headerHeight;
        }
        self.headCellBottomPosition = _height;
       
        
        // Section Items
        NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
        NSMutableArray *itemAttributes = [NSMutableArray arrayWithCapacity:itemCount];
        
        CGFloat minimumInteritemSpacing;
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:minimumInteritemSpacingForSectionAtIndex:)]) {
            minimumInteritemSpacing = [self.delegate collectionView:self.collectionView layout:self minimumInteritemSpacingForSectionAtIndex:section];
        } else {
            minimumInteritemSpacing = self.minimumInteritemSpacing;
        }

        UIEdgeInsets sectionInset;
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:insetForSectionAtIndex:)]) {
            sectionInset = [self.delegate collectionView:self.collectionView layout:self insetForSectionAtIndex:section];
        } else {
            sectionInset = self.sectionInset;
        }
        
        if (section < 3) {
            /*
             * Offerlne & Comments Sections
             */

            for (NSInteger idx=0; idx<itemCount; idx++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:section];
                CGSize itemSize = [self.delegate collectionView:self.collectionView layout:self sizeForItemAtIndexPath:indexPath];
                
                UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
                attributes.frame = CGRectMake(0, _height, itemSize.width, itemSize.height);
                [itemAttributes addObject:attributes];
                [self.allItemAttributes addObject:attributes];
                
                _height += itemSize.height;
            }
        }
        self.offerCellBottomPosition = _height;
        
        //CGFloat oldGlobalHeight = _height;
        //CGFloat maxGlobalHeight = _height;

        if (section >= 3) {
            /*
             * Last Waterfall Section
             */
            
            CGFloat width = self.collectionView.frame.size.width - sectionInset.left - sectionInset.right;
            CGFloat itemWidth = floorf((width - (self.columnCount - 1) * self.minimumColumnSpacing) / self.columnCount);

            for (NSInteger idx = 0; idx < self.columnCount; idx++) {
                [self.columnHeights addObject:@(_height)];
            }
                       for (NSInteger idx = 0; idx < itemCount; idx++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:section];
                NSUInteger columnIndex = [self shortestColumnIndex];
                CGFloat xOffset = sectionInset.left + (itemWidth + self.minimumColumnSpacing) * columnIndex;
                CGFloat yOffset = [self.columnHeights[columnIndex] floatValue];
                CGSize itemSize = [self.delegate collectionView:self.collectionView layout:self sizeForItemAtIndexPath:indexPath];
                CGFloat itemHeight = 0;
                if (itemSize.height > 0 && itemSize.width > 0) {
                    itemHeight = floorf(itemSize.height * itemWidth / itemSize.width);
                }
               
                
                UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
                attributes.frame = CGRectMake(xOffset, yOffset, itemWidth, itemHeight);
                           
                [itemAttributes addObject:attributes];
                [self.allItemAttributes addObject:attributes];
                self.columnHeights[columnIndex] = @(CGRectGetMaxY(attributes.frame) + minimumInteritemSpacing);
            }
            NSUInteger longestColumnIndex = [self longestColumnIndex];
            _height = [self.columnHeights[longestColumnIndex] floatValue];
        }
        
        [self.sectionItemAttributes addObject:itemAttributes];
        //get the last item frame of water flow
        
        
        //section footer
        CGFloat footerHeight = 0;
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:heightForFooterInSection:)]) {
            footerHeight = [self.delegate collectionView:self.collectionView layout:self heightForFooterInSection:section];
        }else{
            footerHeight = self.FooterHeigth;
        }
        if (footerHeight > 0) {
            UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionFooter withIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]];
            attributes.frame = CGRectMake(0,_height , self.collectionView.frame.size.width, footerHeight);
            
            self.footersAttributes[@(section)] = attributes;
            [self.allItemAttributes addObject:attributes];
            
            _height += footerHeight;
        }
        

    }

    
    
    
    // Build union rects
    NSInteger idx = 0;
    NSInteger itemCounts = [self.allItemAttributes count];
    while (idx < itemCounts) {
        CGRect rect1 = ((UICollectionViewLayoutAttributes *)self.allItemAttributes[idx]).frame;
        idx = MIN(idx + UNIONSIZE, itemCounts) - 1;
        CGRect rect2 = ((UICollectionViewLayoutAttributes *)self.allItemAttributes[idx]).frame;
        [self.unionRects addObject:[NSValue valueWithCGRect:CGRectUnion(rect1, rect2)]];
        idx++;
    }
}

static int defaultLayoutContentHeight = 594; //634
-(float)getExtraHeight
{
    if (SCREEN_HEIGHT == 480 || SCREEN_HEIGHT == 568) {
        //iphone4 || iphone5
        return defaultLayoutContentHeight - SCREEN_HEIGHT;
    }
    else if(SCREEN_HEIGHT == 667 || SCREEN_HEIGHT == 736){
        ///iphone6|| iphone6p
        return defaultLayoutContentHeight - 568;
    }
    return 0;
}
- (CGSize)collectionViewContentSize
{
    CGSize contentSize = self.collectionView.bounds.size;
    NSInteger idx = [self longestColumnIndex];
    contentSize.height = self.columnHeights.count > 0 ? [self.columnHeights[idx] floatValue] :self.offerCellBottomPosition;
    contentSize.height += [self getExtraHeight]  ;
    
    return contentSize;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section >= [self.sectionItemAttributes count]) {
        return nil;
    }
    if (indexPath.item >= [self.sectionItemAttributes[indexPath.section] count]) {
        return nil;
    }
    return (self.sectionItemAttributes[indexPath.section])[indexPath.item];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath
{
    if ([elementKind isEqualToString:UICollectionElementKindSectionHeader]) {
        return self.headersAttributes[@(indexPath.section)];
    }
    if ([elementKind isEqualToString:UICollectionElementKindSectionFooter]) {
        return self.footersAttributes[@(indexPath.section)];
    }
    return nil;
    
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSInteger i;
    NSInteger begin = 0, end = self.unionRects.count;
    NSMutableArray *attrs = [NSMutableArray array];
    
    for (i = 0; i < self.unionRects.count; i++) {
        if (CGRectIntersectsRect(rect, [self.unionRects[i] CGRectValue])) {
            begin = i * UNIONSIZE;
            break;
        }
    }
    
    for (i = self.unionRects.count - 1; i >= 0; i--) {
        if (CGRectIntersectsRect(rect, [self.unionRects[i] CGRectValue])) {
            end = MIN((i + 1) * UNIONSIZE, self.allItemAttributes.count);
            break;
        }
    }
    
    for (i = begin; i < end; i++) {
        UICollectionViewLayoutAttributes *attr = self.allItemAttributes[i];
        if (CGRectIntersectsRect(rect, attr.frame)) {
            [attrs addObject:attr];
        }
    }
    
    return [NSArray arrayWithArray:attrs];
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    CGRect oldBounds = self.collectionView.bounds;
    if (CGRectGetWidth(newBounds) != CGRectGetWidth(oldBounds)) {
        return YES;
    }
    return NO;
}

#pragma mark - Private Methods

/**
 *  Find the shortest column.
 *
 *  @return index for the shortest column
 */
- (NSUInteger)shortestColumnIndex {
    __block NSUInteger index = 0;
    __block CGFloat shortestHeight = MAXFLOAT;
    
    [self.columnHeights enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CGFloat height = [obj floatValue];
        if (height < shortestHeight) {
            shortestHeight = height;
            index = idx;
        }
    }];
    
    return index;
}

/**
 *  Find the longest column.
 *
 *  @return index for the longest column
 */
- (NSUInteger)longestColumnIndex {
    __block NSUInteger index = 0;
    __block CGFloat longestHeight = 0;
    
    [self.columnHeights enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CGFloat height = [obj floatValue];
        if (height > longestHeight) {
            longestHeight = height;
            index = idx;
        }
    }];
    
    return index;
}

@end

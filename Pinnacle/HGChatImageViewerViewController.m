//
//  HGChatImageViewerViewController.m
//  Pinnacle
//
//  Created by shengyuhong on 15/5/26.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatImageViewerViewController.h"
#import "HGUtils.h"


@interface HGChatImageViewerViewController ()

@property (nonatomic, strong) X4ImageViewer *imageViewer;
@property (weak, nonatomic) IBOutlet UIView *displayView;

@end

@implementation HGChatImageViewerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.imageViewer = [[X4ImageViewer alloc] initWithFrame:self.displayView.bounds];
    self.imageViewer.delegate = self;
    self.imageViewer.carouselPosition = CarouselPositionBottomLeft;
    self.imageViewer.carouselType = CarouselTypePageNumber;
    self.imageViewer.contentMode = ContentModeAspectNormal;
    self.imageViewer.currentPageIndex = self.currentPageIndex;
    [self.imageViewer setPageControlCurrentIndicatorImage:[UIImage imageNamed:@"pageControl-active"]];
    [self.imageViewer setPageControlIndicatorImage:[UIImage imageNamed:@"pageControl-inactive"]];
    [self.imageViewer setImages:self.imagesArray withPlaceholder:nil];

    [self.displayView addSubview:self.imageViewer];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage-title"]];
    //self.navigationItem.hidesBackButton = YES;
}

- (void)viewDidLayoutSubviews{
    self.imageViewer.frame = self.displayView.bounds;
}


- (void)dealloc{
    self.imageViewer.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)imageViewer:(X4ImageViewer *)imageViewer didEndZoomingWith:(UIImageView *)imageView atIndex:(NSInteger)index inScrollView:(UIScrollView *)scrollView{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"chatlookphoto_view" action:@"chatphoto_pinch" label:nil value:nil];
}

- (void)imageViewer:(X4ImageViewer *)imageViewer didSlideFrom:(UIImageView *)fromImageView fromIndex:(NSInteger)fromIndex to:(UIImageView *)toImageView toIndex:(NSInteger)toIndex{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"chatlookphoto_view" action:@"chatphoto_slide" label:nil value:nil];
}

- (void)imageViewer:(X4ImageViewer *)imageViewer didSingleTap:(UIImageView *)imageView atIndex:(NSInteger)index inScrollView:(UIScrollView *)scrollView{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"chatlookphoto_view" action:@"chatphoto_close" label:nil value:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)imageViewer:(X4ImageViewer *)imageViewer loadingInProcess:(UIImageView *)imageView withProcess:(CGFloat)process atIndex:(NSInteger)index{
}

- (void)imageViewer:(X4ImageViewer *)imageViewer loadingSuccess:(UIImageView *)imageView withImage:(UIImage *)image atIndex:(NSInteger)index{
    
}


- (void)imageViewer:(X4ImageViewer *)imageViewer loadingFailed:(UIImageView *)imageView withError:(NSError *)error atIndex:(NSInteger)index{
}

@end

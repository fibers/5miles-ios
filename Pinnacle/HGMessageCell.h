//
//  HGMessageCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-1.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGRoundCornerImageView.h"

@class HGMessage;

@interface HGMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet HGAvararImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (strong, nonatomic) UILabel *lbMessage;
@property (weak, nonatomic) IBOutlet HGRoundCornerImageView *itemView;
@property (nonatomic, strong) UIImageView* UnreadMark;
@property (nonatomic, strong) UILabel * UnreadNumber;

//@property (nonatomic, strong) UIImageView* avatarCover;
//@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;
@property (nonatomic, strong) UIView* SeperatorView;




@property (nonatomic, strong)HGMessage* message;
- (void)configWithEntity:(HGMessage *)message;


@end

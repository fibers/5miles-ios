//
//  HGCategoryCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGCategory.h"

@interface HGCategoryCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;




@property (nonatomic, strong) UIView* seperatorView;

-(void)configCategoryCell:(HGCategory*)category;
@end

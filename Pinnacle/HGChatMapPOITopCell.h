//
//  HGChatMapPOITopCell.h
//  Pinnacle
//
//  Created by Alex on 15/5/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGChatMapPOITopCell : UITableViewCell
@property (nonatomic, strong)UILabel* titleText;
@property (nonatomic, strong)UIView* seperatorLine;


-(void)configCell:(NSString*)titleString;
@end

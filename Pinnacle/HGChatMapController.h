//
//  HGChatMapController.h
//  Pinnacle
//
//  Created by Alex on 15/5/6.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class HGChatMapController;

@protocol HGChatMapDelegate <NSObject>

-(void)chatMapContrller:(HGChatMapController*)maper startSendingLocation:(UIImage*)locationImage contentDictionary:(NSDictionary*)dict;

@end



@interface HGChatMapController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, MKMapViewDelegate>


@property(nonatomic, assign) id<HGChatMapDelegate> delegate;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic, assign) int defaultSelectedPOIindex;


@property (nonatomic, assign) CLLocationCoordinate2D theOtherUserCoord;


@property (nonatomic, strong) NSMutableArray* annotionArray;
@end

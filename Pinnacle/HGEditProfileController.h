//
//  HGEditProfileController.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-6.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGEditProfileController : UITableViewController
@property (weak, nonatomic) IBOutlet UITableViewCell *photocell;
@property (weak, nonatomic) IBOutlet UITableViewCell *namecell;
@property (weak, nonatomic) IBOutlet UITableViewCell *emailcell;
@property (weak, nonatomic) IBOutlet UITableViewCell *ZipCodeCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *phoneCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *facebookcell;


@property (weak, nonatomic) IBOutlet UILabel *ZipCodeLabel;
@property (weak, nonatomic) IBOutlet UITextField *zipCodeContent;

@property (weak, nonatomic) IBOutlet UILabel *photolabel;
@property (weak, nonatomic) IBOutlet UILabel *phonelabel;
@property (weak, nonatomic) IBOutlet UILabel *namelabel;
@property (weak, nonatomic) IBOutlet UILabel *emaillabel;
@property (weak, nonatomic) IBOutlet UILabel *facebooklabel;



@property (weak, nonatomic) IBOutlet UISwitch *linkFBaccountSwitch;


@end

//
//  HGWebContentController.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-6.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGWebContentController.h"
#import "HGUtils.h"
#import <SVProgressHUD.h>
#import "NSURL+Pinnacle.h"
#import "HGFBUtils.h"

@interface HGWebContentController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation HGWebContentController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)onTouchShareButton
{
    [[HGFBUtils sharedInstance] shareWebLink:@"https://5milesapp.com/review" withSuccess:^(NSDictionary *reponse) {
        [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Share success.", nil) onView:self.view];
    } withFailure:^(NSError *error) {
        
        [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Share fail", nil) onView:self.view];
        NSLog(@"Error: %@", error);
        
    }];
}
-(void)addRightShareButton
{
    UIButton*rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,30,30)];
    [rightButton addTarget:self action:@selector(onTouchShareButton) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:[UIImage imageNamed:@"item_detail_share"]forState:UIControlStateNormal];
    UIBarButtonItem*rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem= rightItem;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.bShowRightShareButton) {
        [self addRightShareButton];
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:self.contentLink]];
    [request setHTTPMethod:@"GET"];
    
    
    NSString* preferredLang = [[HGUtils sharedInstance] getPreferLanguage];
    
    NSString *contentType = [NSString stringWithFormat:@"text/xml"];
    [request addValue:preferredLang forHTTPHeaderField:@"Accept-Language"];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.contentLink]]];
    // [SVProgressHUD dismiss];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage-title"]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    if (self.webView.isLoading) {
        [self.webView stopLoading];
    }
    self.webView = nil;
}

- (void)dealloc
{
    //[super dealloc];
    [self.webView stopLoading];
}

#pragma mark - UIWebview Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    if ([self.contentLink isEqualToString:@"https://5milesapp.com/info/support"]) {
        //can not dismiss this web page , so temply disalbe the loading animation for this page.
        return;
    }
    
    [SVProgressHUD show];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
    //self.navigationItem.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"load web content failed:%@", error);
    [SVProgressHUD dismiss];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if(navigationType == UIWebViewNavigationTypeLinkClicked){
        NSURL *url = request.URL;
        NSDictionary *queryDict = [url queryAsDictionary];
        NSString *target = [queryDict objectForKey:@"target"];
        
        if([target isEqualToString:@"_blank"]){
            [[UIApplication sharedApplication] openURL:url];
            return NO;
        }
    }
    
    return YES;
}

@end

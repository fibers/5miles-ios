//
//  HGProfileSelfIntroductionViewController.m
//  Pinnacle
//
//  Created by Alex on 14-10-1.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGProfileSelfIntroductionViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HGConstant.h"
@interface HGProfileSelfIntroductionViewController ()

@end

@implementation HGProfileSelfIntroductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;//[UIColor groupTableViewBackgroundColor];

    
   
    
    self.introductionField.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.introductionField.layer.borderWidth =1.0;
    
    self.introductionField.layer.cornerRadius =5.0;
    [self.introductionField.layer setMasksToBounds:YES];
    [self.introductionField setTextAlignment:NSTextAlignmentLeft];//并设置左对齐
    
    self.introductionField.contentInset = UIEdgeInsetsMake(-50.0f, 2.0f, 5.0f, 4.0f);
   
    self.introductionField.contentSize = CGSizeMake(self.introductionField.contentSize.width,self.introductionField.frame.size.height);
    
   
       // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.introductionField.contentSize = CGSizeMake(self.introductionField.frame.size.width,self.introductionField.contentSize.height);
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

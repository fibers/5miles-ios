//
//  HGStartSystemStrAction.m
//  Pinnacle
//
//  Created by Alex on 15-4-9.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGStartSystemStrAction.h"
#import "HGAppDelegate.h"
#import "HGSearchResultsController.h"
#import "HGSearchViewController.h"
#import "HGUser.h"
#import "HGWebContentController.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGAppData.h"
#import "HGMessageCenterController.h"
#import "HGRatingsController.h"

#import "HGItemDetailController.h"

@implementation HGStartSystemStrAction



+ (HGStartSystemStrAction *)sharedInstance
{
    static HGStartSystemStrAction * _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [HGStartSystemStrAction new];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
-(void)startHomeBannerAction:(NSString*)actionString withController:(UIViewController*)controller withActionSource:(int)messagesource
{
    //actionString = @"c";
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([actionString isEqualToString:@"h"]) {
        [appDelegate.tabController setSelectedIndex:0];
        UINavigationController* Navi = appDelegate.tabController.viewControllers[0];
        [Navi popToRootViewControllerAnimated:YES];
    }
    if ([actionString isEqualToString:@"m"]) {
        [appDelegate.tabController setSelectedIndex:3];
        UINavigationController* Navi = appDelegate.tabController.viewControllers[3];
        [Navi popToRootViewControllerAnimated:YES];
        
    }
    if ([actionString isEqualToString:@"c"]) {
        
        [appDelegate.tabController setSelectedIndex:1];
        UINavigationController* Navi = appDelegate.tabController.viewControllers[1];
        [Navi popToRootViewControllerAnimated:YES];
        
    }
    if ([actionString hasPrefix:@"n"]) {
        //push to message center, with notification segment selected.
        UINavigationController* naviMessageCenter = [appDelegate.tabController setTab4SelectedStyle];
        
        HGMessageCenterController* messageController =  naviMessageCenter.viewControllers[0];
        messageController.bShowNotificationSegment = YES;
        [naviMessageCenter popToRootViewControllerAnimated:YES];
    }
    if ([actionString hasPrefix:@"r"] && [HGAppData sharedInstance].bOpenReviewFunctions) {
        //push to review list controller.
        
        if(messagesource==SYSTEM_ACTION_SOURCE_MESSAGECENTER)
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"notif_review" label:nil value:nil];
        }
        HGRatingsController *ratingControlelr = (HGRatingsController *)[[UIStoryboard profileStoryboard]instantiateViewControllerWithIdentifier:@"SellerRatings"];
        ratingControlelr.uid = [HGUserData sharedInstance].fmUserID;
        [controller.navigationController pushViewController:ratingControlelr animated:YES];
    }
    
    
    if ([actionString hasPrefix:@"c:"]) {
        //category
        NSString* valueString = [actionString substringFromIndex:2];
        
        
        NSString * title = @"";
        
      
         NSDictionary* dict = @{@"catID": [NSNumber numberWithInteger:valueString.integerValue], @"catTitle": title};
        
         HGSearchResultsController *vcDest = (HGSearchResultsController *)[[UIStoryboard searchStoryboard]instantiateViewControllerWithIdentifier:@"VCSearchResults"];
        vcDest.title = title;
        vcDest.searchMode = HGSearchModeCategory;
        vcDest.searchPayload = dict;
        
        [controller.navigationController pushViewController:vcDest animated:YES];
    }
    if ([actionString hasPrefix:@"s:"]) {
        //s:#{keyword}
        NSString* valueString = [actionString substringFromIndex:2];
        NSDictionary* dict = @{@"keyword":valueString};
        
        
        HGSearchResultsController *vcSearchResults = (HGSearchResultsController *)[[UIStoryboard searchStoryboard]instantiateViewControllerWithIdentifier:@"VCSearchResults"];
        vcSearchResults.searchMode = HGSearchModeKeyword;
        vcSearchResults.searchPayload =dict;
        vcSearchResults.title = valueString;
        
        [controller.navigationController pushViewController:vcSearchResults animated:YES];
        
      
        
    }
    if ([actionString hasPrefix:@"s2:"]) {
        //s2:#{keyword},cid:#{campain_id}
        NSArray* strings = [actionString componentsSeparatedByString:@","];
        NSString* keyString = [[strings objectAtIndex:0] substringFromIndex:3];
        NSString* cidString = [[strings objectAtIndex:1] substringFromIndex:4];
        
        NSDictionary* dict = @{@"keyword":keyString,@"cid":cidString};
        
        HGSearchResultsController *vcSearchResults = (HGSearchResultsController *)[[UIStoryboard searchStoryboard]instantiateViewControllerWithIdentifier:@"VCSearchResults"];
        vcSearchResults.searchMode = HGSearchModeKeyword;
        vcSearchResults.searchPayload =dict;
        vcSearchResults.title = keyString;
        
        [controller.navigationController pushViewController:vcSearchResults animated:YES];
    }

    
    
    if ([actionString hasPrefix:@"p:"]) {
        //p:#{user_id}
        if(messagesource==SYSTEM_ACTION_SOURCE_MESSAGECENTER)
        {
             [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"notif_follow" label:nil value:nil];
        }
        NSString* valueString = [actionString substringFromIndex:2];
        HGUser* user = [[HGUser alloc] init];
        user.uid = valueString;
        user.displayName = @"";
        user.portraitLink = @"";
        
        HGUserViewController *vcUser = (HGUserViewController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCUser"];
        vcUser.user = user;
        
        [controller.navigationController pushViewController:vcUser animated:YES];
        
    }
    
    if ([actionString hasPrefix:@"i:"]) {
        //i:#{item_id}
        if(messagesource==SYSTEM_ACTION_SOURCE_MESSAGECENTER)
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"notif_product" label:nil value:nil];
        }
        
        NSString* valueString = [actionString substringFromIndex:2];
        
        
        HGShopItem* item = [[HGShopItem alloc] init];
        item.uid = valueString;
        HGItemDetailController *vcItemDetail = (HGItemDetailController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCItemDetail"];
        vcItemDetail.item = item;
       
        [controller.navigationController pushViewController:vcItemDetail animated:YES];
        
    }
    if ([actionString hasPrefix:@"u:"]) {
        //u:#{url}
        NSString* valueString = [actionString substringFromIndex:2];
        HGWebContentController *vcWebContent = (HGWebContentController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCWebContent"];
        vcWebContent.contentLink = valueString;
        [controller.navigationController pushViewController:vcWebContent animated:YES];
    }
    
    if ([actionString hasPrefix:@"f:"]) {
        //f:fid (recommend items)
        NSString* valueString = [actionString substringFromIndex:2];
        [self startSystemThemeItem:valueString];
    }
   
    
    
}

-(void)startSystemThemeItem:(NSString*)valueString
{
    [HGAppData sharedInstance].recommend_themeID = valueString.intValue;
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController * sysThemeNavi = [storyBoard instantiateViewControllerWithIdentifier:@"systemThemeNavi"];
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    sysThemeNavi.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    [appDelegate.tabController presentViewController:sysThemeNavi animated:YES completion:nil];
}

@end

//
//  FMDBdatabaseReviewData.m
//  Pinnacle
//
//  Created by Alex on 15-4-20.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "FMDBdatabaseReviewData.h"
#import <FMDB.h>
@implementation FMDBdatabaseReviewData
static FMDatabase *_fmdb;
#define REVIEW_SQLITE_NAME @"reivews.sqlite"

+ (void)initDataBase {
    // 执行打开数据库和创建表操作
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:REVIEW_SQLITE_NAME];
    _fmdb = [FMDatabase databaseWithPath:filePath];
    
    [_fmdb open];
    
    //#warning 必须先打开数据库才能创建表。。。否则提示数据库没有打开
    [_fmdb executeUpdate:@"CREATE TABLE IF NOT EXISTS reviews_table( review_keystring TEXT NOT NULL,review_content TEXT NOT NULL, review_score TEXT NOT NULL);"];
}

+ (BOOL)insertReviews:(NSDictionary*)dict {
    NSString* review_keystring = [dict objectForKey:@"review_keystring"];
    NSString* review_content =[dict objectForKey: @"review_content"];
    NSString* review_score = [dict objectForKey:@"review_score"];
    
    NSString *insertSql = [NSString stringWithFormat:@"INSERT INTO reviews_table(review_keystring, review_content, review_score) VALUES ('%@', '%@', '%@');", review_keystring, review_content, review_score];
    return [_fmdb executeUpdate:insertSql];
}

+ (NSMutableDictionary *)getReview:(NSString *)review_keystring {
    
    if (review_keystring == nil) {
        return  nil;
    }
    NSString*queryString = [@"SELECT * FROM reviews_table where review_keystring = " stringByAppendingFormat:@"'%@'",review_keystring];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    FMResultSet *set = [_fmdb executeQuery:queryString];
    
    while ([set next]) {
        
        NSString *review_keystring = [set stringForColumn:@"review_keystring"];
        NSString *review_content = [set stringForColumn:@"review_content"];
        NSString *review_score = [set stringForColumn:@"review_score"];
        
        [dict setValue:review_keystring forKey:@"review_keystring"];
        [dict setValue:review_content forKey:@"review_content"];
        [dict setValue:review_score forKey:@"review_score"];
    }
    return dict;
}

+ (BOOL)deleteReview:(NSString *)review_keystring {
    
    if (review_keystring == nil) {
       
        return FALSE;
    }
    NSString*queryString = [@"delete * FROM reviews_table where review_keystring = " stringByAppendingFormat:@"'%@'",review_keystring];
    return [_fmdb executeUpdate:queryString];
    
}

+ (BOOL)updateReview:(NSDictionary *)dict {
    
    if (dict == nil) {
       return FALSE;
    }
    
    
    NSString* review_keystring = [dict objectForKey:@"review_keystring"];
    NSString* review_content =[dict objectForKey: @"review_content"];
    NSString* review_score = [dict objectForKey:@"review_score"];
    
    NSString *modifySql = [NSString stringWithFormat:@"INSERT INTO reviews_table(review_keystring, review_content, review_score) VALUES ('%@', '%@', '%@');", review_keystring, review_content, review_score];
    return [_fmdb executeUpdate:modifySql];
}
@end

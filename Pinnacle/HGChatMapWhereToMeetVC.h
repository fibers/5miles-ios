//
//  HGChatMapWhereToMeetVC.h
//  Pinnacle
//
//  Created by Alex on 15/5/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface HGChatMapWhereToMeetVC : UIViewController<MKMapViewDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, assign)CLLocationCoordinate2D POICoor;

@property (nonatomic, strong) NSString* POITitleString;
@property (nonatomic, strong) NSString* POIAddressString;
@end

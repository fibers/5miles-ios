//
//  HGUserListViewController.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGUser.h"
#import "HGResourceListController.h"

#import <JDStatusBarNotification.h>
#import "SVPullToRefresh.h"

@interface HGUserListViewController : HGResourceListController

@property (nonatomic, strong) NSString * userID;
@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;

@end

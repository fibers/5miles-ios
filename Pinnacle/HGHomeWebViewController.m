//
//  HGHomeWebViewController.m
//  Pinnacle
//
//  Created by mumuhou on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGHomeWebViewController.h"
#import "HGSegmentViewController.h"
#import "HGHomeChildViewControllerProtocol.h"
#import "HGSearchResultsController.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGConstant.h"
#import "HGHomeViewController.h"

static NSInteger kScrollViewTopMargin = 64 - 20;

@interface HGHomeWebViewController () <UIWebViewDelegate, HGSegmentSubViewControllerProtocol, HGHomeChildViewControllerProtocol>

@property (nonatomic, strong) UIWebView *webView;

@property (nonatomic, assign) BOOL needReload;

@property (nonatomic, assign) BOOL isObserveContentOffset;

@end

@implementation HGHomeWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.webView.scrollView.contentInset = UIEdgeInsetsMake(kScrollViewTopMargin, 0, 0, 0);
//    self.webView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    self.webView.backgroundColor = [UIColor whiteColor];
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
}

- (void)dealloc
{
    [self observeValueForContentOffset:NO];
}

- (void)loadUrl:(NSString *)url
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    
    NSString* preferredLang = [[HGUtils sharedInstance] getPreferLanguage];
    [request addValue:preferredLang forHTTPHeaderField:@"Accept-Language"];
    [self.webView loadRequest:request];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self observeValueForContentOffset:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self observeValueForContentOffset:NO];
}

- (void)observeValueForContentOffset:(BOOL)isAddObserver
{
    if (isAddObserver != self.isObserveContentOffset) {
        self.isObserveContentOffset = isAddObserver;
        if (isAddObserver) {
            [self.webView.scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:NULL];
        } else {
            [self.webView.scrollView removeObserver:self forKeyPath:@"contentOffset"];
        }
    }
}


#pragma mark- UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = request.URL;
    
    if ([url.scheme isEqualToString:@"fivemiles"]) {
        NSLog(@"%@", url);
        NSDictionary *parameters = [self parseParametersFromUrlQueryString:url.query];
        return [self javascriptResponsedWithHost:url.host parameters:parameters];
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.title = title;

    self.needReload = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    self.needReload = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshContent
{
    
}

- (void)refreshContentSilent
{

}

- (NSDictionary *)parseParametersFromUrlQueryString:(NSString *)queryString
{
    NSMutableDictionary *retDic = [[NSMutableDictionary alloc] init];
    
    if (queryString.length > 0) {
        NSArray *keyAndValueArray = [queryString componentsSeparatedByString:@"&"];
        if (keyAndValueArray && keyAndValueArray.count > 0) {
            for (NSString *keyAndValue in keyAndValueArray) {
                NSArray *kv = [keyAndValue componentsSeparatedByString:@"="];
                if (kv.count == 2) {
                    NSString *key = kv[0];
                    NSString *value = kv[1];
                    if (value.length > 0) {
                        [retDic setObject:value forKey:key];
                    }
                }
            }
        }
    }
    return retDic;
}

- (BOOL)javascriptResponsedWithHost:(NSString *)host parameters:(NSDictionary *)parameters
{
    if ([host isEqualToString:@"search-category"]) {
        NSInteger categoryId = [[parameters objectForKey:@"cat"] integerValue];
        NSString *title = [parameters objectForKey:@"title"];
        
        UIStoryboard *searchStoryboard = [UIStoryboard searchStoryboard];
        HGSearchResultsController *searchResultViewController = [searchStoryboard instantiateViewControllerWithIdentifier:@"VCSearchResults"];
        searchResultViewController.title = [title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        searchResultViewController.searchMode = HGSearchModeCategory;
        NSDictionary* dict = @{@"catID": @(categoryId), @"catTitle": title};
        searchResultViewController.searchPayload = dict;
        
        
        [self observeValueForContentOffset:NO];
        [self.delegate viewController:self navigationBarTransformProgress:0];
        
        [self.navigationController pushViewController:searchResultViewController animated:YES];
    }
    return NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:self.webView.scrollView] && [keyPath isEqualToString:@"contentOffset"]) {
        CGFloat offsetY = [[change valueForKey:NSKeyValueChangeNewKey] CGPointValue].y;
        [self notifyHomePageForContentOffsetDidChange:offsetY];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)notifyHomePageForContentOffsetDidChange:(CGFloat)offsetY
{
    offsetY += kScrollViewTopMargin;
    if (offsetY > 0) {
        if (offsetY > 44) {
            [self.delegate viewController:self navigationBarTransformProgress:1];
            
        } else {
            [self.delegate viewController:self navigationBarTransformProgress:offsetY / 44];
        }
    } else {
        [self.delegate viewController:self navigationBarTransformProgress:0];
    }
}


#pragma mark- HGSegmentSubViewControllerProtocol

- (void)segmentedPageShown:(BOOL)shown
{
    self.webView.scrollView.scrollsToTop = shown;
    
    CGFloat headerOffsetY = [self.delegate headerViewOffset];
    [self.webView.scrollView setContentOffset:CGPointMake(0, headerOffsetY - kScrollViewTopMargin) animated:NO];
    
    if (self.needReload) {
        [self.webView reload];
    }
}



@end

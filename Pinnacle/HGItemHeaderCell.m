//
//  HGItemHeaderCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemHeaderCell.h"
#import "HGUser.h"
#import <QuartzCore/QuartzCore.h>
#import "HGShopItem.h"
#import "HGItemDetail.h"
#import <MHPrettyDate/MHPrettyDate.h>
@interface HGItemHeaderCell () <AVAudioPlayerDelegate>
@end
@implementation HGItemHeaderCell
{
    NSURL * _mediaURL;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _commonSetup];
}


////huangzf: 这个文件和home界面的 item detail 有关: item detail view 1
- (void)_commonSetup
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.imageViewer = [[X4ImageViewer alloc] initWithFrame:CGRectMake(0, IMAGEPAGE_Y_OFFSET, CGRectGetWidth(self.bounds), CGRectGetWidth(self.bounds))];
    self.imageViewer.contentMode = ContentModeAspectFill;
    self.imageViewer.bZoomEnable = NO;
    self.imageViewer.carouselType = CarouselTypePageControl;
    self.imageViewer.carouselPosition = CarouselPositionBottomCenter;
    [self.imageViewer setPageControlCurrentIndicatorImage:[UIImage imageNamed:@"pageControl-active"]];
    [self.imageViewer setPageControlIndicatorImage:[UIImage imageNamed:@"pageControl-inactive"]];
    [self.contentView addSubview:self.imageViewer];
    
    self.soldIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-sold"]];
    self.soldIconView.hidden = YES;
    [self.contentView addSubview:self.soldIconView];
    
    self.unNormalStateIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"new_icon_Unavailable"]];
    self.unNormalStateIcon.hidden = YES;
    [self.contentView addSubview:self.unNormalStateIcon];
    
    self.itemNewIcon = [[UIImageView alloc] initWithFrame:CGRectMake(self.imageViewer.frame.size.width - 10 - 92/2, 10, 92/2, 62/2)];
    self.itemNewIcon.image = [UIImage imageNamed:@"home_item_new"];
    [self.imageViewer addSubview:self.itemNewIcon];
    
    
    self.userView = [[HGUserHoverView alloc] initWithFrame:CGRectZero];
    [self.userView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapUserBanner:)]];
    [self addSubview:self.userView];
    self.userDisplayName = [[UILabel alloc] initWithFrame:CGRectZero];
    self.userDisplayName.textAlignment = NSTextAlignmentLeft;
    self.userDisplayName.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.userDisplayName.font = [UIFont fontWithName:@"Helvetica-Regular" size:15];
    [self addSubview:self.userDisplayName];
    [self addRatingView];
    
    self.blackview = [[HGNoTouchActionView alloc] initWithFrame:CGRectMake(0, 0, self.imageViewer.frame.size.width, self.imageViewer.frame.size.height)];
    self.blackview.responseView = self.imageViewer;
    
    self.blackview.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.35];
    self.blackview.hidden = YES;
    [self.imageViewer addSubview:self.blackview];
    
    
    self.canvasView = [HGItemCanvasView new];
    self.canvasView.opaque = YES;
    self.canvasView.ownerCell = self;
    self.canvasView.backgroundColor = self.contentView.backgroundColor;
    [self.contentView addSubview:self.canvasView];
    
    
    
    
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    [self addDeliveryView];
    
    [self addAudioView];
    [self addBrandButton];
    [self addUserActionView];
    [self addLocationView];
    [self addCreateTimeView];
    [self addMapview];
    [self addPriceView];
}
-(void)addRatingView
{
    self.UserRatingView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 75, 6.4*1.5)];
    self.UserRatingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    self.UserRatingLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.UserRatingLabel.font = [UIFont systemFontOfSize:12];
    
    //[self addSubview:self.UserRatingLabel];
    [self addSubview:self.UserRatingView];
    
}
static int priceViewHeight = 40;
-(void)addPriceView
{
    self.priceView = [[UIView alloc] initWithFrame:CGRectMake(8, 57 + 3, self.frame.size.width, priceViewHeight)];
    self.originalPrice = [[StrikeThroughLabel alloc] initWithFrame:CGRectMake(120, 3, 120, priceViewHeight)];
    self.currentPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, priceViewHeight)];
    
    self.currentPrice.textColor = FANCY_COLOR(@"ff8830");
    self.currentPrice.font = [UIFont boldSystemFontOfSize:18];
    
    self.originalPrice.strikeColor = self.originalPrice.textColor = FANCY_COLOR(@"818181");
    self.originalPrice.strikeThroughEnabled = YES;
    self.originalPrice.font = [UIFont systemFontOfSize:12];
    
    
    
    [self.priceView addSubview:self.originalPrice];
    [self.priceView addSubview:self.currentPrice];
    
    [self.canvasView addSubview:self.priceView];
}

static int userActionHeight = 50;
-(void)addUserActionView
{
    const CGFloat kLikeButtonWidth = 40.f;
    const CGFloat kAskButtonWidth = 106;
    const CGFloat kSpacing = 8;
    
    self.userActionView = [[UIView alloc] initWithFrame:CGRectMake(0, 12, self.frame.size.width, userActionHeight)];
    self.button_like = [[UIButton alloc] initWithFrame:CGRectMake(kSpacing, 0, kLikeButtonWidth, kLikeButtonWidth)];
    self.button_like.layer.cornerRadius = 4;
    self.button_like.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
    self.button_like.layer.borderWidth = 1;
    [self.userActionView addSubview:self.button_like];
    
    
    self.button_renew = [[UIButton alloc] initWithFrame:CGRectMake(8, 0, 104, 40)];
    self.button_renew.layer.cornerRadius = 4;
    self.button_renew.layer.borderWidth = 1;
    self.button_renew.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    [self.userActionView addSubview:self.button_renew];
    [self.button_renew setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    self.button_renew.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.button_renew setTitle:NSLocalizedString(@"Renew",nil) forState:UIControlStateNormal];
    [self.button_renew addTarget:self action:@selector(handleTapRenewButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.button_edit = [[UIButton alloc] initWithFrame:CGRectMake(8 + 104+10, 0, SCREEN_WIDTH-130, 40)];
    self.button_edit.layer.cornerRadius = 4;
    self.button_edit.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
    self.button_edit.layer.borderWidth = 1;
    self.button_edit.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.button_edit setTitleColor:SYSTEM_DEFAULT_FONT_COLOR_1 forState:UIControlStateNormal];
    [self.button_edit setTitle:NSLocalizedString(@"Edit",nil) forState:UIControlStateNormal];
    [self.userActionView addSubview:self.button_edit];
    
    self.editButton_icon = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.button_edit.frame)/2-34, 11, 34/2, 34/2)];
    self.editButton_icon.image = [UIImage imageNamed:@"item_edit"];
    [self.button_edit addSubview:self.editButton_icon];
    
    CGFloat makeOfferButtonWidth = SCREEN_WIDTH - 4 * kSpacing - kLikeButtonWidth - kAskButtonWidth;
    
    self.button_buy= [[UIButton alloc] initWithFrame:CGRectMake(kSpacing * 2 + kLikeButtonWidth, 0, makeOfferButtonWidth, 40)];
    self.button_ask = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - kAskButtonWidth - kSpacing, 0, kAskButtonWidth, 40)];

    [self.userActionView addSubview:self.button_ask];
    [self.userActionView addSubview:self.button_buy];
    self.button_buy.backgroundColor = [UIColor whiteColor];
    self.button_buy.layer.cornerRadius = 4.0;
    self.button_buy.layer.borderWidth = 1;
    self.button_buy.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    
    [self.button_buy setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    self.button_buy.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.button_buy setTitle:NSLocalizedString(@"Make offer", nil) forState:UIControlStateNormal];
    
    self.button_ask.backgroundColor = [UIColor whiteColor];
    self.button_ask.layer.cornerRadius = 4.0;
    self.button_ask.layer.borderWidth = 1;
    self.button_ask.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
    [self.button_ask setTitleColor:SYSTEM_DEFAULT_FONT_COLOR_1 forState:UIControlStateNormal];
    self.button_ask.titleLabel.font = [UIFont systemFontOfSize:17];
    
    self.button_askIcon = [[UIImageView alloc] initWithFrame:CGRectMake(20, 12, 44/2, 32/2)];
    self.button_askIcon.image = [UIImage imageNamed:@"item_detail_ask_icon"];
    
    if ([[[HGUtils sharedInstance] getPreferLanguage] isEqualToString:@"en"]
        || [[[HGUtils sharedInstance] getPreferLanguage] isEqualToString:@"zh-Hans"]) {
        ///only English shows the icon
        const CGFloat spacing = 6.f;
        [self.button_ask setImage:[UIImage imageNamed:@"item_detail_ask_icon"] forState:UIControlStateNormal];
        [self.button_ask setTitle:@"Ask" forState:UIControlStateNormal];
        self.button_ask.titleEdgeInsets = UIEdgeInsetsMake(0, spacing / 2, 0, -spacing / 2);
        self.button_ask.imageEdgeInsets = UIEdgeInsetsMake(0, -spacing / 2, 0, spacing / 2);
        
//        [self.button_ask addSubview:self.button_askIcon];
//        [self.button_ask setTitle:NSLocalizedString(@"    Ask",nil) forState:UIControlStateNormal];
    }
    else{
        [self.button_ask setTitle:@"Ask" forState:UIControlStateNormal];
//        NSString* titleString = NSLocalizedString(@"    Ask",nil);
//        titleString = [titleString stringByReplacingOccurrencesOfString:@" " withString:@""];
//        [self.button_ask setTitle:titleString forState:UIControlStateNormal];
    }
    
    
    
    
    [self.canvasView addSubview:self.userActionView];
    
    
    self.button_sold = [[UIButton alloc] initWithFrame:CGRectMake(8, 0, SCREEN_WIDTH-16, 40)];
    self.button_sold.layer.cornerRadius = 4;
    self.button_sold.layer.borderWidth=1;
    self.button_sold.layer.borderColor = FANCY_COLOR(@"f0f0f0").CGColor;
    [self.userActionView addSubview:self.button_sold];
    self.button_sold.hidden = YES;
    [self.button_sold setTitleColor:FANCY_COLOR(@"b9b9b9") forState:UIControlStateNormal];
    self.button_sold.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.button_sold setTitle:NSLocalizedString(@"Sold out",nil) forState:UIControlStateNormal];
    
    
    self.userActionView.hidden = YES;
    [self configUserActionView];
}
-(void)configUserActionView
{
    [self.button_like setImage:[UIImage imageNamed:@"item_detail_like"] forState:UIControlStateNormal];
    [self.button_edit addTarget:self action:@selector(handleTapEditButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.button_like addTarget:self action:@selector(handleTapLike:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.button_ask addTarget:self action:@selector(handleTapAsk:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.itemDetail.state >= HGItemStateOfferAccepted ) {
        self.button_buy.backgroundColor = [UIColor grayColor];
    }
    else{
        [self.button_buy addTarget:self action:@selector(handleTapMakeOffer:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
}
-(void)addBrandButton
{
    self.brandButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.imageViewer.frame.size.height+IMAGEPAGE_Y_OFFSET - 20, 60, 24)];
    self.brandButton.titleLabel.textColor = [UIColor whiteColor];
    self.brandButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"item_detail_brand_head"]];
    self.brandButton.titleLabel.font = [UIFont fontWithName:FONT_TYPE_1 size :12];
    [self.brandButton addTarget:self action:@selector(handleTapBrandString:)forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.brandButton];
    
    self.brandEndIcon = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.brandButton.frame), CGRectGetMinY(self.brandButton.frame), 32/2, 48/2)];
    self.brandEndIcon.image = [UIImage imageNamed:@"item_detail_brand_end"];
    [self addSubview:self.brandEndIcon];
    
}
-(void)addAudioView
{
    self.audioView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, ITEM_DETAIL_AUDIO_HEIGHT)];
    [self.contentView addSubview:self.audioView];
    
    self.audioView_grayArea = [[UIView alloc] initWithFrame:CGRectMake(40+8+7, 0,self.frame.size.width - 66, ITEM_DETAIL_AUDIO_HEIGHT)];
    self.audioView_grayArea.backgroundColor = FANCY_COLOR(@"f8f8f8");
    self.audioView_grayArea.layer.cornerRadius = 3.0;
    [self.audioView addSubview:self.audioView_grayArea];
    
    self.btnPlaySound = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnPlaySound.enabled = NO;
    
    self.btnPlaySound.layer.cornerRadius = 4;
    self.btnPlaySound.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
    self.btnPlaySound.layer.borderWidth = 1;
    
    [self.btnPlaySound setBackgroundImage:[UIImage imageNamed:@"audio_play"] forState:UIControlStateNormal];
    UIImage* tImage = [UIImage imageNamed:@"audio_play"];
    UIImage* tintedImage = [UIImage getTintedImage:tImage withColor:[UIColor grayColor]];
    [self.btnPlaySound setBackgroundImage:tintedImage  forState:UIControlStateDisabled];
    
    
    [self.btnPlaySound sizeToFit];
    [self.btnPlaySound addTarget:self action:@selector(_playDownloadedMedia:) forControlEvents:UIControlEventTouchUpInside];
    self.btnPlaySound.frame = CGRectMake(8,
                                         0, 40, 40);
    [self.audioView addSubview:self.btnPlaySound];
    
    
    self.audioLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 25, ITEM_DETAIL_AUDIO_HEIGHT)];
    self.audioLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-100, 0, 40, ITEM_DETAIL_AUDIO_HEIGHT)];
    self.audioSlide = [[UISlider alloc] initWithFrame:CGRectMake(30,6, self.frame.size.width-132, 30)];
    //[self.audioSlide addTarget:self action:@selector(sliderChange:) forControlEvents:UIControlEventValueChanged];
    self.audioSlide.userInteractionEnabled = NO;
    
    
    self.audioLabel1.textAlignment = self.audioLabel2.textAlignment = NSTextAlignmentCenter;
    
    
    self.audioSlide.tintColor = SYSTEM_MY_ORANGE;
    
    
    [self.audioSlide setThumbImage:[UIImage imageNamed:@"slider_icon"] forState:UIControlStateNormal];
    [self.audioSlide setThumbImage:[UIImage imageNamed:@"slider_icon"] forState:UIControlStateHighlighted];
    //[self.audioSlide setMinimumTrackImage:[UIImage imageNamed:@"test.jpg"] forState:UIControlStateNormal];
    
    
    
    self.audioSlide.minimumValue = 0.0;
    self.audioLabel2.textColor = self.audioLabel1.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.audioLabel1.font = self.audioLabel2.font = [UIFont systemFontOfSize:12];
    [self.audioView_grayArea addSubview:self.audioLabel1];
    [self.audioView_grayArea addSubview:self.audioLabel2];
    [self.audioView_grayArea addSubview:self.audioSlide];
    
}

-(void)addDeliveryView
{
    self.deliveryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, ITEM_DETAIL_DELIVERY_HEIGHT)];
    
    self.deliveryICON = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 34/2, 34/2)];
    self.deliveryICON.image = [UIImage imageNamed:@"item_detail_delivery"];
    [self.deliveryView addSubview:self.deliveryICON];
    
    self.deliveryContent = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, self.frame.size.width-60-10, ITEM_DETAIL_DELIVERY_HEIGHT)];
    self.deliveryContent.textAlignment = NSTextAlignmentRight;
    [self.deliveryView addSubview:self.deliveryContent];
    
    self.deliveryTitle = [[UILabel alloc] initWithFrame:CGRectMake(8+47/2+7, 0, 200, ITEM_DETAIL_DELIVERY_HEIGHT)];
    self.deliveryTitle.textAlignment = NSTextAlignmentLeft;

    if(IPHONE6PLUS){
        self.deliveryTitle.font = [UIFont systemFontOfSize:13];
        self.deliveryContent.font = [UIFont systemFontOfSize:13];
    }
    else{
        self.deliveryTitle.font = [UIFont systemFontOfSize:12];
        self.deliveryContent.font = [UIFont systemFontOfSize:12];
    }

    self.deliveryTitle.text = NSLocalizedString(@"Delivery",nil);
    [self.deliveryView addSubview:self.deliveryTitle];
    
    
    self.deliverySeperator = [[UIView alloc] initWithFrame:CGRectMake(36, 0,self.frame.size.width, 1)];
    self.deliverySeperator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15] ;
    [self.deliveryView addSubview:self.deliverySeperator];
    
    
    
    [self.contentView addSubview:self.deliveryView];
}
-(void)addCreateTimeView
{
    self.createTimeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, ITEM_DETAIL_CREATETIME_HEIGHT)];
    
    self.createTimeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 34/2, 34/2)];
    self.createTimeIcon.image = [UIImage imageNamed:@"item_detail_time_icon"];
    [self.createTimeView addSubview:self.createTimeIcon];
    
    
    self.createTimeTitle = [[UILabel alloc] initWithFrame:CGRectMake(8+47/2+7,  10, 200, ITEM_DETAIL_DELIVERY_HEIGHT)];
    self.createTimeTitle.textAlignment = NSTextAlignmentLeft;
    
    
    [self.createTimeView addSubview:self.createTimeTitle];
    self.createTimeTitle.text = NSLocalizedString(@"Update time",nil);
    
    self.createTimeContent = [[UILabel alloc] initWithFrame:CGRectMake(60,  10, self.frame.size.width-60-10, ITEM_DETAIL_DELIVERY_HEIGHT)];
    self.createTimeContent.textAlignment = NSTextAlignmentRight;
    
    if(IPHONE6PLUS)
    {
        self.createTimeTitle.font = [UIFont systemFontOfSize:13];
        self.createTimeContent.font = [UIFont systemFontOfSize: 13];
    }
    else{
        self.createTimeTitle.font = [UIFont systemFontOfSize:12];
        self.createTimeContent.font = [UIFont systemFontOfSize: 12];
    }
    
    [self.createTimeView addSubview:self.createTimeContent];
    self.createTimeContent.text = @"";
    
    self.createTimeSeperator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 10)];
    self.createTimeSeperator.backgroundColor = FANCY_COLOR(@"f0f0f0") ;
    [self.createTimeView addSubview:self.createTimeSeperator];
    
    [self.contentView addSubview:self.createTimeView];
}
-(void)addLocationView
{
    self.locationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, ITEM_DETAIL_LOCATION_HEIGHT)];
    [self.contentView addSubview:self.locationView];
    self.locationIcon =[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 34/2, 34/2)];
    self.locationIcon.image = [UIImage imageNamed:@"item_detail_location"];
    [self.locationView addSubview:self.locationIcon];
    
    self.locationTitle = [[UILabel alloc] initWithFrame:CGRectMake(8+47/2+7, 0, 100, ITEM_DETAIL_LOCATION_HEIGHT)];
    self.locationTitle.textAlignment = NSTextAlignmentLeft;
    [self.locationView addSubview:self.locationTitle];
    self.locationTitle.text = NSLocalizedString(@"Location",nil);
    
    self.locationContent = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, self.frame.size.width-90-10, ITEM_DETAIL_DELIVERY_HEIGHT)];
    self.locationContent.textAlignment = NSTextAlignmentRight;
    
    if(IPHONE6PLUS)
    {
        self.locationTitle.font = [UIFont systemFontOfSize:13];
        self.locationContent.font = [UIFont systemFontOfSize:13];
    }
    else{
        self.locationTitle.font = [UIFont systemFontOfSize:12];
        self.locationContent.font = [UIFont systemFontOfSize:12];
    }
    [self.locationView addSubview:self.locationContent];
    
    self.locationSeperator = [[UIView alloc] initWithFrame:CGRectMake(36, 0,self.frame.size.width, 1)];
    self.locationSeperator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15] ;
    [self.locationView addSubview:self.locationSeperator];
    
    self.locationView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapButtonLocation:)];
    [self.locationView addGestureRecognizer:tapGesture];
    
    
}
-(void)addMapview
{
    self.staticMapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 120)];
    [self.staticMapView addSubview:self.mapImage];
    self.staticMapView.clipsToBounds =YES;
    self.mapImage = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 120)];
    self.mapImage.backgroundColor = FANCY_COLOR(@"f8f8f8");
    [self.mapImage addTarget:self action:@selector(handleTapButtonLocation:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:self.staticMapView];
    
    
}
-(void)configBuyerView
{
    //buyer view.
    self.button_edit.hidden = YES;
    self.button_renew.hidden = YES;
    self.button_like.hidden = NO;
    self.button_ask.hidden = NO;
    self.button_buy.hidden = NO;
    if (self.itemDetail.state != HGItemStateListing) {
        self.soldIconView.hidden = NO;
        self.blackview.hidden = NO;
    }else{
        //listing status
        self.button_sold.hidden = YES;
        self.soldIconView.hidden = YES;
        self.unNormalStateIcon.hidden = YES;
        self.blackview.hidden = YES;
    }
    
    
    [self.button_buy addTarget:self action:@selector(handleTapMakeOffer:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.itemDetail.liked) {
        [self.button_like setImage:[UIImage imageNamed:@"item_detail_already_liked"] forState:UIControlStateNormal];
    }
    else{
        [self.button_like setImage:[UIImage imageNamed:@"item_detail_like"] forState:UIControlStateNormal];
    }
    
    if (self.itemDetail.state != ITEM_STATE_LISTING) {
        self.button_buy.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
        self.button_buy.userInteractionEnabled = NO;
        [self.button_buy setTitleColor:FANCY_COLOR(@"b9b9b9") forState:UIControlStateNormal];
        
        self.button_ask.userInteractionEnabled = NO;
        [self.button_ask setTitleColor:FANCY_COLOR(@"b9b9b9") forState:UIControlStateNormal];
        UIImage* originImage =[UIImage imageNamed:@"item_detail_ask_icon"];
        UIImage* tintedImage = [UIImage getTintedImage:originImage withColor:FANCY_COLOR(@"b9b9b9")];
        self.button_askIcon.image =tintedImage;
        [self.button_ask setImage:tintedImage forState:UIControlStateNormal];

        
    }
    if (self.itemDetail.state == ITEM_STATE_SOLD)
    {
        
    }
    if (self.itemDetail.state == ITEM_STATE_UNAPPROVED) {
        self.soldIconView.hidden = YES;
        self.unNormalStateIcon.hidden =NO;
        self.unNormalStateIcon.image = [UIImage imageNamed:@"new_icon_Unapproved"];
        
    }
    if (self.itemDetail.state == ITEM_STATE_UNAVAILABLE) {
        self.soldIconView.hidden = YES;
        self.unNormalStateIcon.hidden =NO;
        self.unNormalStateIcon.image = [UIImage imageNamed:@"new_icon_Unavailable"];
        
    }
    if (self.itemDetail.state == ITEM_STATE_UNLISTED) {
        self.soldIconView.hidden = YES;
        self.unNormalStateIcon.hidden = NO;
        self.unNormalStateIcon.image = [UIImage imageNamed:@"new_icon_Unlist"];
    }
    if (self.itemDetail.state == ITEM_STATE_PENDING) {
        self.soldIconView.hidden = YES;
        self.unNormalStateIcon.hidden = NO;
        self.unNormalStateIcon.image = [UIImage imageNamed:@"new_icon_apending"];
    }
    
}

-(void)configSellerView
{
    //seller view...
    self.button_edit.hidden = NO;
    self.button_renew.hidden = NO;
    self.button_like.hidden =YES;
    self.button_ask.hidden = YES;
    self.button_buy.hidden =YES;
    if (self.itemDetail.state != HGItemStateListing)
    {
        self.button_edit.hidden =YES;
        self.button_renew.hidden = YES;
        self.button_sold.hidden = NO;
        self.soldIconView.hidden = NO;
        self.blackview.hidden = NO;
        
    }else{
        //listing status
        self.button_sold.hidden = YES;
        self.soldIconView.hidden = YES;
        self.blackview.hidden = YES;
        self.unNormalStateIcon.hidden = YES;
    }
    
    if (self.itemDetail.state == ITEM_STATE_UNLISTED) {
        self.button_renew.hidden = NO;
        //[self DimRenewButton];
        self.button_sold.hidden = YES;
        self.soldIconView.hidden = YES;
        self.button_edit.hidden = NO;
        self.unNormalStateIcon.hidden = NO;
        self.unNormalStateIcon.image = [UIImage imageNamed:@"new_icon_Unlist"];
    }
    if (self.itemDetail.state == ITEM_STATE_UNAPPROVED)
    {
        self.soldIconView.hidden = YES;
        self.unNormalStateIcon.hidden =NO;
        self.unNormalStateIcon.image = [UIImage imageNamed:@"new_icon_Unapproved"];
        
        
        self.button_sold.hidden = YES;
        self.button_renew.hidden = NO;
        [self DimRenewButton];
        self.button_edit.hidden = NO;
        
        if(self.itemDetail.deletable == 0)
        {
            self.button_edit.userInteractionEnabled = NO;
            self.button_edit.titleLabel.textColor = FANCY_COLOR(@"b9b9b9");
        }
        
    }
    if (self.itemDetail.state == ITEM_STATE_UNAVAILABLE)
    {
        self.button_sold.hidden = YES;
        self.button_renew.hidden = NO;
        [self DimRenewButton];
        self.button_edit.hidden = NO;
        
        self.button_edit.userInteractionEnabled = NO;
        [self.button_edit setTitleColor:FANCY_COLOR(@"b9b9b9") forState:UIControlStateNormal];
        
        self.editButton_icon.image = [UIImage getTintedImage:[UIImage imageNamed:@"item_edit"] withColor:FANCY_COLOR(@"b9b9b9")];
        
        self.soldIconView.hidden = YES;
        self.unNormalStateIcon.hidden =NO;
        self.unNormalStateIcon.image = [UIImage imageNamed:@"new_icon_Unavailable"];
        
    }
    if (self.itemDetail.state == ITEM_STATE_SOLD) {
        self.unNormalStateIcon.hidden = YES;
        
        //reopen the sold edit : v3.0
        self.button_sold.hidden = YES;
        self.button_renew.hidden = NO;
        [self DimRenewButton];
        self.button_edit.hidden = NO;
    }
    if (self.itemDetail.state == ITEM_STATE_PENDING) {
        self.soldIconView.hidden = YES;
        self.unNormalStateIcon.hidden =NO;
        self.unNormalStateIcon.image = [UIImage imageNamed:@"new_icon_apending"];
        
        
        self.button_sold.hidden = YES;
        self.button_renew.hidden = NO;
//        [self DimRenewButton];
        self.button_edit.hidden = NO;
    }
    
    [self.button_buy removeTarget:self action:@selector(handleTapMakeOffer:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)DimRenewButton
{
    self.button_renew.userInteractionEnabled = NO;
    self.button_renew.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
    self.button_renew.titleLabel.textColor = FANCY_COLOR(@"b9b9b9");
}
- (void)layoutSubviews
{
    self.contentView.frame = self.bounds;
    
    self.userView.frame = CGRectMake(8, 10, CGRectGetWidth(self.userView.bounds), CGRectGetHeight(self.userView.bounds));
    self.userDisplayName.frame = CGRectMake(57+8, 8, 160, 21);
    [self.userDisplayName sizeToFit];
    
    self.UserRatingView.frame = CGRectMake(CGRectGetMaxX(self.userDisplayName.frame)+8, 13, self.UserRatingView.frame.size.width, self.UserRatingView.frame.size.height);
    self.UserRatingLabel.frame = CGRectMake(CGRectGetMaxX(self.UserRatingView.frame)+8, 8, 30, 20);
    
    
    self.blackview.frame =CGRectMake(0, 0, self.imageViewer.frame.size.width, self.imageViewer.frame.size.height);
    
    self.imageViewer.frame = CGRectMake(0, IMAGEPAGE_Y_OFFSET, CGRectGetWidth(self.bounds), CGRectGetWidth(self.bounds));
    
    self.itemNewIcon.frame =CGRectMake(self.imageViewer.frame.size.width - 10 - 92/2, 10, 92/2, 62/2);
    self.canvasView.frame = CGRectMake(0,
                                       CGRectGetMaxY(self.imageViewer.frame),
                                       CGRectGetWidth(self.frame),
                                       CGRectGetHeight(self.bounds) - CGRectGetHeight(self.imageViewer.frame)-IMAGEPAGE_Y_OFFSET);
    
    
    NSString* brandString = @"";
    if (self.itemDetail.brand_name != nil) {
        brandString = [@"   " stringByAppendingString:self.itemDetail.brand_name];
    }
    CGRect brandButtonRect = [brandString boundingRectWithSize: CGSizeMake(CGFLOAT_MAX, 24) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:NULL];
    float brandButtonWidth = brandButtonRect.size.width > 180 ? 180:brandButtonRect.size.width;
    self.brandButton.frame = CGRectMake(0, self.imageViewer.frame.size.height+IMAGEPAGE_Y_OFFSET - 24 - 30 +1, brandButtonWidth, 24);
    self.brandEndIcon.frame = CGRectMake(CGRectGetMaxX(self.brandButton.frame), CGRectGetMinY(self.brandButton.frame), 32/2, 48/2);
    
    
    
    if ([self.itemDetail isMine]) {
        [self configSellerView];
    }
    else{
        [self configBuyerView];
    }
    
    self.soldIconView.frame = CGRectMake(CGRectGetMaxX(self.imageViewer.frame) - 70, 10, 100, 100);
    self.soldIconView.center = self.imageViewer.center;
    
    
//    self.unNormalStateIcon.frame = CGRectMake(0, 0, 205, 47);
    self.unNormalStateIcon.center = self.imageViewer.center;
    
    int mapDisplayHeight = ITEM_DETAIL_MAP_HEIGHT;
    if (self.itemDetail.geoLocation.lat != 0 && self.itemDetail.geoLocation.lon != 0) {
    }
    else{
        mapDisplayHeight = 0;
    }
    
    self.audioView.frame = CGRectMake(0,
                                      self.bounds.size.height - ITEM_DETAIL_DELIVERY_HEIGHT - ITEM_DETAIL_CREATETIME_HEIGHT-ITEM_DETAIL_LOCATION_HEIGHT-mapDisplayHeight-12 - self.audioView.frame.size.height,
                                      self.audioView.frame.size.width,
                                      self.audioView.frame.size.height);
    
    
    self.createTimeView.frame = CGRectMake(0, self.bounds.size.height-ITEM_DETAIL_DELIVERY_HEIGHT - ITEM_DETAIL_CREATETIME_HEIGHT-ITEM_DETAIL_LOCATION_HEIGHT-mapDisplayHeight, self.deliveryView.frame.size.width, self.deliveryView.frame.size.height);
    self.deliveryView.frame = CGRectMake(0, self.bounds.size.height-ITEM_DETAIL_DELIVERY_HEIGHT-ITEM_DETAIL_LOCATION_HEIGHT - mapDisplayHeight, self.deliveryView.frame.size.width, self.deliveryView.frame.size.height);
    self.locationView.frame = CGRectMake(
                                         0,self.bounds.size.height - ITEM_DETAIL_LOCATION_HEIGHT - mapDisplayHeight,
                                         self.locationView.frame.size.width, self.locationView.frame.size.height);
    if (self.itemDetail.geoLocation.lat != 0 && self.itemDetail.geoLocation.lon != 0) {
        self.staticMapView.hidden = NO;
        self.staticMapView.frame = CGRectMake(
                                              0,self.bounds.size.height - mapDisplayHeight,
                                              self.mapImage.frame.size.width, self.mapImage.frame.size.height);
        [self.staticMapView addSubview:self.mapImage];
        
        
        
    }
    else{
        self.staticMapView.hidden = YES;
    }
}

- (void)dealloc
{
    //[super dealloc];
    [self.audioPlayer stop];
    self.audioPlayer = nil;
}

- (void)prepareForReuse
{
    [self.audioPlayer stop];
    self.audioPlayer = nil;
}

-(void)stopPlaying
{
    [self.audioPlayer stop];
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"record_stop" label:nil value:nil];
    
    
    
    UIImage* image = [UIImage imageNamed:@"audio_play"];
    [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
    [self.btnPlaySound addTarget:self action:@selector(_playDownloadedMedia:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)configWithItemDetail:(HGItemDetail *)itemDetail
{
    if (!itemDetail) {
        return;
    }
    if(itemDetail.seller.uid != nil && itemDetail.seller.uid.length > 0)
    {
        self.userActionView.hidden = NO;
    }
    self.itemDetail = itemDetail;
    
    // Set item pictures
    NSMutableArray * imageLinks = [@[] mutableCopy];
    for (HGItemImage * itemImage in self.itemDetail.images) {
        NSString* linkString = itemImage.imageLink;
        [imageLinks addObject:[NSURL URLWithString:linkString]];
    }
    
    [self.imageViewer setImages:imageLinks withPlaceholder:[UIImage imageNamed:@"item-new-placeholder"]];
    //    self.imageViewer.imageLinks = imageLinks;
    //    [self.imageViewer reloadData];
    if (itemDetail.is_new == 1) {
        self.itemNewIcon.hidden = NO;
    }
    else{
        self.itemNewIcon.hidden = YES;
    }
    
    
    
    // Config user hover view
    self.userView.user = itemDetail.seller;
    self.userDisplayName.text = self.userView.user.displayName;
    
    if (itemDetail.review_num != nil && itemDetail.review_score != nil) {
        
        NSString* image_index = [[HGUtils sharedInstance] getReviewImageNameByScore:itemDetail.review_score.floatValue];
        self.UserRatingView.image = [UIImage imageNamed:image_index];
        self.UserRatingLabel.text = [@"" stringByAppendingFormat:@"(%@)", itemDetail.review_num];
        
        if([HGAppData sharedInstance].bOpenReviewFunctions)
        {
            self.UserRatingLabel.hidden = self.UserRatingView.hidden = NO;
        }
        else{
            self.UserRatingLabel.hidden = self.UserRatingView.hidden = YES;
        }
    }
    else{
        self.UserRatingLabel.hidden = self.UserRatingView.hidden = YES;
    }
    
    
    if (itemDetail.seller!=nil) {
        self.currentPrice.text = [itemDetail priceForDisplay];
        [self.currentPrice sizeToFit];
    }
    
    
    self.originalPrice.text = [itemDetail originalPriceForDisplay];
    [self.originalPrice sizeToFit];
    self.originalPrice.frame = CGRectMake(self.currentPrice.frame.size.width + 5, self.originalPrice.frame.origin.y, self.originalPrice.frame.size.width, self.originalPrice.frame.size.height);
    
    
    // Set location button title
    NSString *dist;
    NSString* distance = [self.itemDetail formatDistance];
    NSString* address = [self.itemDetail formatAddress];
    
    if( distance.length > 0 && address.length > 0){
        dist = [NSString stringWithFormat:NSLocalizedString(@"%1$@ away in %2$@",nil), distance, address];
    }else if( distance.length > 0 && address.length == 0){
        dist = [NSString stringWithFormat:NSLocalizedString(@"%@ away",nil), distance];
    }else if( distance.length == 0 && address.length > 0){
        dist = [NSString stringWithFormat:@"%@", address];
    }else{
        dist = NSLocalizedString(@"unknown", nil);
    }
    
    self.locationContent.text = dist;
    
    if (self.itemDetail.mediaLink.length > 0)
    {
        self.audioView.hidden = NO;
    }
    else{
        self.audioView.hidden = YES;
    }
    // Config play button
    self.btnPlaySound.hidden = itemDetail.mediaLink.length == 0;
    if (!self.btnPlaySound.hidden) {
        [self _startDownloadMedia];
    }
    
    
    if (itemDetail.shipping_method >=1 && itemDetail.shipping_method <=3) {
        self.deliveryContent.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        if (itemDetail.shipping_method == 1) {
            self.deliveryContent.text = NSLocalizedString(@"Local exchange",nil);
        }
        if (itemDetail.shipping_method == 2) {
            self.deliveryContent.text = NSLocalizedString(@"Shipping", nil);
        }
        if (itemDetail.shipping_method == 3) {
            self.deliveryContent.text = NSLocalizedString(@"Local exchange or shipping",nil);
        }
    }
    else{
        //error
        self.deliveryContent.text =  NSLocalizedString(@"Local exchange or shipping",nil);
        
    }
    
    
    if (itemDetail.brand_name != nil && itemDetail.brand_name.length >0) {
        NSString* branddisplayString = [itemDetail.brand_name stringByAppendingString:@" >"];
        [self.brandButton setTitle:branddisplayString forState:UIControlStateNormal];
        self.brandButton.hidden = NO;
        self.brandEndIcon.hidden = NO;
    }
    else{
        self.brandButton.hidden = YES;
        self.brandEndIcon.hidden =YES;
    }
    
    self.createTimeContent.text = [[HGUtils sharedInstance] prettyDate:itemDetail.updated_at];
    if (itemDetail.renew_ttl > 0) {
        [self disableUpdateButton];
    }
    [self setNeedsLayout];
    // Refresh canvas view which displays price, title, desc, etc...
    [self.canvasView setNeedsDisplay];
}
- (void)_startDownloadMedia
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:self.itemDetail.mediaLink];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response)
                                              {
                                                  NSURL *directoryURL = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
                                                  NSString * filename = [URL lastPathComponent];
                                                  if ([URL pathExtension].length == 0) {
                                                      filename = [filename stringByAppendingString:@".m4a"];
                                                  }
                                                  //NSError *error;
                                                  
                                                  
                                                  
                                                  /*@try {
                                                   self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:_mediaURL error:&error];
                                                   if ([self.audioPlayer prepareToPlay]) {
                                                   self.audioLabel2.text =[ @"" stringByAppendingFormat:@"%.0f'",ceil(self.audioPlayer.duration) ];
                                                   self.audioLabel1.text = @"0'";
                                                   self.audioSlide.maximumValue = (float)self.audioPlayer.duration;
                                                   }
                                                   
                                                   }
                                                   @catch (NSException *exception) {
                                                   [Flurry logError: [@"" stringByAppendingFormat:@"%s ,%d", __FILE__,__LINE__] message:@"Exception" exception:exception] ;
                                                   
                                                   NSLog(@"audio prepare may fail");
                                                   }*/
                                                  
                                                  return [directoryURL URLByAppendingPathComponent:filename];
                                              } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                                                  NSLog(@"File downloaded to: %@", filePath);
                                                  _mediaURL = filePath;
                                                  self.btnPlaySound.enabled = YES;
                                                  
                                                  
                                                  @try {
                                                      self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:_mediaURL error:&error];
                                                      if ([self.audioPlayer prepareToPlay]) {
                                                          self.audioLabel2.text =[ @"" stringByAppendingFormat:@"%.0f'", ceil(self.audioPlayer.duration) ];
                                                          self.audioLabel1.text = @"0'";
                                                          self.audioSlide.maximumValue = (float)self.audioPlayer.duration;
                                                          
                                                      }
                                                  }
                                                  @catch (NSException *exception) {
                                                      [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
                                                      
                                                      NSLog(@"audio prepare may fail");
                                                  }
                                                  
                                                  
                                              }];
    [downloadTask resume];
}

- (void)_playDownloadedMedia:(id)sender
{
    NSError * error;
    //NSDictionary * attribs = [[NSFileManager defaultManager] attributesOfItemAtPath:_mediaURL.path error:&error];
    //NSNumber * filesize = [attribs objectForKey:NSFileSize];
    //NSLog(@"sound file size : %@", filesize);
    
    
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:_mediaURL error:&error];
    if (error) {
        PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Audio player could not be created.", nil)];
        [view useDefaultIOS7Style];
        self.audioPlayer = nil;
        return;
    }
    
    if ([self.audioPlayer prepareToPlay]) {
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"record_play" label:nil value:nil];
        
        UIImage* image =[UIImage imageNamed:@"audio_pause"];
        [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
        
        [self.btnPlaySound removeTarget:self action:@selector(_playDownloadedMedia:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnPlaySound addTarget:self action:@selector(pauseAudioPlay) forControlEvents:UIControlEventTouchUpInside];
        
        self.audioPlayer.delegate = self;
        BOOL success = [self.audioPlayer play];
        if (!success) {
            NSLog(@"start playing :%d", success);
        }
        self.updateSlideTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateAudioSlide) userInfo:nil repeats:YES];
    } else {
        NSLog(@"audio file not supported.");
        self.audioPlayer = nil;
        PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Audio file format not supported.", nil)];
        [view useDefaultIOS7Style];
    }
}
-(void)updateAudioSlide
{
    float currentSecond = self.audioPlayer.currentTime;
    self.audioLabel1.text = [@"" stringByAppendingFormat:@"%.0f'",  ceil(currentSecond) ];
    [self.audioSlide setValue:currentSecond];
    NSLog(@"the current slide value should be %f", currentSecond);
}
#pragma mark - Audio Player Delegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if (!flag) {
        NSLog(@"audio player finished with error.");
    }
    
    self.audioPlayer = nil;
    [self.updateSlideTimer invalidate];
    self.updateSlideTimer = nil;
    UIImage* image = [UIImage imageNamed:@"audio_play"];
    [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
    [self.audioSlide setValue:0];
    self.audioLabel1.text = @"0'";
    
    [self.btnPlaySound removeTarget:self action:@selector(resumeAudioPlay) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnPlaySound removeTarget:self action:@selector(pauseAudioPlay) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnPlaySound addTarget:self action:@selector(_playDownloadedMedia:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"play sound failed:%@", error);
    self.audioPlayer = nil;
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
    [player pause];
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
    [player play];
}
-(void)pauseAudioPlay
{
    
    [self.audioPlayer pause];
    UIImage* image = [UIImage imageNamed:@"audio_play"];
    [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
    
    [self.btnPlaySound removeTarget:self action:@selector(pauseAudioPlay) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPlaySound addTarget:self action:@selector(resumeAudioPlay) forControlEvents:UIControlEventTouchUpInside];
}
-(void)resumeAudioPlay
{
    [self.audioPlayer play];
    UIImage* image = [UIImage imageNamed:@"audio_pause"];
    [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
    
    [self.btnPlaySound removeTarget:self action:@selector(resumeAudioPlay) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPlaySound addTarget:self action:@selector(pauseAudioPlay) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - User Action Handlers

- (void)handleTapButtonLocation:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onDistanceButtonTapped)]) {
        [self.delegate onDistanceButtonTapped];
    }
}

- (void)handleTapUserBanner:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onUserBannerTapped)]) {
        [self.delegate onUserBannerTapped];
    }
}
- (void)handleTapBrandString:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTouchBrandString:withBrandString:)]) {
        NSString* BrandidString = [@"" stringByAppendingFormat:@"%lu",(unsigned long)self.itemDetail.brand_id];
        [self.delegate onTouchBrandString:BrandidString withBrandString:self.itemDetail.brand_name];
    }
}

-(void)handleTapLike:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTouchLikeButton)]) {
        [self.delegate onTouchLikeButton];
    }
}
-(void)handleTapShare:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTouchShareButton)]) {
        [self.delegate onTouchShareButton];
    }
}
-(void)handleTapMakeOffer:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTouchMakeOfferButton)]) {
        [self.delegate onTouchMakeOfferButton];
    }
}

-(void)handleTapAsk:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTouchAskButton)]) {
        [self.delegate onTouchAskButton];
    }
}

-(void)handleTapEditButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTouchEditButton)]) {
        [self.delegate onTouchEditButton];
    }
}
-(void)handleTapRenewButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTouchRenewButton)]) {
        [self disableUpdateButton];
        [self.delegate onTouchRenewButton];
    }
}
-(void)disableUpdateButton
{
    if (self.button_renew_textlabel == nil && self.button_renew_timelabel == nil) {
        self.button_renew_timelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, self.button_renew.frame.size.width, 20)];
        self.button_renew_timelabel.textColor = FANCY_COLOR(@"b9b9b9");
        self.button_renew_timelabel.font = [UIFont systemFontOfSize:16];
        self.button_renew_timelabel.textAlignment = NSTextAlignmentCenter;
        self.button_renew_timelabel.text = @"00:00:00";
        self.button_renew_textlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.button_renew.frame.size.width, 20)];
        self.button_renew_textlabel.textAlignment = NSTextAlignmentCenter;
        self.button_renew_textlabel.font = [UIFont systemFontOfSize:8];
        self.button_renew_textlabel.textColor = FANCY_COLOR(@"818181");
        self.button_renew_textlabel.text = NSLocalizedString(@"Renew",nil);
        [self.button_renew addSubview:self.button_renew_textlabel];
        [self.button_renew addSubview:self.button_renew_timelabel];
        [self.button_renew setTitle:@"" forState:UIControlStateNormal];
        
    }
    self.button_renew.layer.borderColor = FANCY_COLOR(@"f0f0f0").CGColor;
}
-(void)resetUpdateButton
{
    [self.button_renew_timelabel removeFromSuperview];
    [self.button_renew_textlabel removeFromSuperview];
    self.button_renew_timelabel = nil;
    self.button_renew_textlabel = nil;
    
    self.button_renew.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    [self.button_renew setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    self.button_renew.titleLabel.textColor = FANCY_COLOR(@"ff8830");
    self.button_renew.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.button_renew setTitle:NSLocalizedString(@"Renew",nil) forState:UIControlStateNormal];
    self.button_renew.userInteractionEnabled = YES;
    [self.button_renew addTarget:self action:@selector(handleTapRenewButton:) forControlEvents:UIControlEventTouchUpInside];
}
@end

@implementation HGItemCanvasView
#define CATEGORY_DYNAMIC_FONT_SIZE_MAXIMUM_VALUE 30
#define CATEGORY_DYNAMIC_FONT_SIZE_MINIMUM_VALUE 3

-(int) adjustFontSizeToFillItsContents:(NSString*)text withRect:(CGRect)rect
{
    if (text==nil) {
        return 20;
    }
    
    int i = 35;
    for (i = CATEGORY_DYNAMIC_FONT_SIZE_MAXIMUM_VALUE; i>CATEGORY_DYNAMIC_FONT_SIZE_MINIMUM_VALUE; i--) {
        
        UIFont *font = [UIFont boldSystemFontOfSize:i];
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: font}];
        
        CGRect rectSize = [attributedText boundingRectWithSize:CGSizeMake(rect.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        if (rectSize.size.height <= rect.size.height) {
            //size i should be ok
            break;
        }
    }
    NSLog(@"the font size is %d, ",i);
    return i;
    
}

- (void)drawRect:(CGRect)rect
{
    if (self.ownerCell.itemDetail.seller.uid == nil || self.ownerCell.itemDetail.seller.uid.length == 0) {
        //the information is not all ready.wait.
        return;
    }
    
    
    /*int audioOffset = 0;
     if (self.ownerCell.itemDetail.mediaLink.length > 0 ) {
     audioOffset = 45;
     }*/
    
    
    UIFont * titleFont = [UIFont fontWithName:@"Helvetica-bold" size:15];
    
    CGRect titleRectCaculate = [self.ownerCell.itemDetail.title boundingRectWithSize: CGSizeMake(CGRectGetWidth(rect) - 10, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: titleFont} context:NULL];
    CGRect titleRect = CGRectMake(10, 69 + 20 , titleRectCaculate.size.width, titleRectCaculate.size.height);
    
    [self.ownerCell.itemDetail.title drawWithRect:titleRect
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:titleFont, NSForegroundColorAttributeName: SYSTEM_DEFAULT_FONT_COLOR_1} context:nil];
    
    /*CGRect detailRectCaculate = [self.ownerCell.itemDetail.desc boundingRectWithSize: CGSizeMake(CGRectGetWidth(rect) - 10, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12.0]} context:NULL];
     */
    CGRect detailRect = CGRectMake(10, CGRectGetMaxY(titleRect) + 1, CGRectGetWidth(rect)-20, CGRectGetHeight(rect) - CGRectGetMaxY(titleRect) - 8);
    
    [self.ownerCell.itemDetail.desc drawWithRect:detailRect
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0], NSForegroundColorAttributeName: SYSTEM_DEFAULT_FONT_COLOR_1}
                                         context:nil];
    
}





@end

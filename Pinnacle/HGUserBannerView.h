//
//  HGUserBannerView.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-31.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HGAvatarView;
@class HGUser;

@interface HGUserBannerView : UIView

@property (nonatomic, strong) HGAvatarView * headView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIImageView * bkgView;
@property (nonatomic, strong) UITapGestureRecognizer * tapGesture;

- (id)initWithFrame:(CGRect)frame entity:(HGUser *)user;

@end

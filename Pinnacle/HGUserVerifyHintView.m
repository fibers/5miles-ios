//
//  HGUserVerifyHintView.m
//  
//
//  Created by Alex on 15-1-5.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "HGUserVerifyHintView.h"
#import "HGConstant.h"

@implementation HGUserVerifyHintView
{
    UILabel* label1;
    UILabel* label2;
    UILabel* label3;
    UILabel* label4;
    UIButton *verifyButton;
}

#define upperHeightOFF 5

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self _setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setup];
}

-(void)_setup
{
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, upperHeightOFF, self.frame.size.width, self.frame.size.height - upperHeightOFF)];
    self.contentView.backgroundColor = [UIColor colorWithRed:0.227f green:0.243f blue:0.255f alpha:1.00f];
    self.contentView.layer.cornerRadius = 5;
    self.contentView.clipsToBounds = YES;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self addSubview:self.contentView];
    
    self.triangleViewHint = [[HGTriangleUpView alloc] initWithFrame:CGRectMake(self.frame.size.width - 30, 0, 20, 10)];
    [self addSubview:self.triangleViewHint];
    
    [self addLineSeperate];
    [self addImageView];
    [self addLabel];
    [self addButton];
   
}

-(void)addLineSeperate
{
    UIView* line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 65, self.frame.size.width, 1)];
    UIView* line2 = [[UIView alloc] initWithFrame:CGRectMake(0, 65+37, self.frame.size.width, 1)];
    UIView* line3 = [[UIView alloc] initWithFrame:CGRectMake(0, 65+37*2, self.frame.size.width, 1)];
    
    [self addSubview:line1];
    [self addSubview:line2];
    [self addSubview:line3];
    line3.backgroundColor = line2.backgroundColor = line1.backgroundColor = FANCY_COLOR(@"6d6d6d");//[UIColor grayColor];
}

-(void)addImageView
{
    UIImageView* image1=[[UIImageView alloc] initWithFrame:CGRectMake(10, 17, 24, 24)];
    UIImageView* image2=[[UIImageView alloc] initWithFrame:CGRectMake(12, 8+65, 21, 21)];
    UIImageView* image3 = [[UIImageView alloc] initWithFrame:CGRectMake(12,8+ 65+37, 21, 21)];
    UIImageView* image4 = [[UIImageView alloc] initWithFrame:CGRectMake(12,8+ 65+37*2, 21, 21)];
    
    image1.image = [UIImage imageNamed:@"verify_info_sample_avatar"];
    image2.image = [UIImage imageNamed:@"verify_email_verify"];
    image3.image = [UIImage imageNamed:@"verify_fb_verify"];
    image4.image = [UIImage imageNamed:@"verify_phone_verify"];
    
    [self addSubview:image1];
    [self addSubview:image2];
    [self addSubview:image3];
    [self addSubview:image4];
}

-(void)addLabel
{
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+24+11, 15, self.frame.size.width - 44 - 4, 65-20)];
    label1.numberOfLines = 3;
    [self addSubview:label1];
    
    
    label2 =[[UILabel alloc] initWithFrame:CGRectMake(10+24+11, 5+65, self.frame.size.width - 44, 25)];
    label2.numberOfLines = 1;
    [self addSubview:label2];
    
    label3 = [[UILabel alloc] initWithFrame:CGRectMake(10+24+11, 3+65+37, self.frame.size.width-44, 35)];
    label3.numberOfLines = 2;
    [self addSubview:label3];
    
    label4 =[[UILabel alloc] initWithFrame:CGRectMake(10+24+11, 5+65+37*2, self.frame.size.width-44, 35)];
    label4.numberOfLines = 2;
    [self addSubview:label4];
    
    
    label4.textColor = label3.textColor = label2.textColor = label1.textColor = FANCY_COLOR(@"c6c8c9");
    label4.font = label3.font = label2.font = label1.font = [UIFont systemFontOfSize:11];
    

    label1.text = NSLocalizedString(@"Avatars that show a \"✓\" are verified users and it's safer to make a deal with them.",nil);
    label2.text = NSLocalizedString(@"5miles has verified their email.",nil);
    label3.text = NSLocalizedString(@"User has linked their Facebook account with 5miles.",nil);
    label4.text = NSLocalizedString(@"5miles has verified their phone number.",nil);

    
}
-(void)configContentText
{
    if (self.isCurrentUser== 1) {
        label1.text = NSLocalizedString(@"Avatars that show a \"✓\" are verified by phone, making buyers feel safe to deal with these sellers.",nil);
        label2.text = NSLocalizedString(@"5miles has verified your email.",nil);
        label3.text = NSLocalizedString(@"You have linked your Facebook account with 5miles.",nil);
        label4.text = NSLocalizedString(@"5miles has verified your phone number.",nil);
        
    }
}

-(void)configGetVerifiedButton{
    if (self.isCurrentUser){
        verifyButton.hidden = NO;
    }
}

- (void)removeVerfiedButton:(BOOL)remove {
    if (remove) {
        if (verifyButton.superview) {
            [verifyButton removeFromSuperview];
            CGRect tempRect = self.frame;
            tempRect.size.height -= 46;
            
            self.frame = tempRect;
        }
    }
}

-(void)addButton
{
    verifyButton = [[UIButton alloc] initWithFrame:CGRectMake((self.frame.size.width - 131)/2, 65+37*3 + 16, 131, 24)];
    verifyButton.layer.borderWidth = 1;
    verifyButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    verifyButton.layer.cornerRadius = 4;
    verifyButton.clipsToBounds = YES;
    verifyButton.hidden = YES;
    
    [verifyButton setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    verifyButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [verifyButton setTitle:NSLocalizedString(@"Get Verified",nil) forState:UIControlStateNormal];

    [verifyButton addTarget:self action:@selector(GetVerify) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview: verifyButton];
}

-(void)GetVerify
{
    if ([self.delegate respondsToSelector:@selector(clickGetVerifyButtion)]) {
        [self.delegate clickGetVerifyButtion];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

@implementation HGTriangleUpView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
    
    }
    return self;
}
- (void)drawRect:(CGRect)rect
{
    //An opaque type that represents a Quartz 2D drawing environment.
    //一个不透明类型的Quartz 2D绘画环境,相当于一个画布,你可以在上面任意绘画
    CGContextRef context = UIGraphicsGetCurrentContext();
 
    CGContextSetRGBStrokeColor(context,0.227,0.243,0.255,1.0);
    CGContextSetLineWidth(context, 1.0);
    UIColor*aColor = [UIColor colorWithRed:0.227 green:0.243 blue:0.255 alpha:1];
    CGContextSetFillColorWithColor(context, aColor.CGColor);
    CGPoint sPoints[3];//坐标点
    sPoints[0] =CGPointMake(self.frame.size.width/2, 0);//坐标1
    sPoints[1] =CGPointMake(0, self.frame.size.height);//坐标2
    sPoints[2] =CGPointMake(self.frame.size.width, self.frame.size.height);//坐标3
    CGContextAddLines(context, sPoints, 3);//添加线
    CGContextClosePath(context);//封起来
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
}

@end

@implementation HGTriangleDownView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}
- (void)drawRect:(CGRect)rect
{
    //An opaque type that represents a Quartz 2D drawing environment.
    //一个不透明类型的Quartz 2D绘画环境,相当于一个画布,你可以在上面任意绘画
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBStrokeColor(context,0.227,0.243,0.255,1.0);
    CGContextSetLineWidth(context, 1.0);
    UIColor*aColor = [UIColor colorWithRed:0.227 green:0.243 blue:0.255 alpha:1];
    CGContextSetFillColorWithColor(context, aColor.CGColor);
    CGPoint sPoints[3];//坐标点
    sPoints[0] =CGPointMake(0, 0);//坐标1
    sPoints[1] =CGPointMake(self.frame.size.width, 0);//坐标2
    sPoints[2] =CGPointMake(self.frame.size.width/2, self.frame.size.height);//坐标3
    CGContextAddLines(context, sPoints, 3);//添加线
    CGContextClosePath(context);//封起来
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
}

@end

//
//  FBlikeReminderViewController.m
//  Pinnacle
//
//  Created by Alex on 14-12-5.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "FBlikeReminderViewController.h"
#import "HGConstant.h"
#import "HGFBUtils.h"


@interface FBlikeReminderViewController ()

@end

@implementation FBlikeReminderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addFBLikerReminder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fbcontroller
{
  
    
}
#define UILABEL_HEIGHT 70

-(void)addFBLikerReminder
{
    self.view.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    self.boardview = [[UIView alloc] initWithFrame:CGRectMake(40, 100, 260, 0)];
    self.boardview.backgroundColor = [UIColor whiteColor];
    self.boardview.layer.cornerRadius = 4;
    self.boardview.clipsToBounds = YES;
    
    CGFloat top = 15;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.numberOfLines = 2;
    titleLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = self.alertTitle;
    CGSize fitSize = [titleLabel sizeThatFits:CGSizeMake(230, 0)];
    titleLabel.frame = CGRectMake(15, top, 230, fitSize.height);
    [self.boardview addSubview:titleLabel];
    
    top += (fitSize.height + 5);
    
    self.boardview.frame = CGRectMake(40, 100, 260, fitSize.height + UILABEL_HEIGHT + 70);
    
    UILabel* lable = [[UILabel alloc] initWithFrame:CGRectMake(10, top, 240, UILABEL_HEIGHT)];
    [self.boardview addSubview:lable];
    lable.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    lable.text = NSLocalizedString(@"We need you! Join us now to receive exclusive fan deals, tips, tricks and more! Tap Like now.", nil) ;
    lable.numberOfLines = 3;
    lable.font = [UIFont systemFontOfSize:13];
    lable.textAlignment = NSTextAlignmentCenter;
    
    top += UILABEL_HEIGHT;
    
    UIView* seperatorView =[[UIView alloc] initWithFrame:CGRectMake(0, top, 260, 0.5)];
    seperatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15];
    [self.boardview addSubview:seperatorView];
    
    FBSDKLikeControl *fbLikeControl = [[FBSDKLikeControl alloc] init];
    fbLikeControl.objectID = @"https://www.facebook.com/5milesapp";
    fbLikeControl.frame = CGRectMake(25, top + 10, fbLikeControl.frame.size.width, fbLikeControl.frame.size.height );
    fbLikeControl.alpha = 1.0;
    [self.boardview addSubview:fbLikeControl];
    [fbLikeControl addTarget:self action:@selector(onTouchLikeControl:) forControlEvents:UIControlEventTouchUpInside];

    
    UIButton* closeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.boardview.frame.size.width - 25, 5, 20, 20)];
    [self.boardview addSubview:closeButton];
    [closeButton setBackgroundImage:[UIImage imageNamed:@"close-button"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    
    CGPoint centerPoint = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    self.boardview.center = centerPoint;
    [self.view addSubview:self.boardview];
}

- (void)onTouchLikeControl:(UIControl *)sender
{
    [self.delegate touchedLikeButtonWithFblikeReminder:self];
}

-(void)closeView
{
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"likefacebookpage_close" label:nil value:nil];
    [self.delegate touchedCloseButtonWithFblikeReminder:self];
}

@end

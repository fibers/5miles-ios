//
//  HGGoodSellerViewController.m
//  Pinnacle
//
//  Created by Alex on 15-3-31.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGGoodSellerViewController.h"
#import "HGConstant.h"
#import "UIImage+pureColorImage.h"
#import "HGGoodSellerCell.h"
#import "HGGoodSellerTopCell.h"
#import "HGAppData.h"
#import "HGGoodUserNearBy.h"
#import "HGAppDelegate.h"

#import <SVProgressHUD.h>
@interface HGGoodSellerViewController ()

@property (nonatomic, assign) int totalFollowCount;
@property (nonatomic, strong) UILabel* totalFollowLabel;
@property (nonatomic, strong) UIButton* TotalFollowBtn;
@property (nonatomic, assign) int followActionIsSending;

@property (nonatomic, strong) NSString *sufficientHint;
@property (nonatomic, strong) NSString *insufficientHint;

@end

#define GOODSELLER_HEIGHT 138

@implementation HGGoodSellerViewController

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.tabBarController.tabBar.hidden = YES;
    [[HGUtils sharedInstance] gaTrackViewName:@"sellersnearby"];
    
    
    ///what is this ???? gps may small than 0...-112.0343
    if ([HGAppData sharedInstance].GPSlat > 0 && [HGAppData sharedInstance].GPSlon > 0) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellernearby" action:@"rightgps" label:nil value:nil];
    }else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellernearby" action:@"rightgps" label:nil value:nil];
    }
    
    
    
    self.dataArray = [[NSMutableArray alloc] init];
    self.navigationItem.title = @"";
    self.totalFollowCount = 0;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.tableView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    if (self.leftNavigationType == 0) {
        [self customizeNavBarLeftButton];
    }
    else{
        
    }
    [self customizeNavBarRightButton];
    
    [self getUserNearBy];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}



-(void)getUserNearBy
{
    __weak HGGoodSellerViewController* weakSelf = self;
    
    NSNumber* latNumber = [[NSNumber alloc] initWithDouble:[[HGUtils sharedInstance] getPreferLat]];
    NSNumber* lonNumber = [[NSNumber alloc] initWithDouble:[[HGUtils sharedInstance] getPreferLon]];
    
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"users_nearby_new/" parameters: @{@"lat": latNumber,@"lon":lonNumber} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *responsedDic = responseObject;
        NSDictionary *hints = [responsedDic objectForKey:@"res_ext"];
        if (hints) {
            self.sufficientHint = [hints objectForKey:@"sufficient_users_hint"];
            self.insufficientHint = [hints objectForKey:@"insufficient_users_hint"];
        }
        
        NSArray* objects = [responsedDic objectForKey:@"objects"];
        for (id dictObj in objects) {
            NSError * error = nil;
            HGGoodUserNearBy * item = [[HGGoodUserNearBy alloc] initWithDictionary:dictObj error:&error];
            item.userPreferFollow = YES;
            if (!error) {
                [weakSelf.dataArray addObject:item];
            } else {
                NSLog(@"good user near by  %@ creation error: %@", [dictObj objectForKey:@"id"], error);
            }
        }
        
        if (self.dataArray.count==0 || [self hasSellerDistanceBiggerThanLimit:self.dataArray])
        {
            self.navigationItem.title = NSLocalizedString(@"Recommended",nil);
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"showrecommend" label:nil value:nil];

            topCellType = 1;
        }
        else
        {
            
            self.navigationItem.title = NSLocalizedString(@"Sellers Nearby",nil);
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"showsellersnearby" label:nil value:nil];
            topCellType = 0;
        }
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get user near by fail %@",error);
        [SVProgressHUD dismiss];
    }];
}


-(BOOL)hasSellerDistanceBiggerThanLimit:(NSArray*)dataArray
{
    for (int i = 0; i<[dataArray count]; i++) {
        HGGoodUserNearBy* seller = [dataArray objectAtIndex:i];
        
        NSString* latString = [seller.location objectForKey:@"lat"];
        NSString* lonString = [seller.location objectForKey:@"lon"];
        
        CLLocation * itemLoc = [[CLLocation alloc] initWithLatitude:latString.floatValue  longitude:lonString.floatValue];
        

        float distance = 0;
        if (fabs(latString.floatValue - 0.0) < 0.000001
            && fabs(lonString.floatValue - 0.0) < 0.000001) {
            distance = -1.0;;
        }
        else{
            CLLocation* preferLocation = [[CLLocation alloc] initWithLatitude:[[HGUtils sharedInstance] getPreferLat] longitude:[[HGUtils sharedInstance] getPreferLon]];
            
            distance = [itemLoc distanceFromLocation:preferLocation ]* 0.001;
        }

        //float distanceByMiles = distance * KM_TO_MILE;
        if (distance > 50) {
            return YES;
        }
        if (distance < 0) {
            return YES;
        }
        
    }
    
    
    return FALSE;
}


-(void)followAction
{
    NSLog(@"click on follow action");
    if (self.followActionIsSending == 1) {
        return;
    }
    else{
        self.followActionIsSending = 1;
    }
    
    NSString* idsString = @"";
    for (int i = 0; i<self.dataArray.count; i++) {
        HGGoodUserNearBy* user = [self.dataArray objectAtIndex:i];
        if(user.userPreferFollow)
        {
            idsString = [idsString stringByAppendingFormat:@"%@,",user.uid ];
        }
    }
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"follow/" parameters:@{@"user_ids":idsString } success:^(NSURLSessionDataTask *task, id responseObject) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"followyes" label:nil value:nil];
        [SVProgressHUD dismiss];
        self.followActionIsSending = 0;
        [HGAppData sharedInstance].bHomeSwitchToFollowingNeeded  = YES;
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        self.followActionIsSending = 0;
        [SVProgressHUD dismiss];
        [HGAppData sharedInstance].bHomeSwitchToFollowingNeeded  = YES;
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
-(void)skipAction
{
    // 0 is default style
    if (self.leftNavigationType == 0) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"follow_back" label:nil value:nil];
    }
    else{
        
        //// the default navigation back do not go through this,,,todo....GA bug..
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"skip_follow" label:nil value:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)customizeNavBarLeftButton
{
    UIButton *TotalFollowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    TotalFollowBtn.frame = CGRectMake(0, 7, 40, 26);
    
    //useButton.backgroundColor = SYSTEM_MY_ORANGE;
    [TotalFollowBtn setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [TotalFollowBtn setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateHighlighted];
    
    self.totalFollowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 25)];
    self.totalFollowLabel.backgroundColor = [UIColor clearColor];
    self.totalFollowLabel.textColor = [UIColor grayColor];
    self.totalFollowLabel.text = NSLocalizedString(@"Skip",nil);
    self.totalFollowLabel.font = [UIFont systemFontOfSize:15];
    self.totalFollowLabel.textAlignment = NSTextAlignmentLeft;
    [TotalFollowBtn addSubview:self.totalFollowLabel];
    
    [TotalFollowBtn addTarget:self action:@selector(skipAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *skipItem = [[UIBarButtonItem alloc] initWithCustomView:TotalFollowBtn];
    [self.navigationItem setLeftBarButtonItem:skipItem];
    
    
    
}
-(void)customizeNavBarRightButton
{
    UIView* rightButtonBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 35)];
    
    self.TotalFollowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.TotalFollowBtn.frame = CGRectMake(10, 7, 76, 26);
    self.TotalFollowBtn.clipsToBounds = YES;
    self.TotalFollowBtn.layer.cornerRadius = 4;
    
    [self.TotalFollowBtn setBackgroundImage:[UIImage imageWithColor:SYSTEM_MY_ORANGE] forState:UIControlStateNormal];
    self.totalFollowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 76, 25)];
    self.totalFollowLabel.backgroundColor = [UIColor clearColor];
    self.totalFollowLabel.textColor = [UIColor whiteColor];
    self.totalFollowLabel.text = NSLocalizedString(@"Follow(10)",nil);
    self.totalFollowLabel.font = [UIFont systemFontOfSize:12];
    self.totalFollowLabel.textAlignment = NSTextAlignmentCenter;
    [self.TotalFollowBtn addSubview:self.totalFollowLabel];
    
    [self.TotalFollowBtn addTarget:self action:@selector(followAction) forControlEvents:UIControlEventTouchUpInside];
    
    [rightButtonBackView addSubview:self.TotalFollowBtn];
    UIBarButtonItem *followItem = [[UIBarButtonItem alloc] initWithCustomView:rightButtonBackView];
    [self.navigationItem setRightBarButtonItem:followItem];
    
  
  
}
-(void)updateFollowLabelCount
{
    int count = 0;
    for ( int i = 0; i<self.dataArray.count; i++) {
        HGGoodUserNearBy* user = [self.dataArray objectAtIndex:i];
        if (user.userPreferFollow) {
            count ++;
        }
    }
    self.totalFollowCount = count;
    self.totalFollowLabel.text = [NSLocalizedString(@"Follow",nil) stringByAppendingFormat:@"(%d)", count ];
    if(self.totalFollowCount == 0)
    {
        [self.TotalFollowBtn setBackgroundImage:[UIImage imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
        self.TotalFollowBtn.userInteractionEnabled = NO;
    }
    else{
        self.TotalFollowBtn.userInteractionEnabled = YES;
        [self.TotalFollowBtn setBackgroundImage:[UIImage imageWithColor:SYSTEM_MY_ORANGE] forState:UIControlStateNormal];
        
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count+1;
}


static int topCellType = 0;
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if (topCellType==0) {
            return 90;
        }
        else{
            return 94+14;
        }
    }
    else{
        return GOODSELLER_HEIGHT;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        HGGoodSellerTopCell*cell = [tableView dequeueReusableCellWithIdentifier:@"GoodSellerTableTopCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.CellType = topCellType;
        [cell configCellWithSufficientHint:self.sufficientHint insufficientHint:self.insufficientHint];
        return cell;
    }
    else{
        HGGoodSellerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodSellerTableCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        HGGoodUserNearBy* gooduser = [self.dataArray objectAtIndex:indexPath.row -1];
    
        [cell configCell:gooduser];
        return cell;
    }
}

-(void)onTouchCellFollowPrefer
{
    [self updateFollowLabelCount];
}

@end

//
//  UIApplication+Pinnacle.h
//  Pinnacle
//
//  Created by zhenyonghou on 15/6/30.
//  Copyright © 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIApplication(Pinnacle)

+ (BOOL)isEnableRemoteNotification;

+ (void)gotoSystemSetting;

@end

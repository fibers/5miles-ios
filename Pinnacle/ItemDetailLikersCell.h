//
//  ItemDetailLikersCell.h
//  Pinnacle
//
//  Created by Alex on 14-11-17.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ItemDetailLikeUserClickAction <NSObject>

- (void)onTouchAvatarForUserView:(NSObject *)user;
-(void)onTouchLikerMore:(NSObject*)sender;
@end
@interface ItemDetailLikersCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView* likersIcon;
@property (nonatomic, strong) UILabel * likersCount;
@property (nonatomic, strong) UIView* SeperatorView;
@property (nonatomic, assign) id<ItemDetailLikeUserClickAction> actionDelegate;


-(void)configCell:(NSArray*)likersArray withTotalLikerCount:(int)totalCount;
@end

//
//  HGAvatarView.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-31.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <SDWebImage/UIImageView+WebCache.h>

@interface HGAvatarView : UIImageView

- (id)initWithFrame:(CGRect)frame imageLink:(NSString *)imageLink;

@end

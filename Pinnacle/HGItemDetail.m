//
//  HGItemDetail.m
//  Pinnacle
//
//  Created by Alex on 15-2-28.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemDetail.h"

@implementation HGItemDetail

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"uid",
                                                       @"currency":@"currencyUnit", @"location":@"geoLocation",
                                                       @"owner":@"seller",
                                                       @"comment_count":@"commentCount", @"has_more_comments":@"hasMoreComments",
                                                       @"root_cat_id" : @"rootCategoryId",
                                                       @"cat_id":@"categoryIndex",
                                                       @"country":@"country",
                                                       @"region":@"region",
                                                       @"city":@"city",
                                                       @"state":@"state",
                                                       @"user_blocked":@"bBlockedOwner",
                                                       @"original_price":@"originalPrice"
                                                       }];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[
                        @"mediaLink",
                        @"distance",
                        @"state",
                        @"created_at",
                        @"updated_at",
                        @"seller",
                        @"originalPrice",
                        @"brand_name",
                        @"shipping_method",
                        @"country",
                        @"region",
                        @"city",
                        @"brand_id",
                        @"renew_ttl",
                        @"is_new",
                        @"deletable",
                        @"bBlockedOwner",
                        @"review_num",
                        @"review_score",
                        @"rf_tag"
                        ];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    
    return NO;
}

@end

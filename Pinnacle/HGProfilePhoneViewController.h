//
//  HGProfilePhoneViewController.h
//  Pinnacle
//
//  Created by Alex on 14-9-30.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, HGPhoneVerifyType) {
    HGPhoneVerifyType_profile,
    HGPhoneVerifyType_listing
};

typedef NS_ENUM(NSInteger, HGProfilePhoneViewControllerFromPage) {
    HGProfilePhoneViewControllerFromProfilePage = 0,
    HGProfilePhoneViewControllerFromSellPage,
    HGProfilePhoneViewControllerFromDetailPage
};

NSString * const HGProfilePhoneViewControllerVerifySuccessNotification;

@interface HGProfilePhoneViewController : UIViewController<UITableViewDataSource,
UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;


@property (nonatomic, strong) UITextField* phoneNumber;

@property (nonatomic, strong) UIView* vSeperate1;
@property (nonatomic ,strong) UIView* vSeperate2;
@property (nonatomic, strong) UIView* vSeperate3;
@property (nonatomic, strong) UIView* hSeperate1;
@property (nonatomic, strong) UIView* hSeperate2;

@property (nonatomic, strong) UIButton* sendpassCodeButton;
@property (nonatomic, strong) UIButton* confirmButton;



@property (nonatomic, strong) UILabel* passcodeLabel;
@property (nonatomic, strong) UITextField* passcodeTextfield;

@property (nonatomic, strong) UILabel* ClickCountry;
@property (nonatomic, strong) UILabel* countryPhoneCode;
@property (nonatomic, strong) UILabel* countryName;

@property (nonatomic ,strong) NSTimer *Timer;

@property (nonatomic, assign) BOOL bStartByRateUserView;
@property (nonatomic, assign) HGPhoneVerifyType type;

@property (nonatomic, assign) HGProfilePhoneViewControllerFromPage fromPage;

@end

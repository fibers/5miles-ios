//
//  HGResourceListController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-28.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGResourceListController.h"
#import "HGAppData.h"

#import <SVProgressHUD.h>

@interface HGResourceListController ()
@property (nonatomic, strong) UIView* noMoreHintView;
@end

@implementation HGResourceListController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        [self commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self commonSetup];
}

- (void)commonSetup
{
    self.objects = [@[] mutableCopy];
    self.nextLink = @"";
    self.endpoint = @"";
    self.params = nil;
}

-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    [self.tableView addSubview:self.noMoreHintView];
    [self.noMoreHintView setHidden:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 20)];
    footView.autoresizesSubviews = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.tableFooterView = footView;
    [self addNoMoreHintView];
    
    }

-(void)getServerData
{
    __weak typeof(self) weakSelf = self;
    [SVProgressHUD show];
    if ([self.nextLink isEqual:[NSNull null]] ) {
        self.nextLink = @"";
    }
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[self.endpoint stringByAppendingString:@""] parameters:self.params success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        NSArray * dataset = [responseObject objectForKey:@"objects"];
        
        [self.objects removeAllObjects];
        [self fillObjectsWithServerDataset:dataset];
        
        [self.tableView reloadData];
        
        if (![self.nextLink isEqual:[NSNull null]]) {
            [self.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf _loadNextPage];
            }];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        
        NSLog(@"http get %@ error: %@", self.endpoint, error.userInfo);
    }];
    

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getServerData];
    }

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Child Class Overridables

- (void)fillObjectsWithServerDataset:(NSArray *)dataset
{
    
}

- (NSString *)titleForNoMore
{
    return NSLocalizedString(@"No more", nil);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.objects.count;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Helpers




- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.tableView.infiniteScrollingView stopAnimating];
        self.tableView.showsInfiniteScrolling = NO;
        [self.noMoreHintView setFrame:CGRectMake(0, self.tableView.contentSize.height, self.tableView.frame.size.width, 40)];
        if (self.objects.count!=0) {
            [self.noMoreHintView setHidden:NO];

        }
    } else {
        
        
       
        [self addGAlog];
        
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[self.endpoint stringByAppendingString:self.nextLink] parameters:self.params success:^(NSURLSessionDataTask *task, id responseObject) {
            [self.tableView.infiniteScrollingView stopAnimating];
            
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            NSArray * dataset = (NSArray *)[responseObject objectForKey:@"objects"];
            
            [self fillObjectsWithServerDataset:dataset];
            [self.tableView reloadData];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self.tableView.infiniteScrollingView stopAnimating];
            NSLog(@"http get %@ error: %@", self.endpoint, error.userInfo);
        }];
        
    }
}


-(void)addGAlog
{
    if ([self.endpoint isEqualToString:@"user_followers/"]) {
        NSString* uidstring = [self.params objectForKey:@"user_id"];
        
        if ([uidstring isEqualToString:[HGUserData sharedInstance].fmUserID])
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollower_loadmore" label:nil value:nil];
        }
        else
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"itsfollowers_loadmore" label:nil value:nil];
        }
    
        
    }
    if ([self.endpoint isEqualToString:@"user_following/"]) {
        NSString* uidstring = [self.params objectForKey:@"user_id"];
        
        if ([uidstring isEqualToString:[HGUserData sharedInstance].fmUserID])
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollowing_loadmore" label:nil value:nil];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerProfile_view" action:@"itsfollowing_loadmore" label:nil value:nil];
        }
    }
}

@end

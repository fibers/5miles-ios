//
//  JSQMessagesCollectionViewCell+Pinnacle.m
//  Pinnacle
//
//  Created by shengyuhong on 15/5/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "JSQMessagesCollectionViewCell+Pinnacle.h"
#import "HGUtils.h"
#import "HGUserData.h"
#import "TTTAttributedLabel.h"
#import <UIImageView+WebCache.h>
#import "JSQMessage+Pinnacle.h"

static const NSInteger TagAvatarMark = 1000;
static const NSInteger TagTimeLabel = 1001;
static const NSInteger TagIndicatorSending = 1002;
const NSInteger TagIndicatorFailed = 1003;

static const NSUInteger SizeIndicator = 20;

@implementation JSQMessagesCollectionViewCell (Pinnacle)


- (void)configAvatar:(NSURL *)url verified:(BOOL)verified{
    
    [self.avatarImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"default-avatar"]];

    self.avatarImageView.layer.masksToBounds = YES;
    self.avatarImageView.layer.cornerRadius = CGRectGetWidth(self.avatarImageView.bounds) * 0.5;
    self.avatarImageView.layer.borderWidth = 2.0f;
    self.avatarImageView.layer.borderColor =  [UIColor colorWithRed:0xff/255.0f green:0x88/255.0f blue:0x30/255.0f alpha:0.4f].CGColor;
    
    UIImageView *ivAvartarMark = (UIImageView *)[self.contentView viewWithTag:TagAvatarMark];
    if(!ivAvartarMark){
        ivAvartarMark = [[UIImageView alloc] initWithFrame:CGRectZero];
        
        ivAvartarMark.tag = TagAvatarMark;
        ivAvartarMark.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.contentView addSubview:ivAvartarMark];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:ivAvartarMark attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:self.avatarImageView.bounds.size.width/3]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:ivAvartarMark attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:self.avatarImageView.bounds.size.height/3]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:ivAvartarMark attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.avatarImageView attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:ivAvartarMark attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.avatarImageView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0]];
    }
    
    if(verified){
        ivAvartarMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }else{
        ivAvartarMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    
}

- (void)configTimeText:(NSString *)timeText{
    
    TTTAttributedLabel *lbTime = (TTTAttributedLabel *)[self.cellTopLabel viewWithTag:TagTimeLabel];
    if(!lbTime){
        lbTime = [[TTTAttributedLabel alloc]initWithFrame:self.cellTopLabel.bounds];
        lbTime.clipsToBounds = YES;
        lbTime.layer.cornerRadius = 4.0f;
        lbTime.textColor = [UIColor whiteColor];
        lbTime.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
        lbTime.font = [UIFont systemFontOfSize:12];
        lbTime.textAlignment = NSTextAlignmentCenter;
        lbTime.numberOfLines = 1;
        lbTime.textInsets = UIEdgeInsetsMake(2, 10, 2, 10);
        lbTime.translatesAutoresizingMaskIntoConstraints = NO;
        lbTime.tag = TagTimeLabel;
        [self.cellTopLabel addSubview:lbTime];
        
        [self.cellTopLabel addConstraint:[NSLayoutConstraint constraintWithItem:lbTime attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.cellTopLabel attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    }
    
    if([timeText length] > 0){
        lbTime.text = timeText;
        [lbTime sizeToFit];
        // IOS 8.0.2 : need to change the center of lbTime manually. Confused.
        CGPoint center = lbTime.center;
        center.x = self.cellTopLabel.center.x;
        lbTime.center = center;
    }else{
        lbTime.hidden = YES;
    }
    
}

- (void)configSendingIndicator:(JSQMessage *)message{
    
    UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[self.contentView viewWithTag:TagIndicatorSending];
    if(!indicator){
        
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        indicator.userInteractionEnabled = NO;
        indicator.translatesAutoresizingMaskIntoConstraints = NO;
        indicator.tag = TagIndicatorSending;
        [self.contentView addSubview:indicator];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.messageBubbleContainerView attribute:NSLayoutAttributeLeading multiplier:1 constant:-10]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.messageBubbleContainerView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:SizeIndicator]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:SizeIndicator]];
    }
    
    if(message.bSendInProcess && !message.isMediaMessage){
        indicator.hidden = NO;
        [indicator startAnimating];
    }else{
        indicator.hidden = YES;
        [indicator stopAnimating];
    }

}

- (void)configFailedIndicator:(JSQMessage *)message{
    
    UIImageView *indicator = (UIImageView *)[self.contentView viewWithTag:TagIndicatorFailed];
    if(!indicator){
        
        indicator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-send-failed"]];
        indicator.userInteractionEnabled = YES;
        indicator.translatesAutoresizingMaskIntoConstraints = NO;
        indicator.tag = TagIndicatorFailed;

        [self.contentView addSubview:indicator];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.messageBubbleContainerView attribute:NSLayoutAttributeLeading multiplier:1 constant:-10]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.messageBubbleContainerView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:SizeIndicator]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:SizeIndicator]];
    }
    
    if(message.bSendFailed){
        indicator.hidden = NO;
    }else{
        indicator.hidden = YES;
    }
    
}

@end

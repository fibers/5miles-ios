//
//  HGPublicPageLayoutModel.h
//  Pinnacle
//
//  Created by mumuhou on 15/7/17.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, HGPublicPageLayoutModelLayoutType) {
    HGPublicPageLayoutModelLayoutTypeListing = 0,
    HGPublicPageLayoutModelLayoutTypeService,
};

typedef NS_ENUM(NSInteger, HGPublicPageLayoutModelSectionType) {
    HGPublicPageLayoutModelSectionTypeTitle   = 0,
    HGPublicPageLayoutModelSectionTypeCategory,
    HGPublicPageLayoutModelSectionTypeDescription,
    HGPublicPageLayoutModelSectionTypeVoice,
    HGPublicPageLayoutModelSectionTypeMoreInformation,
    HGPublicPageLayoutModelSectionTypeShare
};

typedef NS_ENUM(NSInteger, HGPublicPageLayoutModelRowType) {
    // section 0
    HGPublicPageLayoutModelRowTypeTitle   = 0,
    
    // section 1
    HGPublicPageLayoutModelRowTypeCategory,
    HGPublicPageLayoutModelRowTypePrice,
    HGPublicPageLayoutModelRowTypeLocation,
    
    // section 2
    HGPublicPageLayoutModelRowTypeDescription,
    
    // section 3
    HGPublicPageLayoutModelRowTypeVoice,
    
    // section 4
    HGPublicPageLayoutModelRowTypeOriganlPrice,
    HGPublicPageLayoutModelRowTypeBrand,
    HGPublicPageLayoutModelRowTypeDelivery,
    
    // section 5
    HGPublicPageLayoutModelRowTypeShare
};

@interface HGPublicPageLayoutModel : NSObject

@property (nonatomic, assign) HGPublicPageLayoutModelLayoutType currentLayoutType;

@property (nonatomic, strong, readonly) NSArray *currentSectionTypeArray;

- (NSArray *)rowTypeArrayForSection:(NSInteger)section;

@end

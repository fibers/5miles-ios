//
//  HGFollowersViewController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGFollowersViewController.h"
#import "HGFollowerCell.h"
#import "HGUtils.h"
#import "HGAppDelegate.h"
#import "HGUserViewController.h"
#import "HGAppData.h"
#import "UIStoryboard+Pinnacle.h"

@interface HGFollowersViewController ()
@property(nonatomic, assign) BOOL isMyFollowers;
@property(nonatomic, assign) BOOL bHasUserActionBeforeReturn;
@end

@implementation HGFollowersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self commonSetup];
    }
    return self;
}

- (void)commonSetup
{
    [super commonSetup];
    self.endpoint = @"user_followers/";
    self.nextLink = @"";
}

- (void)viewDidLoad
{
    self.params = @{@"user_id": self.userID, @"limit":[[NSNumber alloc] initWithInt:15]};
    self.bHasUserActionBeforeReturn = NO;
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if ([self.userID isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        self.isMyFollowers = TRUE;
    }
    else{
        self.isMyFollowers = FALSE;
    }
    self.navigationItem.title = NSLocalizedString(@"Followers", nil);
    
   
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.tabBarController.tabBar.hidden = YES;
    [[HGUtils sharedInstance] gaTrackViewName:@"Followers"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addotherUserDefaultEmptyView
{
    
    int iconYoffset = 144;
    int lineOffset = 10;
    
    self.view.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];
    
    int iphone4_Y_offset_config_Value = 0;
    if (SCREEN_HEIGHT <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    CGPoint iconCenter  = CGPointMake(SCREEN_WIDTH/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [self.view addSubview:icon];
    
    
    CGPoint centerPoint = CGPointMake(SCREEN_WIDTH/2, CGRectGetMaxY(icon.frame)+lineOffset + 60/2);
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 40)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"No followers right now",nil);
    textLabel.font = [UIFont systemFontOfSize:12];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [self.view addSubview:textLabel];
}


-(void)addDefaultEmptyView
{
    int iconYoffset = 144;
   int lineOffset = 10;
    
    self.view.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];
   
    CGRect rx = [ UIScreen mainScreen ].bounds;
    int iphone4_Y_offset_config_Value = 0;
    if (rx.size.height <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
   
    CGPoint iconCenter  = CGPointMake(rx.size.width/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [self.view addSubview:icon];
    
    
    CGPoint centerPoint = CGPointMake(rx.size.width/2, CGRectGetMaxY(icon.frame)+lineOffset + 60/2);
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 40)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"Time to get your first fan! Publish listings and\n let people discover them.",nil);
    textLabel.font = [UIFont systemFontOfSize:12];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [self.view addSubview:textLabel];
    //[textLabel sizeToFit];
    
    
     CGPoint buttonCenter = CGPointMake(rx.size.width/2, CGRectGetMaxY(textLabel.frame)+lineOffset + 36/2);
    UIButton* button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 200, 36)];
    button.layer.cornerRadius = 4.0;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = DEFAULT_EMPTY_BOARDER_COLOR;
    UILabel* buttonTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    buttonTitle.textAlignment = NSTextAlignmentCenter;
    buttonTitle.text = NSLocalizedString(@"Sell more listings", nil);
    buttonTitle.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [button addSubview:buttonTitle];
    button.center = buttonCenter;
    [self.view addSubview:button];
    [button addTarget:self action:@selector(defaultEmptyAction) forControlEvents:UIControlEventTouchUpInside];
}
-(void)defaultEmptyAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollowers_empty" label:nil value:nil];
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    {
        UINavigationController *navUpload = (UINavigationController *)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"NavUpload"];
        [appDelegate.tabController presentViewController:navUpload animated:YES completion:nil];
    }
    
}
#pragma mark - Overrides

- (void)fillObjectsWithServerDataset:(NSArray *)dataset
{
    for (NSDictionary * dictObj in dataset) {
        NSError * error;
        HGUser * user = [[HGUser alloc] initWithDictionary:dictObj error:&error];
        if (nil == error) {
            [self.objects addObject:user];
        }
    }
    if (self.objects.count == 0 && [self.userID isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        [self addDefaultEmptyView];
    }
    else if(self.objects.count==0)
    {
        [self addotherUserDefaultEmptyView];
    }
}




#pragma mark - UITableView Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGFollowerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FollowerCell" forIndexPath:indexPath];
    
    HGUser * user = [self.objects objectAtIndex:indexPath.row];
    [cell configWithObject:user withIsMyFollower: self.isMyFollowers];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.bHasUserActionBeforeReturn = YES;
    if ([self.userID isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollowersperson" label:nil value:nil];
        
    }else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowers_view" action:@"sellerfollowerperson" label:nil value:nil];
        
    }
    HGUser * user = [self.objects objectAtIndex:indexPath.row];
    
    HGUserViewController *vcUser = (HGUserViewController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCUser"];
    vcUser.user = user;
    
    [self.navigationController pushViewController:vcUser animated:YES];
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.isMyFollowers && !self.bHasUserActionBeforeReturn) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollower_back" label:nil value:nil];
    }
    if (!self.isMyFollowers && !self.bHasUserActionBeforeReturn) {
        
    }
 }

@end

//
//  HGShopItem.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGShopItem.h"
#import "HGAppData.h"
#import "HGUtils.h"

@implementation HGGeoLocation

@end

@implementation HGItemImage

@end

@implementation HGShopItem

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"uid",
                                                       @"currency":@"currencyUnit", @"location":@"geoLocation",
                                                       @"owner":@"seller",
                                                       @"root_cat_id" : @"rootCategoryId",
                                                       @"cat_id":@"categoryIndex",
                                                       @"country":@"country",
                                                       @"region":@"region",
                                                       @"city":@"city",
                                                       @"state":@"state",
                                                       @"user_blocked":@"bBlockedOwner",
                                                       @"original_price":@"originalPrice"
                                                       }];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[
                        @"mediaLink",
                        @"distance",
                        @"state",
                        @"created_at",
                        @"updated_at",
                        @"seller",
                        @"originalPrice",
                        @"brand_name",
                        @"shipping_method",
                        @"country",
                        @"region",
                        @"city",
                        @"renew_ttl",
                        @"is_new",
                        @"deletable",
                        @"bBlockedOwner",
                        @"rf_tag",
                        @"rootCategoryId"
                        ];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    
    return NO;
}

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    self = [super initWithDictionary:dict error:err];
    if (self) {
        if ([dict objectForKey:@"distance"] == nil) {
            CLLocation * itemLoc = [[CLLocation alloc] initWithLatitude:self.geoLocation.lat longitude:self.geoLocation.lon];
            if (fabs(self.geoLocation.lat - 0.0) < 0.000001
                && fabs(self.geoLocation.lon - 0.0) < 0.000001) {
                self.distance = -1.0;;
            }
            else{
                 ///
                CLLocation* preferLocation = [[CLLocation alloc] initWithLatitude:[[HGUtils sharedInstance] getPreferLat] longitude:[[HGUtils sharedInstance] getPreferLon]];
                  self.distance = [itemLoc distanceFromLocation:preferLocation] * 0.001;
            }
          
        }
        
       // Format the price. Eg, double value 69.99 will be changed to string "69.989999999"
        if( self.price > 0 ){
            self.price = [[NSString stringWithFormat:@"%.2f", self.price] floatValue];
        }
        
        if( self.originalPrice > 0){
            self.originalPrice = [[NSString stringWithFormat:@"%.2f", self.originalPrice] floatValue];
        }
        
    }
    return self;
}


-(NSString*)formatAddress
{
    int stringCounter = 0;
    NSString* addressString = @"";
    if (self.city != nil && self.city.length > 0) {
        stringCounter = stringCounter + 1;
        addressString = [addressString stringByAppendingFormat:@"%@",self.city];
    }
    if (self.region != nil && self.region.length > 0) {
        stringCounter = stringCounter + 1;
        if (addressString.length >0) {
            addressString  = [addressString stringByAppendingFormat:@", %@", self.region];
        }
        else{
            addressString = self.region;
        }
    }
    
    if (self.country !=nil && self.country.length > 0 && stringCounter < 2) {
        if (addressString.length > 0 ) {
            addressString=[addressString stringByAppendingFormat:@", %@",self.country];
        }
        else{
            addressString = self.country;
        }
    }
    
  
    return addressString;
}
- (NSString *)formatDistance
{
    if (![CLLocationManager locationServicesEnabled]) {
        return @"";
    }
    
    NSString * distString = @"";
    
   
    NSString* preferredLang = [[HGUtils sharedInstance] getPreferLanguage];
    
    if ([preferredLang isEqualToString:@"zh-Hans"]) {
        //Chinese
        distString = [[HGUtils sharedInstance] getDistanceKm:self.distance];

    }else
    {
        distString = [[HGUtils sharedInstance]getDistanceMiles:self.distance];
    }
    
    return distString;
}


- (NSString *)priceForDisplay
{
    if(fabs(self.price - 0) < 0.000001){
        return NSLocalizedString(@"Free", nil);
    }else{
        if([self.currencyUnit length] > 0){
            
            NSString *currencyDisplay = [self currencyForDisplay];
            return [currencyDisplay stringByAppendingFormat:@"%.2f", self.price];
            
        }else{
            return [NSString stringWithFormat:@"%.2f", self.price];
        }
        
    }
}

- (NSString *)originalPriceForDisplay
{
    if(fabs(self.originalPrice - 0) < 0.000001){
        return @"";
    }else{
        if([self.currencyUnit length] > 0){
            
            NSString *currencyDisplay = [self currencyForDisplay];
            return [currencyDisplay stringByAppendingFormat:@"%.2f", self.originalPrice];
        }else{
            return [NSString stringWithFormat:@"%.2f", self.originalPrice];
        }
        
    }
}

- (NSString *)currencyForDisplay{
    if([self.currencyUnit length] > 0){

        if([self.currencyUnit isEqualToString:@"USD"]){
            return @"$";
        }else{
            return self.currencyUnit;
        }
        
    }else{
        return @"";
    }
}


- (NSURL *)coverImageUrlWithWidth:(CGFloat)width height:(CGFloat)height
{
    if (self.images.count == 0) return nil;
    
    HGItemImage * coverImage = [self.images firstObject];
    NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:coverImage.imageLink width:width height:height];
    
    return [NSURL URLWithString:image_url];
}

- (CGSize)sizeForBoundingWidth:(CGFloat)width
{
    HGItemImage * coverImage = [self.images firstObject];
    return CGSizeMake(width, ceil(coverImage.height * width / coverImage.width) + WATER_FLOW_CELL_EXTEND_HEIGHT);

}

@end


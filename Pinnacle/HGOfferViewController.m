//
//  HGOfferViewController.m
//  Pinnacle
//
//  Created by shengyuhong on 15/2/3.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGOfferViewController.h"
#import "HGConstant.h"
#import <UIImageView+WebCache.h>
#import "HGAppData.h"
#import "HGChatViewController.h"
#import "HGUser.h"
#import "UIImage+pureColorImage.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGAppDelegate.h"

@interface HGOfferViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tfOfferPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnMakeOffer;

@property (nonatomic, strong) UILabel *lbPadding;
@property (nonatomic, strong) UIImageView *ivBackground;
@property (nonatomic, strong) UIView *coverBackground;

@end

@implementation HGOfferViewController


static BOOL bUserTouchBack = YES;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[HGUtils sharedInstance] gaTrackViewName:@"buy_view"];
    [FBSDKAppEvents logEvent:@"buy_view"];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage-title"]];
    
    self.ivBackground = [[UIImageView alloc]initWithFrame:self.view.bounds];
    self.ivBackground.contentMode = UIViewContentModeScaleAspectFill;
    self.ivBackground.clipsToBounds = YES;
    self.coverBackground = [[UIImageView alloc]initWithFrame:self.view.bounds];
    self.coverBackground.backgroundColor = [UIColor clearColor];
    [self.ivBackground addSubview:self.coverBackground];
    [self.view addSubview:self.ivBackground];
    [self.view sendSubviewToBack:self.ivBackground];
    
    int sugggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.view.bounds.size.width*2];
    int sugggestHeight = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.view.bounds.size.height*2];
    
    NSURL *imageURL = [self.item coverImageUrlWithWidth:sugggestWidth height:sugggestHeight];
    [self.ivBackground sd_setImageWithURL:imageURL placeholderImage:[UIImage imageWithColor:FANCY_COLOR(@"eaeaea")] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.coverBackground.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    }];
    
    self.tfOfferPrice.delegate = self;
    
    self.tfOfferPrice.clipsToBounds = YES;
    self.tfOfferPrice.layer.cornerRadius = 10.0f;
    self.tfOfferPrice.backgroundColor = [UIColor whiteColor];
    self.tfOfferPrice.textColor = FANCY_COLOR(@"666565");
    self.tfOfferPrice.font = [UIFont systemFontOfSize:56.0f];
    self.tfOfferPrice.keyboardType = UIKeyboardTypeDecimalPad;
    self.tfOfferPrice.tintColor = FANCY_COLOR(@"ffb985");
    self.tfOfferPrice.clearButtonMode = UITextFieldViewModeAlways;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMinimumFractionDigits:0];
    [formatter setMaximumFractionDigits:2];
    self.tfOfferPrice.text = [formatter stringFromNumber:[NSNumber numberWithFloat:self.item.price]];
    self.tfOfferPrice.textAlignment = NSTextAlignmentCenter;
    self.tfOfferPrice.placeholder = @"0.00";
    
    NSString *currencySymbol = [self.item currencyForDisplay];
    self.lbPadding = [[UILabel alloc]initWithFrame:CGRectMake(0,0,60,self.tfOfferPrice.frame.size.height)];
    self.lbPadding.font = self.tfOfferPrice.font;
    self.lbPadding.text = [NSString stringWithFormat:@"  %@", currencySymbol];
    self.lbPadding.textColor = self.tfOfferPrice.textColor;
    self.lbPadding.adjustsFontSizeToFitWidth = YES;
    self.lbPadding.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    
    self.tfOfferPrice.leftView = self.lbPadding;
    self.tfOfferPrice.leftViewMode = UITextFieldViewModeUnlessEditing;
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 44.0f)];
    toolbar.barStyle=UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onTouchDone)];
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpace, barButtonItem, nil]];
    self.tfOfferPrice.inputAccessoryView = toolbar;
    
    self.lbDescription.font = [UIFont systemFontOfSize:17.0f];
    self.lbDescription.textColor = [UIColor whiteColor];
    self.lbDescription.backgroundColor = [UIColor clearColor];
    self.lbDescription.numberOfLines = 2;
    self.lbDescription.lineBreakMode = NSLineBreakByTruncatingTail;
    self.lbDescription.textAlignment = NSTextAlignmentCenter;
    
    if (self.item && self.item.title==nil) {
        //crash defense.
        self.item.title = @"";
    }
    self.lbDescription.text = [NSLocalizedString(@"Enter your offer for ",nil) stringByAppendingString:self.item.title];
    
    [self.btnMakeOffer setTitle:NSLocalizedString(@"Make offer",nil) forState:UIControlStateNormal];
    self.btnMakeOffer.titleLabel.font = [UIFont boldSystemFontOfSize:36];
    [self.btnMakeOffer setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnMakeOffer setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    [self.btnMakeOffer setTitleShadowColor:[UIColor colorWithRed:0x71/255.0f green:0x33/255.0f blue:0x06/255.0f alpha:0.17f] forState:UIControlStateNormal];
    [self.btnMakeOffer setTintAdjustmentMode:UIViewTintAdjustmentModeAutomatic];
    self.btnMakeOffer.titleLabel.shadowOffset  = CGSizeMake(1.0f, 1.0f);
    [self.btnMakeOffer setBackgroundImage:[UIImage imageNamed:@"btn-make-offer"] forState:UIControlStateNormal];
    [self.btnMakeOffer setBackgroundImage:[UIImage imageNamed:@"btn-press-make-offer"] forState:UIControlStateDisabled];
    [self.btnMakeOffer addTarget:self action:@selector(onTouchMakeOffer) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    bUserTouchBack = YES;
}
- (void)viewWillDisappear:(BOOL)animated{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMinimumFractionDigits:0];
    [formatter setMaximumFractionDigits:2];
    self.tfOfferPrice.text = [formatter stringFromNumber:[NSNumber numberWithFloat:self.item.price]];
    [super viewWillDisappear:animated];
    
    
    if (bUserTouchBack) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"buy_view" action:@"buy_back" label:nil value:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    float price = [textField.text floatValue];
    if( price < (self.item.price / 3) ){
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Price is too low, please make a reasonable price!", nil)];
        return NO;
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *expression = @"^([0-9]{1,7})?(\\.([0-9]{1,2})?)?$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
    if (numberOfMatches == 0){
        return NO;
    }
    return YES;
}

#pragma mark - Helpers

- (void)onTouchDone{
    [self.tfOfferPrice resignFirstResponder];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"buy_view" action:@"changeoffer" label:nil value:nil];
    
}

- (void)onTouchMakeOffer{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"buy_view" action:@"make_offer" label:nil value:nil];
    bUserTouchBack = NO;
    
    
    self.btnMakeOffer.enabled = NO;
    if (!(self.item && self.item.uid != nil && self.item.seller.uid)) {
        return;
    }

    
    if (self.rf_tag !=nil && self.rf_tag.length > 0) {
        
    }
    else
    {
        self.rf_tag = @"";
    }
    
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"make_offer/" parameters:@{@"price":self.tfOfferPrice.text, @"item_id":self.item.uid, @"to_user":self.item.seller.uid, @"text":[self generateMessage], @"rf_tag":self.rf_tag} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [FBSDKAppEvents logEvent:@"offer_send"];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"offer_send" withValue:@""];

        
        self.offerLineID = [responseObject objectForKey:@"offerline_id"];
        
        HGChatViewController *vcChat = [[UIStoryboard messageStoryboard]
                                        instantiateViewControllerWithIdentifier:@"VCChat"];
        vcChat.bFromItemDetailPage = YES;
        vcChat.offerLineID = self.offerLineID;
        vcChat.item = self.item;
        vcChat.toUser = self.item.seller;
        vcChat.bBuyer = YES;
        
        [self.navigationController pushViewController:vcChat animated:YES];
    
        self.btnMakeOffer.enabled = YES;
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Failed to make offer!");
        self.btnMakeOffer.enabled = YES;
    }];
}

- (NSString *)generateMessage{
    
    NSString *message;
    
    NSString *currencySymbol = [self.item currencyForDisplay];
    float price = [self.tfOfferPrice.text floatValue];
    
    if(fabs(price - 0) < 0.000001){
        message = NSLocalizedString(@"I would like to buy this listing for free.", nil);
    }else{
        message = [NSString stringWithFormat:NSLocalizedString(@"I would like to buy this listing for %1$@%2$.2f.", nil), currencySymbol, price];
    }
    
    return message;
    
}



@end



//
//  HGResourceListController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-28.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <JDStatusBarNotification.h>
#import "SVPullToRefresh.h"

@interface HGResourceListController : UITableViewController

@property (nonatomic, strong) NSString * endpoint;
@property (nonatomic, strong) NSDictionary * params;
@property (nonatomic, strong) NSString * nextLink;
@property (nonatomic, strong) NSMutableArray * objects;

- (void)commonSetup;
- (void)fillObjectsWithServerDataset:(NSArray *)dataset;
- (NSString *)titleForNoMore;
-(void)getServerData;

@end

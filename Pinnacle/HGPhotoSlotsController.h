//
//  HGPhotoSlotsController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-16.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HGPhotoSlotsControllerDelegate <NSObject>

- (void)handleTapAddPhoto;
- (void)handleTapPhotoImageAtSlot:(NSInteger)slotIndex;

@end

@interface HGPhotoSlotsController : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray * images;
@property (nonatomic, assign) id<HGPhotoSlotsControllerDelegate> delegate;
@property (nonatomic, assign) NSUInteger availableSlotNum;

- (BOOL)addImage:(UIImage *)image withUUID:(NSString *)imgUUID withRomoteUrl:(NSString*)remoteUrl;
-(BOOL)addImage:(NSString*)fakeImageUUID withRemoteUrl:(NSString*)remoteUrl;

- (NSString *)removeImageAtIndex:(NSUInteger)index;
- (void)insertImageAsFirstCover:(NSMutableDictionary*)imgUUID;
@end

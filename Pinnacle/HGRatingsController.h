//
//  HGRatingsController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGResourceListController.h"
#import "HGRatingCell.h"
#import "BHInputToolbar.h"
@interface HGRatingsController : UIViewController <HGRatingCellDelegate, UIActionSheetDelegate, BHInputToolbarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSString* uid;
@property (nonatomic, strong) NSString* currentDisplayUserName;

@property (nonatomic, strong) UIView* headView;
@property (nonatomic, strong) UIImageView* ratingResultView;
@property (nonatomic, strong) UILabel* ratingCountLabel;
@end

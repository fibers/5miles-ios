//
//  HGFollowButton.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGStrokeButton.h"

typedef NS_ENUM(NSInteger, HGFollowButtonType) {
    HGFollowButtonTypeFollowing,
    HGFollowButtonTypeNotFollowing,
    HGFollowButtonTypeNormal
};

@interface HGFollowButton : HGStrokeButton

@property (nonatomic, assign) HGFollowButtonType displayType;
@property (nonatomic, strong) UIImageView* icon;
@property (nonatomic, strong) UILabel* myTitle;
@end

//
//  HGCampaignsObject.m
//  Pinnacle
//
//  Created by Alex on 14-12-9.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGCampaignsObject.h"

@implementation HGCampaignsObject
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"CampaignObjectID",                                                   @"begin_at":@"begin_at",
        @"end_at":@"end_at",
                                                       @"image_width":@"image_width",
                                                       @"image_height":@"image_height",
                                                       @"name":@"name",
                                                       @"image_url":@"image_url",
                                                       @"click_action":@"click_action"}];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[@"id", @"begin_at", @"end_at", @"image_width", @"image_height",
                        @"name", @"image_url", @"click_action"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    
    return NO;
}
@end

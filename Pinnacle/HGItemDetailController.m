//
//  HGItemDetailController.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-20.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemDetailController.h"
#import "UserReportTableViewController.h"
#import "ItemDetailLikersCell.h"
#import "HGSearchResultsController.h"
#import "SectionFooterCollectionReusableView.h"
#import "ItemLikers.h"
#import "ItemLikersDetailViewController.h"
#import "HGAppData.h"
#import "HGProfilePhoneViewController.h"
#import "IBActionSheet.h"
#import "JSONKit.h"
#import "UIStoryboard+Pinnacle.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "HGItemImageViewerViewController.h"
#import "FBlikeReminderViewController.h"

static NSString * const GlobalHeaderID = @"GlobalHeader";
static NSString * const SectionTitleID = @"SectionTitle";

static NSString * const SimilarSectionFooter =@"similarsectionFooter";

static NSString * const SectionSwitchID = @"SectionSwitch";
static NSString * const HeaderCellID = @"ItemHeaderCell";
static NSString * const OfferCellID = @"ItemOfferCell";
static NSString * const MoreCellID = @"MoreCell";
static NSString * const SimilarItemCellID = @"SimilarItemCell";
static NSString * const ItemDetailLikersCellID = @"ItemDetailLikersCellID";
static int section_likers_index= 1;

#define DEFAULT_ITEM_DETAIL_IMAGE_COUNT 1
//static int ReportActionTag = 34;
static int EditButtonActionTag = 36;
static int UnapprovedEditActionTag = 37;
static int SoldItemEditActionTag = 38;
static int PendingItemEditActionTag = 39;

///////////////////////////////////////
static int userActionHeight = 50;
static int flyingUserActinoView_offst = 0;

@interface HGItemDetailController ()<FBLikerReminderCloseDelegate>

@property(nonatomic, strong)IBActionSheet* editActionSheet;
@end
@implementation HGItemDetailController




-(void)customizeRightButton_share
{
    UIButton*rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,30,30)];
    [rightButton addTarget:self action:@selector(onTouchShareButton) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:[UIImage imageNamed:@"item_detail_share"]forState:UIControlStateNormal];
    UIBarButtonItem*rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem= rightItem;
}

-(void)addUserActionView
{
    const CGFloat kLikeButtonWidth = 40.f;
    const CGFloat kAskButtonWidth = 106;
    const CGFloat kSpacing = 8;
    
    self.flyingUserActionView = [[UIView alloc] initWithFrame:CGRectMake(0, flyingUserActinoView_offst, self.view.frame.size.width, userActionHeight)];
    self.flyingUserActionView.backgroundColor = [UIColor whiteColor];
    self.button_like = [[UIButton alloc] initWithFrame:CGRectMake(kSpacing, 5, kLikeButtonWidth, kLikeButtonWidth)];
    self.button_like.layer.cornerRadius = 4;
    self.button_like.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
    self.button_like.layer.borderWidth = 1;
    [self.flyingUserActionView addSubview:self.button_like];
    [self.button_like setBackgroundImage:[UIImage imageNamed:@"item_detail_like"] forState:UIControlStateNormal];
    
    
    if ([self.item.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID]){
        self.button_like.hidden = YES;
    }
    else{
        self.button_like.hidden = NO;
    }
    
    CGFloat makeOfferButtonWidth = SCREEN_WIDTH - 4 * kSpacing - kLikeButtonWidth - kAskButtonWidth;
    self.button_buy = [[UIButton alloc] initWithFrame:CGRectMake(kSpacing * 2 + kLikeButtonWidth, 5, makeOfferButtonWidth, 40)];
    self.button_ask = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - kAskButtonWidth - kSpacing, 5, kAskButtonWidth, 40)];
    
    [self.flyingUserActionView addSubview:self.button_ask];
    [self.flyingUserActionView addSubview:self.button_buy];
    
    self.flyingUserActionView.backgroundColor = [UIColor whiteColor];
    
    self.button_buy.backgroundColor = [UIColor whiteColor];
    self.button_buy.layer.cornerRadius = 4.0;
    self.button_buy.layer.borderWidth = 1;
    self.button_buy.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    [self.button_buy setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    self.button_buy.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.button_buy setTitle:NSLocalizedString(@"Make offer",nil) forState:UIControlStateNormal];

    self.button_ask.backgroundColor = [UIColor whiteColor];
    self.button_ask.layer.cornerRadius = 4.0;
    self.button_ask.layer.borderWidth = 1;
    self.button_ask.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
    [self.button_ask setTitleColor:SYSTEM_DEFAULT_FONT_COLOR_1 forState:UIControlStateNormal];
    self.button_ask.titleLabel.font = [UIFont systemFontOfSize:17];
    
    self.button_askIcon = [[UIImageView alloc] initWithFrame:CGRectMake(20, 12, 44/2, 32/2)];
    self.button_askIcon.image = [UIImage imageNamed:@"item_detail_ask_icon"];
    
    if ([[[HGUtils sharedInstance] getPreferLanguage] isEqualToString:@"en"]
        || [[[HGUtils sharedInstance] getPreferLanguage] isEqualToString:@"zh-Hans"]) {
        ///only English shows the icon
        const CGFloat spacing = 6.f;
        [self.button_ask setImage:[UIImage imageNamed:@"item_detail_ask_icon"] forState:UIControlStateNormal];
        [self.button_ask setTitle:@"Ask" forState:UIControlStateNormal];
        self.button_ask.titleEdgeInsets = UIEdgeInsetsMake(0, spacing / 2, 0, -spacing / 2);
        self.button_ask.imageEdgeInsets = UIEdgeInsetsMake(0, -spacing / 2, 0, spacing / 2);
    }
    else{
        [self.button_ask setTitle:@"Ask" forState:UIControlStateNormal];
    }
    
    [self.view addSubview:self.flyingUserActionView];
    
    
    
    [self.button_like addTarget:self action:@selector(onTouchLikeButton) forControlEvents:UIControlEventTouchUpInside];
    
    [self.button_buy addTarget:self action:@selector(onTouchMakeOfferButton) forControlEvents:UIControlEventTouchUpInside];
    
    [self.button_ask addTarget:self action:@selector(onTouchAskButton) forControlEvents:UIControlEventTouchUpInside];
    
    /////////////////////////////////
    self.SeperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, self.flyingUserActionView.frame.origin.y + userActionHeight, self.view.frame.size.width, 2)];
    self.SeperatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15];
    [self.view addSubview:self.SeperatorView];
    
    self.SeperatorView.hidden = YES;
    self.flyingUserActionView.hidden = YES;
}



-(void)addMapview
{
    //if the item do not have lat lon, not show the map.
    if (self.itemDetail.geoLocation.lat == 0 || self.itemDetail.geoLocation.lon == 0) {
        return;
    }
    
    CGPoint center = CGPointMake(0, 0);
    int DisplayDist = METERS_PER_MILE;
    double templat = [[HGUtils sharedInstance] getPreferLat];
    double templon = [[HGUtils sharedInstance] getPreferLon];
    
    
    if (fabs(templat-0.0) < 0.000001 || fabs(templon - 0.0) < 0.000001) {
        center.x = self.itemDetail.geoLocation.lat;
        center.y = self.itemDetail.geoLocation.lon;
        DisplayDist = METERS_PER_MILE;
    }
    else{
        center.x = (templat + self.itemDetail.geoLocation.lat)/2;
        center.y = (templon + self.itemDetail.geoLocation.lon)/2;
        CLLocation *orig=[[CLLocation alloc] initWithLatitude:self.itemDetail.geoLocation.lat longitude:self.itemDetail.geoLocation.lon];
        CLLocation* dist = [[CLLocation alloc] initWithLatitude:templat longitude:templon];
        
        DisplayDist =[orig distanceFromLocation:dist];
        if (DisplayDist < METERS_PER_MILE ) {
            DisplayDist = METERS_PER_MILE;
        }
    }
    // Cooridinate
    MKCoordinateRegion region;
    
    region.center.latitude = center.x;
    region.center.longitude = center.y;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.055;
    span.longitudeDelta = 0.055;
    region.span = span;
    
    int timer = 3.5;
    
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(region.center, DisplayDist*timer* 120/self.view.frame.size.width, DisplayDist*timer );
    
    if (viewRegion.span.latitudeDelta > 180 || viewRegion.span.longitudeDelta > 180) {
        if (viewRegion.span.longitudeDelta > 180) {
            viewRegion.span.longitudeDelta = 180;
            viewRegion.span.latitudeDelta = 180* 120/self.view.frame.size.width;
        }
    }
    if(viewRegion.span.latitudeDelta < 0 || viewRegion.span.longitudeDelta < 0)
    {
        viewRegion.span.longitudeDelta = 180;
        viewRegion.span.latitudeDelta = 180* 120/self.view.frame.size.width;
    }
    if (viewRegion.span.latitudeDelta > 100 || viewRegion.span.longitudeDelta > 100) {
        viewRegion.center.latitude = self.itemDetail.geoLocation.lat;
        viewRegion.center.longitude = self.itemDetail.geoLocation.lon;
    }
    
    MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
    options.region = viewRegion;
    options.scale = [UIScreen mainScreen].scale;
    options.size = CGSizeMake(self.view.frame.size.width, 120);
    
    
    MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
    
    dispatch_queue_t executeOnBackground = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    [snapshotter startWithQueue:executeOnBackground completionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                self.headcell.mapImage.backgroundColor = [UIColor colorWithPatternImage:snapshot.image];
                [self clearMapSubviews];
                [self addMapItemIcon:viewRegion withMainImage:snapshot.image];
                [self addMapUserIcon:viewRegion withMainImage:snapshot.image];
                
            }
        });
    }];
}
-(void)clearMapSubviews
{
    NSArray* views = [self.headcell.mapImage subviews];
    for(UIView* subview in views)
    {
        [subview removeFromSuperview];
    }
}
-(void)addMapUserIcon:(MKCoordinateRegion)viewRegion withMainImage:(UIImage*)mainImage
{
    UIImageView* userIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 74/2, 104/2)];
    userIcon.image =[UIImage imageNamed:@"map_user_location_big"];
    
    CGPoint mapCenter = CGPointMake(self.headcell.staticMapView.frame.size.width/2,self.headcell.staticMapView.frame.size.height/2);
    CGPoint center = CGPointMake(0, 0);
    double templat = [[HGUtils sharedInstance] getPreferLat];
    double templon = [[HGUtils sharedInstance] getPreferLon];
    
    double xDelta = self.headcell.staticMapView.frame.size.width * (templon - viewRegion.center.longitude)/viewRegion.span.longitudeDelta;
    double yDelta = self.headcell.staticMapView.frame.size.height *(templat - viewRegion.center.latitude)/viewRegion.span.latitudeDelta;
    yDelta = -1 * yDelta;
    
    
    center.x = mapCenter.x + xDelta;
    center.y = mapCenter.y + yDelta - 94/8;
    userIcon.center = center;
    
    [self.headcell.mapImage addSubview:userIcon];
    /*NSString*x1= [ @"" stringByAppendingFormat:@"%f", center.x ];
     NSString*y1 =[@"" stringByAppendingFormat:@"%f",center.y];
     UIImage* resultImage = [[HGUtils sharedInstance] mergedImageOnMainImage:mainImage WithImageArray:@[userIcon.image] AndImagePointArray:@[x1,y1]];
     return resultImage;
     */
    
}
-(void)addMapItemIcon:(MKCoordinateRegion)viewRegion withMainImage:(UIImage*)mainImage
{
    UIImageView* itemIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 166/2, 166/2)];
    itemIcon.image =[UIImage imageNamed:@"map_item_location_icon"];
    
    CGPoint mapCenter = CGPointMake(self.headcell.staticMapView.frame.size.width/2,self.headcell.staticMapView.frame.size.height/2);
    CGPoint center = CGPointMake(0, 0);
    
    double xDelta = self.headcell.staticMapView.frame.size.width * (self.itemDetail.geoLocation.lon - viewRegion.center.longitude)/viewRegion.span.longitudeDelta;
    double yDelta = self.headcell.staticMapView.frame.size.height *(self.itemDetail.geoLocation.lat - viewRegion.center.latitude)/viewRegion.span.latitudeDelta;
    yDelta = -1 * yDelta;
    
    
    center.x = mapCenter.x + xDelta;
    center.y = mapCenter.y + yDelta;
    itemIcon.center = center;
    
    [self.headcell.mapImage addSubview:itemIcon];
    
    /*NSString*x1= [ @"" stringByAppendingFormat:@"%f", center.x ];
     NSString*y1 =[@"" stringByAppendingFormat:@"%f",center.y];
     UIImage* resultImage = [[HGUtils sharedInstance] mergedImageOnMainImage:mainImage WithImageArray:@[itemIcon.image] AndImagePointArray:@[x1,y1]];
     return resultImage;
     */
}

-(void)customizeBackButton
{
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    self.navigationItem.backBarButtonItem = backbutton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = YES;
    [self customizeBackButton];
    [self customizeRightButton_share];
    self.likersArray = [[NSMutableArray alloc] init];
    self.sellingItems = [[NSMutableArray alloc] init];
    self.item_SellerUserID = @"";
    self.similarItems = [[NSMutableArray alloc] init];
    self.offerLines = [[NSMutableArray alloc] init];
    self.MD5_recommendItems = @"";
    self.MD5_ItemDetail = @"";
    self.MD5_SellerOtherItems = @"";
    
    [self initCollection];
    
    [self getItemLikers];
    [self addSuggestItemsSegment];
    [self addUserActionView];
    [self addReporterButton];
    
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage-title"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMyItemEdit:) name:@"Notification.Item.Edit" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onPhoneVerifySuccess:)
                                                 name:HGProfilePhoneViewControllerVerifySuccessNotification
                                               object:nil];
    
    if(self.bFromSellingPage){
        @weakify(self);
        [[[RACObserve([HGAppData sharedInstance], bSellingFBShareSuccess) skip:1] take:1] subscribeNext:^(id x) {
            @strongify(self);
            if([HGUserData sharedInstance].bSellingFBShare){
                if([x boolValue]){
                    [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Item has been shared.",nil) onView:self.view];
                }else{
                    [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Share listing failed.",nil) onView:self.view];
                }
            }
        }];
    }
    
}
-(void)initCollection
{
    HGItemDetailLayout * layout = [HGItemDetailLayout new];
    layout.delegate = self;
    
    
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    
    
    [self.collectionView registerClass:[HGSectionTitleReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:SectionTitleID];
    [self.collectionView registerClass:[SectionFooterCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:SimilarSectionFooter];
    
    
    
    [self.collectionView registerClass:[HGItemHeaderCell class] forCellWithReuseIdentifier:HeaderCellID];
    [self.collectionView registerClass:[HGItemOfferCell class] forCellWithReuseIdentifier:OfferCellID];
    [self.collectionView registerClass:[HGHomeItemCell class] forCellWithReuseIdentifier:SimilarItemCellID];
    [self.collectionView registerClass:[ItemDetailLikersCell class] forCellWithReuseIdentifier:ItemDetailLikersCellID];
}
-(void)showVerifyRemind
{
    if ([HGUserData sharedInstance].bShowVerifyRemind){
        [HGUserData sharedInstance].bShowVerifyRemind = NO;
        HGInstructViewController * instructionViewController = [[HGInstructViewController alloc] initWithNibName:@"HGInstructViewController" bundle:nil];
        instructionViewController.AdditionalViewType = HGInstructViewType_VerifyIntro;
        instructionViewController.delegate = self;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            //ios 8
            instructionViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        }
        else{
            //
            HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
            
            application.tabController .modalPresentationStyle = UIModalPresentationCurrentContext;
            self.modalPresentationStyle = UIModalPresentationFormSheet;
            
        }
        
        HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
        [application.tabController presentViewController:instructionViewController animated:NO completion:nil];
        
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        if(instructionViewController != nil)
        {
            [dict setObject:instructionViewController forKey:@"instructionViewController"];
        }
        
        
    } else if ([HGUserData sharedInstance].showServiceVerifyRemind) {
        [HGUserData sharedInstance].showServiceVerifyRemind = NO;
        
        HGProfilePhoneViewController *viewcontroller = [[HGProfilePhoneViewController alloc] initWithNibName:@"HGProfilePhoneViewController" bundle:nil];
        viewcontroller.fromPage = HGProfilePhoneViewControllerFromSellPage;
        
        UINavigationController *naviController = [[UINavigationController alloc] initWithRootViewController:viewcontroller];
        
        HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
        [application.tabController presentViewController:naviController animated:YES completion:nil];
    }
}

- (void)onPhoneVerifySuccess:(NSNotification *)notification
{
    [self getDetailInformation];
}

-(void)onStartVerifyAction:(id)sender{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"Go_to_verify" label:nil value:nil];
    HGInstructViewController *instructionViewController = (HGInstructViewController*)sender;
    [instructionViewController dismissViewControllerAnimated:NO completion:nil];
    HGProfilePhoneViewController *viewcontroller = [[HGProfilePhoneViewController alloc] initWithNibName:@"HGProfilePhoneViewController" bundle:nil];
    
    [self.navigationController pushViewController:viewcontroller animated:YES];
    
    
}
-(void)onClickOutOfButton:(id)sender{
    HGInstructViewController *instructionViewController = (HGInstructViewController*)sender;
    [instructionViewController dismissViewControllerAnimated:NO completion:nil];
}
-(void)addReporterButton
{
    self.reportView = [[UIView alloc] initWithFrame:CGRectMake(0, 8, self.view.frame.size.width, 42)];
    self.reportView.backgroundColor = [UIColor whiteColor];
    
    
    self.reporterButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width, 39)];
    self.reporterButton.backgroundColor = [UIColor whiteColor];//FANCY_COLOR(@"f0f0f0");
    [self.reporterButton setTitle:NSLocalizedString(@"Report this listing",nil) forState:UIControlStateNormal];
    self.reporterButton.titleLabel.font = [UIFont systemFontOfSize: 15];
    self.reporterButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    [self.reporterButton setTitleColor:FANCY_COLOR(@"242424") forState:UIControlStateNormal];
    [self.reporterButton addTarget:self action:@selector(onTouchReportButton) forControlEvents:UIControlEventTouchUpInside];
    
    [self.reportView addSubview:self.reporterButton];
}

-(void)getItemLikers
{
    if (self.item.uid==nil) {
        return;
    }
    
    NSNumber* limit = [[NSNumber alloc] initWithInt:13];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"item_likers/" parameters:@{@"item_id": self.item.uid, @"limit":limit } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError * error;
        [self.likersArray removeAllObjects];
        
        NSDictionary* metaData = [responseObject objectForKey:@"meta"];
        NSNumber* likerTotalNumer =  [metaData objectForKey:@"total_count"];
        
        self.likerTotalcount = likerTotalNumer.intValue;
        NSArray* likerArray = [responseObject objectForKey:@"objects"];
        if (likerArray != nil && likerArray.count > 0) {
            for (NSDictionary * dictObj in likerArray) {
                ItemLikers * user = [[ItemLikers alloc] initWithDictionary:dictObj error:&error];
                if(user!=nil)
                {
                    [self.likersArray addObject:user];
                }
                
            }
        }
        NSMutableIndexSet *idxSet = [[NSMutableIndexSet alloc] init];
        [idxSet addIndex:section_likers_index];
        [self.collectionView reloadSections:idxSet];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get item likers failed: %@", error);
    }];
    
    
}
-(void)getDetailInformation
{
    
    NSString *itemID;
    if( self.itemDetail ){
        itemID = self.itemDetail.uid;
    }else if( self.item){
        itemID = self.item.uid;
        self.itemDetail = [[HGItemDetail alloc] init];
        self.itemDetail.title = self.item.title;
        self.itemDetail.images = self.item.images;
        self.itemDetail.state = self.item.state;
        
    }else{
        itemID = @"O6zdMEg5VJ42BwXG";
    }
    
    
    if (itemID ==nil) {
        return;
    }
    
    if (self.trackstring !=nil && self.trackstring.length > 0) {
        
    }
    else
    {
        self.trackstring = @"";
    }
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"item_detail/" parameters:@{@"item_id": itemID,@"rf_tag":self.trackstring} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError * error;
        [SVProgressHUD dismiss];
        NSString *jsonString = [[NSString alloc] initWithData:[responseObject JSONData] encoding:NSUTF8StringEncoding];
        NSString* currentMD5 = [[HGUtils sharedInstance] md5HexDigest:jsonString];
        
        @try{
            self.itemDetail = [[HGItemDetail alloc] initWithDictionary:responseObject error:&error];            
        }
        @catch(NSException *exception) {
            [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
        }
        
        
        [FBSDKAppEvents logEvent: [@"category_" stringByAppendingFormat:@"%d", self.itemDetail.categoryIndex]];
        
        [self configFlyingActinoView];
        [self addMapview];
        [self initUpdateButtonTimer];
        
        [self.offerLines removeAllObjects];
        [self.offerLines addObjectsFromArray:self.itemDetail.offerLines];
        
        if (![self.MD5_ItemDetail isEqualToString:currentMD5]) {
            self.MD5_ItemDetail = currentMD5;
            NSLog(@"collection reload data");
            [self.collectionView reloadData];
        }
        
        
        if (![self.itemDetail.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
            self.item_SellerUserID = self.itemDetail.seller.uid;
            if (self.item_SellerUserID !=nil) {
                [self getUserOtherSellingItems];
            }
            [self _loadRecommendations];
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get item detail failed: %@", error);
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

static int updateTimerCounter = 0;
-(void)initUpdateButtonTimer
{
    if (self.itemDetail.renew_ttl != 0 && self.itemDetail.state != ITEM_STATE_UNAPPROVED) {
        updateTimerCounter = 0;
        if (self.updateButtonTimer != nil) {
            [self.updateButtonTimer invalidate];
            self.updateButtonTimer = nil;
        }
        self.updateButtonTimer = [NSTimer scheduledTimerWithTimeInterval: 1 target: self selector: @selector(updateTimerCallBack) userInfo: nil repeats: YES];
        [self setUpdateButtonTimeLabel:(int)self.itemDetail.renew_ttl];
    }
    else
    {
        if (self.itemDetail.state == ITEM_STATE_LISTING || self.itemDetail.state == ITEM_STATE_PENDING) {
            [self.headcell resetUpdateButton];
        }
        else{
            [self.headcell DimRenewButton];
        }
    }
    
}
-(void)updateTimerCallBack
{
    updateTimerCounter ++;
    int timerLeft = (int)self.itemDetail.renew_ttl - updateTimerCounter;
    if (timerLeft <= 0) {
        if (self.updateButtonTimer != nil) {
            [self.updateButtonTimer invalidate];
            self.updateButtonTimer = nil;
        }
        [self.headcell resetUpdateButton];
    }
    else{
        [self setUpdateButtonTimeLabel:timerLeft];
    }
}
-(void)setUpdateButtonTimeLabel:(int)timeLeft
{
    NSDateComponents *comp = [[NSDateComponents alloc]init];
    [comp setMonth:01];
    [comp setDay:01];
    [comp setYear:2001];
    NSCalendar *myCal = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *myDate1 = [myCal dateFromComponents:comp];
    NSLog(@"myDate1 = %@",myDate1);
    NSDate *myDate2 = [myDate1 dateByAddingTimeInterval:(NSTimeInterval)timeLeft];
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"HH:mm:ss"];
    NSString* dateString=[fmt stringFromDate:myDate2];
    self.headcell.button_renew_timelabel.text = dateString;
}
-(void)configFlyingActinoView
{
    
    if (self.itemDetail.liked) {
        [self.button_like setImage:[UIImage imageNamed:@"item_detail_already_liked"] forState:UIControlStateNormal];
    }
    else{
        [self.button_like setImage:[UIImage imageNamed:@"item_detail_like"] forState:UIControlStateNormal];
    }
    
    
    if (self.itemDetail.state != ITEM_STATE_LISTING) {
        self.button_buy.layer.borderColor = FANCY_COLOR(@"b9b9b9").CGColor;
        self.button_buy.userInteractionEnabled = NO;
        [self.button_buy setTitleColor:FANCY_COLOR(@"b9b9b9") forState:UIControlStateNormal];
        
        self.button_ask.userInteractionEnabled = NO;
        [self.button_ask setTitleColor:FANCY_COLOR(@"b9b9b9") forState:UIControlStateNormal];
        UIImage* originImage =[UIImage imageNamed:@"item_detail_ask_icon"];
        UIImage* tintedImage = [UIImage getTintedImage:originImage withColor:FANCY_COLOR(@"b9b9b9")];
        self.button_askIcon.image =tintedImage;
        
        [self.button_ask setImage:tintedImage forState:UIControlStateNormal];
    }
    else{
        [self.button_buy addTarget:self action:@selector(onTouchMakeOfferButton) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([self.itemDetail.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID]){
        self.button_like.hidden = YES;
    }
    else{
        self.button_like.hidden = NO;
    }
}
-(void)getUserOtherSellingItems
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"user_sellings/" parameters:@{@"seller_id": self.item_SellerUserID} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSString *jsonString = [[NSString alloc] initWithData:[responseObject JSONData] encoding:NSUTF8StringEncoding];
        NSString* currentMD5 = [[HGUtils sharedInstance] md5HexDigest:jsonString];
        
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        
        [self.sellingItems removeAllObjects];
        for (id dictObj in results) {
            NSError * error = nil;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            if (!error && ![self.item.uid isEqualToString:item.uid]) {
                [self.sellingItems addObject:item];
            } else {
                //NSLog(@"shop item creation error: %@", error);
            }
        }
        if (![self.MD5_SellerOtherItems isEqualToString:currentMD5]) {
            self.MD5_SellerOtherItems = currentMD5;
            NSMutableIndexSet *idxSet = [[NSMutableIndexSet alloc] init];
            [idxSet addIndex:3];
            [self.collectionView reloadSections:idxSet];
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get user selling listings error: %@", error);
    }];
}

-(void)addSuggestItemsSegment
{
    self.suggestItemsSegment=[[UISegmentedControl alloc] initWithFrame:CGRectMake(10.0f, 60, self.view.frame.size.width-20, 30.0f) ];
    
    NSString* segString1 =  NSLocalizedString(@"Similar listings", nil);
    NSString* segString2 = NSLocalizedString(@"Seller's other listings", nil);
    [self.suggestItemsSegment insertSegmentWithTitle:segString1 atIndex:0 animated:YES];
    [self.suggestItemsSegment insertSegmentWithTitle:segString2 atIndex:1 animated:YES];
    self.suggestItemsSegment.momentary = NO;
    self.suggestItemsSegment.tintColor = FANCY_COLOR(@"242424");
    self.suggestItemsSegment.selectedSegmentIndex = 0;
    self.suggestItemsSegment.layer.borderWidth = 1;
    self.suggestItemsSegment.layer.borderColor = FANCY_COLOR(@"242424").CGColor;
    self.suggestItemsSegment.layer.cornerRadius = 6.0;
    self.suggestItemsSegment.clipsToBounds = YES;
    self.suggestItemsSegment.backgroundColor = FANCY_COLOR(@"f0f0f0");
    
    int suggestFontSize = 13;
    if(IPHONE6PLUS)
    {
        suggestFontSize = 15;
    }
    else
    {
        suggestFontSize = 13;
    }
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:FANCY_COLOR(@"242424"),NSForegroundColorAttributeName,  [UIFont fontWithName:@"Museo-300" size:suggestFontSize],NSFontAttributeName,nil];
    
    [self.suggestItemsSegment setTitleTextAttributes:dic forState:UIControlStateNormal];
    [self.suggestItemsSegment addTarget:self action:@selector(Selectbutton:) forControlEvents:UIControlEventValueChanged];
}
-(void)Selectbutton:(UISegmentedControl *)seg
{
    if (seg.selectedSegmentIndex == 0 )
    {
        NSLog(@"select segment index 0");
    }
    else if(seg.selectedSegmentIndex == 1)
    {
        NSLog(@"select segment index 1");
    }
    NSMutableIndexSet *idxSet = [[NSMutableIndexSet alloc] init];
    [idxSet addIndex:3];
    [self.collectionView reloadSections:idxSet];
    
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self getDetailInformation];
    
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
    [[HGUtils sharedInstance] gaTrackViewName:@"product_view"];
    [FBSDKAppEvents logEvent:@"product_view"];
    
    
    [self showVerifyRemind];
    [self.collectionView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [SVProgressHUD dismiss];
    [super viewWillDisappear:animated];
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"product_back" label:nil value:nil];
        
    }
    
    //[self resignFirstResponder];
    
    if ([self.headcell.audioPlayer play]) {
        [self.headcell stopPlaying];
        
    }
    
    if (self.updateButtonTimer != nil) {
        [self.updateButtonTimer invalidate];
        self.updateButtonTimer = nil;
    }
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Notification.Item.Edit" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.collectionView.delegate = nil;
    self.collectionView.dataSource = nil;
    if (self.headcell.updateSlideTimer != nil) {
        [self.headcell.updateSlideTimer invalidate];
        self.headcell.updateSlideTimer = nil;
        
    }
    if (self.updateButtonTimer != nil) {
        [self.updateButtonTimer invalidate];
        self.updateButtonTimer = nil;
    }
}
- (void)_loadRecommendations
{
    
    NSNumber * lat = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLat]];
    NSNumber * lon = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLon]];
    NSNumber* limitNumber = [NSNumber numberWithInt:6];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"suggest/" parameters:@{@"item_id":self.item.uid, @"lat":lat, @"lon":lon, @"limit":limitNumber} success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.similarItems removeAllObjects];
        NSString *jsonString = [[NSString alloc] initWithData:[responseObject JSONData] encoding:NSUTF8StringEncoding];
        NSString* currentMD5 = [[HGUtils sharedInstance] md5HexDigest:jsonString];
        
        
        for (NSDictionary * dictObj in responseObject) {
            
            NSError * error;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                [self.similarItems addObject:item];
                [HGTrackingData sharedInstance].suggest_rf_tag = [dictObj objectForKey:@"rf_tag"];
            }
            
            
            
        }
        if (![self.MD5_recommendItems isEqualToString:currentMD5]) {
            self.MD5_recommendItems = currentMD5;
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:3]];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get suggested listings failed: %@", error.localizedDescription);
    }];
}



#pragma mark - CollectionView Datasource & Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if ([self.itemDetail.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        return 3;
    }else{
        return 4 ;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            
        case 1:
            return 1;
        case 2:
            return self.itemDetail ? self.offerLines.count : 0;
        case 3:
        {
            if (self.suggestItemsSegment.selectedSegmentIndex == 0) {
                return self.similarItems.count;
            }
            else if(self.suggestItemsSegment.selectedSegmentIndex == 1)
            {
                return self.sellingItems.count;
            }
            
        }
            
        default:
            break;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            HGItemHeaderCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:HeaderCellID forIndexPath:indexPath];
            self.headcell = cell;
            cell.delegate = self;
            [cell configWithItemDetail:self.itemDetail];
            cell.imageViewer.delegate = self;
            return cell;
        }
        case 1:
        {
            //likers cell
            ItemDetailLikersCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:ItemDetailLikersCellID forIndexPath:indexPath];
            cell.backgroundColor = [UIColor whiteColor];
            cell.actionDelegate = self;
            [cell configCell:self.likersArray withTotalLikerCount:self.likerTotalcount];
            //cell con
            return cell;
        }
        case 2:
        {
            //offer cell
            HGItemOfferCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:OfferCellID forIndexPath:indexPath];
            BOOL hidderSepertorLine = NO;
            if (self.offerLines.count == 1 ||
                (self.offerLines.count == 2 && indexPath.row == 1)) {
                hidderSepertorLine = YES;
            }
            HGOfferLine * offerline = [self.offerLines objectAtIndex:indexPath.item];
            [cell configWithOfferLine:offerline withSeperatorLineHidder:hidderSepertorLine];
            
            
            return cell;
        }
            
            
        case 3:
        {
            //recommendation cell
            HGHomeItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:SimilarItemCellID forIndexPath:indexPath];
            
            
            if (self.suggestItemsSegment.selectedSegmentIndex == 0) {
                HGShopItem  * item = [self.similarItems objectAtIndex:indexPath.item];
                [cell configWithItem:item];
            }
            else if(self.suggestItemsSegment.selectedSegmentIndex == 1)
            {
                HGShopItem  * item = [self.sellingItems objectAtIndex:indexPath.item];
                [cell configWithItem:item];
            }
            
            return cell;
        }
            
        default:
            break;
    }
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"the collection kind is %@", kind);
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        HGSectionTitleReusableView * titleView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:SectionTitleID forIndexPath:indexPath];
        NSString * title = NSLocalizedString(@"Buy & Ask",nil);
        titleView.titleLabel.text = title;
        
        if (indexPath.section == 2) {
            //this is the offers section
            titleView.titleLabel.font = [UIFont fontWithName:FONT_TYPE_2 size:15];
            titleView.titleLabel.textColor = FANCY_COLOR(@"242424");
            NSRange  range = [title rangeOfString:@"&"];
            if (range.length > 0 ) {
                //self.facebookLoginButton a
                UIFont *font = [UIFont systemFontOfSize:15];
                UIColor* fontcolor = FANCY_COLOR(@"242424");
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:title];
                [attrString addAttribute:NSFontAttributeName value:font range:range];
                [attrString addAttribute:NSForegroundColorAttributeName value:fontcolor range:NSMakeRange(0, title.length)];
                [titleView.titleLabel setAttributedText:attrString];
            }
            
        }
        
        if (indexPath.section == 3) {
            //if it is similar section
            CGPoint centerPoint = CGPointMake(titleView.center.x,self.suggestItemsSegment.center.y);
            self.suggestItemsSegment.center = centerPoint;
            [titleView addSubview:self.suggestItemsSegment];
            titleView.titleLabel.text = @"";
            
            [titleView addSubview:self.reportView];
        }
        
        return titleView;
    }
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        SectionFooterCollectionReusableView * titleView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:SimilarSectionFooter forIndexPath:indexPath];
        if (indexPath.section == 3) {
            //if it is similar section
            //titleView.backgroundColor = [UIColor redColor];
            
            if ([self.itemDetail.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
                self.reporterButton.hidden = YES;
            }
            else{
                self.reporterButton.hidden = NO;
                [titleView addSubview:self.reporterButton];
            }
        }
        return titleView;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(BshowDetailPictures)
    {
        return;
    }
    self.navigationController.navigationBar.hidden = NO;
    
    switch (indexPath.section) {
        case 1:
        {
            //likers cell view;
            //do nothing
        }
            break;
        case 2:
        {
            NSLog(@"click offer line: %d", (int)indexPath.row);
            
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"offering" label:nil value:nil];
            HGOfferLine * offerLine = [self.offerLines objectAtIndex:indexPath.row];
            if (offerLine.diggable) {
                [self navigateToChatView:offerLine];
            }
        }
            break;
            
        case 3:
        {
            HGItemDetailController *vcItemDetail = (HGItemDetailController *)[[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:@"VCItemDetail"];
            if (self.suggestItemsSegment.selectedSegmentIndex == 0) {
                vcItemDetail.item = [self.similarItems objectAtIndex:indexPath.item];
            }
            else if(self.suggestItemsSegment.selectedSegmentIndex == 1)
            {
                vcItemDetail.item = [self.sellingItems objectAtIndex:indexPath.item];
            }
            
            vcItemDetail.trackstring = vcItemDetail.item.rf_tag;
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"similar_product" label:nil value:nil];
            [self.navigationController pushViewController:vcItemDetail animated:YES];
        }
            break;
            
            
            
        default:
            break;
    }
}

#pragma mark - HGItemDetailLayout Delegate

- (CGFloat)heightForHeaderAboveCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
{
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            NSString * itemDesc = self.itemDetail && self.itemDetail.desc ? self.itemDetail.desc : @"";
            UIFont * titleFont = [UIFont fontWithName:@"Helvetica-bold" size:15];
            CGRect titleRect = [self.itemDetail.title boundingRectWithSize: CGSizeMake(CGRectGetWidth(self.view.bounds) - 10, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: titleFont} context:NULL];
            
            CGRect rect = [itemDesc boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.view.bounds) - 10, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0]} context:NULL];
            if ([itemDesc isEqual:@""]) {
                rect=CGRectMake(0, 0, 0, 0);
            }
            int audioPlayRectHeight = 0;
            if (self.itemDetail.mediaLink.length > 0) {
                audioPlayRectHeight = ITEM_DETAIL_AUDIO_HEIGHT +10;
            }
            
            int mapDisplayHeight = 0;
            if (self.itemDetail.geoLocation.lat != 0 && self.itemDetail.geoLocation.lon != 0) {
                mapDisplayHeight = ITEM_DETAIL_MAP_HEIGHT;
            }
            CGSize cellSize =  CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame) + CGRectGetHeight(titleRect)+1+ CGRectGetHeight(rect) + 62+40 + IMAGEPAGE_Y_OFFSET + audioPlayRectHeight + ITEM_DETAIL_CREATETIME_HEIGHT+ ITEM_DETAIL_DELIVERY_HEIGHT + ITEM_DETAIL_LOCATION_HEIGHT + mapDisplayHeight);
            return cellSize;
        }
            
        case 1:
        {
            //likers view.
            if (self.likersArray.count<=7 && self.likersArray.count > 0 )
            {
                return CGSizeMake(CGRectGetWidth(collectionView.frame), 54);
            }else if(self.likersArray.count>7)
            {
                return CGSizeMake(CGRectGetWidth(collectionView.frame), 92);
            }else if(self.likersArray.count <= 0){
                return CGSizeZero;
            }
        }
        case 2:
        {
            //item offer
            return CGSizeMake(CGRectGetWidth(self.view.bounds), 44);
            
        }
            
        case 3:
        {
            if(self.suggestItemsSegment.selectedSegmentIndex == 0)
            {
                HGShopItem * product = [self.similarItems objectAtIndex:indexPath.item];
                return [product sizeForBoundingWidth:(CGRectGetWidth(self.view.frame) * 0.5 - 20)];
            }
            else if(self.suggestItemsSegment.selectedSegmentIndex == 1)
            {
                HGShopItem * product = [self.sellingItems objectAtIndex:indexPath.item];
                return [product sizeForBoundingWidth:(CGRectGetWidth(self.view.frame) * 0.5 - 20)];
                
            }
        }
            
        default:
            break;
    }
    return CGSizeZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 1:
            return 0;
        case 2:
            return self.itemDetail && self.offerLines.count > 0 ? 32 : 0;
            
        case 3:
            return 40 + 40 + 20;
            if (self.suggestItemsSegment.selectedSegmentIndex==0) {
                return self.similarItems.count > 0 ? 40 : 0;
            }
            else if(self.suggestItemsSegment.selectedSegmentIndex == 1)
            {
                return self.sellingItems.count > 0 ? 40 : 0;
            }
            
        default:
            break;
    }
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section
{
    switch (section) {
        case 3:
            return 0;
        default:
            break;
    }
    return 0;
}




- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section;
{
    return 7.5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section == collectionView.numberOfSections - 1) {
        //similar section
        HGItemDetailLayout *layout = (HGItemDetailLayout*)collectionViewLayout;
        layout.minimumColumnSpacing = 5;
        return UIEdgeInsetsMake(5, 5, 5, 4);
    } else {
        return UIEdgeInsetsZero;
    }
}

#pragma mark - Item Header Action Delegate

- (void)onUserBannerTapped
{
    if (BshowDetailPictures) {
        return;
    }
    if([self.itemDetail.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID])
    {
        //it is current user.
        HGUserViewController *vcUser = (HGUserViewController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCUser"];
        vcUser.user = [[HGUserData sharedInstance] currentUser];
        [self.navigationController pushViewController:vcUser animated:YES];
        
    }
    else{
        //not current user
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"product_seller" label:nil value:nil];
        [self performSegueWithIdentifier:@"ShowUserDetail" sender:self.itemDetail.seller];
        
    }
}

- (void)onDistanceButtonTapped
{
    if (BshowDetailPictures) {
        return;
    }
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"map" label:nil value:nil];
    [self performSegueWithIdentifier:@"ShowItemLocation" sender:nil];
}

-(void)onTouchBrandString:(NSString*)brandIDString withBrandString:(NSString *)brandName
{
    if (BshowDetailPictures) {
        return;
    }
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"product_brand" label:nil value:nil];
    NSArray* senderArray = @[brandIDString, brandName];
    
    HGSearchResultsController *vcSearchResults = (HGSearchResultsController *)[[UIStoryboard searchStoryboard]instantiateViewControllerWithIdentifier:@"VCSearchResults"];
    vcSearchResults.searchMode = HGSearchModeBrandID;
    vcSearchResults.searchPayload = @{@"brand_keyword": (NSString*)(senderArray[0])};
    vcSearchResults.title = (NSString*)(senderArray[1]);
    
    [self.navigationController pushViewController:vcSearchResults animated:YES];
    
}

-(void)onTouchLikeButton
{
    if([[HGUserData sharedInstance] isLoggedIn]){
        [self startLikeAction];
    }else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"like" label:nil value:nil];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"like_login" label:nil value:nil];
        
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startLikeAction)];
    }
}

-(void)onTouchShareButton
{
    [self startSystemShare];
}

-(void)onTouchMakeOfferButton
{
    if([[HGUserData sharedInstance] isLoggedIn]){
        [self startMakeOfferAction];
    }else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"buy" label:nil value:nil];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"buy_login" label:nil value:nil];
        
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startMakeOfferAction)];
    }
    
}

-(void)onTouchAskButton
{
    if([[HGUserData sharedInstance] isLoggedIn]){
        [self startAskAction];
    }else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"ask" label:nil value:nil];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"ask_login" label:nil value:nil];
        
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startAskAction)];
    }
    
}



-(void)startLikeAction
{
    if (!self.itemDetail || self.itemDetail.uid == nil) {
        return;
    }
    if (!self.itemDetail.liked) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"like" label:nil value:nil];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"like/" parameters:@{@"item_id": self.itemDetail.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
            self.itemDetail.liked = YES;
            
            
            [self.headcell.button_like setImage:[UIImage imageNamed:@"item_detail_already_liked"] forState:UIControlStateNormal];
            [self configFlyingActinoView];
            [self getItemLikers];
            
            [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Added to Likes", nil) onView:self.view];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"like item %@ failed: %@", self.item.uid, error.userInfo);
        }];
    } else {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"unlike" label:nil value:nil];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unlike/" parameters:@{@"item_id": self.itemDetail.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
            self.itemDetail.liked = NO;
            
            [self.headcell.button_like setImage:[UIImage imageNamed:@"item_detail_like"] forState:UIControlStateNormal];
            [self configFlyingActinoView];
            [self getItemLikers];
            
            NSDictionary* dict = @{@"item_id":self.itemDetail.uid};
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_ITEMS_UNLIKE object:dict];
            
            [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Removed from Likes", nil) onView:self.view];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"unlike item %@ failed: %@", self.item.uid, error.userInfo);
        }];
    }
    
}

static bool BshowDetailPictures = false;

-(void)startMakeOfferAction
{
    if (BshowDetailPictures) {
        return;
    }
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"buy" label:nil value:nil];
    for (HGOfferLine * offerline in self.offerLines) {
        if ([offerline.buyer.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
            [self performSegueWithIdentifier:@"ItemDetailToOffer" sender:offerline];
            return;
        }
    }
    [self performSegueWithIdentifier:@"ItemDetailToOffer" sender:nil];
}

-(void)startAskAction
{
    if (BshowDetailPictures) {
        return;
    }
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"ask" label:nil value:nil];
    for (HGOfferLine * offerline in self.offerLines) {
        if ([offerline.buyer.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
            [self navigateToChatView:offerline];
            return;
        }
    }
    
    [self navigateToChatView:nil];
    
}


#pragma mark - UIActionsheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == EditButtonActionTag)
    {
        if (buttonIndex==0) {
            [self startEditView];
        }else if (buttonIndex==1)
        {
            [self startMarkAsSoldAlert];
            
        }else if (buttonIndex==2)
        {
            if (self.itemDetail.state != ITEM_STATE_UNLISTED) {
                [self startUnlistAlert];
                
            }
            else{
                [self startRelistAction:nil];
            }
        }else if (buttonIndex==3)
        {
            if (EditModeHasDeleteButton == 1) {
                [self startDeleteAlert];
            }
        }
    }
    else if(actionSheet.tag == UnapprovedEditActionTag)
    {
        if (buttonIndex == 0) {
            [self startDeleteAlert];
        }
    }
    else if(actionSheet.tag == SoldItemEditActionTag)
    {
        if (buttonIndex == 0) {
            
            [self startRelistAction:^(){
                [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"This listing is available again.",nil) onView:self.view];
            }];
        }
        if (buttonIndex == 1) {
            [self startDeleteAlert];
        }
    }
    else if (actionSheet.tag == PendingItemEditActionTag) {
        if (buttonIndex == 0) {
            HGProfilePhoneViewController *viewcontroller = [[HGProfilePhoneViewController alloc] initWithNibName:@"HGProfilePhoneViewController" bundle:nil];
            
            viewcontroller.fromPage = HGProfilePhoneViewControllerFromDetailPage;
            
            [self.navigationController pushViewController:viewcontroller animated:YES];
//            HGProfilePhoneViewController *viewcontroller = [[HGProfilePhoneViewController alloc] initWithNibName:@"HGProfilePhoneViewController" bundle:nil];
//            viewcontroller.isFromServiceVerify = YES;
//            
//            UINavigationController *naviController = [[UINavigationController alloc] initWithRootViewController:viewcontroller];
//            
//            HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
//            [application.tabController presentViewController:naviController animated:NO completion:nil];
        } else if (buttonIndex == 1) {
            [self startEditView];
        } else if (buttonIndex == 2) {
            [self startDeleteAlert];
        }
    }
}
-(void)startMarkAsSoldAlert
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"markassold" label:nil value:nil];
    int nOfferCount = (int)self.offerLines.count;
    NSString* contentString = @"";
    if (nOfferCount == 1) {
        contentString = NSLocalizedString(@"1 person has messaged you about this listing and will be notified. Continue?",nil);
    }
    else if(nOfferCount > 1){
        contentString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d people have messaged you about this listing and will be notified. Continue?",nil),nOfferCount];
    }
    else if(nOfferCount == 0)
    {
        contentString = [@"" stringByAppendingString:NSLocalizedString(@"Your listing will be marked as sold and no further messages will be accepted. Continue?",nil)];
    }
    
    PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Mark listing as Sold?",nil) message:contentString cancelTitle:NSLocalizedString(@"Cancel", nil)  otherTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
        if(!cancelled)
        {
            [self sendMarkAsSoldRequest];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"markassold_no" label:nil value:nil];
        }
    }];
    [view useDefaultIOS7Style];
    [view setLayLineColor];
}
-(void)sendMarkAsSoldRequest
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"markassold_yes" label:nil value:nil];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"mark_sold/" parameters:@{@"item_id": self.item.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"mark_sold ok");
        [self getDetailInformation];
        
        
        NSDictionary*dict = @{@"item_id":self.item.uid,@"new_item_status":[[NSNumber alloc] initWithInt: ITEM_STATE_SOLD]};
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_ITEMS_STATUS_CHANGE object:dict];
        if([HGUserData sharedInstance].bShowRatingAppRemind_MarkAsSold)
        {
            [HGUserData sharedInstance].bShowRatingAppRemind_MarkAsSold = NO;
            [[HGUtils sharedInstance] startRatingAppRemind:self];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Mark item as sold fail.%@", error);
        
    }];
    
}
-(void)startUnlistAlert
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"unlist" label:nil value:nil];
    
    int nOfferCount = (int)self.offerLines.count;
    NSString* contentString = @"";
    if (nOfferCount == 1) {
        contentString = NSLocalizedString(@"1 person has messaged you about this listing. Continue?",nil);
    }
    else if(nOfferCount > 1){
        contentString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d  people have messaged you about this item. If your item was sold, please mark it Sold. Continue?",nil),nOfferCount];
    }else{
        contentString = [@"" stringByAppendingString:NSLocalizedString(@"Are you sure?",nil)];
    }
    
    PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Unlist listing?",nil) message:contentString cancelTitle:NSLocalizedString(@"Cancel", nil)  otherTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
        
        if(!cancelled)
        {
            [self sendUnlistRequest];
            
        }else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"unlist_no" label:nil value:nil];
        }
    }];
    
    [view useDefaultIOS7Style];
}
-(void)sendUnlistRequest
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"unlist_yes" label:nil value:nil];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unlist_item/" parameters:@{@"item_id": self.item.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"unlist_item success.");
        
        NSDictionary*dict = @{@"item_id":self.item.uid,@"new_item_status":[[NSNumber alloc] initWithInt: ITEM_STATE_UNLISTED]};
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_ITEMS_STATUS_CHANGE object:dict];
        
        [self getDetailInformation];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"unlist_item  as sold fail.%@", error);
        
    }];
}
-(void)startRelistAction:(void(^)())completion
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"relist" label:nil value:nil];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"relist_item/" parameters:@{@"item_id": self.item.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"relist_item success.");
        NSDictionary*dict = @{@"item_id":self.item.uid,@"new_item_status":[[NSNumber alloc] initWithInt: ITEM_STATE_LISTING]};
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_ITEMS_STATUS_CHANGE object:dict];
        
        [self getDetailInformation];
        self.headcell.blackview.hidden = YES;
        if (completion!= nil) {
            completion();
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"relist_item  as sold fail.%@", error);
        
    }];
}

-(void)startSystemShare
{
    if(self.itemDetail.uid == nil)
    {
        return;
    }
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"share_product" label:nil value:nil];
    NSString *template = @"";
    NSString *itemWebsiteLink = [NSString stringWithFormat:@"https://5milesapp.com/item/%@", self.itemDetail.uid];
    
    UIImageView *imageView = [self.headcell.imageViewer currentImageView];
    UIImage *shareImage = imageView.image;
    NSString *shareContent = @"";
    
    if ([self.item.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"branchview" action:@"branchmyitem" label:nil value:nil];
        template = NSLocalizedString(@"Are you in %1$@? Check out my mobile marketplace on the #5milesapp - I'm selling a %2$@ for %3$@. More details here: ", nil);
        
        NSString* addressStrig = [self.item formatAddress];
        if(addressStrig.length ==0)
        {
            addressStrig = NSLocalizedString(@"my city",nil);
        }
        shareContent = [NSString stringWithFormat:template,addressStrig, self.itemDetail.title, [self.itemDetail priceForDisplay]];
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"branchview" action:@"branchotheritem" label:nil value:nil];
        template = NSLocalizedString(@"Check this out on the #5milesapp! %1$@ for %2$@.",nil);
        shareContent = [NSString stringWithFormat:template, self.itemDetail.title, [self.itemDetail priceForDisplay]];
    }
    
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:itemWebsiteLink forKey:@"$desktop_url"];
    [params setObject:itemWebsiteLink forKey:@"$og_url"];
    NSString* featureString = [@"" stringByAppendingFormat:@"%@item%@", [HGUserData sharedInstance].fmUserID, self.itemDetail.uid];
    NSString* md5string = [[HGUtils sharedInstance] md5HexDigest:featureString];
    md5string = [md5string substringToIndex:6];
    [params setObject:self.itemDetail.uid forKey:@"event_id"];
    [params setObject:@"item" forKey:@"share_type"];
    [params setObject:md5string forKey:@"md5string"];
    
    NSString* branchUrl = [[HGUtils sharedInstance] getBranchUrl:params];
    
    @try {
        UIActivityViewController * vcActivity = [[UIActivityViewController alloc] initWithActivityItems:@[shareContent,shareImage,branchUrl] applicationActivities:nil];
        
        if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
            vcActivity.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
                if (completed && !activityError) {
                    if ([HGUserData sharedInstance].bHomeShowFBLike)
                    {
                        [self addFBlikerViewWithTitle:NSLocalizedString(@"Share listing successfully", nil)];
                    }
                }
            };
        } else {
            vcActivity.completionHandler = ^(NSString *activityType, BOOL completed) {
                if (completed) {
                    if ([HGUserData sharedInstance].bHomeShowFBLike)
                    {
                        [self addFBlikerViewWithTitle:NSLocalizedString(@"Share listing successfully", nil)];
                    }
                }
            };
        }
        
        
        
        [self presentViewController:vcActivity animated:YES completion:nil];
        
    }
    @catch (NSException *exception) {
        [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
        
    }
}

-(void)addFBlikerViewWithTitle:(NSString *)title
{

    FBlikeReminderViewController *vc = [[FBlikeReminderViewController alloc] initWithNibName:@"FBlikeReminderViewController" bundle:nil];
    vc.delegate = self;
    vc.alertTitle = title;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        //ios 8
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    else{
        //
        HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
        
        application.tabController .modalPresentationStyle = UIModalPresentationCurrentContext;
        self.modalPresentationStyle = UIModalPresentationFormSheet;
        
    }
    
    HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
    [application.tabController presentViewController:vc animated:NO completion:nil];
}

- (void)touchedCloseButtonWithFblikeReminder:(FBlikeReminderViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchedLikeButtonWithFblikeReminder:(FBlikeReminderViewController *)viewController
{
    [HGUserData sharedInstance].bHomeShowFBLike = NO;
}


#pragma mark - HGChatViewControllerDelegate

- (void)didFinishChatWithNewMessage
{
    NSLog(@"back from offertalk");
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"item_offerlines/" parameters:@{@"item_id": self.item.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSArray * objects = [responseObject objectForKey:@"objects"];
        
        [self.offerLines removeAllObjects];
        for (NSDictionary * obj in objects) {
            NSError * err;
            HGOfferLine * offerLine = [[HGOfferLine alloc] initWithDictionary:obj error:&err];
            if (nil == err) {
                [self.offerLines addObject:offerLine];
            } else {
                NSLog(@"offerline creation failed:%@", err.userInfo);
            }
        }
        
        [self.collectionView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"refresh offline failed:%@", error);
    }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ShowItemLocation"]) {
        HGItemMapController * vcDest = segue.destinationViewController;
        vcDest.itemDetail = self.itemDetail;
    } else if ([segue.identifier isEqualToString:@"ShowUserDetail"]) {
        HGUserViewController * vcDest = segue.destinationViewController;
        vcDest.user = sender;
    } else if ([segue.identifier isEqualToString:@"ItemDetailToOffer"]){
        
        [FBSDKAppEvents logEvent:@"make_offer"];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"make_offer" withValue:@""];
        
        HGOfferViewController *vcDest = (HGOfferViewController *)segue.destinationViewController;
        HGOfferLine * offerline = (HGOfferLine *)sender;
        
        vcDest.offerLineID = offerline.uid;
        vcDest.item = self.itemDetail;
        vcDest.rf_tag = self.trackstring;
        
    }else if ([segue.identifier isEqualToString:@"showItemDetailLikers"]) {
        ItemLikersDetailViewController *viewcontroller = segue.destinationViewController;
        viewcontroller.itemIdString = self.itemDetail.uid;
    }
}

#pragma mark - Helpers


- (void)navigateToChatView:(HGOfferLine *) offerline{
    
    HGChatViewController *vcChat = [[UIStoryboard messageStoryboard]instantiateViewControllerWithIdentifier:@"VCChat"];
    vcChat.delegate = self;
    vcChat.bFromItemDetailPage = YES;
    vcChat.offerLineID = offerline ? offerline.uid : nil;
    vcChat.item = self.itemDetail;
    vcChat.toUser = self.itemDetail.seller;
    vcChat.bBuyer = YES;
    vcChat.bBlockedUser = self.bBlockedOwner;
    
    [vcChat setRf_tag: self.trackstring];
    
    [self.navigationController pushViewController:vcChat animated:YES];
}

- (void)_delistItem
{
    
    
    if (self.itemDetail.isMine) {
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"delete_item/" parameters:@{@"item_id": self.item.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
            
            [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Item has been deleted.",nil) onView:self.view];
            
            NSDictionary*dict = @{@"item_id":self.item.uid};
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_ITEMS_DELETE object:dict];
            
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"delete item failed: %@", error.userInfo);
        }];
        
    }
}
-(void)startSoldItemEditActionSheet
{
    self.editActionSheet = [[IBActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                         destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Relist",nil),NSLocalizedString(@"Delete",nil), nil];
    self.editActionSheet.tag = SoldItemEditActionTag;
    
    
    [self.editActionSheet setButtonTextColor:SYSTEM_DEFAULT_FONT_COLOR_1 forButtonAtIndex:0];
    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:0];
    [self.editActionSheet setFont:[UIFont systemFontOfSize:21] forButtonAtIndex:0];
    
    
    [self.editActionSheet setButtonTextColor:FANCY_COLOR(@"f84633") forButtonAtIndex:1];
    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:1];
    [self.editActionSheet setFont:[UIFont systemFontOfSize:21] forButtonAtIndex:1];
    
    
    [self.editActionSheet setCancelButtonFont:[UIFont systemFontOfSize: 21]];
    [self.editActionSheet showInView:self.view.window];
}

-(void)startPendingItemEditActionSheet
{
    self.editActionSheet = [[IBActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                         destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Verify phone number to list",nil), NSLocalizedString(@"Edit Details",nil), NSLocalizedString(@"Delete",nil), nil];
    self.editActionSheet.tag = PendingItemEditActionTag;
    
    
//    [self.editActionSheet setButtonTextColor:SYSTEM_DEFAULT_FONT_COLOR_1 forButtonAtIndex:0];
//    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:0];
//    [self.editActionSheet setFont:[UIFont systemFontOfSize:21] forButtonAtIndex:0];
    
    
    [self.editActionSheet setButtonTextColor:FANCY_COLOR(@"f84633") forButtonAtIndex:2];
    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:2];
    [self.editActionSheet setFont:[UIFont systemFontOfSize:21] forButtonAtIndex:2];
    
    
    [self.editActionSheet setCancelButtonFont:[UIFont systemFontOfSize: 21]];
    [self.editActionSheet showInView:self.view.window];
}

static int EditModeHasDeleteButton = 1;
-(void)startUnapprovedEditActionSheet
{
    self.editActionSheet = [[IBActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Delete",nil), nil];
    self.editActionSheet.tag = UnapprovedEditActionTag;
    
    
    [self.editActionSheet setButtonTextColor:FANCY_COLOR(@"f84633") forButtonAtIndex:0];
    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:0];
    [self.editActionSheet setFont:[UIFont systemFontOfSize:21] forButtonAtIndex:0];
    
    
    [self.editActionSheet setCancelButtonFont:[UIFont systemFontOfSize: 21]];
    [self.editActionSheet showInView:self.view.window];
}
-(void)clickOnEdit
{
    if (self.itemDetail.state == ITEM_STATE_UNAPPROVED) {
        [self startUnapprovedEditActionSheet];
        return;
    }
    if (self.itemDetail.state == ITEM_STATE_SOLD) {
        [self startSoldItemEditActionSheet];
        return;
    }
    
    if (self.itemDetail.state == ITEM_STATE_PENDING) {
        [self startPendingItemEditActionSheet];
        return;
    }
    
    ///////normal item edit action sheet.
    
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"edit" label:nil value:nil];
    NSString* listingString = @"";
    if (self.itemDetail.state != ITEM_STATE_UNLISTED) {
        listingString = NSLocalizedString(@"Unlist",nil);
    }
    else{
        listingString = NSLocalizedString(@"Relist",nil);
    }
    
    
    if(self.itemDetail.deletable == 0)
    {
        ///if item has offer, can not be deleted.
        self.editActionSheet = [[IBActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Edit Details",nil), NSLocalizedString(@"Mark as Sold",nil), listingString, nil];
        EditModeHasDeleteButton = 0;
    }
    else{
        self.editActionSheet = [[IBActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Edit Details",nil), NSLocalizedString(@"Mark as Sold",nil), listingString,NSLocalizedString(@"Delete",nil), nil];
        EditModeHasDeleteButton = 1;
    }
    
    
    self.editActionSheet.tag = EditButtonActionTag;
    
    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor]];
    [self.editActionSheet setButtonTextColor:[UIColor blueColor]];
    [self.editActionSheet setTitleBackgroundColor:[UIColor orangeColor]];
    [self.editActionSheet setTitleTextColor:[UIColor blackColor]];
    [self.editActionSheet setTitleFont:[UIFont systemFontOfSize:10]];
    
    [self.editActionSheet setButtonTextColor:SYSTEM_DEFAULT_FONT_COLOR_1 forButtonAtIndex:0];
    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:0];
    [self.editActionSheet setFont:[UIFont systemFontOfSize:21] forButtonAtIndex:0];
    
    [self.editActionSheet setButtonTextColor:SYSTEM_DEFAULT_FONT_COLOR_1 forButtonAtIndex:1];
    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:1];
    [self.editActionSheet setFont:[UIFont systemFontOfSize: 21] forButtonAtIndex:1];
    
    [self.editActionSheet setButtonTextColor:SYSTEM_DEFAULT_FONT_COLOR_1 forButtonAtIndex:2];
    [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:2];
    [self.editActionSheet setFont:[UIFont systemFontOfSize:21] forButtonAtIndex:2];
    
    if (EditModeHasDeleteButton == 1) {
        [self.editActionSheet setButtonTextColor:FANCY_COLOR(@"f84633") forButtonAtIndex:3];
        [self.editActionSheet setButtonBackgroundColor:[UIColor whiteColor] forButtonAtIndex:3];
        [self.editActionSheet setFont:[UIFont systemFontOfSize:21] forButtonAtIndex:3];
    }
    
    
    [self.editActionSheet setCancelButtonFont:[UIFont systemFontOfSize: 21]];
    
    [self.editActionSheet showInView:self.view.window];
    
    
}

-(void)startEditView
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"edit_detail" label:nil value:nil];
    HGAppDelegate * appDelegate = (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UINavigationController *navUpload = (UINavigationController *)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"NavUpload"];
    
    [HGAppData sharedInstance].bHasItemValue = 1;
    [HGAppData sharedInstance].editItem = self.itemDetail;
    
    [appDelegate.tabController presentViewController:navUpload animated:YES completion:nil];
}








-(void)handleMyItemEdit:(NSNotification *)notification
{
    NSString * itemID = self.item ? self.item.uid : @"O6zdMEg5VJ42BwXG";
    if (self.item) {
        self.itemDetail = [[HGItemDetail alloc] init];
        self.itemDetail.title = self.item.title;
        self.itemDetail.images = self.item.images;
        //[self.collectionView reloadData];
    }
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"item_detail/" parameters:@{@"item_id": itemID} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError * error;
        self.itemDetail = [[HGItemDetail alloc] initWithDictionary:responseObject error:&error];
        
        [self configFlyingActinoView];
        
        [self.offerLines removeAllObjects];
        [self.offerLines addObjectsFromArray:self.itemDetail.offerLines];
        
        [self.collectionView reloadData];
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        
        if (![self.itemDetail.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
            [self _loadRecommendations];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get item detail failed: %@", error);
    }];
    
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    @try {
        
        float headcell_userActionView_yoffset= CGRectGetMinY(self.headcell.userActionView.frame);
        float headcell_canvas_yoffset=CGRectGetMinY(self.headcell.canvasView.frame);
        
        float detectYposition =  headcell_userActionView_yoffset+headcell_canvas_yoffset -4 ;
        if(scrollView.contentOffset.y > detectYposition-flyingUserActinoView_offst)
        {
            if (self.itemDetail.seller.uid == nil || self.itemDetail.seller.uid.length ==0) {
                return;
            }
            if (![self.itemDetail.seller.uid isEqualToString:[HGUserData sharedInstance].fmUserID]){
                //it is not current user 's item
                self.flyingUserActionView.hidden = NO;
                self.SeperatorView.hidden = NO;
                
                self.headcell.userActionView.hidden = YES;
            }
            
        }
        else{
            self.flyingUserActionView.hidden = YES;
            self.SeperatorView.hidden = YES;
            self.headcell.userActionView.hidden = NO;
        }
        
    }
    @catch (NSException *exception) {
        [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
        
        NSLog(@"item detail scroll view did scroll fail");
    }
    
}

-(void)onTouchAvatarForUserView:(NSObject*)sender
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"like_avatar" label:nil value:nil];
    [[HGUtils sharedInstance] gaTrackViewName:@"wholikethisitem_view"];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"wholikethisitem_view" action:@"peoplelikesitem" label:nil value:nil];
    
    UITapGestureRecognizer *tap = (UITapGestureRecognizer*)sender;
    
    UIView *views = (UIView*) tap.view;
    int likerIndex = (int)views.tag;
    if (likerIndex <= self.likersArray.count) {
        ItemLikers* liker = [self.likersArray objectAtIndex:likerIndex];
        HGUser* user = [[HGUser alloc] init];
        user.uid = liker.uid;
        user.displayName = @"";
        user.portraitLink = @"";
        [self performSegueWithIdentifier:@"ShowUserDetail" sender:user];
        
    }
}
-(void)onTouchLikerMore:(NSObject *)sender
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"likes_more" label:nil value:nil];
    [self performSegueWithIdentifier:@"showItemDetailLikers" sender:self];
}
-(void)onTouchEditButton
{
    if([[HGUserData sharedInstance] isLoggedIn]){
        [self clickOnEdit];
    }else{
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(clickOnEdit)];
    }
}
-(void)onTouchRenewButton
{
    if([[HGUserData sharedInstance] isLoggedIn]){
        [self startRenew];
    }else{
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startRenew)];
    }
}

- (void)startRenew{
    if (self.updateButtonTimer != nil) {
        [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Please wait a while to renew again",nil) onView:self.view];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"Renew_countdown" label:nil value:nil];
    }
    else{
        
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"renew_item/" parameters:@{@"item_id": self.item.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
            [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Your listing has been renewed.",nil) onView:self.view];
            NSLog(@"renew_item ok");
            NSNumber* timeTTL= [responseObject objectForKey:@"renew_ttl"];
            
            self.itemDetail.renew_ttl = timeTTL.intValue;
            NSNumber* updateTime = [responseObject objectForKey:@"updated_at"];
            self.headcell.createTimeContent.text = [[HGUtils sharedInstance] prettyDate:updateTime.longValue];
            [self initUpdateButtonTimer];
            
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"Renew" label:nil value:nil];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"renew_item  failed: %@", error);
        }];
        
    }
    
}

- (void)onTouchReportButton{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"reportitem" label:nil value:nil];
    if([[HGUserData sharedInstance] isLoggedIn]){
        
        [self startReportChoice];
    }else{
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"report_login" label:nil value:nil];
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startReportChoice)];
    }
    
}

-(void)startReportChoice{
    UserReportTableViewController *viewcontroller = [[UserReportTableViewController alloc] initWithNibName:@"UserReportTableViewController" bundle:nil];
    viewcontroller.reportType = ReportReasonTypeItem;
    viewcontroller.itemID = self.itemDetail.uid;
    [self.navigationController pushViewController:viewcontroller animated:YES];
}

-(void)startDeleteAlert
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"delete_product" label:nil value:nil];
    
    ///////
    int nOfferCount = (int)self.offerLines.count;
    NSString* contentString = @"";
    if (nOfferCount == 1) {
        contentString = NSLocalizedString(@"1 person has messaged you about this listing. Continue?",nil);
    }
    else if(nOfferCount > 1){
        contentString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d people have messaged you about this listing. Continue?",nil),nOfferCount];
    }else{
        contentString = [@"" stringByAppendingString:NSLocalizedString(@"Are you sure?",nil)];
    }
    
    PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Delete listing?",nil) message:contentString cancelTitle:NSLocalizedString(@"Cancel", nil)  otherTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
        
        if(!cancelled)
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"delete_productyes" label:nil value:nil];
            [self _delistItem];
            
        }else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"delete_productno" label:nil value:nil];
        }
    }];
    
    [view useDefaultIOS7Style];
    
    
}

#pragma mark - X4ImageViewerDelegate

- (void)imageViewer:(X4ImageViewer *)imageViewer didSingleTap:(UIImageView *)imageView atIndex:(NSInteger)index inScrollView:(UIScrollView *)scrollView{
    
    
    HGItemImageViewerViewController *vcItemImageViewer = (HGItemImageViewerViewController *)[[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:@"VCItemImageViewer"];
    vcItemImageViewer.imagesArray = [imageViewer currentLoadedImages];
    vcItemImageViewer.currentPageIndex = imageViewer.currentPageIndex;
    
    [self.navigationController pushViewController:vcItemImageViewer animated:YES];
    
}



- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning  in ItemDetail view Controller");
    [super didReceiveMemoryWarning];
    [[SDImageCache sharedImageCache] clearMemory];
}

@end

//
//  HGUserViewController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-2.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGUserVerifyHintView.h"
#import "HGUserSummaryView.h"
@class HGUser;

@interface HGUserViewController : UICollectionViewController<HGUserSummaryDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) HGUser * user;

@end

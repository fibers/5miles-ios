//
//  HGItemDetail.h
//  Pinnacle
//
//  Created by Alex on 15-2-28.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HGShopItem.h"
#import "HGAppData.h"
#import "HGUtils.h"




@interface HGItemDetail : HGShopItem

//@property (nonatomic, assign) int commentCount;
//@property (nonatomic, assign) BOOL hasMoreComments;
@property (nonatomic, assign) BOOL isMine;
@property (nonatomic, assign) BOOL liked;
@property (nonatomic, assign) NSInteger brand_id;
@property (nonatomic, strong) NSArray<HGOfferLine> * offerLines;

@property (nonatomic, strong)NSNumber* review_num;
@property (nonatomic, strong)NSNumber* review_score;
@end

//
//  HGSysMessageCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-28.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HGSysMessage;
@class HGSysMessageDrawingView;

@interface HGSysMessageCell : UITableViewCell

@property (nonatomic, strong) UIImageView * avatarView;
@property (nonatomic, strong) HGSysMessageDrawingView * canvasView;
@property (nonatomic, strong) UIImageView* itemImageView;
@property (nonatomic, strong) HGSysMessage * message;
@property (nonatomic, strong) UIView * SeperatorView;

@property (nonatomic, strong) UIImageView* urlAcionIcon;
@property (nonatomic, strong) UIImageView* systemMessageImage;





- (void)configWithSysMessage:(HGSysMessage *)message;

@end






@protocol HGSysMessageDrawingViewDelegate <NSObject>

-(void)clickUrlAction:(NSURL*)url;

@end
@interface HGSysMessageDrawingView : UIView

@property (nonatomic, weak) HGSysMessageCell * ownerCell;
@property (nonatomic, strong)NSTextCheckingResult *activeLink;
@property (nonatomic, assign) id<HGSysMessageDrawingViewDelegate> delegate;
////////UILABEL //////////////////
@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UILabel* descLabel;
@property (nonatomic, strong) UILabel* dateLabel;

///////END OF UILABEL/////////////


-(void)configuration;
@end

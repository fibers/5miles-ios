//
//  HGCategoryPickController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGCategoryPickController.h"
#import "HGAppData.h"
#import "HGUtils.h"
#import "HGCategory.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+pureColorImage.h"
#import "HGCategoryTableCell.h"



@interface HGCategoryPickController ()
@property (nonatomic, strong) NSMutableDictionary* level2CategoryDict;
@property (nonatomic, strong) NSMutableArray* level1CategoryArray;
@property (nonatomic, strong) NSString* currentSelectedCategoryLevel1Title;

@property (nonatomic, strong) NSMutableArray* leftCategoryButtons;
@property (nonatomic, assign) int currentSelectCateogrylevel1Index;
@property (nonatomic, strong) NSMutableArray* currentSelectedCategories;
@end

@implementation HGCategoryPickController
-(void)initCategoryfromLocal
{
    if ([HGAppInitData sharedInstance].categoryArray_c1) {
        [self.level1CategoryArray removeAllObjects];
        [self.level1CategoryArray addObjectsFromArray:[HGAppInitData sharedInstance].categoryArray_c1];
        for (int i= 0; i<self.level1CategoryArray.count; i++) {
            HGCategory* c = [self.level1CategoryArray objectAtIndex:i];
            
            if (i==0) {
                self.currentSelectedCategoryLevel1Title = c.title;
            }
            [self.level2CategoryDict setObject:c.subCategories forKey:c.title];
        }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.level2CategoryDict = [[NSMutableDictionary alloc] init];
    self.level1CategoryArray = [[NSMutableArray alloc] init];
    self.leftCategoryButtons = [[NSMutableArray alloc] init];
    self.currentSelectedCategories = [[NSMutableArray alloc] init];
    
    [self initCategoryfromLocal];
    [self addLeftSearchTypeButton];

    self.navigationItem.title = NSLocalizedString(@"Categories", nil);
    
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"PickCategoryWhenSell"];
}


static int leftSearchTypeButtonWidth = 80;
static int leftSearchTypeButtonHeight = 89;

static int btn_right_triangle = 9997;
static int btn_icon_tag = 9998;
static int btn_label_tag = 9999;



-(void)addLeftSearchTypeButton
{
    UIScrollView* leftTypeBoard = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, leftSearchTypeButtonWidth, self.view.frame.size.height)];
    
    UIView* rightLine = [[UIView alloc] initWithFrame:CGRectMake(leftTypeBoard.frame.size.width -1, 0, 1, leftTypeBoard.frame.size.height)];
    rightLine.backgroundColor = FANCY_COLOR(@"c8c8c8");
    [leftTypeBoard addSubview:rightLine];
    
    [self.view addSubview:leftTypeBoard];
    leftTypeBoard.backgroundColor = FANCY_COLOR(@"f4f4f4");
    
    leftTypeBoard.contentSize = CGSizeMake(leftSearchTypeButtonWidth, leftSearchTypeButtonHeight* [HGAppInitData sharedInstance].categoryArray_c1.count + 100);
    for (int i = 0; i<[HGAppInitData sharedInstance].categoryArray_c1.count ; i++) {
        HGCategory* category = [[HGAppInitData sharedInstance].categoryArray_c1 objectAtIndex:i];
        
        UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0, leftSearchTypeButtonHeight*i, leftSearchTypeButtonWidth, leftSearchTypeButtonHeight)];
        btn.tag = i;
        [leftTypeBoard addSubview:btn];
        UIImageView* iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        iconView.center = CGPointMake(leftSearchTypeButtonWidth/2, leftSearchTypeButtonHeight/2-5);
        iconView.tag = btn_icon_tag;
        [btn addSubview:iconView];
        
        
        NSString* localIconName = [[HGAppInitData sharedInstance].category_level1_localIcon objectAtIndex:i%[HGAppInitData sharedInstance].category_level1_localIcon.count];
        
        [iconView sd_setImageWithURL:[NSURL URLWithString:category.icon] placeholderImage:[UIImage imageNamed:localIconName]];
        
        
        
        UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
        title.numberOfLines = 1;
        title.font = [UIFont fontWithName:@"Museo-300" size:11.0f];
        title.textColor  = FANCY_COLOR(@"656565");
        title.text = [category.title uppercaseString];
        title.textAlignment = NSTextAlignmentCenter;
        CGPoint center = iconView.center;
        center.y = center.y + 25;
        title.center = center;
        title.tag = btn_label_tag;
        [btn addSubview:title];
        
        btn.backgroundColor = FANCY_COLOR(@"f4f4f4");
        UIView* leftColor = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2, btn.frame.size.height)];
        
        leftColor.backgroundColor = FANCY_COLOR(@"ff8830");
        
        UIView* bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, btn.frame.size.height-1, btn.frame.size.width, 1)];
        bottomLine.backgroundColor = FANCY_COLOR(@"c8c8c8");
        [btn addSubview:bottomLine];
        [btn addSubview:leftColor];
        
        UIView* rightLine = [[UIView alloc] initWithFrame:CGRectMake(btn.frame.size.width -1, 0, 1, btn.frame.size.height)];
        rightLine.backgroundColor = FANCY_COLOR(@"c8c8c8");
        [btn addSubview:rightLine];
        
        
        UIImageView* rightTriangle = [[UIImageView alloc] initWithFrame:CGRectMake(leftSearchTypeButtonWidth-16/2, leftSearchTypeButtonHeight/2-19/4, 16/2, 19/2)];
        rightTriangle.image = [UIImage imageNamed:@"searchRightCategoryTriangle"];
        [btn addSubview:rightTriangle];
        rightTriangle.tag = btn_right_triangle;
        rightTriangle.hidden = YES;
        
        [btn addTarget:self action:@selector(onTouchSearhMenuChange:) forControlEvents:UIControlEventTouchUpInside];
        [self.leftCategoryButtons addObject:btn];
        
        [self.currentSelectedCategories addObject:[[NSNumber alloc] initWithInt:-1]];
    }
    
    if(self.selectedC1_ID == 0)
    {
        [self onTouchSearhMenuChange:[self.leftCategoryButtons objectAtIndex:0]];
    }else{
        //1000
        int lastIndexFromSellView = self.selectedC1_ID - 1000;
        if (lastIndexFromSellView < self.leftCategoryButtons.count) {
            [self onTouchSearhMenuChange:[self.leftCategoryButtons objectAtIndex:lastIndexFromSellView]];
        }
    }
}

-(void)resetLeftCateogryButton
{
    for (int i =0; i<self.leftCategoryButtons.count; i++) {
        UIButton* btn = [self.leftCategoryButtons objectAtIndex:i];
        btn.backgroundColor = FANCY_COLOR(@"f4f4f4");
        
        if (self.level1CategoryArray && i < self.level1CategoryArray.count) {
            HGCategory* c =[self.level1CategoryArray objectAtIndex:i];
            
            UIImageView* iconView = (UIImageView*)[btn viewWithTag:btn_icon_tag];
            if ([iconView isKindOfClass:[UIImageView class]]) {
                NSString* localIconName = [[HGAppInitData sharedInstance].category_level1_localIcon objectAtIndex:i%[HGAppInitData sharedInstance].category_level1_localIcon.count];
                
                [iconView sd_setImageWithURL:[NSURL URLWithString:c.icon] placeholderImage:[UIImage imageNamed:localIconName]];
                
            }
            
        }
        
        UILabel* title = (UILabel*)[btn viewWithTag:btn_label_tag];
        if ([title isKindOfClass:[UILabel class]]) {
            title.textColor = FANCY_COLOR(@"656565");
        }
        UIImageView* trangle = (UIImageView*)[btn viewWithTag:btn_right_triangle];
        if ([trangle isKindOfClass:[UIImageView class]]) {
            trangle.hidden = YES;
        }
    }
}
-(void)onTouchSearhMenuChange:(id)sender
{
    [self resetLeftCateogryButton];
    UIButton * btn = (UIButton*)sender;
    int index = (int)btn.tag;
    self.currentSelectCateogrylevel1Index = index;
    HGCategory* c =[self.level1CategoryArray objectAtIndex:index];
    if (index >= self.level1CategoryArray.count) {
        return;
    }
    
    btn.backgroundColor = FANCY_COLOR(@"ff8830");
    
    UIImageView* iconView = (UIImageView*)[btn viewWithTag:btn_icon_tag];
    if ([iconView isKindOfClass:[UIImageView class]]) {
        NSString* localIconName = [[HGAppInitData sharedInstance].category_level1_localIcon objectAtIndex:index%[HGAppInitData sharedInstance].category_level1_localIcon.count];
        localIconName = [localIconName stringByAppendingString:@"_psd"];
        
        [iconView sd_setImageWithURL:[NSURL URLWithString:c.hover_icon] placeholderImage:[UIImage imageNamed:localIconName]];
        
    }
    
    
    UILabel* title = (UILabel*)[btn viewWithTag:btn_label_tag];
    if ([title isKindOfClass:[UILabel class]]) {
        title.textColor = FANCY_COLOR(@"ffffff");
    }
    
    UIImageView* trangle = (UIImageView*)[btn viewWithTag:btn_right_triangle];
    if ([trangle isKindOfClass:[UIImageView class]]) {
        trangle.hidden = NO;
    }
  
    self.currentSelectedCategoryLevel1Title = c.title;
    self.tableview.dataSource =self;
    self.tableview.delegate = self;
    [self.tableview reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* categories = [self.level2CategoryDict objectForKey:self.currentSelectedCategoryLevel1Title];
    return categories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGCategoryTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryTableCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSArray* categories = [self.level2CategoryDict objectForKey:self.currentSelectedCategoryLevel1Title];
    HGCategory* category = [categories objectAtIndex:indexPath.row];
    
    
    NSNumber* currentSelectedIndex = [self.currentSelectedCategories objectAtIndex:self.currentSelectCateogrylevel1Index];
    if (currentSelectedIndex.integerValue == indexPath.row) {
        cell.lbTitle.textColor = FANCY_COLOR(@"ff8830");
    }
    else{
        cell.lbTitle.textColor = FANCY_COLOR(@"242424");
    }
    
    cell.lbTitle.text = category.title;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray* categories = [self.level2CategoryDict objectForKey:self.currentSelectedCategoryLevel1Title];
    HGCategory* category = [categories objectAtIndex:indexPath.row];
    self.selectedCatID = category.index;
    
    
    HGCategory* categoryLevel1 = [[HGAppInitData sharedInstance].categoryArray_c1 objectAtIndex:self.currentSelectCateogrylevel1Index];
    
    self.selectedC1_ID = categoryLevel1.index;
    
    [self.currentSelectedCategories replaceObjectAtIndex:self.currentSelectCateogrylevel1Index withObject:[[NSNumber alloc] initWithInt:(int)indexPath.row]];
    self.currentSelectedC2TitleString  = category.title;
    
    [self.tableview reloadData];
    
    if (categoryLevel1.index == 1000) {
        //for item sales;
        self.bIsItemSell = YES;
    }
    [self performSegueWithIdentifier:@"DonePickCategory" sender:self];
}



@end

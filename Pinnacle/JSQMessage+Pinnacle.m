//
//  JSQMessage+Pinnacle.m
//  Pinnacle
//
//  Created by shengyuhong on 15/5/20.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "JSQMessage+Pinnacle.h"
#import "HGUserData.h"
#import "objc/runtime.h"
#import "HGOffer.h"
#import "HGUser.h"
#import "HGChatPhotoMediaItem.h"
#import "HGChatLocationMediaItem.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGConstant.h"
#import "UIImage+pureColorImage.h"

static const void *keyBSendFailed = (void *)@"keyBSendFailed";
static const void *keyBSendInProcess = (void *)@"keyBSendInProcess";
static const void *keyBReceiveFailed = (void *)@"keyBReceiveFailed";
static const void *keyBReceiveInProcess = (void *)@"keyBReceiveInProcess";
static const void *keyBShowTimestamp = (void *)@"keyBShowTimestamp";
static const void *keyUid = (void *)@"keyUid";

@implementation JSQMessage (Pinnacle)

@dynamic bSendFailed;
@dynamic bSendInProcess;
@dynamic bReceiveFailed;
@dynamic bReceiveInProcess;
@dynamic bShowTimestamp;
@dynamic uid;


+ (instancetype)messageWithOffer:(HGOffer *)offer receiver:(HGUser *)receiver showTimestamp:(BOOL)showTimestamp{
    
    if(!offer){
        return [JSQMessage messageWithSenderId:@"" displayName:@"" text:@""];
    }
    
    NSString *senderID;
    NSString *senderDisplayName;
    
    if(offer.fromMe){
        senderID = [HGUserData sharedInstance].fmUserID;
        senderDisplayName = [HGUserData sharedInstance].userDisplayName;
    }else{
        senderID = receiver.uid;
        senderDisplayName = receiver.displayName;
    }
    
    JSQMessage *message = nil;
    
    if(offer.messageType == HGOfferMessageTypeText){
        NSString *text = [offer.payload objectForKey:@"text"];
        message = [[JSQMessage alloc] initWithSenderId:senderID senderDisplayName:senderDisplayName date:offer.timestamp text:text];
        
    }else if(offer.messageType == HGOfferMessageTypePhoto){
        
        NSString *pictureURL = [offer.payload objectForKey:@"picture_url"];
        if([pictureURL length] == 0){
            return [JSQMessage messageWithSenderId:@"" displayName:@"" text:@""];
        }
        
        
        HGChatPhotoMediaItem *photoMediaItem = [[HGChatPhotoMediaItem alloc] init];
        photoMediaItem.imageURL = [NSURL URLWithString:pictureURL];
        photoMediaItem.appliesMediaViewMaskAsOutgoing = offer.fromMe;
        [photoMediaItem addLoadingMask];
        
        message = [[JSQMessage alloc] initWithSenderId:senderID senderDisplayName:senderDisplayName date:offer.timestamp media:photoMediaItem];
        message.bReceiveInProcess = YES;
        message.bReceiveFailed = NO;
        
        CGSize mediaViewDisplaySize = [photoMediaItem mediaViewDisplaySize];
        NSString *imageURL = [[HGUtils sharedInstance] cloudinaryLink:pictureURL width:mediaViewDisplaySize.width * 2 height:0];
        
        [photoMediaItem.imageView sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"item-new-placeholder"] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
            CGFloat progress = (CGFloat)receivedSize/expectedSize;
            if(progress > 0.99){
                progress = 0.99;
            }
            [photoMediaItem updateProgress:progress];
            
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            message.bReceiveInProcess = NO;
            [photoMediaItem removeLoadingMask];
            
            if(error){
                message.bReceiveFailed = YES;
                [photoMediaItem addFailedIcon];
            }
           
        }];
        
        
    }else if(offer.messageType == HGOfferMessageTypeLocation){
        NSString* map_thumb_url = [offer.payload objectForKey:@"address_map_thumb"];
        if([map_thumb_url length] == 0){
            return [JSQMessage messageWithSenderId:@"" displayName:@"" text:@""];
        }
        
        NSURL *imageURL = [NSURL URLWithString:map_thumb_url];
        
        HGChatLocationMediaItem *locationMediaItem = [[HGChatLocationMediaItem alloc] init];
        locationMediaItem.poiAddress = [offer.payload objectForKey:@"address_name"];
        locationMediaItem.poiTitle = [offer.payload objectForKey:@"place_name"];
        locationMediaItem.latString = [offer.payload objectForKey:@"lat"];
        locationMediaItem.lonString= [offer.payload objectForKey:@"lon"];
        locationMediaItem.appliesMediaViewMaskAsOutgoing = offer.fromMe;
        if (offer.fromMe) {
            [locationMediaItem setRightSpeakerInsets];
        }
        else
        {
            [locationMediaItem setLeftSpeakerInsets];
        }
        
        locationMediaItem.addressLabel.text =locationMediaItem.poiAddress;
        locationMediaItem.titleLabel.text = locationMediaItem.poiTitle;
        
        [locationMediaItem addLoadingMask];
        
         message = [[JSQMessage alloc] initWithSenderId:senderID senderDisplayName:senderDisplayName date:offer.timestamp media:locationMediaItem];
        message.bReceiveInProcess = YES;
        message.bReceiveFailed = NO;
        
        [locationMediaItem.imageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"item-new-placeholder"] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
            CGFloat progress = (CGFloat)receivedSize/expectedSize;
            if(progress > 0.99){
                progress = 0.99;
            }
            [locationMediaItem updateProgress:progress];
            
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            message.bReceiveInProcess = NO;
            [locationMediaItem removeLoadingMask];
            
            if(error){
                message.bReceiveFailed = YES;
                [locationMediaItem addFailedIcon];
            }
           
        }];
    }
    
    message.uid = offer.uid;
    message.bShowTimestamp = showTimestamp;
    message.bSendInProcess = NO;
    message.bSendFailed = NO;
    
    return message;
}


- (BOOL)fromMe{
    return [self.senderId isEqualToString:[HGUserData sharedInstance].fmUserID];
}

- (BOOL)bSendInProcess{
    NSNumber *number = objc_getAssociatedObject(self, keyBSendInProcess);
    return [number boolValue];
}

- (void)setBSendInProcess:(BOOL)bSendInProcess{
    objc_setAssociatedObject(self, keyBSendInProcess, @(bSendInProcess), OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)bSendFailed{
    NSNumber *number = objc_getAssociatedObject(self, keyBSendFailed);
    return [number boolValue];
}

- (void)setBSendFailed:(BOOL)bSendFailed{
    objc_setAssociatedObject(self, keyBSendFailed, @(bSendFailed), OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)bReceiveInProcess{
    NSNumber *number = objc_getAssociatedObject(self, keyBReceiveInProcess);
    return [number boolValue];
}

- (void)setBReceiveInProcess:(BOOL)bReceiveInProcess{
    objc_setAssociatedObject(self, keyBReceiveInProcess, @(bReceiveInProcess), OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)bReceiveFailed{
    NSNumber *number = objc_getAssociatedObject(self, keyBReceiveFailed);
    return [number boolValue];
}

- (void)setBReceiveFailed:(BOOL)bReceiveFailed{
    objc_setAssociatedObject(self, keyBReceiveFailed, @(bReceiveFailed), OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)bShowTimestamp{
    NSNumber *number = objc_getAssociatedObject(self, keyBShowTimestamp);
    return [number boolValue];
}

- (void)setBShowTimestamp:(BOOL)bShowTimestamp{
    objc_setAssociatedObject(self, keyBShowTimestamp, @(bShowTimestamp), OBJC_ASSOCIATION_ASSIGN);
}

- (NSString *)uid{
    return objc_getAssociatedObject(self, keyUid);
}

- (void)setUid:(NSString *)uid{
    objc_setAssociatedObject(self, keyUid, uid, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end

//
//  HGHomeFollowingViewController.h
//  Pinnacle
//
//  Created by mumuhou on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGNavigationBarScrollProtocol.h"

@interface HGHomeFollowingViewController : UIViewController

- (void)refreshContent;

- (void)refreshContentSilent;

@property (nonatomic, weak) id<HGNavigationBarScrollProtocol> delegate;

@end

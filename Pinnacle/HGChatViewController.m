//
//  HGChatViewController.m
//  Pinnacle
//
//  Created by shengyuhong on 15/1/22.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatViewController.h"
#import "HGUserViewController.h"
#import "HGItemDetailController.h"
#import "HGWebContentController.h"
#import "HGConstant.h"
#import "HGChatMessageCell.h"
#import "HGAppData.h"
#import "HGOffer.h"
#import "HGUser.h"
#import <SVProgressHUD.h>
#import <UIColor+FlatColors.h>
#import <UIImageView+WebCache.h>
#import <FXBlurView.h>
#import "UIStoryboard+Pinnacle.h"
#import "UserReportTableViewController.h"
#import "HGRateUserController.h"
#import "CMPopTipView.h"
#import "HGChatMapController.h"
#import "HGChatCollectionViewController.h"



#define TAG_ACTION_SHEET_REVIEW 1000
#define TAG_ACTION_SHEET_REVIEW_CLOSE 1001
#define TAG_ACTION_SHEET_CHAT_MORE 1002

@interface HGChatViewController () 


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *messagesView;

@property (nonatomic, strong) HGOfferLineMeta *offerLineMeta;

@property (nonatomic, strong) NSTimer *timerUpdateOffers;
@property (nonatomic, assign) NSTimeInterval latestTimestamp;

@property (nonatomic, strong) UITapGestureRecognizer *gestTapOutOfKeyboard;

@property (nonatomic, strong) CMPopTipView *popTipReview;

//FIXME: currency symbol temporary solution
@property (nonatomic, strong) NSString *currencyUnit;

@property(nonatomic, assign) int firstTimeToAppear;

@property (nonatomic, strong) HGChatCollectionViewController *vcChatCollection;


@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@end


@implementation HGChatViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.hidesBottomBarWhenPushed = YES;
        self.firstTimeToAppear = 1;
    }
    return self;
}

-(void)setRf_tag:(NSString *)rf_tag
{
    _rf_tag = rf_tag;
    if (self.vcChatCollection) {
        self.vcChatCollection.rf_tag = rf_tag;
    }
    
}
- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    
    if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
         [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];

    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
   
    self.leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:self.leftSwipeGestureRecognizer];
   
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] init];
    backBarButtonItem.title = @"";
    self.navigationItem.backBarButtonItem = backBarButtonItem;
    
    UIImageView *ivBack = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"naviBack_icon2"]];
    
    ivBack.userInteractionEnabled = YES;
     
    UITapGestureRecognizer *tapGestureNaviBack = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapNavigationBack)];
    [ivBack addGestureRecognizer:tapGestureNaviBack];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:ivBack];
    
    if([HGAppData sharedInstance].bOpenReviewFunctions)
    {
        UIView* rightButtonBackView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
        
        UIButton *btnReview = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 68, 30)];
        btnReview.clipsToBounds = YES;
        btnReview.layer.cornerRadius = 3;
        btnReview.layer.borderWidth = 1;
        btnReview.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
        btnReview.titleLabel.font = [UIFont systemFontOfSize:14];
        [btnReview setTitle: NSLocalizedString(@"Review",nil) forState:UIControlStateNormal];
        [btnReview setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
        [btnReview addTarget:self action:@selector(showReviewActionSheet) forControlEvents:UIControlEventTouchUpInside];
        [rightButtonBackView addSubview:btnReview];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButtonBackView];
    }
    
    self.headerView.backgroundColor = FANCY_COLOR(@"f7f7f7");
    UITapGestureRecognizer *tapGestureItemDetail = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapItemDetail)];
    [self.headerView addGestureRecognizer:tapGestureItemDetail];
    
    self.vcChatCollection = [[HGChatCollectionViewController alloc] initFromChatVC:self];
    self.vcChatCollection.view.frame = self.messagesView.frame;
    [self.view addSubview:self.vcChatCollection.view];
    [self addChildViewController:self.vcChatCollection];
    [self.vcChatCollection didMoveToParentViewController:self];
    self.vcChatCollection.rf_tag = self.rf_tag;
    
    
    [self updateUI];
    
    if(!self.offerLineID) {
        
        [SVProgressHUD dismiss];
        return;
    }
    else{
        [self getOffersAndOfferLineMeta];
    }
 
}


- (void)viewDidLayoutSubviews{
    self.vcChatCollection.view.frame = self.messagesView.frame;
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (self.offerLineMeta==nil && self.offerLineID !=nil) {
        //getOffersAndOfferLineMeta is not finished yet.
        [SVProgressHUD show];
    }
    
    [[HGUtils sharedInstance] gaTrackViewName:@"chat_view"];
    [FBSDKAppEvents logEvent:@"chat_view"];

    [self.timerUpdateOffers setFireDate:[NSDate distantPast]];
    if (self.firstTimeToAppear == 1) {
        self.firstTimeToAppear = 0;
    }
    else{
        //////refresh the review information status.
        [self getHistoryReviewInformation];
    }
   
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [HGUserData sharedInstance].enterChatCounter++;
    
    if([HGUserData sharedInstance].bChatShowReviewTip && [HGUserData sharedInstance].enterChatCounter == 2){
        [HGUserData sharedInstance].bChatShowReviewTip = NO;
    
        self.popTipReview = [[HGUtils sharedInstance] showTipsWithMessage:NSLocalizedString(@"Review your experience with this person here!",nil) atBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES withOffset:CGPointMake(0, -5) withSize:CGSizeMake(160,70)];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];

    [SVProgressHUD dismiss];
    [self.popTipReview dismissAnimated:NO];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [self.timerUpdateOffers setFireDate:[NSDate distantFuture]];

}

- (void)dealloc
{
    if (self.timerUpdateOffers) {
        [self.timerUpdateOffers invalidate];
        self.timerUpdateOffers = nil;
    }
}

- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning  in HGChatViewController.");
    [super didReceiveMemoryWarning];
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(actionSheet.tag == TAG_ACTION_SHEET_REVIEW){
    
        switch (buttonIndex) {
            case 0:
            {
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:reviewActionGAstring label:nil value:nil];

                [self startReviewUserController];
                break;
            }
            case 1:
            {
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_reportperson" label:nil value:nil];

                UserReportTableViewController *vcUserReport = [[UserReportTableViewController alloc] initWithNibName:@"UserReportTableViewController" bundle:nil];
                vcUserReport.reportType = ReportReasonTypeSeller;
                vcUserReport.userID = self.toUser.uid;
                [self.navigationController pushViewController:vcUserReport animated:YES];
                break;
            }
            case 2:
            {
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:blockActionGAstring label:nil value:nil];

                if(self.bBlockedUser){
                    [self unblockUser];
                }else{
                    [self blockUser];
                }
                
                break;
            }
            default:
            {
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"reviewbuttoncancel" label:nil value:nil];
                break;
            }
        }
    }
    if (actionSheet.tag == TAG_ACTION_SHEET_REVIEW_CLOSE) {
        switch (buttonIndex) {
           
            case 0:
            {
                UserReportTableViewController *vcUserReport = [[UserReportTableViewController alloc] initWithNibName:@"UserReportTableViewController" bundle:nil];
                vcUserReport.reportType = ReportReasonTypeSeller;
                vcUserReport.userID = self.toUser.uid;
                [self.navigationController pushViewController:vcUserReport animated:YES];
                break;
            }
            case 1:
            {
                if(self.bBlockedUser){
                    [self unblockUser];
                }else{
                    [self blockUser];
                }
                
                break;
            }
            default:
                break;
        }

    }
    if (actionSheet.tag == TAG_ACTION_SHEET_CHAT_MORE) {
       
        //todo: alex.
        UINavigationController *vcChat = (UINavigationController *)[[UIStoryboard messageStoryboard]instantiateViewControllerWithIdentifier:@"chatMapNavigation"];
        [self presentViewController:vcChat animated:YES completion:^(){}];
        //[self.navigationController pushViewController:vcChat animated:YES];
    }
}


#pragma mark - helpers

- (void)blockUser{
    
    NSString* prefixString = NSLocalizedString(@"Are you sure you want to block them?", nil);
    NSString* CompleteString = NSLocalizedString(@"Are you sure you want to block them?\nThis 5miler will no longer be able to connect with you.",nil);
    NSRange  range = [CompleteString rangeOfString:prefixString];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:CompleteString];
    if (range.length > 0 ) {
        UIFont* font = [UIFont boldSystemFontOfSize:15];
        [attrString addAttribute:NSFontAttributeName value:font range:range];
        
        //[attrString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, prefixString.length)];
     
    }

    
    PXAlertView *alert = [PXAlertView showAlertWithTitle:nil messageWithAttribute:attrString cancelTitle:NSLocalizedString(@"Cancel", nil) otherTitle:NSLocalizedString(@"Confirm", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
        
        if(!cancelled){
            [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"block/" parameters:@{@"target_user":self.toUser.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
                self.bBlockedUser = YES;
                [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"You have successfully blocked them.", nil) onView:self.view];
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_blockyes" label:nil value:nil];

            } failure:^(NSURLSessionDataTask *task, NSError *error) {}];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_blockno" label:nil value:nil];

        }
    }];
    
    [alert useDefaultIOS7Style];
}


- (void)unblockUser{
    
    NSString* prefixString = NSLocalizedString(@"Are you sure you want to unblock them?", nil);
    NSString* CompleteString =NSLocalizedString(@"Are you sure you want to unblock them?\nThis 5miler will now be able to connect with you.",nil);
    NSRange  range = [CompleteString rangeOfString:prefixString];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:CompleteString];
    if (range.length > 0 ) {
        UIFont* font = [UIFont boldSystemFontOfSize:15];
        [attrString addAttribute:NSFontAttributeName value:font range:range];
        
        //[attrString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, prefixString.length)];
        
    }

    
    PXAlertView *alert = [PXAlertView showAlertWithTitle:nil messageWithAttribute:attrString cancelTitle:NSLocalizedString(@"Cancel", nil) otherTitle:NSLocalizedString(@"Confirm", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
        
        if(!cancelled){
            [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unblock/" parameters:@{@"target_user":self.toUser.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
                self.bBlockedUser = NO;
                [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"You have successfully unblocked them.", nil) onView:self.view];
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_unblockyes" label:nil value:nil];

                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {}];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_unblockno" label:nil value:nil];

        }
    }];
    
    [alert useDefaultIOS7Style];

}

- (void)getOffersAndOfferLineMeta{
    
    //[SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"offerline/" parameters:@{@"offerline_id": self.offerLineID} success:^(NSURLSessionDataTask *task, id responseObject) {
         [SVProgressHUD dismiss];
       NSDictionary * meta = [responseObject objectForKey:@"meta"];
        NSArray * dataset = [responseObject objectForKey:@"objects"];
        
        NSError * error;
        self.offerLineMeta = [[HGOfferLineMeta alloc] initWithDictionary:meta error:&error];
        if (error) {
            NSLog(@"fetch offerline meta failed: %@", error.userInfo);
            return;
        }
        
        self.latestTimestamp = self.offerLineMeta.latestTimestamp;
        self.item = self.offerLineMeta.item;
        
        //FIXME: currency symbol temporary solution
        self.currencyUnit = self.item.currencyUnit;
        
        HGUser* buyer = self.offerLineMeta.buyer;
        HGUser* seller = self.offerLineMeta.item.seller;
        
        if ([buyer isMe]) {
            self.bBuyer = YES;
            self.toUser = seller;
        }
        else{
            self.bBuyer = NO;
            self.toUser = buyer;
        }
        
        self.bBlockedUser = [[meta objectForKey:@"user_blocked"] boolValue];
        
        [self updateUI];
        
        NSMutableArray *offers = [NSMutableArray array];
        
        for (NSDictionary * dictObj in dataset) {
            NSError * error = nil;
            HGOffer * offer = [[HGOffer alloc] initWithDictionary:dictObj error:&error];

            if (nil == error) {
                [offers addObject:offer];
            } else {
                NSLog(@"offer init error:%@", error.userInfo);
            }
        }
        
        [self.vcChatCollection refreshMessagesWithOffers:offers];
       
        // Start the timer.
        self.timerUpdateOffers = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(updateOffers) userInfo:nil repeats:YES];
        [self getHistoryReviewInformation];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"get offerline details failed: %@", error.description);
    }];

}

-(void)getHistoryReviewInformation
{
    if (self.toUser == nil || self.item == nil || self.toUser.uid == nil || self.item.uid == nil) {
        return;
    }
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"review/" parameters:@{ @"target_user": self.toUser.uid,@"item_id":self.item.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary*dict = responseObject;
        if ([dict objectForKey:@"score"] != nil) {
            self.reviewAleadyExist = TRUE;
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get history reviews failed: %@", error.userInfo);
        
    }];
}

- (void)updateOffers
{
    self.latestTimestamp = self.vcChatCollection.latestTimestamp;
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"offerline_updates/" parameters:@{@"offerline_id": self.offerLineID, @"from_timestamp": [NSString stringWithFormat:@"%d", (int)self.latestTimestamp]} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self.latestTimestamp = [[[responseObject objectForKey:@"meta"] objectForKey:@"latest_ts"] intValue];
        self.offerLineMeta.item.state = [[[responseObject objectForKey:@"meta"] objectForKey:@"item_state"] integerValue];
        
        NSArray * dataset = [responseObject objectForKey:@"objects"];
        if (dataset.count == 0){
            return;
        }
        
        NSMutableArray *newOffers = [NSMutableArray array];
        
        for (NSDictionary * dictObj in dataset) {
            NSError * error = nil;
            HGOffer * offer = [[HGOffer alloc] initWithDictionary:dictObj error:&error];

            if (nil == error) {
                [newOffers addObject:offer];
            } else {
                NSLog(@"offer init error:%@", error.userInfo);
            }
        }
        
        [self.vcChatCollection updateMessagesWithNewOffers:newOffers];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"pull offerline updates failed: %@", error.userInfo);
    }];
    
}



- (void)onClickNaviBack
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_back" label:nil value:nil];
    [self.navigationController popViewControllerAnimated:YES];
}



static NSString* reviewActionGAstring = @"leavereview";
static NSString* blockActionGAstring = @"chat_block";
- (void)showReviewActionSheet{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"reviewbutton" label:nil value:nil];

    
    NSString *blockTitle = self.bBlockedUser ? NSLocalizedString(@"Unblock this Person",nil):NSLocalizedString(@"Block this Person",nil);
    if (self.bBlockedUser) {
        blockActionGAstring = @"chat_unblock";
    }
    
    
    NSString* reviewTitle = NSLocalizedString(@"Leave Review", nil);
    if (self.reviewAleadyExist) {
        reviewActionGAstring = @"changereview";
        reviewTitle = NSLocalizedString(@"Change Review", nil);
    }
    
    if([HGAppData sharedInstance].bOpenReviewFunctions)
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles: reviewTitle,NSLocalizedString(@"Report this Person", nil), blockTitle, nil];
        actionSheet.tag = TAG_ACTION_SHEET_REVIEW;
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showInView:self.view];
    }
    else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Report this Person", nil), blockTitle, nil];
        actionSheet.tag = TAG_ACTION_SHEET_REVIEW_CLOSE;
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showInView:self.view];
    }
}

- (void)onTapNavigationBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onTapItemDetail{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_item" label:nil value:nil];
    HGShopItem* item = [[HGShopItem alloc] init];
    item.uid = self.item.uid;
    
    HGItemDetailController *vcItemDetail = (HGItemDetailController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCItemDetail"];
    vcItemDetail.item = item;
    vcItemDetail.trackstring = @"chat";
    [self.navigationController pushViewController:vcItemDetail animated:YES];
}

- (void)updateUI{
    
    [self configNavigationBar];
    [self configHeaderView];
    [self configCollectionView];
    
}

- (void)configNavigationBar{
    
    self.navigationItem.title = self.toUser.displayName;
}

- (void)configHeaderView{
    UIImageView *ivItem = (UIImageView *)[self.headerView viewWithTag:1000];
    ivItem.contentMode = UIViewContentModeScaleAspectFill;
    ivItem.clipsToBounds = YES;
    ivItem.layer.borderWidth = 0;
    ivItem.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
    
    int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:ivItem.bounds.size.width * 2];
    int suggestHeight = [[HGUtils sharedInstance] getImageSuggestEdgeLength:ivItem.bounds.size.width * 2];
    NSURL *avatarImageURL = [self.item coverImageUrlWithWidth:suggestWidth height:suggestHeight];
    
    [ivItem sd_setImageWithURL: avatarImageURL placeholderImage:[UIImage imageWithColor:FANCY_COLOR(@"eaeaea")]];
    
    UILabel *lbPrice = (UILabel *)[self.headerView viewWithTag:1001];
    lbPrice.font = [UIFont systemFontOfSize:15];
    lbPrice.text = [self.item priceForDisplay];
    
    
    //they do want to show right arrow here,hidden
    UIImageView *ivNaviForward = (UIImageView *)[self.headerView viewWithTag:1002];
    ivNaviForward.image = [UIImage imageNamed:@"navi_forward"];
    ivNaviForward.hidden = YES;
    
}

- (void)configCollectionView{
    [self.vcChatCollection refreshDataFromChatVC:self];
}


-(void)startReviewUserController
{
    HGRateUserController *controller = (HGRateUserController *)[[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:@"RateUserController"];
    if (self.bBuyer) {
        // 0: buyer rate seller
        controller.direction = 0;
    }
    else{
        // 1: seller rate buyer.
        controller.direction = 1;
    }
    controller.userID = self.toUser.uid;
    controller.userName = self.toUser.displayName;
    controller.itemID = self.item.uid;
   
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)chatMoreAction:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles: @"Show Maps",NSLocalizedString(@"Report this Person", nil), @"Test", nil];
    actionSheet.tag = TAG_ACTION_SHEET_CHAT_MORE;
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}
@end

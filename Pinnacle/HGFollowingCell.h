//
//  HGFollowingCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HGFollowingCellDelegate <NSObject>

@optional
-(void)onTouchUnfollowing:(NSString*)uidString;
-(void)onTouchFollowing:(NSString*)uidString;
@end


@class HGUser;

@interface HGFollowingCell : UITableViewCell<UIActionSheetDelegate>

@property (nonatomic, strong) UIView* seperatorLine;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;



@property (nonatomic, strong) UIButton* followButton;
@property (nonatomic, strong) UIImageView* followIcon;

@property (nonatomic, strong) NSString* uid;
@property (nonatomic, assign) BOOL following;
@property (nonatomic, assign) id<HGFollowingCellDelegate> delegate;

@property (nonatomic, assign) BOOL isMyFollowing;
- (void)configWithObject:(HGUser *)entry withIsMyFollowing:(BOOL)isMyFollowing;

@end

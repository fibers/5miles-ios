//
//  HGPublicPageLayoutModel.m
//  Pinnacle
//
//  Created by mumuhou on 15/7/17.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGPublicPageLayoutModel.h"

@implementation HGPublicPageLayoutModel {
    NSDictionary *_sectionAndRowMap;
}

- (id)init
{
    if (self = [super init]) {
        _currentLayoutType = HGPublicPageLayoutModelLayoutTypeService;
        _currentSectionTypeArray = @[@(HGPublicPageLayoutModelSectionTypeTitle),
                                     @(HGPublicPageLayoutModelSectionTypeCategory),
                                     @(HGPublicPageLayoutModelSectionTypeDescription),
                                     @(HGPublicPageLayoutModelSectionTypeVoice),
                                     @(HGPublicPageLayoutModelSectionTypeShare),
                                     ];
        [self buildSectionsAndRowsMap];
    }
    return self;
}

- (void)buildSectionsAndRowsMap
{
    NSMutableDictionary *mutableDic = [[NSMutableDictionary alloc] initWithCapacity:6];
    
    // section 0
    [mutableDic setObject:@[@(HGPublicPageLayoutModelRowTypeTitle)] forKey:@(HGPublicPageLayoutModelSectionTypeTitle)];
    
    // section 1
    [mutableDic setObject:@[@(HGPublicPageLayoutModelRowTypeCategory),
                            @(HGPublicPageLayoutModelRowTypePrice),
                            @(HGPublicPageLayoutModelRowTypeLocation)
                            ] forKey:@(HGPublicPageLayoutModelSectionTypeCategory)];
    
    // section 2
    [mutableDic setObject:@[@(HGPublicPageLayoutModelRowTypeDescription)
                            ] forKey:@(HGPublicPageLayoutModelSectionTypeDescription)];
    
    // section 3
    [mutableDic setObject:@[@(HGPublicPageLayoutModelRowTypeVoice)
                            ] forKey:@(HGPublicPageLayoutModelSectionTypeVoice)];
    
    // section 4
    [mutableDic setObject:@[@(HGPublicPageLayoutModelRowTypeOriganlPrice),
                            @(HGPublicPageLayoutModelRowTypeBrand),
                            @(HGPublicPageLayoutModelRowTypeDelivery)
                            ] forKey:@(HGPublicPageLayoutModelSectionTypeMoreInformation)];
    
    // section 5
    [mutableDic setObject:@[@(HGPublicPageLayoutModelRowTypeShare)
                            ] forKey:@(HGPublicPageLayoutModelSectionTypeShare)];
    
    _sectionAndRowMap = mutableDic;
}

- (void)setCurrentLayoutType:(HGPublicPageLayoutModelLayoutType)currentLayoutType
{
    _currentLayoutType = currentLayoutType;
    
    if (_currentLayoutType == HGPublicPageLayoutModelLayoutTypeListing) {
        _currentSectionTypeArray = @[@(HGPublicPageLayoutModelSectionTypeTitle),
                                     @(HGPublicPageLayoutModelSectionTypeCategory),
                                     @(HGPublicPageLayoutModelSectionTypeDescription),
                                     @(HGPublicPageLayoutModelSectionTypeVoice),
                                     @(HGPublicPageLayoutModelSectionTypeMoreInformation),
                                     @(HGPublicPageLayoutModelSectionTypeShare),
                                     ];
    } else if (_currentLayoutType == HGPublicPageLayoutModelLayoutTypeService) {
        _currentSectionTypeArray = @[@(HGPublicPageLayoutModelSectionTypeTitle),
                                     @(HGPublicPageLayoutModelSectionTypeCategory),
                                     @(HGPublicPageLayoutModelSectionTypeDescription),
                                     @(HGPublicPageLayoutModelSectionTypeVoice),
                                     @(HGPublicPageLayoutModelSectionTypeShare),
                                     ];
    }
}

- (NSArray *)rowTypeArrayForSection:(NSInteger)section
{
    HGPublicPageLayoutModelSectionType sectionType = [self.currentSectionTypeArray[section] integerValue];
    return [_sectionAndRowMap objectForKey:@(sectionType)];
}

@end

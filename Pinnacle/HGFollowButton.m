//
//  HGFollowButton.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGFollowButton.h"
#import "HGConstant.h"
#import <UIColor+FlatColors.h>
#import "UIImage+pureColorImage.h"

@implementation HGFollowButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _commonSetup];
}

- (void)_commonSetup
{
    self.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.layer.cornerRadius = 6;
    
    
    CGRect rx = [ UIScreen mainScreen ].bounds;
    float icon_xoff = rx.size.width/2 - 60;
    self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(icon_xoff,6, 40/2, 42/2)];
    self.icon.image = [UIImage imageNamed:@"user-profile-follow-icon"];
    [self addSubview:self.icon];
    
    
    self.myTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame)+10, 8, 100, 20 )];
    [self addSubview: self.myTitle];
    self.myTitle.textColor = [UIColor whiteColor];
    self.myTitle.backgroundColor = [UIColor clearColor];
    self.myTitle.font = [UIFont systemFontOfSize:15];
    self.myTitle.textAlignment = NSTextAlignmentLeft;
    self.myTitle.text= @"";
    self.hidden = YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setDisplayType:(HGFollowButtonType)displayType
{
    _displayType = displayType;
    switch (_displayType) {
        case HGFollowButtonTypeFollowing:
        {
            UIImage* currentImage = [UIImage imageNamed:@"user-profile-following-icon"];
             self.icon.image =[UIImage getTintedImage:currentImage withColor:[UIColor whiteColor]];
            
           
            NSString * btnTitle = @"";
            [self setTitle:btnTitle forState:UIControlStateNormal];
          
            self.titleLabel.clipsToBounds = NO;
            self.myTitle.text = NSLocalizedString(@"Following", nil);
            
            
            self.myTitle.textColor = [UIColor whiteColor];
            self.backgroundColor = FANCY_COLOR(@"ff8830");
            
            
            
        }
            break;
            
        case HGFollowButtonTypeNotFollowing:
        {
            UIImage* currentImage = [UIImage imageNamed:@"user-profile-follow-icon"];
            self.icon.image =[UIImage getTintedImage:currentImage withColor:FANCY_COLOR(@"ff8830")];
            
            NSString * btnTitle = @"";
            [self setTitle:btnTitle forState:UIControlStateNormal];
            
            self.myTitle.textColor = FANCY_COLOR(@"ff8830");
            self.myTitle.text = NSLocalizedString(@"Follow", nil);
            
            self.backgroundColor = [UIColor whiteColor];
           
            self.layer.borderWidth = 1.0;
            self.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
        }
            break;
            
        default:
            break;
    }
    self.hidden = NO;
}

@end

//
//  UINavigationBar+Pinnacle.h
//  Pinnacle
//
//  Created by mumuhou on 15/7/23.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UINavigationBar(Pinnacle)

- (void)lt_setBackgroundColor:(UIColor *)backgroundColor;
- (void)lt_setContentAlpha:(CGFloat)alpha;
- (void)lt_setTranslationY:(CGFloat)translationY;
- (void)lt_reset;

@end

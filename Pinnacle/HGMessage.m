//
//  HGMessage.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-4.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGMessage.h"
#import "HGAppData.h"
#import "HGUser.h"
#import "MHPrettyDate.h"

@implementation HGMessage

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid", @"item_id":@"itemID", @"item_image":@"itemImage", @"thread_id":@"threadID", @"count":@"badgeCount",@"item_title":@"item_title",@"owner_nickname":@"owner_nickname"
                                                       }];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[@"text", @"badgeCount", @"timestamp", @"to", @"owner_nickname",@"item_title"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    return NO;
}

- (NSString *)prettyDate
{
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:self.timestamp];
    NSString * dateString = [MHPrettyDate prettyDateFromDate:date withFormat:MHPrettyDateFormatTodayTimeOnly];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MM/dd/yyyy"];
    if ([dateString rangeOfString:@"/"].location != NSNotFound) {
        dateString=[fmt stringFromDate:date];

    }
    
    return dateString;
}

- (BOOL)fromMe
{
    return [self.from.uid isEqualToString:[HGUserData sharedInstance].fmUserID];
}

@end

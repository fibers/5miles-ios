//
//  HGPurchaseItemCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGPurchaseItemCell.h"
#import "HGShopItem.h"
#import "HGUtils.h"
#import "UIImage+pureColorImage.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIApplication+Pinnacle.h"
#import "HGAppData.h"
@interface HGPurchaseItemCell ()

@property (nonatomic, strong) NSString* itemId;
@property (nonatomic, assign) BOOL bcanBeRenew;
@end
@implementation HGPurchaseItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       self.translatesAutoresizingMaskIntoConstraints = NO;
       self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
       [self commonSetup];
        
        [self configConstraint];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [self commonSetup];
}
-(void)commonSetup
{
    self.bcanBeRenew = YES;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.lbTitle.font = [UIFont systemFontOfSize:15];
    self.lbPrice.font = [UIFont systemFontOfSize:15];
    
    self.coverImgView.layer.cornerRadius = 3;
    self.coverImgView.layer.borderWidth  = 0;
    self.coverImgView.layer.borderColor = FANCY_COLOR(@"d8d8d8").CGColor;
    self.coverImgView.clipsToBounds = YES;
    self.coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.SeperatorView = [[UIView alloc] initWithFrame:CGRectMake(12, self.frame.size.height-1, self.frame.size.width - 12, 0.5)];
    [self addSubview:self.SeperatorView];
    self.SeperatorView.backgroundColor =[UIColor colorWithRed:0xcc/255.0 green:0xcc/255.0 blue:0xcc/255.0 alpha:1.0];
    
   
    
    
    self.statusButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-12-56, 20, 56, 26)];
    self.statusButton.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
    self.statusButton.layer.borderWidth = 1;
    self.statusButton.layer.cornerRadius = 4;
    [self.statusButton setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
    self.statusButton.titleLabel.font = [UIFont systemFontOfSize:12];
    self.statusButton.hidden = YES;
    [self.statusButton addTarget:self action:@selector(onTouchRenew) forControlEvents:UIControlEventTouchUpInside];
    [self.statusButton setUserInteractionEnabled:NO];
    [self.contentView addSubview:self.statusButton];
    
    self.userInteractionEnabled = YES;
    self.contentView.userInteractionEnabled = YES;
   
    
}
-(void)updateConstraints
{
    [super updateConstraints];
    
}
-(void)layoutSubviews
{
     
    self.SeperatorView.frame = CGRectMake(12, self.frame.size.height-0.5, self.frame.size.width - 12, 0.5);
  
}
-(void)onTouchRenew
{
    if (self.itemId == nil || self.itemId.length == 0 || !self.bcanBeRenew) {
        return;
    }
    
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"renew_item/" parameters:@{@"item_id": self.itemId} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"renew_item ok");
        
        
        self.bcanBeRenew = NO;
        self.statusButton.backgroundColor = [UIColor whiteColor];
        self.statusButton.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
        [self.statusButton setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
        self.statusButton.userInteractionEnabled = NO;
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myitems_view" action:@"renewbutton" label:nil value:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"renew_item fail");
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithEntity:(HGShopItem *)item
{
    HGItemImage * coverImage = [item.images firstObject];
    self.itemId = item.uid;
    
    if (item.renew_ttl > 0) {
        self.bcanBeRenew = NO;
    }
    
    int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.coverImgView.bounds.size.width * 2];
    int suggestHeight = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.coverImgView.bounds.size.height * 2];
    
    NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:coverImage.imageLink width:suggestWidth height:suggestHeight];

    [self.coverImgView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageWithColor:FANCY_COLOR(@"eaeaea")]];
    self.lbTitle.text = item.title;    
    self.lbPrice.text = [item priceForDisplay];
    
    switch (item.state) {
        case HGItemStateListing:
            self.lbState.text = NSLocalizedString(@"Listing", nil);
            break;
        case HGItemStateOfferAccepted:
        {
            self.lbState.text = NSLocalizedString(@"Sold", nil);
            break;
        }
            
        case HGItemStateDoneDelivery:
        {
            self.lbState.text = NSLocalizedString(@"Sold", nil);
            break;
        }
            
        case HGItemStateRatedSeller:
        {
            self.lbState.text = NSLocalizedString(@"Sold", nil);
            break;
        }
           
            
        case HGItemStateUnapproved:
            self.lbState.text = NSLocalizedString(@"Unapproved", nil);
            break;
        case HGItemStateUnavailable:
            self.lbState.text = NSLocalizedString(@"Unavailable", nil);
            break;
        case ITEM_STATE_UNLISTED:
            self.lbState.text = NSLocalizedString(@"Unlisted",nil);
            break;
        case ITEM_STATE_PENDING:
            self.lbState.text = NSLocalizedString(@"Pending",nil);
            break;
        default:
            self.lbState.text = NSLocalizedString(@"Sold", nil);
            break;
    }
    
    if (self.CellType == HGItemCellType_MyItem) {
        self.statusButton.hidden = NO;
        [self.statusButton setTitle:self.lbState.text forState:UIControlStateNormal];
        self.lbState.hidden = YES;
        if (item.state == HGItemStateListing && self.bcanBeRenew) {
            self.statusButton.backgroundColor = FANCY_COLOR(@"ff8830");
            self.statusButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
            [self.statusButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.statusButton setTitle: NSLocalizedString(@"Renew", nil) forState:UIControlStateNormal];
            self.statusButton.userInteractionEnabled = YES;
        }
        else{
            self.statusButton.backgroundColor = [UIColor whiteColor];
            self.statusButton.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
            [self.statusButton setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
            
             [self.statusButton setUserInteractionEnabled:NO];
        }
        
        if (self.lbState.text.length > @"listing".length) {
            self.statusButton.frame = CGRectMake(self.frame.size.width-12-74, self.statusButton.frame.origin.y, 74, self.statusButton.frame.size.height);
        }
        else{
             self.statusButton.frame = CGRectMake(self.frame.size.width-12-56, self.statusButton.frame.origin.y, 56, self.statusButton.frame.size.height);
        }
        
        
    }
}

-(void)configConstraint
{
    NSLayoutConstraint* lbstateRight = [NSLayoutConstraint constraintWithItem:self.lbState attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0f constant:-12.0];
    [self addConstraint:lbstateRight];
}


@end

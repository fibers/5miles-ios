//
//  HGItemSingleLineCell.m
//  Pinnacle
//
//  Created by Alex on 15/7/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemSingleLineCell.h"
#import "HGConstant.h"
#import "HGShopItem.h"
#import "HGUtils.h"
#import "HGAppData.h"
#import "UIImage+pureColorImage.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>

#define CELL_FONT_COLOR_1 [UIColor colorWithRed:0x24/255.0 green:0x24/255.0 blue:0x24/255.0 alpha:1.0];
#define CELL_SUGGEST_FONT_SIZE 16

@implementation HGItemSingleLineCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _setupCell];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setupCell];
}

-(void)addSellerAvatar
{
    self.avatarView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.imageView.frame) + 10, 70, 25, 25)];
    [self.contentView addSubview:self.avatarView];
    self.avatarView.layer.cornerRadius = self.avatarView.frame.size.width/2;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.borderWidth = 1.0;
    self.avatarView.layer.borderColor= FANCY_COLOR(@"ff883040").CGColor;
    
    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.avatarView.frame.size.width*0.4, self.avatarView.frame.size.width*0.4)];
    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    CGPoint centerPoint = self.avatarView.center;
    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    self.userAvatar_verifyMark.center = centerPoint;
    [self.contentView addSubview:self.userAvatar_verifyMark];

    
    
}

-(void)addRatingView
{
    self.UserRatingView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 75, 6.4*1.5)];
    
    [self.contentView addSubview:self.UserRatingView];
    
}

- (void)_setupCell
{
    self.contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.contentView.layer.borderWidth = 0.0f;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    
    self.bottomShadow = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:self.bottomShadow];
    
    
    self.imageView = [[UIImageView alloc]initWithFrame: CGRectZero];
    [self.contentView addSubview:self.imageView];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds =YES;
    
    [self addSellerAvatar];
    [self addRatingView];
    
    
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.imageView.backgroundColor = [UIColor grayColor];
    
    
    self.ItemNewIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 66/2, 44/2)];
    self.ItemNewIcon.image = [UIImage imageNamed:@"home_item_new"];
    self.ItemNewIcon.hidden = NO;
    [self.imageView addSubview:self.ItemNewIcon];
    
    
    
    
    
    
    self.lbDistance = [[UILabel alloc] initWithFrame:CGRectZero];
    self.lbDistance.backgroundColor = [UIColor clearColor];
    self.lbDistance.font = [UIFont systemFontOfSize:12];
    self.lbDistance.textColor = FANCY_COLOR(@"919191");
    self.lbDistance.textAlignment =NSTextAlignmentRight;
    [self.contentView addSubview:self.lbDistance];
    
    self.lbPrice = [[UILabel alloc] initWithFrame:CGRectZero];
    self.lbPrice.backgroundColor = [UIColor clearColor];
    self.lbPrice.textAlignment = NSTextAlignmentLeft;
    self.lbPrice.font = [UIFont fontWithName:@"Helvetica-bold" size:14];;
    self.lbPrice.textColor = CELL_FONT_COLOR_1;
    [self.contentView addSubview:self.lbPrice];
    
    self.originalPrice = [[StrikeThroughLabel alloc] initWithFrame:CGRectZero];
    self.originalPrice.backgroundColor = [UIColor clearColor];
    self.originalPrice.textAlignment = NSTextAlignmentLeft;
    self.originalPrice.textColor = self.originalPrice.strikeColor = FANCY_COLOR(@"818181");
    self.originalPrice.font = [UIFont fontWithName:@"Helvetica" size:14];;
    self.originalPrice.strikeThroughEnabled = YES;
    [self.contentView addSubview:self.originalPrice];
    
    self.soldIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-sold"]];
    [self.contentView addSubview:self.soldIconView];
    
    
    self.locationIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"item_detail_location"]];
    //[self.contentView addSubview:self.locationIcon];
    
    
   
    
    self.itemTitle = [[UILabel alloc] init];
    self.itemTitle.font = [UIFont systemFontOfSize:16];
    [self.contentView addSubview: self.itemTitle];
    self.itemTitle.numberOfLines = 1;
    self.itemTitle.textColor = FANCY_COLOR(@"242424");
    
   
}
- (void)layoutSubviews
{
    
    self.contentView.frame = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width-1, self.bounds.size.height-1) ;
    
   
    self.bottomShadow.frame = CGRectMake(108, self.bounds.size.height - 1, self.bounds.size.width -108, 1);
    
    
    
    self.imageView.frame = CGRectMake(0, 5, 100 , 100);
    
    self.avatarView.frame = CGRectMake(SCREEN_WIDTH-10-25, 70, 25, 25);
    CGPoint centerPoint = self.avatarView.center;
    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    self.userAvatar_verifyMark.center = centerPoint;
    self.UserRatingView.frame = CGRectMake(CGRectGetMaxX(self.avatarView.frame) -110, CGRectGetMaxY(self.avatarView.frame)-8, 75, 6.4*1.5);
    
    
    self.ItemNewIcon.frame = CGRectMake(self.imageView.frame.size.width - 6 - 66/2, self.imageView.frame.size.height - 6 - 44/2, 66/2, 44/2);
    [self.imageView addSubview:self.ItemNewIcon];
    
    self.itemTitle.frame = CGRectMake(108, 8 ,CGRectGetWidth(self.contentView.bounds)-8, 22 );
    
    self.soldIconView.frame = CGRectMake(0, 0, 30, 30);
    self.soldIconView.center = self.imageView.center;
  
    self.locationIcon.frame = CGRectMake(108 ,  8+ 22 + 19, 17, 17);
    self.lbDistance.frame = CGRectMake(CGRectGetMidX(self.avatarView.frame)-170, self.frame.size.height - 45,  150, 24);
    
    {
        self.lbPrice.frame = CGRectMake(108, 8 + 22, CGRectGetWidth(self.bounds) * 1 - 4, CGRectGetHeight(self.lbDistance.frame));
        
        self.originalPrice.frame = CGRectMake(self.lbPrice.frame.size.width, 8 + 22,  80, 24);
        if (self.lbPrice.text.length > 0) {
            CGRect lbPriceRect = [self.lbPrice.text boundingRectWithSize: CGSizeMake(300, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lbPrice.font} context:NULL];
            self.lbPrice.frame = CGRectMake(self.lbPrice.frame.origin.x, self.lbPrice.frame.origin.y, lbPriceRect.size.width , self.lbPrice.frame.size.height);
            
            int max_PriceWidth = MIN(CGRectGetWidth(self.bounds) * 1 - 13 - lbPriceRect.size.width, 180);
            self.originalPrice.frame = CGRectMake(12+CGRectGetMaxX(self.lbPrice.frame), 8 + 22, max_PriceWidth, 24);
            
        }
    }
    
   
}


- (void)configWithItem:(HGShopItem *)item
{
    HGItemImage * coverImage = [item.images firstObject];
    NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:coverImage.imageLink width:300 height:0];
    
    
    
    @try {
        UIColor* color = [[HGAppData sharedInstance].AppInitDataSingle.default_placehold_colors objectAtIndex:rand()%24];
        UIImage*fancyImage = [UIImage imageWithColor:color];
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:fancyImage
                                   options:SDWebImageRetryFailed
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     if (error) {
                                         NSLog(@"load image error: %@", error);
                                     }
                                 }];
    }
    @catch (NSException *exception) {
        [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
    }
    
    UIColor* color = [[HGAppData sharedInstance].AppInitDataSingle.default_placehold_colors objectAtIndex:rand()%24];
    UIImage*fancyImage = [UIImage imageWithColor:color];
    [self.avatarView sd_setImageWithURL: [NSURL URLWithString:item.seller.portraitLink] placeholderImage:fancyImage];
    if (item.seller.verified.intValue == 1) {
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    
    
    if (item.seller.review_num.intValue > 0) {
        self.UserRatingView.hidden = NO;
        float review_score = item.seller.review_score.floatValue;
        NSString* image_index = [[HGUtils sharedInstance] getReviewImageNameByScore:review_score];
        self.UserRatingView.image = [UIImage imageNamed:image_index];
 
    }
    else{
        self.UserRatingView.hidden = YES;
    }
    
    
    if (item.is_new == 1) {
        if (item.state == HGItemStateListing) {
            self.ItemNewIcon.hidden = NO;
        }
        else{
            self.ItemNewIcon.hidden = YES;
        }
    }
    else{
        self.ItemNewIcon.hidden = YES;
    }
    
    self.itemTitle.text = item.title;
    
    self.soldIconView.hidden = item.state == HGItemStateListing;
    if (item.state >= HGItemStateOfferAccepted) {
        self.soldIconView.hidden = NO;
    }
    if (item.state == HGItemStateUnapproved) {
        self.soldIconView.hidden = NO;
        self.soldIconView.image = [UIImage imageNamed:@"icon-Unapproved"];
    }
    if (item.state == HGItemStateUnavailable) {
        self.soldIconView.hidden = NO;
        self.soldIconView.image = [UIImage imageNamed:@"icon-Unavailable"];
        
    }
    
    //address name has higher priority
    NSString *displayPlace;
    NSString *address = [item formatAddress];
    if (address.length > 0) {
        displayPlace = address;
    }
    else
    {
        NSString *distance = [item formatDistance];
        if ( distance.length == 0) {
            //user close location
            displayPlace = NSLocalizedString(@"unknown", nil);
        }else{
            displayPlace = distance;
        }
    }
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"singleLineCell_location_icon"];
    
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSMutableAttributedString* attachmentString2 =[[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    
    
    displayPlace = [@" " stringByAppendingString:displayPlace];
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:displayPlace];
    [attachmentString2  appendAttributedString:myString];
    self.lbDistance.attributedText = attachmentString2;
    self.lbPrice.text = [item priceForDisplay];
    
    
    if (self.lbPrice.text.length > 0) {
        CGRect lbPriceRect = [self.lbPrice.text boundingRectWithSize: CGSizeMake(300, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lbPrice.font} context:NULL];
        self.lbPrice.frame = CGRectMake(self.lbPrice.frame.origin.x, self.lbPrice.frame.origin.y, lbPriceRect.size.width , self.lbPrice.frame.size.height);
        
        
        int max_PriceWidth = MIN(CGRectGetWidth(self.bounds) * 1 - 13 - lbPriceRect.size.width, 180);
        self.originalPrice.frame = CGRectMake(12+CGRectGetMaxX(self.lbPrice.frame), 8 + 22, max_PriceWidth, 24);
        
    }
    
    self.originalPrice.text = [item originalPriceForDisplay];
    
    
   
}
@end

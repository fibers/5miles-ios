//
//  HGCategoryTableCell.m
//  Pinnacle
//
//  Created by Alex on 15-3-16.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGCategoryTableCell.h"
#import "HGConstant.h"
@implementation HGCategoryTableCell

- (void)awakeFromNib {
    // Initialization code
}



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commonSetup];
        
    }
    return self;
}

-(id)init
{
    self = [super init];
    if (self) {
        [self commonSetup];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self commonSetup];
    }
    return self;
}
-(void)commonSetup
{
    self.imgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7, 32, 32)];
    //[self addSubview:self.imgIcon];
    
    self.lbTitle = [[UILabel alloc] initWithFrame:CGRectMake(11, 10, 200, 30)];
    self.lbTitle.font = [UIFont systemFontOfSize:15];
    self.lbTitle.textColor = FANCY_COLOR(@"242424");
    [self addSubview:self.lbTitle];
    
    self.seperatorView = [[UIView alloc] initWithFrame:CGRectMake(11, self.frame.size.height-0.5, self.frame.size.width, 0.5)];
    self.seperatorView.backgroundColor = FANCY_COLOR(@"cccccc");
    [self addSubview:self.seperatorView];
  
}


-(void) layoutSubviews
{
    [super layoutSubviews];
    
    self.seperatorView.frame = CGRectMake(11, self.frame.size.height-0.5, self.frame.size.width, 0.5);
}




@end

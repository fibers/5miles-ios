//
//  HGChatCollectionViewController.m
//  Pinnacle
//
//  Created by shengyuhong on 15/5/13.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatCollectionViewController.h"
#import "HGUserData.h"
#import "HGUtils.h"
#import "JSQMessagesCollectionViewCell+Pinnacle.h"
#import "HGUser.h"
#import "HGOffer.h"
#import "HGShopItem.h"
#import "HGOfferLine.h"
#import "FXBlurView.h"
#import <UIImageView+WebCache.h>
#import "HGAppData.h"
#import "HGChatSupplementaryReusableView.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGChatMapController.h"
#import "HGUserViewController.h"
#import "UIStoryboard+Pinnacle.h"
#import "JSQMessage+Pinnacle.h"
#import "HGChatPhotoMediaItem.h"
#import "HGChatLocationMediaItem.h"
#import "HGChatMapWhereToMeetVC.h"
#import <SVProgressHUD.h>
#import "X4ImageViewer.h"
#import "HGChatImageViewerViewController.h"
#import "HGWebContentController.h"
#import "UIImage+Scale.h"
#import <QBPopupMenu/QBPopupMenu.h>
static const NSInteger TagSheetAccessory = 1000;
static const NSUInteger MaxImagesCanBeSent = 6;

@interface HGChatCollectionViewController ()<HGChatMapDelegate>

@property (nonatomic, assign) BOOL bHaveDistance;
@property (nonatomic, assign) BOOL offerLineChanged;
@property (nonatomic, strong) CMPopTipView *popTipAttachment;
@property (nonatomic, strong) FXBlurView *blurView;
@property (nonatomic, strong) JSQMessage *emptyMessage;
@property (nonatomic, strong) JSQMessagesAvatarImage *incomingAvatarImage;
@property (nonatomic, strong) JSQMessagesAvatarImage *outgoingAvatarImage;
@property (nonatomic, strong) JSQMessagesBubbleImage *incomingBubbleImage;
@property (nonatomic, strong) JSQMessagesBubbleImage *outgoingBubbleImage;
@property (nonatomic, strong) NSMutableParagraphStyle *styleDescription;
@property (nonatomic, strong) UIImageView *ivCollectionViewBackground;


@property (nonatomic, strong) QBPopupMenu* popupMenu;
@property (nonatomic, strong) NSString* currentTapMessageTextString;
@property (nonatomic, strong) JSQMessage* currentTapMessage;
@end

static NSString * const ChatHeadingCell = @"ChatHeadingCell";
static NSString * const ChatResuableSupplementary = @"ChatResuableSupplementary";

@implementation HGChatCollectionViewController

- (instancetype)initFromChatVC:(HGChatViewController *)vcChat{
    
    self = [[HGChatCollectionViewController alloc] initWithNibName:NSStringFromClass([JSQMessagesViewController class]) bundle:[NSBundle bundleForClass:[JSQMessagesViewController class]]];
    
    if(self){
        
        self.currentTapMessageTextString = @"";
        self.bFromItemDetailPage = vcChat.bFromItemDetailPage;
        self.bBuyer = vcChat.bBuyer;
        self.item = vcChat.item;
        self.offerLineID = vcChat.offerLineID;
        self.toUser = vcChat.toUser;
        self.delegate = vcChat.delegate;
        
        self.styleDescription = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        self.styleDescription.alignment = NSTextAlignmentLeft;
        self.styleDescription.firstLineHeadIndent = 14.0f;
        self.styleDescription.headIndent = 14.0f;
        self.styleDescription.tailIndent = -14.0f;
    }
    
    return self;
}


-(void)languageDetect:(NSString*)inputString
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    
    NSString* languageDetectUrl = @"https://www.googleapis.com/language/translate/v2/detect?key=AIzaSyD9g-S_vye6xkxGoWzaj3IDHEb-IYcLZXw&q=";
    languageDetectUrl = [languageDetectUrl stringByAppendingString:inputString];
    
    
    languageDetectUrl = [languageDetectUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [request setURL:[NSURL URLWithString:languageDetectUrl]];
    
    //Send an asynchronous request so it doesn't pause the main thread
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSHTTPURLResponse *responseCode = (NSHTTPURLResponse *)response;
        if ([responseCode statusCode] == 200)
        {
            if (data == nil) {
                return;
            }
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingAllowFragments error:nil];
            NSDictionary* dataDict = [dict objectForKey:@"data"];
            NSLog(@"the result is %@", dataDict);
            NSArray* detections = [dataDict objectForKey:@"detections"];
            NSArray* firstDetection_level_one_array = [detections objectAtIndex:0];
            NSDictionary* firstDetection= [firstDetection_level_one_array objectAtIndex:0];
           
            NSString* firstPossibleLanguage= [firstDetection objectForKey:@"language"];
            
            NSLog(@"the language may be %@", firstPossibleLanguage);
            NSString *preferredLanguages = [[NSLocale preferredLanguages] firstObject];
            if (![firstPossibleLanguage isEqualToString:preferredLanguages]) {
                [self translationString:self.currentTapMessageTextString withDetectLanguage:firstPossibleLanguage];
            }
        }
        else{
            NSLog(@"language detect error, %@, %@", responseCode, data);
        }
        
    }];
}
-(void)translationString:(NSString*)inputString withDetectLanguage:(NSString*)detectLanguage
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    
     NSString *preferredLanguages = [[NSLocale preferredLanguages] firstObject];
    NSString* languageDetectUrl = @"https://www.googleapis.com/language/translate/v2?key=AIzaSyD9g-S_vye6xkxGoWzaj3IDHEb-IYcLZXw&q=";
    languageDetectUrl = [languageDetectUrl stringByAppendingFormat:@"%@&source=%@&target=%@",inputString,detectLanguage,preferredLanguages];
    
    languageDetectUrl = [languageDetectUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [request setURL:[NSURL URLWithString:languageDetectUrl]];
    
    //Send an asynchronous request so it doesn't pause the main thread
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSHTTPURLResponse *responseCode = (NSHTTPURLResponse *)response;
        if ([responseCode statusCode] == 200)
        {
            if (data == nil) {
                return;
            }
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingAllowFragments error:nil];
            NSDictionary* dataDict = [dict objectForKey:@"data"];
            NSLog(@"the result is %@", dataDict);
            NSArray* translations = [dataDict objectForKey:@"translations"];
            
            NSDictionary* firstTranslation= [translations objectAtIndex:0];
            
            NSString* translationString= [firstTranslation objectForKey:@"translatedText"];
            NSLog(@"the language may be %@", translationString);
            
            [self.collectionView reloadData];
        }
        else{
            NSLog(@"language translate error, %@, %@", responseCode, data);
        }
        
    }];


}
-(void)MenuAction_translate
{
    [self languageDetect:self.currentTapMessageTextString];
}
-(void)MenuAction_copy
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.currentTapMessageTextString;
}
-(void)addCustomMenuItems
{
    QBPopupMenuItem *item = [QBPopupMenuItem itemWithTitle:@"Translation" target:self action:@selector(MenuAction_translate)];
    QBPopupMenuItem *item2 = [QBPopupMenuItem itemWithTitle:@"Copy" target:self action:@selector(MenuAction_copy)];

    NSArray *items = @[item, item2];
    
    QBPopupMenu *popupMenu = [[QBPopupMenu alloc] initWithItems:items];
    popupMenu.highlightedColor = [[UIColor colorWithRed:0 green:0.478 blue:1.0 alpha:1.0] colorWithAlphaComponent:0.8];
    self.popupMenu = popupMenu;

}
-(void)addBlurView
{
    // Blur view
    CGRect rx = [ UIScreen mainScreen ].bounds;
    self.ivCollectionViewBackground = [[UIImageView alloc] initWithFrame:rx];
    self.ivCollectionViewBackground.contentMode = UIViewContentModeScaleAspectFill;
    self.blurView = [[FXBlurView alloc] initWithFrame:self.ivCollectionViewBackground.bounds];
    self.blurView.dynamic = NO;
    self.blurView.blurRadius = 30.0f;
    self.blurView.tintColor = [UIColor clearColor];
    [self.ivCollectionViewBackground addSubview:self.blurView];
    self.collectionView.backgroundView = self.ivCollectionViewBackground;

}
-(void)addToolBar
{
    // Toolbar input
    self.inputToolbar.contentView.textView.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.inputToolbar.contentView.textView.font = [UIFont systemFontOfSize: 14.0];
    self.inputToolbar.contentView.textView.backgroundColor = [UIColor whiteColor];
    self.inputToolbar.contentView.textView.placeHolder = NSLocalizedString(@"Private message",nil);
    self.inputToolbar.contentView.textView.placeHolderTextColor = FANCY_COLOR(@"bdbdbd");
    self.inputToolbar.contentView.textView.layer.cornerRadius = 4.0f;
    self.inputToolbar.contentView.textView.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
    self.inputToolbar.contentView.textView.layer.borderWidth = 1.0f;
    self.inputToolbar.contentView.textView.clipsToBounds = YES;
    self.inputToolbar.contentView.textView.showsHorizontalScrollIndicator = NO;
    self.inputToolbar.contentView.textView.showsVerticalScrollIndicator = NO;
    
    // Toolbar send accessary button
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLeft setImage:[UIImage imageNamed:@"icon_chat_send_media"] forState:UIControlStateNormal];
    self.inputToolbar.contentView.leftBarButtonItem = btnLeft;
    self.inputToolbar.contentView.leftBarButtonItemWidth = 32;
    
    // Toolbar send text button
    NSString* btnRightTitle = NSLocalizedString(@"Send",nil);
    
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setTitle:btnRightTitle forState:UIControlStateNormal];
    btnRight.titleLabel.font = [UIFont systemFontOfSize:15];
    [btnRight setTitleColor:SYSTEM_DEFAULT_FONT_COLOR_1 forState:UIControlStateNormal];
    [btnRight setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateDisabled];
    self.inputToolbar.contentView.rightBarButtonItem = btnRight;
    NSMutableParagraphStyle *titleParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    titleParagraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGRect rect =[btnRightTitle boundingRectWithSize:CGSizeMake(100, 99999.0)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{
                                                       NSFontAttributeName: btnRight.titleLabel.font,
                                                       NSParagraphStyleAttributeName: titleParagraphStyle
                                                       }
                                             context:nil];
    float rightButtonWith = rect.size.width;
    if(rightButtonWith < 40)
    {
        rightButtonWith = 40;
    }
    self.inputToolbar.contentView.rightBarButtonItemWidth = rightButtonWith;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    ///todo: alex. translate not finished, in v3.2
    //[self addCustomMenuItems];
    [self addBlurView];
    
    self.messages = [NSMutableArray array];
    self.dictMessageIDs = [NSMutableDictionary dictionary];
    self.bHaveDistance = NO;
    self.emptyMessage = [JSQMessage messageWithSenderId:@"" displayName:@"" text:@""];
    self.senderId = [HGUserData sharedInstance].fmUserID;
    self.senderDisplayName = [HGUserData sharedInstance].userDisplayName;

    self.collectionView.showsHorizontalScrollIndicator = self.collectionView.showsVerticalScrollIndicator = NO;
    
    
    // JSQ bubble and avatar
    JSQMessagesBubbleImageFactory *factory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage imageNamed:@"bubble_self_defined"] capInsets:UIEdgeInsetsZero];
    
    self.incomingBubbleImage = [factory incomingMessagesBubbleImageWithColor:[UIColor whiteColor]];
    self.outgoingBubbleImage = [factory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.996f green:0.808f blue:0.698f alpha:1.00f]];
    
    self.incomingAvatarImage = [JSQMessagesAvatarImageFactory avatarImageWithPlaceholder:[UIImage imageNamed:@"default-avatar"] diameter:36];
    self.outgoingAvatarImage = [JSQMessagesAvatarImageFactory avatarImageWithPlaceholder:[UIImage imageNamed:@"default-avatar"] diameter:36];
    
    // CollectionView layout
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeMake(36,36);
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeMake(36,36);
    self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont systemFontOfSize:15];
    self.collectionView.collectionViewLayout.messageBubbleLeftRightMargin = 50;
    self.collectionView.collectionViewLayout.messageBubbleTextViewFrameInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    
    [self.collectionView registerClass:[HGChatSupplementaryReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ChatResuableSupplementary];
    
   
    [self addToolBar];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self scrollToBottomAnimated:YES];
    
    if(!IPHONE4X && self.bFromItemDetailPage){
        self.bFromItemDetailPage = NO;
        [self.inputToolbar.contentView.textView becomeFirstResponder];
    }

    
    if([HGUserData sharedInstance].bChatShowSendMediaTip){
        [HGUserData sharedInstance].bChatShowSendMediaTip = NO;
        self.popTipAttachment = [[HGUtils sharedInstance] showTipsWithMessage:@"Enhanced chat features:\n1. Send photos\n2. Select a meeting location" atView:self.inputToolbar.contentView.leftBarButtonContainerView inView:self.view animated:YES withOffset:CGPointMake(0,0) withSize:CGSizeMake(180,90) withDirection:PointDirectionDown];
    }

}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.popTipAttachment dismissAnimated:NO];
    [self.view endEditing:YES];

    if (self.offerLineChanged && self.delegate && [self.delegate respondsToSelector:@selector(didFinishChatWithNewMessage)]) {
        [self.delegate didFinishChatWithNewMessage];
    }

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.inputToolbar.contentView.textView resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
}

- (void)refreshDataFromChatVC:(HGChatViewController *)vcChat{
    
    self.bFromItemDetailPage = vcChat.bFromItemDetailPage;
    self.bBuyer = vcChat.bBuyer;
    
    if(vcChat.item){
        self.item = vcChat.item;
    }
    
    if(vcChat.offerLineID){
        self.offerLineID = vcChat.offerLineID;
    }
    
    if(vcChat.toUser){
        self.toUser = vcChat.toUser;
    }
    
    int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.collectionView.bounds.size.width];
    int suggestHeight = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.collectionView.bounds.size.height];
    
    NSURL *imageURL = [self.item coverImageUrlWithWidth:suggestWidth height:suggestHeight];
    [self.ivCollectionViewBackground sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"item-placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.blurView updateAsynchronously:YES completion:nil];
    }];
}


- (void)refreshMessagesWithOffers:(NSArray *)offers{
    
    
    [self.messages removeAllObjects];
    [self.dictMessageIDs removeAllObjects];
    
    
    
    ////wrong: will crash in ios9. can not convert to nsstring.
    NSString *lastDateString;
    BOOL showTimestamp;
    
    for (HGOffer *offer in offers) {
        
        if([self shouldShowTimestamp:offer.timestamp]){
            NSString *dateString = [[HGUtils sharedInstance]friendlyDate:offer.timestamp];
            if(lastDateString && [dateString isEqualToString:lastDateString]){
                showTimestamp = NO;
            }else{
                lastDateString = dateString;
                showTimestamp = YES;
            }
        }else{
            showTimestamp = NO;
        }
        
        JSQMessage *message = [JSQMessage messageWithOffer:offer receiver:self.toUser showTimestamp:showTimestamp];
        
        if(![self.dictMessageIDs objectForKey:message.uid]){
            [self.messages addObject:message];
            [self.dictMessageIDs setObject:@(1) forKey:message.uid];
        }
    }
    
    [self finishReceivingMessage];
    
    JSQMessage *newestMessage = [self.messages lastObject];
    self.latestTimestamp = [newestMessage.date timeIntervalSince1970];
}

- (void)updateMessagesWithNewOffers:(NSArray *)newOffers{
    
    BOOL bHaveNewMessage = NO;
    
    JSQMessage *lastMessage = [self.messages lastObject];
    NSString *lastDateString = [[HGUtils sharedInstance] friendlyDate:lastMessage.date];
    BOOL showTimestamp;

    for (HGOffer *offer in newOffers) {
        
        if([self shouldShowTimestamp:offer.timestamp]){
            NSString *dateString = [[HGUtils sharedInstance]friendlyDate:offer.timestamp];
            if([dateString isEqualToString:lastDateString]){
                showTimestamp = NO;
            }else{
                lastDateString = dateString;
                showTimestamp = YES;
            }
        }else{
            showTimestamp = NO;
        }
        
        JSQMessage *message = [JSQMessage messageWithOffer:offer receiver:self.toUser showTimestamp:showTimestamp];
        ///again, nil message ???
        if (message==nil) {
            continue;
        }
        if( ![self.dictMessageIDs objectForKey:message.uid] ){
            bHaveNewMessage = YES;
            [self.messages addObject:message];
            [self.dictMessageIDs setObject:@(1) forKey:message.uid];
        }
    }
    
    JSQMessage *newestMessage = [self.messages lastObject];
    self.latestTimestamp = [newestMessage.date timeIntervalSince1970];
    
    if(bHaveNewMessage){
        [self finishReceivingMessage];
    }
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{

    JSQMessage *newMessage = [JSQMessage messageWithSenderId:senderId displayName:senderDisplayName text:text];
    [self.messages addObject:newMessage];
    
    [self sendText:newMessage];
    
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chatplus" label:nil value:nil];
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:
                             NSLocalizedString(@"Take Photo", nil),
                            NSLocalizedString(@"Select from album", nil),                                                                NSLocalizedString(@"Select a meeting location", nil), nil];
    
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    sheet.tag = TagSheetAccessory;
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if(actionSheet.tag == TagSheetAccessory){
        
        switch (buttonIndex) {
            case 0:
            {
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_takephoto" label:nil value:nil];
                [self sendCameraPhotoMessage];
                break;
            }
            case 1:
            {
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_choosephoto" label:nil value:nil];
                [self sendAlbumPhotoMessage];
                break;
            }
            case 2:
            {
                
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_selectlocation" label:nil value:nil];
                UINavigationController *vcChat = (UINavigationController *)[[UIStoryboard messageStoryboard]instantiateViewControllerWithIdentifier:@"chatMapNavigation"];
                HGChatMapController* chatMap = [vcChat.viewControllers objectAtIndex:0];
                chatMap.delegate=self;
                CLLocationCoordinate2D theOtheUserCoodr = (CLLocationCoordinate2D){self.toUser.geoLocation.lat, self.toUser.geoLocation.lon};
                chatMap.theOtherUserCoord = theOtheUserCoodr;
                [self presentViewController:vcChat animated:YES completion:^(){}];
                break;
            }
            default:
            {
               
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chatplus_cancel" label:nil value:nil];
                break;
            }
        }
    }

}


#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        return [self.messages objectAtIndex:indexPath.item];
    }else{
        return self.emptyMessage;
    }
 
    
}


- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
        
        if ([message fromMe]) {
            return self.outgoingBubbleImage;
        }else{
            return self.incomingBubbleImage;
        }
    }else{
        return nil;
    }
    
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.section == 0){
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
        
        if ([message fromMe]){
            return self.outgoingAvatarImage;
        }else{
            return self.incomingAvatarImage;
        }
    }else{
        return nil;
    }
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(section == 0){
        return [self.messages count];
    }else{
        return 0;
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        
        HGChatSupplementaryReusableView *reusableSupplementary = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ChatResuableSupplementary forIndexPath:indexPath];
        
        [reusableSupplementary configText:[self generateSupplementaryText] style:self.styleDescription boldFirstSentence:self.bHaveDistance];
        return reusableSupplementary;
        
    }else{
        return [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    }
    
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
        
        cell.textView.delegate = self;
        
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
        [self configCell:cell withMessage:message];
        
        if (!message.isMediaMessage) {
            
            cell.textView.textColor = [UIColor blackColor];
            cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : FANCY_COLOR(@"0077ff"), NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
        }
        
        return cell;

    }else{
        return nil;
    }
   
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    
    if([[URL scheme] isEqualToString:@"tel"]){
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"click_phonenumber" label:nil value:nil];
    }else if([[URL scheme] isEqualToString:@"http"] || [[URL scheme] isEqualToString:@"https"]){
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"click_url" label:nil value:nil];
        ///we prefer to use local web view;
        HGWebContentController *vcWebContent = (HGWebContentController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCWebContent"];
        vcWebContent.contentLink = [URL absoluteString];
        [self.navigationController pushViewController:vcWebContent animated:YES];
        return NO;
    }
    
    return YES;
}



#pragma mark - UICollectionView Delegate



- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    if(action == @selector(copy:)){
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"copytest" label:nil value:nil];
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:@"Custom Action"
                                message:nil
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}



#pragma mark - JSQMessages collection view flow layout delegate

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if(section == 0){
        NSString *description = [self generateSupplementaryText];
        
        CGRect rectDescription = [description boundingRectWithSize:CGSizeMake(collectionViewLayout.itemWidth - LeadingConstraintDescription - TrailingConstraintDescription, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13], NSParagraphStyleAttributeName: self.styleDescription} context:nil];
        
        CGFloat height = rectDescription.size.height + TopConstraintDescription + BottomConstraintDescription + TopPaddingDescription + BottomPaddingDescription;
        
        return CGSizeMake(collectionViewLayout.itemWidth, height);

    }else{
        return [super collectionView:collectionView layout:collectionViewLayout referenceSizeForHeaderInSection:section];
    }
    
}


- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.section == 0){
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
        if(message.bShowTimestamp && [self shouldShowTimestamp:message.date] ){
            return 20;
        }else{
            return 0;
        }
    }else{
        return 0;
    }
    
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 20;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 20;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chatperson" label:nil value:nil];
        JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
        
        HGUserViewController *vcUser = (HGUserViewController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCUser"];
        
        if ([message fromMe]){
            vcUser.user = [[HGUserData sharedInstance] currentUser];
        }else{
            vcUser.user = self.toUser;
        }

        [self.navigationController pushViewController:vcUser animated:YES];
    }
}


- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(indexPath.section == 0){
        JSQMessage *tappedMessage = (JSQMessage *)[self.messages objectAtIndex:indexPath.item];
        if (![tappedMessage isMediaMessage]) {
            ///add User Translation buttion.
            JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
            
          
            CGRect Rect = [cell convertRect:cell.messageBubbleImageView.frame toView:self.collectionView];
            
            NSValue* rectValue = [NSValue valueWithCGRect:Rect];
            NSLog(@"the rect value is %@", rectValue);
            Rect.origin.y = Rect.origin.y - collectionView.contentOffset.y +20;
            
            self.currentTapMessageTextString = tappedMessage.text;
            [self.popupMenu showInView:self.collectionView targetRect:Rect animated:YES];
        }
        
        if([tappedMessage isMediaMessage]
           && !tappedMessage.bReceiveFailed
           && !tappedMessage.bReceiveInProcess
           && !tappedMessage.bSendFailed
           && !tappedMessage.bSendInProcess){
            
            if ([[tappedMessage media] isKindOfClass:[HGChatLocationMediaItem class]]) {
                
                
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"click_location" label:nil value:nil];
                HGChatMapWhereToMeetVC *vcChat = (HGChatMapWhereToMeetVC *)[[UIStoryboard messageStoryboard]instantiateViewControllerWithIdentifier:@"ChatMapWhereToMeet"];
                HGChatLocationMediaItem* meditItem = (HGChatLocationMediaItem*)tappedMessage.media;
                vcChat.POICoor = (CLLocationCoordinate2D){meditItem.latString.doubleValue, meditItem.lonString.doubleValue};
                vcChat.POITitleString =meditItem.poiTitle;
                vcChat.POIAddressString = meditItem.poiAddress;
                
                [self.navigationController pushViewController:vcChat animated:YES];
            
            }else if([[tappedMessage media] isKindOfClass:[HGChatPhotoMediaItem class]]){
                
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"click_photo" label:nil value:nil];
                NSMutableArray *arrayPhotos = [NSMutableArray array];
                NSString *sendID = tappedMessage.senderId;
                
                for(JSQMessage *message in self.messages){
                    if(!message.bReceiveInProcess
                       && !message.bReceiveFailed
                       && !message.bSendInProcess
                       && !message.bSendFailed
                       && [message isMediaMessage]
                       && [message.senderId isEqualToString:sendID]
                       && [[message media] isKindOfClass:[HGChatPhotoMediaItem class]]){
                        
                        HGChatPhotoMediaItem *photoMediaItem = (HGChatPhotoMediaItem *)message.media;
                        [arrayPhotos addObject:photoMediaItem.imageURL];
                    }
                }
                
                HGChatPhotoMediaItem *tappedPhotoMediaItem = (HGChatPhotoMediaItem *)tappedMessage.media;
                NSUInteger tappedMessageIndex = [arrayPhotos indexOfObject:tappedPhotoMediaItem.imageURL];
                
                HGChatImageViewerViewController *
                vcChatImageViewer = (HGChatImageViewerViewController *)[[UIStoryboard messageStoryboard] instantiateViewControllerWithIdentifier:@"VCChatImageViewer"];
                vcChatImageViewer.imagesArray = arrayPhotos;
                vcChatImageViewer.currentPageIndex = tappedMessageIndex;
                
                [self.navigationController pushViewController:vcChatImageViewer animated:YES];
                
            }else{
                
                
                NSLog(@"[Warn] Missed handler for new media type.!");
            }

        }
        
    }
   
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation{
    
    if(indexPath.section == 0){
        
        JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        self.currentTapMessage = [self.messages objectAtIndex:indexPath.item];
        
        UIImageView *iconFailed = (UIImageView *)[cell.contentView viewWithTag:TagIndicatorFailed];
        
        if(!iconFailed.isHidden
           && CGRectContainsPoint(iconFailed.frame, touchLocation)
           && self.currentTapMessage.bSendFailed
           ){
            
            [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Your message was not sent, tap to send again.", nil) withTitle:nil cancelTitle:NSLocalizedString(@"Cancel", nil) otherTitle:NSLocalizedString(@"Send again", nil)  completion:^(BOOL cancelled, NSInteger buttonIndex) {
                if (!cancelled) {
                    [self resendFailedMessage:self.currentTapMessage];
                }
            }];
            
        }else{
            if([self.inputToolbar.contentView.textView isFirstResponder]){
                [self.inputToolbar.contentView.textView resignFirstResponder];
            }
        }
    }
    
}



- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if([self.inputToolbar.contentView.textView isFirstResponder]){
        [self.inputToolbar.contentView.textView resignFirstResponder];
    }
}


#pragma mark - UIImagePickerControllerDelegate delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *image = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    HGChatPhotoMediaItem *photoMediaItem = [[HGChatPhotoMediaItem alloc] init];
    
    if(IPHONE4X){
        photoMediaItem.imageView.image = [[HGUtils sharedInstance] compressImage:image toSize:CGSizeMake(640,640) withCompressionQuality:0.0];
    }else{
        photoMediaItem.imageView.image = [[HGUtils sharedInstance] compressImage:image toSize:image.size withCompressionQuality:0.0];
    }
    
    photoMediaItem.appliesMediaViewMaskAsOutgoing = YES;
    
    JSQMessage *newMessage = [JSQMessage messageWithSenderId:[HGUserData sharedInstance].fmUserID displayName:[HGUserData sharedInstance].userDisplayName media:photoMediaItem];
    
    [self.messages addObject:newMessage];
    [self sendPhoto:newMessage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    
    if (error){
        NSLog(@"Save the image to album failed");
    }else{
        NSLog(@"Save the image to album successfully.");
    }
}


#pragma mark - CTAssetsPickerController Delegate

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset
{
    return picker.selectedAssets.count < MaxImagesCanBeSent;
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker isDefaultAssetsGroup:(ALAssetsGroup *)group
{
    // Set Camera Roll as default album and it will be shown initially.
    return ([[group valueForProperty:ALAssetsGroupPropertyType] integerValue] == ALAssetsGroupSavedPhotos);
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldShowAssetsGroup:(ALAssetsGroup *)group
{
    // Do not show empty albums
    return group.numberOfAssets > 0;
}

//if user pick from phone albums.
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"photo_album" label:nil value:nil];
    
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    for (ALAsset * asset in assets) {
        ALAssetRepresentation * assetRepresentation = asset.defaultRepresentation;
        
        UIImage *image = [UIImage imageWithCGImage:assetRepresentation.fullResolutionImage scale:1.0 orientation:(UIImageOrientation)assetRepresentation.orientation];
        
        HGChatPhotoMediaItem *photoMediaItem = [[HGChatPhotoMediaItem alloc] init];
        if(IPHONE4X){
            photoMediaItem.imageView.image = [[HGUtils sharedInstance] compressImage:image toSize:CGSizeMake(640,640) withCompressionQuality:0.0];
        }else{
            photoMediaItem.imageView.image = [[HGUtils sharedInstance] compressImage:image toSize:image.size withCompressionQuality:0.0];
        }
        photoMediaItem.appliesMediaViewMaskAsOutgoing = YES;
        
        JSQMessage *newMessage = [JSQMessage messageWithSenderId:[HGUserData sharedInstance].fmUserID displayName:[HGUserData sharedInstance].userDisplayName media:photoMediaItem];
        
        [self.messages addObject:newMessage];
        [self sendPhoto:newMessage];
    }
}


#pragma mark - helpers
- (NSString *)generateSupplementaryText{
    
    NSString *toUserDistance = [self.toUser formatDistance];
    NSString *toUserPlace = self.toUser.place;
    NSString *toUserName = self.toUser.displayName;
    
    NSMutableArray *linesOfString = [@[] mutableCopy];
    
    if(toUserName.length != 0 && (toUserDistance.length > 0 || toUserPlace.length > 0 )){
        
        self.bHaveDistance = YES;
        
        if( toUserDistance.length > 0 && toUserPlace.length > 0){
            [linesOfString addObject:[NSString stringWithFormat: NSLocalizedString(@"%1$@ is %2$@ away in %3$@.\n",nil), toUserName, toUserDistance, toUserPlace]];
        }else if( toUserDistance.length > 0 && toUserPlace.length == 0){
            [linesOfString addObject:[NSString stringWithFormat:NSLocalizedString(@"%1$@ is %2$@ away.\n", nil), toUserName, toUserDistance]];
        }else if( toUserDistance.length == 0 && toUserPlace.length > 0){
            [linesOfString addObject:[NSString stringWithFormat:NSLocalizedString(@"%1$@ is in %2$@.\n", nil), toUserName, toUserPlace]];
        }
    }
    
    if(self.bBuyer){
        [linesOfString addObject:NSLocalizedString(@"Ask the seller for more information here. You can share photos and select a good meeting location.\n",nil)];
        [linesOfString addObject:NSLocalizedString(@"Don't forget to rate the seller by tapping the Review button!", nil)];
    }else{
        [linesOfString addObject:NSLocalizedString(@"Reply to the buyer quickly to make a fast sale. You can share photos and select a good meeting location.\n",nil)];
        [linesOfString addObject:NSLocalizedString(@"Don't forget to rate the buyer by tapping the Review button!",nil)];
    }
    
    
    NSString* headingDesc = NSLocalizedString(@"Tips:\n", nil);
    
    for(NSInteger i=0; i<[linesOfString count]; i++){
        headingDesc = [headingDesc stringByAppendingFormat:@"%ld. %@", (long)i+1, [linesOfString objectAtIndex:i]];
    }
    
    return headingDesc;
}

- (void)configCell:(JSQMessagesCollectionViewCell *)cell withMessage:(JSQMessage *)message{
    
    int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.collectionView.collectionViewLayout.outgoingAvatarViewSize.width * 2];
    int suggestHeight = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.collectionView.collectionViewLayout.outgoingAvatarViewSize.height * 2];
    
    if([message fromMe]){
        NSString *outgoingAvatarLink = [[HGUtils sharedInstance] cloudinaryLink:[HGUserData sharedInstance].userAvatarLink width:suggestWidth height:suggestHeight];
        [cell configAvatar:[NSURL URLWithString:outgoingAvatarLink] verified:[HGUserData sharedInstance].bUserAvatarVerified];
        
    }else{
        NSString *incomingAvatarLink = [[HGUtils sharedInstance] cloudinaryLink:self.toUser.portraitLink width:suggestWidth height:suggestHeight];
        [cell configAvatar:[NSURL URLWithString:incomingAvatarLink] verified:[self.toUser.verified boolValue]];
    }
    
    NSString *timeString = [self shouldShowTimestamp:message.date] ? [[HGUtils sharedInstance] friendlyDate:message.date] : nil;
    
    if(message.bShowTimestamp){
        [cell configTimeText:timeString];
    }
    
    [cell configSendingIndicator:message];
    
    [cell configFailedIndicator:message];
}



- (BOOL)shouldShowTimestamp:(NSDate *)date{
    return [[NSDate date] timeIntervalSinceDate:date] > 5 * 60;
}

- (void)sendAlbumPhotoMessage{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        
        CTAssetsPickerController *vcPicker = [[CTAssetsPickerController alloc] init];
        vcPicker.assetsFilter = [ALAssetsFilter allPhotos];
        vcPicker.delegate = self;
        
        [self presentViewController:vcPicker animated:YES completion:nil];
    }
    
}


- (void)sendCameraPhotoMessage{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        UIImagePickerController *vcPicker = [[UIImagePickerController alloc] init];
        vcPicker.delegate = self;
        vcPicker.allowsEditing = NO;
        vcPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:vcPicker animated:YES completion:nil];
    }
    
}
-(void)chatMapContrller:(HGChatMapController *)maper startSendingLocation:(UIImage *)locationImage contentDictionary:(NSDictionary *)dict
{
    HGChatLocationMediaItem *locationMediaItem = [[HGChatLocationMediaItem alloc] init];
    locationMediaItem.appliesMediaViewMaskAsOutgoing = YES;
    NSString* addressName =[dict objectForKey:@"address_name"];
    NSString* placeName = [dict objectForKey:@"place_name"];
    NSNumber* latNumber= [dict objectForKey:@"lat"];
    NSNumber* lonNumber = [dict objectForKey:@"lon"];
    locationMediaItem.poiTitle = placeName;
    locationMediaItem.poiAddress = addressName;
    locationMediaItem.latString = latNumber.stringValue;
    locationMediaItem.lonString = lonNumber.stringValue;
    locationMediaItem.imageView.image = locationImage;
    
    locationMediaItem.titleLabel.text = placeName;
    locationMediaItem.addressLabel.text = addressName;
    
    JSQMessage *newMessage = [JSQMessage messageWithSenderId:[HGUserData sharedInstance].fmUserID displayName:[HGUserData sharedInstance].userDisplayName media:locationMediaItem];
    [self.messages addObject:newMessage];
    [self sendLocation:newMessage];
    
    [maper dismissViewControllerAnimated:YES completion:nil];
   
}

- (void)resendFailedMessage:(JSQMessage *)failedMessage{
    
    if(failedMessage.isMediaMessage){
        
        if([failedMessage.media isKindOfClass:[HGChatPhotoMediaItem class]]){
            [self sendPhoto:failedMessage];
        }else if([failedMessage.media isKindOfClass:[HGChatLocationMediaItem class]]){
            [self sendLocation:failedMessage];
        }else{
            NSLog(@"[Warn] Missed resend handler for new media type.");
        }
        
    }else{
        [self sendText:failedMessage];
    }
}


////Chat image :  MAX 300K.
static int DEFAULT_SYSTEM_SUGGEST_MAX_IMAGE_SIZE = 300 * 1024;
- (void)sendPhoto:(JSQMessage *)message{
    
    if (self.item.uid == nil) {
        //error denfense;
        return;
    }
    
    HGChatPhotoMediaItem *photoMediaItem = (HGChatPhotoMediaItem *)message.media;
    
    NSData *imageData = UIImageJPEGRepresentation(photoMediaItem.imageView.image, 0.8);
    
    if (imageData.length > DEFAULT_SYSTEM_SUGGEST_MAX_IMAGE_SIZE) {
        NSLog(@"the picture size is %.2fk", (int)imageData.length/1024.0);
        float fSizeChangeRate =  DEFAULT_SYSTEM_SUGGEST_MAX_IMAGE_SIZE /(imageData.length + 0.1);
        UIImage* smallImage = [photoMediaItem.imageView.image resizeImage:photoMediaItem.imageView.image withTargetSize:CGSizeMake(floorf(photoMediaItem.imageView.image.size.width * fSizeChangeRate), floorf(photoMediaItem.imageView.image.size.height * fSizeChangeRate))];
        imageData = UIImageJPEGRepresentation(smallImage, 0.8);
    }

    message.bSendInProcess = YES;
    message.bSendFailed = NO;
    [photoMediaItem addLoadingMask];
    [self finishSendingMessage];
    
    CLUploader *uploader = [[CLUploader alloc] init:[HGUtils sharedInstance].cloudinary  delegate:nil];
    [uploader upload:imageData options:@{} withCompletion:^(NSDictionary *successResult, NSString *errorResult, NSInteger code, id context) {
        
        if (successResult) {
            
            NSLog(@"Block upload success.");
            if (self.rf_tag !=nil && self.rf_tag.length > 0) {
                
            }
            else
            {
                self.rf_tag = @"";
            }
            
            photoMediaItem.imageURL = [NSURL URLWithString:[successResult objectForKey:@"url"]];
            [photoMediaItem removeLoadingMask];
            [self finishSendingMessage];
            
            
            [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"make_offer/" parameters:@{@"msg_type":@(HGOfferMessageTypePhoto), @"picture_url": [successResult objectForKey:@"url"], @"item_id": self.item.uid, @"to_user": self.toUser.uid, @"rf_tag":self.rf_tag} success:^(NSURLSessionDataTask *task, id responseObject) {
                
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_sendphoto" label:nil value:nil];
                
                self.offerLineChanged = YES;
                self.offerLineID = [responseObject objectForKey:@"offerline_id"];
                NSString *newOfferID = [responseObject objectForKey:@"offer_id"];
                
                message.uid = newOfferID;
                
                if(![self.dictMessageIDs objectForKey:message.uid]){
                    [self.dictMessageIDs setObject:@(1) forKey:message.uid];
                }
                
                self.latestTimestamp = [message.date timeIntervalSince1970];
                
                message.bSendInProcess = NO;
                message.bSendFailed = NO;
               
                
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                NSLog(@"make chat failed:%@", error.userInfo);
                message.bSendInProcess = NO;
                message.bSendFailed = YES;
               
                //[self finishSendingMessage];
                
            }];
            
        } else {
            NSLog(@"Block upload error: %@, %ld", errorResult, (long)code);
            
            message.bSendInProcess = NO;
            message.bSendFailed = YES;
            [photoMediaItem removeLoadingMask];
            [self finishSendingMessage];
        }
        
    } andProgress:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite, id context) {
        
        NSLog(@"Block upload progress: %.2fk/%.2fk (+%.2f)k", (long)totalBytesWritten/1024.0, (long)totalBytesExpectedToWrite/1024.0, (long)bytesWritten/1024.0);
        
        CGFloat progress = (CGFloat)totalBytesWritten/totalBytesExpectedToWrite;
        
        
        if(progress > 0.99){
            progress = 0.99;
        }
        
        [photoMediaItem updateProgress:progress];
        
    }];

}

- (void)sendLocation:(JSQMessage *)message{
    if(self.item.uid==nil)
    {
        //crash defense.
        return;
    }
    HGChatLocationMediaItem *locationMediaItem = (HGChatLocationMediaItem *)message.media;
    NSData *imageData = UIImageJPEGRepresentation(locationMediaItem.imageView.image, 0.5);
    
    message.bSendInProcess = YES;
    message.bSendFailed = NO;
    [locationMediaItem addLoadingMask];
    [self finishSendingMessage];
    
    CLUploader *uploader = [[CLUploader alloc] init:[HGUtils sharedInstance].cloudinary delegate:nil];
    
    [uploader upload:imageData options:@{} withCompletion:^(NSDictionary *successResult, NSString *errorResult, NSInteger code, id context) {
        if (successResult) {
            
            NSLog(@"Block upload success.");
            if (self.rf_tag !=nil && self.rf_tag.length > 0) {
                
            }
            else
            {
                self.rf_tag = @"";
            }
            
            
            [locationMediaItem removeLoadingMask];
            [self finishSendingMessage];
            
            
            [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"make_offer/" parameters:@{@"msg_type":@(HGOfferMessageTypeLocation), @"address_map_thumb": [successResult objectForKey:@"url"], @"address_name":locationMediaItem.poiAddress,@"place_name":locationMediaItem.poiTitle,
                @"lat":locationMediaItem.latString,@"lon":locationMediaItem.lonString, @"item_id": self.item.uid, @"to_user": self.toUser.uid, @"rf_tag":self.rf_tag} success:^(NSURLSessionDataTask *task, id responseObject) {
                

                [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_sendlocation" label:nil value:nil];
                
                self.offerLineChanged = YES;
                self.offerLineID = [responseObject objectForKey:@"offerline_id"];
                NSString *newOfferID = [responseObject objectForKey:@"offer_id"];
                
                message.uid = newOfferID;
                
                if(![self.dictMessageIDs objectForKey:message.uid]){
                    [self.dictMessageIDs setObject:@(1) forKey:message.uid];
                }
                
                self.latestTimestamp = [message.date timeIntervalSince1970];
                
                message.bSendInProcess = NO;
                message.bSendFailed = NO;
                
                
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSLog(@"make chat failed:%@", error.userInfo);
                
                message.bSendInProcess = NO;
                message.bSendFailed = YES;
                
                //[self finishSendingMessage];
                
            }];

        } else {
            NSLog(@"Block upload error: %@, %ld", errorResult, (long)code);
            
            message.bSendInProcess = NO;
            message.bSendFailed = YES;
            [locationMediaItem removeLoadingMask];
            [self finishSendingMessage];
        }

    } andProgress:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite, id context) {
        
        NSLog(@"Block upload progress: %ld/%ld (+%ld)", (long)totalBytesWritten, (long)totalBytesExpectedToWrite, (long)bytesWritten); CGFloat progress = (CGFloat)totalBytesWritten/totalBytesExpectedToWrite;
        
        if(progress > 0.99){
            progress = 0.99;
        }
        
        [locationMediaItem updateProgress:progress];
        
    }];

}

- (void)sendText:(JSQMessage *)message{
    
    if (self.item.uid == nil) {
        //crash defense.
        return;
    }
    
    message.bSendInProcess = YES;
    message.bSendFailed = NO;
    [self finishSendingMessage];
    
    if (self.rf_tag !=nil && self.rf_tag.length > 0) {
        
    }
    else
    {
        self.rf_tag = @"";
    }
    
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"make_offer/" parameters:@{@"msg_type":@(HGOfferMessageTypeText), @"text": message.text, @"item_id":self.item.uid, @"to_user": self.toUser.uid, @"rf_tag":self.rf_tag} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"chat_view" action:@"chat_sendlocation" label:nil value:nil];
        
        self.offerLineChanged = YES;
        self.offerLineID = [responseObject objectForKey:@"offerline_id"];
        NSString *newOfferID = [responseObject objectForKey:@"offer_id"];
        
        message.uid = newOfferID;
        message.bSendInProcess = NO;
        message.bSendFailed = NO;
        
        if(![self.dictMessageIDs objectForKey:message.uid]){
            [self.dictMessageIDs setObject:@(1) forKey:message.uid];
        }
        self.latestTimestamp = [message.date timeIntervalSince1970];
        
        [self finishSendingMessage];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"make chat failed:%@", error.userInfo);
        
        message.bSendInProcess = NO;
        message.bSendFailed = YES;
        
        [self finishSendingMessage];
        
    }];

}

@end

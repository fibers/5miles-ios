//
//  HGEditProfileController.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-6.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGEditProfileController.h"
#import "HGAppData.h"
#import "HGUtils.h"
#import "HGAppDelegate.h"
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SVProgressHUD.h>
#import "UIImage+Scale.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGProfilePhoneViewController.h"
#import "HGProfileSelfIntroductionViewController.h"
#import "BDKNotifyHUD.h"
#import "HGFBUtils.h"
#import "HGChangeNickNameVC.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGZipCodeVC.h"
#import "HGEditEmailVC.h"
#import "FBlikeReminderViewController.h"

@interface HGEditProfileController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLUploaderDelegate, FBLikerReminderCloseDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UITextField *tfName;
@property (weak, nonatomic) IBOutlet UITextField *lbEmail;



@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonSave;

@property (weak, nonatomic) IBOutlet UILabel *phoneVerifiedLabel;



@property (nonatomic, strong) NSString* name_recorder;
@property (nonatomic, strong) NSString * avatarLink;
@property (nonatomic, strong) RACSubject * subAvatarUpload;
@property (nonatomic, strong) NSMutableArray * tasks;
@property (nonatomic, strong) CLUploader* uploader;


@property (nonatomic, strong) UIImageView* avatarCover;
@property (weak, nonatomic) IBOutlet UIImageView *avatarVerifyMark;
//@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;
@end

@implementation HGEditProfileController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}
-(void)setCellAccessIcon:(UITableViewCell*)cell
{
    UIImageView*accView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 17/2, 30/2)];
    accView.image = [UIImage imageNamed:@"uitableview_cell_access"];
    cell.accessoryView = accView;
}
-(void)addCellAccessIcon
{
    [self setCellAccessIcon:self.photocell];
    [self setCellAccessIcon:self.namecell];
    [self setCellAccessIcon:self.emailcell];
    [self setCellAccessIcon:self.ZipCodeCell];
    
    //[self setCellAccessIcon:self.phoneCell];

}
-(void)addAvatarMark
{
//    self.avatarCover = [[UIImageView alloc] initWithFrame:self.avatarView.frame];
//    self.avatarCover.image = [UIImage imageNamed:@"avatar-cover"];
//    self.avatarCover.center = self.avatarView.center;
//    [self.photocell addSubview:self.avatarCover];
    
//    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.avatarView.frame.size.width/3, self.avatarView.frame.size.width/3)];
    if ([HGUserData sharedInstance].bUserAvatarVerified) {
        self.avatarVerifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else
    {
        self.avatarVerifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
//    CGPoint centerPoint = self.avatarView.center;
//    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
//    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
//    self.userAvatar_verifyMark.center = centerPoint;
//    [self.photocell addSubview:self.userAvatar_verifyMark];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addCellAccessIcon];
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    self.tasks = [@[] mutableCopy];
    self.avatarLink = [HGUserData sharedInstance].userAvatarLink;
    
    self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    
    int sugggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatarView.frame.size.width * 2];
     NSString* image_url = [[HGUtils sharedInstance] cloudinaryLink:self.avatarLink width:sugggestWidth height:sugggestWidth];
    
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    self.avatarView.layer.cornerRadius = self.avatarView.frame.size.width/2;
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.clipsToBounds = YES;
    
 
    
   
    [self addAvatarMark];

    
    
     self.ZipCodeLabel.textColor = self.zipCodeContent.textColor = self.photolabel.textColor = self.namelabel.textColor = self.emaillabel.textColor = self.phonelabel.textColor = self.facebooklabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
     self.ZipCodeLabel.font = self.zipCodeContent.font = self.photolabel.font = self.namelabel.font = self.emaillabel.font = self.phonelabel.font = self.facebooklabel.font = [UIFont systemFontOfSize:15];

    self.phoneVerifiedLabel.font = [UIFont systemFontOfSize:15];
    
    self.lbEmail.textColor = self.tfName.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.lbEmail.font = self.tfName.font = [UIFont systemFontOfSize:15];

    [self customizeBackButton];
    
    [self setRACAction];

    [self.linkFBaccountSwitch setOnTintColor:FANCY_COLOR(@"ff8830")];
     self.linkFBaccountSwitch.frame = CGRectMake(self.view.frame.size.width - 15 - self.linkFBaccountSwitch.frame.size.width, self.linkFBaccountSwitch.frame.origin.y, self.linkFBaccountSwitch.frame.size.width, self.linkFBaccountSwitch.frame.size.height);

}

- (void)setRACAction{
    RACChannelTerminal *racCTUserFBVerifiedSwitch = self.linkFBaccountSwitch.rac_newOnChannel;
    RACChannelTerminal *racCTUserFBVerifiedValue = RACChannelTo([HGUserData sharedInstance], bUserFBVerified, @(NO));
    [racCTUserFBVerifiedValue subscribe:racCTUserFBVerifiedSwitch];
    [[racCTUserFBVerifiedSwitch takeUntil:self.rac_willDeallocSignal] subscribe:racCTUserFBVerifiedValue];
}

-(void)customizeBackButton
{
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    self.navigationItem.backBarButtonItem = backbutton;
}
-(void)viewDidLayoutSubviews{
    
    
    self.linkFBaccountSwitch.frame = CGRectMake(self.view.frame.size.width - 15 - self.linkFBaccountSwitch.frame.size.width, self.linkFBaccountSwitch.frame.origin.y, self.linkFBaccountSwitch.frame.size.width, self.linkFBaccountSwitch.frame.size.height);
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([HGUserData sharedInstance].bUserPhoneVerified)
    {
        self.phoneVerifiedLabel.text = NSLocalizedString(@"Verified",nil);
        if ([HGAppData sharedInstance].userVerifiedPhonenumberString != nil && [HGAppData sharedInstance].userVerifiedPhonenumberString.length > 0) {
            self.phoneVerifiedLabel.text = [HGAppData sharedInstance].userVerifiedPhonenumberString;
        }
    }
    else{
        [self setCellAccessIcon:self.phoneCell];
        self.phoneVerifiedLabel.text = NSLocalizedString(@"Not Verified",nil);
    }
    
    self.tfName.text = [HGUserData sharedInstance].userDisplayName;
    self.tfName.enabled = NO;
    
    self.lbEmail.text = [HGUserData sharedInstance].userEmail;
    self.lbEmail.enabled = NO;
    self.zipCodeContent.text = [HGUserData sharedInstance].zipcodeString;
    
    
    
    NSString* zipAddressString = @"";
    if ([HGUserData sharedInstance].zipCity.length > 0) {
        zipAddressString = [zipAddressString stringByAppendingFormat:@"%@",[HGUserData sharedInstance].zipCity ];
    }
    if ([HGUserData sharedInstance].zipRegion.length > 0) {
        zipAddressString = [zipAddressString stringByAppendingFormat:@",%@",[HGUserData sharedInstance].zipRegion];
    }
    
    self.zipCodeContent.text = zipAddressString;
    //////if zip address is nil, using GPS address.
    if(zipAddressString.length == 0)
    {
        if ([HGAppData sharedInstance].cityString.length > 0 ) {
            zipAddressString = [zipAddressString stringByAppendingFormat:@"%@",[HGAppData sharedInstance].cityString];
        }
        if ([HGAppData sharedInstance].regionString.length > 0) {
            zipAddressString = [zipAddressString stringByAppendingFormat:@",%@", [HGAppData sharedInstance].regionString];
        }
        self.zipCodeContent.text = zipAddressString;
    }
    
    
    
    self.zipCodeContent.enabled = NO;
    
    [[HGUtils sharedInstance] gaTrackViewName:@"editProfile_view"];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    
    
    if (self.uploader != nil) {
        [self.uploader cancel];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1 ) {
        return 30;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 1)
    {
        UIView* tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
        tableHeaderView.backgroundColor = [UIColor clearColor];
        UILabel * sectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 3, 300, 20)];
        [tableHeaderView addSubview:sectionTitle];
        sectionTitle.textAlignment = NSTextAlignmentLeft;
        sectionTitle.text = NSLocalizedString(@"Verify your identify", nil);
        sectionTitle.font = [UIFont fontWithName: FONT_TYPE_2
                                            size:14];
        sectionTitle.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        return tableHeaderView;
        
    }
    return  nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1 ) {
        return 120;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1 && indexPath.row == 1) {
        if ([HGUserData sharedInstance].accountType == HGAccountTypeFacebook) {
            self.facebookcell.hidden = YES;
            return 0;
        }
    }
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section == 1)
    {
        UIView* tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 120)];
        tableHeaderView.backgroundColor = [UIColor clearColor];
        UILabel * sectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(2, 5, self.view.frame.size.width-20, 120)];
        [tableHeaderView addSubview:sectionTitle];
        CGPoint centerPoint =  tableHeaderView.center;
        centerPoint.x = centerPoint.x + 5;
        sectionTitle.center = centerPoint;
        sectionTitle.textAlignment = NSTextAlignmentLeft;
        sectionTitle.text = NSLocalizedString(@"Become a verified user above - this shows other 5milers you're the real deal! Once verified, you'll receive a special avatar.\n5miles will only use your phone number or Facebook details to verify your identity, and we will not share them with anyone else.",nil);
        sectionTitle.font = [UIFont systemFontOfSize:13];
        sectionTitle.textColor = FANCY_COLOR(@"787878");
        sectionTitle.numberOfLines = 0;
        
        return tableHeaderView;
        
    }
    return  nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        [self.tfName resignFirstResponder];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"editProfile_view" action:@"changephoto" label:nil value:nil];
        UIActionSheet * actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Please use your real photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take a photo", nil), NSLocalizedString(@"Select from album", nil), nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        
        [actionSheet showInView:self.view];
    }

    if (indexPath.section == 0 && indexPath.row == 1) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        HGChangeNickNameVC * viewController = (HGChangeNickNameVC*)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"ChangeNickName"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if (indexPath.section == 0 && indexPath.row==2) {
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"editProfile_view" action:@"changeemail" label:nil value:nil];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        HGEditEmailVC * viewController = (HGEditEmailVC*)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"editEmail"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if (indexPath.section == 0 && indexPath.row == 3) {
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"editProfile_view" action:@"changezipcode" label:nil value:nil];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        HGZipCodeVC * viewController = (HGZipCodeVC*)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"ZipCode"];
        
        viewController.currentViewType = zipViewTypeEditProfile;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    ////////////////////////////////////////////////////
    
    
    if (indexPath.section ==1 && indexPath.row == 0)
    {
        if ([HGUserData sharedInstance].bUserPhoneVerified) {
            return;
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"editProfile_view" action:@"verifyphone" label:nil value:nil];
            
            HGProfilePhoneViewController *viewcontroller = [[HGProfilePhoneViewController alloc] initWithNibName:@"HGProfilePhoneViewController" bundle:nil];
            
            [self.navigationController pushViewController:viewcontroller animated:YES];
        }
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        /*HGProfileSelfIntroductionViewController *viewcontroller = [[HGProfileSelfIntroductionViewController alloc] initWithNibName:@"HGProfileSelfIntroductionViewController" bundle:nil];
        
        [self.navigationController pushViewController:viewcontroller animated:YES];
         */
    }

}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            UIImagePickerController * controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            controller.delegate = self;
            controller.allowsEditing = YES;
            
            [self.navigationController presentViewController:controller animated:YES completion:nil];
            
            break;
        }
        case 1:
        {
            UIImagePickerController * controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            controller.delegate = self;
            controller.allowsEditing = YES;
            
            [self.navigationController presentViewController:controller animated:YES completion:nil];
            
            break;
        }
        default:
            break;
    }
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString * mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(__bridge NSString *)kUTTypeImage]) {
        //NSDictionary * metaData = [info objectForKey:UIImagePickerControllerMediaMetadata];
        //NSLog(@"image meta: %@", metaData);
        
        UIImage * headImage = [info objectForKey:UIImagePickerControllerEditedImage];
        headImage = [[HGUtils sharedInstance] compressImage:headImage toSize:CGSizeMake(headImage.size.width, headImage.size.height) withCompressionQuality:0.5];

        [self.avatarView setImage:headImage];
        
        self.subAvatarUpload = [RACSubject subject];
        [self.tasks addObject:self.subAvatarUpload];
        
        [SVProgressHUD show];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"sign_image_upload/" parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
            NSDictionary * signedRequest = responseObject;
            self.uploader = [[HGUtils sharedInstance] uploadPhoto:headImage withSignedOptions:signedRequest delegate:self];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"sign image upload failed: %@", error);
            [self.subAvatarUpload sendError:error];
        }];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Cloudinary Upload Delegate

- (void)uploaderSuccess:(NSDictionary *)result context:(id)context
{
    [SVProgressHUD dismiss];
    self.uploader = nil;
    self.avatarLink = [result objectForKey:@"url"];
    [self.tasks removeObject:self.subAvatarUpload];
    [self.subAvatarUpload sendCompleted];
    [self updateAvatarLink];
    NSLog(@"upload image ok with link: %@", self.avatarLink);
}

- (void)uploaderError:(NSString *)result code:(NSInteger)code context:(id)context
{
    [SVProgressHUD dismiss];
    self.uploader = nil;
    [self.tasks removeObject:self.subAvatarUpload];
    [self.subAvatarUpload sendError:[NSError errorWithDomain:@"ProfileImageUpload" code:code userInfo:@{@"result": result}]];
    NSLog(@"upload image error: %@", result);
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

#pragma mark - UI Control Handlers

- (IBAction)onNameChanged:(id)sender {
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"editProfile_view" action:@"changename" label:nil value:nil];
    self.barButtonSave.enabled = self.tfName.text.length > 0;
}


-(void)updateAvatarLink
{
    
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"fill_profile/" parameters:@{@"portrait":self.avatarLink} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [SVProgressHUD dismiss];
        
        [HGUserData sharedInstance].userAvatarLink = [responseObject objectForKey:@"portrait"];
        
        //[self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        ///temply keep
        PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:error.localizedDescription];
        [view useDefaultIOS7Style];
    }];
}

- (IBAction)onTouchButtonSave:(id)sender {
   
    
    
 }


- (IBAction)linkFBSwitchTouch:(id)sender {
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    
    [switchButton setOn:NO];
    
    if (isButtonOn) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"editProfile_view" action:@"verifyfacebook" label:nil value:nil];
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
        
        [[HGFBUtils sharedInstance] verifyAccountWithSuccess:^(NSDictionary *reponse) {
            [SVProgressHUD dismiss];
            [switchButton setOn:YES];
            if ([HGUserData sharedInstance].bHomeShowFBLike)
            {
                [self addFBlikerViewWithTitle:NSLocalizedString(@"Link Facebook account successfully", nil)];
            } else {
                [[HGUtils sharedInstance] showSimpleNotify:@"Link facebook account successfully." onView:self.view];
            }
        } withFailure:^(NSError *error) {
            [SVProgressHUD dismiss];
            [switchButton setOn:NO];
            [[HGUtils sharedInstance] showSimpleNotify:@"There was an error linking your Facebook account. Please try again later." onView:self.view];
            
        }];
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"editProfile_view" action:@"closelinkfacebook" label:nil value:nil];
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
        
        [[HGFBUtils sharedInstance] unverifyAccountWithSuccess:^(NSDictionary *reponse) {
            [SVProgressHUD dismiss];
            [[HGUtils sharedInstance] showSimpleNotify:@"UnLink facebook account successfully." onView:self.view];
        } withFailure:^(NSError *error) {
            [SVProgressHUD dismiss];
            [[HGUtils sharedInstance] showSimpleNotify:@"UnLink facebook account unsuccessfully." onView:self.view];
        }];
    }
    
}


-(void)addFBlikerViewWithTitle:(NSString *)title
{
//    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"likefacebookpage_open" label:nil value:nil];
    FBlikeReminderViewController *fblikerview = [[FBlikeReminderViewController alloc] initWithNibName:@"FBlikeReminderViewController" bundle:nil];
    fblikerview.delegate = self;
    fblikerview.alertTitle = title;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        //ios 8
        fblikerview.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    else{
        //
        HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
        
        application.tabController .modalPresentationStyle = UIModalPresentationCurrentContext;
        self.modalPresentationStyle = UIModalPresentationFormSheet;
        
    }
    
    HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [application.tabController presentViewController:fblikerview animated:NO completion:nil];
}

- (void)touchedCloseButtonWithFblikeReminder:(FBlikeReminderViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchedLikeButtonWithFblikeReminder:(FBlikeReminderViewController *)viewController
{
    [HGUserData sharedInstance].bHomeShowFBLike = NO;
}

@end

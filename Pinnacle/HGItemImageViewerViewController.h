//
//  HGItemImageViewerViewController.h
//  Pinnacle
//
//  Created by shengyuhong on 15/6/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "X4ImageViewer.h"

@interface HGItemImageViewerViewController : UIViewController<X4ImageViewerDelegate>

@property (nonatomic, assign) NSInteger currentPageIndex;
@property (nonatomic, strong) NSArray *imagesArray;


@end
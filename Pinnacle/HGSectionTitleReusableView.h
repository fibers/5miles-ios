//
//  HGSectionTitleReusableView.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-21.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGSectionTitleReusableView : UICollectionReusableView

@property (nonatomic, strong) UILabel * titleLabel;

@end

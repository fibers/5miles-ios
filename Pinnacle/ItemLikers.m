//
//  ItemLikers.m
//  Pinnacle
//
//  Created by Alex on 14-11-23.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "ItemLikers.h"

@implementation ItemLikers
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid", @"nickname":@"nickname", @"portrait":@"portrait",@"verified":@"verified"}];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[@"uid", @"nickname", @"portrait",@"verified",@"following"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }

    return NO;
}



@end

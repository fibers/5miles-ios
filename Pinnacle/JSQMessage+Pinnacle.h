//
//  JSQMessage+Pinnacle.h
//  Pinnacle
//
//  Created by shengyuhong on 15/5/20.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSQMessage.h"

@class HGOffer, HGUser;

@interface JSQMessage (Pinnacle)

@property (nonatomic, assign) BOOL bShowTimestamp;
@property (nonatomic, assign) BOOL bSendFailed;
@property (nonatomic, assign) BOOL bSendInProcess;
@property (nonatomic, assign) BOOL bReceiveFailed;
@property (nonatomic, assign) BOOL bReceiveInProcess;
@property (nonatomic, strong) NSString *uid;

@property (nonatomic, strong) NSString* translatedString;

+ (instancetype)messageWithOffer:(HGOffer *)offer receiver:(HGUser *)receiver showTimestamp:(BOOL)showTimestamp;

- (BOOL)fromMe;


@end

//
//  HGGoodSellerViewController.h
//  Pinnacle
//
//  Created by Alex on 15-3-31.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGGoodSellerCell.h"
@interface HGGoodSellerViewController : UITableViewController<HGGoodSellerCellDelegate>

@property (nonatomic, strong)NSMutableArray* dataArray;

@property (nonatomic, assign)int leftNavigationType;

@end

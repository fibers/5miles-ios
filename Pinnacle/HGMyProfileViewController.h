//
//  HGMyProfileViewController.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGUserVerifyHintView.h"

//@class HGMyProfile;

@interface HGMyProfileViewController : UITableViewController<UITableViewDelegate, UINavigationControllerDelegate, HGUserVerifyHintDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *lbNumItems;
@property (weak, nonatomic) IBOutlet UILabel *lbNumFollowers;
@property (weak, nonatomic) IBOutlet UILabel *lbNumFollowing;
@property (weak, nonatomic) IBOutlet UILabel *itemUnit;
@property (weak, nonatomic) IBOutlet UILabel *followerUnit;
@property (weak, nonatomic) IBOutlet UILabel *followingUnit;

@property (nonatomic, strong)UIView*leftDivider;
@property (nonatomic, strong)UIView*rightDivider;




@property (weak, nonatomic) IBOutlet UITableViewCell *likesCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *purchase_bury_cell;

@property (weak, nonatomic) IBOutlet UITableViewCell *feedback_cell;

@property (weak, nonatomic) IBOutlet UITableViewCell *inviteFriend_cell;
@property (weak, nonatomic) IBOutlet UITableViewCell *shareMyShop_cell;

@property (weak, nonatomic) IBOutlet UITableViewCell *setting_cell;


@property (weak, nonatomic) IBOutlet UITableViewCell *helpCenter_cell;

@property (strong, nonatomic) UIImageView *feedback_score;

@property (nonatomic, strong) NSString* lastAvatarUrlString;
@property (nonatomic, strong) UIImageView* needUpdate_mark;


@end

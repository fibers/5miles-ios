//
//  HGItemDetailLayout.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-20.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const HGCollectionElementKindGlobalHeader;
extern NSString *const HGCollectionElementKindGlobalFooter;
@protocol HGItemDetailLayoutDelegate <NSObject>

- (CGFloat)heightForHeaderAboveCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout;

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section;

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section;

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section;

@optional

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section;

@end

@interface HGItemDetailLayout : UICollectionViewLayout

@property (nonatomic, assign) id <HGItemDetailLayoutDelegate> delegate;

@property (nonatomic, assign) NSInteger columnCount;
@property (nonatomic, assign) CGFloat minimumColumnSpacing;
@property (nonatomic, assign) CGFloat minimumInteritemSpacing;
@property (nonatomic, assign) CGFloat headerHeight;
@property (nonatomic, assign) CGFloat FooterHeigth;

@property (nonatomic, assign) UIEdgeInsets sectionInset;

@end

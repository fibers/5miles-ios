//
//  HGTabBarViewController.m
//  Pinnacle
//
//  Created by Alex on 14-10-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGTabBarViewController.h"
#import "HGItemDetailController.h"
#import "HGChatViewController.h"
#import "HGMessage.h"
#import "HGHomeViewController.h"
#import "HGMessageCenterController.h"
#import "UIStoryboard+Pinnacle.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface HGTabBarViewController ()

@property (nonatomic, strong) UIImageView *ivTopImage;
@property (nonatomic, strong) UIImageView *ivBottomLogo;

@end

@implementation HGTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    [self.tabBar setHidden:YES];
    
    [self initHGTabbarContent];
    
    // Separate the main storyboard
    NSMutableArray *mutablViewControllers = [self.viewControllers mutableCopy];
    
    UINavigationController *navSearch = (UINavigationController *)[[UIStoryboard searchStoryboard] instantiateViewControllerWithIdentifier:@"NavSearch"];
    [mutablViewControllers addObject:navSearch];
    
    UINavigationController *navUpload = (UINavigationController *)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"NavUpload"];
    [mutablViewControllers addObject:navUpload];
    
    UINavigationController *navMessage = (UINavigationController *)[[UIStoryboard messageStoryboard] instantiateViewControllerWithIdentifier:@"NavMessage"];
    [mutablViewControllers addObject:navMessage];
    
    UINavigationController *navProfile = (UINavigationController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"NavProfile"];
    [mutablViewControllers addObject:navProfile];
    
    self.viewControllers = [mutablViewControllers copy];
    
    if([HGAppData sharedInstance].bShowStartAD){
        [HGAppData sharedInstance].bShowStartAD = NO;
        [self addStartADView];
    }
    
    HGAppDelegate *delegate = (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.tabController = self;

}


- (void)addStartADView{
    
    float topImageHeightRate = 464/568.0;
    float bottomImageHeightRate = 1 - topImageHeightRate;
    
    self.ivTopImage = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, self.view.bounds.size.width, ceil(topImageHeightRate* self.view.bounds.size.height)+1)];
    [self.view addSubview:self.ivTopImage];
    
    self.ivBottomLogo = [[UIImageView alloc] initWithFrame:CGRectMake(0, ceil(self.view.bounds.size.height - bottomImageHeightRate*self.view.bounds.size.height) , self.view.bounds.size.width, ceil(bottomImageHeightRate*self.view.bounds.size.height))];
    self.ivBottomLogo.image =[UIImage imageNamed:@"default_system_ad_bottom"];
    [self.view addSubview:self.ivBottomLogo];
    
    if ([HGAppData sharedInstance].startADCampaign !=nil) {
        if (self.view.frame.size.height>480) {
            
             [self.ivTopImage sd_setImageWithURL:[NSURL URLWithString:[HGAppData sharedInstance].startADCampaign.image_url] placeholderImage: [UIImage imageWithColor:FANCY_COLOR(@"f0f0f0")]];
        }
        else{
            //iphone4
             self.ivTopImage.frame = CGRectMake(0, 0, self.view.bounds.size.width, 464-88);
            self.ivBottomLogo.frame = CGRectMake(0, self.view.bounds.size.height - 105, self.view.bounds.size.width, 105);
            [self.ivTopImage sd_setImageWithURL:[NSURL URLWithString:[HGAppData sharedInstance].startADCampaign.image_url] placeholderImage:[UIImage imageWithColor:FANCY_COLOR(@"f0f0f0")]];
        }
    }
    else{
        if (self.view.frame.size.height>480) {
            self.ivTopImage.image  = [UIImage imageNamed:@"system_defaultAD"];
        }
        else{
            ///iphone4
             self.ivTopImage.frame = CGRectMake(0, 0, self.view.bounds.size.width, 464-88);
            self.ivBottomLogo.frame = CGRectMake(0, self.view.bounds.size.height - 105, self.view.bounds.size.width, 105);
            self.ivTopImage.image  = [UIImage imageNamed:@"system_defaultAD_iphone4"];
        }
        
    }
    
    
    [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(systemADfinished:) userInfo:nil repeats:NO];
}

- (void)systemADfinished:(NSTimer *)timer{

    HGHomeViewController *vcHomePage = (HGHomeViewController *)[[self.viewControllers[0] viewControllers] firstObject];
    
    {
        
        if(![[HGUserData sharedInstance] isLoggedIn]){
            
            [vcHomePage freshHomeItemSilent];
            
            UINavigationController *navWelcome = (UINavigationController *)[[UIStoryboard loginStoryboard] instantiateViewControllerWithIdentifier:@"NavWelcome"];
            
            self.modalPresentationStyle = UIModalPresentationFullScreen;
            navWelcome.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            __weak HGTabBarViewController *wSelf = self;
            [self presentViewController:navWelcome animated:YES completion:^{
                HGTabBarViewController *swSelf = wSelf;
                [swSelf.ivTopImage removeFromSuperview];
                [swSelf.ivBottomLogo removeFromSuperview];
                
                self.ivTopImage = self.ivBottomLogo = nil;
            }];
        }else{
            [self.ivTopImage removeFromSuperview];
            [self.ivBottomLogo removeFromSuperview];
            
            self.ivTopImage = self.ivBottomLogo = nil;
            [vcHomePage freshHomeItem];
        }
    }
    
}


static float Tabbar_button_width = 64;
-(void)initHGTabbarContent
{
   
    if (SCREEN_WIDTH>320) {
        //iphone6. 6p
        self.suggestedTabbarHeight = 51;

    }
    else{
        //iphone4, 5
        self.suggestedTabbarHeight = 50;
    }
    
    self.lastSelectedButtonNumber = 1;
    self.fmTabbarView = [[UIView alloc] initWithFrame:CGRectMake(0, self.tabBar.frame.origin.y , self.view.frame.size.width, self.suggestedTabbarHeight)];
    [self.view addSubview:self.fmTabbarView];
    self.fmTabbarView.backgroundColor = [UIColor clearColor];
    
    self.tabBar.frame = CGRectMake(0, self.tabBar.frame.origin.y + self.tabBar.frame.size.height, self.tabBar.frame.size.width, self.tabBar.frame.size.height);

    //self.halo = [PulsingHaloLayer layer];
    
    Tabbar_button_width = self.fmTabbarView.frame.size.width / 5;
 
    
    self.button1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Tabbar_button_width, self.suggestedTabbarHeight )];
    self.label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, self.suggestedTabbarHeight - TABBAR_LAB_Y_OFFSET, Tabbar_button_width, Label_height)];
    [self.button1 addSubview:self.label1];
    self.label1.backgroundColor = [UIColor clearColor];
    self.label1.textColor = FANCY_COLOR(@"5f5f5f");
    self.label1.text = NSLocalizedString(@"Home", nil);
    [self.fmTabbarView addSubview:self.button1];
   
    
    
    self.button2 = [[UIButton alloc] initWithFrame:CGRectMake(Tabbar_button_width, 0, Tabbar_button_width, self.suggestedTabbarHeight )];
    self.label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, self.suggestedTabbarHeight - TABBAR_LAB_Y_OFFSET, Tabbar_button_width, Label_height)];
    [self.button2 addSubview:self.label2];
    self.label2.backgroundColor = [UIColor clearColor];
    self.label2.textColor = FANCY_COLOR(@"5f5f5f");;
    self.label2.text = NSLocalizedString(@"Search", nil);
    [self.fmTabbarView addSubview:self.button2];
    
    self.button3 = [[UIButton alloc] initWithFrame:CGRectMake(Tabbar_button_width*2, 0, Tabbar_button_width, self.suggestedTabbarHeight)];
    self.label3 = [[UILabel alloc] initWithFrame:CGRectMake(0, self.suggestedTabbarHeight- 15, Tabbar_button_width, Label_height)];
    [self.button3 addSubview:self.label3];
    self.label3.backgroundColor = [UIColor clearColor];
    self.label3.textColor = FANCY_COLOR(@"5f5f5f");;
    self.label3.text = NSLocalizedString(@"Sell", nil);
    [self.fmTabbarView addSubview:self.button3];

    
    self.button4 = [[UIButton alloc] initWithFrame:CGRectMake(Tabbar_button_width*3, 0, Tabbar_button_width, self.suggestedTabbarHeight)];
    self.label4 = [[UILabel alloc] initWithFrame:CGRectMake(0, self.suggestedTabbarHeight- TABBAR_LAB_Y_OFFSET, Tabbar_button_width, Label_height)];
    [self.button4 addSubview:self.label4];
    self.label4.backgroundColor = [UIColor clearColor];
    self.label4.textColor = FANCY_COLOR(@"5f5f5f");;
    self.label4.text = NSLocalizedString(@"Messages", nil);
    [self.fmTabbarView addSubview:self.button4];
    
    self.button5 = [[UIButton alloc] initWithFrame:CGRectMake(Tabbar_button_width*4, 0, Tabbar_button_width, self.suggestedTabbarHeight)];
    self.label5 = [[UILabel alloc] initWithFrame:CGRectMake(0, self.suggestedTabbarHeight- TABBAR_LAB_Y_OFFSET, Tabbar_button_width, Label_height)];
    [self.button5 addSubview:self.label5];
    self.label5.backgroundColor = [UIColor clearColor];
    self.label5.textColor = FANCY_COLOR(@"5f5f5f");;
    self.label5.text = NSLocalizedString(@"Profile", nil);
    [self.fmTabbarView addSubview:self.button5];


    [self.button1 addTarget:self action:@selector(button1pressed) forControlEvents:UIControlEventTouchUpInside];
    [self.button2 addTarget:self action:@selector(button2pressed) forControlEvents:UIControlEventTouchUpInside];
    [self.button3 addTarget:self action:@selector(button3pressed) forControlEvents:UIControlEventTouchUpInside];
    [self.button4 addTarget:self action:@selector(button4pressed) forControlEvents:UIControlEventTouchUpInside];
    [self.button5 addTarget:self action:@selector(button5pressed) forControlEvents:UIControlEventTouchUpInside];
    [self.button1 addTarget:self action:@selector(button1TouchDown) forControlEvents:UIControlEventTouchDown];
    [self.button2 addTarget:self action:@selector(button2TouchDown) forControlEvents:UIControlEventTouchDown];
    [self.button3 addTarget:self action:@selector(button3TouchDown) forControlEvents:UIControlEventTouchDown];
    [self.button4 addTarget:self action:@selector(button4TouchDown) forControlEvents:UIControlEventTouchDown];
    [self.button5 addTarget:self action:@selector(button5TouchDown) forControlEvents:UIControlEventTouchDown];
    
    
   
    [self setButtonNormalBG];
    
    if(SCREEN_WIDTH > 320)
    {
        [self.button1 setBackgroundImage:[UIImage imageNamed:@"tabbar_button1_psd"] forState:UIControlStateHighlighted];
        [self.button2 setBackgroundImage:[UIImage imageNamed:@"tabbar_button2_psd"] forState:UIControlStateHighlighted];
        [self.button3 setBackgroundImage:[UIImage imageNamed:@"tabbar_button3_psd"] forState:UIControlStateHighlighted];
        [self.button4 setBackgroundImage:[UIImage imageNamed:@"tabbar_button4_psd"] forState:UIControlStateHighlighted];
        [self.button5 setBackgroundImage:[UIImage imageNamed:@"tabbar_button5_psd"] forState:UIControlStateHighlighted];
    }
    else{
        [self.button1 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button1_psd"] forState:UIControlStateHighlighted];
        [self.button2 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button2_psd"] forState:UIControlStateHighlighted];
        [self.button3 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button3_psd"] forState:UIControlStateHighlighted];
        [self.button4 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button4_psd"] forState:UIControlStateHighlighted];
        [self.button5 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button5_psd"] forState:UIControlStateHighlighted];
    }
    
    
    
    
    // self.label1.textColor = SYSTEM_MY_ORANGE;
    [self setTab1SelectedStyle];
    self.label1.textAlignment = self.label2.textAlignment = self.label3.textAlignment = self.label4.textAlignment = self.label5.textAlignment = NSTextAlignmentCenter;
    self.label1.font = self.label2.font = self.label3.font = self.label4.font = self.label5.font = [UIFont fontWithName:FONT_TYPE_1 size :10];
    
    
    unreadmessage_xoffset = self.button4.frame.size.width/2 + 2;
    self.unreadmessage_mark = [[UIImageView alloc] initWithFrame:CGRectMake(unreadmessage_xoffset, 6, 29/2, 28/2)];
    self.unreadmessage_mark.image = [UIImage imageNamed:@"message-dot"];
    [self.button4 addSubview:self.unreadmessage_mark];
    self.unreadmessage_mark.hidden = YES;
    
    self.unreadMessageCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    self.unreadMessageCount.font = [UIFont systemFontOfSize:10];
    self.unreadMessageCount.textColor = [UIColor whiteColor];
    CGPoint centerPoint = CGPointMake(self.unreadmessage_mark.frame.size.width/2, self.unreadmessage_mark.frame.size.height/2);
    self.unreadMessageCount.center = centerPoint;
    self.unreadMessageCount.text = @"";
    self.unreadMessageCount.textAlignment = NSTextAlignmentCenter;
    
    [self.unreadmessage_mark addSubview:self.unreadMessageCount];
    
    
}
static float unreadmessage_xoffset = 0;
-(void)setLongUnReadBadget
{
    self.unreadmessage_mark.frame = CGRectMake(unreadmessage_xoffset, 6, 39/2, 28/2);
    self.unreadmessage_mark.image = [UIImage imageNamed:@"message-dot-2"];
    CGPoint centerPoint = CGPointMake(self.unreadmessage_mark.frame.size.width/2, self.unreadmessage_mark.frame.size.height/2);
    self.unreadMessageCount.center = centerPoint;
}
-(void)setShortUnReadBadget
{
    self.unreadmessage_mark.frame = CGRectMake(unreadmessage_xoffset, 6, 29/2, 28/2);
    self.unreadmessage_mark.image = [UIImage imageNamed:@"message-dot"];
    CGPoint centerPoint = CGPointMake(self.unreadmessage_mark.frame.size.width/2, self.unreadmessage_mark.frame.size.height/2);
    self.unreadMessageCount.center = centerPoint;
}
-(void)setButtonNormalBG
{
    
    if (SCREEN_WIDTH>320) {
        [self.button1 setBackgroundImage:[UIImage imageNamed:@"tabbar_button1"] forState:UIControlStateNormal];
        [self.button2 setBackgroundImage:[UIImage imageNamed:@"tabbar_button2"] forState:UIControlStateNormal];
        [self.button3 setBackgroundImage:[UIImage imageNamed:@"tabbar_button3"] forState:UIControlStateNormal];
        [self.button4 setBackgroundImage:[UIImage imageNamed:@"tabbar_button4"] forState:UIControlStateNormal];
        [self.button5 setBackgroundImage:[UIImage imageNamed:@"tabbar_button5"] forState:UIControlStateNormal];

    }
    else{
        [self.button1 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button1"] forState:UIControlStateNormal];
        [self.button2 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button2"] forState:UIControlStateNormal];
        [self.button3 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button3"] forState:UIControlStateNormal];
        [self.button4 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button4"] forState:UIControlStateNormal];
        [self.button5 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button5"] forState:UIControlStateNormal];

    }
    
    self.label1.textColor = self.label2.textColor = self.label3.textColor = self.label4.textColor = self.label5.textColor = FANCY_COLOR(@"5f5f5f");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addAnimation:(UIView*)view
{
    CAMediaTimingFunction *defaultCurve = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    CAAnimationGroup *animationGroup;
    animationGroup = [CAAnimationGroup animation];
    animationGroup.duration = 1;
    animationGroup.repeatCount = 1;
    animationGroup.removedOnCompletion = NO;
    animationGroup.timingFunction = defaultCurve;
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale.xy"];
    scaleAnimation.fromValue = @1;
    scaleAnimation.toValue = @2.0;
    scaleAnimation.duration = 1;
    
    CAKeyframeAnimation *opacityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.duration = 1;
    opacityAnimation.values = @[@1,@0];
    opacityAnimation.keyTimes = @[@0,@1];
    opacityAnimation.removedOnCompletion = NO;
    
    NSArray *animations = @[opacityAnimation,scaleAnimation];
    animationGroup.animations = animations;
    [view.layer addAnimation:animationGroup forKey:@"button1"];
}

-(void)button1pressed
{
    
    if (self.selectedIndex == 0) {
        //when it is already in home view controller , start to fresh home
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"hometotop" label:nil value:nil];
        UINavigationController* Navi = self.viewControllers[0];
        HGHomeViewController* homeViewcontroller= Navi.viewControllers[0];
        [homeViewcontroller freshHomeItem];
    }
    self.lastSelectedButtonNumber = 1;
    [self setTab1SelectedStyle];
}

- (UINavigationController *)setTab1SelectedStyle{
    [self setSelectedIndex:0];
    [self setSelectedViewController: [self.viewControllers objectAtIndex:0]];
    
    [self setButtonNormalBG];
    
    if(SCREEN_WIDTH>320)
    {
        [self.button1 setBackgroundImage:[UIImage imageNamed:@"tabbar_button1_psd"] forState:UIControlStateNormal];
        
    }
    else
    {
        [self.button1 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button1_psd"] forState:UIControlStateNormal];
        
    }
    self.label1.textColor = SYSTEM_MY_ORANGE;
    
    return (UINavigationController *)self.viewControllers[0];
}

-(void)button2pressed
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"discover" label:nil value:nil];
    self.lastSelectedButtonNumber = 2;
    [self setTab2SelectedStyle];
}

- (UINavigationController *)setTab2SelectedStyle{
    
    if([SVProgressHUD isVisible]){
        [SVProgressHUD dismiss];
    }
    
    [self setSelectedIndex:1];
    [self setSelectedViewController: [self.viewControllers objectAtIndex:1]];
    
    [self setButtonNormalBG];
    
    if(SCREEN_WIDTH>320)
    {
        [self.button2 setBackgroundImage:[UIImage imageNamed:@"tabbar_button2_psd"] forState:UIControlStateNormal];
        
    }
    else
    {
        [self.button2 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button2_psd"] forState:UIControlStateNormal];
        
    }
    self.label2.textColor = SYSTEM_MY_ORANGE;
    
    return (UINavigationController *)self.viewControllers[1];
}

-(void)button3pressed
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"sell" label:nil value:nil];
    
    if([[HGUserData sharedInstance] isLoggedIn]){
        [self startButton3Action];
    }else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"sell_login" label:nil value:nil];
        
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startButton3Action)];
    }
}

- (void)startButton3Action{
    
    [self setTab3SelectedStyle];
    
    UINavigationController *navUpload = (UINavigationController *)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"NavUpload"];
    [self presentViewController:navUpload animated:YES completion:nil];
}

- (UINavigationController *)setTab3SelectedStyle{

    if([SVProgressHUD isVisible]){
        [SVProgressHUD dismiss];
    }
    //[self setSelectedIndex:2];
    [self setButtonNormalBG];
    if(SCREEN_WIDTH>320)
    {
        [self.button3 setBackgroundImage:[UIImage imageNamed:@"tabbar_button3_psd"] forState:UIControlStateNormal];
        
    }
    else
    {
        [self.button3 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button3_psd"] forState:UIControlStateNormal];
        
    }
    self.label3.textColor = SYSTEM_MY_ORANGE;
    
    return (UINavigationController *)self.viewControllers[2];
}

-(void)button4pressed
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"message" label:nil value:nil];

    
    if( [[HGUserData sharedInstance] isLoggedIn] ){
    
        self.lastSelectedButtonNumber = 4;
        [self startButton4Action];
    }else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"message_login" label:nil value:nil];
        
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startButton4Action)];
    }
    
}

- (void)startButton4Action{
    
    UINavigationController* naviMessageCenter = [self setTab4SelectedStyle];
    HGMessageCenterController* MessageCenter = naviMessageCenter.viewControllers[0];
    MessageCenter.bNeedSegmentAutoChange = YES;
}

- (UINavigationController *)setTab4SelectedStyle{
    
    if([SVProgressHUD isVisible]){
        [SVProgressHUD dismiss];
    }
    
    [self setSelectedIndex:3];
    [self setSelectedViewController: [self.viewControllers objectAtIndex:3]];
    
    [self setButtonNormalBG];
    if(SCREEN_WIDTH>320)
    {
        [self.button4 setBackgroundImage:[UIImage imageNamed:@"tabbar_button4_psd"] forState:UIControlStateNormal];
        
    }
    else
    {
        [self.button4 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button4_psd"] forState:UIControlStateNormal];
        
    }self.label4.textColor = SYSTEM_MY_ORANGE;
    
    return (UINavigationController *)self.viewControllers[3];
}

-(void)button5pressed
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"profile" label:nil value:nil];
    
    if( [[HGUserData sharedInstance] isLoggedIn] ){
        
        self.lastSelectedButtonNumber = 5;
        [self startButton5Action];
        
    }else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"profile_login" label:nil value:nil];
        
        [[HGUtils sharedInstance] presentLoginGuide:self selector:@selector(startButton5Action)];
    }
}

- (void)startButton5Action{
    [self setTab5SelectedStyle];
}

- (UINavigationController *)setTab5SelectedStyle{
    
    if([SVProgressHUD isVisible]){
        [SVProgressHUD dismiss];
    }
    
    [self setSelectedIndex:4];
    [self setSelectedViewController: [self.viewControllers objectAtIndex:4]];
    
    [self setButtonNormalBG];
    if(SCREEN_WIDTH>320)
    {
        [self.button5 setBackgroundImage:[UIImage imageNamed:@"tabbar_button5_psd"] forState:UIControlStateNormal];
        
    }
    else
    {
        [self.button5 setBackgroundImage:[UIImage imageNamed:@"ip5_tabbar_button5_psd"] forState:UIControlStateNormal];
        
    }self.label5.textColor = SYSTEM_MY_ORANGE;
    
    return (UINavigationController *)self.viewControllers[4];
}

// Remove temporary
-(void)showMessageInstruction
{
    if(!self.ivTopImage && !self.ivBottomLogo){
        [[HGUtils sharedInstance] showTipsWithMessage:@"Check and manager your messages from buyers and sellers" atView:self.button4 inView:self.view animated:NO withOffset:CGPointMake(0, 7) withSize:CGSizeMake(154, 86) withDirection:PointDirectionDown];
    }
}




-(void)button1TouchDown
{
    self.label1.textColor = SYSTEM_MY_ORANGE;
}
-(void)button2TouchDown
{
    self.label2.textColor = SYSTEM_MY_ORANGE;
}
-(void)button3TouchDown
{
    self.label3.textColor = SYSTEM_MY_ORANGE;
}
-(void)button4TouchDown
{
    self.label4.textColor = SYSTEM_MY_ORANGE;
}
-(void)button5TouchDown
{
    self.label5.textColor = SYSTEM_MY_ORANGE;
}

@end

//
//  HGOffer.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGOffer.h"
#import "HGUtils.h"
#import "JSONKit.h"


@implementation HGOffer

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid", @"from_buyer":@"fromBuyer", @"from_me":@"fromMe", @"msg_type":@"messageType"}];
}
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[@"bShowTimestamp", @"messageType", @"payload"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    
    return NO;
}


-(instancetype)initWithDictionary:(NSDictionary*)dict error:(NSError**)err{
    
    HGOffer *offer = (HGOffer *)[super initWithDictionary:dict error:err];
    
    // From version 2.5, the product status is removed from the chat.
    // Treat all the offers as chat.
    switch(offer.type)
    {
        case HGOfferTypeOffer:
            break;
        case HGOfferTypeAcceptOffer:
            offer.text = NSLocalizedString(@"Deal. I accept your offer.",nil);
            break;
        case HGOfferTypeChat:
            break;
        case HGOfferTypeConfirmDelivery:
            offer.text = NSLocalizedString(@"I have received the listing. Thank you.",nil);
            break;
        case HGOfferTypeRatedSeller:
        {
            
            NSData *jsonData = [offer.text dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dict = [jsonData objectFromJSONData];
            NSString *text = [dict objectForKey:@"text"];
            NSString *score = [dict objectForKey:@"score"];
            
            NSString *message = [NSString stringWithFormat:NSLocalizedString(@"%1$@ stars! %2$@.",nil), score, text];
            
            if( message.length == 0){
                message = NSLocalizedString(@"The buyer rates you with 5 stars.",nil);
            }
            
            offer.text = message;
            break;
        }
        default:
            break;
    }


    return offer;
}

@end

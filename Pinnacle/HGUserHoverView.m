//
//  HGUserHoverView.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGUserHoverView.h"
#import "HGUser.h"
#import "HGUtils.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation HGUserHoverView
static int avatar_size = 49;
-(void)addAvatarCover
{
    self.avatarCover = [[UIImageView alloc] initWithFrame:self.avatarView.frame];
    self.avatarCover.image = [UIImage imageNamed:@"avatar-cover"];
    self.avatarCover.center = self.avatarView.center;
    [self addSubview:self.avatarCover];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.bkgView = [HGUserHoverBkgView new];
        self.bkgView.backgroundColor = UIColorFromRGBWithAlpha(0x000000, 0.3);
        //[self addSubview:self.bkgView];
        
        self.avatarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, avatar_size, avatar_size)];
        self.avatarView.clipsToBounds = YES;
        self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
        self.avatarView.layer.cornerRadius = CGRectGetHeight(self.avatarView.frame) * 0.5;
        [self addSubview:self.avatarView];
        [self addAvatarCover];
        
        self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, avatar_size/3, avatar_size/3)];
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
        CGPoint centerPoint = self.avatarView.center;
        centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
        centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
        self.userAvatar_verifyMark.center = centerPoint;
        [self addSubview:self.userAvatar_verifyMark];

        
        
        self.layer.cornerRadius = CGRectGetHeight(self.avatarView.bounds) * 0.5;
        //self.layer.borderColor = [UIColor darkGrayColor].CGColor;
        //self.layer.borderWidth = 0.0;
        self.clipsToBounds = YES;
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)layoutSubviews
{
    self.bkgView.frame = self.bounds;
}

- (void)setUser:(HGUser *)user
{
    if (_user == user) {
        return;
    }
    
    _user = user;
    int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatarView.frame.size.width*2];
     NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:user.portraitLink width:suggestWidth height:suggestWidth];
    
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    
    if (user.verified.intValue == 1) {
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    
    [self.bkgView setNeedsDisplay];
    
    [self sizeToFit];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGSize)sizeThatFits:(CGSize)size
{
    UIFont * font = [UIFont systemFontOfSize:16.0];
    CGRect r = [_user.displayName boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, font.lineHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    return CGSizeMake(avatar_size + CGRectGetWidth(r), avatar_size);
}

@end

@implementation HGUserHoverBkgView

- (void)drawRect:(CGRect)rect
{
    HGUserHoverView * ownerView = (HGUserHoverView *)self.superview;
    UIFont * font = [UIFont systemFontOfSize:16.0];
    [ownerView.user.displayName drawInRect:CGRectMake(40, (CGRectGetHeight(rect) - font.lineHeight) * 0.5, CGRectGetWidth(rect) - 40, font.lineHeight) withAttributes:@{NSFontAttributeName:font, NSForegroundColorAttributeName:FANCY_COLOR(@"242424")}];
}

@end

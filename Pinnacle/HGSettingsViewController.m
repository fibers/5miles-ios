//
//  HGSettingsViewController.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSettingsViewController.h"
#import "HGAppData.h"
#import "HGAppDelegate.h"
#import "HGCurrencySelectController.h"
#import "HGWebContentController.h"
#import "HGUtils.h"
#import "HGBackDoorViewController.h"
#import "HGPushAndMailViewController.h"
#import "JSONKit.h"
#import "UIStoryboard+Pinnacle.h"


NSString * const kAboutUsLink = @"https://5milesapp.com/mobile/about";
NSString * const kSupport = @"https://5milesapp.com/mobile/support";

NSString * const kTermsOfServiceLink = @"https://5milesapp.com/mobile/terms";
NSString * const kPrivacyPolicyLink = @"https://5milesapp.com/mobile/privacy";

@interface HGSettingsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lbCurrency;

@end

@implementation HGSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)customizeBackButton
{
    
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
}
-(void)setCellAccessIcon:(UITableViewCell*)cell
{
    UIImageView*accView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 17/2, 30/2)];
    accView.image = [UIImage imageNamed:@"uitableview_cell_access"];
    cell.accessoryView = accView;
    
    
}
-(void)setAllCell
{
    [self setCellAccessIcon:self.currencyCell];
    [self setCellAccessIcon:self.NotificationCell];
    [self setCellAccessIcon:self.AboutUsCell];
    [self setCellAccessIcon:self.supportCell];
    [self setCellAccessIcon:self.PrivacyPolicyCell];
    [self setCellAccessIcon:self.TermsOfUseCell];
    [self setCellAccessIcon:self.LeaveFeedBackCell];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [self setAllCell];
    [self customizeBackButton];
    NSString* versionBuild = [[HGAppData sharedInstance] versionBuild];
    self.versionLabel.text = versionBuild;
    
    self.versionAutoCheckHintLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 5, 150, 35)];
    self.versionAutoCheckHintLabel.textAlignment = NSTextAlignmentRight;
    [self.version_cell addSubview:self.versionAutoCheckHintLabel];
    
    self.version_cell.accessoryType = UITableViewCellAccessoryNone;
   
    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
    self.lbCurrency.text = [HGUserData sharedInstance].preferredCurrency;
    [[HGUtils sharedInstance] gaTrackViewName:@"setting_view"];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(void)startWebViewPushAndMailSetting
{
    NSString *str = [REMOTE_PUSH_SETTING stringByAppendingFormat:@"?user_token=%@",[HGUserData sharedInstance].fmUserToken ];
    HGWebContentController *vcWebContent = (HGWebContentController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCWebContent"];
    vcWebContent.contentLink = str;
    [self.navigationController pushViewController:vcWebContent animated:YES];
}

-(void)startLocalPushAndMailSetting
{
    HGPushAndMailViewController * ViewController = [[HGPushAndMailViewController alloc] initWithNibName:@"HGPushAndMailViewController" bundle:nil];
    [self.navigationController pushViewController:ViewController animated:YES];
}

static int clickCount = 0;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0 && indexPath.row == 1) {
        [self startWebViewPushAndMailSetting];
        //[self startLocalPushAndMailSetting];
    }
    if (indexPath.section == 2 && indexPath.row == 0) {
        //start back door.
               NSLog(@"click on version section");
#ifdef DEBUG
        clickCount++;
#else
        //release version close the door.
         clickCount=0;
#endif
        if (clickCount >= 5) {
            clickCount = 0;
            //start back door.
            HGBackDoorViewController * bdoorViewController = [[HGBackDoorViewController alloc] initWithNibName:@"HGBackDoorViewController" bundle:nil];
            [self.navigationController pushViewController:bdoorViewController animated:YES];
            
        }

    }
    if (indexPath.section == 2 && indexPath.row == 1) {
        
        UIActionSheet * actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles:
                                       NSLocalizedString(@"Confirm",nil), nil];
        actionSheet.tag = 1;
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showInView:self.view];
        
    } else if (indexPath.section == 1 && indexPath.row == 4) {
        [self performSegueWithIdentifier:@"SettingsToFeedback" sender:self];
    } else if (indexPath.section == 0 && indexPath.row == 0) {
        [self performSegueWithIdentifier:@"ShowCurrencySelection" sender:self];
    }
}

#pragma mark - UIActionsheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex) {
            case 0:
            {
                [self confirmLogout];
                break;
            }
           
            default:
                break;
        }
    }
}

-(void)confirmLogout
{
    NSLog(@"tapped logout");
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"logout" label:nil value:nil];
    [self.navigationController popToRootViewControllerAnimated:NO];
    [(HGAppDelegate *)[[UIApplication sharedApplication] delegate] logUserOut];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowAbout"]) {
        HGWebContentController * vcDest = segue.destinationViewController;
        vcDest.contentLink = kAboutUsLink;
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"setting_view" action:@"aboutus" label:nil value:nil];
    }else if([segue.identifier isEqualToString:@"showSupport"]){
        HGWebContentController * vcDest = segue.destinationViewController;
        vcDest.contentLink = [kSupport stringByAppendingFormat:@"/?email=%@&name=%@", [HGUserData sharedInstance].userEmail,[HGUserData sharedInstance].userDisplayName];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"setting_view" action:@"support" label:nil value:nil];
    }
    else if ([segue.identifier isEqualToString:@"ShowTerms"]) {
        HGWebContentController * vcDest = segue.destinationViewController;
        vcDest.contentLink = kTermsOfServiceLink;
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"setting_view" action:@"termofuse" label:nil value:nil];
    } else if ([segue.identifier isEqualToString:@"ShowPrivacy"]) {
        HGWebContentController * vcDest = segue.destinationViewController;
        vcDest.contentLink = kPrivacyPolicyLink;
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"setting_view" action:@"policy" label:nil value:nil];
    }
}

#pragma mark - Unwind Navigation

- (IBAction)backFromCurrencySelection:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"BackToParent"]) {
        HGCurrencySelectController * vcSource = segue.sourceViewController;
        self.lbCurrency.text = vcSource.selectedCurrency;
        
        [HGUserData sharedInstance].preferredCurrency = vcSource.selectedCurrency;
    }
}


- (IBAction)onTouchShare:(id)sender {
    NSString * template = NSLocalizedString(@"Join me and create your own mobile marketplace on the #5milesapp! Download it here: https://5milesapp.com/ Easy to sell. Fun to explore. Safe to buy.", nil);
    
    UIActivityViewController * vcActivity = [[UIActivityViewController alloc] initWithActivityItems:@[template] applicationActivities:nil];
    [self presentViewController:vcActivity animated:YES completion:nil];
}


@end

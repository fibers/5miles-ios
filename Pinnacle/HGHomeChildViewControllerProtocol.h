//
//  HGHomeChildViewControllerProtocol.h
//  Pinnacle
//
//  Created by mumuhou on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HGHomeChildViewControllerProtocol <NSObject>

- (void)refreshContent;

- (void)refreshContentSilent;

@end

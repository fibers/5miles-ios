//
//  HGSignUpEmailController.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-5.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSignUpEmailController.h"
#import "HGAppData.h"
#import "HGUtils.h"
#import "AppsFlyerTracker.h"
#import "NSString+EmailAddresses.h"
#import "HGWebContentController.h"
#import "TTTAttributedLabel.h"
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <SVProgressHUD.h>
#import "UIStoryboard+Pinnacle.h"
#import "HGEmailSuggestTextField.h"

#define TAG_TF_USERNAME 1001
#define TAG_TF_PASSWORD 1002

@interface HGSignUpEmailController () <UITextFieldDelegate, TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgCancel;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfDisplayName;
@property (weak, nonatomic) IBOutlet HGEmailSuggestTextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lbDescription;

@property (nonatomic, assign) CGFloat yKeyboardShow;
@property (nonatomic,strong) UITapGestureRecognizer *gestTapOutOfKeyboard;


@end

@implementation HGSignUpEmailController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.yKeyboardShow = self.view.frame.origin.y + Y_OFFSET_KEYBOARD_SHOW;
    
    self.imgCancel.image = [UIImage imageNamed: @"cancel.png"];
    UITapGestureRecognizer *cancelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapCancel:)];
    self.imgCancel.userInteractionEnabled = YES;
    [self.imgCancel addGestureRecognizer:cancelGesture];
    
    self.imgLogo.image = [UIImage imageNamed: @"sign-logo.png"];
    
    
    self.lbTitle.text = NSLocalizedString(@"Your Mobile Marketplace", nil);
    self.lbTitle.font = [UIFont fontWithName:@"Museo-700" size:18];
    self.lbTitle.textColor = FANCY_COLOR(@"818181");
    self.lbTitle.textAlignment = NSTextAlignmentCenter;
    
    self.tfDisplayName.delegate = self;
    self.tfDisplayName.tag = TAG_TF_USERNAME;
    self.tfDisplayName.backgroundColor = FANCY_COLOR(@"f0f0f0");
    self.tfDisplayName.font = [UIFont systemFontOfSize:15];
    self.tfDisplayName.layer.cornerRadius = 3.0f;
    [self.tfDisplayName setPlaceholder: NSLocalizedString(@"First and Last name", nil)];
    [[HGUtils sharedInstance ]addHeadPadding:54.0f forTextField:self.tfDisplayName];
    
    UIImageView *iconUsername = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, self.tfDisplayName.bounds.size.height)];
    iconUsername.image = [UIImage imageNamed:@"icon-signup-username"];
    iconUsername.contentMode = UIViewContentModeCenter;
    [self.tfDisplayName.leftView addSubview:iconUsername];
    
    UIView *separatorUsername = [[UIView alloc] initWithFrame:CGRectMake(iconUsername.bounds.size.width, 0, 1, self.tfDisplayName.bounds.size.height)];
    separatorUsername.backgroundColor = [UIColor whiteColor];
    [self.tfDisplayName.leftView addSubview:separatorUsername];
    
    NSString *domainFilePath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"email_suggest_domain.plist"];
    NSArray *domainList = [NSArray arrayWithContentsOfFile:domainFilePath];
    self.tfEmail.preferredDomainList = domainList;
    
    self.tfEmail.delegate = self;
    self.tfEmail.font = [UIFont systemFontOfSize:15];
    self.tfEmail.backgroundColor = FANCY_COLOR(@"f0f0f0");
    self.tfEmail.layer.cornerRadius = 3.0f;
    [self.tfEmail setPlaceholder: NSLocalizedString(@"Email address", nil)];
    [[HGUtils sharedInstance ]addHeadPadding:54.0f forTextField:self.tfEmail];
    
    UIImageView *iconEmail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, self.tfEmail.bounds.size.height)];
    iconEmail.image = [UIImage imageNamed:@"icon-signup-email"];
    iconEmail.contentMode = UIViewContentModeCenter;
    [self.tfEmail.leftView addSubview:iconEmail];
    
    UIView *separatorEmail = [[UIView alloc] initWithFrame:CGRectMake(iconEmail.bounds.size.width, 0, 1, self.tfEmail.bounds.size.height)];
    separatorEmail.backgroundColor = [UIColor whiteColor];
    [self.tfEmail.leftView addSubview:separatorEmail];
   
    self.tfPassword.delegate = self;
    self.tfPassword.tag = TAG_TF_PASSWORD;
    self.tfPassword.backgroundColor = FANCY_COLOR(@"f0f0f0");
    self.tfPassword.font = [UIFont systemFontOfSize:15];
    self.tfPassword.layer.cornerRadius = 3.0f;
    [self.tfPassword setPlaceholder:NSLocalizedString(@"Password (6~16 characters)", nil)];
    [[HGUtils sharedInstance ]addHeadPadding:54.0f forTextField:self.tfPassword];

    UIImageView *iconPassword = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, self.tfPassword.bounds.size.height)];
    iconPassword.image = [UIImage imageNamed:@"icon-signup-password"];
    iconPassword.contentMode = UIViewContentModeCenter;
    [self.tfPassword.leftView addSubview:iconPassword];
    
    UIView *separatorPassword = [[UIView alloc] initWithFrame:CGRectMake(iconPassword.bounds.size.width, 0, 1, self.tfPassword.bounds.size.height)];
    separatorPassword.backgroundColor = [UIColor whiteColor];
    [self.tfPassword.leftView addSubview:separatorPassword];
    
    self.btnSignup.layer.cornerRadius = 3.0;
    [self.btnSignup setTitle:NSLocalizedString(@"Sign up", nil) forState:UIControlStateNormal];
    self.btnSignup.titleLabel.font = [UIFont systemFontOfSize:18];
    self.btnSignup.backgroundColor = FANCY_COLOR(@"ff8830");
    [self.btnSignup setTitleColor:FANCY_COLOR(@"ffffff") forState:UIControlStateNormal];
    
    
    // Use TTTAttributedLabel to set the description content with hyperlinks.
    self.lbDescription.font = [UIFont systemFontOfSize:12];
    self.lbDescription.textColor = FANCY_COLOR(@"242424");
    self.lbDescription.numberOfLines = 0;
    self.lbDescription.textAlignment = NSTextAlignmentLeft;
    self.lbDescription.lineBreakMode = NSLineBreakByWordWrapping;
    self.lbDescription.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.lbDescription.delegate = self;
    
    NSString *description = NSLocalizedString(@"By creating an account and signing up to 5miles, you agree to the Terms of Use and Privacy Policy.", nil);
    
    NSRange rangeTerms = [description rangeOfString: NSLocalizedString(@"Terms of Use", nil)];
    NSRange rangePrivacy = [description rangeOfString: NSLocalizedString(@"Privacy Policy", nil)];
    
    self.lbDescription.linkAttributes = @{(NSString *)kCTUnderlineStyleAttributeName : [NSNumber numberWithBool:YES],
                                          (NSString*)kCTForegroundColorAttributeName : FANCY_COLOR(@"242424")};
    self.lbDescription.text = description;
    
    [self.lbDescription addLinkToURL: [NSURL URLWithString:@"https://5milesapp.com/mobile/terms"] withRange:rangeTerms];
    [self.lbDescription addLinkToURL: [NSURL URLWithString:@"https://5milesapp.com/mobile/privacy"] withRange:rangePrivacy];
                                                      
}

- (void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
    [super viewWillDisappear:animated];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:NULL];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"signup_view"];
    [[AppsFlyerTracker sharedTracker] trackEvent:@"signup_confirm" withValue:@""];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TTTAttributedLabel Delegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    
   
    
    HGWebContentController *vcWeb = (HGWebContentController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCWebContent"];
    
    vcWeb.contentLink = [url absoluteString];
    
    [self.navigationController pushViewController:vcWeb animated:YES];
    
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.tfDisplayName]) {
        [self.tfEmail becomeFirstResponder];
    } else if ([textField isEqual:self.tfEmail]) {
        [self.tfPassword becomeFirstResponder];
    } else {
        [self _doSignUp];
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(textField.tag == TAG_TF_USERNAME){
        return newString.length > 50 ? NO:YES;
    }else if(textField.tag == TAG_TF_PASSWORD){
        return newString.length > 16 ? NO:YES;
    }
    
    return YES;
}

#pragma mark - UI Control Actions

- (IBAction)onTouchButtonSignup:(id)sender {
    [self _doSignUp];
}


#pragma mark - Helpers


- (void)keyboardWillShow: (NSNotification *)notification{
    
    if( self.view.frame.origin.y > self.yKeyboardShow){
        [UIView animateWithDuration:0.3f animations:^{
            CGRect rect = self.view.frame;
            rect.origin.y = self.yKeyboardShow;
            self.view.frame = rect;
        }];
    }
    
    if(IPHONE4X){
        self.imgCancel.hidden = YES;
        self.imgLogo.hidden = YES;
        self.lbTitle.hidden = YES;
    }
    
    self.gestTapOutOfKeyboard = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapOutOfKeyboard:)];
    [self.view addGestureRecognizer:self.gestTapOutOfKeyboard];
    
}

- (void)keyboardWillHide: (NSNotification *)notification{
    
    if( self.view.frame.origin.y == self.yKeyboardShow){
        [UIView animateWithDuration:0.3f animations:^{
            CGRect rect = self.view.frame;
            rect.origin.y = self.yKeyboardShow - Y_OFFSET_KEYBOARD_SHOW;
            self.view.frame = rect;
        }];
    }
    
    self.imgCancel.hidden = NO;
    self.imgLogo.hidden = NO;
    self.lbTitle.hidden = NO;
    
    [self.view removeGestureRecognizer:self.gestTapOutOfKeyboard];
}

- (void) onTapOutOfKeyboard: (UITapGestureRecognizer *)gesture{
    
    [self.view endEditing:YES];
}

- (void) onTapCancel: (UITapGestureRecognizer* )gesture{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)_doSignUp
{
    NSString * nickname = self.tfDisplayName.text;
    NSString * email = self.tfEmail.text;
    NSString * password = self.tfPassword.text;
    
    if (nickname.length == 0) {
        PXAlertView*view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"Please enter your name", nil)];
        [view useDefaultIOS7Style];
        [self.tfDisplayName becomeFirstResponder];
        return;
    }
    if (nickname.length > MAX_USER_NAME_LENGTH) {
        
        NSString* errorString = [NSLocalizedString(@"Max name length is", nil) stringByAppendingFormat:@" %d",MAX_USER_NAME_LENGTH ];
        PXAlertView * view = [PXAlertView showAlertWithTitle:nil message:errorString];
        [view useDefaultIOS7Style];
        return;
    }

    
    if (![email isValidEmailAddress]) {
        PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"This email doesn't seem to work.", nil) cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            [self.tfEmail becomeFirstResponder];
        }];
        [view useDefaultIOS7Style];
        return;
    }
    
    if (password.length < 6 || password.length > 16) {
        NSLog(@"Password is too short");
        PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"Your password should contain 6~16 characters.", nil) cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            [self.tfPassword becomeFirstResponder];
        }];
        [view useDefaultIOS7Style];
        return;
    }
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"welcome_view" action:@"SignUp" label:@"Email" value:nil];
    
    email = [[email stringByCorrectingEmailTypos] lowercaseString];
    self.tfEmail.text = email;
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:nickname forKey:@"nickname"];
    [params setObject:email forKey:@"email"];
    [params setObject:password forKey:@"password"];
    [params setObject:[[HGUtils sharedInstance] advertisingIdentifier] forKey:@"appsflyer_user_id"];
    
    [params setObject:[NSNumber numberWithDouble:[HGAppData sharedInstance].GPSlat] forKey:@"lat"];
    [params setObject:[NSNumber numberWithDouble:[HGAppData sharedInstance].GPSlon] forKey:@"lon"];
    
    [params setObject:DEVICE_UUID forKey:@"device_id"];
    
    if([HGUserData sharedInstance].bAnonymous){
        [params setObject:[HGUserData sharedInstance].fmUserID forKey:@"anonymous_user_id"];
    }else{
        [params setObject:@"" forKey:@"anonymous_user_id"];
    }
    
    
    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
   
    [params setObject:[timeZoneLocal name] forKey:@"timezone"];
    
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[HGAppData sharedInstance].guestApiClient FIVEMILES_POST:@"signup_email/" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        [[HGUserData sharedInstance] updateUserLoggedInfo:responseObject withAnonymousSignup:NO withAccountType:HGAccountTypeEmail];
        
        // If self.fmUserID changed, call this method after the updating of the self.fmUserID;
        
        
#ifndef DEBUG
        [[HGAppData sharedInstance].chatApiClient FIVEMILES_POST:@"register/" parameters:@{@"device_id":    [HGAppData sharedInstance].deviceToken} success:nil failure:nil];
#endif
        
        [[AppsFlyerTracker sharedTracker] trackEvent:@"register" withValue:@"email register iOS"];
        
        [self performSegueWithIdentifier:@"ShowFillProfile" sender:self];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"email sign up failed: %@", [error.userInfo objectForKey:@"body"]);
    }];
}

@end

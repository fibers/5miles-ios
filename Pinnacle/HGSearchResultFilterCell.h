//
//  HGSearchResultFilterCell.h
//  Pinnacle
//
//  Created by Alex on 15/7/23.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGSearchResultFilterCell : UITableViewCell


@property (nonatomic, strong) UILabel* titleString;

@end

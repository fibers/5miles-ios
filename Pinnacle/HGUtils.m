//
//  HGUtils.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-6.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGUtils.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "HGAppData.h"
#import "HGAppDelegate.h"
#import <CommonCrypto/CommonDigest.h>
#import <Branch.h>
#import "BDKNotifyHUD.h"
#import <PXAlertView/PXAlertView+Customization.h>
#import "HGSearchViewController.h"
#import "HGUser.h"
#import <MHPrettyDate.h>
#import "sys/utsname.h"
#import <AdSupport/AdSupport.h>
#import "UIStoryboard+Pinnacle.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <CommonCrypto/CommonDigest.h>
#import "TTTAttributedLabel.h"
#import "HGFBUtils.h"

@implementation HGUtils

+ (HGUtils *)sharedInstance
{
    static HGUtils * _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [HGUtils new];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _cloudinary = [CLCloudinary new];
        [_cloudinary.config setValue:@"fivemiles" forKey:@"cloud_name"];
        [_cloudinary.config setValue:@"495993672824736" forKey:@"api_key"];
        [_cloudinary.config setValue:@"ho0wS1epe1W_ydChszuqEyeThPo" forKey:@"api_secret"];
    }
    return self;
}

- (NSString *)cloudinaryLink:(NSString *)sourceLink width:(int)idealWidth height:(int)idealHeight
{
    if([sourceLink rangeOfString:@"h_"].location != NSNotFound && [sourceLink rangeOfString:@"w_"].location != NSNotFound)
    {
        return sourceLink;
    }
    
    NSRange range = [sourceLink rangeOfString:@"upload/"];
    if (range.location != NSNotFound) {
        NSUInteger parameterLocation = range.location + range.length;
        NSString *headPart = [sourceLink substringToIndex:parameterLocation];
        NSString *tailPart = [sourceLink substringFromIndex:parameterLocation];
        
        NSString *newLink;
        if(idealWidth != 0 && idealHeight !=0 ){
            newLink = [NSString stringWithFormat:@"%@w_%d,h_%d,c_fill/%@",headPart, idealWidth, idealHeight, tailPart];
        }else if(idealWidth == 0 || idealHeight != 0){
            newLink = [NSString stringWithFormat:@"%@h_%d,c_fill/%@",headPart, idealHeight, tailPart];
        }else if(idealWidth != 0 && idealHeight == 0){
            newLink = [NSString stringWithFormat:@"%@w_%d,c_fill/%@",headPart, idealWidth, tailPart];
        }else{
            newLink = sourceLink;
        }
        ////using jpg format
        newLink = [newLink stringByReplacingOccurrencesOfString:@".png" withString:@".jpg"];
        return newLink;
    } else {
        return sourceLink;
    }
}


- (void)uploadPhoto:(UIImage *)image delegate:(id<CLUploaderDelegate>)delegate
{
    NSData * photoData = UIImageJPEGRepresentation(image, 0.5);
    
    CLUploader * uploader = [[CLUploader alloc] init:_cloudinary delegate:delegate];
    [uploader upload:photoData options:@{}];

}

- (CLUploader*)uploadPhoto:(UIImage *)image withSignedOptions:(NSDictionary *)options delegate:(id<CLUploaderDelegate>)delegate
{
    NSData * photoData = UIImagePNGRepresentation(image);
    
    CLUploader * uploader = [[CLUploader alloc] init:_cloudinary delegate:delegate];
    [uploader upload:photoData options:options];
    return  uploader;
}

- (NSString *)documentDirectoryPath
{
    NSArray * urls = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL * docURL = [urls firstObject];
    return docURL.path;
}

- (CGRect)changeFrameHeight:(CGRect)originalFrame toHeight:(CGFloat)newHeight
{
    CGRect newFrame = originalFrame;
    newFrame.origin = originalFrame.origin;
    newFrame.size.width = originalFrame.size.width;
    newFrame.size.height = newHeight;
    
    return newFrame;
}

- (CGRect)changeFrameHeight:(CGRect)originalFrame offset:(CGFloat)offset
{
    CGRect newFrame = originalFrame;
    newFrame.origin = originalFrame.origin;
    newFrame.size.width = originalFrame.size.width;
    newFrame.size.height = originalFrame.size.height + offset;
    
    return newFrame;
}

- (NSString *)formatInteger:(NSInteger)value
{
    if (value >= 1000000) {
        return [NSString stringWithFormat:@"%.1fM", (float)(value)/1000000];
    } else if (value >= 1000) {
        return [NSString stringWithFormat:@"%.1fK", (float)(value)/1000];
    } else {
        return [NSString stringWithFormat:@"%d", (int)value];
    }
}



- (void)gaTrackViewName:(NSString *)viewName
{
    if (viewName.length == 0) {
        return;
    }
    
    id tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:viewName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)gaSendEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value
{
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:category action:action label:label value:value] build]];
}

- (UIImage *)centerCropImage:(UIImage *)image
{
    // Use smallest side length as crop square length
    CGFloat squareLength = MIN(image.size.width, image.size.height);
    // Center the crop area
    CGRect clippedRect = CGRectMake((image.size.width - squareLength) / 2, (image.size.height - squareLength) / 2, squareLength, squareLength);
    
    // Crop logic
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], clippedRect);
    UIImage * croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}

-(UIImage*)thumbnailOfImage:(UIImage*)image withSize:(CGSize)aSize
{
    if (!image)
        return nil;
    CGImageRef imageRef = [image CGImage];
    UIImage *thumb = nil;
    float _width = CGImageGetWidth(imageRef);
    float _height = CGImageGetHeight(imageRef);
    // hardcode width and height for now, shouldn't stay like that
    float _resizeToWidth;
    float _resizeToHeight;
    _resizeToWidth = aSize.width;
    _resizeToHeight = aSize.height;
    float _moveX = 0.0f;
    float _moveY = 0.0f;
    // determine the start position in the window if it doesn't fit the sizes 100%
    //NSLog(@" width: %f  to: %f", _width, _resizeToWidth);
    //NSLog(@" height: %f  to: %f", _height, _resizeToHeight);
    // resize the image if it is bigger than the screen only
    if ( (_width > _resizeToWidth) || (_height > _resizeToHeight) )
    {
        float _amount = 0.0f;
        if (_width > _resizeToWidth)
        {
            _amount = _resizeToWidth / _width;
            _width *= _amount;
            _height *= _amount;
        }
        if (_height > _resizeToHeight)
        {
            _amount = _resizeToHeight / _height;
            _width *= _amount;
            _height *= _amount;
        }
    }
    _width = (NSInteger)_width;
    _height = (NSInteger)_height;
    _resizeToWidth = _width;
    _resizeToHeight = _height;
    CGContextRef bitmap = CGBitmapContextCreate(
                                                NULL,
                                                _resizeToWidth,
                                                _resizeToHeight,
                                                CGImageGetBitsPerComponent(imageRef),
                                                CGImageGetBitsPerPixel(imageRef)*_resizeToWidth,
                                                CGImageGetColorSpace(imageRef),
                                                CGImageGetBitmapInfo(imageRef)
                                                );
    // now center the image
    _moveX = (_resizeToWidth - _width) / 2;
    _moveY = (_resizeToHeight - _height) / 2;
    CGContextSetRGBFillColor(bitmap, 1.f, 1.f, 1.f, 1.0f);
    CGContextFillRect( bitmap, CGRectMake(0, 0, _resizeToWidth, _resizeToHeight));
    CGContextDrawImage( bitmap, CGRectMake(_moveX, _moveY, _width, _height), imageRef );
    // create a templete imageref.
    CGImageRef ref = CGBitmapContextCreateImage( bitmap );
    thumb = [UIImage imageWithCGImage:ref];
    // release the templete imageref.
    CGContextRelease( bitmap );
    CGImageRelease( ref );
    return thumb;
}

-(UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2
{
    UIGraphicsBeginImageContext(image2.size);
    
    //Draw image2
    [image2 drawInRect:CGRectMake(0, 0, image2.size.width, image2.size.height)];
    
    //Draw image1
    [image1 drawInRect:CGRectMake(20, 20, image1.size.width, image1.size.height)];
    
    UIImage *resultImage=UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resultImage;
}

// Get IP Address
- (NSString *)getIPAddress {
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

-(int)getFromHexValue:(char)c
{
    int result = 0;
    if (c>='0' && c <='9' ) {
        result = c -'0';
    }
    else if(c>='a' && c<='f'){
        result = c -'a' + 10;
    }
    
    return result;
}
-(UIColor*)FancyColorWithString:(NSString*)colorvalue
{

    if (colorvalue.length!=6 && colorvalue.length != 8) {
        return [UIColor colorWithRed:0x0/255.0 green:0x0/255.0 blue:0x0/255.0 alpha:1.0];
    }
    colorvalue = [colorvalue lowercaseString];
    
    int redValue = 0.0;
    int greenValue = 0.0;
    int blueValue = 0.0;
    float alphaValue = 1.0;
    redValue = [self getFromHexValue:[colorvalue characterAtIndex:0]] * 16 + [self getFromHexValue:[colorvalue characterAtIndex:1]];
    
    greenValue = [self getFromHexValue:[colorvalue characterAtIndex:2]] * 16 + [self getFromHexValue:[colorvalue characterAtIndex:3]];
    blueValue = [self getFromHexValue:[colorvalue characterAtIndex:4]] * 16 + [self getFromHexValue:[colorvalue characterAtIndex:5]];
    
    if (colorvalue.length==8) {
        alphaValue = ([colorvalue characterAtIndex:6] * 10 + [colorvalue characterAtIndex:7])/100.0;
    }
    
    return [UIColor colorWithRed:redValue/255.0 green:greenValue/255.0 blue:blueValue/255.0 alpha:alphaValue];
    
}



// SHA1 generator....
-(NSString*) digest:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(data.bytes, (unsigned int)data.length, digest);
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    return output;
}

///md5 ====length..32
-(NSString *) md5HexDigest:(NSString*)input
{
    const char *original_str = [input cStringUsingEncoding:NSUTF8StringEncoding];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, (unsigned int)strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}

//let's asssume the branch url is always works ok, not waiting for return.
//
-(NSString*)getBranchUrl:(NSMutableDictionary*)params
{
    
    [params setObject:[HGUserData sharedInstance].fmUserID forKey:@"user_id"];
    [params setObject:@"channel" forKey:@"iOS"];

    
    if ([params valueForKey:@"$desktop_url"] ==nil) {
        [params setObject:@"https://5milesapp.com/" forKey:@"$desktop_url"];
    }
    
    if ([params valueForKey:@"md5string"]==nil) {
        return @"";
    }
    NSString*md5string = [params valueForKey:@"md5string"];
    NSLog(@"the branch md5 string is %@", md5string);
    NSString* suggestedUrl = [@"5miles-v2-" stringByAppendingString:md5string];
    
    Branch *branch = [Branch getInstance];
    [branch getShortURLWithParams:params andTags:@[@"version2.4"]
                       andChannel:@"iOS"
                       andFeature:BRANCH_FEATURE_TAG_SHARE andStage:@"level_6" andAlias:suggestedUrl andCallback:^(NSString *url, NSError *error) {
                           // show the link to the user or share it immediately
                           
                           if (!error) {
                               NSLog(@"the branch url is %@", url);
                           }
                           else {
                               NSLog(@"the branch url error %@", error);
                           }
                           
                       }];
    
    return [@"http://bnc.lt/" stringByAppendingString: suggestedUrl];
}

- (void) addHeadPadding:(CGFloat)padding forTextField:(UITextField*) textField {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0,0,padding,textField.bounds.size.height)];
    paddingView.backgroundColor = [UIColor clearColor];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.leftView = paddingView;
}

- (void)showSimpleAlert:(NSString *)alertString{
    
    if(![PXAlertView isCurrentViewShowed]){
        PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:alertString cancelTitle:NSLocalizedString(@"OK", nil) completion:nil];
        
        [view useDefaultIOS7Style];
    }

}

- (void)showSimpleAlert:(NSString *)alertString withTitle:(NSString *)title{
    
    if(![PXAlertView isCurrentViewShowed]){
        PXAlertView* view = [PXAlertView showAlertWithTitle:title message:alertString cancelTitle:NSLocalizedString(@"OK", nil) completion:nil];
        
        [view useDefaultIOS7Style];
    }

}


- (void)showSimpleAlert:(NSString *)alertString withTitle:(NSString *)title completion:(void(^)())completion
{
     if(![PXAlertView isCurrentViewShowed]){
        PXAlertView* view = [PXAlertView showAlertWithTitle:title message:alertString  cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex){
            completion();
        }];
         
        [view useDefaultIOS7Style];
     }
}

- (void)showSimpleAlert:(NSString *)alertString withTitle:(NSString *)title cancelTitle:(NSString *)cancelTitle otherTitle:(NSString *)otherTitle completion:(void(^)(BOOL cancelled, NSInteger buttonIndex))completion
{
    if(![PXAlertView isCurrentViewShowed]){
        PXAlertView* view = [PXAlertView showAlertWithTitle:title message:alertString  cancelTitle:cancelTitle otherTitle:otherTitle completion:completion];
        
        [view useDefaultIOS7Style];
    }
}

-(void)showSimpleNotify:(NSString *)alertString onView:(UIView *)view
{
    BDKNotifyHUD* hud = [BDKNotifyHUD notifyHUDWithView:nil text:alertString withFrameWith:220 withFrameHeight:100];
    hud.center = CGPointMake(view.center.x, view.center.y - 20);
    
    HGAppDelegate* appDelegate = (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.window addSubview:hud];
    [hud presentWithDuration:1.5f speed:0.5f inView:view completion:^{
        [hud removeFromSuperview];
    }];
}
-(BOOL)hasSimpleNotifyOnWindow
{
    HGAppDelegate* appDelegate = (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    for (UIView* subview in [appDelegate.window subviews]) {
        if ([subview isKindOfClass:[BDKNotifyHUD class]]) {
            return YES;
        }
    }
    return NO;
}

- (int)getImageSuggestEdgeLength:(int)edgeLength{
    if (edgeLength <= 100) {
        return 100;
    }
    else if(edgeLength <= 200){
        return 200;
    }
    else if(edgeLength <=300){
        return 300;
    }else if (edgeLength <= 400){
        return 400;
    }
    else if(edgeLength <= 650){
        return 650;
    }
    else{
        return 800;
    }
}

- (NSString *)friendlyDate:(NSDate *)date
{
    
    NSString* dateString = [MHPrettyDate prettyDateFromDate:date withFormat:MHPrettyDateFormatTodayTimeOnly];
    
    if ([dateString rangeOfString:@"/"].location != NSNotFound) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        [fmt setDateFormat:@"MM/dd/yyyy"];
        dateString = [fmt stringFromDate:date];
    }
    
    return dateString;
}



- (UIImage*) mergedImageOnMainImage:(UIImage *)mainImg WithImageArray:(NSArray *)imgArray AndImagePointArray:(NSArray *)imgPointArray
{
    
   
   
    UIGraphicsBeginImageContext(mainImg.size);
    
    [mainImg drawInRect:CGRectMake(0, 0, mainImg.size.width, mainImg.size.height)];
    int i = 0;
    
    for (UIImage *img in imgArray) {
        [img drawInRect:CGRectMake([[imgPointArray objectAtIndex:i] floatValue],
                                   [[imgPointArray objectAtIndex:i+1] floatValue],
                                   img.size.width,
                                   img.size.height)];
        
        i+=2;
    }
    
    CGImageRef MergeImg = CGImageCreateWithImageInRect(UIGraphicsGetImageFromCurrentImageContext().CGImage,
                                                          CGRectMake(0, 0, mainImg.size.width, mainImg.size.height));
    
    UIImage* image = [UIImage imageWithCGImage: MergeImg];
    CGImageRelease(MergeImg);
    UIGraphicsEndImageContext();
    
    if (image == nil) {
        return nil;
    }
    else {
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
        return image;
    }
}

- (NSString *)prettyDate:(NSInteger)timestamp
{
    if (timestamp==0) {
        return @"";
    }
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    long long int date = (long long int)time;
    
    long distance = (long)date - timestamp;
    int year= (int)distance / (365*24*3600);
    distance = distance % (365*24*3600);
    int mon = (int)distance /(30*24*3600);
    distance = distance % (30*24*3600);
    int weeks =(int)distance /(7*24*3600);
    distance = distance % (7*24*3600);
    int days = (int)distance /(24*3600);
    distance  = distance % (24*3600);
    int hours = (int)distance /(3600);
    distance = distance % (3600);
    int mins = (int)distance / 60;
    
    NSString* resultString =@"";
    if (year > 0) {
        if (year > 1) {
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d years ago",nil),year];
        }
        else{
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d year ago",nil),year];
        }
    }
    else if (mon > 0) {
        if (mon > 1) {
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d months ago",nil), mon];
        }
        else{
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d month ago",nil),mon];
        }
    }
    else if(weeks > 0)
    {
        if (weeks > 1) {
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d weeks ago",nil),weeks];
        }
        else{
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d week ago",nil),weeks];
        }
    }
    else if(days > 0)
    {
        if(days>1)
        {
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d days ago",nil),days];
        }
        else
        {
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d day ago",nil),days];
        }
    }
    else if(hours >0)
    {
        if (hours > 1) {
            resultString = [@"" stringByAppendingFormat: NSLocalizedString(@"%d hours ago",nil),hours];
        }
        else{
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d hour ago",nil), hours];
        }
    }
    else if(mins > 0)
    {
        if (mins > 0) {
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d mins ago",nil), mins];
        }
        else{
            resultString = [@"" stringByAppendingFormat:NSLocalizedString(@"%d min ago",nil),mins ];
        }
    }
    
    if(resultString.length == 0)
    {
       // NSDate * date = [NSDate dateWithTimeIntervalSince1970:timestamp];
       // NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
       // [fmt setDateFormat:@"MM/dd/yyyy hh:mm:ss"];
       // resultString=[fmt stringFromDate:date];
        resultString = @"Just now";
    }
    
    
    
    return resultString;
}

- (NSString *)getDistanceKm:(CGFloat)distance
{
    NSString * distString;
    if (distance < 100) {
        
        if (distance < 0) {
            distString = @"";
        }else{
            distString = [NSString stringWithFormat: @"%.2f %@", distance, NSLocalizedString(@"km", nil)];
        }
        
    } else {
        distString = [NSString stringWithFormat: @"%d %@", (int)distance, NSLocalizedString(@"km", nil)];
    }
    
    return distString;
    
}

- (NSString *)getDistanceMiles:(CGFloat)distance
{
    NSString * distString;
    float distance_byMile = distance * KM_TO_MILE;
    
    if (distance_byMile < 100) {
        
        if (distance_byMile < 0) {
            distString = @"";
        }else{
            distString = [NSString stringWithFormat: @"%.2f %@", distance_byMile, NSLocalizedString(@"miles", nil)];
        }
    } else {
        distString = [NSString stringWithFormat: @"%d %@", (int)distance_byMile, NSLocalizedString(@"miles", nil)];
    }
    return distString;
}

- (NSString *)deviceString{
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"])           { platform = @"iPhone 1G";
    } else if ([platform isEqualToString:@"iPhone1,2"])    { platform = @"iPhone 3G";
    } else if ([platform isEqualToString:@"iPhone2,1"])    { platform = @"iPhone 3GS";
    } else if ([platform isEqualToString:@"iPhone3,1"])    { platform = @"iPhone 4";
    } else if ([platform isEqualToString:@"iPhone3,2"])    { platform = @"iPhone 4";
    } else if ([platform isEqualToString:@"iPhone3,3"])    { platform = @"Verizon iPhone 4";
    } else if ([platform isEqualToString:@"iPhone4,1"])    { platform = @"iPhone 4S";
    } else if ([platform isEqualToString:@"iPhone5,1"])    { platform = @"iPhone 5 (GSM)";
    } else if ([platform isEqualToString:@"iPhone5,2"])    { platform = @"iPhone 5 (GSM+CDMA)";
    } else if ([platform isEqualToString:@"iPhone5,3"])    { platform = @"iPhone 5C (GSM)";
    } else if ([platform isEqualToString:@"iPhone5,4"])    { platform = @"iPhone 5C (GSM+CDMA)";
    } else if ([platform isEqualToString:@"iPhone6,1"])    { platform = @"iPhone 5S (GSM)";
    } else if ([platform isEqualToString:@"iPhone6,2"])    { platform = @"iPhone 5S (GSM+CDMA)";
    } else if ([platform isEqualToString:@"iPhone7,1"])    { platform = @"iPhone 6 Plus";
    } else if ([platform isEqualToString:@"iPhone7,2"])    { platform = @"iPhone 6";
    } else if ([platform isEqualToString:@"iPod1,1"])      { platform = @"iPod Touch 1G";
    } else if ([platform isEqualToString:@"iPod2,1"])      { platform = @"iPod Touch 2G";
    } else if ([platform isEqualToString:@"iPod3,1"])      { platform = @"iPod Touch 3G";
    } else if ([platform isEqualToString:@"iPod4,1"])      { platform = @"iPod Touch 4G";
    } else if ([platform isEqualToString:@"iPod5,1"])      { platform = @"iPod Touch 5G";
    } else if ([platform isEqualToString:@"iPad1,1"])      { platform = @"iPad";
    } else if ([platform isEqualToString:@"iPad2,1"])      { platform = @"iPad 2 (WiFi)";
    } else if ([platform isEqualToString:@"iPad2,2"])      { platform = @"iPad 2 (GSM)";
    } else if ([platform isEqualToString:@"iPad2,3"])      { platform = @"iPad 2 (CDMA)";
    } else if ([platform isEqualToString:@"iPad2,4"])      { platform = @"iPad 2 (WiFi)";
    } else if ([platform isEqualToString:@"iPad2,5"])      { platform = @"iPad Mini (WiFi)";
    } else if ([platform isEqualToString:@"iPad2,6"])      { platform = @"iPad Mini (GSM)";
    } else if ([platform isEqualToString:@"iPad2,7"])      { platform = @"iPad Mini (GSM+CDMA)";
    } else if ([platform isEqualToString:@"iPad3,1"])      { platform = @"iPad 3 (WiFi)";
    } else if ([platform isEqualToString:@"iPad3,2"])      { platform = @"iPad 3 (GSM+CDMA)";
    } else if ([platform isEqualToString:@"iPad3,3"])      { platform = @"iPad 3 (GSM)";
    } else if ([platform isEqualToString:@"iPad3,4"])      { platform = @"iPad 4 (WiFi)";
    } else if ([platform isEqualToString:@"iPad3,5"])      { platform = @"iPad 4 (GSM)";
    } else if ([platform isEqualToString:@"iPad3,6"])      { platform = @"iPad 4 (GSM+CDMA)";
    } else if ([platform isEqualToString:@"iPad4,1"])      { platform = @"iPad Air (WiFi)";
    } else if ([platform isEqualToString:@"iPad4,2"])      { platform = @"iPad Air (Cellular)";
    } else if ([platform isEqualToString:@"iPad4,3"])      { platform = @"iPad Air";
    } else if ([platform isEqualToString:@"iPad4,4"])      { platform = @"iPad mini 2G (WiFi)";
    } else if ([platform isEqualToString:@"iPad4,5"])      { platform = @"iPad mini 2G (Cellular)";
    } else if ([platform isEqualToString:@"iPad4,6"])      { platform = @"iPad mini 2G";
    } else if ([platform isEqualToString:@"i386"])         { platform = @"Simulator";
    } else if ([platform isEqualToString:@"x86_64"])       { platform = @"Simulator";
    } else                                                 { platform = @"unknown";
    }
    
    return platform;
    
}

-(float)getSuggestSeperatorLineHeight
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    if([platform isEqualToString:@"iPhone7,1"])
    {
        //iphone 6 plus
        return 0.3333;
    }
    if ([platform isEqualToString:@"iPhone7,2"] || [platform isEqualToString:@"iPhone6,1"] || [platform isEqualToString:@"iPhone6,2"]) {
        ///iphone 6, iphone5s.
        return 0.5;
    }
    
    
    
    
    ///other cases
    return 1;
}

- (NSString *)advertisingIdentifier{
    NSString *uuid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    if(!uuid){
        NSLog(@"Cannot get the advertisingIdentifier now, please try again later");
        return @"";
    }
    return uuid;
}


-(int)checkPushNotificationStatus
{
    int pushEnabled=0 ;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
            pushEnabled=1;
        }
        else
            pushEnabled=0;
    }
    else
    {
        UIRemoteNotificationType types = [[UIApplication sharedApplication]        enabledRemoteNotificationTypes];
        if (types & UIRemoteNotificationTypeAlert)
            pushEnabled=1;
        else
            pushEnabled=0;
    }
    
    NSLog(@"devic push notification status is %d",pushEnabled );
    return pushEnabled;
}

/*- (void)presentLoginGuide:(UIViewController *)vc selector:(SEL)sel{
    
    @try {
        UINavigationController *navLogin = (UINavigationController *)[[UIStoryboard loginStoryboard] instantiateViewControllerWithIdentifier:@"NavLogin"];
        HGAnonymousLoginViewController *vcAnonymousLogin = (HGAnonymousLoginViewController *)[[navLogin viewControllers] firstObject];
        vcAnonymousLogin.tempPresentingVC = vc;
        
        __weak UIViewController* weakVC = vc;
        [vc presentViewController:navLogin animated:YES completion:^{
            
            UIViewController *strongVC = weakVC;
            [[NSNotificationCenter defaultCenter] addObserver:strongVC selector:sel name:NOTIFICATION_USER_LOGIN_WITH_ACTION object:nil];
            
            @weakify(vc);
            [[vc rac_willDeallocSignal] subscribeCompleted:^{
                @strongify(vc);
                [[NSNotificationCenter defaultCenter] removeObserver:vc name:NOTIFICATION_USER_LOGIN_WITH_ACTION object:nil];
            }];
        }];
        
        
        NSLog(@"test present action!");
    }
    @catch (NSException *exception) {
        //FLURRY_LOG_ERROR(exception);
    }
    
}
 */

- (void)presentLoginGuide:(UIViewController *)vc{
    @try {
        UINavigationController *navLogin = (UINavigationController *)[[UIStoryboard loginStoryboard] instantiateViewControllerWithIdentifier:@"NavLogin"];
        
        [vc presentViewController:navLogin animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        //FLURRY_LOG_ERROR(exception);
    }
}


- (CMPopTipView *)showTipsWithMessage:(NSString *)message atView:(UIView *)atView inView:(UIView *)inView animated:(BOOL)animated withOffset:(CGPoint)offset withSize:(CGSize)size withDirection:(PointDirection)direction{
    
    CGFloat width = size.width - 20;
    CGFloat height = size.height - 25;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    UIImage *imgClose = [UIImage imageNamed:@"pop_tip_close"];
    UIImageView *ivClose = [[UIImageView alloc] initWithImage:imgClose];
    ivClose.frame = CGRectMake(width - imgClose.size.width, height - imgClose.size.height, 10, 10);
    [customView addSubview:ivClose];

    
    CMPopTipView *popTipView = [[CMPopTipView alloc] initWithCustomView:customView];
    popTipView.disableTapToDismiss = NO;
    popTipView.message = message;
    popTipView.bubblePaddingX = 8;
    popTipView.bubblePaddingY = 8;
    popTipView.backgroundColor = [UIColor colorWithRed:0x24/255 green:0x24/2255 blue:0x24/255 alpha:0.84];
    popTipView.textFont = [UIFont systemFontOfSize:12];
    popTipView.textColor = [UIColor whiteColor];
    popTipView.textAlignment = NSTextAlignmentLeft;
    popTipView.cornerRadius = 4;
    popTipView.has3DStyle = NO;
    popTipView.preferredPointDirection = direction;
    popTipView.topMargin = offset.y;
    
    [popTipView presentPointingAtView:atView inView:inView animated:animated xOffset:offset.x];
    
    NSTimeInterval timeInteval = 5.0f;
    [popTipView autoDismissAnimated:YES atTimeInterval:timeInteval];
    
    return popTipView;
}

// Caution: should dismiss in viewWillDisappear manually.
- (CMPopTipView *)showTipsWithMessage:(NSString *)message atBarButtonItem:(UIBarButtonItem *)barButtonItem animated:(BOOL)animated withOffset:(CGPoint)offset withSize:(CGSize)size{
    
    CGFloat width = size.width - 20;
    CGFloat height = size.height - 25;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    UIImage *imgClose = [UIImage imageNamed:@"pop_tip_close"];
    UIImageView *ivClose = [[UIImageView alloc] initWithImage:imgClose];
    ivClose.frame = CGRectMake(width - imgClose.size.width, height - imgClose.size.height, 10, 10);
    [customView addSubview:ivClose];
    
    CMPopTipView *popTipView = [[CMPopTipView alloc] initWithCustomView:customView];
    popTipView.disableTapToDismiss = NO;
    popTipView.message = message;
    popTipView.bubblePaddingX = 8;
    popTipView.bubblePaddingY = 8;
    popTipView.backgroundColor = [UIColor colorWithRed:0x24/255 green:0x24/2255 blue:0x24/255 alpha:0.84];
    popTipView.textFont = [UIFont systemFontOfSize:12];
    popTipView.textColor = [UIColor whiteColor];
    popTipView.textAlignment = NSTextAlignmentLeft;
    popTipView.cornerRadius = 4;
    popTipView.has3DStyle = NO;
    
    popTipView.topMargin = offset.y;
    popTipView.sidePadding = 0 - offset.x;
    
    [popTipView presentPointingAtBarButtonItem:barButtonItem animated:animated];
    
    NSTimeInterval timeInteval = 5.0f;
    [popTipView autoDismissAnimated:YES atTimeInterval:timeInteval];
    
    return popTipView;
    
}


- (NSString *)apiErrorMessageFrom:(NSError *)error{
    
    if(!error){
        return @"";
    }
    
    NSString *errorDomain = [error domain];
    NSString *errorMessage;
    
    if([errorDomain isEqualToString:AFURLResponseSerializationErrorDomain]){
        NSData *data = [error.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSNumber *errorCode = [response objectForKey:@"err_code"];
        NSString *errorMsg = [response objectForKey:@"err_msg"];
        
        // Error code must be lied with a error message
        if(errorCode == nil || errorMsg == nil){
            //errorMessage = NSLocalizedString(@"Sorry, 5miles is experiencing a technical issue. Please try again later.", nil);
            
            NSException* ex = [[NSException alloc] initWithName:@"ExceptionName"                                 reason:@"unknonw server error message."
                                                       userInfo:nil];
             [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",ex]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
            
            return @"";
            
            
        }
        else if([errorCode integerValue] == 1012)
        {
        
            return @"";
        }
        else if([errorCode integerValue] >= 9000 && [errorCode integerValue] < 10000){
            errorMessage = errorMsg;
        }else{
            errorMessage = [NSString stringWithFormat:@"%@ %@", errorMsg, errorCode];
        }
    }else if([errorDomain isEqualToString:ErrorDomainFacebook]){
        errorMessage = NSLocalizedString(@"Sorry, 5miles is experiencing a technical issue. Please try again later.", nil);
    }
    
    return errorMessage;
}

-(void)startRatingAppRemind:(UIViewController*)viewController
{
    
    PXAlertView * view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Do you enjoy 5miles?",nil)
                                                 message: NSLocalizedString(@"Please take a moment to rate it on the App Store.", nil)
                                             cancelTitle:NSLocalizedString(@"Yes, rate it now.", nil)
                                             otherTitles:@[NSLocalizedString(@"Not really, leave feedback.", nil),NSLocalizedString(@"Cancel",nil)]
                                              completion:^(BOOL cancelled, NSInteger buttonIndex){
                                                  {
                                                      
                                                      
                                                      if (buttonIndex == 0) {
                                                          [HGUserData sharedInstance].bShowRatingAppRemind_FirstReview5star = [HGUserData sharedInstance].bShowRatingAppRemind_MarkAsSold =[HGUserData sharedInstance].bHomeShowRateApp = NO;
                                                          
                                                          [[HGUtils sharedInstance] gaSendEventWithCategory:@"rate5miles_popup" action:@"rate5miles" label:nil value:nil];
                                                          NSString *str = [NSString stringWithFormat: @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%@", @"917554930"];
                                                          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                                                          
                                                      }
                                                      else if(buttonIndex ==1)
                                                      {
                                                          [HGUserData sharedInstance].bShowRatingAppRemind_FirstReview5star = [HGUserData sharedInstance].bShowRatingAppRemind_MarkAsSold =[HGUserData sharedInstance].bHomeShowRateApp = NO;
                                                          
                                                          
                                                          [[HGUtils sharedInstance] gaSendEventWithCategory:@"rate5miles_popup" action:@"leavefeedback" label:nil value:nil];
                                                          
                                                          UINavigationController *navFeedback = [[UIStoryboard profileStoryboard]instantiateViewControllerWithIdentifier:@"NavFeedback"];
                                                          
                                                          [viewController.navigationController presentViewController:navFeedback animated:YES completion:nil];
                                                      }
                                                      else if(buttonIndex ==2)
                                                      {
                                                          //cancel;
                                                      }
                                                      
                                                  }
                                                  
                                              }];
    [view useDefaultIOS7Style];
    
    
}


-(void)startApplicationSetting
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    }
}


-(NSString*)getReviewImageNameByScore:(float)score
{
    float numStars = MIN(5, (float)score);
    int nPart = (int)numStars;
    float fPart = numStars - nPart;
    
    ///0.1~0.4x 是0.5
    ///0.5x~1是进1
    if (fPart < 0.499999 && fPart > 0.1) {
        numStars = nPart + 0.5;
    }
    else if(fPart > 0.5){
        numStars = nPart + 1;
    }
    
    numStars = MIN(5, (float)numStars);
    
    NSString* image_index = [@"icon-feedback-score-" stringByAppendingFormat:@"%.1f", numStars ];
    
    image_index = [image_index stringByReplacingOccurrencesOfString:@".5" withString:@"p5"];
    image_index = [image_index stringByReplacingOccurrencesOfString:@".0" withString:@""];
    
    NSLog(@"the image index is %@", image_index);
    return  image_index ;
}

-(double)getPreferLat
{
    double lat = 0.0;
    if ([HGUserData sharedInstance].zipcodeString.length > 0) {
        lat = [HGUserData sharedInstance].zipLat.doubleValue;
    }else{
        lat = [HGAppData sharedInstance].GPSlat;
    }
    
    return lat;
}

-(double)getPreferLon
{
    double lon = 0.0;
    if ([HGUserData sharedInstance].zipcodeString.length > 0) {
        lon = [HGUserData sharedInstance].zipLon.doubleValue;
    }
    else{
        lon =[HGAppData sharedInstance].GPSlon;
    }
    return lon;
}

- (NSString *)decimalToPercentage:(CGFloat)percentage{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterPercentStyle;
    NSString *strProgress = [formatter stringFromNumber:[NSNumber numberWithDouble:percentage]];
    
    return strProgress;
}


- (UIImage *)compressImage:(UIImage *)image toSize:(CGSize)size withCompressionQuality:(CGFloat)quality{
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = size.height;
    float maxWidth = size.width;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, quality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

-(NSString*)getPreferLanguage
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    
    return preferredLang;
}

@end

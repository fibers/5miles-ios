//
//  HGSysMessage.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-28.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSysMessage.h"

#import "MHPrettyDate.h"

@implementation HGSysMessage

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid", @"sm_type":@"smType"}];
}
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[@"action", @"image", @"desc", @"image_type",@"smType",@"review_score"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    return NO;
}

- (NSString *)senderName
{
    return @"5miles";
}

@end

@implementation HGSysMessageResponseMeta

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"total_count": @"totalCount"}];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[@"next", @"previous"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    return NO;
}

+(void)updateSystemMessageResponseMeta:(HGSysMessageResponseMeta*)destMeta withSourceDeta:(HGSysMessageResponseMeta*)sourceMeta
{
    destMeta.next = sourceMeta.next;
    destMeta.previous = sourceMeta.previous;
    destMeta.limit = sourceMeta.limit;
    destMeta.offset = sourceMeta.offset;
    destMeta.totalCount = sourceMeta.totalCount;
    
}
@end


@implementation HGSysMessageResponse



@end
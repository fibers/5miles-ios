//
//  HGOfferLineHeaderView.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-13.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGOfferLineHeaderView.h"
#import "HGConstant.h"

@implementation HGOfferLineHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = FANCY_COLOR(@"fdfdfd");
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.imageView.clipsToBounds = YES;
        self.imageView.layer.borderWidth = 1.0;
        self.imageView.layer.borderColor = FANCY_COLOR(@"dbdbdb").CGColor;
        self.imageView.layer.cornerRadius = 2.0;
        self.imageView.userInteractionEnabled = YES;
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
         UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTouchItemImage)];
        [self.imageView addGestureRecognizer:tapGesture];
        
        self.naviBackButton = [[UIButton alloc] initWithFrame: CGRectMake(5, 0, 41/2, 34/2)];
        CGPoint centerPoint = CGPointMake(10, 35);
        self.naviBackButton.center = centerPoint;
        [self addSubview:self.naviBackButton];
        self.naviBackButton.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"naviBack"]];
        self.bigNaviBackButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, self.frame.size.height)];
        self.itemContentAreaButton = [[UIButton alloc] initWithFrame:CGRectMake(self.bigNaviBackButton.frame.size.width, 0, self.frame.size.width - self.bigNaviBackButton.frame.size.width, self.frame.size.height)];
        [self addSubview:self.itemContentAreaButton];
        
        [self addSubview:self.bigNaviBackButton];
        [self addSubview:self.imageView];
        
        self.itemTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 200, 20)];
        self.itemTitle.backgroundColor = [UIColor clearColor];
        self.itemTitle.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        self.itemTitle.font = [UIFont systemFontOfSize:14.0];
        self.itemTitle.textAlignment = NSTextAlignmentLeft;
        self.itemTitle.numberOfLines = 2;
        self.itemTitle.lineBreakMode = NSLineBreakByTruncatingTail;
        [self addSubview:self.itemTitle];
        
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, 45, 80, 20)];
        self.priceLabel.backgroundColor = [UIColor clearColor];
        self.priceLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        self.priceLabel.font = [UIFont boldSystemFontOfSize:14.0];
        self.priceLabel.textAlignment = NSTextAlignmentLeft;
        self.priceLabel.numberOfLines = 1;
        [self addSubview:self.priceLabel];
        
        self.ivSeparator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 70, self.frame.size.width, 2)];
        self.ivSeparator.backgroundColor = [UIColor colorWithRed:0xb9/255.0 green:0xb9/255.0 blue:0xb9/255.0 alpha:0.6];
        [self addSubview:self.ivSeparator];
    }
    return self;
}

- (void)layoutSubviews
{
    self.imageView.frame = CGRectMake(30, 12, 44, 44);
    self.itemTitle.frame = CGRectMake(84, 10, 224, 34);
    self.priceLabel.frame = CGRectMake(84, 44, 224, 12);
}

-(void)onTouchItemImage
{
    if ([self.delegate respondsToSelector:@selector(onTouchItemImageAction)]) {
        [self.delegate onTouchItemImageAction];
    }
}


@end

//
//  SectionFooterCollectionReusableView.h
//  Pinnacle
//
//  Created by Alex on 14-11-21.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionFooterCollectionReusableView : UICollectionReusableView

@end

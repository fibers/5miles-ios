//
//  HGGetuiManager.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-2.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGGetuiManager.h"
#import "HGAppData.h"
#import "HGUtils.h"


@interface HGGetuiManager () <GexinSdkDelegate>

@property (retain, nonatomic) NSString *appKey;
@property (retain, nonatomic) NSString *appSecret;
@property (retain, nonatomic) NSString *appID;

@end

@implementation HGGetuiManager

+ (HGGetuiManager *)sharedManager
{
    static HGGetuiManager * _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[HGGetuiManager alloc] init];
    });
    return _sharedInstance;
}

- (void)startSdk
{
    if (!_getuiPusher) {
        _sdkStatus = SdkStatusStoped;
        
        self.appID = GETUI_APP_ID;
        self.appKey = GETUI_APP_KEY;
        self.appSecret = GETUI_APP_SECRET;
        NSLog(@" GEtui id key, secret,%@, %@,%@",self.appID,self.appKey,self.appSecret);
        
        NSError *err = nil;
        _getuiPusher = [GexinSdk createSdkWithAppId:_appID
                                             appKey:_appKey
                                          appSecret:_appSecret
                                         appVersion:@"1.0.0"
                                           delegate:self
                                              error:&err];
        if (!_getuiPusher) {
            NSLog(@"getui push created error:%@", err);
        } else {
            _sdkStatus = SdkStatusStarting;
        }
    }
}

- (void)stopSdk
{
    if (_getuiPusher) {
        [_getuiPusher destroy];
        _getuiPusher = nil;
        
        _sdkStatus = SdkStatusStoped;
        _clientId = nil;
    }
}

- (void)setDeviceToken:(NSString *)aToken
{
    if (![self checkSdkInstance]) {
        return;
    }
    
    [_getuiPusher registerDeviceToken:aToken];
}

- (BOOL)setTags:(NSArray *)aTags error:(NSError **)error
{
    if (![self checkSdkInstance]) {
        return NO;
    }
    
    return [_getuiPusher setTags:aTags];
}

- (NSString *)sendMessage:(NSData *)body error:(NSError **)error {
    if (![self checkSdkInstance]) {
        return nil;
    }
    
    return [_getuiPusher sendMessage:body error:error];
}

#pragma mark - Helpers

- (BOOL)checkSdkInstance
{
    if (!_getuiPusher) {
        [[HGUtils sharedInstance] gaTrackViewName:@"getui sdk fail to start"];
        NSLog(@"Push notification fail. getui pusher is nil ,,fail to start...");
        /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"错误" message:@"SDK未启动" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
         */
        return NO;
    }
    return YES;
}

#pragma mark - GexinSdkDelegate

- (void)GexinSdkDidRegisterClient:(NSString *)clientId
{
    // [4-EXT-1]: 个推SDK已注册
    _sdkStatus = SdkStatusStarted;
    self.clientId = clientId;
}

- (void)GexinSdkDidReceivePayload:(NSString *)payloadId fromApplication:(NSString *)appId
{
    // [4]: 收到个推消息
    _payloadId = payloadId;
    
    NSData *payload = [_getuiPusher retrivePayloadById:payloadId];
    NSString *payloadMsg = nil;
    if (payload) {
        payloadMsg = [[NSString alloc] initWithBytes:payload.bytes
                                              length:payload.length
                                            encoding:NSUTF8StringEncoding];
    }
    NSString *record = [NSString stringWithFormat:@"%d, %@, %@", ++self.lastPayloadIndex, [NSDate date], payloadMsg];
    if (record.length > 0) {
        NSLog(@"getui received payload: %@", record);
    }
}

- (void)GexinSdkDidSendMessage:(NSString *)messageId result:(int)result {
    // [4-EXT]:发送上行消息结果反馈
    NSString *record = [NSString stringWithFormat:@"Received sendmessage:%@ result:%d", messageId, result];
    if (record.length > 0) {
        NSLog(@"getui send message with record: %@", record);
    }
}

- (void)GexinSdkDidOccurError:(NSError *)error
{
    // [EXT]:个推错误报告，集成步骤发生的任何错误都在这里通知，如果集成后，无法正常收到消息，查看这里的通知。
     NSLog(@"getui occured error: %@", [NSString stringWithFormat:@">>>[GexinSdk error]:%@", [error localizedDescription]]);
}

@end

//
//  HGSystemThemeItemCell.h
//  Pinnacle
//
//  Created by Alex on 14-12-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrikeThroughLabel.h"
#import "HGShopItem.h"
@interface HGSystemThemeItemCell : UITableViewCell
@property (strong, nonatomic) UIView *textView;

@property (strong, nonatomic) UIImageView *ImageView;

@property (nonatomic, strong) UILabel* ItemTitle;
@property (nonatomic, strong) UILabel* listPrice;
@property (nonatomic, strong) StrikeThroughLabel* originalPrice;
@property (nonatomic, strong) UIImageView* locationIcon;
@property (nonatomic, strong) UILabel* distanceLabel;

//end of view for text /////

@property (nonatomic, strong) UIView* SeperatorView;



-(void)configCell:(HGShopItem*) itemDetail;;

@end

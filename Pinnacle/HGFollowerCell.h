//
//  HGFollowerCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemLikers.h"
@class HGUser;

@interface HGFollowerCell : UITableViewCell<UIActionSheetDelegate>


@property (nonatomic, strong) UIView* seperatorLine;


@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;

@property (nonatomic, strong) UIButton* followButton;
@property (nonatomic, strong) UIImageView* followIcon;

@property (nonatomic, strong) NSString* uid;

@property (nonatomic, strong) HGUser *userData;
@property (nonatomic, assign) BOOL following;

@property (nonatomic, assign) BOOL isMyFollower;
- (void)configWithObject:(HGUser *)entry withIsMyFollower:(BOOL)isMyFollower;
//-(void)configWithLiker:(ItemLikers*)liker;
@end

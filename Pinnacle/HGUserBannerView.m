//
//  HGUserBannerView.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-31.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGUserBannerView.h"
#import "HGAvatarView.h"
#import "HGUser.h"
#import "HGUtils.h"

@implementation HGUserBannerView
{
    HGUser * _user;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.headView = [[HGAvatarView alloc] initWithFrame:CGRectMake(0, 0, 32, 32) imageLink:nil];
        [self addSubview:self.headView];
        
        self.nameLabel = [[UILabel alloc] init];
        self.nameLabel.backgroundColor = [UIColor clearColor];
        self.nameLabel.textColor = [UIColor lightTextColor];
        self.nameLabel.font = [UIFont systemFontOfSize:16.0];
        self.nameLabel.layer.shadowOffset = CGSizeMake(0.0, 1.0);
        self.nameLabel.layer.shadowColor = [UIColor blackColor].CGColor;
        [self addSubview:self.nameLabel];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame entity:(HGUser *)user
{
    self = [super initWithFrame:frame];
    if (self) {
        _user = user;
        [self _setup];
    }
    return self;
}

- (void)_setup
{
    self.headView = [[HGAvatarView alloc] initWithFrame:CGRectMake(0, 0, 32, 32) imageLink:_user.portraitLink];
    [self addSubview:self.headView];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 0, 160, 32)];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor lightTextColor];
    self.nameLabel.font = [UIFont systemFontOfSize:16.0];
    self.nameLabel.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.nameLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.nameLabel.layer.shadowOpacity = 1.0;
    self.nameLabel.text = _user.displayName;
    CGRect rect = [_user.displayName boundingRectWithSize:self.nameLabel.bounds.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.nameLabel.font} context:NULL];
    self.nameLabel.frame = CGRectMake(36, 0, CGRectGetWidth(rect), 32);
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, CGRectGetMaxX(self.nameLabel.frame) + 6, self.frame.size.height);
    [self addSubview:self.nameLabel];
    
    self.backgroundColor = UIColorFromRGBWithAlpha(0x000000, 0.8);
    self.layer.cornerRadius = CGRectGetHeight(self.headView.bounds) * 0.5;
    self.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.layer.borderWidth = 1.0;
    
    self.tapGesture = [[UITapGestureRecognizer alloc] init];
    [self addGestureRecognizer:self.tapGesture];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  HGItemMapController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-19.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "HGConstant.h"
@class HGShopItem, HGItemDetail;

@interface HGItemMapController : UIViewController

@property (nonatomic, strong) HGShopItem * item;
@property (nonatomic, strong) HGItemDetail * itemDetail;


@property (nonatomic, strong) UIButton* showMylocationButton;
@property (nonatomic, strong) MKCircle *circle;
@end


//
//  HGReportReason.m
//  Pinnacle
//
//  Created by shengyuhong on 15/4/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGReportReason.h"

@implementation HGReportReason

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"uid",
                                                       @"reason_content": @"name"
                                                       }];
}

- (BOOL)isEqual:(id)object{
    HGReportReason *reason = (HGReportReason *)object;
    return self.uid == reason.uid && [self.name isEqualToString:reason.name];
}


@end

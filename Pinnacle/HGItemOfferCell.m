//
//  HGItemOfferCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-21.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemOfferCell.h"
#import "HGOfferLine.h"
#import "HGSeperatorView.h"
#import "UIImage+pureColorImage.h"
#import "HGUtils.h"
@implementation HGItemOfferCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _commonSetup];
}

- (void)_commonSetup
{
    self.backgroundColor = [UIColor blueColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.opaque = YES;
    
    self.lbDesc = [UILabel new];
    self.lbDesc.backgroundColor = [UIColor clearColor];
    self.lbDesc.textColor = [UIColor darkTextColor];
    self.lbDesc.font = [UIFont systemFontOfSize:15.0];
    [self.contentView addSubview:self.lbDesc];
    
    self.lbIndicator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 17/2, 30/2)];
    self.lbIndicator.image = [UIImage getTintedImage:[UIImage imageNamed:@"uitableview_cell_access"] withColor:SYSTEM_DEFAULT_FONT_COLOR_1];
    [self.contentView addSubview:self.lbIndicator];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    self.imageView.clipsToBounds = YES;
    self.imageView.layer.cornerRadius = CGRectGetWidth(self.imageView.bounds) * 0.5;
    [self.contentView addSubview:self.imageView];
    
    
    self.seperatorView = [[HGSeperatorView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.seperatorView];
}

- (void)layoutSubviews
{
    self.imageView.frame = CGRectMake(8, 10, 24, 24);
    self.lbDesc.frame = CGRectMake(CGRectGetMaxX(self.imageView.frame) + 8, 0, CGRectGetWidth(self.bounds) - CGRectGetMaxX(self.imageView.frame) - 50, CGRectGetHeight(self.bounds));
    self.lbIndicator.frame = CGRectMake(CGRectGetWidth(self.bounds) - 25, (self.bounds.size.height - 15) / 2, 17/2, 30/2);
    self.seperatorView.frame = CGRectMake(36, CGRectGetHeight(self.bounds) - 1, CGRectGetWidth(self.bounds) - CGRectGetMinX(self.imageView.frame), 1);
}

- (void)configWithOfferLine:(HGOfferLine *)offerline withSeperatorLineHidder:(BOOL)hidden
{
    self.lbDesc.text = offerline.desc;
    self.lbIndicator.hidden = !offerline.diggable;
    self.imageView.image = [UIImage imageNamed:@"coin"];
    self.seperatorView.hidden = hidden;
    
}

@end

//
//  HGCurrencySelectController.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-15.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGCurrencySelectController.h"
#import "HGAppData.h"
#import "HGUtils.h"

@interface HGCurrencySelectController ()

@property (nonatomic, strong) NSArray * currencies;
@property (nonatomic, strong) UIImageView* selected_image;
@end

@implementation HGCurrencySelectController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"setting_view" action:@"set_currency" label:nil value:nil];
     [[HGUtils sharedInstance] gaTrackViewName:@"currency_view"];
    
    [super viewDidLoad];
    
    self.selected_image = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 40, 14, 65/4, 51/4)];
    self.selected_image.image = [UIImage imageNamed:@"cell-selected-icon"];
    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    self.currencies = [HGAppData sharedInstance].AppInitDataSingle.currencies;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.currencies.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cell_keystring = [@"CurrencyCell" stringByAppendingFormat:@"%ld", (long)indexPath.row ];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_keystring];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cell_keystring];
        
    }
    
    // Configure the cell...
    NSString * currencyCode = [self.currencies objectAtIndex:indexPath.row];
    cell.textLabel.text = currencyCode;
    
    if([[HGUserData sharedInstance].preferredCurrency isEqualToString:currencyCode])
    {
        [cell addSubview:self.selected_image];
    }
    else{
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
     [cell addSubview:self.selected_image];
    NSString * currencyCode = [self.currencies objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"BackToParent" sender:currencyCode];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"BackToParent"]) {
        self.selectedCurrency = sender;
    }
}

@end

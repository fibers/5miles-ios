//
//  HGSearchResultFilter.m
//  test
//
//  Created by Alex on 15-3-5.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "HGSearchResultFilter.h"
#import "HGUtils.h"
#import "HGConstant.h"
#import "HGCategory.h"
#import "HGUITextField.h"
#import "HGSearchResultFilterCell.h"
#import <ReactiveCocoa.h>

@interface HGSearchResultFilter()

@property (nonatomic, strong)UIView* filterHeaderView;

@property(nonatomic, strong)UIButton* button1;
@property(nonatomic, strong)UIButton* button2;
@property(nonatomic, strong)UIButton* button3;
@property(nonatomic, strong)UIButton* button4;

@property(nonatomic, strong)UIImageView* icon1;
@property(nonatomic, strong)UIImageView* icon2;
@property(nonatomic, strong)UIImageView* icon3;
@property(nonatomic, strong)UIImageView* icon4;


@property(nonatomic, strong)UISwitch*verifiedSwitch;
@property(nonatomic, strong)HGUITextField* textField1;
@property(nonatomic, strong)HGUITextField* textField2;




@property(nonatomic, strong)UIView* fileterCategoryLeftBoard;
@property(nonatomic, strong)NSMutableArray* leftCategory1Buttons;
@property(nonatomic, strong)NSArray* leftC1DataArray;
@property(nonatomic, strong)UIView* footView;

/////////////////////////////////////////////////////////////////////////
////strange bug, UIView do not response animation, UITableView works.~~~
@property(nonatomic, strong)UITableView* fileterSubview4;
@property(nonatomic, strong)UITableView* mytableview;


@property(nonatomic, assign) float screenWidth;
@end

@implementation HGSearchResultFilter


static int button1ClickCount = 0;
static int button2ClickCount = 0;
static int button3ClickCount = 0;
static int button4ClickCount = 0;


static int screenHeight = 400;
- (id)init {
    self = [super init];
    if (self) {
        [self commonSetup];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonSetup];
    }
    return self;
}

-(void)clearCategoriesArray
{
    [self.array2 removeAllObjects];
    [self.array2_apply_c1 removeAllObjects];
}

static int SuggestFontSize = 12;
-(void)commonSetup
{
    SuggestFontSize = 12;
    if (SCREEN_WIDTH == 320) {
        SuggestFontSize = 12;
    }
    else{
        SuggestFontSize = 14;
    }
    self.selectedSortedIndex = self.selectedCategoryIndex =  self.selectedDistanceIndex = Filter_index_user_not_selected;
    
    // [UIScreen mainScreen].bounds.size.height
    screenHeight = [UIScreen mainScreen].bounds.size.height - 64;
    self.screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    self.filterTypes = @[NSLocalizedString(@"Distance",nil), NSLocalizedString(@"Category",nil), NSLocalizedString(@"Sort order",nil), NSLocalizedString(@"Filter",nil)];
    
    
    self.backgroundColor = [UIColor clearColor];//FANCY_COLOR(@"00000010");

    self.array1 = [[NSMutableArray alloc] initWithArray:@[@[NSLocalizedString(@"5 miles",nil),[[NSNumber alloc] initWithInt:1]] ,
                                                          @[NSLocalizedString(@"10 miles",nil),[[NSNumber alloc] initWithInt:2]],
                                                          @[NSLocalizedString(@"20 miles",nil),[[NSNumber alloc] initWithInt:3]],
                                                          @[NSLocalizedString(@"30 miles",nil),[[NSNumber alloc] initWithInt:4]],
                                                          @[NSLocalizedString(@"50 miles",nil),[[NSNumber alloc] initWithInt:5]],
                                                          @[NSLocalizedString(@"No limit",nil),[[NSNumber alloc] initWithInt:0]]
                                                        ]];
    
   
    
    self.array2 = [[NSMutableArray alloc] initWithArray:@[]];
    self.array2_apply_c1 = [[NSMutableArray alloc] init];
    
    
    self.array3 = [[NSMutableArray alloc] initWithArray:@[NSLocalizedString(@"Most Relevant (default)",nil),
                                                          NSLocalizedString(@"Closest",nil),
                                                          NSLocalizedString(@"Most Recent",nil),
                                                          NSLocalizedString(@"Lowest Price",nil),
                                                          NSLocalizedString(@"Highest Price",nil)]];
    
    
    
    self.leftCategory1Buttons=  [[NSMutableArray alloc] init];
    
    
    
    [self setupBlackBackView];
    
    [self setupHeadView];
    [self setupTableView];
    [self addFilterCategoryLeftBoard];
    
    
    
    
    [self setUpFilter4Subview];
    [self addSwipeGesture];
}

static int categoryLeftBoardWidth = 80;
-(void)addFilterCategoryLeftBoard
{
    self.fileterCategoryLeftBoard = [[UIView alloc] initWithFrame:CGRectMake(0, buttonHeight, 80, 0)];
    [self addSubview:self.fileterCategoryLeftBoard];
    self.fileterCategoryLeftBoard.backgroundColor = FANCY_COLOR(@"f8f8f8");
    
    NSArray* categoryTitle = @[@"All", @"For Sale", @"Services" ,@"Housing", @"Jobs"];
    for (int i = 0; i< categoryTitle.count ; i++)
    {
        UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0, singleCellHeight*i, categoryLeftBoardWidth, singleCellHeight)];
        btn.tag = i;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [self.fileterCategoryLeftBoard addSubview:btn];
        [btn setTitleColor:SYSTEM_DEFAULT_FONT_COLOR_1 forState:UIControlStateNormal];
        NSString* titleString = [categoryTitle objectAtIndex:i];
        [btn setTitle:titleString forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.leftCategory1Buttons addObject:btn];
        
        [btn addTarget:self action:@selector(onTouchCategory1Button:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    self.fileterCategoryLeftBoard.hidden =YES;
    
}
-(void)reFillLeftCategoryBoardWithArray:(NSArray*)dataArray
{
    
    self.leftC1DataArray = dataArray;
    for (UIView * subview in self.fileterCategoryLeftBoard.subviews) {
        [subview removeFromSuperview];
    }
    
    for (int i = 0; i< dataArray.count ; i++)
    {
        
        UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0, singleCellHeight*i, categoryLeftBoardWidth, singleCellHeight)];
        btn.tag = i;
        [self.fileterCategoryLeftBoard addSubview:btn];
        [btn setTitleColor:SYSTEM_DEFAULT_FONT_COLOR_1 forState:UIControlStateNormal];
        
        
        HGCategory* c1= [dataArray objectAtIndex:i];
        NSString* titleString = c1.title;
        [btn setTitle:titleString forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.leftCategory1Buttons addObject:btn];
        
        if (i==0) {
            [btn setBackgroundColor:[UIColor whiteColor]];
        }
        
        [btn addTarget:self action:@selector(onTouchCategory1Button:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    [self.mytableview reloadData];

    
}

-(void)resetCategory1Buttons
{
    for (int i = 0; i<self.leftCategory1Buttons.count; i++) {
        UIButton* btn = [self.leftCategory1Buttons objectAtIndex:i];
        [btn setBackgroundColor:FANCY_COLOR(@"f8f8f8")];
    }
}
-(void)onTouchCategory1Button:(id)sender
{
    
    
    
    [self resetCategory1Buttons];
    UIButton* btn = (UIButton*)sender;
    [btn setBackgroundColor:[UIColor whiteColor]];
    
    [self.array2_apply_c1 removeAllObjects];
    HGCategory* currentC1 = [self.leftC1DataArray objectAtIndex:btn.tag];
    NSString* currentC1ID = [@"" stringByAppendingFormat:@"%d", currentC1.index];
    if (btn.tag == 0) {
        //default all;
        [self.array2_apply_c1 addObjectsFromArray:self.array2];
    }
    else{
        for (int i = 0; i<self.array2.count; i++) {
            HGCategory* c2 = [self.array2 objectAtIndex:i];
            if ([c2.parent_id isEqualToString:currentC1ID]) {
                [self.array2_apply_c1 addObject:c2];
            }
        }

    }
    
    [self.mytableview reloadData];
    
    
}
-(void)addSwipeGesture
{
    UISwipeGestureRecognizer *recognizer;
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeAction)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionUp)];
    [self addGestureRecognizer:recognizer];
}
-(void)SwipeAction
{
    button1ClickCount=button2ClickCount =button3ClickCount = button4ClickCount= 0;
    [self resetButtonsWithAnimation];
}


-(void)setupBlackBackView
{
    self.blackBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    [self addSubview:self.blackBackground];
    self.blackBackground.hidden = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(filterTap:)];
    [self addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    tapGesture.cancelsTouchesInView = NO;
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
-(void)filterTap:(UITapGestureRecognizer *)gesture
{
     CGPoint point = [gesture locationInView:self];
    NSLog(@"handleSingleTap!pointx:%f,y:%f",point.x,point.y);
    if (self.mytableview.frame.size.height > 0 && self.mytableview.hidden == NO && point.y > CGRectGetMaxY(self.mytableview.frame)) {
        button1ClickCount=button2ClickCount =button3ClickCount = button4ClickCount= 0;
        [self resetButtonsWithAnimation];
    }
    if (self.fileterSubview4.hidden == NO && point.y > CGRectGetMaxY(self.fileterSubview4.frame)) {
        button1ClickCount=button2ClickCount =button3ClickCount = button4ClickCount= 0;
        [self resetButtonsWithAnimation];
    }
}

-(void)setupHeadView
{
    self.filterHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, buttonHeight)];
    self.filterHeaderView.backgroundColor = FANCY_COLOR(@"f8f8f8");
    
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    UIView* seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, buttonHeight-fSeperatorHeight, self.frame.size.width, fSeperatorHeight)];
    [self.filterHeaderView addSubview:seperatorView];
    seperatorView.backgroundColor = FANCY_COLOR(@"d5d5d5");
    
    
    
    [self addSubview:self.filterHeaderView];
    float singleButtonWidth = self.screenWidth/4;
    self.button1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.screenWidth/4, buttonHeight)];
    self.button2 = [[UIButton alloc] initWithFrame:CGRectMake(singleButtonWidth, 0, self.screenWidth/4, buttonHeight)];
    self.button3 = [[UIButton alloc] initWithFrame:CGRectMake(singleButtonWidth*2, 0, self.screenWidth/4, buttonHeight)];
    self.button4 = [[UIButton alloc] initWithFrame:CGRectMake(singleButtonWidth*3, 0, self.screenWidth/4, buttonHeight)];
    
   
    
    self.button1.titleLabel.font = self.button2.titleLabel.font= self.button3.titleLabel.font = self.button4.titleLabel.font = [UIFont systemFontOfSize:SuggestFontSize];
    
    
    
    
    [self.button1 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button2 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button3 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button4 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    
    
    [self.button1 addTarget:self action:@selector(button1Click) forControlEvents:UIControlEventTouchUpInside];
    [self.button2 addTarget:self action:@selector(button2Click) forControlEvents:UIControlEventTouchUpInside];
    [self.button3 addTarget:self action:@selector(button3Click) forControlEvents:UIControlEventTouchUpInside];
    [self.button4 addTarget:self action:@selector(button4Click) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.button1 setTitle:NSLocalizedString(@"Distance",nil) forState:UIControlStateNormal];
    [self.button2 setTitle:NSLocalizedString(@"Category",nil) forState:UIControlStateNormal];
    [self.button3 setTitle:NSLocalizedString(@"Sort order",nil) forState:UIControlStateNormal];
    [self.button4 setTitle:NSLocalizedString(@"Filter",nil) forState:UIControlStateNormal];
    [self.button4 setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    
    [self.button1.titleLabel sizeToFit];
    [self.button2.titleLabel sizeToFit];
    [self.button3.titleLabel sizeToFit];
    [self.button4.titleLabel sizeToFit];
    self.button1.titleLabel.textAlignment = self.button2.titleLabel.textAlignment =
    self.button3.titleLabel.textAlignment = self.button4.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    
    [self.filterHeaderView addSubview:self.button1];
    [self.filterHeaderView addSubview:self.button2];
    [self.filterHeaderView addSubview:self.button3];
    [self.filterHeaderView addSubview:self.button4];
    
    
    [self addSubview:self.filterHeaderView];
    [self addICONs];
}
-(void)addICONs
{
    self.icon1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 18/2, 10/2)];
    self.icon2 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 18/2, 10/2)];
    self.icon3 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 18/2, 10/2)];
    self.icon4 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 22/2, 24/2)];
    
    float singleButtonWidth = self.screenWidth/4;
    
    self.icon1.image = self.icon2.image = self.icon3.image =  [UIImage imageNamed:@"searchFilterArrow1"];
    CGPoint centerPoint = self.button1.center;
    centerPoint.x = centerPoint.x + self.button1.titleLabel.frame.size.width/2 + 9;
    self.icon1.center = centerPoint;
    
    centerPoint = self.button2.center ;
    centerPoint.x = centerPoint.x -singleButtonWidth + self.button2.titleLabel.frame.size.width/2 + 9;
    self.icon2.center = centerPoint;
    
    centerPoint = self.button3.center;
    centerPoint.x = centerPoint.x - singleButtonWidth*2 + self.button3.titleLabel.frame.size.width/2 + 9;
    self.icon3.center = centerPoint;
    
    
    self.icon4.image = [UIImage imageNamed:@"searchFilter4_icon1"];
    centerPoint = self.button4.center;
    centerPoint.x = centerPoint.x -singleButtonWidth*3 - self.button4.titleLabel.frame.size.width/2 -3;
    self.icon4.center = centerPoint;
    
    [self.button1 addSubview:self.icon1];
    [self.button2 addSubview:self.icon2];
    [self.button3 addSubview:self.icon3];
    [self.button4 addSubview:self.icon4];
    
    
    
}
-(void)resetButtonsWithAnimation
{
   
    [self.button1 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button2 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button3 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button4 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    
    self.icon1.image = self.icon2.image = self.icon3.image = [UIImage imageNamed:@"searchFilterArrow1"];
    self.icon4.image = [UIImage imageNamed:@"searchFilter4_icon1"];
    
    
    self.blackBackground.hidden = YES;
    self.frame = CGRectMake(self.frame.origin.x
                            , self.frame.origin.y, self.frame.size.width, buttonHeight);

    [UIView animateWithDuration:0.3 animations:^(){
        self.fileterSubview4.frame =CGRectMake(self.fileterSubview4.frame.origin.x,
                                               self.fileterSubview4.frame.origin.y,
                                               self.fileterSubview4.frame.size.width,
                                               0) ;
        self.mytableview.frame = CGRectMake(self.mytableview.frame.origin.x,
                                            self.mytableview.frame.origin.y,
                                            self.mytableview.frame.size.width,
                                            0) ;
        self.fileterCategoryLeftBoard.frame = CGRectMake(self.fileterCategoryLeftBoard.frame.origin.x,
                                                         self.fileterCategoryLeftBoard.frame.origin.y,
                                                         self.fileterCategoryLeftBoard.frame.size.width,
                                                         0);
        
        for (UIView*subview in [self.fileterCategoryLeftBoard subviews]) {
            subview.hidden = YES;;
        }

    } completion:^(BOOL completed){
        self.fileterSubview4.hidden = YES;
        self.fileterCategoryLeftBoard.hidden = YES;

    }];


    [self.textField1 resignFirstResponder];
    [self.textField2 resignFirstResponder];
    [self.delegate clearSearchField];
}

//clear without animation.
-(void)clearResetButton
{
    
    [self.button1 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button2 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button3 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    [self.button4 setTitleColor:FANCY_COLOR(@"818181") forState:UIControlStateNormal];
    
    self.icon1.image = self.icon2.image = self.icon3.image = [UIImage imageNamed:@"searchFilterArrow1"];
    self.icon4.image = [UIImage imageNamed:@"searchFilter4_icon1"];
    
    
    self.blackBackground.hidden = YES;
    self.fileterSubview4.hidden = YES;
    self.frame = CGRectMake(self.frame.origin.x
                            , self.frame.origin.y, self.frame.size.width, buttonHeight);
    self.mytableview.frame = CGRectMake(self.mytableview.frame.origin.x,
                                        self.mytableview.frame.origin.y,
                                        self.mytableview.frame.size.width,
                                        0) ;
    self.fileterSubview4.frame =CGRectMake(self.fileterSubview4.frame.origin.x,
                                           self.fileterSubview4.frame.origin.y,
                                           self.fileterSubview4.frame.size.width,
                                           0) ;
    
    self.fileterCategoryLeftBoard.frame = CGRectMake(self.fileterCategoryLeftBoard.frame.origin.x,
                                                     self.fileterCategoryLeftBoard.frame.origin.y,
                                                     self.fileterCategoryLeftBoard.frame.size.width,
                                                     0);
    self.fileterCategoryLeftBoard.hidden =YES;

    
    [self.textField1 resignFirstResponder];
    [self.textField2 resignFirstResponder];

}
-(void)button1Click
{
    [self clearResetButton];
    self.currentFilteIndex = filter_distance;
    
    button1ClickCount++;
    button2ClickCount=  button3ClickCount = button4ClickCount = 0;
    if (button1ClickCount % 2 == 1) {
         [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"distanceon" label:nil value:nil];
        
        [self.button1 setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
        self.icon1.image = [UIImage imageNamed:@"searchFilterArrow2"];
        self.frame = CGRectMake(self.frame.origin.x
                                , self.frame.origin.y, self.frame.size.width, screenHeight);
        
        self.mytableview.hidden = NO;
        self.blackBackground.hidden = NO;
        
        [UIView animateWithDuration:0.3 animations:^(){
            self.mytableview.frame = CGRectMake(self.mytableview.frame.origin.x,
                                                self.mytableview.frame.origin.y,
                                                self.mytableview.frame.size.width,
                                                singleCellHeight*self.array1.count + FooterViewHeight) ;
            
        } completion:^(BOOL finished){
        }];
       
        

        [self.mytableview reloadData];
    }
    else{
        self.frame = CGRectMake(self.frame.origin.x
                               , self.frame.origin.y, self.frame.size.width, buttonHeight);
        
        [UIView animateWithDuration:0.3 animations:^(){
            self.mytableview.frame = CGRectMake(self.mytableview.frame.origin.x,
                                                self.mytableview.frame.origin.y,
                                                self.mytableview.frame.size.width,
                                                0) ;
            
        } completion:^(BOOL finished){
            //self.mytableview.hidden = YES;
            self.blackBackground.hidden = YES;
        }];
        

       
    }
    
}
-(void)button2Click
{
    
    [self clearResetButton];
    
    
    
    button2ClickCount++;
    self.currentFilteIndex = filter_category;
    button1ClickCount = button3ClickCount = button4ClickCount = 0;
    if (button2ClickCount %2 == 1) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"categoryon" label:nil value:nil];

        [self.button2 setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
        self.icon2.image = [UIImage imageNamed:@"searchFilterArrow2"];
        self.frame = CGRectMake(self.frame.origin.x
                                , self.frame.origin.y, self.frame.size.width, screenHeight);
       
        self.mytableview.hidden = NO;
        self.blackBackground.hidden = NO;
        
        
        [UIView animateWithDuration:0.3 animations:^(){
            
          
            int nCount = (int)MAX((int)self.leftC1DataArray.count, (int)self.array2_apply_c1.count);
            float tableMaxHeight = singleCellHeight*nCount +FooterViewHeight;
            
            if (tableMaxHeight> singleCellHeight*7.5 + FooterViewHeight) {
                tableMaxHeight  = singleCellHeight*7.5 + FooterViewHeight;
            }
            
            self.fileterCategoryLeftBoard.frame = CGRectMake(self.fileterCategoryLeftBoard.frame.origin.x,
                                                             self.fileterCategoryLeftBoard.frame.origin.y,
                                                             self.fileterCategoryLeftBoard.frame.size.width,
                                                             tableMaxHeight-FooterViewHeight);
            
            self.mytableview.frame = CGRectMake(self.mytableview.frame.origin.x,
                                                self.mytableview.frame.origin.y,
                                                self.mytableview.frame.size.width,
                                                tableMaxHeight) ;
            
            
            self.fileterCategoryLeftBoard.hidden = NO;
            for (UIView*subview in [self.fileterCategoryLeftBoard subviews]) {
                subview.hidden = NO;
            }
            
        } completion:^(BOOL finished){
           
            
        }];
        
        [self.mytableview reloadData];
    }
    else{
        self.frame = CGRectMake(self.frame.origin.x
                                , self.frame.origin.y, self.frame.size.width, buttonHeight);
        [UIView animateWithDuration:0.3 animations:^(){
            self.mytableview.frame = CGRectMake(self.mytableview.frame.origin.x,
                                                self.mytableview.frame.origin.y,
                                                self.mytableview.frame.size.width,
                                                0) ;
            
             self.fileterCategoryLeftBoard.frame = CGRectMake(self.fileterCategoryLeftBoard.frame.origin.x,
                                                              self.fileterCategoryLeftBoard.frame.origin.y,
                                                              self.fileterCategoryLeftBoard.frame.size.width,
                                                              0);
            
        } completion:^(BOOL finished){
            //self.mytableview.hidden = YES;
            self.blackBackground.hidden = YES;
        }];
    }

    
}
-(void)button3Click
{
   
    [self clearResetButton];
     button3ClickCount ++ ;
    self.currentFilteIndex = filter_sortorder;
    button1ClickCount = button2ClickCount = button4ClickCount = 0;
    if (button3ClickCount % 2 == 1) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"sortorderon" label:nil value:nil];

        [self.button3 setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
        self.icon3.image = [UIImage imageNamed:@"searchFilterArrow2"];
        self.frame = CGRectMake(self.frame.origin.x
                                , self.frame.origin.y, self.frame.size.width, screenHeight);
        
        self.mytableview.hidden = NO;
        self.blackBackground.hidden = NO;
        
        [UIView animateWithDuration:0.3 animations:^(){
            self.mytableview.frame = CGRectMake(self.mytableview.frame.origin.x,
                                                self.mytableview.frame.origin.y,
                                                self.mytableview.frame.size.width,
                                                singleCellHeight*self.array3.count + FooterViewHeight) ;
        }];
        
        [self.mytableview reloadData];
    }
    else{
        self.frame = CGRectMake(self.frame.origin.x
                                , self.frame.origin.y, self.frame.size.width, buttonHeight);
        [UIView animateWithDuration:0.3 animations:^(){
            self.mytableview.frame = CGRectMake(self.mytableview.frame.origin.x,
                                                self.mytableview.frame.origin.y,
                                                self.mytableview.frame.size.width,
                                                0) ;
            
        } completion:^(BOOL finished){
            //self.mytableview.hidden = YES;
            self.blackBackground.hidden = YES;
        }];

    }

}
-(void)button4Click
{
   
    [self clearResetButton];
     button4ClickCount ++ ;
    self.currentFilteIndex = filter_filter4;
    button1ClickCount = button2ClickCount = button3ClickCount = 0;
    if (button4ClickCount % 2 == 1) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"filteron" label:nil value:nil];

        [self.button4 setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
        self.icon4.image = [UIImage imageNamed:@"searchFilter4_icon2"];
        self.frame = CGRectMake(self.frame.origin.x
                                , self.frame.origin.y, self.frame.size.width, screenHeight);
        
        self.fileterSubview4.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^(){
            self.fileterSubview4.frame = CGRectMake(self.fileterSubview4.frame.origin.x,
                                                self.fileterSubview4.frame.origin.y,
                                                self.fileterSubview4.frame.size.width,
                                                filterSubviewHeight) ;
        }];
    }
    else{
        
        self.frame = CGRectMake(self.frame.origin.x
                                , self.frame.origin.y, self.frame.size.width, buttonHeight);
        [UIView animateWithDuration:0.3 animations:^(){
            self.fileterSubview4.frame = CGRectMake(self.fileterSubview4.frame.origin.x,
                                                self.fileterSubview4.frame.origin.y,
                                                self.fileterSubview4.frame.size.width,
                                                0) ;
            
        } completion:^(BOOL finished){
            self.blackBackground.hidden = YES;
            self.fileterSubview4.hidden = YES;
        }];
       
        
    }

    
}
-(void)setupTableView
{
    self.mytableview = [[UITableView alloc] initWithFrame:CGRectMake(0, buttonHeight, self.frame.size.width, 0)];
    self.mytableview.delegate = self;
    self.mytableview.dataSource = self;
    self.mytableview.showsHorizontalScrollIndicator = NO;
    self.mytableview.showsVerticalScrollIndicator = NO;
    //self.mytableview.backgroundColor = [UIColor clearColor];
    self.mytableview.bounces = NO;
    [self addSubview:self.mytableview];
    self.mytableview.hidden = YES;
    [self addTableFootView];
}
-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        //showSwitchValue.text = @"是";
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"verifyrefineon" label:nil value:nil];

    }else {
        //showSwitchValue.text = @"否";
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"verifyrefineoff" label:nil value:nil];

    }
}
static int filterSubviewHeight = 46*2 + 62;
-(void)setUpFilter4Subview
{
    self.fileterSubview4 = [[UITableView alloc] initWithFrame:CGRectMake(0, buttonHeight, self.frame.size.width, filterSubviewHeight)];
    self.fileterSubview4.separatorStyle = UITableViewCellSelectionStyleNone;
    
    
    
    UIView* view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 46)];
    view1.backgroundColor = FANCY_COLOR(@"f8f8f8");
    UIView* seperatorView = [[UIView alloc] initWithFrame:CGRectMake(12, 45, self.frame.size.width, 0.5)];
    [view1 addSubview:seperatorView];
    seperatorView.backgroundColor = FANCY_COLOR(@"d5d5d5");
    
    UILabel* label1 = [[UILabel alloc] initWithFrame:CGRectMake(14, 10, 200, 25)];
    label1.textColor = [UIColor blackColor];
    [view1 addSubview:label1];
    label1.font = [UIFont systemFontOfSize:14];
    label1.text = NSLocalizedString(@"Only see verified sellers",nil);
    
    self.verifiedSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.screenWidth - 50 -10 , 10, 50, 30)];
    [view1 addSubview:self.verifiedSwitch];
    [self.verifiedSwitch setOnTintColor:FANCY_COLOR(@"ff8830")];
    
    [self.verifiedSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    
    
    
    UIView* view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + 46 , self.frame.size.width, 46)];
    view2.backgroundColor = FANCY_COLOR(@"f8f8f8");
    UIView* seperatorView2 = [[UIView alloc] initWithFrame:CGRectMake(12, 45, self.frame.size.width, 0.5)];
    [view2 addSubview:seperatorView2];
    seperatorView2.backgroundColor = FANCY_COLOR(@"d5d5d5");
    UILabel* label2 = [[UILabel alloc] initWithFrame:CGRectMake(14, 10, 100, 25)];
    label2.textColor = [UIColor blackColor];
    [view2 addSubview:label2];
    label2.font = [UIFont systemFontOfSize:14];
    label2.text = NSLocalizedString(@"Price range",nil);
    self.textField1 = [[HGUITextField alloc] initWithFrame:CGRectMake(self.frame.size.width - (320- 144), 10, 72, 26)];
    self.textField2 = [[HGUITextField alloc] initWithFrame:CGRectMake( self.frame.size.width - (320-(144+72+20)), 10, 72, 26)];
    self.textField1.font =  self.textField2.font =[UIFont systemFontOfSize:12];
    
    self.textField1.backgroundColor =   self.textField2.backgroundColor = [UIColor whiteColor];
    self.textField1.layer.borderColor=  self.textField2.layer.borderColor = FANCY_COLOR(@"8a8a8a").CGColor;
    self.textField1.layer.borderWidth =  self.textField2.layer.borderWidth = 1;
    self.textField1.layer.cornerRadius =  self.textField2.layer.cornerRadius = 2;
    self.textField1.delegate =  self.textField2.delegate = self;
    UILabel* priceRangeSymbol = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - (320-(144+72+5)),16, 10, 10)];
    self.textField1.textAlignment =  self.textField2.textAlignment= NSTextAlignmentLeft;
    
    self.textField1.edgeInsets = self.textField2.edgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
    self.textField1.keyboardType = self.textField2.keyboardType = UIKeyboardTypeNumberPad;
    priceRangeSymbol.text=@"~";
    priceRangeSymbol.textColor= SYSTEM_DEFAULT_FONT_COLOR_1;
    [view2 addSubview:priceRangeSymbol];
    [view2 addSubview: self.textField1];
    [view2 addSubview: self.textField2];
    

    
    
    UIView* view3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + 46*2, self.frame.size.width, 62)];
    view3.backgroundColor = FANCY_COLOR(@"f8f8f8");
    UIButton* confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(160-53, 31-15, 106, 30)];
    confirmButton.layer.cornerRadius = 6;
    confirmButton.layer.borderWidth = 1;
    confirmButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    [confirmButton setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [confirmButton setTitle:NSLocalizedString(@"Confirm",nil) forState:UIControlStateNormal];
    confirmButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [view3 addSubview:confirmButton];
    
    CGPoint centerpoint = CGPointMake(self.frame.size.width/2, confirmButton.center.y);
    confirmButton.center = centerpoint;
    [confirmButton addTarget:self action:@selector(confirmButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.fileterSubview4 addSubview:view1];
    [self.fileterSubview4 addSubview:view2];
    [self.fileterSubview4 addSubview:view3];
    
    self.fileterSubview4.hidden = YES;
    
    [self addSubview:self.fileterSubview4];
    
    
}

-(void)confirmButtonAction
{
    //self.verifiedSwitch.on
    button1ClickCount = button2ClickCount = button3ClickCount = button4ClickCount = 0;
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"filerconfirm" label:nil value:nil];

    self.VerifiedUser = self.verifiedSwitch.on?1:0;
    
    if (self.textField1.text.length > 0) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"minprice" label:nil value:nil];
        self.priceRangelow = self.textField1.text.floatValue;
    }
    else
    {
        self.priceRangelow = 0;
    }
    
    if (self.textField2.text.length > 0 ) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"maxprice" label:nil value:nil];
        self.priceRangeHigh = self.textField2.text.floatValue;
    }
    else{
        self.priceRangeHigh = 0;
    }

    if (self.selectedCategoryIndex == Filter_index_user_not_selected
        && self.selectedDistanceIndex == Filter_index_user_not_selected
        && self.selectedSortedIndex == Filter_index_user_not_selected
        && self.VerifiedUser == 0 && self.priceRangeHigh == 0 && self.priceRangelow == 0)
    {
        //not any filter value, do nothing.        
        
    }
    else
    {
        [self.delegate StartFilterSearch];
    }
    [self resetButtonsWithAnimation];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (self.currentFilteIndex) {
        case filter_distance:
        {
            return self.array1.count;
            break;
        }
        case filter_category:
        {
            
            int nCount = (int)MAX((int)self.leftC1DataArray.count, (int)self.array2_apply_c1.count);
            return nCount;
            break;
            
            
        }
        case filter_sortorder:
        {
            return self.array3.count;
            break;
        }
        default:
            return 0;
            break;
    }
}

-(int)getCurrentSelectedFilterIndex
{
    switch (self.currentFilteIndex) {
        case filter_distance:
            return self.selectedDistanceIndex;
            break;
        case filter_category:
            return self.selectedCategoryIndex;
            break;
        case filter_sortorder:
            return self.selectedSortedIndex;
            break;
            
        default:
            return self.selectedDistanceIndex;
            break;
    }
}
-(NSMutableArray*)getCurrentDataArray
{
    switch (self.currentFilteIndex) {
        case filter_distance:
            return self.array1;
            break;
        case filter_category:
            return self.array2_apply_c1;
            break;
        case filter_sortorder:
            return self.array3;
            break;
            
        default:
            return self.array1;
            break;
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return singleCellHeight;
}


static int singleCellHeight = 40;
static int FooterViewHeight = 18;
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return FooterViewHeight;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray* dataArray = [self getCurrentDataArray];
    int currentFilterSelectedIndex = [self getCurrentSelectedFilterIndex];
    if (self.currentFilteIndex == filter_category) {
        
        HGSearchResultFilterCell *cell=[[HGSearchResultFilterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
        if (indexPath.row < dataArray.count) {
            HGCategory* category = [dataArray objectAtIndex:indexPath.row];
            cell.titleString.text = category.title;
            if (currentFilterSelectedIndex == category.index) {
                cell.titleString.textColor = SYSTEM_MY_ORANGE;
            }
            else{
                cell.titleString.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
            }
        }
        else
        {
            cell.titleString.text = @"";
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        UITableViewCell *cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
      
        
        if(self.currentFilteIndex == filter_distance)
        {
            NSArray* array  = [dataArray objectAtIndex:indexPath.row];
            cell.textLabel.text = [array objectAtIndex:0];
            NSNumber* distanceIndex = [array objectAtIndex:1];
            if (currentFilterSelectedIndex == distanceIndex.intValue) {
                cell.textLabel.textColor = SYSTEM_MY_ORANGE;
            }
            else{
                cell.textLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
            }
        }
        else{
            cell.textLabel.text= [dataArray objectAtIndex:indexPath.row];
            if (currentFilterSelectedIndex == indexPath.row) {
                cell.textLabel.textColor = SYSTEM_MY_ORANGE;
            }
            else{
                cell.textLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
            }
        }
        cell.backgroundColor = FANCY_COLOR(@"f8f8f8");
        
        
        cell.textLabel.font =[UIFont systemFontOfSize:SuggestFontSize];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    
}


-(void)addTableFootView
{
    @weakify(self);
    [RACObserve(self.mytableview, frame ) subscribeNext:^(id x) {
        @strongify(self);
        if (self.mytableview.frame.size.height > 0) {
            self.footView.hidden = NO;
        }
        else
        {
            self.footView.hidden = YES;
        }
        self.footView.frame =CGRectMake(0, CGRectGetMaxY(self.mytableview.frame)-FooterViewHeight, self.frame.size.width, FooterViewHeight);
        
    }];
    
    
    self.footView = [[UIView alloc] initWithFrame:CGRectMake(0, self.mytableview.frame.size.height - FooterViewHeight, self.frame.size.width, FooterViewHeight)];
    self.footView.backgroundColor = FANCY_COLOR(@"f8f8f8");
    
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    UIView* seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, fSeperatorHeight)];
    seperatorView.backgroundColor = FANCY_COLOR(@"d5d5d5");
    [self.footView addSubview:seperatorView];
    
    UIImageView* icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 34/2, 8/2)];
    CGPoint centerPoint = CGPointMake(SCREEN_WIDTH/2, 9);
    icon.center = centerPoint;
    
    
    icon.image = [UIImage imageNamed:@"searchFilter_footerView_icon"];
    [self.footView addSubview:icon];
    [self addSubview:self.footView];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, FooterViewHeight)];
    footerView.backgroundColor = [UIColor clearColor];
    
    
    
    
    
    return footerView;
    
}
-(NSString*)getMaxLengthString:(NSString*)inputString
{
    if (inputString.length > 10) {
        inputString = [inputString substringToIndex: 7];
        inputString = [inputString stringByAppendingString:@".."];
    }
    return  inputString;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    button1ClickCount = button2ClickCount = button3ClickCount  = button4ClickCount = 0;
    if (self.currentFilteIndex == filter_distance) {
        
       
        
        
        NSArray* array = [self.array1 objectAtIndex:indexPath.row];
        NSString* titlestring  = [self getMaxLengthString:[array objectAtIndex:0]];
        
        NSNumber * index = [array objectAtIndex:1];
        self.selectedDistanceIndex =index.intValue;
        
        [self.button1 setTitle:titlestring forState:UIControlStateNormal];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"distancerefine" label:nil value:nil];
        
    }
    
    if (self.currentFilteIndex == filter_sortorder) {
        self.selectedSortedIndex = (int)indexPath.row;
        NSString* titlestring = [self.array3 objectAtIndex:indexPath.row];
        titlestring = [self getMaxLengthString:titlestring];
        
        [self.button3 setTitle:titlestring forState:UIControlStateNormal];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"sortorderrefine" label:nil value:nil];
        
    }
    if (self.currentFilteIndex == filter_category) {
        
        if (indexPath.row < self.array2_apply_c1.count) {
            HGCategory*category = [self.array2_apply_c1 objectAtIndex:indexPath.row];
            self.selectedCategoryIndex = category.index;
            self.selectedCategoryString = category.title;
            
            NSString* titlestring = category.title;
            titlestring = [self getMaxLengthString:titlestring];
            
            [self.button2 setTitle:titlestring forState:UIControlStateNormal];
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"categoryrefine" label:nil value:nil];
        }
        else
        {
            //do nothing
            return;
        }
        
    }
    
    self.VerifiedUser = self.verifiedSwitch.on?1:0;
    
    if (self.textField1.text.length > 0) {
        self.priceRangelow = self.textField1.text.floatValue;
    }
    else
    {
        self.priceRangelow = 0;
    }
    
    if (self.textField2.text.length > 0 ) {
        self.priceRangeHigh = self.textField2.text.floatValue;
    }
    else{
        self.priceRangeHigh = 0;
    }
    [self.delegate StartFilterSearch];
    [self resetButtonsWithAnimation];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#define NUMBERS @"0123456789."

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // return NO to not change text
   {
       NSCharacterSet*cs;
       cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
        NSString*filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL basicTest = [string isEqualToString:filtered];
        if(!basicTest) {
             return NO;
        }
    }
    return YES;
}

@end

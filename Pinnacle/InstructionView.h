//
//  InstructionView.h
//  Pinnacle
//
//  Created by Alex on 15-4-2.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGUserVerifyHintView.h"
@class HGTriangleUpView;

@interface InstructionView : UIView


@property (nonatomic, assign) int TriangleType;
@property (nonatomic, strong)UIView* contentView;
@property (nonatomic, strong) HGTriangleUpView* triangleViewHintUp;
@property (nonatomic, strong) HGTriangleDownView* triangleViewHintDown;



-(void)startInitView;
@end

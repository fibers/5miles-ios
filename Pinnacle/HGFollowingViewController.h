//
//  HGFollowingViewController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGUserListViewController.h"
#import "HGFollowingCell.h"
@interface HGFollowingViewController : HGUserListViewController<HGFollowingCellDelegate>



@end

//
//  HGItemBrandViewController.m
//  Pinnacle
//
//  Created by Alex on 14-9-30.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemBrandViewController.h"
#import "HGAppData.h"
#import "HGUtils.h"
@interface HGItemBrandViewController ()

@end
#define RGBACOLOR(R,G,B,A) [UIColor colorWithRed:(R)/255.0f green:(G)/255.0f blue:(B)/255.0f alpha:(A)]

@implementation HGItemBrandViewController

- (void)viewDidLoad {
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"brand_select" label:nil value:nil];
    
    [[HGUtils sharedInstance] gaTrackViewName:@"brandselect_view"];
    
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Select a brand", nil);
    
    [self initData];
    [self initUI];
    self.tableView.separatorStyle =  UITableViewCellSeparatorStyleSingleLine;
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.mySearchBar becomeFirstResponder];
}
-(void)initData
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"5milesBrand" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    NSArray* brandsArray = [data valueForKey:@"brandsArray"];
    
    self.allItems = [[NSMutableArray alloc] init];
    self.searchResults = [[NSMutableArray alloc] init];
    for (int i =0; i<brandsArray.count; i++) {
        [self.allItems addObject:[brandsArray objectAtIndex:i]];
    }
    
    [self.allItems addObjectsFromArray:[HGUserData sharedInstance].userLocalBrand];
}

-(void)initUI
{
    
    self.mySearchBar = [[UISearchBar alloc] init];
    self.mySearchBar.delegate = self;
    [self.mySearchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.mySearchBar sizeToFit];
    self.tableView.tableHeaderView = [[UIView alloc] init];
    self.tableView.tableHeaderView = self.mySearchBar;
    self.mySearchBar.backgroundColor = RGBACOLOR(45,249,249,1);
    
    self.mySearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.mySearchBar contentsController:self];
    self.mySearchDisplayController.delegate = self;
    self.mySearchDisplayController.searchResultsDataSource = self;
    self.mySearchDisplayController.searchResultsDelegate = self;
    
    [self.searchDisplayController.searchResultsTableView reloadData];
    
    
    
}



#pragma mark UISearchBar and UISearchDisplayController Delegate Methods
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    //準備搜尋前，把上面調整的TableView調整回全屏幕的狀態，如果要產生動畫效果，要另外執行animation代碼
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    //搜尋結束後，恢復原狀，如果要產生動畫效果，要另外執行animation代碼
    //self.myTableView.frame = CGRectMake(60, 40, 260, self.myTableView.frame.size.height);
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"brandselect_view" action:@"brand_search" label:nil value:nil];
    return YES;
}



static bool bSearchResultEmpty = false;
- (void)filterContentForSearchText:(NSString*)searchText  scope:(NSString*)scop
{
    
    bSearchResultEmpty = false;
    
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",searchText];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH [cd] %@",searchText];
    [self.searchResults removeAllObjects];
    [self.searchResults addObjectsFromArray: [self.allItems filteredArrayUsingPredicate:pred]];
    
    if (self.searchResults.count == 0) {
        bSearchResultEmpty = true;
        [self.searchResults addObject:searchText];
    }
    else{
        [self.searchResults insertObject:searchText atIndex:0];
    }
    [self.tableView reloadData];
    
}
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    //一旦SearchBar輸入內容有變化，則執行這個方法，詢問要不要重裝searchResultTableView的數據
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles]
     objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
   
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    //一旦Scope Button有變化，則執行這個方法，詢問要不要重裝searchResultTableView的數據
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles]
    objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}



- (NSInteger)tableView:(UITableView *)tableView   numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rows = 0;
    if ([tableView           isEqual:self.mySearchDisplayController.searchResultsTableView]){
        rows = [self.searchResults count];
    }else{
        rows = [self.allItems count];
    }
    return rows;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault                   reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    /* Configure the cell. */
    if ([tableView isEqual:self.mySearchDisplayController
         .searchResultsTableView]){
        cell.textLabel.text = [self.searchResults objectAtIndex:indexPath.row];
        if (bSearchResultEmpty) {
            cell.textLabel.text = [@"" stringByAppendingFormat: NSLocalizedString(@" + Add '%@' as new brand",nil), cell.textLabel.text ];
        }else{
            if (indexPath.row==0) {
                cell.textLabel.text = [@"" stringByAppendingFormat: NSLocalizedString(@" + Add '%@' as new brand",nil), cell.textLabel.text ];
            }
        }

    }else{
        
        cell.textLabel.text = [self.allItems objectAtIndex:indexPath.row];
        
    }
    
    ////////notice: because, plist do not support char "&", we are using "#" instead, ,so we need to convert back here
    cell.textLabel.text = [cell.textLabel.text stringByReplacingOccurrencesOfString:@"#" withString:@"&"];
    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString* brandString = @"";
    if ([tableView isEqual:self.mySearchDisplayController
         .searchResultsTableView]){
        brandString = [self.searchResults objectAtIndex:indexPath.row];
    }else{
        brandString = [self.allItems objectAtIndex:indexPath.row];
    }
    [HGAppData sharedInstance].sellingItemBrandString = brandString;
    
    NSMutableArray *localBrand = [[HGUserData sharedInstance].userLocalBrand mutableCopy];
    
    if (bSearchResultEmpty) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"brandselect_view" action:@"brand_add" label:nil value:nil];
        [localBrand addObject:brandString];
    }
    else{
        if (indexPath.row==0) {
            [localBrand addObject:brandString];
        }
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"brandselect_view" action:@"brandselectconfirm" label:nil value:nil];
    }
    [HGUserData sharedInstance].userLocalBrand = localBrand;
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if ([HGAppData sharedInstance].sellingItemBrandString != nil && [HGAppData sharedInstance].sellingItemBrandString.length > 0) {
        
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"brandselect_view" action:@"brand_back" label:nil value:nil];
    }
}


@end

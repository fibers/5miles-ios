//
//  HGSellPageInputDataManager.m
//  Pinnacle
//
//  Created by zhenyonghou on 15/7/3.
//  Copyright © 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGSellPageInputDataManager.h"
#import "HGUserData.h"

@implementation HGSellPageInputData

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_title forKey:@"title"];
    [aCoder encodeObject:_listingPrice forKey:@"listingPrice"];
    [aCoder encodeObject:_brand forKey:@"brand"];
    [aCoder encodeObject:_category forKey:@"category"];
    [aCoder encodeObject:_delivery forKey:@"delivery"];
    [aCoder encodeObject:_originalPrice forKey:@"originalPrice"];
    [aCoder encodeInteger:_categoryId forKey:@"categoryId"];
    [aCoder encodeInteger:_deliveryType forKey:@"deliveryType"];
    [aCoder encodeObject:_selectedAssetUrls forKey:@"assetUrls"];
    [aCoder encodeObject:_itemDescription forKey:@"desc"];
    [aCoder encodeObject:_mediaLink forKey:@"mediaLink"];
    
    [aCoder encodeInteger:_rootCategoryId forKey:@"rootCategoryId"];
    [aCoder encodeObject:_country forKey:@"country"];
    [aCoder encodeObject:_city forKey:@"city"];
    [aCoder encodeObject:_region forKey:@"region"];
}

- (id)init {
    if (self = [super init]) {
        _categoryId = -1;
        _deliveryType = 0;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [self init]) {
        _title = [aDecoder decodeObjectForKey:@"title"];
        _listingPrice = [aDecoder decodeObjectForKey:@"listingPrice"];
        _brand = [aDecoder decodeObjectForKey:@"brand"];
        _category = [aDecoder decodeObjectForKey:@"category"];
        _delivery = [aDecoder decodeObjectForKey:@"delivery"];
        _originalPrice = [aDecoder decodeObjectForKey:@"originalPrice"];
        _mediaLink = [aDecoder decodeObjectForKey:@"mediaLink"];

        if ([aDecoder containsValueForKey:@"categoryId"]) {
            _categoryId = [aDecoder decodeIntegerForKey:@"categoryId"];
        } else {
            _categoryId = -1;
        }

        if ([aDecoder containsValueForKey:@"deliveryType"]) {
            _deliveryType = [aDecoder decodeIntegerForKey:@"deliveryType"];
        } else {
            _deliveryType = 0;
        }

        _itemDescription = [aDecoder decodeObjectForKey:@"desc"];

        _selectedAssetUrls = [aDecoder decodeObjectForKey:@"assetUrls"];
        
        _rootCategoryId = [aDecoder decodeIntegerForKey:@"rootCategoryId"];
        _country = [aDecoder decodeObjectForKey:@"country"];
        _city = [aDecoder decodeObjectForKey:@"city"];
        _region = [aDecoder decodeObjectForKey:@"region"];
    }
    return self;
}

@end


@implementation HGSellPageInputDataManager

+ (HGSellPageInputData *)loadFromLocal {
    HGSellPageInputData *inputData = [NSKeyedUnarchiver unarchiveObjectWithFile:[[self class] pathForSave]];
    return inputData;
}

+ (void)saveToLocalWithInputData:(HGSellPageInputData *)inputData {
    if (inputData) {
        [NSKeyedArchiver archiveRootObject:inputData toFile:[[self class] pathForSave]];
    }
}

+ (void)removeData
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[[self class] pathForSave]]) {
        [[NSFileManager defaultManager] removeItemAtPath:[[self class] pathForSave] error:NULL];
    }
}

+ (NSString *)pathForSave
{
    NSArray  *searchPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [searchPath objectAtIndex:0];
    NSString *savePath = [NSString stringWithFormat:@"%@/sell_input_%@", documentPath, [HGUserData sharedInstance].currentUser.uid];
    return savePath;
}

@end

//
//  HGRoundCornerImageView.m
//  Pinnacle
//
//  Created by zhenyonghou on 15/6/26.
//  Copyright © 2015年 The Third Rock Ltd. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "HGRoundCornerImageView.h"

@interface HGRoundCornerImageView()

@property (nonatomic, strong, readwrite) UIImageView *coverImageView;

@end

@implementation HGRoundCornerImageView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit {
    self.contentMode = UIViewContentModeScaleAspectFit;
    
    _coverImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _coverImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_coverImageView];
}

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage
{
    [self sd_setImageWithURL:url placeholderImage:placeholderImage];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _coverImageView.frame = self.bounds;
}

@end


#pragma mark- HGAvararImageView

@interface HGAvararImageView()

@property (nonatomic, strong, readwrite) UIImageView *verifyImageView;

@end

@implementation HGAvararImageView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitWithVerifySize:CGSizeMake(0, 0)];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame verifySize:(CGSize)verifySize {
    if (self = [super initWithFrame:frame]) {
        [self commonInitWithVerifySize:CGSizeMake(verifySize.width, verifySize.height)];
    }
    
    return self;
}

- (void)commonInitWithVerifySize:(CGSize)verifySize {
    _verifyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, verifySize.width, verifySize.height)];
    [self addSubview:_verifyImageView];
}

- (void)setVerifySize:(CGSize)verifySize
{
    _verifyImageView.frame = CGRectMake(_verifyImageView.frame.origin.x, _verifyImageView.frame.origin.y, verifySize.width, verifySize.height);
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize verifyImageSize = _verifyImageView.bounds.size;
    
    _verifyImageView.frame = CGRectMake(self.bounds.size.width - verifyImageSize.width, self.bounds.size.height - verifyImageSize.height, verifyImageSize.width, verifyImageSize.height);
}

@end

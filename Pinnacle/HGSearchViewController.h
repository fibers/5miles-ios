//
//  HGSearchViewController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-19.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGSearchViewController : UIViewController <UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *myCategories;
@property (nonatomic, strong) UISearchBar * searchBar;

-(void)performSegue:(id)sender withSegueName:(NSString*)segueName;

@end

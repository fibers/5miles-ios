//
//  HGSearchResultFilter.h
//  test
//
//  Created by Alex on 15-3-5.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HGSearchFilterDelegate <NSObject>

-(void)StartFilterSearch;
-(void)clearSearchField;
-(void)onTouchLeftCategory1Button:(UIButton*)btn;
@end


@interface HGSearchResultFilter : UIView<UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate, UITextFieldDelegate>

typedef enum {
    filter_distance=1,
    filter_category=2,
    filter_sortorder=3,
    filter_filter4 = 4,
}FILTERTYPE;

#define buttonHeight 43
#define Filter_index_user_not_selected -2

/////////////////////////////////////////////////////////
//usnig -2 as the default not selected value.

@property(nonatomic, assign)int selectedDistanceIndex;
@property(nonatomic, assign)int selectedCategoryIndex;
@property(nonatomic, assign)int selectedSortedIndex;

/////////////////////////////////////////////////////////


@property(nonatomic, assign)int VerifiedUser;
@property(nonatomic, assign)float priceRangelow;
@property(nonatomic, assign)float priceRangeHigh;

@property(nonatomic, strong)NSString* selectedCategoryString;

@property(nonatomic, strong)NSArray* filterTypes;
@property(nonatomic, strong)UIView* blackBackground;
@property(nonatomic, assign)FILTERTYPE currentFilteIndex;
@property(nonatomic, strong)NSMutableArray* array1;
@property(nonatomic, strong)NSMutableArray* array2;
@property(nonatomic, strong)NSMutableArray* array2_apply_c1;

@property(nonatomic, strong)NSMutableArray* array3;

@property(nonatomic, assign)id<HGSearchFilterDelegate> delegate;

-(void)clearCategoriesArray;
-(void)reFillLeftCategoryBoardWithArray:(NSArray*)dataArray;
@end

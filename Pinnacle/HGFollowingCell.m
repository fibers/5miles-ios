//
//  HGFollowingCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGFollowingCell.h"
#import "HGUser.h"
#import "HGConstant.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGAppData.h"


@implementation HGFollowingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    [self _commonSetup];
}

- (void)_commonSetup
{
    self.avatarView.clipsToBounds = YES;
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.layer.borderColor = SYSTEM_MY_ORANGE.CGColor; //[UIColor whiteColor].CGColor;
    self.avatarView.layer.borderWidth = 1;
    self.avatarView.layer.cornerRadius = CGRectGetWidth(self.avatarView.bounds) * 0.5;
    
    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.avatarView.frame.size.width/3, self.avatarView.frame.size.width/3)];
    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    
    CGPoint centerPoint = self.avatarView.center;
    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    self.userAvatar_verifyMark.center = centerPoint;
    
    
    [self.contentView addSubview:self.userAvatar_verifyMark];

    [self addFollowButton];
    
    self.seperatorLine = [[UIView alloc] initWithFrame:CGRectMake(12, self.frame.size.height-0.5, SCREEN_WIDTH-12, 0.5)];
    [self.contentView addSubview: self.seperatorLine];
    self.seperatorLine.backgroundColor = FANCY_COLOR(@"cccccc");
}
-(void)followButtonAction
{
    NSLog(@"button click ");
    if (! (self.uid != nil && self.uid.length > 0) ) {
        return;
    }
    if (self.following) {
        UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:
                                      NSLocalizedString(@"Unfollow", nil),
                                      nil];
        actionSheet.tag = 0;
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showInView:self.superview];
    }
    else{
        [self startFollow];
    }
    
    
}



-(void)startUnfollow
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onTouchUnfollowing:)]) {
        self.followButton.backgroundColor = [UIColor whiteColor];
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
        [self.delegate onTouchUnfollowing:self.uid];
    }
}
-(void)startFollow
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onTouchFollowing:)]) {
        self.followButton.backgroundColor = FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];
        [self.delegate onTouchFollowing:self.uid];
    }
}

-(void)addFollowButton{
    self.followButton = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-12-42, 9, 42, 26)];
    [self addSubview:self.followButton];
    self.followButton.layer.borderWidth = 1;
    self.followButton.layer.cornerRadius = 4;
    self.followButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.followIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 38/2, 40/2)];
    [self.followButton addSubview:self.followIcon];
    
    self.followIcon.center = CGPointMake(42/2, 26/2);
    self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];
    [self.followButton addTarget:self action:@selector(followButtonAction) forControlEvents:UIControlEventTouchUpInside];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 0)
    {
        switch (buttonIndex) {
            case 0:{
                if (self.isMyFollowing) {
                     [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowing_view" action:@"myfollowing_unfollowyes" label:nil value:nil];
                }
                else{
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowing_view" action:@"sellerfollowing_unfollowyes" label:nil value:nil];
                }
                
                [self startUnfollow];
                break;
            }
            default:{
                if (self.isMyFollowing) {
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowing_view" action:@"myfollowing_unfollowno" label:nil value:nil];
                }
                else{
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowing_view" action:@"sellerfollowing_unfollowno" label:nil value:nil];
                }
                break;
            }
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithObject:(HGUser *)entry withIsMyFollowing:(BOOL)isMyFollowing
{
    self.isMyFollowing = isMyFollowing;
    self.uid = entry.uid;
    int sugggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatarView.frame.size.width*2];
      NSString* image_url = [[HGUtils sharedInstance] cloudinaryLink:entry.portraitLink width:sugggestWidth height:sugggestWidth];
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    self.nameLabel.text = entry.displayName;
    self.nameLabel.numberOfLines = 1;
    
    if (entry.verified.intValue == 1) {
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    [self.nameLabel sizeToFit];
    
    self.following = entry.following;
    if (!entry.following) {
        
        self.followButton.backgroundColor = [UIColor whiteColor];
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
        
    }
    else{
       self.followButton.backgroundColor = FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];

    }
    if([self.uid isEqualToString:[HGUserData sharedInstance].fmUserID])
    {
        self.followButton.hidden = YES;
        self.followIcon.hidden = YES;
    }
    else{
        self.followIcon.hidden = self.followButton.hidden = NO;
    }
}

@end

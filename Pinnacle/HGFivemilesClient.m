//
//  HGFivemilesClient.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-13.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGFivemilesClient.h"
#import "HGAppData.h"
#import "HGUtils.h"
#import <AFNetworking/AFNetworking.h>
#import <AdSupport/AdSupport.h>
@implementation HGFivemilesClient

static NSURLSessionConfiguration *config;
static HGFivemilesClient *_sharedGuestClient = nil;
static HGFivemilesClient *_sharedUserClient = nil;
static HGFivemilesClient *_sharedChatClient = nil;


+ (instancetype)guestClient
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:20 * 1024
                                                          diskCapacity:50 * 1024
                                                              diskPath:nil];
        
        [config setURLCache:cache];
        
        _sharedGuestClient = [[HGFivemilesClient alloc] initWithBaseURL:[NSURL URLWithString:FMBizAPIBaseURLString] sessionConfiguration:config];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    });
    
    return _sharedGuestClient;
}


+ (instancetype)userClient
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:50 * 1024
                                                          diskCapacity:1 * 200 * 1024
                                                              diskPath:nil];
        
        [config setURLCache:cache];
        
        _sharedUserClient = [[HGFivemilesClient alloc] initWithBaseURL:[NSURL URLWithString:FMBizAPIBaseURLString] sessionConfiguration:config];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    });
    
    return _sharedUserClient;

}


+ (instancetype)chatClient
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:50 * 1024
                                                          diskCapacity:1 * 200 * 1024
                                                              diskPath:nil];
        
        [config setURLCache:cache];
        
        _sharedChatClient = [[HGFivemilesClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://chat.5milesapp.com/"] sessionConfiguration:config];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    });
    
    return _sharedChatClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration
{
    self = [super initWithBaseURL:url sessionConfiguration:configuration];
    if(self) {
        NSString * versionBuildString = [NSString stringWithFormat: @"%@(%@)",
                                         [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"],
                                         [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey]];
        NSString *deviceType = [[HGUtils sharedInstance] deviceString];
        NSString *osVersion = [UIDevice currentDevice].systemVersion;
        NSString *preferredLanguages = [[NSLocale preferredLanguages] firstObject];
        
        NSString *userAgentString = [@"" stringByAppendingFormat:@"os:ios,os_version:%@,app_version:%@,device:%@,Accept-Language:%@",osVersion,versionBuildString,deviceType,preferredLanguages];
        config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.serializer = [AFHTTPRequestSerializer serializer];
        [self.serializer setValue:userAgentString forHTTPHeaderField:@"User-Agent"];
        [self setFB_advertisingID];
        self.requestSerializer = _serializer;
    }
    
    return self;

}
-(void)setFB_advertisingID
{
    
    NSString *idfaString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    int advertisingValue = [[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]?1:0;
    
    
    [self.serializer setValue:idfaString forHTTPHeaderField:@"advertiser-id"];
    [self.serializer setValue:@"" forHTTPHeaderField:@"attribution"];
    [self.serializer setValue:[@"" stringByAppendingFormat:@"%d",advertisingValue] forHTTPHeaderField:@"advertiser-tracking_enabled"];
    [self.serializer setValue:@"1" forHTTPHeaderField:@"application-tracking-enabled"];
    
}

static int serverSelector = 0;
+(void) resignClientServer
{
    if (serverSelector % 2 == 0) {
        NSLog(@"using test server, change to real on line server");
       _sharedGuestClient = [[HGFivemilesClient alloc] initWithBaseURL:[NSURL URLWithString:FMBizAPIBaseURLStringRealServer] sessionConfiguration:config];
        _sharedUserClient = [[HGFivemilesClient alloc] initWithBaseURL:[NSURL URLWithString:FMBizAPIBaseURLStringRealServer] sessionConfiguration:config];
        
    }
    else{
        NSLog(@"using real online server, change to test server");
         _sharedGuestClient = [[HGFivemilesClient alloc] initWithBaseURL:[NSURL URLWithString:FMBizAPIBaseURLString] sessionConfiguration:config];
        _sharedUserClient = [[HGFivemilesClient alloc] initWithBaseURL:[NSURL URLWithString:FMBizAPIBaseURLString] sessionConfiguration:config];
    }
    serverSelector ++;
}

- (BOOL)checkNetworkStatus
{
    return self.reachabilityManager.isReachable;
}


- (NSURLSessionDataTask *)FIVEMILES_POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
   
    
    void (^fivemilesFailture)(NSURLSessionDataTask *task, NSError *error)
    =^(NSURLSessionDataTask *task, NSError *error){
        
        if(failure){
            failure(task, error);
        }
        
        NSString *errorMessage = [[HGUtils sharedInstance] apiErrorMessageFrom:error];
        if([errorMessage length] > 0){
            [[HGUtils sharedInstance] showSimpleAlert:errorMessage];
        }
        
    };
    
    NSURLSessionDataTask* task = [self POST:URLString parameters:parameters success:success failure:fivemilesFailture];
    return task;
}

- (NSURLSessionDataTask *)FIVEMILES_GET:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
   
    void (^fivemilesFailture)(NSURLSessionDataTask *task, NSError *error)
    =^(NSURLSessionDataTask *task, NSError *error){
        
        if(failure){
            failure(task, error);
        }

        NSString *errorMessage = [[HGUtils sharedInstance] apiErrorMessageFrom:error];
        if([errorMessage length] > 0){
            [[HGUtils sharedInstance] showSimpleAlert:errorMessage];
        }

    };
    
    void (^fivemilesSuccess)(NSURLSessionDataTask *task, id responseObject) = ^(NSURLSessionDataTask *task, id responseObject){
        //todo, we can insert common http response handle here.
        if(success){
            success(task, responseObject);
        }
    };
    
    NSURLSessionDataTask* task = [self GET:URLString parameters:parameters success:fivemilesSuccess failure:fivemilesFailture];
    return task;
    
}

@end

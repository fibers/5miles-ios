//
//  HGUserVerifyHintView.h
//  
//
//  Created by Alex on 15-1-5.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HGTriangleUpView;
@class HGTriangleDownView;
@protocol HGUserVerifyHintDelegate <NSObject>

-(void)clickGetVerifyButtion;

@end

@interface HGUserVerifyHintView : UIView

@property (nonatomic, strong) HGTriangleUpView* triangleViewHint;
@property (nonatomic, strong) UIView* contentView;
@property (nonatomic, assign) int isCurrentUser;

@property (nonatomic, assign) id<HGUserVerifyHintDelegate> delegate;


-(void)configContentText;
-(void)configGetVerifiedButton;

- (void)removeVerfiedButton:(BOOL)remove;
@end


@interface HGTriangleUpView : UIView
{
    
}
@end
@interface HGTriangleDownView: UIView
{
    
}
@end
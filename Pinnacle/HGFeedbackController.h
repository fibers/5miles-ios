//
//  HGFeedbackController.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-15.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGFeedbackController : UIViewController

@property (nonatomic, strong) UILabel* fakePlaceHolder;
@end

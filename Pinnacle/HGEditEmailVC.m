//
//  HGEditEmailVC.m
//  Pinnacle
//
//  Created by Alex on 15/5/19.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGEditEmailVC.h"
#import "HGConstant.h"
#import "HGUserData.h"
#import "HGAppData.h"
#import "HGUITextField.h"
#import <SVProgressHUD.h>
#import "PXAlertView+Customization.h"
#import "HGUtils.h"
#import "NSString+EmailAddresses.h"

@interface HGEditEmailVC ()
@property (nonatomic, strong)HGUITextField* emailField;
@property (nonatomic, strong)UILabel* hintLabel;
@property (nonatomic, strong)UIButton* btnSend;
@end

@implementation HGEditEmailVC


static BOOL bUserTouchBack = YES;
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    ////TextField has a strange bug, from 8.3..view re show will clear the content.
    self.emailField = [[HGUITextField alloc] initWithFrame:CGRectMake(0, 70, self.view.frame.size.width,40)];
    [self.view addSubview:self.emailField];
    self.emailField.font = [UIFont systemFontOfSize:15];
    self.emailField.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.emailField.delegate = self;
    self.emailField.text = [HGUserData sharedInstance].userEmail;
    self.emailField.backgroundColor = [UIColor whiteColor];
    self.emailField.edgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    
    if (self.emailField.text.length > 0) {
        [self enableSendButton];
    }
    else{
        [self disableSendButton];
    }
    

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[HGUtils sharedInstance] gaTrackViewName:@"changeemail_view"];
    self.navigationItem.title = NSLocalizedString(@"Change Email",nil);
    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    
    
    
    self.hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 70+40+5, 200, 30)];
    [self.view addSubview:self.hintLabel];
    self.hintLabel.textColor = FANCY_COLOR(@"636363");
    self.hintLabel.font = [UIFont systemFontOfSize:13];
    self.hintLabel.text = NSLocalizedString(@"Edit your new email address", nil);
    
    
    [self addRightButton];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.emailField becomeFirstResponder];
}


////email max length limit 100;;
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (newString.length==0) {
        [self disableSendButton];
    }
    else{
        [self enableSendButton];
    }
    
    
    return newString.length > 100 ? NO:YES;
    
}
- (void)enableSendButton{
    self.btnSend.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)disableSendButton{
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}
-(void)onTouchSend
{
    if (![self.emailField.text isValidEmailAddress]) {
        PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"This email doesn't seem to work.", nil) cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            [self.emailField becomeFirstResponder];
        }];
        [view useDefaultIOS7Style];
        return;
    }
    
    self.emailField.text = [self.emailField.text stringByCorrectingEmailTypos];
    
    
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"fill_profile/" parameters:@{@"email":self.emailField.text} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [SVProgressHUD dismiss];
        [HGUserData sharedInstance].userEmail = [responseObject objectForKey:@"email"];
        
        
        
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"5miles has sent an email to verify your account, please check your inbox.", nil)  withTitle:NSLocalizedString(@"Email has been changed",nil)];
        bUserTouchBack = NO;
        
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        
    }];
    
}
-(void)addRightButton
{
    UIView* rightBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    
    self.btnSend = [[UIButton alloc] initWithFrame:CGRectMake(13, 0, 65, 30)];
    self.btnSend.clipsToBounds = YES;
    self.btnSend.layer.cornerRadius = 3;
    self.btnSend.layer.borderWidth = 1;
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnSend.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.btnSend setTitle:NSLocalizedString(@"Save",nil) forState:UIControlStateNormal];
    [self.btnSend setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [self.btnSend setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.btnSend addTarget:self action:@selector(onTouchSend) forControlEvents:UIControlEventTouchUpInside];
    
   
    
    [rightBackView addSubview:self.btnSend];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBackView];
    
    [self enableSendButton];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    if (bUserTouchBack) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"changeemail_view" action:@"changeemail_back" label:nil value:nil];
    }
}

@end

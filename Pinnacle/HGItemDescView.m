//
//  HGItemDescView.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-1.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemDescView.h"
#import "HGShopItem.h"
#import <UIColor+FlatColors.h>

#import <AVFoundation/AVFoundation.h>
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <AFNetworking/AFNetworking.h>


@interface HGItemDescView () <AVAudioPlayerDelegate>

@property (nonatomic, strong) AVAudioPlayer * audioPlayer;

@end

@implementation HGItemDescView
{
    HGShopItem * _item;
    NSString * _price;
    NSString * _dist;
    NSURL * _mediaURL;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame item:(HGShopItem *)item
{
    self = [super initWithFrame:frame];
    if (self) {
        _item = item;
        [self _setup];
    }
    return self;
}

- (void)_setup
{
    self.backgroundColor = [UIColor whiteColor];
    
    self.lbTitle = [UILabel new];
    self.lbTitle.backgroundColor = [UIColor clearColor];
    self.lbTitle.font = [UIFont systemFontOfSize:14.0];
    self.lbTitle.numberOfLines = 1;
    self.lbTitle.text = _item.title;
    [self addSubview:self.lbTitle];
    
    self.lbPrice = [UILabel new];
    self.lbPrice.backgroundColor = [UIColor clearColor];
    self.lbPrice.font = [UIFont boldSystemFontOfSize:18.0];
    self.lbPrice.numberOfLines = 1;
    self.lbPrice.textColor = [UIColor darkTextColor];
    self.lbPrice.text = _price = [_item priceForDisplay];
    [self addSubview:self.lbPrice];
    
    self.originalPrice = [StrikeThroughLabel new];
    self.originalPrice.backgroundColor = [UIColor clearColor];
    self.originalPrice.font = [UIFont boldSystemFontOfSize:18.0];
    self.originalPrice.numberOfLines = 1;
    self.originalPrice.textColor = self.originalPrice.strikeColor = [UIColor darkTextColor];
    self.originalPrice.text = @"$100";
    self.originalPrice.strikeThroughEnabled = YES;
    [self addSubview:self.originalPrice];

    
    self.lbDesc = [UILabel new];
    self.lbDesc.backgroundColor = [UIColor clearColor];
    self.lbDesc.font = [UIFont systemFontOfSize:13.0];
    self.lbDesc.text = _item.desc;
    [self addSubview:self.lbDesc];
    
    self.btnPlaySound = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [self.btnPlaySound addTarget:self action:@selector(_playDownloadedMedia:) forControlEvents:UIControlEventTouchUpInside];
    
    self.btnPlaySound.hidden = _item.mediaLink.length == 0;
    self.btnPlaySound.enabled = NO;
    if (!self.btnPlaySound.hidden) {
        [self addSubview:self.btnPlaySound];
        [self _startDownloadMedia];
    }
    
    NSString *address = [_item formatAddress];
    if (address.length > 0) {
        _dist = address;
    }
    else
    {
        NSString *distance = [_item formatDistance];
        if(distance.length > 0){
            _dist = distance;
        }else{
            _dist = NSLocalizedString(@"unknown", nil);
        }
        
    }

    
    self.btnLocation = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnLocation.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    [self.btnLocation setTitle:_dist forState:UIControlStateNormal];
    [self.btnLocation setTitleColor:[UIColor flatOrangeColor] forState:UIControlStateNormal];
    [self addSubview:self.btnLocation];

    CGFloat priceHeight = [self.lbPrice.font lineHeight] + 2;
    CGRect rect = [_price boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.bounds), priceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lbPrice.font} context:NULL];
    self.lbPrice.frame = CGRectMake(8, 2, CGRectGetWidth(rect), priceHeight);
    
    {
         CGFloat originalpriceHeight = [self.originalPrice.font lineHeight] + 2;
         CGRect originalrect = [@"$100" boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.bounds), originalpriceHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.originalPrice.font} context:NULL];
          self.originalPrice.frame = CGRectMake(CGRectGetMinX(self.lbPrice.frame), CGRectGetMaxY(self.lbPrice.frame) + 2, CGRectGetWidth(originalrect), originalpriceHeight);
    }
    //==========price end=======
    
    
    CGFloat titleHeight = [self.lbTitle.font lineHeight] + 4;
    rect = [_item.title boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.bounds), titleHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lbTitle.font} context:NULL];
    self.lbTitle.frame = CGRectMake(CGRectGetMinX(self.originalPrice.frame), CGRectGetMaxY(self.originalPrice.frame) + 2, CGRectGetWidth(rect), titleHeight);
    
    
    CGFloat maxDescHeight = [self.lbDesc.font lineHeight] * 3 + 4;
    rect = [_item.desc boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.bounds), maxDescHeight) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:self.lbDesc.font} context:NULL];
    self.lbDesc.frame = CGRectMake(CGRectGetMinX(self.lbTitle.frame), CGRectGetMaxY(self.lbTitle.frame) + 4, CGRectGetWidth(rect), CGRectGetHeight(rect));
    
    CGFloat distHeight = [self.btnLocation.titleLabel.font lineHeight];
    rect = [_dist boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, distHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.btnLocation.titleLabel.font} context:NULL];
    self.btnLocation.frame = CGRectMake(0, 0, CGRectGetWidth(rect) + 10, CGRectGetHeight(rect) + 4);
    self.btnLocation.center = CGPointMake(CGRectGetWidth(self.bounds) - CGRectGetWidth(self.btnLocation.bounds) * 0.5 - 6, self.lbPrice.center.y) ;
    
    self.btnPlaySound.center = CGPointMake(CGRectGetMinX(self.btnLocation.frame) - CGRectGetWidth(self.btnPlaySound.bounds) * 0.5 - 10, self.lbPrice.center.y) ;
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, CGRectGetMaxY(self.lbDesc.frame) + 2);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)_startDownloadMedia
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:_item.mediaLink];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *directoryURL = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSString * filename = [URL lastPathComponent];
        if ([URL pathExtension].length == 0) {
            filename = [filename stringByAppendingString:@".m4a"];
        }
        return [directoryURL URLByAppendingPathComponent:filename];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        _mediaURL = filePath;
        self.btnPlaySound.enabled = YES;
    }];
    [downloadTask resume];
}

- (void)_playDownloadedMedia:(id)sender
{
    NSError * error;
    /*NSDictionary * attribs = [[NSFileManager defaultManager] attributesOfItemAtPath:_mediaURL.path error:&error];
    NSNumber * filesize = [attribs objectForKey:NSFileSize];
    NSLog(@"sound file size : %@", filesize);*/

    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:_mediaURL error:&error];
    if (error) {
        PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Audio player could not be created.", nil)];
        [view useDefaultIOS7Style];
        self.audioPlayer = nil;
        return;
    }
    
    if ([self.audioPlayer prepareToPlay]) {
        
        UIImage* image = [UIImage imageNamed:@"audio_pause"];
        [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
        
        [self.btnPlaySound removeTarget:self action:@selector(_playDownloadedMedia:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnPlaySound addTarget:self action:@selector(pauseAudioPlay) forControlEvents:UIControlEventTouchUpInside];
        
        self.audioPlayer.delegate = self;
        BOOL success = [self.audioPlayer play];
        if (!success) {
            NSLog(@"start playing :%d", success);
        }
    } else {
        NSLog(@"audio file not supported.");
        self.audioPlayer = nil;
        PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Audio file format not supported.", nil)];
        [view useDefaultIOS7Style];
    }
}

#pragma mark - Audio Player Delegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if (!flag) {
        NSLog(@"audio player finished with error.");
    }
    
    self.audioPlayer = nil;
    
   
    UIImage*image = [UIImage imageNamed:@"audio_play"];
    [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
    [self.btnPlaySound addTarget:self action:@selector(_playDownloadedMedia:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"play sound failed:%@", error);
    self.audioPlayer = nil;
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}


-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}
-(void)pauseAudioPlay
{
    
    [self.audioPlayer pause];
    UIImage* image = [UIImage imageNamed:@"audio_play"];
    [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
    
    [self.btnPlaySound removeTarget:self action:@selector(pauseAudioPlay) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPlaySound addTarget:self action:@selector(resumeAudioPlay) forControlEvents:UIControlEventTouchUpInside];
}
-(void)resumeAudioPlay
{
    [self.audioPlayer play];
    UIImage* image = [UIImage imageNamed:@"audio_pause"];
    [self.btnPlaySound setBackgroundImage:image forState:UIControlStateNormal];
    
    [self.btnPlaySound removeTarget:self action:@selector(resumeAudioPlay) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPlaySound addTarget:self action:@selector(pauseAudioPlay) forControlEvents:UIControlEventTouchUpInside];
}

@end

//
//  HGMyItemsController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemListController.h"
#import "MGSwipeTableCell.h"

@interface HGMyItemsController : UITableViewController<MGSwipeTableCellDelegate>
//HGItemListController

@property (nonatomic, strong) NSString * userID;

@end

//
//  HGUserSummaryView.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-13.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGFollowButton.h"

@protocol HGUserSummaryDelegate <NSObject>

-(void)showUserVerifyInfoView;

@end

@class HGUser;

@interface HGUserSummaryView : UICollectionReusableView

@property (nonatomic, assign)id<HGUserSummaryDelegate> delegate;


@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet HGFollowButton *btnAction;



//////////////////////////
@property (weak, nonatomic) IBOutlet UIButton *btnItemNum;
@property (weak, nonatomic) IBOutlet UIButton *btnFollowerNum;
@property (weak, nonatomic) IBOutlet UIButton *btnFollowingNum;
@property (weak, nonatomic) IBOutlet UILabel *itemNumUnit;
@property (weak, nonatomic) IBOutlet UILabel *followerUnit;
@property (weak, nonatomic) IBOutlet UILabel *followingUnit;
@property (weak, nonatomic) IBOutlet UIImageView *divider1;
@property (weak, nonatomic) IBOutlet UIImageView *divide2;
//////////////////////////


@property (nonatomic, strong) UIView* bottomPartBackground;
@property (strong, nonatomic) UIImageView* avartarCover;

//////////////////user verify UI /////////////
@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;

@property (nonatomic, strong) UIButton* userEmail_verifyButton;
@property (nonatomic, strong) UIButton* userPhone_verifyButton;
@property (nonatomic, strong) UIButton* userFB_verifyButton;

@property (nonatomic, strong) UIButton* user_verifyInfoButton;


////////////////end of user verify UI .///////


//{{ location view
@property (nonatomic, strong) UIView* locationView;
@property (nonatomic, strong) UIImageView* locationIcon;
@property (nonatomic, strong) UIButton* locationNameButton;
@property (nonatomic, strong) UILabel * locationLabel;
//}} end of location view

- (void)configWithEntity:(HGUser *)user;

@end

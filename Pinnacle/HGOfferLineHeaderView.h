//
//  HGOfferLineHeaderView.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-13.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HGOfferHeaderActionDelegate <NSObject>
-(void)onTouchItemImageAction;
@end

@interface HGOfferLineHeaderView : UIView

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIButton *naviBackButton;
@property (nonatomic, strong) UIButton *bigNaviBackButton;


@property (nonatomic, strong) UIButton * itemContentAreaButton;


@property (nonatomic, strong) UILabel *itemTitle;
@property (nonatomic, strong) UIImageView *ivSeparator;
@property (nonatomic, assign) id<HGOfferHeaderActionDelegate> delegate;
@end

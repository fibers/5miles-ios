//
//  HGProfileSelfIntroductionViewController.h
//  Pinnacle
//
//  Created by Alex on 14-10-1.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCPlaceholderTextView.h"
@interface HGProfileSelfIntroductionViewController : UIViewController
@property (weak, nonatomic) IBOutlet GCPlaceholderTextView *introductionField;

@end

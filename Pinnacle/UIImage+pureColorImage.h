//
//  UIImage+pureColorImage.h
//  Rainbow
//
//  Created by Maya Game on 13-6-9.
//  Copyright (c) 2013年 hugang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (pureColorImage)

+ (UIImage*) imageWithColor:(UIColor*)color;
+(UIImage *)getTintedImage:(UIImage*)image withColor:(UIColor *)tintColor;
@end

//
//  UserReportTableViewController.h
//  Pinnacle
//
//  Created by Alex on 14-11-12.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SZTextView/SZTextView.h>
#import "HGReportReason.h"

@interface UserReportTableViewController : UITableViewController<UITextViewDelegate>

@property (nonatomic, strong) NSString *itemID;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *reviewID;

//0 for item report, 1, for seller report.
@property (nonatomic, assign)ReportReasonType reportType;

@end

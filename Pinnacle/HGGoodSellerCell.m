//
//  HGGoodSellerCell.m
//  Pinnacle
//
//  Created by Alex on 15-4-1.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGGoodSellerCell.h"
#import "HGConstant.h"
#import "HGAppData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGGoodUserNearBy.h"
#import "UIImage+pureColorImage.h"

@implementation HGGoodSellerCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self commonSetup];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self commonSetup];
}


-(void)addAvatarCover
{
    self.avatarCover = [[UIImageView alloc] initWithFrame:self.avatarView.frame];
    self.avatarCover.image = [UIImage imageNamed:@"avatar-cover"];
    self.avatarCover.center = self.avatarView.center;
    [self addSubview:self.avatarCover];
}
-(void)commonSetup
{
    self.nameLabel.textColor = self.distanceLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.distanceLabel.font = [UIFont systemFontOfSize:13];
    self.nameLabel.font = [UIFont systemFontOfSize:16];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.layer.cornerRadius = CGRectGetWidth(self.avatarView.bounds) * 0.5;
    [self addAvatarCover];

    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.avatarView.frame.size.width/3, self.avatarView.frame.size.width/3)];
    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    CGPoint centerPoint = self.avatarView.center;
    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    self.userAvatar_verifyMark.center = centerPoint;
    [self addSubview:self.userAvatar_verifyMark];
    
    UITapGestureRecognizer *tapAvatar = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapAvatar)];
    [self.avatarView addGestureRecognizer:tapAvatar];
    
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    self.seperatedView = [[UIView alloc] initWithFrame:CGRectMake(11, 62, self.frame.size.width-5, fSeperatorHeight)];
    [self addSubview:self.seperatedView];
    self.seperatedView.backgroundColor = FANCY_COLOR(@"eeeeee");
    
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height -14, self.frame.size.width, 14)];
    
    self.bottomView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    [self addSubview: self.bottomView];
    [self addFollowButton];
    
    
    self.itemNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(11, 61, 120, 20)];
    self.itemNumberLabel.numberOfLines = 1;
    self.itemNumberLabel.font = [UIFont systemFontOfSize:13];
    self.itemNumberLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    [self addSubview: self.itemNumberLabel];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    self.seperatedView.frame = CGRectMake(11, 62, self.frame.size.width-5, fSeperatorHeight);
    self.followButton.frame = CGRectMake(self.frame.size.width-8-42, 18, 42, 26);
    
};
-(void)onTapAvatar
{
    
}
-(void)followButtonAction
{
    NSLog(@"button click ");
   
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onTouchCellFollowPrefer)]) {
        
        
        if (self.goodUserNearBy.userPreferFollow) {
            self.followButton.backgroundColor = [UIColor whiteColor];
            self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"unfollow_seller" label:nil value:nil];

        }
        else{
            
            self.followButton.backgroundColor = FANCY_COLOR(@"ff8830");
            self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"follow_seller" label:nil value:nil];
        }
        self.goodUserNearBy.userPreferFollow = !self.goodUserNearBy.userPreferFollow;
        

        [self.delegate onTouchCellFollowPrefer];
    }
   
    
  
}

-(void)addFollowButton
{
    self.followButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-8-42, 18, 42, 26)];
    [self addSubview:self.followButton];
    self.followButton.layer.borderWidth = 1;
    self.followButton.layer.cornerRadius = 4;
    self.followButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.followIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 38/2, 40/2)];
    [self.followButton addSubview:self.followIcon];
    
    self.followIcon.center = CGPointMake(42/2, 26/2);
    self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
    [self.followButton addTarget:self action:@selector(followButtonAction) forControlEvents:UIControlEventTouchUpInside];
}

static int smallItemPictureTag = 5678;
static int maxDisplayPicCount = 7;
-(void)configCell:(NSObject*)obj
{
    
    self.goodUserNearBy =  (HGGoodUserNearBy*)obj;
   
    self.uid = self.goodUserNearBy.uid;

    
    
    if (IOS_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        /////ios 8.
        for(UIView*subviews in [self subviews])
        {
            
            if (subviews.tag == smallItemPictureTag) {
                //NSLog(@"remove from super view in config cell, small images.");
                [subviews removeFromSuperview];
                
            }
        }

    }
    else{
        ////ios 7.0
        for(UIView*subviews in [self.contentView subviews])
        {
            if (subviews.tag == smallItemPictureTag) {
                //NSLog(@"remove from super view in config cell, small images.");
                [subviews removeFromSuperview];
                
            }
        }
    }
    
   
    UIImage*fancyImage = [UIImage imageNamed:@"item-placeholder"];
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:self.goodUserNearBy.portrait] placeholderImage:fancyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    if (self.goodUserNearBy.verified) {
         self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
         self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    
    
    NSString* distanceString = [self getDistanceString:self.goodUserNearBy];
    
    self.nameLabel.text = self.goodUserNearBy.nickname;
    
    NSString* placeString = self.goodUserNearBy.place;
    if (placeString.length > 0 ) {
        placeString = [NSLocalizedString(@" in ",nil) stringByAppendingString:placeString];
    }
                          
    self.distanceLabel.text = [distanceString stringByAppendingString: placeString];
    
    
    if (self.distanceLabel.text.length == 0)
    {
        self.goodseller_location.hidden = YES;
    }
    else
    {
        self.goodseller_location.hidden = NO;
    }
    
    if (self.goodUserNearBy.item_num == 1) {
        self.itemNumberLabel.text = NSLocalizedString(@"1 listing",nil);
    } else {
        self.itemNumberLabel.text = [@"" stringByAppendingFormat:NSLocalizedString(@"%d listings",nil), self.goodUserNearBy.item_num ];
    }
    
    
    for (int i = 0; i< self.goodUserNearBy.item_num && i< maxDisplayPicCount && i<self.goodUserNearBy.item_images.count ; i++) {
        UIImageView* view = [[UIImageView alloc] initWithFrame:CGRectMake(11+ (34+10)*i , 62+18, 34, 34)];
        view.layer.cornerRadius = 2;
        view.layer.borderWidth = 0;
        view.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
        view.clipsToBounds = YES;
        view.tag = smallItemPictureTag;
        view.contentMode = UIViewContentModeScaleAspectFill;
        
         if (IOS_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
             [self addSubview:view];
         }
         else{
             [self.contentView addSubview:view];
         }
        
        NSDictionary* itemImageUrlDic = (NSDictionary*)[self.goodUserNearBy.item_images objectAtIndex:i];
        NSString* itemImageUrl = [itemImageUrlDic objectForKey:@"imageLink"];
        if (itemImageUrl != nil && itemImageUrl.length > 0) {
            
            NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:itemImageUrl width:100 height:0];
            
            UIImage*fancyImage = [UIImage imageNamed:@"item-placeholder"];
            [view sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:fancyImage];
        
        }
    }
    
    
    
    if (self.goodUserNearBy.userPreferFollow) {
        self.followButton.backgroundColor = FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];
        
    }
    else{
        self.followButton.backgroundColor = [UIColor whiteColor];
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];

    }
    
    self.bottomView.hidden = NO;
    self.bottomView.frame = CGRectMake(0, self.frame.size.height -14, self.frame.size.width, 14);
    
}
-(NSString*)getDistanceString:(HGGoodUserNearBy*)userNearBy
{
    double distance=0;
    
    NSString* latString = [userNearBy.location objectForKey:@"lat"];
    NSString* lonString = [userNearBy.location objectForKey:@"lon"];
    
    CLLocation * itemLoc = [[CLLocation alloc] initWithLatitude:latString.floatValue longitude:lonString.floatValue];
    
    
    if ((fabs(latString.floatValue - 0.0) < 0.000001 && fabs(lonString.floatValue - 0.0) < 0.000001))
    {
        distance = -1.0;;
    }
    else{
        
        double userLat=[[HGUtils sharedInstance] getPreferLat] ;
        double userLon =[[HGUtils sharedInstance] getPreferLon];
        if (fabs(userLat - 0.0)< 0.000001 && fabs(userLon - 0.0) < 0.000001) {
            distance = -1.0;
        }
        else{
            
            CLLocation* preferLocation = [[CLLocation alloc] initWithLatitude:userLat longitude:userLon];
            
            distance = [itemLoc distanceFromLocation:preferLocation] * 0.001;
        }
    }
    
    
    if (distance < 0) {
        //1 mile or unknown
        return @"";
    }
    else{
        NSString* disMiles = [self getDistanceMiles:distance];
        return disMiles;
    }
    
}

- (NSString *)getDistanceMiles:(CGFloat)distance
{
    //because #269.
    //https://github.com/3rdStone/fivemiles-ios/issues/269
    NSString * distString;
    float distance_byMile = distance * KM_TO_MILE;
    
    if (distance_byMile < 100) {
        
        if (distance_byMile < 0) {
            distString = @"";
        }else if(distance_byMile < 1)
        {
            distString = [NSString stringWithFormat: @"1 %@", NSLocalizedString(@"mile", nil)];
        }
        else{
            distString = [NSString stringWithFormat: @"%.0f %@", round(distance_byMile), NSLocalizedString(@"miles", nil)];
        }
    } else {
        distString = [NSString stringWithFormat: @"%.0f %@", round(distance_byMile), NSLocalizedString(@"miles", nil)];
    }
    return distString;
}

@end

//
//  HGAppData.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-29.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HGFivemilesClient.h"
#import "HGShopItem.h"
#import <CoreLocation/CoreLocation.h>
#import "HGCampaignsObject.h"
#import <AudioToolbox/AudioToolbox.h>
#import <FMDatabaseQueue.h>

#import "HGUtils.h"
#import "HGConstant.h"
#import "HGAppInitData.h"
#import "HGTrackingData.h"
#import "HGUserData.h"
#import <Raven/RavenClient.h>
#import <MapKit/MapKit.h>
@interface HGAppData : NSObject

@property (nonatomic, strong) CLLocation * currentLocation;
@property (nonatomic, assign) double GPSlat;
@property (nonatomic, assign) double GPSlon;
@property (nonatomic, strong) NSString* countryString;
@property (nonatomic, strong) NSString* regionString;
@property (nonatomic, strong) NSString* cityString;

@property (nonatomic, assign) BOOL bSellingFBShareSuccess;


//********************system module control***********************
@property (nonatomic, assign) BOOL bOpenReviewFunctions;


//****************************************************************


@property (nonatomic, strong) HGAppInitData* AppInitDataSingle;

@property (nonatomic, strong) NSString *deviceToken;

@property (nonatomic, strong) NSString * phoneNumber_CountryName;
@property (nonatomic, strong) NSString * phoneNumber_countryCode;
@property (nonatomic, strong) NSString * phoneNumber;
@property (nonatomic, strong) NSString * selfIntroduct;

//{{used for edit user items
@property (nonatomic, assign) BOOL bHasItemValue;
@property (nonatomic, strong) HGShopItem* editItem;
//}}end of used for edit user items


@property (nonatomic, strong) HGFivemilesClient *guestApiClient;
@property (nonatomic, strong) HGFivemilesClient *userApiClient;
@property (nonatomic, strong) HGFivemilesClient *chatApiClient;


@property (nonatomic, strong) NSString* sellingItemBrandString;
@property (nonatomic, strong) NSString* sellingItemDeliveryString;
@property (nonatomic, assign) int sellingItemDeliveryType;





@property (nonatomic, strong) HGCampaignsObject* startADCampaign;
@property (nonatomic, strong) HGCampaignsObject* homeBanner;
@property (nonatomic, assign) BOOL homeBanner_userClose;


@property (nonatomic, assign) int recommend_themeID;

@property (nonatomic, strong) NSString* userVerifiedPhonenumberString;

@property (nonatomic, assign) BOOL bHomeSwitchToFollowingNeeded;
@property (nonatomic, assign) BOOL bShowStartAD;
@property (nonatomic, assign) BOOL bHaveNewMessage;

@property (nonatomic, strong) NSArray *reportItemReasons;
@property (nonatomic, strong) NSArray *reportSellerReasons;
@property (nonatomic, strong) NSArray *reportReviewReasons;

@property (nonatomic, strong) FMDatabaseQueue* dataBaseQueue;

@property (nonatomic, assign) BOOL jumpToSearchFromHomeRightButton;
/////////////////////////////////////////////////////////////////
@property (nonatomic, strong) NSString* chatMapSearchKeyString;
@property (nonatomic, strong) NSString* chatMapUserSelectAddrLat;
@property (nonatomic, strong) NSString* chatMapUserSelectAddrLon;
@property (nonatomic, strong) NSString* chatMapSysteSuggestKeys;
@property (nonatomic, strong) MKMapItem* chatMapUserSelectSearchResultItem;

//////////////////////////////////////////////////////////////////
+ (HGAppData *)sharedInstance;

- (void)saveAppDataToDisk;
- (NSString *)localUserKey:(NSString *)key;
- (NSString *) versionBuild;



- (void)prefetchConfiguration;

@end

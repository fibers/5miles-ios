//
//  HGSignUpEmailController.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-5.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#define Y_OFFSET_KEYBOARD_SHOW ([UIScreen mainScreen].bounds.size.height - 600)

@interface HGSignUpEmailController : UIViewController

@end

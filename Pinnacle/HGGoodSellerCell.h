//
//  HGGoodSellerCell.h
//  Pinnacle
//
//  Created by Alex on 15-4-1.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGGoodUserNearBy.h"

@protocol HGGoodSellerCellDelegate <NSObject>

@optional
-(void)onTouchCellFollowPrefer;

@end



///////////////////////////////////////////////////////////
@interface HGGoodSellerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (nonatomic, strong) UIImageView* avatarCover;
@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;

@property (weak, nonatomic) IBOutlet UIImageView *goodseller_location;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property(nonatomic, strong) UIView* seperatedView;
@property(nonatomic, strong) UIButton* followingButton;

@property(nonatomic, strong) UILabel* itemNumberLabel;


@property (nonatomic, strong) UIButton* followButton;
@property (nonatomic, strong) UIImageView* followIcon;


@property (nonatomic, strong) UIView* bottomView;



@property(nonatomic, strong) NSString* uid;
@property(nonatomic, strong) HGGoodUserNearBy* goodUserNearBy;

@property(nonatomic, assign) id<HGGoodSellerCellDelegate> delegate;
//////////////////function///////////////

-(void)configCell:(NSObject*)obj;
@end

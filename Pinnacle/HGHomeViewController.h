//
//  HGHomeViewController.h
//  Pinnacle
//
//  Created by mumuhou on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGSegmentViewController.h"

@interface HGHomeViewController : HGSegmentViewController

- (void)freshHomeItem;

- (void)freshHomeItemSilent;

- (void)showNearbyView;

- (void)performSegue:(id)sender withSegueName:(NSString*)segueName;

//- (void)hideNavigationBarWithCompletion:(void (^)(BOOL finished))completion;
//
//- (void)restoreNavigationBarWithCompletion:(void (^)(BOOL finished))completion;

@end

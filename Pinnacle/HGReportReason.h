//
//  HGReportReason.h
//  Pinnacle
//
//  Created by shengyuhong on 15/4/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "JSONModel.h"

typedef NS_ENUM(NSUInteger, ReportReasonType){
    ReportReasonTypeItem = 0,
    ReportReasonTypeSeller,
    ReportReasonTypeReview
};

@interface HGReportReason : JSONModel

@property (assign, nonatomic) int uid;
@property (strong, nonatomic) NSString *name;

@end

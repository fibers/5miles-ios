//
//  HGCategoryPickController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGCategoryPickController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, assign) BOOL bIsItemSell;
@property (nonatomic, assign) NSInteger selectedCatID;
@property (nonatomic, assign) int selectedC1_ID;
@property (weak, nonatomic) IBOutlet UITableView *tableview;



@property (nonatomic ,strong) NSString* currentSelectedC2TitleString;
@end

//
//  HGCountryCodeSelectViewController.m
//  Pinnacle
//
//  Created by Alex on 14-10-9.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGCountryCodeSelectViewController.h"
#import "HGUtils.h"
@interface HGCountryCodeSelectViewController ()

@end

@implementation HGCountryCodeSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.currentTypeString = @"phoneselectcountry_view";
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"phoneselectcountry_view" action:@"select_country" label:nil value:nil];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"CountryData" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    NSArray* countryCode = [data valueForKey:@"cityArray"];
    NSMutableArray* countryNames = [[NSMutableArray alloc] init];
    NSMutableArray* countryCodesNumber = [[NSMutableArray alloc] init];
    for (int i = 0; i<countryCode.count; i++) {
        NSString* currentString = countryCode[i];
        [countryNames addObject:currentString];
        i++;
        NSString* currentCodeNumber = countryCode[i];
        [countryCodesNumber addObject:currentCodeNumber];
    }
    self.countryArray = countryNames;
    self.countryCodeNumbers = countryCodesNumber;
    self.name2CodeNumber = [[NSMutableDictionary alloc] init];
    for (int i = 0;i< self.countryArray.count; i++) {
        [self.name2CodeNumber setObject:self.countryCodeNumbers[i] forKey:self.countryArray[i]];
    }

    
    
    self.tableView.tableHeaderView = self.searchBar;
    
    // The search bar is hidden when the view becomes visible the first time
    //self.tableView.contentOffset = CGPointMake(0, CGRectGetHeight(self.searchBar.bounds));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)scrollTableViewToSearchBarAnimated:(BOOL)animated
{
    [self.tableView scrollRectToVisible:self.searchBar.frame animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"phoneselectcountry_view" action:@"" label:nil value:nil];
}
@end

//
//  HGFollowerCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGFollowerCell.h"
#import "HGUser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ItemLikers.h"
#import "HGConstant.h"
#import "HGAppData.h"

@implementation HGFollowerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    [self _commonSetup];
}

- (void)_commonSetup
{
    self.avatarView.clipsToBounds = YES;
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.layer.borderColor = SYSTEM_MY_ORANGE.CGColor;//[UIColor whiteColor].CGColor;
    self.avatarView.layer.borderWidth = 1;
    self.avatarView.layer.cornerRadius = CGRectGetWidth(self.avatarView.bounds) * 0.5;
    
    
    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.avatarView.frame.size.width/3, self.avatarView.frame.size.width/3)];
    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    CGPoint centerPoint = self.avatarView.center;
    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    self.userAvatar_verifyMark.center = centerPoint;
    [self addSubview:self.userAvatar_verifyMark];
    
    [self addFollowButton];
    
    self.seperatorLine = [[UIView alloc] initWithFrame:CGRectMake(12, self.frame.size.height-0.5, SCREEN_WIDTH-12, 0.5)];
    [self.contentView addSubview: self.seperatorLine];
    self.seperatorLine.backgroundColor = FANCY_COLOR(@"cccccc");

}
-(void)addFollowButton{
    self.followButton = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-12-42, 9, 42, 26)];
    [self addSubview:self.followButton];
    self.followButton.layer.borderWidth = 1;
    self.followButton.layer.cornerRadius = 4;
    self.followButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.followIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 38/2, 40/2)];
    [self.followButton addSubview:self.followIcon];
    
    self.followIcon.center = CGPointMake(42/2, 26/2);
    self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
    [self.followButton addTarget:self action:@selector(followButtonAction) forControlEvents:UIControlEventTouchUpInside];
}
-(void)followButtonAction
{
    NSLog(@"button click ");
    if (! (self.uid != nil && self.uid.length > 0) ) {
        return;
    }
    if (self.following) {
        UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:
                                      NSLocalizedString(@"Unfollow", nil),
                                      nil];
        actionSheet.tag = 0;
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showInView:self.superview];
    }
    else{
        [self startFollow];
    }
}


-(void)startUnfollow
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unfollow/" parameters:@{@"user_id": self.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        if (self.isMyFollower) {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollowers_unfollow" label:nil value:nil];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowers_view" action:@"sellerfollowers_unfollow" label:nil value:nil];
        }
        self.followButton.backgroundColor = [UIColor whiteColor];//FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
        self.following = FALSE;
        if (self.userData) {
            self.userData.following = FALSE;
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];

}
-(void)startFollow
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"follow/" parameters:@{@"user_id": self.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if (self.isMyFollower) {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollowers_follow" label:nil value:nil];
        }else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowers_view" action:@"sellerfollowers_follow" label:nil value:nil];
        }
        
        self.followButton.backgroundColor = FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];
        self.following = TRUE;
        if (self.userData) {
            self.userData.following = TRUE;
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 0)
    {
        switch (buttonIndex) {
            case 0:
            {
                if (self.isMyFollower) {
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollowers_unfollowyes" label:nil value:nil];
                }
                else{
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowers_view" action:@"sellerfollowers_unfollowyes" label:nil value:nil];
                }
                [self startUnfollow];
                break;
            }
            default:
            {
                if (self.isMyFollower) {
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowers_view" action:@"myfollowers_unfollowno" label:nil value:nil];
                }
                else{
                    [[HGUtils sharedInstance] gaSendEventWithCategory:@"selelrfollowers_view" action:@"sellerfollowers_unfollowno" label:nil value:nil];
                }
                break;
            }
        }
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithObject:(HGUser *)userData withIsMyFollower:(BOOL)isMyFollower
{
    self.userData = userData;
    self.isMyFollower = isMyFollower;
    int sugggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatarView.frame.size.width*2];
    NSString* image_url = [[HGUtils sharedInstance] cloudinaryLink:userData.portraitLink width:sugggestWidth height:sugggestWidth];
    
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    
    if (userData.verified.intValue == 1) {
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    self.nameLabel.text = userData.displayName;
    self.nameLabel.numberOfLines = 1;
    [self.nameLabel sizeToFit];
    
    
    self.following = userData.following;
    if (!userData.following) {
        self.followButton.backgroundColor = [UIColor whiteColor];
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
    }
    else{
        self.followButton.backgroundColor = FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];
    }
    self.uid = userData.uid;
    if([self.uid isEqualToString:[HGUserData sharedInstance].fmUserID])
    {
        self.followButton.hidden = YES;
        self.followIcon.hidden = YES;
    }
    else{
        self.followIcon.hidden = self.followButton.hidden = NO;
    }
}


-(void)configWithLiker:(ItemLikers*)liker
{
    int sugggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatarView.frame.size.width*2];
     NSString* image_url = [[HGUtils sharedInstance] cloudinaryLink:liker.portrait width:sugggestWidth height:sugggestWidth];
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    self.nameLabel.text = liker.nickname;
    [self.nameLabel sizeToFit];

}

@end

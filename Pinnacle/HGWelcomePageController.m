//
//  HGWelcomePageController.m
//  Pinnacle
//
//  Created by Alex on 15-3-31.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGWelcomePageController.h"
#import "HGAppData.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGSignInEmailController.h"
#import "HGSignUpEmailController.h"
#import "HGAppDelegate.h"
#import <SVProgressHUD.h>
#import "HGFBUtils.h"
#import "UIImage+Scale.h"

@interface HGWelcomePageController ()

@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIView *loginBackground;
@property (nonatomic, strong) X4ImageViewer *imageViewer;
@property (nonatomic, strong) SMPageControl *pageControl;

@end

@implementation HGWelcomePageController

#pragma mark Life Cycle
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.loginBackground.backgroundColor = [UIColor colorWithWhite:0.95 alpha:0.9];
    
    self.btnSignUp.clipsToBounds = YES;
    self.btnSignUp.layer.cornerRadius = 4.0f;
    self.btnSignUp.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    [self.btnSignUp setTitle:NSLocalizedString(@"Sign up", nil) forState:UIControlStateNormal];
    [self.btnSignUp setTitleColor:FANCY_COLOR(@"242424") forState:UIControlStateNormal];
    
    self.btnSignIn.clipsToBounds = YES;
    self.btnSignIn.layer.cornerRadius = 4.0f;
    self.btnSignIn.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    [self.btnSignIn setTitle:NSLocalizedString(@"Sign in", nil) forState:UIControlStateNormal];
    [self.btnSignIn setTitleColor:FANCY_COLOR(@"242424") forState:UIControlStateNormal];
    
    self.btnFacebook.clipsToBounds = YES;
    self.btnFacebook.layer.cornerRadius = 4;
    self.btnFacebook.layer.borderWidth = 1;
    self.btnFacebook.layer.borderColor = [UIColor clearColor].CGColor;
    
    NSString* localizatedString = NSLocalizedString(@"Login with Facebook", nil);
    NSRange  range = [localizatedString rangeOfString:@"Facebook"];
    if (range.length > 0 ) {
        UIFont *font = [UIFont fontWithName:@"Facebook Letter Faces" size:(18.0f)];
        UIColor* fontcolor = [UIColor whiteColor];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:localizatedString];
        [attrString addAttribute:NSFontAttributeName value:font range:range];
        [attrString addAttribute:NSForegroundColorAttributeName value:fontcolor range:NSMakeRange(0, localizatedString.length)];
        [self.btnFacebook setAttributedTitle:attrString forState:UIControlStateNormal];
        [self.btnFacebook setAttributedTitle:attrString forState:UIControlStateHighlighted];
    }
    self.btnFacebook.backgroundColor = [UIColor colorWithRed:0.322f green:0.463f blue:0.796f alpha:1.00f];
    
    self.imageViewer = [[X4ImageViewer alloc] initWithFrame:self.view.bounds];
    self.imageViewer.delegate = self;
    self.imageViewer.dataSource = self;
    
    NSArray *imagesArray;
    if(IPHONE4X){
        imagesArray = @[
                        [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"guide_960_0@2x"  ofType:@"png"]],
                        [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"guide_960_1@2x"  ofType:@"png"]],
                        [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"guide_960_2@2x"  ofType:@"png"]],
                        [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"guide_960_3@2x"  ofType:@"png"]]
                        ];
    } else if (IPHONE5X) {
        imagesArray = @[
                        [UIImage imageNamed:@"guide_1136_0"],
                        [UIImage imageNamed:@"guide_1136_1"],
                        [UIImage imageNamed:@"guide_1136_2"],
                        [UIImage imageNamed:@"guide_1136_3"]
                         ];
    } else if (IPHONE6) {
        imagesArray = @[
                        [UIImage imageNamed:@"guide_1334_0"],
                        [UIImage imageNamed:@"guide_1334_1"],
                        [UIImage imageNamed:@"guide_1334_2"],
                        [UIImage imageNamed:@"guide_1334_3"]
                        ];
    } else {
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        for (NSInteger k = 0; k < 4; ++k) {
            NSString *imageName = [NSString stringWithFormat:@"guide_2208_%d", (int)k];
            UIImage *originImage = [UIImage imageNamed:imageName];
            UIImage *cutImg =  [self cropAndScaleImage:originImage toSize:self.view.bounds.size];
            [tempArray addObject:cutImg];
        }
        
        imagesArray = tempArray;
    }
    
    CGFloat pageControlOffsetY = 150.f;
    if (IPHONE5X || IPHONE4X || IPHONE6) {
        pageControlOffsetY = 126.f;
    }
    
    [self.imageViewer setImages:imagesArray withPlaceholder:nil];
    self.imageViewer.carouselType = CarouselTypePageNone;
    
    self.pageControl = [[SMPageControl alloc] initWithFrame:CGRectMake(0, pageControlOffsetY, self.view.bounds.size.width, 24)];
    self.pageControl.numberOfPages = [imagesArray count];
    self.pageControl.currentPage = 0;
    
    self.pageControl.pageIndicatorImage = [UIImage imageNamed:@"pageControl-inactive"];
    self.pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"pageControl-active"];
    
    [self.imageViewer addSubview:self.pageControl];
    
    [self.view insertSubview:self.imageViewer atIndex:0];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}


- (UIImage *)cropAndScaleImage:(UIImage *)originImage toSize:(CGSize)destSize
{
    UIImage *retImage;
    UIImage *cropedImage;
    // crop
    CGRect destCropRect;
    if (originImage.size.width / originImage.size.height > destSize.width / destSize.height) {
        double destCropWidth = originImage.size.height * (destSize.width / destSize.height);
        destCropRect = CGRectMake((originImage.size.width - destCropWidth) / 2, 0, destCropWidth, originImage.size.height);
    } else if (originImage.size.width / originImage.size.height < destSize.width / destSize.height) {
        double destCropHeight = originImage.size.width * (destSize.height / destSize.width);
        destCropRect = CGRectMake(0, originImage.size.height - destCropHeight, originImage.size.width, destCropHeight);
    } else {
        destCropRect = CGRectMake(0, 0, originImage.size.width, originImage.size.height);
    }
    
    if (!CGSizeEqualToSize(destCropRect.size, originImage.size)) {
        CGImageRef cgImg = CGImageCreateWithImageInRect(originImage.CGImage, destCropRect);
        cropedImage = [UIImage imageWithCGImage:cgImg];
        CGImageRelease(cgImg);
    } else {
        cropedImage = originImage;
    }
    
    if (cropedImage.size.width != destSize.width) {
        // scale
        UIGraphicsBeginImageContextWithOptions(destSize, YES, [UIScreen mainScreen].scale);
        [cropedImage drawInRect:CGRectMake(0, 0, destSize.width, destSize.height)];
        UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        // Pop the current context from the stack
        UIGraphicsEndImageContext();
        
        retImage = scaledImage;
    } else {
        retImage = cropedImage;
    }
    
    return retImage;
}




/* Maybe used in the future
-(void)imageViewScrollToEnd{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"guide4_view" action:@"slidetohome" label:nil value:nil];
}
*/

- (IBAction)signInAction:(id)sender {
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"welcome_view" action:@"signin" label:nil value:nil];
    HGSignInEmailController *signUp = (HGSignInEmailController *)[[UIStoryboard loginStoryboard] instantiateViewControllerWithIdentifier:@"VCSignIn"];
    
    [self.navigationController pushViewController:signUp animated:YES];
}

- (IBAction)signUpAction:(id)sender {
    
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"welcome_view" action:@"signup" label:nil value:nil];
    HGSignUpEmailController *signUp = (HGSignUpEmailController *)[[UIStoryboard loginStoryboard] instantiateViewControllerWithIdentifier:@"VCSignUp"];
    
    [self.navigationController pushViewController:signUp animated:YES];
}



- (IBAction)onTouchFacebook:(id)sender {
    
    if (![[HGAppData sharedInstance].guestApiClient checkNetworkStatus]) {
        return;
    }
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"Login_view" action:@"loginwithfacebook" label:nil value:nil];
    [[AppsFlyerTracker sharedTracker] trackEvent:@"loginwithfacebook" withValue:@""];
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [[HGFBUtils sharedInstance] loginAccountWithSuccess:^(NSDictionary* response){
        
        [SVProgressHUD dismiss];
        
        [[HGUserData sharedInstance] updateUserLoggedInfo:response withAnonymousSignup:NO withAccountType:HGAccountTypeFacebook];
        [self dismissViewControllerAnimated:NO completion:^{
            
        }];
        
    } withFailure:^(NSError* error){
        [SVProgressHUD dismiss];
        NSLog(@"got error: %@", error);
    }];
}

#pragma mark - X4ImageViewerDataSource
- (NSArray *)imageViewer:(X4ImageViewer *)imageViewer supplementaryViewsFor:(UIImageView *)imageView atIndex:(NSInteger)index{
    
    NSMutableArray *supplementaryViews = [NSMutableArray array];
    
    CGFloat text1Top;
    CGFloat text2Top;
    
    UIFont *fontText1;
    UIFont *fontText2;
    
    if (IPHONE4X || IPHONE5X || IPHONE6) {
        text1Top = 36;
        text2Top = 70.f;
        fontText1 = [UIFont fontWithName:@"Museo-700" size:25];
        fontText2 = [UIFont fontWithName:@"Museo" size:18];
    } else {
        text1Top = 46;
        text2Top = 84.f;
        fontText1 = [UIFont fontWithName:@"Museo-700" size:29];
        fontText2 = [UIFont fontWithName:@"Museo" size:21];
    }
    
    CGRect rectText1 = CGRectMake(0, text1Top, imageView.bounds.size.width, 40);
    
    if(index == 0){
        
        UILabel *text1 = [[UILabel alloc] initWithFrame:rectText1];
        text1.textColor = [UIColor whiteColor];
        text1.textAlignment = NSTextAlignmentCenter;
        text1.font = fontText1;
        text1.text = NSLocalizedString(@"The best way", nil);
        [supplementaryViews addObject:text1];
        
        UILabel *text2 = [[UILabel alloc] initWithFrame:CGRectZero];
        text2.numberOfLines = 0;
        text2.textColor = [UIColor whiteColor];
        text2.textAlignment = NSTextAlignmentCenter;
        text2.font = fontText2;
        text2.text = NSLocalizedString(@"to find preloved stuff and local services", nil);
        [supplementaryViews addObject:text2];
        
        CGSize fitSzie = [text2 sizeThatFits:CGSizeMake(imageView.bounds.size.width, 0)];
        text2.frame = CGRectMake(0, text2Top, imageView.bounds.size.width, fitSzie.height);
        
    }else if(index == 1){
        
        UILabel *text1 = [[UILabel alloc] initWithFrame:rectText1];
        text1.textColor = [UIColor whiteColor];
        text1.textAlignment = NSTextAlignmentCenter;
        text1.font = fontText1;
        text1.text = NSLocalizedString(@"Sell fast",nil);
        [supplementaryViews addObject:text1];
        
        UILabel *text2 = [[UILabel alloc] initWithFrame:CGRectZero];
        text2.numberOfLines = 2;
        text2.textColor = [UIColor whiteColor];
        text2.textAlignment = NSTextAlignmentCenter;
        text2.font = fontText2;
        text2.text = NSLocalizedString(@"Snap a photo and list items in\n seconds", nil);
        [supplementaryViews addObject:text2];
        
        CGSize fitSzie = [text2 sizeThatFits:CGSizeMake(imageView.bounds.size.width, 0)];
        text2.frame = CGRectMake(0, text2Top, imageView.bounds.size.width, fitSzie.height);
        
    }else if(index == 2){
        
        UILabel *text1 = [[UILabel alloc] initWithFrame:rectText1];
        text1.textColor = [UIColor whiteColor];
        text1.textAlignment = NSTextAlignmentCenter;
        text1.font = fontText1;
        text1.text = NSLocalizedString(@"Buy local", nil);
        [supplementaryViews addObject:text1];
        
        UILabel *text2 = [[UILabel alloc] initWithFrame:CGRectZero];
        text2.numberOfLines = 2;
        text2.textColor = [UIColor whiteColor];
        text2.textAlignment = NSTextAlignmentCenter;
        text2.font = fontText2;
        text2.text = NSLocalizedString(@"Find tons of amazing deals right\n next door", nil);
        [supplementaryViews addObject:text2];
        
        CGSize fitSzie = [text2 sizeThatFits:CGSizeMake(imageView.bounds.size.width, 0)];
        text2.frame = CGRectMake(0, text2Top, imageView.bounds.size.width, fitSzie.height);
        
    }else if(index == 3){
        
        UILabel *text1 = [[UILabel alloc] initWithFrame:rectText1];
        text1.textColor = [UIColor whiteColor];
        text1.textAlignment = NSTextAlignmentCenter;
        text1.font = fontText1;
        text1.text = NSLocalizedString(@"Trade safely", nil);
        [supplementaryViews addObject:text1];
        
        UILabel *text2 = [[UILabel alloc] initWithFrame:CGRectZero];
        text2.numberOfLines = 2;
        text2.textColor = [UIColor whiteColor];
        text2.textAlignment = NSTextAlignmentCenter;
        text2.font = fontText2;
        text2.text = NSLocalizedString(@"Chat privately and protect your\n information", nil);
        [supplementaryViews addObject:text2];
        
        CGSize fitSzie = [text2 sizeThatFits:CGSizeMake(imageView.bounds.size.width, 0)];
        text2.frame = CGRectMake(0, text2Top, imageView.bounds.size.width, fitSzie.height);
    }
    
    return supplementaryViews;
}


#pragma mark - X4ImageViewerDelegate

- (void)imageViewer:(X4ImageViewer *)imageViewer didSlideFrom:(UIImageView *)fromImageView fromIndex:(NSInteger)fromIndex to:(UIImageView *)toImageView toIndex:(NSInteger)toIndex{
    
    self.pageControl.currentPage = toIndex;
    
    if(fromIndex == 0 && toIndex == 1){
        [[HGUtils sharedInstance] gaTrackViewName:@"guide2_view"];
    }else if(fromIndex == 1 && toIndex == 2){
        [[HGUtils sharedInstance] gaTrackViewName:@"guide3_view"];
    }else if(fromIndex == 2 && toIndex == 3){
        [[HGUtils sharedInstance] gaTrackViewName:@"guide4_view"];
    }else if(fromIndex == 1 && toIndex == 0){
        [[HGUtils sharedInstance] gaTrackViewName:@"guide1_view"];
    }
}

- (void)imageViewer:(X4ImageViewer *)imageViewer didDoubleTap:(UIImageView *)imageView atIndex:(NSInteger)index inScrollView:(UIScrollView *)scrollView{
    
}
@end

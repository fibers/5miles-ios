//
//  HGMyLikesController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemListController.h"

@interface HGMyLikesController : UITableViewController

@property (nonatomic, strong) NSString * userID;

@end

//
//  HGServiceViewController.m
//  Pinnacle
//
//  Created by mumuhou on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGServiceViewController.h"
#import "HGSegmentViewController.h"
#import "HGSearchResultsController.h"
#import "HGUtils.h"
@interface HGServiceViewController ()

@end

@implementation HGServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self loadUrl:@"https://m.5milesapp.com/app/service"];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"tab_services" label:nil value:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)javascriptResponsedWithHost:(NSString *)host parameters:(NSDictionary *)parameters
{
    [super javascriptResponsedWithHost:host parameters:parameters];
    
    return NO;
}

@end

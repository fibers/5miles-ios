//
//  HGItemListController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemListController.h"
#import "HGHomeItemCell.h"
#import "HGItemDetailController.h"

@interface HGItemListController ()

@end

@implementation HGItemListController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _commonSetup];
}

- (void)_commonSetup
{
    self.items = [@[] mutableCopy];
    self.nextLink = @"";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Custom collection view layout
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    layout.minimumColumnSpacing = 10;
    self.collectionView.collectionViewLayout = layout;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView Datasource & Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGHomeItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShopItemCell" forIndexPath:indexPath];
    
    HGShopItem * item = [self.items objectAtIndex:indexPath.row];
    [cell configWithItem:item];
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGShopItem * item = [self.items objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ShowSingleItem" sender:item];
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGShopItem * item = [self.items objectAtIndex:indexPath.row];
    HGItemImage * coverImage = [item.images firstObject];
    return CGSizeMake(140, ceil(coverImage.height * 140 / coverImage.width) + WATER_FLOW_CELL_EXTEND_HEIGHT);
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowSingleItem"]) {
        HGItemDetailController * vcDest = segue.destinationViewController;
        vcDest.item = sender;
    }
}

@end

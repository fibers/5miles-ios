//
//  main.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-10.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HGAppDelegate class]));
    }
}

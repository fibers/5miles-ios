//
//  HGOfferLine.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGOfferLine.h"

#import "HGUser.h"
#import "HGShopItem.h"

@implementation HGOfferLine

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid", @"description":@"desc"}];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"seller"] || [propertyName isEqualToString:@"buyer"] || [propertyName isEqualToString:@"diggable"]) {
        return YES;
    }
    return NO;
}

@end

@implementation HGOfferLineMeta

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"latest_ts": @"latestTimestamp", @"sold_to_others": @"soldToOthers"}];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"latestTimestamp"] || [propertyName isEqualToString:@"soldToOthers"]) {
        return YES;
    }
    return NO;
}

@end

//
//  HGItemListController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGShopItem.h"

#import "CHTCollectionViewWaterfallLayout.h"
#import <JDStatusBarNotification.h>
#import "SVPullToRefresh.h"

@interface HGItemListController : UICollectionViewController <CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic, strong) NSMutableArray * items;
@property (nonatomic, strong) NSString * nextLink;

@end

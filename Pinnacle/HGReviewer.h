//
//  HGReviewer.h
//  Pinnacle
//
//  Created by Alex on 15-4-14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"


@interface HGReviewer : JSONModel

@property(nonatomic, strong)NSString* uid;
@property(nonatomic, assign)BOOL is_robot;
@property(nonatomic, strong)NSDictionary* location;
@property(nonatomic, strong)NSString* nickname;
@property(nonatomic, strong)NSString* place;
@property(nonatomic, strong)NSString* portrait;
@property(nonatomic, assign)BOOL verified;
@end

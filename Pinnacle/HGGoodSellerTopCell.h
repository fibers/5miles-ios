//
//  HGGoodSellerTopCell.h
//  Pinnacle
//
//  Created by Alex on 15-4-8.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGGoodSellerTopCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) UILabel *descriptionLabel;

@property (nonatomic, strong) UIView* bottomView;
@property (nonatomic, assign) int CellType;

-(void)configCellWithSufficientHint:(NSString *)sufficientHint insufficientHint:(NSString *)insufficientHint;

@end

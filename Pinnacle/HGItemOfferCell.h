//
//  HGItemOfferCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-21.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGSeperatorView.h"
@class HGOfferLine;

@interface HGItemOfferCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic, strong) UILabel * lbDesc;
@property (nonatomic, strong) UIImageView * lbIndicator;
@property (nonatomic, strong) HGSeperatorView * seperatorView;

- (void)configWithOfferLine:(HGOfferLine *)offerline withSeperatorLineHidder:(BOOL)hidden
;

@end

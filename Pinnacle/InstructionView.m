//
//  InstructionView.m
//  Pinnacle
//
//  Created by Alex on 15-4-2.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "InstructionView.h"

@implementation InstructionView
#define upperHeightOFF 5
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //[self _setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    //[self _setup];
}
-(void)startInitView
{
   
    
    if (self.TriangleType == 0) {
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, upperHeightOFF, self.frame.size.width, self.frame.size.height - upperHeightOFF)];
        self.contentView.backgroundColor = [UIColor colorWithRed:0.227f green:0.243f blue:0.255f alpha:1.00f];
        self.contentView.layer.cornerRadius = 5;
        self.contentView.clipsToBounds = YES;
        [self addSubview:self.contentView];
        self.triangleViewHintUp = [[HGTriangleUpView alloc] initWithFrame:CGRectMake(self.frame.size.width - 30, 0, 20, 10)];
        [self addSubview:self.triangleViewHintUp];

    }
    else
    {
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - upperHeightOFF)];
        self.contentView.backgroundColor = [UIColor colorWithRed:0.227f green:0.243f blue:0.255f alpha:1.00f];
        self.contentView.layer.cornerRadius = 5;
        self.contentView.clipsToBounds = YES;
        [self addSubview:self.contentView];
        self.triangleViewHintDown = [[HGTriangleDownView alloc] initWithFrame:CGRectMake(self.frame.size.width - 30, self.frame.size.height - 10, 20, 10)];
        [self addSubview:self.triangleViewHintDown];
    }
    
    self.contentView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *cancelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSelf)];
    [self.contentView addGestureRecognizer:cancelGesture];
}

-(void)removeSelf
{
    [self removeFromSuperview];
}

@end

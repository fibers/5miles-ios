//
//  HGAppInitData.m
//  Pinnacle
//
//  Created by Alex on 15/5/4.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGAppInitData.h"
#import "HGConstant.h"
#import "JSONKit.h"
@implementation HGAppInitData


+ (HGAppInitData *)sharedInstance
{
    static HGAppInitData * _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[HGAppInitData alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.categoryArray_c1 = [[NSMutableArray alloc] init];
        NSString* categoryString = [[NSUserDefaults standardUserDefaults] objectForKey:SERVER_CATEGORY_LIST_V3];
        
        self.category_level1_localIcon = [[NSMutableArray alloc] init];
        
        [self.category_level1_localIcon addObjectsFromArray:@[@"categoryV3_sale", @"categoryV3_service", @"categoryV3_housing", @"categoryV3_job"]];
        
        
        if (categoryString != nil && categoryString.length > 0) {
            NSArray * searchMenus = [categoryString objectFromJSONString];
            [self.categoryArray_c1 removeAllObjects];
            if(searchMenus != nil && searchMenus.count > 0)
            {
                 NSError* error = nil;
                for (int i = 0; i<searchMenus.count; i++) {
                    NSDictionary* searchSingleMenu = [searchMenus objectAtIndex:i];
                    HGCategory* c = [[HGCategory alloc] init];
                    c.index = ((NSString*)[searchSingleMenu objectForKey:@"id"]).intValue;
                    c.title = [searchSingleMenu objectForKey:@"title"];
                    c.icon = [searchSingleMenu objectForKey:@"icon"];
                    
                    if ([searchSingleMenu valueForKey:@"child"]) {
                        NSArray* subCategories = [searchSingleMenu valueForKey:@"child"];
                        
                        for (int j = 0 ; j< subCategories.count; j++) {
                            NSDictionary* subCategoryDic =  [subCategories objectAtIndex:j];
                            HGCategory* c_level2 = [[HGCategory alloc] initWithDictionary:subCategoryDic error:&error];
                            [c.subCategories addObject:c_level2];
                        }
                        
                    }
                    [self.categoryArray_c1 addObject:c];
                }
            }

        }
        else{
            //load from local
            
            NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"new_categories" ofType:@"json"];
            //==Json数据
            NSData *data=[NSData dataWithContentsOfFile:plistPath];
            //==JsonObject
            NSError* error = nil;
            NSDictionary* JsonObject=[NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingAllowFragments
                                                            error:&error];
            
            NSArray* searchMenus = [JsonObject objectForKey:@"objects"];
            
            if(searchMenus != nil && searchMenus.count > 0)
            {
                NSString* categoryJsonString = [searchMenus JSONString];
                [[NSUserDefaults standardUserDefaults] setValue:categoryJsonString forKey:SERVER_CATEGORY_LIST_V3];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self.categoryArray_c1 removeAllObjects];
                for (int i = 0; i<searchMenus.count; i++) {
                    NSDictionary* searchSingleMenu = [searchMenus objectAtIndex:i];
                    NSError * error;
                    HGCategory* c = [[HGCategory alloc] initWithDictionary:searchSingleMenu error:&error];
                    if ([searchSingleMenu valueForKey:@"child"]) {
                        NSArray* subCategories = [searchSingleMenu valueForKey:@"child"];
                        
                        for (int j = 0 ; j< subCategories.count; j++) {
                            NSDictionary* subCategoryDic =  [subCategories objectAtIndex:j];
                            HGCategory* c_level2 = [[HGCategory alloc] initWithDictionary:subCategoryDic error:&error];
                            [c.subCategories addObject:c_level2];
                        }
                        
                    }
                    [self.categoryArray_c1 addObject:c];
                }
            }

        }
        
        

        
        self.currencies = @[@"USD",@"ARS", @"AUD", @"BYR", @"BDT", @"BRL", @"CAD", @"CLP", @"CNY", @"EGP", @"EUR", @"INR", @"IDR", @"IRR", @"JPY", @"MYR", @"MXN", @"NZD", @"NGN", @"PKR", @"PHP", @"RUB", @"ZAR", @"THB", @"TRY", @"UAH", @"GBP",  @"VND"];
        
        
        self.currenciesSymbolDict = @{@"USD":@"US$",
                                      @"ARS":@"ARS",
                                      @"AUD":@"$",
                                      @"BYR":@"BYR",
                                      @"BDT":@"BDT",
                                      @"BRL":@"R$",
                                      @"CAD":@"CA$",
                                      @"CLP":@"CLP",
                                      @"CNY":@"CN¥",
                                      @"EGP":@"EGP",
                                      @"EUR":@"€",
                                      @"INR":@"₹",
                                      @"IDR":@"IDR",
                                      @"IRR":@"IRR",
                                      @"JPY":@"¥",
                                      @"MYR":@"MYR",
                                      @"MXN":@"MX$",
                                      @"NZD":@"NZ$",
                                      @"NGN":@"NGN",
                                      @"PKR":@"PKR",
                                      @"PHP":@"PHP",
                                      @"RUB":@"RUB",
                                      @"ZAR":@"ZAR",
                                      @"THB":@"฿",
                                      @"TRY":@"TRY",
                                      @"UAH":@"UAH",
                                      @"GBP":@"£",
                                      @"VND":@"₫" };
        
        self.default_placehold_colors = @[FANCY_COLOR(@"dcdbde"),
                                          FANCY_COLOR(@"d0c0be"),
                                          FANCY_COLOR(@"c2dcd3"),
                                          FANCY_COLOR(@"d7e0a3"),
                                          FANCY_COLOR(@"e5d7c5"),
                                          FANCY_COLOR(@"fec9be"),
                                          
                                          FANCY_COLOR(@"e1dfed"),
                                          FANCY_COLOR(@"d4e7dc"),
                                          FANCY_COLOR(@"e1d3dd"),
                                          FANCY_COLOR(@"e2c8b7"),
                                          FANCY_COLOR(@"d9d3d6"),
                                          FANCY_COLOR(@"faf3d2"),
                                          
                                          FANCY_COLOR(@"daedee"),
                                          FANCY_COLOR(@"d2dad8"),
                                          FANCY_COLOR(@"e9e7d5"),
                                          FANCY_COLOR(@"c4b9b2"),
                                          FANCY_COLOR(@"cfb0bd"),
                                          FANCY_COLOR(@"eee0f0"),
                                          
                                          FANCY_COLOR(@"a2d4db"),
                                          FANCY_COLOR(@"aedecb"),
                                          FANCY_COLOR(@"b6b6b6"),
                                          FANCY_COLOR(@"dfdba7"),
                                          FANCY_COLOR(@"cae0c2"),
                                          FANCY_COLOR(@"ecd6d4")
                                          ];

    }
    return self;
}

-(int)getParentCategoryID:(NSString*)subCategoryID
{
    
    if (self.categoryArray_c1) {
        for (int i = 0 ; i<self.categoryArray_c1.count; i++) {
            HGCategory* c1 = [self.categoryArray_c1 objectAtIndex:i];
            NSArray* c2Array = c1.subCategories;
            for (int j= 0; j<c2Array.count; j++) {
                HGCategory* c2= [c2Array objectAtIndex:j];
                if (c2.index == subCategoryID.intValue) {
                    return c1.index;
                }
            }
        }
    }

    //return for sale id :1000
    return 1000;
    
}
@end

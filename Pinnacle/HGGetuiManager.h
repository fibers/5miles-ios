//
//  HGGetuiManager.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-2.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GexinSdk.h"

typedef enum {
    SdkStatusStoped,
    SdkStatusStarting,
    SdkStatusStarted
} SdkStatus;

@interface HGGetuiManager : NSObject

@property (nonatomic, strong) GexinSdk * getuiPusher;
@property (retain, nonatomic) NSString *clientId;
@property (assign, nonatomic) SdkStatus sdkStatus;

@property (assign, nonatomic) int lastPayloadIndex;
@property (retain, nonatomic) NSString *payloadId;

+ (HGGetuiManager *)sharedManager;
- (void)startSdk;
- (void)stopSdk;
- (void)setDeviceToken:(NSString *)aToken;
- (BOOL)setTags:(NSArray *)aTags error:(NSError **)error;
- (NSString *)sendMessage:(NSData *)body error:(NSError **)error;

@end

//
//  HGTrackingData.h
//  Pinnacle
//
//  Created by Alex on 15-4-20.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//


///https://github.com/3rdStone/5miles-android/issues/249


#import <Foundation/Foundation.h>


typedef enum
{
    ITME_DETAIL_TRACKSOURCE_HOME = 1,
    ITEM_DETAIL_TRACKSOURCE_SEARCH = 2,
    ITEM_DETAIL_TRACKSOURCE_SEARCH_CATEGORY = 3,
    ITEM_DETAIL_TRACKSOURCE_SUGGEST = 4,
    ITEM_DETAIL_TRACKSOURCE_BRAND_ITEM = 5,
    ITEM_DETAIL_TRACKSOURCE_SEARCH_ITEM = 6,
    ITEM_DETAIL_TRACKSROUCE_USER_LIKES = 7
} ITME_DETAIL_TRACKSOURCE;

@interface HGTrackingData : NSObject

@property (nonatomic, strong)NSString* home_rf_tag;
@property (nonatomic, strong)NSString* search_rf_tag;
@property (nonatomic, strong)NSString* searchCategory_rf_tag;
@property (nonatomic, strong)NSString* suggest_rf_tag;
@property (nonatomic, strong)NSString* brand_item_rf_tag;
@property (nonatomic, strong)NSString* search_items_rf_tag;
@property (nonatomic, strong)NSString* user_likes_rf_tag;

+ (HGTrackingData *)sharedInstance;

@end

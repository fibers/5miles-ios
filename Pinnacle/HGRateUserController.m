//
//  HGRateUserController.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-1.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGRateUserController.h"
#import "HGAppData.h"
#import "HGUtils.h"
#import "UIImage+pureColorImage.h"
#import <AXRatingView.h>
#import <SZTextView.h>
#import "PXAlertView+Customization.h"
#import "HGProfilePhoneViewController.h"
#import "FMDBdatabaseReviewData.h"
#import <SVProgressHUD.h>

@interface HGRateUserController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonPost;
@property (weak, nonatomic) IBOutlet SZTextView *tvReview;
@property (strong, nonatomic) AXRatingView *ratingView;


@property (nonatomic, strong) UIImage* currentImage;
@property (nonatomic, strong) UIImage* tintedImage;

@property (nonatomic, assign) BOOL bNeedAutoPost;
@property (nonatomic, assign) int ReviewInformationIsSending ;
@end

@implementation HGRateUserController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)addBackgroundBoard
{
    self.backgroundBoard1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 123)];
    self.backgroundBoard1.backgroundColor = [UIColor whiteColor];
    [self.view insertSubview: self.backgroundBoard1 atIndex:1];
    
    
    self.backgroundBoard2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0+124+6, self.view.frame.size.width, 138)];
    self.backgroundBoard2.backgroundColor = [UIColor whiteColor];
    [self.view insertSubview:self.backgroundBoard2 atIndex:0];
    
    
    UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-273)/2, 22, 273, 51)];
    [self.view addSubview:title];
    title.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    title.font = [UIFont systemFontOfSize:17];
    title.textAlignment = NSTextAlignmentCenter;
    if (self.userName != nil && self.userName.length > 0) {
        title.text = [@"" stringByAppendingFormat: NSLocalizedString(@"Review your experience with \n %@ here!",nil), self.userName] ;
    }
    else
    {
        title.text = NSLocalizedString(@"Review your experience with this person here!",nil);
    }
    title.numberOfLines = 2;
    
    self.ratingView = [[AXRatingView alloc] initWithFrame:CGRectMake(88 +(SCREEN_WIDTH-320)/2, 71, 334/2, 44)];
    [self.view addSubview:self.ratingView];
    self.ratingView.value = 5;
    self.ratingView.markFont = [UIFont systemFontOfSize:30];
    //self.ratingView.markFont = [UIFont systemFontOfSize:22];
    self.ratingView.baseColor = FANCY_COLOR(@"cccccc");
    self.ratingView.highlightColor = FANCY_COLOR(@"ff8830");
    
}

-(void)FMDB_getReviews
{
    [FMDBdatabaseReviewData initDataBase];

    NSString* keystring = [@"" stringByAppendingFormat:@"%@_%@_%@",[HGUserData sharedInstance].fmUserID,self.itemID,self.userID];
    
    NSDictionary* dictResult = [FMDBdatabaseReviewData getReview:keystring];
    if (dictResult != nil) {
        NSString* content = [dictResult objectForKey:@"review_content"];
        NSString* score = [dictResult objectForKey:@"review_score"];
        if (content!=nil) {
            self.tvReview.text = content;
            [self customizeRightButton_clickable];
        }
        if (score != nil) {
            self.ratingView.value = score.floatValue;
        }
        
    }
    
}
-(void)FMDB_updateReviews
{
    NSString* keystring = [@"" stringByAppendingFormat:@"%@_%@_%@",[HGUserData sharedInstance].fmUserID,self.itemID,self.userID];
    
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    [dict setValue:keystring forKey:@"review_keystring"];
    [dict setValue:self.tvReview.text forKey:@"review_content"];
    [dict setValue:[@"" stringByAppendingFormat:@"%f", self.ratingView.value] forKey:@"review_score"];
    [FMDBdatabaseReviewData insertReviews:dict];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[HGUtils sharedInstance] gaTrackViewName:@"Review_view"];

    self.view.backgroundColor = FANCY_COLOR(@"f0f0f0");
    [self addBackgroundBoard];
    self.tvReview.placeholder = NSLocalizedString(@"Leave a review. You can come back here and change it anytime.(Required)",nil);
    
    [self.tvReview becomeFirstResponder];
    self.tvReview.font = [UIFont systemFontOfSize:15];
    self.tvReview.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.tvReview.delegate = self;
   
    self.ratingView.stepInterval = 1.0;
  
    
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Send", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(onTouchPost:)];
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:SYSTEM_MY_ORANGE,                                                                    NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:14], NSFontAttributeName,nil] forState:UIControlStateNormal];
    
    if (self.tvReview.text.length == 0) {
        [self customizeRightButton_unclickable];
    }

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"RateSeller"];
    
    if(self.bNeedAutoPost && [HGUserData sharedInstance].bUserPhoneVerified)
    {
        [self onTouchPost:nil];
        self.bNeedAutoPost = FALSE;
    }
    if (self.userID != nil && self.itemID != nil) {
        ///todo
        [self getHistoryReviewInformation];
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

-(void)getHistoryReviewInformation
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"review/" parameters:@{ @"target_user": self.userID,@"item_id":self.itemID} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary*dict = responseObject;
        if ([dict objectForKey:@"score"] != nil) {
            float score = ((NSNumber*)[dict objectForKey:@"score"]).floatValue;
            [self.ratingView setValue:score];
            NSString* commentString = [dict objectForKey:@"comment"];
            self.tvReview.text = commentString;
            self.navigationItem.title = NSLocalizedString(@"Change Review",nil);
            [self customizeRightButton_clickable];

        }
        else{
            [self FMDB_getReviews];
        }
        
                                                                                          } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                                                              NSLog(@"get history reviews failed: %@", error.userInfo);
                                                                                              
                                                                                              //[self.navigationController popViewControllerAnimated:YES];
                                                                                          }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"BackToOfferTalk"]) {
        self.success = [sender boolValue];
    }
}

- (IBAction)onTouchCancel:(id)sender {
    
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"Review_view" action:@"review_cancel" label:nil value:nil];
    if (self.ratingView.value == 5.0 && self.tvReview.text.length ==0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Keep editing", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Back to chat", nil),  nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showInView:self.view];
    }
    
}
#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
       if (buttonIndex == 0) {
           [[HGUtils sharedInstance] gaSendEventWithCategory:@"Review_view" action:@"review_backtochat" label:nil value:nil];
        [self FMDB_updateReviews];
        [self.navigationController popViewControllerAnimated:YES];
       }
       else{
           [[HGUtils sharedInstance] gaSendEventWithCategory:@"Review_view" action:@"review_keepediting" label:nil value:nil];
       }
}


- (IBAction)onTouchPost:(id)sender {
    
    
    
    NSNumber * score = [NSNumber numberWithFloat:self.ratingView.value];
    NSString * review = self.tvReview.text;
    
    if (self.userID == nil || self.userID.length == 0) {
        NSLog(@"post reviews an uid is required.");
        return ;
    }
    if (self.itemID == nil || self.itemID.length == 0) {
        return;
    }
    
    if (score.floatValue < 4.0 && ![HGUserData sharedInstance].bUserPhoneVerified) {
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Review_view" action:@"reviewneedverify" label:nil value:nil];
        [PXAlertView setMessageAlignmentLeft];
        PXAlertView *alert = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Bad Experience?",nil) message:NSLocalizedString(@"Please leave your phone number and we'll connect with you to:\n1. Learn about the incident\n2. Validate your review\nYour private information always remains strictly confidential.",nil) cancelTitle:NSLocalizedString(@"Cancel",nil) otherTitle:NSLocalizedString(@"Go",nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            
            if(!cancelled){
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"Review_view" action:@"reviewneedverify_yes" label:nil value:nil];
                HGProfilePhoneViewController *viewcontroller = [[HGProfilePhoneViewController alloc] initWithNibName:@"HGProfilePhoneViewController" bundle:nil];
                viewcontroller.bStartByRateUserView = YES;
                self.bNeedAutoPost = TRUE;
                [self.navigationController pushViewController:viewcontroller animated:YES];
            }
            else{
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"Review_view" action:@"reviewneedverify_no" label:nil value:nil];
            }
        }];
        
        [alert useDefaultIOS7Style];
    }
    else{
        NSNumber* ratingDirection = [NSNumber numberWithInt:self.direction];
        if (self.ReviewInformationIsSending == 1) {
            return;
        }
        else{
            self.ReviewInformationIsSending = 1;
        }
        
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"post_review/" parameters:@{@"score": score, @"comment": review, @"target_user": self.userID,@"item_id":self.itemID,
                                        @"direction": ratingDirection                                                                                     } success:^(NSURLSessionDataTask *task, id responseObject) {
                                            
                                            
                                            self.ReviewInformationIsSending = 0;
                                            [SVProgressHUD dismiss];
                                            [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Thanks for your review.",nil) onView:self.view];
                                            
                                            [[HGUtils sharedInstance] gaSendEventWithCategory:@"Review_view" action:@"review_send" label:nil value:nil];
                                            if (score.intValue == 5 && [HGUserData sharedInstance].bShowRatingAppRemind_FirstReview5star) {
                                                [HGUserData sharedInstance].bShowRatingAppRemind_FirstReview5star = NO;
                                                [[HGUtils sharedInstance] startRatingAppRemind:self];
                                            }
                                           
                                            [self.navigationController popViewControllerAnimated:YES];
        
                                        } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                            NSLog(@"post reviews failed: %@", error.userInfo);
                                            
                                            [SVProgressHUD dismiss];
                                            self.ReviewInformationIsSending = 0;
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }];
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    NSString* contentString = [textView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (contentString.length > 0) {
        [self customizeRightButton_clickable];
    }
    else{
        [self customizeRightButton_unclickable];
    }
}

-(void)customizeRightButton_unclickable
{
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor grayColor],                                                                    NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:14], NSFontAttributeName,nil] forState:UIControlStateNormal];
    
    
    //oops!, UI give too small ICON, we don's want bore too much.
    UIImage* originalImage=[UIImage imageNamed:@"navibarItem-bg"];
    self.currentImage = [originalImage resizableImageWithCapInsets: UIEdgeInsetsMake(10, 20, 30, 20)];
    self.tintedImage = [UIImage getTintedImage:originalImage withColor:[UIColor grayColor]];
    self.tintedImage = [self.tintedImage resizableImageWithCapInsets: UIEdgeInsetsMake(10, 20, 30, 20)];
    
    [self.navigationItem.rightBarButtonItem setBackgroundImage:self.tintedImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.navigationItem.rightBarButtonItem setBackgroundImage:self.tintedImage forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
}

-(void)customizeRightButton_clickable
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:SYSTEM_MY_ORANGE,                                                                    NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:14], NSFontAttributeName,nil] forState:UIControlStateNormal];
    
    
    //oops!, UI give too small ICON, we don's want bore too much.
    UIImage* originalImage=[UIImage imageNamed:@"navibarItem-bg"];
    self.currentImage = [originalImage resizableImageWithCapInsets: UIEdgeInsetsMake(10, 20, 30, 20)];
    self.tintedImage = [UIImage getTintedImage:self.currentImage withColor:[UIColor grayColor]];
    
    [self.navigationItem.rightBarButtonItem setBackgroundImage:self.currentImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.navigationItem.rightBarButtonItem setBackgroundImage:self.currentImage forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
}


@end

//
//  HGVoiceRecordController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-18.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>

@interface HGVoiceRecordController : NSObject

@property (nonatomic, strong) AVAudioRecorder * audioRecorder;
@property (nonatomic, strong) AVAudioPlayer * audioPlayer;
@property (nonatomic, assign) int voiceDuration;
@property (nonatomic, strong) NSURL * soundFileURL;

- (id)initWithGaugeView:(UIView *)gaugeView;

-(void)setRecordVoiceStatusView:(UIView*)view;

- (void)startRecording;
- (void)stopRecording;
- (void)playRecording;
- (void)stopPlaying;
- (void)pausePlaying;
- (void)resumePlaying;

@end

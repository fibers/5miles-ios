//
//  HGFBUtils.h
//  Pinnacle
//
//  Created by shengyuhong on 15/4/1.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

#define ErrorDomainFacebook @"com.pinnacle.facebook"

typedef NS_ENUM(NSUInteger, HGErrorFacebook) {
    HGErrorFacebookRequestPermission = 1000,
    HGErrorFacebookShareShop,
    HGErrorFacebookShareItem,
};



typedef void(^FacebookPermissionsCompletionBlock)(NSError *error);
typedef void(^FacebookSuccessBlock)(NSDictionary* reponse);
typedef void(^FacebookFailureBlock)(NSError* error);

@interface HGFBUtils : NSObject

+ (HGFBUtils *)sharedInstance;

- (void)requestPermissions:(BOOL)isReadPermission withPermissions:(NSArray *)arrayPermissions completion:(FacebookPermissionsCompletionBlock)completionBlock;

- (BOOL)isValidAccessToken:(FBSDKAccessToken *)fbAccessToken;
- (BOOL)isValidProfile:(FBSDKProfile *)fbProfile;

- (void)addObserver;
- (void)removeObserver;

- (void)loginAccountWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure;
- (void)shareShopWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure;
- (void)shareItem:(NSString *)itemID title:(NSString *)title price:(float)price withSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure;
- (void)shareWebLink:(NSString *)linkString  withSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure;





- (void)verifyAccountWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure;;
- (void)unverifyAccountWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure;;
@end

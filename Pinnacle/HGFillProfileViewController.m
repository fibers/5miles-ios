//
//  HGFillProfileViewController.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGFillProfileViewController.h"
#import "HGAppData.h"
#import "HGUtils.h"

#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <SVProgressHUD.h>
#import "UIImage+Scale.h"

@interface HGFillProfileViewController () <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLUploaderDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UIButton *btnChoosePhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnTakePhoto;

@property (nonatomic, strong) NSString * avatarLink;


@end

@implementation HGFillProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"Add Your Photo", nil);
    
    // Configure text
    self.lbDescription.text = NSLocalizedString(@"Your real photo\nhelps members feel secure.", nil);
    [self.btnChoosePhoto setTitle:NSLocalizedString(@"Select from album", nil) forState:UIControlStateNormal];
    [self.btnTakePhoto setTitle:NSLocalizedString(@"Take a photo", nil) forState:UIControlStateNormal];
    
    // Configure font
    self.lbDescription.font = [UIFont systemFontOfSize:17];
    self.btnChoosePhoto.titleLabel.font = self.btnTakePhoto.titleLabel.font = [UIFont systemFontOfSize:18];
    
    // Configure style
    self.lbDescription.textColor = FANCY_COLOR(@"242424");
    self.lbDescription.textAlignment = NSTextAlignmentCenter;
    self.lbDescription.numberOfLines = 0;
    
    self.btnChoosePhoto.layer.borderColor = self.btnTakePhoto.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.btnChoosePhoto.layer.borderWidth = self.btnTakePhoto.layer.borderWidth = 1.0;
    self.btnChoosePhoto.layer.cornerRadius = self.btnTakePhoto.layer.cornerRadius = 3.0;
    [self.btnChoosePhoto setTitleColor:FANCY_COLOR(@"242424") forState:UIControlStateNormal];
    [self.btnTakePhoto setTitleColor:FANCY_COLOR(@"242424") forState:UIControlStateNormal];
    
    // Configure image
    self.imgAvatar.image = [UIImage imageNamed:@"default-avatar"];
    self.imgAvatar.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.imgAvatar.layer.borderWidth = 1.0;
    self.imgAvatar.layer.cornerRadius = CGRectGetWidth(self.imgAvatar.frame) * 0.5;
    self.imgAvatar.clipsToBounds = YES;
    
    self.navigationItem.rightBarButtonItem.tintColor = FANCY_COLOR(@"818181");
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"signup_view" action:@"photo_skip" label:nil value:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UI Control Actions


- (IBAction)onTouchChoosePhoto:(id)sender {
    
    UIImagePickerController * controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    controller.delegate = self;
    controller.allowsEditing = YES;
    
    [self.navigationController presentViewController:controller animated:YES completion:nil];

}

- (IBAction)onTouchTakePhoto:(id)sender {
    
    UIImagePickerController * controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    controller.delegate = self;
    controller.allowsEditing = YES;
    
    [self.navigationController presentViewController:controller animated:YES completion:nil];
}

- (IBAction)onTouchSkip:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString * mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(__bridge NSString *)kUTTypeImage]) {
        
        UIImage *originalImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
        UIImageWriteToSavedPhotosAlbum(originalImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
        UIImage * headImage = [info objectForKey:UIImagePickerControllerEditedImage];
        headImage = [headImage scaleToSize:self.imgAvatar.bounds.size];
        [self.imgAvatar setImage:headImage];
        
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"sign_image_upload/" parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
            NSDictionary * signedRequest = responseObject;
            
            [[HGUtils sharedInstance] uploadPhoto:headImage withSignedOptions:signedRequest delegate:self];
            
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"sign image upload failed: %@", error);
            
        }];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    
    if (error){
        NSLog(@"Save the image to album failed");
    }else{
        NSLog(@"Save the image to album successfully.");
    }
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Cloudinary Upload Delegate

- (void)uploaderSuccess:(NSDictionary *)result context:(id)context
{
    self.avatarLink = [result objectForKey:@"url"];
    
    if (self.imgAvatar.image == nil) {
        PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Please select an image.", nil)];
        [view useDefaultIOS7Style];
        return;
    }
   
    if (nil == self.avatarLink) {
        self.avatarLink = @"";
    }
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"fill_profile/" parameters:@{@"portrait":self.avatarLink} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [SVProgressHUD dismiss];
        [HGUserData sharedInstance].userDisplayName = [responseObject objectForKey:@"nickname"];
        [HGUserData sharedInstance].userAvatarLink = [responseObject objectForKey:@"portrait"];
        
        [self dismissViewControllerAnimated:NO completion:^{
            
        }];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [SVProgressHUD dismiss];
       
    }];
    
    NSLog(@"upload image ok with link: %@", self.avatarLink);
}

- (void)uploaderError:(NSString *)result code:(NSInteger)code context:(id)context
{

    [SVProgressHUD dismiss];
    NSLog(@"upload listing failed with error: %@", result);
    PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:result];
    [view useDefaultIOS7Style];
    NSLog(@"upload image error: %@", result);
}


@end

//
//  HGSystemThemeItemCell.m
//  Pinnacle
//
//  Created by Alex on 14-12-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSystemThemeItemCell.h"
#import "HGUtils.h"
#import "HGAppData.h"
#import <SDWebImage/UIImageView+WebCache.h>
@implementation HGSystemThemeItemCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self _commonSetup];
}


-(void)_commonSetup
{
   
    self.ImageView.contentMode =  UIViewContentModeScaleAspectFill;//UIViewContentModeCenter;
     self.ImageView.clipsToBounds = YES;
    
    self.ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 270)];
    
    
    [self addSubview:self.ImageView];
    
    
    self.textView = [[UIView alloc] initWithFrame:CGRectMake(0, self.ImageView.frame.size.height, self.frame.size.width, 66)];
    [self addSubview: self.textView];
    
    self.SeperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, 66, self.frame.size.width, 0.5)];
    self.SeperatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    [self.textView addSubview:self.SeperatorView];
    [self setUpTextView];
}

-(void)setUpTextView
{
   
    self.ItemTitle = [[UILabel alloc] initWithFrame:CGRectMake(11, 0, self.frame.size.width, 30)];
    
    self.listPrice =[[UILabel alloc] initWithFrame:CGRectMake(11, 28, 100, 20)];
    self.originalPrice = [[StrikeThroughLabel alloc] initWithFrame:CGRectMake(10 + self.listPrice.frame.size.width, 26, self.frame.size.width, 20)];
    self.originalPrice.strikeThroughEnabled = YES;
    
    self.locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(11, 50, 16/2, 23/2)];
    self.locationIcon.image = [UIImage imageNamed:@"item_detail_location"];
    self.distanceLabel = [[UILabel alloc ] initWithFrame:CGRectMake(11+ 8 + 4, 45, 200, 20)];
    
    self.ItemTitle.textColor = FANCY_COLOR(@"242424");
    self.listPrice.textColor = FANCY_COLOR(@"242424");
    self.distanceLabel.textColor = FANCY_COLOR(@"242424");
    self.originalPrice.textColor = self.originalPrice.strikeColor = FANCY_COLOR(@"818181");
    
    self.ItemTitle.font = [UIFont systemFontOfSize:12];
    self.listPrice.font = self.originalPrice.font = [UIFont systemFontOfSize:15];
    self.distanceLabel.font = [UIFont systemFontOfSize:10];
    
    [self.textView addSubview:self.ItemTitle];
    [self.textView addSubview:self.listPrice];
    [self.textView addSubview: self.originalPrice];
    [self.textView  addSubview:self.locationIcon];
    [self.textView addSubview:self.distanceLabel];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.listPrice sizeToFit];
    self.originalPrice.frame = CGRectMake(self.listPrice.frame.size.width + 11 + 5, self.originalPrice.frame.origin.y, self.originalPrice.frame.size.width, self.originalPrice.frame.size.height);
    
}
-(void)configCell:(HGShopItem*)itemDetail
{
    
    if (itemDetail != nil) {
        
        
        HGItemImage * coverImage = [itemDetail.images firstObject];
      
        int width = 640 ;
        int height = coverImage.height *640/coverImage.width ;// * width /width ;
        
        NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:coverImage.imageLink width:width height:height];
        UIColor* color = [[HGAppData sharedInstance].AppInitDataSingle.default_placehold_colors objectAtIndex:rand()%24];
        UIImage*fancyImage = [UIImage imageNamed:@"icon-empty"];
        self.imageView.backgroundColor = color;
        [self.ImageView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:fancyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error) {
                NSLog(@"load image error: %@", error);
            }
            else{
                image = [[HGUtils sharedInstance] centerCropImage:image];
                self.ImageView.image = image;
            }
        }];

        
        self.ItemTitle.text = itemDetail.title;
        self.listPrice.text = [itemDetail priceForDisplay];
        self.originalPrice.text = [itemDetail originalPriceForDisplay];
        // Set location button title
        NSString *dist;
        NSString* distance = [itemDetail formatDistance];
        NSString* address = [itemDetail formatAddress];
        
        if( distance.length > 0 && address.length > 0){
            dist = [NSString stringWithFormat:@"%@, %@", distance, address];
        }else if( distance.length > 0 && address.length == 0){
            dist = [NSString stringWithFormat:@"%@", distance];
        }else if( distance.length == 0 && address.length > 0){
            dist = [NSString stringWithFormat:@"%@", address];
        }else{
            dist = NSLocalizedString(@"unknown", nil);
        }
        
        self.distanceLabel.text = dist;
        
    }

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  HGItemHeaderCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "HGShopItem.h"
#import "HGUserBannerView.h"
#import "HGUserHoverView.h"
#import "HGAppData.h"
//#import <FAKFontAwesome.h>
#import <UIColor+FlatColors.h>
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import "HGAppDelegate.h"
#import "HGUtils.h"
#import "HGNoTouchActionView.h"
#import "UIImage+pureColorImage.h"
#import "StrikeThroughLabel.h"
#import <MapKit/MapKit.h>
#import "X4ImageViewer.h"

@class HGItemDetail;
@class HGItemCanvasView;

@protocol HGItemHeaderActionDelegate <NSObject>

- (void)onDistanceButtonTapped;
- (void)onUserBannerTapped;
- (void)onTouchBrandString:(NSString*)brandIDString withBrandString:(NSString*)brandName;

- (void)onTouchLikeButton;
-(void)onTouchShareButton;
-(void)onTouchMakeOfferButton;
-(void)onTouchEditButton;
-(void)onTouchAskButton;
-(void)onTouchRenewButton;
@end

@interface HGItemHeaderCell : UICollectionViewCell
@property (nonatomic, strong) HGUserHoverView * userView;
@property (nonatomic, strong) UILabel* userDisplayName;

@property (nonatomic, strong) UIImageView* UserRatingView;
@property (nonatomic, strong) UILabel* UserRatingLabel;


@property (nonatomic, strong) X4ImageViewer *imageViewer;
@property (nonatomic, strong) UIImageView * soldIconView;
@property (nonatomic, strong) UIImageView* unNormalStateIcon;
@property (nonatomic, strong) UIImageView* itemNewIcon;



@property (nonatomic, strong) HGNoTouchActionView * blackview;

@property (nonatomic, strong) HGItemDetail * itemDetail;
@property (nonatomic, assign) id<HGItemHeaderActionDelegate> delegate;
@property (nonatomic, strong) AVAudioPlayer * audioPlayer;



//{{for user action View
@property (nonatomic, strong) UIView* userActionView;
@property (nonatomic, strong) UIButton* button_like;
@property (nonatomic, strong) UIButton* button_buy;
@property (nonatomic, strong) UIButton* button_ask;
@property (nonatomic, strong) UIImageView* button_askIcon;
@property (nonatomic, strong) UIView* SeperatorView;

@property (nonatomic,strong)  UIButton * button_edit;
@property (nonatomic, strong) UIImageView* editButton_icon;


@property (nonatomic, strong) UIButton* button_renew;
@property (nonatomic, strong) UILabel* button_renew_timelabel;
@property (nonatomic, strong) UILabel* button_renew_textlabel;

@property (nonatomic, strong) UIButton* button_sold;
//}}end of user action view




//{{price view
@property (nonatomic, strong) UIView* priceView;
@property (nonatomic, strong) StrikeThroughLabel* originalPrice;
@property (nonatomic, strong) UILabel* currentPrice;
//}}end of price view


//{{ location view
@property (nonatomic, strong) UIView* locationView;
@property (nonatomic, strong) UIImageView* locationIcon;
@property (nonatomic, strong) UILabel * locationTitle;
@property (nonatomic, strong) UILabel * locationContent;
@property (nonatomic, strong) UIView* locationSeperator;
//}} end of location view


//{{ static map view
@property (nonatomic, strong) UIView* staticMapView;
@property (nonatomic, strong) UIButton* mapImage;

//}} end of static map view


//{{for delivery View
@property (nonatomic, strong) UIView* deliveryView;
@property (nonatomic, strong) UIImageView *deliveryICON;
@property (nonatomic, strong) UILabel* deliveryContent;
@property (nonatomic, strong) UILabel* deliveryTitle;
@property (nonatomic, strong) UIView* deliverySeperator;
//}}end of delivery

//{{for item create time View
@property (nonatomic, strong) UIView* createTimeView;
@property (nonatomic, strong) UIImageView* createTimeIcon;
@property (nonatomic, strong) UILabel* createTimeTitle;
@property (nonatomic, strong) UILabel* createTimeContent;
@property (nonatomic, strong) UIView* createTimeSeperator;
//}} end of item create time 

//{{for audio status view

@property (nonatomic, strong) UIView* audioView;
@property (nonatomic, strong) UIView* audioView_grayArea;
@property (nonatomic, strong) UIButton * btnPlaySound;
@property (nonatomic, strong) UILabel* audioLabel1;
@property (nonatomic, strong) UILabel* audioLabel2;
@property (nonatomic, strong) UISlider* audioSlide;
@property (nonatomic, strong) NSTimer* updateSlideTimer;
//}}end of audio status view

//{{for brand
@property (nonatomic, strong) UIButton* brandButton;
@property (nonatomic, strong) UIImageView* brandEndIcon;
//}}end of brand
@property (nonatomic, strong) HGItemCanvasView * canvasView;




/////////////////////funtions//////////////////////////////
-(void)configWithItemDetail:(HGItemDetail *)itemDetail;
-(void)stopPlaying;
-(void)resetUpdateButton;
-(void)disableUpdateButton;
-(void)DimRenewButton;
/////////////////////////////////////////////////////////
@end

@interface HGItemCanvasView : UIView
//avoid circle ref. 
@property (nonatomic, weak) HGItemHeaderCell * ownerCell;

@end
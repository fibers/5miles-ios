//
//  HGSeperatorView.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-23.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSeperatorView.h"

#import "UIColor+FlatColors.h"
#import "HGConstant.h"
@implementation HGSeperatorView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15];
    }
    return self;
}

@end

//
//  MessageCenterHelper.h
//  Pinnacle
//
//  Created by Alex on 15/7/16.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MESSAGE_FRESH_TIME 10
#define SYSTEM_MESSAGE_IMAGE_HEIGHT 0
#define MESSAGE_GET_LIMIT_COUNT 20


typedef NS_ENUM(NSUInteger, HGMessageType) {
    HGMessageTypeBuying,
    HGMessageTypeSelling,
    HGMessageTypeSystem
};
@interface MessageCenterHelper : UIViewController

@end

//
//  HGWebContentController.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-6.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGWebContentController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) NSString * contentLink;

@property (nonatomic, assign) BOOL bShowRightShareButton;
@end

//
//  UserReportTableViewController.m
//  Pinnacle
//
//  Created by Alex on 14-11-12.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "UserReportTableViewController.h"
#import "HGUtils.h"
#import <SVProgressHUD.h>
#import "HGAppData.h"
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "UIImage+pureColorImage.h"
#import "HGAppDelegate.h"
#import "HGReportReason.h"

#define TAG_TEXT_INPUT 1000

@interface UserReportTableViewController ()


@property (nonatomic, strong) UIImageView *selected_image;
@property (nonatomic, strong) UIButton *btnSend;
@property (nonatomic, strong) SZTextView *textView;

@property (nonatomic, strong) NSArray *reasons;
@property (nonatomic, strong) NSNumber *selectedReasonID;
@property (nonatomic, strong) RACCommand *commandReport;

@property (nonatomic, assign) BOOL sendSuccessful;
@end

@implementation UserReportTableViewController

-(void)addRightButton
{
    UIView* rightButtonBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    
    self.btnSend = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 68, 30)];
    self.btnSend.clipsToBounds = YES;
    self.btnSend.layer.cornerRadius = 3;
    self.btnSend.layer.borderWidth = 1;
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnSend.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.btnSend setTitle:NSLocalizedString(@"Send",nil) forState:UIControlStateNormal];
    [self.btnSend setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [self.btnSend setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.btnSend addTarget:self action:@selector(onTouchSend) forControlEvents:UIControlEventTouchUpInside];
    
    [rightButtonBackView addSubview:self.btnSend];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButtonBackView];
    
    [self disableSendButton];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addRightButton];
    self.selectedReasonID = [[NSNumber alloc] initWithInt:-1];
    
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = NO;
    self.tableView.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;

    
   
    

    self.selected_image = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 30
                                                                        , 14, 65/4, 51/4)];
    self.selected_image.image = [UIImage imageNamed:@"cell-selected-icon"];
    
    self.textView = [[SZTextView alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 101)];

    self.textView.delegate = self;
    self.textView.tag = TAG_TEXT_INPUT;
    [self.textView setScrollEnabled:YES];
    self.textView.font = [UIFont systemFontOfSize:14];
    
    switch (self.reportType) {
        case ReportReasonTypeItem:
            [[HGUtils sharedInstance] gaTrackViewName:@"reportitem_view"];
            self.reasons = [HGAppData sharedInstance].reportItemReasons;
            self.navigationItem.title = NSLocalizedString(@"Report Listing", nil);
            self.textView.placeholder = NSLocalizedString(@"Why are you reporting this listing? (Optional)", nil);
             self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
           
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            
            break;
            
        case ReportReasonTypeSeller:
            [[HGUtils sharedInstance] gaTrackViewName:@"reportseller_view"];
            self.reasons = [HGAppData sharedInstance].reportSellerReasons;
            self.navigationItem.title = NSLocalizedString(@"Report person", nil);
            self.textView.placeholder = NSLocalizedString(@"Why are you reporting this person? (Optional)", nil);
             self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            break;
            
        case ReportReasonTypeReview:
            [[HGUtils sharedInstance] gaTrackViewName:@"reportreivew_view"];
            self.reasons = [HGAppData sharedInstance].reportReviewReasons;
            self.navigationItem.title = NSLocalizedString(@"Report Review", nil);
            self.textView.placeholder = NSLocalizedString(@"Why are you reporting this review? (Optional)", nil);

           {
                self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
                
            }
            break;
            
        default:
            break;
    }

    [self updateReportReasons];
    
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillShowNotification object:nil];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
    if (!self.sendSuccessful) {
        switch (self.reportType) {
            case ReportReasonTypeItem:
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"reportitem_view" action:@"reportitemcancel" label:nil value:nil];
                
                break;
            case ReportReasonTypeSeller:
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"reportseller_view" action:@"reportsellercancel" label:nil value:nil];
                
                break;
            case ReportReasonTypeReview:
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"reportreview_view" action:@"reportreviewcancel" label:nil value:nil];
                
                break;
            default:
                break;
        }
    }
}


- (void)updateReportReasons{
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"report_reasons/" parameters:@{@"type": [[NSNumber alloc] initWithInt: self.reportType ]} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary *reason = [(NSArray *)responseObject firstObject];
        
        if(!reason){
            return;
        }

        NSNumber *type = [reason objectForKey:@"type"];
        ReportReasonType reasonType = (ReportReasonType)[type intValue];
        NSArray *arrayReasons = [HGReportReason arrayOfModelsFromDictionaries:[reason objectForKey:@"objects"]];
        
        if(![arrayReasons isEqualToArray:self.reasons]){
            
            self.reasons = arrayReasons;
            [self.tableView reloadData];
            
            switch(reasonType){
                case ReportReasonTypeItem:
                    [HGAppData sharedInstance].reportItemReasons = arrayReasons;
                    break;
                case ReportReasonTypeSeller:
                    [HGAppData sharedInstance].reportSellerReasons = arrayReasons;
                    break;
                case ReportReasonTypeReview:
                    [HGAppData sharedInstance].reportReviewReasons = arrayReasons;
                    break;
                default:
                    break;
            }
            
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];

}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == 0) {
        return [self.reasons count];
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section== 0) {
        return 44;
    }
    else{
        return 103;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *TableSampleIdentifier = [@"TableDeliveryCell" stringByAppendingFormat:@"%d", (int)indexPath.section];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             TableSampleIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:TableSampleIdentifier];
    }
    if (indexPath.section == 0)
    {
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        HGReportReason *reason = (HGReportReason *)[self.reasons objectAtIndex:indexPath.row];
        cell.textLabel.text = reason.name;
        cell.tag = reason.uid;
    }
    else{
        [cell addSubview:self.textView];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;

}


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return;
    }
 
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell addSubview:self.selected_image];
    
    self.selectedReasonID = [[NSNumber alloc] initWithInt:(int)cell.tag];
    [self enableSendButton];
}

#pragma mark - Text view delegate
- (void)textViewDidChange:(UITextView *)textView{
    if(textView.tag == TAG_TEXT_INPUT){
        NSString *input = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
       if([input length] > 0 || [self.selectedReasonID integerValue] > -1){
           [self enableSendButton];
       }else{
           [self disableSendButton];
       }
    }
}


#pragma mark - Helpers


- (void)keyboardWillShow:(NSNotification *)notification{
    
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    NSTimeInterval animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardRect.size.height, 0);
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }];
    
}


- (void)keyboardWillHide:(NSNotification *)notification{
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSTimeInterval animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }];
    
}



- (void)onTouchSend{
 
    switch(self.reportType){
        case ReportReasonTypeItem:
            [self reportItem];
            break;
        case ReportReasonTypeSeller:
            [self reportUser];
            break;
        case ReportReasonTypeReview:
            [self reportReview];
            break;
        default:
            break;
    }
    
}

- (void)reportItem{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"reportItem_view" action:@"ReportItem" label:nil value:nil];
    
    [self disableSendButton];
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"report_bad_item/" parameters:@{@"item_id":self.itemID, @"reason":self.selectedReasonID, @"reason_content":self.textView.text} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self.selectedReasonID = [[NSNumber alloc] initWithInt:-1];
        self.sendSuccessful = TRUE;
        [self.navigationController popViewControllerAnimated:YES];
        [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Reported - we'll look into this soon, thanks.", nil) onView:self.view];

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
       
        [self enableSendButton];
    }];
    
}

- (void)reportUser{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"reportSeller_view" action:@"ReportUser" label:nil value:nil];
    
    [self disableSendButton];
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"report_bad_user/" parameters:@{@"user_id":self.userID, @"reason":self.selectedReasonID, @"reason_content":self.textView.text} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self.selectedReasonID = [[NSNumber alloc] initWithInt:-1];
        self.sendSuccessful = TRUE;
        [self.navigationController popViewControllerAnimated:YES];
        [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Reported - we'll look into this soon, thanks.", nil) onView:self.view];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
       
        [self enableSendButton];
    }];
}


- (void)reportReview{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"reportReview_view" action:@"ReportReview" label:nil value:nil];
    
    [self disableSendButton];
    NSString* reportText = @"";
    if (self.textView.text == nil || self.textView.text.length == 0) {
        reportText = @"";
    }
    else{
        reportText = self.textView.text;
    }
    
    NSNumber* reasonIDNum = self.selectedReasonID;
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"report_review/" parameters:@{@"review_id":self.reviewID, @"reason":reasonIDNum, @"reason_content":reportText} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self.selectedReasonID = [[NSNumber alloc] initWithInt:-1];
        self.sendSuccessful = TRUE;
        [self.navigationController popViewControllerAnimated:YES];
        [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Reported - we'll look into this soon, thanks.", nil) onView:self.view];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self enableSendButton];
    }];
    
}

- (void)enableSendButton{
    self.btnSend.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    [self.btnSend setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)disableSendButton{
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    [self.btnSend setTitleColor:[UIColor grayColor]  forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}


@end

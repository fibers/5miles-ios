//
//  HGSellViewController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-15.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSellViewController.h"
#import "ListingLocationVC.h"
#import "HGPublicPageLayoutModel.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGItemMapController.h"
#import "ListingLocationVC.h"
#import "HGProfilePhoneViewController.h"

@interface HGSellViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, UITextViewDelegate, CTAssetsPickerControllerDelegate, HGPhotoSlotsControllerDelegate, AVAudioPlayerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tfTitle;
@property (weak, nonatomic) IBOutlet SZTextView *szTextDesc;
@property (weak, nonatomic) IBOutlet UILabel *lbBrand;
@property (weak, nonatomic) IBOutlet UILabel *lbInputBrand;
@property (weak, nonatomic) IBOutlet UILabel *lbCategory;
@property (weak, nonatomic) IBOutlet UILabel *lbInputCategory;
@property (weak, nonatomic) IBOutlet UILabel *lbDelivery;
@property (weak, nonatomic) IBOutlet UILabel *lbInputDelivery;
@property (weak, nonatomic) IBOutlet UILabel *lbListingPrice;
@property (weak, nonatomic) IBOutlet UITextField *tfListingPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbOriginalPrice;
@property (weak, nonatomic) IBOutlet UITextField *tfOriginalPrice;

@property (nonatomic, strong) HGPublicPageLayoutModel *cellLayoutModel;


@property (nonatomic, strong)UILabel* fakeTitleView;


@property (nonatomic, strong) HGHorizontalTableView * photosTableView;
@property (nonatomic, strong) HGPhotoSlotsController * photoTableController;

@property (nonatomic, strong) NSMutableArray * imageLinks;
@property (nonatomic, assign) int CoverImageObjIndex ;
@property (nonatomic, strong) NSString* CoverImageUUID;

@property (nonatomic, strong) NSMutableArray * uploadSignals;
@property (nonatomic, strong) NSMutableDictionary * imgDict;

@property (nonatomic, strong) HGVoiceRecordController * recordController;
@property (nonatomic, strong) NSString * mediaLink;
@property (nonatomic, strong) CLUploader * soundUploader;
@property (nonatomic, strong) RACSubject * soundSub;

@property (nonatomic, assign) NSInteger selectedCatID;
@property (nonatomic, strong )NSString* itemIDstring;

@property (nonatomic, strong) NSMutableArray *uploadArray;

@property (nonatomic, strong) NSMutableArray *selectedAssets;

@property (nonatomic, assign) NSInteger rootCategory;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *region;

//////////////////////////////////////////////////////////////////////
@property (nonatomic, strong) NSString* postTitle;
@property (nonatomic, strong) NSString* postJsonImages;
@property (nonatomic, assign) float  postPrice;

@end

#define ITEM_TITLE_DESC 6
#define PRICE_TEXTFIELD_TAG 7
#define ORIGINAL_PRICE_TEXTFIELD_TAG 8

/////////////////////////////////////
#define ACTION_TAG_ADD_PHOTO 0
#define PHOTO_DELETE_CONFIRM_ACTION_TAG 1
#define QUIT_SELL_ITEM_CONFIRM_ACTION_TAG 3
#define SHARE_FRIEND_ACTION_TAG 4
#define AUDIO_DELETE_ATCION_TAG 9
#define REBIND_FACEBOOK_REMIND_ATION_TAG  10


@implementation HGSellViewController
{
    NSInteger _selectedImageIndex;
    UIResponder * _focusControl;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _cellLayoutModel = [[HGPublicPageLayoutModel alloc] init];
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.EditModel = 0;
        self.CoverImageObjIndex = 0;
        self.CoverImageUUID = @"";
    }
    return self;
}
-(void)setCellAccessIcon:(UITableViewCell*)cell
{
    UIImageView*accView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 17/2, 30/2)];
    accView.image = [UIImage imageNamed:@"uitableview_cell_access"];
    cell.accessoryView = accView;
    
}




- (void)customizeNavigationBar{
    
    UIImage* leftImage = [UIImage imageNamed:@"sell_closeIcon"];
    
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],                                                                    NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:14], NSFontAttributeName,nil] forState:UIControlStateNormal];
    [self.navigationItem.leftBarButtonItem setBackgroundImage:leftImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.navigationItem.leftBarButtonItem setBackgroundImage:leftImage forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
    
    NSString* titleString = NSLocalizedString(@"Listing", nil);
    self.fakeTitleView = [[UILabel alloc] initWithFrame:CGRectMake(60, 7, SCREEN_WIDTH-60*2, 30)];
    self.fakeTitleView.textAlignment = NSTextAlignmentCenter;
    self.fakeTitleView.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.fakeTitleView.font = [UIFont boldSystemFontOfSize:16];
    self.fakeTitleView.text = titleString;
    self.navigationItem.title = @"";
    [self.navigationController.navigationBar addSubview: self.fakeTitleView];
    
    
    
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:SYSTEM_MY_ORANGE,                                                                    NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:14], NSFontAttributeName,nil] forState:UIControlStateNormal];
    //oops!, UI give too small ICON, we don's want bore too much.
    UIImage* originalImage=[UIImage imageNamed:@"navibarItem-bg"];
    UIImage* rightImage = [originalImage resizableImageWithCapInsets: UIEdgeInsetsMake(10, 20, 30, 20)];
    [self.navigationItem.rightBarButtonItem setBackgroundImage:rightImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.navigationItem.rightBarButtonItem setBackgroundImage:rightImage forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    [self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Publish", nil)];
    
}

-(void)setTextStyle
{
    // Font size
    self.tfTitle.font = self.lbBrand.font = self.lbInputBrand.font = self.lbCategory.font = self.lbInputCategory.font = self.lbDelivery.font = self.lbInputCategory.font = self.lbListingPrice.font = self.locationTitle.font = self.tfListingPrice.font = self.lbOriginalPrice.font = self.tfOriginalPrice.font = [UIFont systemFontOfSize:14.0f];
    self.szTextDesc.font = [UIFont systemFontOfSize:12.0f];
    self.audio_userHintLabel.font = [UIFont systemFontOfSize:15.0f];
    self.shareOnFacebookLabel.font = [UIFont systemFontOfSize:16.0f];
    
    self.tfTitle.textColor = self.locationTitle.textColor = self.szTextDesc.textColor = self.lbBrand.textColor = self.lbCategory.textColor = self.lbDelivery.textColor = self.lbListingPrice.textColor = self.tfListingPrice.textColor = self.lbOriginalPrice.textColor = self.tfOriginalPrice.textColor = self.audio_userHintLabel.textColor = self.shareOnFacebookLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    
    // Text Style
    self.lbBrand.textAlignment = self.lbCategory.textAlignment = self.lbDelivery.textAlignment = self.lbListingPrice.textAlignment = self.lbOriginalPrice.textAlignment = NSTextAlignmentRight;
    self.audio_userHintLabel.textAlignment = NSTextAlignmentLeft;
    self.shareOnFacebookLabel.textAlignment = NSTextAlignmentCenter;
    
    // Set placeholder
    NSString *tfTitlePlaceholder = [NSString stringWithFormat:@"%@(%@)", NSLocalizedString(@"Title", nil), NSLocalizedString(@"Required", nil)];
    self.tfTitle.attributedPlaceholder = [[NSAttributedString alloc]initWithString:tfTitlePlaceholder attributes:@{NSForegroundColorAttributeName:FANCY_COLOR(@"cccccc"), NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}];

    self.lbInputBrand.textColor = self.lbInputCategory.textColor = FANCY_COLOR(@"cccccc");
    self.lbInputDelivery.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.lbInputBrand.text = NSLocalizedString(@"Optional", nil);
    self.lbInputCategory.text = NSLocalizedString(@"Required", nil);
    self.lbInputDelivery.text = NSLocalizedString(@"Local exchange or shipping", nil);

    NSString *tfListingPricePlaceholdere = [NSString stringWithFormat:@"%@(%@)", [HGUserData sharedInstance].preferredCurrency, NSLocalizedString(@"Required", nil)];
    self.tfListingPrice.attributedPlaceholder = [[NSAttributedString alloc]initWithString:tfListingPricePlaceholdere attributes:@{NSForegroundColorAttributeName:FANCY_COLOR(@"cccccc"), NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}];
    
    NSString *tfOriginalPricePlaceholder = [NSString stringWithFormat:@"%@(%@)", [HGUserData sharedInstance].preferredCurrency, NSLocalizedString(@"Optional", nil)];
    self.tfOriginalPrice.attributedPlaceholder = [[NSAttributedString alloc]initWithString:tfOriginalPricePlaceholder attributes:@{NSForegroundColorAttributeName:FANCY_COLOR(@"cccccc"), NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}];
    
    self.szTextDesc.placeholderTextColor = FANCY_COLOR(@"cccccc");
    self.szTextDesc.placeholder = NSLocalizedString(@"Add a quick description to attract more interest (Optional):\n1. Work location, such as Dallas, TX.\n2. Work history, such as In business 8 years.\n3. Availibility, such as 9:00am–8:00pm.\n4. Whether you are licensed or not.", nil);

    // Set Text
    self.lbBrand.text = NSLocalizedString(@"Brand", nil);
    self.lbCategory.text = NSLocalizedString(@"Category", nil);
    self.lbDelivery.text = NSLocalizedString(@"Delivery", nil);
    self.lbListingPrice.text = NSLocalizedString(@"Listing Price", nil);
    self.lbOriginalPrice.text = NSLocalizedString(@"Original Price", nil);
    self.audio_userHintLabel.text = NSLocalizedString(@"Hold to record a voice description", nil);
    self.shareOnFacebookLabel.text = NSLocalizedString(@"Share On Facebook", nil);
}

- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning in HGSellViewController");
    [super didReceiveMemoryWarning];
    [[SDImageCache sharedImageCache] clearMemory];
}


-(void)addDivideView:(UITableViewCell*)cell
{
    UIView* SeperatorView = [[UIView alloc] initWithFrame:CGRectMake(16, 43.5, self.view.frame.size.width - 25, 0.5)];
    SeperatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15];
    [cell addSubview:SeperatorView];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.szTextDesc.frame = CGRectMake(self.szTextDesc.frame.origin.x, self.szTextDesc.frame.origin.y, self.view.frame.size.width, self.szTextDesc.frame.size.height);
}
-(void)ios7NavigationBarfixup
{
     if ([[[UIDevice currentDevice] systemVersion] floatValue] <= 7.0)
     {
         [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
     }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ios7NavigationBarfixup];
    [self customizeNavigationBar];
    [self setCellAccessIcon:self.brandCell];
    [self setCellAccessIcon:self.category_cell];
    [self setCellAccessIcon:self.delivery_cell];
    [self setCellAccessIcon:self.locationCell];
    
    [self addDivideView:self.originPriceCell];
    [self addDivideView:self.brandCell];
    [self addDivideView:self.category_cell];
    [self addDivideView:self.listingPrice_cell];
    
    [self setTextStyle];
    
    self.country = [HGAppData sharedInstance].countryString;
    self.region = [HGAppData sharedInstance].regionString;
    self.city = [HGAppData sharedInstance].cityString;
    
    self.locationTitle.text = [self buildLocationTitleWithCountry:self.country region:self.region city:self.city];
    
    self.tableView.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    
    self.itemIDstring = @"";
    [HGAppData sharedInstance].sellingItemBrandString = @"";
    [HGAppData sharedInstance].sellingItemDeliveryString = @"";
    [HGAppData sharedInstance].sellingItemDeliveryType = -1;
    
    CGRect rx = [ UIScreen mainScreen ].bounds;
    self.voiceStatusHud = [[BDKNotifyHUD alloc] initWithImage:[UIImage imageNamed:@"voice_status1"] text:NSLocalizedString(@"Recording",nil)];
    self.voiceStatusHud.center = CGPointMake(self.view.center.x, rx.size.height/2);
    
    
    
    self.photosTableView = [[HGHorizontalTableView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 84) style:UITableViewStylePlain];
    self.photosTableView.backgroundColor = [UIColor clearColor];
    self.photosTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.photosTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.photosTableView.contentInset = UIEdgeInsetsMake(4, 0, 4, 0);
    self.photosTableView.showsHorizontalScrollIndicator = NO;
    self.photosTableView.showsVerticalScrollIndicator = NO;
    
    self.photoTableController = [HGPhotoSlotsController new];
    self.photoTableController.delegate = self;
    self.photosTableView.dataSource = self.photoTableController;
    self.photosTableView.delegate = self.photoTableController;
    
    self.tableView.tableHeaderView = self.photosTableView;
    
    [self audioSlideInit];
    
    UIImage* image = [UIImage imageNamed:@"audio_play"];
    [self.btnPlay setBackgroundImage:image forState:UIControlStateNormal];
    
    self.btnPlay.hidden = YES;
    
    self.recordController = [[HGVoiceRecordController alloc] initWithGaugeView:nil];
    [self.recordController setRecordVoiceStatusView:self.voiceStatusHud];
    
    UILongPressGestureRecognizer *gesture = [[UILongPressGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(recordButtonLongPressed:)];
    gesture.minimumPressDuration = 0.1;
    [self.btnRecord addGestureRecognizer:gesture];
    [self.btnRecord addTarget:self action:@selector(recordButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.imageLinks = [@[] mutableCopy];
    self.uploadSignals = [@[] mutableCopy];
    self.imgDict = [@{} mutableCopy];
    self.mediaLink = @"";
    self.selectedCatID = -1;
    
    if ([HGAppData sharedInstance].bHasItemValue == 1) {
        self.EditModel = 1;
        [HGAppData sharedInstance].bHasItemValue = 0;
        
        [self setContentWithOriginItem];
    }
    
    self.tfTitle.delegate = self;
    self.tfTitle.tag = ITEM_TITLE_DESC;
    
    self.tfListingPrice.delegate = self;
    self.tfListingPrice.tag = PRICE_TEXTFIELD_TAG;

    self.tfOriginalPrice.delegate = self;
    self.tfOriginalPrice.tag = ORIGINAL_PRICE_TEXTFIELD_TAG;
    
    [self.shareOnfacebookSwitch setOnTintColor:FANCY_COLOR(@"ff8830")];
    [self setRACAction];
    
    [self loadInputData];
}

- (void)setRACAction{
    
    RACChannelTerminal *racCTSellingFBShareValue = RACChannelTo([HGUserData sharedInstance], bSellingFBShare, @(YES));
    RACChannelTerminal *racCTSellingFBShareSwitch = [self.shareOnfacebookSwitch rac_newOnChannel];
    [racCTSellingFBShareValue subscribe:racCTSellingFBShareSwitch];
    [[racCTSellingFBShareSwitch takeUntil:self.rac_willDeallocSignal] subscribe:racCTSellingFBShareValue];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"sell_view"];
    [FBSDKAppEvents logEvent:@"sell_view"];

    if (self.fakeTitleView!=nil) {
        self.fakeTitleView.hidden = NO;
    }
    //handle over the facebook share cell;
    if (self.EditModel == 1 ) {
        self.ShareOnFaceBookCell.hidden = YES;
    }
}


-(void)updateAudioSlide
{
    float currentSecond = self.recordController.audioPlayer.currentTime;
    self.audio_label1.text = [@"" stringByAppendingFormat:@"%.0f'", ceil( currentSecond)];
    [self.audio_slide setValue:currentSecond];
}
-(void)audioSlideInit
{
    self.audio_slideView = [[UIView alloc] initWithFrame:CGRectMake(56, 5, self.view.frame.size.width-56-2, 40)];
    [self.voiceRecordCell addSubview:self.audio_slideView];
    self.audio_slideView.backgroundColor = FANCY_COLOR(@"f8f8f8");
    self.audio_slideView.layer.cornerRadius = 2;
    
    
    self.audio_slide = [[UISlider alloc] initWithFrame:CGRectMake(36,6, self.view.frame.size.width-140, 30)];
    [self.audio_slideView addSubview:self.audio_slide];
    
    
    self.audio_label1 = [[UILabel alloc] initWithFrame:CGRectMake(1, 6, 35, 30)];
    self.audio_label2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.audio_slideView.frame)-47, 6, 30, 30)];
    self.audio_label1.textAlignment = self.audio_label2.textAlignment = NSTextAlignmentCenter;
    
    self.audio_label1.textColor = self.audio_label2.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.audio_label1.font = self.audio_label2.font = [UIFont systemFontOfSize:12];
    [self.audio_slideView addSubview:self.audio_label1];
    [self.audio_slideView addSubview:self.audio_label2];
    
    
    self.audio_slide.tintColor = SYSTEM_MY_ORANGE;
    [self.audio_slide setThumbImage:[UIImage imageNamed:@"slider_icon"] forState:UIControlStateNormal];
    [self.audio_slide setThumbImage:[UIImage imageNamed:@"slider_icon"] forState:UIControlStateHighlighted];
    self.audio_slide.userInteractionEnabled = NO;
    
    
    self.btnDelete = [[UIButton alloc] initWithFrame: CGRectMake(CGRectGetWidth(self.audio_slideView.frame)- 21, 10, 35/2, 38/2)];
    [self.btnDelete setBackgroundImage:[UIImage imageNamed:@"audio_delete"] forState:UIControlStateNormal];
    [self.audio_slideView addSubview: self.btnDelete];
    [self.btnDelete addTarget:self action:@selector(onTouchDeleteButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.audio_slide.minimumValue = 0.0;
    self.audio_slideView.hidden = YES;
}

-(void)setContentWithOriginItem
{
    HGShopItem* OriginItem = [HGAppData sharedInstance].editItem;
    
    self.tfTitle.text = OriginItem.title;
    
    self.tfListingPrice.text = [NSString stringWithFormat:@"%.2f", OriginItem.price];
    
    self.szTextDesc.text = OriginItem.desc;
    
    NSMutableString *locationTitle = [[NSMutableString alloc] init];

    if (OriginItem.city.length > 0 && ![OriginItem.city isEqual:@"None"]) {
        locationTitle = [OriginItem.city mutableCopy];
    }
    if (OriginItem.region.length > 0 && ![OriginItem.region isEqual:@"None"]) {
        [locationTitle appendFormat:@" %@", OriginItem.region];
    }
    if (locationTitle.length > 0) {
        self.locationTitle.text = locationTitle;
    }
    
    self.rootCategory = OriginItem.rootCategoryId;
    self.country = OriginItem.country;
    self.city = OriginItem.city;
    self.region = OriginItem.region;
    
    if ([self isListingCategoryId:OriginItem.rootCategoryId]) {
        self.cellLayoutModel.currentLayoutType = HGPublicPageLayoutModelLayoutTypeListing;
    } else {
        self.cellLayoutModel.currentLayoutType = HGPublicPageLayoutModelLayoutTypeService;
    }
    
    HGCategory *categoryItem;
    BOOL findCategory = NO;
    for (HGCategory *rootCategoryItem in [HGAppInitData sharedInstance].categoryArray_c1) {
        for (HGCategory *subCategoryItem in rootCategoryItem.subCategories) {
            if (subCategoryItem.index == OriginItem.categoryIndex) {
                findCategory = YES;
                categoryItem = subCategoryItem;
                break;
            }
        }
        
        if (findCategory) {
            break;
        }
    }

    if (findCategory) {
        self.lbInputCategory.text = categoryItem.title;
    }
    
    self.lbInputCategory.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.selectedCatID = OriginItem.categoryIndex;
    if(fabs(OriginItem.originalPrice - 0) > 0.000001){
        self.tfOriginalPrice.text = [NSString stringWithFormat:@"%.2f", OriginItem.originalPrice];
    }
    
    
    if (OriginItem.brand_name!=nil && OriginItem.brand_name.length >0) {
        self.lbInputBrand.text = OriginItem.brand_name;
        self.lbInputBrand.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    }
    
    //set delivery way
    switch (OriginItem.shipping_method) {
            self.lbInputDelivery.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        case 1:
        {
            self.lbInputDelivery.text = NSLocalizedString(@"Local exchange", nil);
            self.ItemDeliveryType = 1;
            break;
        }
        case 2:
        {
            self.lbInputDelivery.text = NSLocalizedString(@"Shipping", nil);
            self.ItemDeliveryType = 2;
            break;
        }
            
        case 3:
        {
            ////oop, using
            self.lbInputDelivery.text = NSLocalizedString(@"Local exchange or shipping", nil);
            self.ItemDeliveryType = 3;
            break;
        }
            
            
        default:
            break;
    }
    self.itemIDstring = OriginItem.uid;
    
    [self.imageLinks removeAllObjects];
    for (int i =0; i<OriginItem.images.count; i++) {
        HGItemImage* itemImage =  OriginItem.images[i];
        
        NSNumber* widthNumber = [NSNumber numberWithInt:itemImage.width];
        NSNumber* heightNumber = [NSNumber numberWithInt:itemImage.height];
        
        [self.imageLinks addObject:@{@"imageLink": itemImage.imageLink, @"width":widthNumber, @"height":heightNumber, @"uuid":itemImage.imageLink}];
        
        [self.photoTableController addImage:itemImage.imageLink withRemoteUrl:itemImage.imageLink ];
    }
    //[self.photoTableController addImage:resizedPhoto withUUID:imgUuid];
    
    [self.photosTableView reloadData];
    
    
    //if have voice record
    if (OriginItem.mediaLink.length > 0) {
        self.btnRecord.hidden = YES;
        self.iconMicrophone.hidden = YES;
        
        
        //self.recordController.soundFileURL =[NSURL fileURLWithPath: OriginItem.mediaLink];
        self.mediaLink = OriginItem.mediaLink;
        [self startDownloadMedia:OriginItem.mediaLink];
    }
    //
    [HGAppData sharedInstance].editItem = nil;
    
    
}

- (void)startDownloadMedia:(NSString*)mediaLink
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:mediaLink];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *directoryURL = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSString * filename = [URL lastPathComponent];
        if ([URL pathExtension].length == 0) {
            filename = [filename stringByAppendingString:@".m4a"];
        }
        return [directoryURL URLByAppendingPathComponent:filename];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        self.recordController.soundFileURL = filePath;
        self.btnPlay.hidden = NO;
        self.audio_slideView.hidden = NO;
       
        @try {
            self.recordController.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.recordController.soundFileURL error:&error];
            if ([self.recordController.audioPlayer prepareToPlay]) {
                
                self.audio_label1.text = @"0'";
                double durationTime = self.recordController.audioPlayer.duration;
                self.audio_label2.text = [@"" stringByAppendingFormat:@"%.0f'", ceil(durationTime)];
                self.audio_slide.maximumValue = (float)durationTime;
            }
        }
        @catch (NSException *exception) {
             [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];

            NSLog(@"audio prepare may fail");
        }



    }];
    [downloadTask resume];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([HGAppData sharedInstance].sellingItemBrandString.length > 0) {
        self.lbInputBrand.text = [HGAppData sharedInstance].sellingItemBrandString;
        self.lbInputBrand.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    }
    if ([HGAppData sharedInstance].sellingItemDeliveryString.length > 0) {
        
        self.ItemDeliveryType = [HGAppData sharedInstance].sellingItemDeliveryType;
        self.lbInputDelivery.text = [HGAppData sharedInstance].sellingItemDeliveryString;
        self.lbInputDelivery.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [self.view endEditing:YES];
    
    if (self.fakeTitleView!=nil) {
        self.fakeTitleView.hidden = YES;
    }
    
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    switch (appDelegate.tabController.lastSelectedButtonNumber) {
        case 1:
            [appDelegate.tabController button1pressed];
            break;
        case 2:
            [appDelegate.tabController button2pressed];
            break;
        case 3:
            break;
        case 4:
            [appDelegate.tabController button4pressed];
            break;
        case 5:
            [appDelegate.tabController button5pressed];
            break;
        default:
            break;
    }
    
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if ([self.recordController.audioPlayer play]) {
        [self.recordController stopPlaying];
        
        UIImage*image = [UIImage imageNamed:@"audio_play"];
        [self.btnPlay setBackgroundImage:image forState:UIControlStateNormal];
    }
    
}

#pragma mark - PhotoSlots Table Controller Delegate

- (void)handleTapAddPhoto
{

    UIActionSheet * actionSheet;
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                destructiveButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Take a photo", nil), NSLocalizedString(@"Select from album", nil), nil];
    
    actionSheet.tag = ACTION_TAG_ADD_PHOTO;
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];

}

-(void)showCamera
{
    UIImagePickerController * vcCamera = [UIImagePickerController new];
    vcCamera.sourceType = UIImagePickerControllerSourceTypeCamera;
    vcCamera.mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeImage, nil];
    //[vcCamera setAllowsEditing:YES];
    
    vcCamera.showsCameraControls = YES;
    vcCamera.delegate = self;
    
    [self presentViewController:vcCamera animated:YES completion:nil];
}

- (void)handleTapPhotoImageAtSlot:(NSInteger)slotIndex
{
    NSLog(@"click Photo Slot %d", (int)slotIndex);
    _selectedImageIndex = slotIndex;
    UIActionSheet * actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:NSLocalizedString(@"Delete Photo", nil), NSLocalizedString(@"Set as First Image", nil), nil];
    actionSheet.tag = PHOTO_DELETE_CONFIRM_ACTION_TAG;
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}

-(void)addCancelSellingActionSheet
{
    UIActionSheet * actionSheet;
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"Keep editing", nil)
                                destructiveButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Cancel & go back", nil),
//                   NSLocalizedString(@"Save draft & go back", nil),
                   nil];
    
    actionSheet.tag = QUIT_SELL_ITEM_CONFIRM_ACTION_TAG;
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}
-(void) addShareToFriendActionSheet
{
    UIActionSheet * actionSheet;
    actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Sharing on Facebook leads to a faster sale! Do you want to link Facebook with 5miles now? #5milesapp", nil)
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"No", nil)
                                destructiveButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Yes", nil),  nil];
    
    actionSheet.tag = SHARE_FRIEND_ACTION_TAG;
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //remove image
    if (actionSheet.tag == PHOTO_DELETE_CONFIRM_ACTION_TAG) {
        if (buttonIndex == 0) {
            
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"photo_delete" label:nil value:nil];
            
            NSString * imgUuid = [self.photoTableController removeImageAtIndex:_selectedImageIndex];
            [self.photosTableView reloadData];
            
            [self.selectedAssets removeObjectAtIndex:_selectedImageIndex];
            
            CLUploader * uploader = [[self.imgDict objectForKey:imgUuid] objectForKey:@"uploader"];
            [uploader cancel];
            
            if (self.imageLinks.count > 0 ) {
                if (self.imageLinks.count > _selectedImageIndex) {
                    [self.imageLinks removeObjectAtIndex:_selectedImageIndex];
                }
                else{
                    //it may possible the image is not finishing upload yet.
                    
                }
            }
            
            
            
            RACSubject * sub = [[self.imgDict objectForKey:imgUuid] objectForKey:@"signal"];
            [sub sendCompleted];
        }
        if (buttonIndex == 1) {
            
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"photosetcover" label:nil value:nil];
            
            self.CoverImageObjIndex = (int)_selectedImageIndex;
            NSMutableDictionary* photoTableItemDict = [self.photoTableController.images objectAtIndex:_selectedImageIndex];
            self.CoverImageUUID = [photoTableItemDict objectForKey:@"uuid"];
            
            [self.photoTableController removeImageAtIndex:_selectedImageIndex];
            [self.photoTableController insertImageAsFirstCover:photoTableItemDict];
            [self.photosTableView reloadData];
            
            
        }
        return;
    }
    if (actionSheet.tag == QUIT_SELL_ITEM_CONFIRM_ACTION_TAG) {
        //quit uploading
        if (buttonIndex==0) {
            [self performSellCancel];
        }
//        else if (1 == buttonIndex) {
//            [self saveInputDataToCache];
//            [self performSellCancel];
//        }
        else if (1 == buttonIndex) {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"keep_editing" label:nil value:nil];
        }
        return ;
    }
    
    if (actionSheet.tag == SHARE_FRIEND_ACTION_TAG) {
        //share to friends
        if (buttonIndex == 0) {
            //user choose share yes
            [self postSellItem:self.postTitle withPrice:self.postPrice withImages:self.postJsonImages];
        }
        if (buttonIndex==1) {
            //user choose share no
            [HGUserData sharedInstance].bSellingFBShare = NO;
            [self postSellItem:self.postTitle withPrice:self.postPrice withImages:self.postJsonImages];
        }
        return;
    }
    if (actionSheet.tag == AUDIO_DELETE_ATCION_TAG) {
        if (buttonIndex == 0) {
            [self ConfirmDeleteAudio];
        }
        return;
    }
    
    
    if (actionSheet.tag == ACTION_TAG_ADD_PHOTO) {
        switch (buttonIndex) {
            case 0:
            {
                BOOL cameraAvailableFlag = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
                if (cameraAvailableFlag)
                    [self performSelector:@selector(showCamera) withObject:nil afterDelay:0.3];
                
                [[HGUtils sharedInstance] gaTrackViewName:@"photoalbum_view"];
               
            }
                break;
            case 1:
            {
                CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
                picker.assetsFilter = [ALAssetsFilter allPhotos];
                picker.delegate = self;
                
                [self presentViewController:picker animated:YES completion:nil];
                
            }
                break;
            default:
                break;
        }

    }
}

#pragma mark- cache input data

- (void)loadInputData {
    HGSellPageInputData *inputData = [HGSellPageInputDataManager loadFromLocal];
    if (inputData) {
        self.tfTitle.text = inputData.title;
        
        if (inputData.brand.length > 0) {
            self.lbInputBrand.text = inputData.brand;
            self.lbInputBrand.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        }
        
        if (inputData.category.length > 0) {
            self.lbInputCategory.text = inputData.category;
            self.lbInputCategory.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        }
        
        self.lbInputDelivery.text = inputData.delivery;
        self.tfListingPrice.text = inputData.listingPrice;
        
        self.tfOriginalPrice.text = inputData.originalPrice;
        self.rootCategory = inputData.rootCategoryId;
        self.selectedCatID = inputData.categoryId;
        self.ItemDeliveryType = (int)inputData.deliveryType;
        
        self.city = inputData.city;
        self.country = inputData.country;
        self.region = inputData.region;
        
        self.szTextDesc.text = inputData.itemDescription;
        
        NSMutableString *locationTitle = [[NSMutableString alloc] init];
        if (self.city.length > 0 && ![self.city isEqual:@"None"]) {
            locationTitle = [self.city mutableCopy];
        }
        if (self.region.length > 0 && ![self.region isEqual:@"None"]) {
            if (!self.city || ![self.region isEqualToString:self.city]) {
                [locationTitle appendFormat:@" %@", self.region];
            }
        }
        if (locationTitle.length > 0) {
            self.locationTitle.text = locationTitle;
        }

        // load assets
        
        __weak __typeof(self)weakSelf = self;
        
        [[HGAssetsHelper sharedInstance] getSavedPhotoList:^(NSArray *assets) {
            NSMutableArray *selectedAssets = [[NSMutableArray alloc] init];
            for (NSString *url in inputData.selectedAssetUrls) {
                for (ALAsset *item in assets) {
                    NSString *itemUrl = [item defaultRepresentation].url.absoluteString;
                    if ([url isEqualToString:itemUrl]) {
                        [selectedAssets addObject:item];
                        break;
                    }
                }
            }
            
            if (selectedAssets.count > 0) {
                [weakSelf loadSelectedAssets:selectedAssets];
            }
            
        } error:^(NSError *error) {
            ;
        }];
        
        if (inputData.mediaLink.length > 0) {
            self.mediaLink = inputData.mediaLink;
            [self startDownloadMedia:self.mediaLink];
        }
        
        [HGSellPageInputDataManager removeData];
    }
}

- (void)saveInputDataToCache {
    HGSellPageInputData *inputData = [[HGSellPageInputData alloc] init];
    inputData.title = self.tfTitle.text;
    inputData.listingPrice = self.tfListingPrice.text;

    if (![self.lbInputBrand.text isEqualToString:NSLocalizedString(@"Optional", nil)]) {
        inputData.brand = self.lbInputBrand.text;
    }

    if (![self.lbInputCategory.text isEqualToString:NSLocalizedString(@"Required", nil)]) {
        inputData.category = self.lbInputCategory.text;
    }
    
    inputData.delivery = self.lbInputDelivery.text;
    inputData.originalPrice = self.tfOriginalPrice.text;
    inputData.rootCategoryId = self.rootCategory;
    inputData.categoryId = self.selectedCatID;
    inputData.deliveryType = self.ItemDeliveryType;
    inputData.itemDescription = self.szTextDesc.text;
    inputData.city = self.city;
    inputData.country = self.country;
    inputData.region = self.region;

    NSMutableArray *assetUrls = [[NSMutableArray alloc] init];
    for (ALAsset *asset in self.selectedAssets) {
        NSString *assetUrl = [asset defaultRepresentation].url.absoluteString;
        [assetUrls addObject:assetUrl];
    }
    
    inputData.selectedAssetUrls = assetUrls;
    
    inputData.mediaLink = self.mediaLink;

    [HGSellPageInputDataManager saveToLocalWithInputData:inputData];
}

- (void)loadSelectedAssets:(NSArray *)assets
{
    self.selectedAssets = [assets mutableCopy];
    
    if (!self.uploadArray) {
        self.uploadArray = [[NSMutableArray alloc] initWithCapacity:assets.count];
    } else {
        [self.uploadArray removeAllObjects];
    }
    
    for (ALAsset * asset in assets) {
        if (self.photoTableController.availableSlotNum > 0) {
            NSString * imgUuid = [[NSUUID UUID] UUIDString];
            [self.photoTableController addImage:[UIImage imageWithCGImage:asset.thumbnail] withUUID:imgUuid withRomoteUrl:@""];
            
            [self.uploadArray addObject:@{@"asset" : asset, @"uuid" : imgUuid}];
            
            RACSubject * sub = [RACSubject subject];
            [self.uploadSignals addObject:sub];
            
        } else {
            break;
        }
    }
    
    [self.photosTableView reloadData];
    [self.photosTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.photoTableController.images.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    
    if (self.uploadArray.count > 0) {
        [self startUploadAssets];
    }
}


#pragma mark - UIImagePickerController Delegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"photo_take" label:nil value:nil];
    
    UIImage * photo = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (photo == nil) {
        //user may take a video, or some other reason, fail to get a pic
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Fail to get a picture, please try again.", nil)];
        [picker dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    
    UIImage * resizedPhoto = [photo scaleToSize:CGSizeMake(floorf(photo.size.width * 0.3), floorf(photo.size.height * 0.3))];
    photo = nil;
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(resizedPhoto, nil, nil, nil);
    }
    
    if (self.photoTableController.availableSlotNum > 0) {
        NSString * imgUuid = [[NSUUID UUID] UUIDString];
        
        
        UIImage* centerCropImage = [[HGUtils sharedInstance] centerCropImage:resizedPhoto];
        
        UIImage* thumbnail = [[HGUtils sharedInstance] thumbnailOfImage:centerCropImage withSize:CGSizeMake(84.0, 84.0)];
        centerCropImage = nil;
        
        [self.photoTableController addImage:thumbnail withUUID:imgUuid withRomoteUrl:@""];
        
        [self.photosTableView reloadData];
        [self.photosTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.photoTableController.images.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        
        RACSubject * sub = [RACSubject subject];
        [self.uploadSignals addObject:sub];
        
        [self _uploadPhoto:resizedPhoto withUUID:imgUuid complete:nil];
    }
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CTAssetsPickerController Delegate

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset
{
    return picker.selectedAssets.count < self.photoTableController.availableSlotNum;
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker isDefaultAssetsGroup:(ALAssetsGroup *)group
{
    // Set Camera Roll as default album and it will be shown initially.
    return ([[group valueForProperty:ALAssetsGroupPropertyType] integerValue] == ALAssetsGroupSavedPhotos);
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldShowAssetsGroup:(ALAssetsGroup *)group
{
    // Do not show empty albums
    return group.numberOfAssets > 0;
}




//if user pick from phone albums.
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    self.selectedAssets = [assets mutableCopy];

    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"photo_album" label:nil value:nil];
    
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];

    if (!self.uploadArray) {
        self.uploadArray = [[NSMutableArray alloc] initWithCapacity:assets.count];
    } else {
        [self.uploadArray removeAllObjects];
    }
    
    for (ALAsset * asset in assets) {
//        ALAssetRepresentation * assetRepresentation = asset.defaultRepresentation;
        
        if (self.photoTableController.availableSlotNum > 0) {
            NSString * imgUuid = [[NSUUID UUID] UUIDString];
            [self.photoTableController addImage:[UIImage imageWithCGImage:asset.thumbnail] withUUID:imgUuid withRomoteUrl:@""];

            [self.uploadArray addObject:@{@"asset" : asset, @"uuid" : imgUuid}];
            
            RACSubject * sub = [RACSubject subject];
            [self.uploadSignals addObject:sub];
            
//            UIImage *image;
//            
//            if (IPHONE4X) {
//                CGImageRef cgImageRef = assetRepresentation.fullScreenImage;
//                image = [UIImage imageWithCGImage:cgImageRef];
//            }
//            else{
//                float resizedPhoteRate = 0.8;
//                image = [UIImage imageWithCGImage:assetRepresentation.fullResolutionImage scale:1.0 orientation:(UIImageOrientation)assetRepresentation.orientation];
//                image = [image scaleToSize:CGSizeMake(floorf(image.size.width * resizedPhoteRate),floorf(image.size.height* resizedPhoteRate))];
//            }
//            
//            if(image.size.height/image.size.width > 2)
//            {
//                ////need resize the image.
//                image = [image scropeImageFromSize:image.size toSize:CGSizeMake(image.size.width, image.size.width*1.33333 )];
//            }
//            
//            [self _uploadPhoto:image withUUID:imgUuid complete:nil];
            
            
           
        } else {
            break;
        }
    }
    
    if (self.uploadArray.count > 0) {
        [self startUploadAssets];
    }
    
    [self.photosTableView reloadData];
    [self.photosTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.photoTableController.images.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)startUploadAssets {
    
    if (self.uploadArray.count > 0) {
        NSDictionary *dic = self.uploadArray[0];
        
        __weak __typeof(self)weakSelf =self;
        
        [self uploadOneAsset:[dic objectForKey:@"asset"] uuid:[dic objectForKey:@"uuid"] complete:^(BOOL isSuccess) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;

            if (strongSelf.uploadArray.count > 0) {
                [strongSelf.uploadArray removeObjectAtIndex:0];
            }

            NSLog(@"startUploadAssets %d", isSuccess);
            [strongSelf startUploadAssets];
        }];
    }
}

- (void)uploadOneAsset:(ALAsset *)asset uuid:(NSString *)imageUuid complete:(void (^)(BOOL isSuccess))completeBlock{
    ALAssetRepresentation * assetRepresentation = asset.defaultRepresentation;
    UIImage *image;
    
    if (IPHONE4X) {
        CGImageRef cgImageRef = assetRepresentation.fullScreenImage;
        image = [UIImage imageWithCGImage:cgImageRef];
    }
    else{
        float resizedPhoteRate = 0.8;
        image = [UIImage imageWithCGImage:assetRepresentation.fullResolutionImage scale:1.0 orientation:(UIImageOrientation)assetRepresentation.orientation];
        image = [image scaleToSize:CGSizeMake(floorf(image.size.width * resizedPhoteRate),floorf(image.size.height* resizedPhoteRate))];
    }
    if(image.size.height/image.size.width > 2)
    {
        ////need resize the image.
        image = [image scropeImageFromSize:image.size toSize:CGSizeMake(image.size.width, image.size.width*1.33333 )];
    }
    
    [self _uploadPhoto:image withUUID:imageUuid complete:completeBlock];
}

#pragma mark - Record & Play Voice
#define Record_lazy_close_time 1
-(void)recordButtonClick
{
    if (IOS_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        /////ios 8.

        if (![self canRecord])
        {
            return;
        }
        else{
            [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Oops, your recording needs to be over 5 seconds long.", nil)];
        }

    }
    else{
        ///ios 7.0
         [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Oops, your recording needs to be over 5 seconds long.", nil)];
    }
}
- (void)recordButtonLongPressed:(UILongPressGestureRecognizer *)gesture
{
    
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        if (![self canRecord]) {
            return;
        }
        //status defense code.
        if(self.recordController.audioRecorder.isRecording)
        {
            return;
        }
        
        NSLog(@"Start recording...");
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"record" label:nil value:nil];
        self.voiceRecordCell.backgroundColor = SYSTEM_MY_ORANGE;
        self.voiceCellRoundCornerView.backgroundColor = SYSTEM_MY_ORANGE;
        [self.view addSubview: self.voiceStatusHud];
        [self.voiceStatusHud setCurrentOpacity:0.75];
        
        [self.iconMicrophone setImage:[UIImage imageNamed:@"icon-starRecord-hl"]];
        self.audio_userHintLabel.textColor = [UIColor whiteColor];
        [self.recordController startRecording];
        
       
    }
    if (gesture.state == UIGestureRecognizerStateEnded) {
        NSLog(@"Record Ended");
        
        int recordDuration = self.recordController.voiceDuration;
        if (recordDuration == 0) {
            recordDuration = 1;
        }
        [self.voiceStatusHud removeFromSuperview];
        self.voiceRecordCell.backgroundColor = [UIColor whiteColor];
        self.voiceCellRoundCornerView.backgroundColor = [UIColor whiteColor];
        
        [self.iconMicrophone setImage:[UIImage imageNamed:@"icon-startRecord"]];
        self.audio_userHintLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
        
        
        if (recordDuration <= 5)
        {
            [self.recordController stopRecording];
            [self resetRecordUI];
            [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Oops, your recording needs to be over 5 seconds long.", nil)];
        }
        else{
            
            
            [NSTimer scheduledTimerWithTimeInterval:Record_lazy_close_time target:self selector:@selector(recorderLazyClose) userInfo:nil repeats:NO];
            
            //only upload the voice length longer than 5s.
            self.btnRecord.hidden = YES;
            self.iconMicrophone.hidden = YES;
            
            self.btnPlay.hidden = NO;
            self.audio_slideView.hidden = NO;
            self.audio_slide.maximumValue = (float)recordDuration;
            self.audio_label1.text = @"0'";
            self.audio_label2.text = [@"" stringByAppendingFormat:@"%d\"", recordDuration];
            
        }
    }
}

-(void)recorderLazyClose
{
    [self.recordController stopRecording];
    int recordDuration = self.recordController.voiceDuration;
    [self _uploadSoundFile:self.recordController.soundFileURL];
    self.audio_label2.text = [@"" stringByAppendingFormat:@"%d\"", recordDuration];
}
-(void)resetRecordUI
{
    self.btnPlay.hidden = YES;
    self.audio_slideView.hidden = YES;
    
    
    self.btnRecord.hidden = NO;
    self.iconMicrophone.hidden = NO;
    
    self.audio_label2.text = @"";
    self.audio_label1.text = @"";
}

- (IBAction)onTouchPlayButton:(id)sender {
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"record_listen" label:nil value:nil];
    
    
    self.audioStatusUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateAudioSlide) userInfo:nil repeats:YES];
    NSLog(@"Start playing voice description...");
    if (nil == self.recordController.audioPlayer ) {
        UIImage* image = [UIImage imageNamed:@"audio_pause"];
        [self.btnPlay setBackgroundImage:image forState:UIControlStateNormal];
        [self.recordController playRecording];
        
        self.recordController.audioPlayer.delegate = self;
    } else if (!self.recordController.audioPlayer.isPlaying) {
        UIImage* image = [UIImage imageNamed:@"audio_pause"];
        [self.btnPlay setBackgroundImage:image forState:UIControlStateNormal];
        [self.recordController resumePlaying];
    } else {
        UIImage* image = [UIImage imageNamed:@"audio_play"];
        [self.btnPlay setBackgroundImage:image forState:UIControlStateNormal];
        [self.recordController pausePlaying];
    }
}


- (void)onTouchDeleteButton:(id)sender {
    NSLog(@"Stop playing voice description...");
    UIActionSheet * actionSheet;
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                destructiveButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Confirm deletion",nil),  nil];
    
    actionSheet.tag = AUDIO_DELETE_ATCION_TAG;
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
    
}
-(void)ConfirmDeleteAudio
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"record_delete" label:nil value:nil];
    [self resetRecordUI];
    
    [self.soundUploader cancel];
    [self.uploadSignals removeObject:self.soundSub];
    self.mediaLink = @"";

}


#pragma mark - UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _focusControl = textField;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    _focusControl = textView;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == PRICE_TEXTFIELD_TAG || textField.tag == ORIGINAL_PRICE_TEXTFIELD_TAG) {
        NSString *input = textField.text;
        NSNumber *price = [[NSNumber alloc] initWithDouble:[input floatValue]];
        if([price compare:@(0)] == NSOrderedSame){
            textField.text = @"0";
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == PRICE_TEXTFIELD_TAG || textField.tag == ORIGINAL_PRICE_TEXTFIELD_TAG) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^([0-9]{1,7})?(\\.([0-9]{1,2})?)?$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0) return NO;
    }
    
    if (textField.tag == ITEM_TITLE_DESC) {
        if(textField.text.length > 500 && ![string isEqualToString:@""])
        {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - Audio Player Delegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if (!flag) {
        return;
    }
    
    self.recordController.audioPlayer = nil;
    
    
    [self.audioStatusUpdateTimer invalidate];
    self.audioStatusUpdateTimer = nil;
    self.audio_label1.text = @"0'";
    [self.audio_slide setValue:0];
    UIImage* image = [UIImage imageNamed:@"audio_play"];
    [self.btnPlay setBackgroundImage:image forState:UIControlStateNormal];
}

#pragma mark - Helpers

- (void)_uploadPhoto:(UIImage *)photo withUUID:(NSString *)uuid complete:(void (^)(BOOL isSuccess))completeBlock
{
    NSData * photoData = UIImageJPEGRepresentation(photo, 0.5);
    
//    RACSubject * sub = [RACSubject subject];
//    [self.uploadSignals addObject:sub];
    RACSubject * sub = [self.uploadSignals objectAtIndex:0];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"sign_image_upload/" parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary * signedRequest = responseObject;
        
        CLUploader * uploader = [[CLUploader alloc] init:[HGUtils sharedInstance].cloudinary delegate:nil];
        [self.imgDict setObject:@{@"uploader": uploader, @"signal": sub} forKey:uuid];
        [uploader upload:photoData options:signedRequest withCompletion:^(NSDictionary *successResult, NSString *errorResult, NSInteger code, id context) {
            NSLog(@"image upload finished for uuid: %@", uuid);
            
            [self.uploadSignals removeObject:sub];
            
            if (nil == errorResult) {
                [self.imageLinks addObject:@{@"imageLink": [successResult objectForKey:@"url"], @"width":[successResult objectForKey:@"width"], @"height":[successResult objectForKey:@"height" ], @"uuid":uuid}];
                [sub sendCompleted];
            } else {
                [sub sendError:[NSError errorWithDomain:@"Cloudinary" code:code userInfo:@{}]];
            }
            
            if (completeBlock) {
                completeBlock(nil == errorResult);
            }
            
        } andProgress:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite, id context) {
            ;
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"sign image upload failed: %@", error);
        [sub sendError:[NSError errorWithDomain:@"Cloudinary" code:1001 userInfo:@{@"reason":@"sign request failed"}]];
        if (completeBlock) {
            completeBlock(NO);
        }
    }];
}

- (void)_uploadSoundFile:(NSURL *)fileURL
{
    NSData * soundData = [NSData dataWithContentsOfURL:fileURL];
    
    self.soundSub = [RACSubject subject];
    [self.uploadSignals addObject:self.soundSub];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"sign_image_upload/" parameters:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary * signedRequest = responseObject;
        NSMutableDictionary * options = [NSMutableDictionary dictionaryWithDictionary:signedRequest];
        [options setValue:@"raw" forKey:@"resource_type"];
        
        self.soundUploader = [[CLUploader alloc] init:[HGUtils sharedInstance].cloudinary delegate:nil];
        [self.soundUploader upload:soundData options:options withCompletion:^(NSDictionary *successResult, NSString *errorResult, NSInteger code, id context) {
            
            [self.uploadSignals removeObject:self.soundSub];
            
            if (nil == errorResult) {
                NSLog(@"sound upload finished successfully");
                self.mediaLink = [successResult objectForKey:@"url"];
                [self.soundSub sendCompleted];
            } else {
                NSLog(@"sound upload failed: %@", errorResult);
                [self.soundSub sendError:[NSError errorWithDomain:@"Cloudinary" code:code userInfo:@{}]];
            }
        } andProgress:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite, id context) {
            ;
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"sound upload get signature failed: %@", error);
        [self.soundSub sendError:[NSError errorWithDomain:@"Cloudinary" code:1001 userInfo:@{@"reason":@"sign request failed"}]];
    }];
}

- (NSString *)buildLocationTitleWithCountry:(NSString *)country region:(NSString *)region city:(NSString *)city
{
    NSMutableString *locationTitle = [[NSMutableString alloc] init];
    if (city) {
        locationTitle = [city mutableCopy];
    }
    
    if (region && ![city isEqual:region]) {
        [locationTitle appendFormat:@" %@", region];
    }

    return locationTitle;
}

#pragma mark - Unwind segue handlers
- (IBAction)doneListingLocationZip:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"doneListingLocationZip"]) {
         ListingLocationVC * vcSource = segue.sourceViewController;

        self.country = vcSource.sellItem_CountryCode;
        self.city = vcSource.sellItem_CityCode;
        self.region = vcSource.sellItem_RegionCode;

        self.locationTitle.text = [self buildLocationTitleWithCountry:vcSource.sellItem_CountryCode region:vcSource.sellItem_RegionCode city:vcSource.sellItem_CityCode];
    }
}
- (IBAction)donePickCategory:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"DonePickCategory"]) {
        HGCategoryPickController * vcSource = segue.sourceViewController;
        NSLog(@"picked category: %d", (int)vcSource.selectedCatID);
        
        
        
        ////selected category level2 title;
        //todo    vcSource.currentSelectedC2TitleString;
        self.selectedCatID = vcSource.selectedCatID;
        
        self.lbInputCategory.textColor = [UIColor darkTextColor];
        self.lbInputCategory.text = vcSource.currentSelectedC2TitleString;
        
        self.rootCategory = vcSource.selectedC1_ID;
        
        BOOL isListing = [self isListingCategoryId:self.rootCategory];
        [self updateLayoutWhenChangedCategoryValue:isListing];
        
        if ([self isListingCategoryId:self.rootCategory]) {
            self.szTextDesc.placeholder = NSLocalizedString(@"Add a quick description to attract more buyers (Optional):\n1. Size, such as Size Large.\n2. How old, such as Bought in 2013.\n3. Condition, such as Never Used.\n4. Any Issues, such as Fully Functional.", nil);
        } else if ([self isHousingCategoryId:self.rootCategory]) {
            self.szTextDesc.placeholder = NSLocalizedString(@"Add a quick description to attract more interest (Optional):\n1.Housing type, such as Apartment.\n2. How many bedrooms and bathrooms.\n3. Size of the house.", nil);
        } else if ([self isJobsCategoryId:self.rootCategory]) {
            self.szTextDesc.placeholder = NSLocalizedString(@"Add a quick description to attract more interest (Optional):\n1. Job description.\n2. Compensation, please be as detailed as possible.\n3. Job type, such as Part-Time.\n4. Only one job description per posting please.", nil);
        } else {
            self.szTextDesc.placeholder = NSLocalizedString(@"Add a quick description to attract more interest (Optional):\n1. Work location, such as Dallas, TX.\n2. Work history, such as In business 8 years.\n3. Availibility, such as 9:00am–8:00pm.\n4. Whether you are licensed or not.", nil);
        }
        
       
    }
}

#pragma mark - UI Action Handlers

- (IBAction)onTouchCancel:(id)sender {
    
    if (self.tfListingPrice.text.length == 0 && self.tfTitle.text.length == 0 && self.szTextDesc.text.length == 0&& self.mediaLink.length == 0 && self.imageLinks.count == 0) {
        //not any user item information, go back directly.
        [self performSellCancel];
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"sell_cancel" label:nil value:nil];
        [self addCancelSellingActionSheet];
    }
    
    
}
-(void)performSellCancel
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"sell_cancel_tohome" label:nil value:nil];
    
    [self.imgDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        CLUploader * uploader = [obj objectForKey:@"uploader"];
        [uploader cancel];
        
        RACSubject * sub = [obj objectForKey:@"signal"];
        [sub sendCompleted];
    }];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(int)checkPostContent
{
    
    if (self.uploadSignals.count == 0 && self.imageLinks.count == 0) {
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Please select an image at least.", nil)];
        
        [self.photoTableController.images removeAllObjects];
        [self.photosTableView reloadData];
        [self.tfTitle becomeFirstResponder];
        return 0;
    }
    
    NSString * title = [self.tfTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (title.length == 0) {
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Please input the title.", nil)];
        [self.tfTitle becomeFirstResponder];
        return 0;
    }
    
    if (0 == [self.lbInputCategory.text length] || [self.lbInputCategory.text isEqualToString:@"Required"]) {
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Please input the Category.", nil)];
        return 0;
    }

    if (self.tfListingPrice.text.length == 0) {
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Please input the price.", nil)];
    }
    
    return 1;
}
- (IBAction)onTouchPublish:(id)sender {
  
    if ([self checkPostContent] == 0) {
        return;
    }
    float price = [self.tfListingPrice.text floatValue];
     NSString * title = [self.tfTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
   
    [self.view endEditing:YES];
    
    RACSignal * merged = [RACSignal merge:self.uploadSignals];
    [merged subscribeCompleted:^{
        NSError * error;
        
        
        if (self.CoverImageObjIndex != 0 && self.CoverImageObjIndex < self.imageLinks.count) {
            
            int ImageIndexUingUUID_confirm = 0;
            for (int i = 0; i < self.imageLinks.count; i++) {
                NSMutableDictionary* coverObj = [self.imageLinks objectAtIndex:i];
                if ([[coverObj objectForKey:@"uuid"] isEqualToString:self.CoverImageUUID]) {
                    ImageIndexUingUUID_confirm = i;
                }
            }
            NSMutableDictionary* coverObj = [self.imageLinks objectAtIndex:ImageIndexUingUUID_confirm];
            [self.imageLinks removeObjectAtIndex:ImageIndexUingUUID_confirm];
            [self.imageLinks insertObject:coverObj atIndex:0];
        }
        
        NSString * jsonImages = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:self.imageLinks options:NSJSONWritingPrettyPrinted error:&error] encoding:NSUTF8StringEncoding];
        
        
        ///////////todo.
        {
            self.postTitle = title;
            self.postPrice = price;
            self.postJsonImages= jsonImages;
        }
        //////////
        
        
        if (self.EditModel== 0) {
//            if ([HGUserData sharedInstance].accountType == HGAccountTypeEmail && ![HGUserData sharedInstance].bUserFBVerified) {
//                if ([HGUserData sharedInstance].bSellingFBShare) {
//                    [self addShareToFriendActionSheet];
//                }
//                else{
//                    [self postSellItem:title withPrice:price withImages:jsonImages];
//                }
//            }
//            else{
//                [self postSellItem:title withPrice:price withImages:jsonImages];
//            }
            if ([HGUserData sharedInstance].bSellingFBShare && [HGUserData sharedInstance].bPromptFBShare) {
                [self addShareToFriendActionSheet];
                [HGUserData sharedInstance].bPromptFBShare = NO;
            }
            else{
                [self postSellItem:title withPrice:price withImages:jsonImages];
            }

        }
        else if (self.EditModel == 1)
        {
            [self editSellItem:title withPrice:price withImages:jsonImages];
        }
    }];
    
    [merged subscribeError:^(NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"upload listing failed with error: %@", error);
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"upload listing failed with error. try later.", nil)];
    }];
}

-(void)postSellItem:(NSString*)title withPrice:(float)price withImages:(NSString*)jsonImages
{
   
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary: @{
                        @"title": title,
                        @"desc": self.szTextDesc.text,
                        @"price": self.tfListingPrice.text,
                        @"root_category" : @(self.rootCategory),
                        @"currency": [HGUserData sharedInstance].preferredCurrency,
                        @"lat":[NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLat]],
                        @"lon":[NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLon]],
                        @"images": jsonImages,
                        @"mediaLink": self.mediaLink,
                        @"share":@(NO),
                        @"country":[HGAppData sharedInstance].countryString,
                        @"region":[HGAppData sharedInstance].regionString,
                        @"city":[HGAppData sharedInstance].cityString
    }];
    
    if (self.country) {
        [params setObject:self.country forKey:@"country"];
    }
    
    if (self.city) {
        [params setObject:self.city forKey:@"city"];
    }
    
    if (self.region) {
        [params setObject:self.region forKey:@"region"];
    }
    
    if(self.selectedCatID != -1)
    {
        [params setValue:[NSNumber numberWithInteger:self.selectedCatID] forKey:@"category"];
    }
    
    if (self.tfOriginalPrice.text.length > 0) {
        [params setValue:self.tfOriginalPrice.text forKey:@"original_price"];
    }
    
    if ([self.lbInputBrand.text rangeOfString:NSLocalizedString(@"Optional", nil)].location==NSNotFound)
    {
        [params setValue:self.lbInputBrand.text forKey:@"brand"];
    }
    
    if (self.ItemDeliveryType != 0 ) {
        [params setValue:[NSNumber numberWithInteger:self.ItemDeliveryType] forKey:@"shipping_method"];
    
    }
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"post_item/" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"publish" label:nil value:nil];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"publish" withValue:@""];
        
        
        self.itemIDstring = [responseObject objectForKey:@"new_id"];
        
        NSDictionary* params = [NSDictionary dictionaryWithObject:self.itemIDstring forKey:@"itemID"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification.Item.Postnew" object:self userInfo:params];
        
        if ([self isServiceCategoryId:self.rootCategory] || [self isHousingCategoryId:self.rootCategory]) {
            
            if(![HGUserData sharedInstance].bUserPhoneVerified){
                [HGUserData sharedInstance].showServiceVerifyRemind = YES;
            }
        } else {
            if([HGUserData sharedInstance].bSellingFirstTime){
                [HGUserData sharedInstance].bSellingFirstTime = NO;
                [HGUserData sharedInstance].bShowVerifyRemind = YES;
            }
        }
        
        if([HGUserData sharedInstance].bSellingFBShare){
            [[HGFBUtils sharedInstance] shareItem:self.itemIDstring title:title price:price withSuccess:^(NSDictionary *reponse) {
                [HGAppData sharedInstance].bSellingFBShareSuccess = YES;
            } withFailure:^(NSError *error) {
                [HGAppData sharedInstance].bSellingFBShareSuccess = NO;
                NSLog(@"Error: %@", error);
            }];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];

        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"Error: %@", error);
    }];
    
    
}


-(void)editSellItem:(NSString*)title  withPrice:(float)price withImages:(NSString*)jsonImages
{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"Edit" label:nil value:nil];
    NSMutableDictionary * params = nil;
    NSString* sellUrl = @"";
    
    //edit item post
    sellUrl = @"edit_item/";
    params = [[NSMutableDictionary alloc] initWithDictionary: @{@"item_id": self.itemIDstring,
                                                                @"title": title,
                                                                @"desc": self.szTextDesc.text,
                                                                @"root_category" : @(self.rootCategory),
                                                                @"category":[NSNumber numberWithInteger:self.selectedCatID],
                                                                @"price": self.tfListingPrice.text,
                                                                @"currency": [HGUserData sharedInstance].preferredCurrency,
                                                                @"images": jsonImages,
                                                                @"mediaLink": self.mediaLink}];
    
    if (self.country) {
        [params setObject:self.country forKey:@"country"];
    }
    
    if (self.city) {
        [params setObject:self.city forKey:@"city"];
    }
    
    if (self.region) {
        [params setObject:self.region forKey:@"region"];
    }
    
    if (self.tfOriginalPrice.text.length > 0) {
        [params setValue:self.tfOriginalPrice.text forKey:@"original_price"];
    }
    
    
    if ([self.lbInputBrand.text rangeOfString:NSLocalizedString(@"Optional", nil)].location == NSNotFound)
    {
        [params setValue:self.lbInputBrand.text forKey:@"brand"];
    }
    
    if (self.ItemDeliveryType != 0) {
            [params setValue:[NSNumber numberWithInteger:self.ItemDeliveryType] forKey:@"shipping_method"];
    }
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:sellUrl parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification.Item.Edit" object:self userInfo:nil];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"edit item failed: %@", error);
    }];
    
}

         
- (BOOL)canRecord
{
    __block BOOL bCanRecord = YES;
    {
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        AVAudioSessionRecordPermission recordPermission = audioSession.recordPermission;
        
        if(recordPermission == AVAudioSessionRecordPermissionDenied){
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:[NSString stringWithFormat:NSLocalizedString(@"Need to visit your microphone, please go to private/setting/mic to open first",nil)]
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK",nil)
                              otherButtonTitles:nil] show];
            return NO;
        }else if(recordPermission == AVAudioSessionRecordPermissionGranted){
            return YES;
        }else if(recordPermission == AVAudioSessionRecordPermissionUndetermined){
            
            if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
                [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
                    if (granted) {
                        bCanRecord = YES;
                        [[HGUtils sharedInstance] gaSendEventWithCategory:@"privacylimit" action:@"allowmicrophone" label:nil value:nil];
                    } else {
                        bCanRecord = NO;
                        [[HGUtils sharedInstance] gaSendEventWithCategory:@"privacylimit" action:@"notallowmicrophone" label:nil value:nil];
                    }
                }];
            }
        }
    }
    
    return bCanRecord;
}

-(void)startListPositionSelect
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sell_view" action:@"changelocation" label:nil value:nil];
    ListingLocationVC * viewController = (ListingLocationVC*)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"ListingLocationVC"];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.cellLayoutModel.currentSectionTypeArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *rowTypeArray = [self.cellLayoutModel rowTypeArrayForSection:section];
    return rowTypeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *rowTypeArray = [self.cellLayoutModel rowTypeArrayForSection:indexPath.section];
    HGPublicPageLayoutModelRowType rowType = [rowTypeArray[indexPath.row] integerValue];
    
    if (HGPublicPageLayoutModelRowTypeTitle == rowType) {
        return self.titleCell;
    } else if (HGPublicPageLayoutModelRowTypeCategory == rowType) {
        return self.category_cell;
    } else if (HGPublicPageLayoutModelRowTypePrice == rowType) {
        return self.listingPrice_cell;
    } else if (HGPublicPageLayoutModelRowTypeLocation == rowType) {
        return self.locationCell;
    } else if (HGPublicPageLayoutModelRowTypeDescription == rowType) {
        return self.descriptionCell;
    } else if (HGPublicPageLayoutModelRowTypeVoice == rowType) {
        return self.voiceRecordCell;
    } else if (HGPublicPageLayoutModelRowTypeOriganlPrice == rowType) {
        return self.originPriceCell;
    } else if (HGPublicPageLayoutModelRowTypeBrand == rowType) {
        return self.brandCell;
    } else if (HGPublicPageLayoutModelRowTypeDelivery == rowType) {
        return self.delivery_cell;
    } else if (HGPublicPageLayoutModelRowTypeShare == rowType) {
        return self.ShareOnFaceBookCell;
    }
    
    return [[UITableViewCell alloc] init];
}

#pragma mark- UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *rowTypeArray = [self.cellLayoutModel rowTypeArrayForSection:indexPath.section];
    HGPublicPageLayoutModelRowType rowType = [rowTypeArray[indexPath.row] integerValue];
    
    if (HGPublicPageLayoutModelRowTypeTitle == rowType) {
        return 50;
    } else if (HGPublicPageLayoutModelRowTypeDescription == rowType) {
        return 152;
    } else if (HGPublicPageLayoutModelRowTypeVoice == rowType) {
        return 50;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    HGPublicPageLayoutModelSectionType sectionType = [self.cellLayoutModel.currentSectionTypeArray[section] integerValue];
    
    if (HGPublicPageLayoutModelSectionTypeMoreInformation == sectionType) {
        return 30;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HGPublicPageLayoutModelSectionType sectionType = [self.cellLayoutModel.currentSectionTypeArray[section] integerValue];
    
    if(HGPublicPageLayoutModelSectionTypeMoreInformation == sectionType)
    {
        UIView* tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
        tableHeaderView.backgroundColor = [UIColor clearColor];
        UILabel * sectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width-20, 20)];
        [tableHeaderView addSubview:sectionTitle];
        sectionTitle.center = tableHeaderView.center;
        sectionTitle.textAlignment = NSTextAlignmentLeft;
        NSString* title = NSLocalizedString(@"More Information", nil);
        sectionTitle.font = [UIFont fontWithName: FONT_TYPE_2 size:15];
        sectionTitle.text = title;
        sectionTitle.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
//        NSRange  range = [title rangeOfString:@"&"];
//        if (range.length > 0 ) {
//            UIFont *font = [UIFont systemFontOfSize:15];
//            UIColor* fontcolor = FANCY_COLOR(@"242424");
//            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:title];
//            [attrString addAttribute:NSFontAttributeName value:font range:range];
//            [attrString addAttribute:NSForegroundColorAttributeName value:fontcolor range:NSMakeRange(0, title.length)];
//            [sectionTitle setAttributedText:attrString];
//        }
        
        
        return tableHeaderView;
        
    }
    return  nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *rowTypeArray = [self.cellLayoutModel rowTypeArrayForSection:indexPath.section];
    HGPublicPageLayoutModelRowType rowType = [rowTypeArray[indexPath.row] integerValue];
    
    if (HGPublicPageLayoutModelRowTypeCategory == rowType) {
        
        HGCategoryPickController * viewController = (HGCategoryPickController*)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"CategoryPicker"];
        viewController.selectedC1_ID = (int)self.rootCategory;
        [self.navigationController pushViewController:viewController animated:YES];
        
    } else if (HGPublicPageLayoutModelRowTypeLocation == rowType) {
        [self startListPositionSelect];
    } else if (HGPublicPageLayoutModelRowTypeBrand == rowType) {
        HGItemBrandViewController *viewcontroller = [[HGItemBrandViewController alloc] initWithNibName:@"HGItemBrandViewController" bundle:nil];
        [self.navigationController pushViewController:viewcontroller animated:YES];
    } else if (HGPublicPageLayoutModelRowTypeDelivery == rowType) {
        HGDeliveryTableViewController *viewcontroller = [[HGDeliveryTableViewController alloc] initWithNibName:@"HGDeliveryTableViewController" bundle:nil];
        
        [self.navigationController pushViewController:viewcontroller animated:YES];
    }
}

#pragma mark- 

- (void)updateLayoutWhenChangedCategoryValue:(BOOL)isListing
{
    if (isListing && (HGPublicPageLayoutModelLayoutTypeListing != self.cellLayoutModel.currentLayoutType)) {
        self.cellLayoutModel.currentLayoutType = HGPublicPageLayoutModelLayoutTypeListing;
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:HGPublicPageLayoutModelSectionTypeMoreInformation];
        [self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    } else if (!isListing && (HGPublicPageLayoutModelLayoutTypeListing == self.cellLayoutModel.currentLayoutType)) {
        self.cellLayoutModel.currentLayoutType = HGPublicPageLayoutModelLayoutTypeService;
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:HGPublicPageLayoutModelSectionTypeMoreInformation];
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (BOOL)isListingCategoryId:(NSInteger)categoryId
{
    return (categoryId == 1000);
}

- (BOOL)isServiceCategoryId:(NSInteger)categoryId
{
    return (categoryId == 1001);
}

- (BOOL)isHousingCategoryId:(NSInteger)categoryId
{
    return (categoryId == 1002);
}

- (BOOL)isJobsCategoryId:(NSInteger)categoryId
{
    return (categoryId == 1003);
}

@end

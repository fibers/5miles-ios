//
//  HGWelcomePageController.h
//  Pinnacle
//
//  Created by Alex on 15-3-31.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <X4ImageViewer/X4ImageViewer.h>

@interface HGWelcomePageController : UIViewController<X4ImageViewerDelegate, X4ImageViewerDataSource>


@end

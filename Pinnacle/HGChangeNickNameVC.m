//
//  HGChangeNickNameVC.m
//  Pinnacle
//
//  Created by Alex on 15/5/12.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChangeNickNameVC.h"
#import "HGConstant.h"
#import "HGUserData.h"
#import "HGAppData.h"
#import "HGUITextField.h"
#import <SVProgressHUD.h>
#import "PXAlertView+Customization.h"
@interface HGChangeNickNameVC ()
@property (nonatomic, strong)HGUITextField* nameField;
@property (nonatomic, strong)UILabel* hintLabel;
@property (nonatomic, strong)UIButton* btnSend;

@end

@implementation HGChangeNickNameVC

static BOOL bUserTouchBack = YES;
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    ////TextField has a strange bug, from 8.3..view re show will clear the content.
    
    self.nameField = [[HGUITextField alloc] initWithFrame:CGRectMake(0, 70, self.view.frame.size.width,40)];
    [self.view addSubview:self.nameField];
    self.nameField.font = [UIFont systemFontOfSize:15];
    self.nameField.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.nameField.delegate = self;
    self.nameField.text = [HGUserData sharedInstance].userDisplayName;
    self.nameField.backgroundColor = [UIColor whiteColor];
    self.nameField.edgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    
    
    if (self.nameField.text.length > 0 ) {
        [self enableSendButton];
    }
    else{
        [self disableSendButton];
    }

}
- (void)viewDidLoad {
    [super viewDidLoad];
    bUserTouchBack = YES;
    [[HGUtils sharedInstance] gaTrackViewName:@"changename_view"];
    self.navigationItem.title = NSLocalizedString(@"Name",nil);
    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    
    
    
    self.hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 70+40+5, 200, 30)];
    [self.view addSubview:self.hintLabel];
    self.hintLabel.textColor = FANCY_COLOR(@"636363");
    self.hintLabel.font = [UIFont systemFontOfSize:13];
    self.hintLabel.text = NSLocalizedString(@"Name(1-15 characters)", nil);
    
    
    [self addRightButton];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.nameField becomeFirstResponder];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(newString.length == 0)
    {
        [self disableSendButton];
    }
    else{
        [self enableSendButton];
    }
    
    return newString.length > 15 ? NO:YES;
    
}
- (void)enableSendButton{
    self.btnSend.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)disableSendButton{
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

-(void)onTouchSend
{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"changename_view" action:@"changename_save" label:nil value:nil];
    NSString* nameString = [self.nameField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (nameString.length == 0) {
        PXAlertView * view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Please input a valid name.", nil)];
        [view useDefaultIOS7Style];
        self.nameField.text = [HGUserData sharedInstance].userDisplayName;
        return;
    }
    
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"fill_profile/" parameters:@{@"nickname":self.nameField.text} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [SVProgressHUD dismiss];
        [HGUserData sharedInstance].userDisplayName = [responseObject objectForKey:@"nickname"];
        
        bUserTouchBack = NO;
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        ///temply keep
        PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Oops!", nil) message:error.localizedDescription];
        [view useDefaultIOS7Style];
    }];
    
}
-(void)addRightButton
{
    UIView* rightBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    
    self.btnSend = [[UIButton alloc] initWithFrame:CGRectMake(13, 0, 65, 30)];
    self.btnSend.clipsToBounds = YES;
    self.btnSend.layer.cornerRadius = 3;
    self.btnSend.layer.borderWidth = 1;
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnSend.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.btnSend setTitle:NSLocalizedString(@"Save",nil) forState:UIControlStateNormal];
    [self.btnSend setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [self.btnSend setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.btnSend addTarget:self action:@selector(onTouchSend) forControlEvents:UIControlEventTouchUpInside];
    
    [rightBackView addSubview:self.btnSend];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBackView];
    
    [self enableSendButton];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    if (bUserTouchBack) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"changename_view" action:@"changename_back" label:nil value:nil];
    }
}

@end

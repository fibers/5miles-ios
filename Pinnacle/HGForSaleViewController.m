//
//  HGForSaleViewController.m
//  Pinnacle
//  copy from HGHomePageViewController
//  Created by mumuhou on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "HGForSaleViewController.h"
#import "HGHomeViewController.h"

#import "HGHomeChildViewControllerProtocol.h"
#import "HGTrackingData.h"
#import "HGFivemilesClient.h"
#import "HGHomeItemCell.h"
#import "HGItemDetailController.h"
#import "HGShopItem.h"
#import "HGUtils.h"
#import "HGMessage.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "SVPullToRefresh.h"
#import <SVProgressHUD.h>
#import "UIColor+FlatColors.h"
#import "HGCampaignsObject.h"
#import "HGWebContentController.h"
#import "JSONKit.h"
#import "HGSearchViewController.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGGoodSellerViewController.h"
#import "InstructionView.h"
#import "HGStartSystemStrAction.h"
#import "CMPopTipView.h"
#import "SectionFooterCollectionReusableView.h"
#import "HGUserData.h"
#import <ReactiveCocoa.h>
#import "HGZipCodeVC.h"
#import "HGSegmentViewController.h"


@interface HGForSaleViewController ()<CLLocationManagerDelegate, UIActionSheetDelegate, UICollectionViewDataSource, UICollectionViewDelegate, CHTCollectionViewDelegateWaterfallLayout, HGHomeChildViewControllerProtocol, HGSegmentSubViewControllerProtocol>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) CLLocationManager * locationManager;

@property (nonatomic, strong) UIView* noMoreHintView;

@property(nonatomic, strong) NSString* latestMD5string;

@property (nonatomic, strong) NSMutableArray * home_shopItems;

@property (nonatomic, strong) NSString * nextLink;
@property (nonatomic, strong) NSString* followingsNextLink;

@end

static NSInteger kScrollViewTopMargin = 64 - 20;

static NSString * const SectionTitleID = @"SectionTitle";
static NSString * const SimilarSectionFooter =@"noMoreFootSection";

@implementation HGForSaleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self _setup];
    }
    return self;
}

//- (void)awakeFromNib
//{
//    [super awakeFromNib];
//    [self _setup];
//}
//
//-(UIView*)createRemindView
//{
//    InstructionView* v = [[InstructionView alloc] initWithFrame:CGRectMake(0, 500, 50, 50)];
//    v.TriangleType = 1;
//    [v startInitView];
//    HGAppDelegate* d =  (HGAppDelegate*)[UIApplication sharedApplication].delegate;
//    [d.window addSubview:v];
//    return v;
//}

- (void)_setup
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.home_shopItems = [NSMutableArray array];
    self.title = NSLocalizedString(@"Home", nil);
    self.nextLink = @"";
    self.followingsNextLink = @"";
    self.latestMD5string = @"";
}

- (void)dealloc
{
    [self.locationManager stopUpdatingLocation];
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Notification.Item.Postnew" object:nil];
    }
    @catch (NSException *exception) {
        [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
        
        NSLog(@"remove observer exception: %@", exception);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
//    self.edgesForExtendedLayout = UIRectEdgeLeft|UIRectEdgeRight|UIRectEdgeBottom;
    
    [self addHomeCollectionView];
    
    [self _startLocationService];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostItem:) name:@"Notification.Item.Postnew" object:nil];
    [self _startLoadingWithCompletion:^{
        [SVProgressHUD dismiss];
    }];
    [self reverseGeoCode];
}

-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    
    [self.noMoreHintView setHidden:YES];
}

-(void)addHomeCollectionView
{
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 0, 4);
    layout.minimumColumnSpacing = 5;
    layout.minimumInteritemSpacing = 7.5;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.collectionView.contentInset = UIEdgeInsetsMake(kScrollViewTopMargin, 0, 0, 0);
    self.collectionView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    [self.collectionView registerClass:[HGHomeItemCell class] forCellWithReuseIdentifier:@"HomeItemCell"];
    [self.view addSubview:self.collectionView];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

    //注册headerView Nib的view需要继承UICollectionReusableView
    [self.collectionView registerNib:[UINib nibWithNibName:@"SQSupplementaryView" bundle:nil] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:SectionTitleID];
    
    // Hook pull down to refresh
    __weak __typeof(self) weakSelf = self;
    [self.collectionView addPullToRefreshWithActionHandler:^{
        NSLog(@"refreshing...");
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"top_refresh" label:nil value:nil];
        //move here to avoid home water flow view image shaking ,
        //we can add some hint to collection head to show update status.
        [weakSelf.collectionView.pullToRefreshView stopAnimating];
        
        if (self.view.window && [self isViewLoaded]) {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
        }
        
        [weakSelf _startLoadingWithCompletion:^{
            [SVProgressHUD dismiss];
            NSLog(@"done.");
        }];
    }];
    
    // Hook pull up to load more
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"bottom_load" label:nil value:nil];
        
        [weakSelf _loadNextPage];
    } withAddtionnalOffset:1000];
    self.collectionView.scrollsToTop = YES;
    
}

-(void)reverseGeoCode
{
    RACSignal *signalUserID = [[[[RACObserve([HGUserData sharedInstance], fmUserID) ignore:nil] ignore:@""] distinctUntilChanged] take:1];
    RACSignal *signalLocation = [[[RACObserve([HGAppData sharedInstance], currentLocation) ignore:nil]  distinctUntilChanged] take:1];
    
    [[RACSignal combineLatest:@[signalUserID, signalLocation]]  subscribeNext:^(id x){
        
        CLLocation* location = [[CLLocation alloc] initWithLatitude:[HGAppData sharedInstance].currentLocation.coordinate.latitude longitude:[HGAppData sharedInstance].currentLocation.coordinate.longitude];
        
        CLGeocoder* geoCoder = [[CLGeocoder alloc]init];
        [geoCoder reverseGeocodeLocation: location completionHandler:
         //Getting Human readable Address from Lat long,,,
         ^(NSArray *placemarks, NSError *error) {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             NSLog(@"I am currently at %@",locatedAt);
             [HGAppData sharedInstance].countryString = placemark.addressDictionary[@"Country"]!=nil?placemark.addressDictionary[@"Country"]:@"";
             [HGAppData sharedInstance].cityString = placemark.addressDictionary[@"City"]!=nil?placemark.addressDictionary[@"City"]:@"";
             [HGAppData sharedInstance].regionString = placemark.addressDictionary[@"State"]!=nil?placemark.addressDictionary[@"State"]:@"";
             
             NSNumber* latNumber = [[NSNumber alloc] initWithDouble:location.coordinate.latitude];
             NSNumber* lonNumber = [[NSNumber alloc] initWithDouble:location.coordinate.longitude];
             [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"update_location/" parameters:@{@"lat":latNumber,@"lon":lonNumber,
                                                                                                       @"country":[HGAppData sharedInstance].countryString,
                                                                                                       @"region":[HGAppData sharedInstance].regionString,
                                                                                                       @"city":[HGAppData sharedInstance].cityString } success:^(NSURLSessionDataTask *task, id responseObject) {
                                                                                                           NSLog(@"update location ok");
                                                                                                           
                                                                                                       } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                                                                           
                                                                                                           NSLog(@"update location fail.");
                                                                                                       }];
         }];
        
    }];
}

-(void)defaultEmptyAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"explore_seller" label:nil value:nil];
    UIStoryboard * storyBoard = [UIStoryboard mainStoryboard];
    HGGoodSellerViewController * goodSeller = [storyBoard instantiateViewControllerWithIdentifier:@"goodSellerView"];
    goodSeller.leftNavigationType = 1;
    [self.navigationController pushViewController:goodSeller animated:YES];
    
}
-(void)addEmptyContent:(UIView*)view
{
    int iconYoffset = 144;
    int lineOffset = 10;
    
    view.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];
    
    int iphone4_Y_offset_config_Value = 0;
    if (SCREEN_HEIGHT <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    CGPoint iconCenter  = CGPointMake(SCREEN_WIDTH/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [view addSubview:icon];
    
    
    CGPoint centerPoint = CGPointMake(SCREEN_WIDTH/2, CGRectGetMaxY(icon.frame)+lineOffset + 60/2);
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 60)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"No listings can be found! Follow more people now to fill this page with listings.", nil);
    textLabel.font = [UIFont systemFontOfSize:13];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [view addSubview:textLabel];
    [textLabel sizeToFit];
    
    
    CGPoint buttonCenter = CGPointMake(SCREEN_WIDTH/2, CGRectGetMaxY(textLabel.frame)+lineOffset + 36/2);
    UIButton* button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 200, 36)];
    button.layer.cornerRadius = 4.0;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = DEFAULT_EMPTY_BOARDER_COLOR;
    UILabel* buttonTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    buttonTitle.textAlignment = NSTextAlignmentCenter;
    buttonTitle.text = NSLocalizedString(@"Go exploring!", nil);
    buttonTitle.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [button addSubview:buttonTitle];
    button.center = buttonCenter;
    [view addSubview:button];
    [button addTarget:self action:@selector(defaultEmptyAction) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark- model

- (void)_startLoadingWithCompletion:(void (^)())completion
{
    self.nextLink = @"";
    __weak __typeof(self) weakSelf = self;
    [self _loadNextPageItemsWithSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSString *jsonString = [[NSString alloc] initWithData:[responseObject JSONData] encoding:NSUTF8StringEncoding];
        NSString* currentMD5 = [[HGUtils sharedInstance] md5HexDigest:jsonString];
        if ([self.latestMD5string isEqualToString:currentMD5]) {
            //the same with last result, do nothing.
            if (completion != nil) {
                completion();
            }
            return;
        }
        else
        {
            self.latestMD5string = currentMD5;
        }
        
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        
        weakSelf.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        
        [weakSelf.home_shopItems removeAllObjects];
        
        for (id dictObj in results) {
            NSError * error = nil;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            
            if (!error) {
                [weakSelf.home_shopItems addObject:item];
            } else {
                NSLog(@"shop item %@ creation error: %@", [dictObj objectForKey:@"id"], error);
            }
        }
        
        [weakSelf.collectionView reloadData];
        
        if (completion != nil) {
            completion();
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.collectionView.infiniteScrollingView stopAnimating];
        
        //[self.followingCollectionView.infiniteScrollingView stopAnimating];
    } else {
        [self _loadNextPageItemsWithSuccess:^(NSURLSessionDataTask *task, id responseObject) {
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    BOOL bDataReapted = NO;
                    for (int i = 0; i<self.home_shopItems.count; i++) {
                        HGShopItem * currentItem = [self.home_shopItems objectAtIndex:i];
                        if ([currentItem.uid isEqualToString:item.uid]) {
                            bDataReapted = YES;
                        }
                    }
                    if (!bDataReapted) {
                        [self.home_shopItems addObject:item];
                    }
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            [self.collectionView reloadData];
            [self.collectionView.infiniteScrollingView stopAnimating];
            //[self.followingCollectionView reloadData];
            //[self.followingCollectionView.infiniteScrollingView stopAnimating];
            
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self.collectionView.infiniteScrollingView stopAnimating];
            //[self.followingCollectionView.infiniteScrollingView stopAnimating];
        }];
    }
}

#define  HOME_ITME_COUNT 50
- (void)_loadNextPageItemsWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    ///networking status check.
//    if (![[HGAppData sharedInstance].userApiClient checkNetworkStatus]) {
//        [SVProgressHUD dismiss];
//        return;
//    }
    
    NSNumber * lat = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLat]];
    NSNumber * lon = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLon]];
    
    NSString * apiEndpoint = [NSString stringWithFormat:@"home/%@", self.nextLink];
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:apiEndpoint parameters:@{@"lat": lat, @"lon": lon, @"limit":[NSNumber numberWithInt:HOME_ITME_COUNT]} success:success failure:failure];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //hide progresshud is view change.
    [SVProgressHUD dismiss];
}

#pragma mark - CollectionView Datasource & Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger count = self.home_shopItems.count;
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* keyCellString = @"HomeItemCell" ;
    HGShopItem * item;
    HGHomeItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:keyCellString forIndexPath:indexPath];
    item = [self.home_shopItems objectAtIndex:indexPath.row];
    [cell configWithItem:item];
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGShopItem * item;
    item = [self.home_shopItems objectAtIndex:indexPath.row];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"home_product" label:nil value:nil];
    
    [HGTrackingData sharedInstance].home_rf_tag = item.rf_tag;
    
    [self.parentViewController performSegueWithIdentifier:@"ShowItemPage" sender:item];
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        if([HGAppData sharedInstance].homeBanner !=nil && ![HGAppData sharedInstance].homeBanner_userClose)
        {
            int homeBannerHeight = 53;
            return homeBannerHeight;
        }
    }
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section
{
    
    return 0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"the collection kind is %@", kind);
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
        
        
        
        if ([HGAppData sharedInstance].homeBanner != nil && ![HGAppData sharedInstance].homeBanner_userClose) {
            UICollectionReusableView *view =  [collectionView dequeueReusableSupplementaryViewOfKind :kind   withReuseIdentifier:SectionTitleID   forIndexPath:indexPath];
            UIImageView* bannerView  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, view.frame.size.width, 48 )];
            
            NSString* imageUrl = [HGAppData sharedInstance].homeBanner.image_url;
            NSLog(@"the home banner url is %@", imageUrl);
            [bannerView sd_setImageWithURL: [NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_home_banner"]];
            
            
            bannerView.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapHomeBanner:)];
            [bannerView addGestureRecognizer:singleTap];
            
            UIButton* bannerClose = [[UIButton alloc] initWithFrame:CGRectMake(view.frame.size.width -30, 12, 25, 25)];
            [bannerView addSubview:bannerClose];
            [bannerClose setImage:[UIImage imageNamed:@"black-close-button"] forState:UIControlStateNormal];
            [bannerClose addTarget:self action:@selector(closeBanner) forControlEvents:UIControlEventTouchUpInside];
            
            
            [view addSubview:bannerView];
            return view;;
            
        }
    }
    
    return nil;
}

-(void)handleTapHomeBanner:(id)sender
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"banneroff" label:nil value:nil];
    NSString* actionUrl = [HGAppData sharedInstance].homeBanner.click_action;
    
    [[HGStartSystemStrAction sharedInstance] startHomeBannerAction:actionUrl withController:self withActionSource:0];
}

-(void)closeBanner
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"banner" label:nil value:nil];
    [HGAppData sharedInstance].homeBanner_userClose = YES;
    [self.collectionView reloadData];
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGShopItem * item = [self.home_shopItems objectAtIndex:indexPath.row];    
    HGItemImage * coverImage = [item.images firstObject];
    CGFloat wd = CGRectGetWidth(self.view.frame) * 0.5 - 20;
    CGSize cellSize = CGSizeMake(wd, ceil(coverImage.height * wd / coverImage.width) +WATER_FLOW_CELL_EXTEND_HEIGHT);
    
    return cellSize;
}

#pragma mark - Location Manager Delegate
-(double)roughDistanc:(NSArray*)locations
{
    ////we need this to disable the reactive status, reactive app will call location update,
    // but we do not hope to fresh the staff.
    //
    CLLocation * location = [locations lastObject];
    
    CLLocation* oldLocation = [HGAppData sharedInstance].currentLocation;
    
    double meters = [oldLocation distanceFromLocation:location];
    return meters;
}

static int minUpadteDistance = 500;

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation * location = [locations lastObject];
    //NSDate * eventDate = location.timestamp;
    //NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    double meters = [self roughDistanc:locations];
    if (meters >= minUpadteDistance -50) {
        //only refresh the listings when the location change bigger than 200.
        if (self.collectionView.contentOffset.y > 100) {
            ///if the user is now watch something, never update and fresh
        }
        else{
            [self _startLoadingByGPSupdate];
        }
    }
    
    [HGAppData sharedInstance].currentLocation = location;
    [HGAppData sharedInstance].GPSlat = location.coordinate.latitude;
    [HGAppData sharedInstance].GPSlon = location.coordinate.longitude;
    NSLog(@"latitude %+.6f, longitude %+.6f\n", location.coordinate.latitude, location.coordinate.longitude);
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error code] != kCLErrorLocationUnknown) {
        [self _stopUpdatingLocation];
    }
}

#pragma mark - Helpers


- (void)_startLocationService
{
    if (self.locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.locationManager.distanceFilter = 500;
        
        // Required for iOS 8
        
        
    }
    
    [self.locationManager startUpdatingLocation];
    
    return;
}

- (void)_stopUpdatingLocation
{
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
}

- (void)_startLoadingByGPSupdate
{
    [self _startLoadingWithCompletion:nil];
}

#pragma mark- HGHomeChildViewControllerProtocol

- (void)refreshContent
{
//    [self.collectionView setContentOffset:CGPointZero animated:YES];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
    [self _startLoadingWithCompletion:^{
        [SVProgressHUD dismiss];
    }];
}

- (void)refreshContentSilent
{
//    [self.collectionView setContentOffset:CGPointZero animated:YES];
    [self _startLoadingWithCompletion:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"tab_nearby" label:nil value:nil];
    
}

#pragma mark - Notification Handlers

- (void)handlePostItem:(NSNotification *)notification
{
    
    //change to go to item detail
    HGAppDelegate* appDelegate = (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *dict = [notification userInfo];
    NSLog(@"input dict is %@", dict);
    HGShopItem* item = [[HGShopItem alloc] init];
    item.uid = [dict valueForKey:@"itemID"];
    
    [appDelegate.tabController button1pressed];
    
    HGItemDetailController *vcItemDetail = (HGItemDetailController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCItemDetail"];
    vcItemDetail.bFromSellingPage = YES;
    vcItemDetail.item = item;
    
    [self.navigationController pushViewController:vcItemDetail animated:YES];
    
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    [self notifyHomePageForContentOffsetDidChange:offsetY];
}

- (void)notifyHomePageForContentOffsetDidChange:(CGFloat)offsetY
{
    NSLog(@"offsetY= %f", offsetY);
    offsetY += kScrollViewTopMargin;
    if (offsetY > 0) {
        if (offsetY > 44) {
            [self.delegate viewController:self navigationBarTransformProgress:1];
            
        } else {
            [self.delegate viewController:self navigationBarTransformProgress:offsetY / 44];
        }
    } else {
        [self.delegate viewController:self navigationBarTransformProgress:0];
    }
}


- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning  in HGHomePageView Controller");
    [super didReceiveMemoryWarning];
    [[SDImageCache sharedImageCache] clearMemory];
}

#pragma mark- HGSegmentSubViewControllerProtocol

- (void)segmentedPageShown:(BOOL)shown
{
    self.collectionView.scrollsToTop = shown;
    
    CGFloat headerOffsetY = [self.delegate headerViewOffset];
    [self.collectionView setContentOffset:CGPointMake(0, headerOffsetY - kScrollViewTopMargin) animated:NO];
}

@end

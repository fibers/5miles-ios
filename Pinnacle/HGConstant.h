//
//  HGConstant.h
//  Pinnacle
//
//  Created by Alex on 14-10-28.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HGUtils.h"

///////////map /////////
#define METERS_PER_MILE 1609.344
#define MERCATOR_RADIUS 85445659.44705395
#define DEFAULT_ZOOM_LEVEL 15
/////////end of map////////////

//RGB color macro
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]


#define SYSTEM_MY_ORANGE [UIColor colorWithRed:1.000f green:0.533f blue:0.188f alpha:1.00f]

#define BEATIFUL_GRAY_FONT_COLOR [UIColor colorWithRed:0xc2/255.0 green:0xc2/255.0 blue:0xc2/255.0 alpha:1.0];



#define DEFAULT_EMPTY_BOARDER_COLOR [UIColor colorWithRed:0xff/255.0 green:0x88/255.0 blue:0x30/255.0 alpha:1.0].CGColor;
#define DEFAULT_EMPTY_FONT_COLOR [UIColor colorWithRed:0x34/255.0 green:0x34/255.0 blue:0x34/255.0 alpha:1.0]

#define IMAGEPAGE_Y_OFFSET 34
#define WATER_FLOW_CELL_EXTEND_HEIGHT 57

#define ITEM_DETAIL_DELIVERY_HEIGHT 36
#define ITEM_DETAIL_AUDIO_HEIGHT 40
#define ITEM_DETAIL_CREATETIME_HEIGHT 46
#define ITEM_DETAIL_LOCATION_HEIGHT 36
#define ITEM_DETAIL_MAP_HEIGHT 120


#define MIN_RANGE 200
///////////////////////////////////////
#define USER_PROFILE_SUMMARY_LOCATION_HEIGHT 30


//////////////////////////////////////
////ox242424
#define SYSTEM_DEFAULT_FONT_COLOR_1 [UIColor colorWithRed:0x24/255.0 green:0x24/255.0 blue:0x24/255.0 alpha:1.0]
//#818181
#define PROFILE_FONT_COLOR_ITEMS [UIColor colorWithRed:0x81/255.0 green:0x81/255.0 blue:0x81/255.0 alpha:1.0]
//#343434
#define PROFILE_FONT_COLOR_LIKES [UIColor colorWithRed:0x34/255.0 green:0x34/255.0 blue:0x34/255.0 alpha:1.0]
//#f0f0f0
#define PROFILE_FONT_COLOR_SECTION_HEADER [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0]
//#cccccc
#define PROFILE_DIVIDER_COLOR [UIColor colorWithRed: 0xcc/255.0 green: 0xcc/255.0 blue:0xcc/255.0 alpha:1.0]

#define SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR  [UIColor colorWithRed:0.941f green:0.941f blue:0.941f alpha:1.00f]

#define FANCY_COLOR(A) [[HGUtils sharedInstance] FancyColorWithString:A]

#define FONT_STRING_1 @"242424"
#define FONT_TYPE_1 @"Museo"
#define FONT_TYPE_2 @"Museo-700"

#define KM_TO_MILE 0.6213712

#define MAX_USER_NAME_LENGTH 50

#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define IPHONE4X ([UIScreen mainScreen].bounds.size.height == 480)
#define IPHONE5X ([UIScreen mainScreen].bounds.size.height == 568)
#define IPHONE6 ([UIScreen mainScreen].bounds.size.height == 667)
#define IPHONE6PLUS ([UIScreen mainScreen].bounds.size.height == 736)

#define IOS_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define COLOR_AVATAR_BORDER [UIColor colorWithRed:0xff/255.0f green:0x88/255.0f blue:0x30/255.0f alpha:0.4f]

#define PADDING_SYSTEM_RECOMMENDED 8

#define DEVICE_UUID ([[[UIDevice currentDevice] identifierForVendor] UUIDString])

#define FLURRY_LOG_ERROR_WITH_DATA(e, format, ...) ([Flurry logError: [NSString stringWithFormat:[@"File: %s, line: %d. " stringByAppendingString:format], __FILE__, __LINE__, ##__VA_ARGS__] message:[e reason] exception:e])

#define FLURRY_LOG_ERROR(e) ([Flurry logError: [NSString stringWithFormat:@"File: %s, line: %d.", __FILE__, __LINE__] message:[e reason] exception:e])

// The user click a button or do another actions before login
//#define NOTIFICATION_USER_LOGIN_WITH_ACTION @"Notification.User.Login.With.Action"


#define NOTIFICATION_USER_ITEMS_DELETE @"Notification.User.items.delete"
#define NOTIFICATION_USER_ITEMS_STATUS_CHANGE @"Notification.User.items.status.change"
#define NOTIFICATION_USER_ITEMS_UNLIKE @"Notification.User.items.unlike"


//#define TESTING_
//#ifdef TESTING_
#ifdef DEBUG

#define FMBizAPIBaseURLString               @"https://api-test.5milesapp.com/api/v2/"
#define FMMessageAPIBaseURLString           @"https://api-test.5milesapp.com/api/v1/"

#define FMBizAPIBaseURLStringRealServer     @"https://api.5milesapp.com/api/v2/"
#define FMMessageAPIBaseURLStringRealServer @"https://message.5milesapp.com/api/v1/"

#define GETUI_APP_ID                        @"wFI7iyZxLT7vjzpMLJUJT3"
#define GETUI_APP_KEY                       @"EiyTodRTyQ9sYceQG9nV32"
#define GETUI_APP_SECRET                    @"Bysy0GRxyqAZBzk5PcFYV2"



#define REMOTE_PUSH_SETTING                 @"http://54.64.220.44/app/notification/"

#else

#define FMBizAPIBaseURLString               @"https://api.5milesapp.com/api/v2/"
#define FMMessageAPIBaseURLString           @"https://message.5milesapp.com/api/v1/"

#define FMBizAPIBaseURLStringRealServer     @"https://api.5milesapp.com/api/v2/"
#define FMMessageAPIBaseURLStringRealServer @"https://message.5milesapp.com/api/v1/"

#define GETUI_APP_ID                        @"h5bghbdWR77kT8cO9mIzn4"
#define GETUI_APP_KEY                       @"KaMlKSsJdm7cmJlXdoyVg3"
#define GETUI_APP_SECRET                    @"mqiuZORyml9l9P6iswx3a1"
#define REMOTE_PUSH_SETTING                 @"https://5milesapp.com/app/notification/"

#endif





////for gray testing ////
//#define GRAY_TESTING_
//#ifdef GRAY_TESTING_
//#define FMBizAPIBaseURLString               @"https://gray.5milesapp.com/api/v2/"
//#define FMMessageAPIBaseURLString           @"https://message.5milesapp.com/api/v1/"
//
//#define FMBizAPIBaseURLStringRealServer     @"https://gray.5milesapp.com/api/v2/"
//#define FMMessageAPIBaseURLStringRealServer @"https://message.5milesapp.com/api/v1/"
//
//#define GETUI_APP_ID                        @"h5bghbdWR77kT8cO9mIzn4"
//#define GETUI_APP_KEY                       @"KaMlKSsJdm7cmJlXdoyVg3"
//#define GETUI_APP_SECRET                    @"mqiuZORyml9l9P6iswx3a1"
//
//#endif


////end of gray testing////



#define SERVER_CATEGORY_LIST_V3 @"server_category_list_V3"

#define CATEGORY_ICON_SIZE 80

@interface HGConstant : NSObject

@end

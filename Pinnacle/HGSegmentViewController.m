//
//  HGSegmentViewController.m
//  LxJSDemo
//
//  Created by mumuhou on 15/7/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//
#import <CoreText/CoreText.h>
#import "HGSegmentViewController.h"

static NSInteger kNaviBarZoomOutHeight      = 20;
static NSInteger kSegmentControlHeight      = 40;

@interface HGSegmentViewController () <UIScrollViewDelegate>

@property (nonatomic, strong, readwrite) UIViewController *currentController;

@end

@implementation HGSegmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.navigationController.navigationBar.translucent = NO;
}

- (void)setPageTitles:(NSArray *)pageTitles
{
    [self setupSegmentedControlWithPageTitles:pageTitles];
    [self setupContentViewWithPageCount:[pageTitles count]];
}

- (void)setupSegmentedControlWithPageTitles:(NSArray *)pageTitles
{
    if (!self.segmentedControl) {
        HGSegmentedControl *segmentedControl = [[HGSegmentedControl alloc] initWithFrame:CGRectMake(0,
                                                                                                    64,
                                                                                                    [UIScreen mainScreen].bounds.size.width,
                                                                                                    kSegmentControlHeight)];
        segmentedControl.backgroundColor = [UIColor whiteColor];
        [segmentedControl addTarget:self action:@selector(onSegmentedSelectedChanged:) forControlEvents:UIControlEventValueChanged];
        segmentedControl.selectedSegmentIndex = 0;
        segmentedControl.sectionTitles = pageTitles;
        self.segmentedControl = segmentedControl;
        
        [self.view addSubview:self.segmentedControl];
        
        __weak typeof(self) weakSelf = self;
        
        self.segmentedControl.indexChangeBlock = ^(NSInteger index) {

            CGRect scrollViewRect = CGRectMake(weakSelf.view.bounds.size.width * index,
                                               segmentedControl.bounds.origin.y,
                                               weakSelf.view.bounds.size.width,
                                               weakSelf.contentView.bounds.size.height);
            [weakSelf.contentView scrollRectToVisible:scrollViewRect animated:YES];
        };
    }
}

- (void)setupContentViewWithPageCount:(NSInteger)pageCount
{
    if (!self.contentView) {

        const CGFloat kTabBarHeight = 49;

        CGFloat scrollViewOriginY = kNaviBarZoomOutHeight + kSegmentControlHeight;
        CGFloat scrollViewHeight = [UIScreen mainScreen].bounds.size.height - scrollViewOriginY - kTabBarHeight;

        CGRect scrollFrame = CGRectMake(0,
                                        scrollViewOriginY,
                                        [UIScreen mainScreen].bounds.size.width,
                                        scrollViewHeight);
        
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:scrollFrame];
//        scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        scrollView.contentInset = UIEdgeInsetsZero;
        scrollView.backgroundColor = [UIColor whiteColor];
        scrollView.pagingEnabled = YES;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.scrollEnabled = YES;
        scrollView.scrollsToTop = NO;
        scrollView.delegate = self;
        scrollView.bounces = NO;
        self.contentView = scrollView;
        self.contentView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * pageCount, self.contentView.bounds.size.height);
//        [self.view insertSubview:self.contentView belowSubview:self.segmentedControl];
        [self.view addSubview:self.contentView];
    }
}

- (void)onSegmentedSelectedChanged:(HGSegmentedControl *)segmentedControl
{
    [self segmentedSelectedPageShown:segmentedControl.selectedSegmentIndex];
}

- (void)setPageControllers:(NSArray *)pageControllers
{
    _pageControllers = pageControllers;
    
    [self setupChildViewControllers:pageControllers];
}

- (void)setupChildViewControllers:(NSArray *)childControllers
{
    for (NSInteger k = 0; k < [childControllers count]; ++k) {
        UIViewController *vc = childControllers[k];
        [self addChildViewController:vc];
        vc.view.frame = CGRectMake([UIScreen mainScreen].bounds.size.width * k,
                                   0,
                                   [UIScreen mainScreen].bounds.size.width,
                                   self.contentView.bounds.size.height);
        vc.view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:vc.view];
    }
}

- (void)setTitleNormalAttributes:(NSDictionary *)normalAttributes titleSelectedAttributes:(NSDictionary *)selectedAttributes
{
    self.segmentedControl.titleTextAttributes = normalAttributes;
    self.segmentedControl.selectedTitleTextAttributes = selectedAttributes;
}

- (void)segmentedSelectedPageShown:(NSInteger)index
{
    for (int k = 0; k < [self.pageControllers count]; ++k) {
        id<HGSegmentSubViewControllerProtocol> obj = self.pageControllers[k];
        
        if ([obj respondsToSelector:@selector(segmentedPageShown:)]) {
            [obj segmentedPageShown:(k == index)];
        }
    }
}

//- (void)viewDidLayoutSubviews
//{
//    [super viewDidLayoutSubviews];
////    self.contentView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * self.pageControllers.count, self.contentView.bounds.size.height);
//}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view bringSubviewToFront:self.segmentedControl];
    [self segmentedSelectedPageShown:self.segmentedControl.selectedSegmentIndex];
}


- (UIViewController *)currentController {
    return self.pageControllers[self.segmentedControl.selectedSegmentIndex];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.contentView]) {
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        
        [self.segmentedControl setSelectedSegmentIndex:page animated:YES];
        
        [self onSegmentedSelectedChanged:self.segmentedControl];
    }
}

@end

//
//  HGUserSummaryView.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-13.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGUserSummaryView.h"
#import "HGUser.h"
#import "HGUtils.h"
#import "HGAppData.h"
#import "HGConstant.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+ClickToFullScreen.h"

@interface  HGUserSummaryView()
@property (nonatomic, assign) int extraXoffset_Iphone6;
@end

@implementation HGUserSummaryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setup];
}

- (void)_setup
{
   
    [self setupAvatar];
    
    self.extraXoffset_Iphone6 = 0;
    if (SCREEN_WIDTH==320) {
        //iphone4. iphone5
        self.extraXoffset_Iphone6 = 0;
    }
    else if(SCREEN_WIDTH == 375){
        //iphone6; UI improve
        self.extraXoffset_Iphone6 = 30 ;
    }
    else
    {
        self.extraXoffset_Iphone6 = 50;
    }
    self.bottomPartBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 170, SCREEN_WIDTH, self.frame.size.height - 170)];
    [self addSubview:self.bottomPartBackground];
    self.bottomPartBackground.backgroundColor = FANCY_COLOR(@"f0f0f0");

    
    
    self.btnItemNum.titleLabel.font = self.btnFollowerNum.titleLabel.font = self.btnFollowingNum.titleLabel.font = [UIFont systemFontOfSize:17];
    self.itemNumUnit.font = self.followerUnit.font = self.followingUnit.font= [UIFont systemFontOfSize:13];
    [self addLocationView];
    
    [self addVerifyUIButton];

}

-(void)addLocationView
{
    float extraXoffset = self.extraXoffset_Iphone6;
   
    self.locationView = [[UIView alloc] initWithFrame:CGRectMake(100+extraXoffset, 55, self.frame.size.width, USER_PROFILE_SUMMARY_LOCATION_HEIGHT)];
    [self addSubview:self.locationView];
    self.locationIcon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 3, 20/2, 22/2)];
    self.locationIcon.image = [UIImage imageNamed:@"goodseller_location"];
    
    self.locationNameButton = [[UIButton alloc] initWithFrame:CGRectMake(0 + 7, 0, 200, 20)];
    
    
    self.locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, -2, 200, 20)];
    [self.locationNameButton addSubview:self.locationLabel];
    self.locationLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.locationLabel.font = [UIFont systemFontOfSize:11];
    self.locationLabel.textAlignment = NSTextAlignmentLeft;
    [self.locationView addSubview:self.locationIcon];
    [self.locationView addSubview:self.locationNameButton];
    
    self.locationLabel.text = @"";
    
    self.locationLabel.userInteractionEnabled = YES;
    self.locationIcon.userInteractionEnabled = YES;
    self.locationView.userInteractionEnabled = YES;
}

-(void)setupAvatar
{
    self.avatarView.clipsToBounds = YES;
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.layer.borderColor = [UIColor grayColor].CGColor;
    self.avatarView.layer.borderWidth = 0.0;
    self.avatarView.layer.cornerRadius = self.avatarView.frame.size.width/2;
    
    [self.avatarView enableClickToFullScreen];
    
    self.avartarCover = [[UIImageView alloc] initWithFrame:self.avatarView.frame];
    self.avartarCover.center =self.avatarView.center;
    self.avartarCover.image = [UIImage imageNamed:@"avatar-cover"];
    [self addSubview:self.avartarCover];
    
    
    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    CGPoint centerPoint = self.avatarView.center;
    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -10;
    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -10;
    self.userAvatar_verifyMark.center = centerPoint;
    [self addSubview:self.userAvatar_verifyMark];
}

static int verifyUI_y_offset = 75;
-(void)addVerifyUIButton
{
    float extraXoffset = self.extraXoffset_Iphone6;
    
    UILabel* verificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(100+extraXoffset, verifyUI_y_offset -5, 80, 30)];
    verificationLabel.text = NSLocalizedString(@"Verification:",nil);
    verificationLabel.font = [UIFont systemFontOfSize:15];
    verificationLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    [self addSubview:verificationLabel];
    
    self.userEmail_verifyButton = [[UIButton alloc] initWithFrame:CGRectMake(185+extraXoffset, verifyUI_y_offset , 21, 21)];
    [self.userEmail_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_email_not_verify"] forState:UIControlStateNormal];
    [self addSubview:self.userEmail_verifyButton];
    
    self.userFB_verifyButton = [[UIButton alloc] initWithFrame:CGRectMake(185 +21 +10+extraXoffset, verifyUI_y_offset , 21, 21)];
    [self.userFB_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_fb_not_verify"] forState:UIControlStateNormal];
    [self addSubview:self.userFB_verifyButton];
    
    self.userPhone_verifyButton = [[UIButton alloc] initWithFrame:CGRectMake(185+ 21*2+10*2+extraXoffset, verifyUI_y_offset  , 21, 21)];
    
    [self.userPhone_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_phone_not_verify"] forState:UIControlStateNormal];
    [self addSubview:self.userPhone_verifyButton];
    
    
    self.user_verifyInfoButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userPhone_verifyButton.frame)+15, verifyUI_y_offset - 5, 32, 32)];
    [self.user_verifyInfoButton setImage:[UIImage imageNamed:@"verify_info_icon"] forState:UIControlStateNormal];
    [self.user_verifyInfoButton addTarget:self action:@selector(clickVerifyInfo) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.user_verifyInfoButton];
    
}

-(void)clickVerifyInfo
{
    if ([self.delegate respondsToSelector:@selector(showUserVerifyInfoView)]) {
        [self.delegate showUserVerifyInfoView];
    }
}

- (void)configWithEntity:(HGUser *)user
{
    
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:user.portraitLink] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    
    if (user.verified != nil && user.verified.intValue == 1 ) {
         self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
         self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    
    
    if (user.email_verified != nil && user.email_verified.intValue == 1) {
        [self.userEmail_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_email_verify"] forState:UIControlStateNormal];
    }
    else{
        [self.userEmail_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_email_not_verify"] forState:UIControlStateNormal];
    }
    
    
    if(user.facebook_verified != nil && user.facebook_verified.intValue == 1)
    {
        [self.userFB_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_fb_verify"] forState:UIControlStateNormal];
        
    }
    else{
        [self.userFB_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_fb_not_verify"] forState:UIControlStateNormal];
    }
    
    if (user.mobile_phone_verified != nil && user.mobile_phone_verified.intValue == 1) {
        [self.userPhone_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_phone_verify"] forState:UIControlStateNormal];
        
    }
    else{
        [self.userPhone_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_phone_not_verify"] forState:UIControlStateNormal];
        
    }
    
    if (user.place.length > 0) {
        self.locationLabel.text = [@" " stringByAppendingString:user.place];
        self.locationView.hidden = NO;
    }
    else{
        self.locationLabel.text = [@" " stringByAppendingString:NSLocalizedString(@"unknown",nil)];
        self.locationView.hidden = YES;
    }
    
    if ([user.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
         self.bottomPartBackground.frame = CGRectMake(0, 170 - 60, self.frame.size.width, self.frame.size.height - 170+60 );
    }
   
}

-(void)iphone6LayoutUpdate
{
    
    float extraXoffset = self.extraXoffset_Iphone6;
    int extraSpaceBetweenEach = 0;
    if (extraXoffset>0) {
        extraSpaceBetweenEach = 10;
    }
        
    //[self setDivider:extraXoffset];
    self.btnItemNum.center = CGPointMake(120+extraXoffset, 30 - 8);
    self.itemNumUnit.center = CGPointMake(120+extraXoffset, 48 - 8);
    
    self.btnFollowerNum.center = CGPointMake(195+extraXoffset+extraSpaceBetweenEach,30 - 8);
    self.followerUnit.center = CGPointMake(195+extraXoffset+extraSpaceBetweenEach, 48 - 8);
    
    self.btnFollowingNum.center = CGPointMake(271+extraXoffset+extraSpaceBetweenEach*2,30 - 8);
    self.followingUnit.center = CGPointMake(271+extraXoffset+extraSpaceBetweenEach*2, 48 - 8);
    
    self.divider1.center = CGPointMake(158+extraXoffset + extraSpaceBetweenEach, self.divider1.center.y);
    self.divide2.center = CGPointMake(233+extraXoffset + (extraSpaceBetweenEach/2)*3, self.divide2.center.y);
    
    if(SCREEN_WIDTH > 320)
    {
        self.avatarView.frame = CGRectMake(22 +10, 11, 58, 58);
        self.avartarCover.center = self.avatarView.center;
        CGPoint centerPoint = self.avatarView.center;
        centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -10;
        centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -10;
        self.userAvatar_verifyMark.center = centerPoint;
    }
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    
    [self iphone6LayoutUpdate];
    
    if (self.locationView.hidden) {
         if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
         {
             [self moveDown:self.btnItemNum];
             [self moveDown:self.btnFollowerNum];
             [self moveDown:self.btnFollowingNum];
             [self moveDown:self.itemNumUnit];
             [self moveDown:self.followerUnit];
             [self moveDown:self.followingUnit];
             [self moveDown:self.divider1];
             [self moveDown:self.divide2];
         }
        
    }
    
    
}
-(void)moveDown:(UIView*)view
{
    CGPoint center = view.center;
    center.y = center.y + 7;
    view.center = center;

}

@end

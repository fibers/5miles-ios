//
//  HGDataCacheManager.m
//  Pinnacle
//
//  Created by Alex on 15-3-2.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGDataCacheManager.h"
#import "HGAppData.h"

@implementation HGDataCacheManager

+ (HGDataCacheManager *)sharedInstance
{
    static HGDataCacheManager * _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [HGDataCacheManager new];
    });
    return _sharedInstance;
}


-(void)updateCacheData:(NSString*)keyString withValueString:(NSString*)valueString
{
    if (keyString != nil && valueString != nil) {
        NSString* userAccount = [HGUserData sharedInstance].fmUserID;
        if (userAccount.length == 0) {
            return;
        }
        else{
            keyString = [userAccount stringByAppendingString:keyString];
        }
        [[NSUserDefaults standardUserDefaults] setValue:valueString forKey:keyString];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(NSString*)getCacheData:(NSString*)keyString
{
    NSString* userAccount = [HGUserData sharedInstance].fmUserID;
    if (userAccount.length == 0) {
        return nil;
    }
    else{
        keyString = [userAccount stringByAppendingString:keyString];
    }
    NSString* valueString = [[NSUserDefaults standardUserDefaults] valueForKey:keyString];
    return valueString;
    
}
@end

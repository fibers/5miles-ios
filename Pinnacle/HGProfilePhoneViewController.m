//
//  HGProfilePhoneViewController.m
//  Pinnacle
//
//  Created by Alex on 14-9-30.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGProfilePhoneViewController.h"
#import "HGCountryCodeSelectViewController.h"
#import "HGAppData.h"
#import <SVProgressHUD.h>
#import <PXAlertView+Customization.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BDKNotifyHUD.h"
#import "HGAppDelegate.h"
#import "MBProgressHUD.h"

static NSInteger kAlertTagForClose = 1001;

NSString * const HGProfilePhoneViewControllerVerifySuccessNotification = @"com.fivemiles.HGProfilePhoneViewControllerVerifySuccessNotification";

@interface HGProfilePhoneViewController ()

@property (nonatomic, assign) BOOL bHasUserActionBeforeReturn;

@property (nonatomic, strong) UILabel* bottomHint;
@end

@implementation HGProfilePhoneViewController

-(void)customizeBackButton
{
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    self.navigationItem.backBarButtonItem = backbutton;
}
-(void)autoGetCountryCode
{
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSString* userCountryCode = [currentLocale objectForKey:NSLocaleCountryCode];

    NSLog(@"the user country code is %@", userCountryCode);
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"CountryData2" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    NSArray* countryCodeArray = [data valueForKey:@"cityArray"];

    for (int i = 0; i<countryCodeArray.count; i++) {
        NSString* currentCode = countryCodeArray[i];
         i++;
        
        NSLog(@"current country code is %@", currentCode);
        NSString* currentName = countryCodeArray[i];
        if ([currentCode isEqualToString:userCountryCode]) {
            NSLog(@"OK, Find the name");
            NSRange range = [currentName rangeOfString:@"("];
            [HGAppData sharedInstance].phoneNumber_CountryName = [currentName substringToIndex:range.location + range.length -1];
            [HGAppData sharedInstance].phoneNumber_countryCode = [currentName substringFromIndex:range.location + range.length];
            [HGAppData sharedInstance].phoneNumber_countryCode = [[HGAppData sharedInstance].phoneNumber_countryCode stringByReplacingOccurrencesOfString:@")" withString:@""];
            
            
            return;
        }
    }

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bHasUserActionBeforeReturn = NO;
    [self autoGetCountryCode];

    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;

    self.tableview.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
     self.tableview.contentInset = UIEdgeInsetsMake(-18, 0, 0, 0);
    
    
    
    self.type = HGPhoneVerifyType_listing;
    if (self.type == HGPhoneVerifyType_profile) {
        self.navigationItem.title = NSLocalizedString(@"Phone number", nil);

    }
    else{
        self.navigationItem.title=NSLocalizedString(@"Verify yourself", nil);
        
      
        
    }
    
    [self commonSetup];
    [self customizeBackButton];
    [self CheckTextField];
    
    if (self.fromPage == HGProfilePhoneViewControllerFromSellPage) {
        [[HGUtils sharedInstance] gaTrackViewName:@"verifytolist_view"];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"product_view" action:@"verifyphonetolist" label:nil value:nil];
        
        
        UIImage *closeImage = [UIImage imageNamed:@"sell_closeIcon"];
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 74, 30)];
        [closeButton setImage:closeImage forState:UIControlStateNormal];
        [closeButton setImage:closeImage forState:UIControlStateHighlighted];
        [closeButton addTarget:self action:@selector(onTouchCloseButton) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
        self.navigationItem.leftBarButtonItem = closeButtonItem;
    }
}

- (void)onTouchCloseButton {
    
    if (self.fromPage == HGProfilePhoneViewControllerFromSellPage) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"verifytolist_view" action:@"notverify" label:nil value:nil];

    }
    
    if ([HGUserData sharedInstance].firstClosedProfilePhonePage) {
        [HGUserData sharedInstance].firstClosedProfilePhonePage = NO;
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:NSLocalizedString(@"If you have not yet been verified your listing will remain pending.", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"Get verified", nil), nil];
        
        alertView.tag = kAlertTagForClose;
        [alertView show];
    } else {
        [self dismissViewControllerAnimated:YES completion:^{
            ;
        }];
    }
}

-(void)CheckTextField
{
    [[self.phoneNumber.rac_textSignal
      filter:^(NSString *str) {
          return YES;
      }]
     subscribeNext:^(NSString *str) {
         NSString* contentString = (NSString*)str;
         if (contentString.length > 0) {
             if (durationCount == 0) {
                 //not in the duration after send (in 60s);
                 self.sendpassCodeButton.layer.borderColor =FANCY_COLOR(@"ff8830").CGColor;
                 [self.sendpassCodeButton setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
                 [self.sendpassCodeButton addTarget:self action:@selector(sendPassCode) forControlEvents:UIControlEventTouchUpInside];
             }
         }
         else{
             self.sendpassCodeButton.layer.borderColor =FANCY_COLOR(@"cccccc").CGColor;
             [self.sendpassCodeButton setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
             [self.sendpassCodeButton removeTarget:self action:@selector(sendPassCode) forControlEvents:UIControlEventTouchUpInside];
         }

     }];
    
    
    [[self.passcodeTextfield.rac_textSignal
      filter:^(NSString *str) {
          return YES;
      }]
     subscribeNext:^(NSString *str) {
         NSString* contentString = (NSString*)str;
         if (contentString.length > 0) {
             self.confirmButton.backgroundColor = FANCY_COLOR(@"ff7c2b");
             //[self.confirmButton setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
             [self.confirmButton addTarget:self action:@selector(sendConfirmRequest) forControlEvents:UIControlEventTouchUpInside];
         }
         else{
             //self.confirmButton.layer.borderColor =FANCY_COLOR(@"cccccc").CGColor;
             //[self.confirmButton setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
             self.confirmButton.backgroundColor = FANCY_COLOR(@"cccccc");
             [self.confirmButton removeTarget:self action:@selector(sendConfirmRequest) forControlEvents:UIControlEventTouchUpInside];
         }
         
     }];
    
   
}
-(void)setupCell1
{
    self.ClickCountry  = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, 120, 30)];
    self.ClickCountry.textAlignment = NSTextAlignmentCenter;
    self.ClickCountry.text = NSLocalizedString(@"Country/Region", nil);
    self.ClickCountry.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.ClickCountry.font = [UIFont systemFontOfSize:15];
    
    self.countryName = [[UILabel alloc] initWithFrame:CGRectMake(180, 8, 110, 30)];
    self.countryName.font = [UIFont systemFontOfSize:15];
    self.countryName.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.countryName.textAlignment = NSTextAlignmentRight;
    
}
-(void)setupCell2
{
    self.countryPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 85, 40)];
    
    self.countryPhoneCode.textAlignment = NSTextAlignmentRight;
    self.countryPhoneCode.text = [HGAppData sharedInstance].phoneNumber_countryCode;
    self.countryPhoneCode.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.countryPhoneCode.font = [UIFont systemFontOfSize:15];
    
    
    self.phoneNumber = [[UITextField alloc] initWithFrame:CGRectMake(101 + 10, 5, 200, 40)];
    self.phoneNumber.placeholder = NSLocalizedString(@"Input your phone number",nil);
    self.phoneNumber.font = [UIFont systemFontOfSize:13];
    self.phoneNumber.keyboardType = UIKeyboardTypePhonePad;
    
    
    self.sendpassCodeButton = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-132)/2,   44+ 15, 264/2, 24)];
    self.sendpassCodeButton.layer.cornerRadius = 4;
    self.sendpassCodeButton.layer.borderColor =FANCY_COLOR(@"cccccc").CGColor;
    self.sendpassCodeButton.layer.borderWidth = 1;
    self.sendpassCodeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.sendpassCodeButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.sendpassCodeButton setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
    [self.sendpassCodeButton setTitle:NSLocalizedString(@"Send Passcode",nil) forState:UIControlStateNormal];
    
    self.confirmButton = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-290)/2, 44*2 + 54+ 17 , 290, 35)];
    self.confirmButton.layer.cornerRadius = 4;
    self.confirmButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.confirmButton.titleLabel.textColor = [UIColor whiteColor];
    self.confirmButton.backgroundColor = FANCY_COLOR(@"ff7c2b");
    self.confirmButton.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.confirmButton setTitle:NSLocalizedString(@"Confirm",nil) forState:UIControlStateNormal];
    
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    self.vSeperate1 = [[UIView alloc] initWithFrame:CGRectMake(5, 44, self.view.frame.size.width, fSeperatorHeight)];
    self.vSeperate2 = [[UIView alloc] initWithFrame:CGRectMake(5, 44 + 54, self.view.frame.size.width, fSeperatorHeight)];
    self.vSeperate3 = [[UIView alloc] initWithFrame:CGRectMake(5, 44 + 54 + 44, self.view.frame.size.width, fSeperatorHeight)];
    
    self.hSeperate1 = [[UIView alloc] initWithFrame:CGRectMake(101, 0, fSeperatorHeight, 44)];
    self.hSeperate2 = [[UIView alloc] initWithFrame:CGRectMake(101, 44 + 54 ,  fSeperatorHeight, 44)];
    self.hSeperate1.backgroundColor = self.hSeperate2.backgroundColor = FANCY_COLOR(@"e4e4e4");
    
    
    self.vSeperate1.backgroundColor = self.vSeperate2.backgroundColor = self.vSeperate3.backgroundColor = FANCY_COLOR(@"e4e4e4");
    
    
    self.passcodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,44+54+5, 90, 35)];
    self.passcodeLabel.text = NSLocalizedString(@"Passcode",nil);
    self.passcodeLabel.font = [UIFont systemFontOfSize:15];
    self.passcodeLabel.textAlignment = NSTextAlignmentRight;
    self.passcodeLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    
    self.passcodeTextfield = [[UITextField alloc] initWithFrame:CGRectMake(101 + 8, 44+54 +5, 200, 35)];
    self.passcodeTextfield.placeholder = NSLocalizedString(@"Input the number you received",nil);
    self.passcodeTextfield.font = [UIFont systemFontOfSize:13];
    self.passcodeTextfield.keyboardType = UIKeyboardTypeNumberPad;

    [self.sendpassCodeButton addTarget:self action:@selector(sendPassCode) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmButton addTarget:self action:@selector(sendConfirmRequest) forControlEvents:UIControlEventTouchUpInside];
}

-(void)commonSetup
{
    [self setupCell1];
    [self setupCell2];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
  
    
    if ([HGAppData sharedInstance].phoneNumber_countryCode != nil && [HGAppData sharedInstance].phoneNumber_countryCode.length > 0) {
        self.countryPhoneCode.text = [HGAppData sharedInstance].phoneNumber_countryCode;
    }
    if ([HGAppData sharedInstance].phoneNumber_CountryName != nil && [HGAppData sharedInstance].phoneNumber_CountryName.length > 0) {
        self.countryName.text = [HGAppData sharedInstance].phoneNumber_CountryName;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.tableview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 44;
    }
    if (indexPath.section == 1) {
        return 44*2 + 54 + 70;
    }
    if (indexPath.section == 2) {
        //an empty section to make to content more easy to scroll in iphone4.
        return 140;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(void)setCellAccessIcon:(UITableViewCell*)cell
{
    UIImageView*accView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 17/2, 30/2)];
    accView.image = [UIImage imageNamed:@"uitableview_cell_access"];
    cell.accessoryView = accView;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    if (indexPath.section == 0) {
        [cell addSubview:self.ClickCountry];
        [cell addSubview:self.countryName];
        [self setCellAccessIcon:cell];
       
    }
    if (indexPath.section == 1) {
        [cell addSubview: self.countryPhoneCode];
         [cell addSubview:self.phoneNumber];
       
       
        
        
        [cell addSubview:self.sendpassCodeButton];
        [cell addSubview:self.confirmButton];
        [cell addSubview:self.vSeperate1];
        [cell addSubview:self.vSeperate2];
        [cell addSubview:self.vSeperate3];
        [cell addSubview:self.hSeperate1];
        [cell addSubview:self.hSeperate2];
        [cell addSubview:self.passcodeTextfield];
        [cell addSubview:self.passcodeLabel];
    }
    if (indexPath.section == 2) {
        
        if (self.bottomHint==nil) {
            UIView* footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
            self.bottomHint = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH-10, 55)];
            [footView addSubview:self.bottomHint];
            self.bottomHint.textColor = FANCY_COLOR(@"cccccc");
            self.bottomHint.font = [UIFont systemFontOfSize:14];
            self.bottomHint.numberOfLines = 3;
            
            if (self.fromPage == HGProfilePhoneViewControllerFromSellPage || self.fromPage == HGProfilePhoneViewControllerFromDetailPage) {
                self.bottomHint.textColor = [UIColor blackColor];
                self.bottomHint.textAlignment = NSTextAlignmentCenter;
                self.bottomHint.numberOfLines = 0;
                self.bottomHint.frame = CGRectMake(5, 0, SCREEN_WIDTH-10, 140);
                self.bottomHint.text = NSLocalizedString(@"Get verified to make your listing visible. \n5miles will only use your number to verify your identity and promote a safe community. We promise to keep your info secure and never send spam.", nil);
            } else {
                self.bottomHint.text = NSLocalizedString(@"We found you list an service, you need to verify yourself to make your service visible.\n If no, your listing will be in the status of unlisted.", nil);
            }
            
            CGSize fitSize = [self.bottomHint sizeThatFits:CGSizeMake(SCREEN_WIDTH-10, 0)];
            self.bottomHint.frame = CGRectMake(5, 0, fitSize.width, fitSize.height);
            
            [cell addSubview:footView];

        }
        cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"table view cell click");
    if ([indexPath section] == 0 ) {
       
        [self onClickCountryCode];
    }
     [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


-(void)onClickCountryCode
{
    HGCountryCodeSelectViewController * countryCode =   [[HGCountryCodeSelectViewController alloc] initWithSectionIndexes:NO];
    [self.navigationController pushViewController:countryCode animated:YES];
}
-(void)sendPassCode
{
    if (self.phoneNumber.text.length == 0) {
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Please enter a valid phone number first!",nil)];
        return;
    }
    
    if(self.fromPage == HGProfilePhoneViewControllerFromSellPage)
    {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"verifytolist_view" action:@"Sendpasscode" label:nil value:nil];
    }
    else
    {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Verifyphone_view" action:@"Sendpasscode" label:nil value:nil];
    }
    
    self.bHasUserActionBeforeReturn = YES;
    
    NSString* phonestring = [self.countryPhoneCode.text stringByAppendingString:self.phoneNumber.text];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"send_passcode/" parameters:@{@"phone":phonestring} success:^(NSURLSessionDataTask *task, id responseObject) {
        
       
        [SVProgressHUD dismiss];
        
        
        durationCount = 0;
        self.Timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownDuration) userInfo:nil repeats:YES];
        self.sendpassCodeButton.layer.borderColor =FANCY_COLOR(@"cccccc").CGColor;
        [self.sendpassCodeButton setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
        [self.sendpassCodeButton removeTarget:self action:@selector(sendPassCode) forControlEvents:UIControlEventTouchUpInside];
        NSString* resendTitle = [NSLocalizedString(@"Resend ",nil) stringByAppendingFormat:@" %d s", 60-durationCount];
        [self.sendpassCodeButton setTitle:resendTitle forState:UIControlStateNormal];
        [self.passcodeTextfield becomeFirstResponder];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
     
    }];

}
static int durationCount = 0;
-(void)countDownDuration
{
    durationCount ++;
    if (durationCount < 60) {
        self.sendpassCodeButton.layer.borderColor =FANCY_COLOR(@"cccccc").CGColor;
        [self.sendpassCodeButton setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
        [self.sendpassCodeButton removeTarget:self action:@selector(sendPassCode) forControlEvents:UIControlEventTouchUpInside];
        NSString* resendTitle = [NSLocalizedString(@"Resend ",nil) stringByAppendingFormat:@" %d s.", 60-durationCount];
        [self.sendpassCodeButton setTitle:resendTitle forState:UIControlStateNormal];
    }
    else{
        [self.Timer invalidate];
        self.Timer = nil;
        durationCount = 0;
        self.sendpassCodeButton.layer.borderColor =FANCY_COLOR(@"ff8830").CGColor;
        [self.sendpassCodeButton setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
        [self.sendpassCodeButton addTarget:self action:@selector(sendPassCode) forControlEvents:UIControlEventTouchUpInside];
         [self.sendpassCodeButton setTitle:NSLocalizedString(@"Send Passcode",nil) forState:UIControlStateNormal];
    }
}

-(void)sendConfirmRequest
{
    if (self.passcodeTextfield.text.length != 4) {
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Please enter the 4 number passcode first !",nil)];
        return;
    }
    
    if (self.fromPage == HGProfilePhoneViewControllerFromSellPage) {
         [[HGUtils sharedInstance] gaSendEventWithCategory:@"verifytolist_view" action:@"yesverify" label:nil value:nil];
    }
    else{
      [[HGUtils sharedInstance] gaSendEventWithCategory:@"Verifyphone_view" action:@"passcodeconfirm" label:nil value:nil];
    }
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
     NSString* phonestring = [self.countryPhoneCode.text stringByAppendingString:self.phoneNumber.text];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"verify_passcode/" parameters:@{@"phone":phonestring, @"pass_code":self.passcodeTextfield.text} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [SVProgressHUD dismiss];
        
        if (!self.bStartByRateUserView) {
//            [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"If there are some problems, we will connect you via phone or SMS. If you don't want to hear from us, select your preference via Profile>Settings>Notifications.", nil) withTitle:NSLocalizedString(@"Thanks for verifying your phone number.",nil)];
            
            [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Thank you for becoming verified. We will now make your listing active.", nil) withTitle:nil];
            
            
        }
        
        [HGUserData sharedInstance].bUserPhoneVerified = YES;
        
        if (self.fromPage == HGProfilePhoneViewControllerFromSellPage) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:HGProfilePhoneViewControllerVerifySuccessNotification object:nil userInfo:@{}];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)alertInformation:(NSString*)alertString
{
    
    BDKNotifyHUD* hud = [BDKNotifyHUD notifyHUDWithView:nil text:alertString withFrameWith:220 withFrameHeight:100];
    
    hud.center = CGPointMake(self.view.center.x, self.view.center.y - 20);
    
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.window addSubview:hud];
    [hud presentWithDuration:1.5f speed:0.5f inView:self.view completion:^{
        [hud removeFromSuperview];
    }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    if (!self.bHasUserActionBeforeReturn) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"Verifyphone_view" action:@"verifyphone_back" label:nil value:nil];
    }
}

#pragma mark- UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kAlertTagForClose) {
        if (0 == buttonIndex) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

@end

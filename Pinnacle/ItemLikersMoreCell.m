//
//  ItemLikersMoreCell.m
//  Pinnacle
//
//  Created by Alex on 14-12-3.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "ItemLikersMoreCell.h"
#import "HGConstant.h"
#import "HGAppData.h"
#import "ItemLikers.h"
#import <UIImageView+WebCache.h>
@implementation ItemLikersMoreCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self setup];
}
-(void)setup
{
    self.avatar = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 40, 40)];
    [self addSubview:self.avatar];
    self.userName =[[UILabel alloc] initWithFrame:CGRectMake(60, 12, 200, 20)];
    [self addSubview:self.userName];
    
    self.SeperatorView = [[UIView alloc] initWithFrame:CGRectMake(15, self.frame.size.height - 1, self.frame.size.width, 1)];
    self.SeperatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15];
    [self addSubview:self.SeperatorView];
    
    
    
    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.avatar.frame.size.width/3, self.avatar.frame.size.width/3)];
    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    CGPoint centerPoint = self.avatar.center;
    centerPoint.x = centerPoint.x + self.avatar.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    centerPoint.y = centerPoint.y + self.avatar.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    self.userAvatar_verifyMark.center = centerPoint;
    [self addSubview:self.userAvatar_verifyMark];
    
    [self addFollowButton];
    
}

-(void)followButtonAction
{
    NSLog(@"button click ");
    if (! (self.uid != nil && self.uid.length > 0) ) {
        return;
    }
    if (self.following) {
        
        UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:
                                      NSLocalizedString(@"Unfollow", nil),
                                      nil];
        actionSheet.tag = 0;
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showInView:self.superview];

        
    }
    else{
        [self startFollow];
    }
   
    
    
}

-(void)startUnfollow
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unfollow/" parameters:@{@"user_id": self.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"wholikethisitem_view" action:@"unfollowliker" label:nil value:nil];
        
        self.followButton.backgroundColor = [UIColor whiteColor];//FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
        self.following = FALSE;
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}
-(void)startFollow
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"follow/" parameters:@{@"user_id": self.uid} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"wholikethisitem_view" action:@"followliker" label:nil value:nil];
        

        
        self.followButton.backgroundColor = FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];
        self.following = TRUE;
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

-(void)addFollowButton{
    self.followButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-8-42, 9, 42, 26)];
    [self addSubview:self.followButton];
    self.followButton.layer.borderWidth = 1;
    self.followButton.layer.cornerRadius = 4;
    self.followButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.followIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 38/2, 40/2)];
    [self.followButton addSubview:self.followIcon];
    
    self.followIcon.center = CGPointMake(42/2, 26/2);
    self.followButton.backgroundColor = [UIColor whiteColor];
    self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
    [self.followButton addTarget:self action:@selector(followButtonAction) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UIActionsheet Delegate
- (void)actionSheetCancel:(UIActionSheet *)actionSheet{
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 0)
    {
        switch (buttonIndex) {
            case 0:{
                 [[HGUtils sharedInstance] gaSendEventWithCategory:@"wholikethisitem_view" action:@"unfollowliker_yes" label:nil value:nil];
                [self startUnfollow];
                break;
            }
            default:{
                 [[HGUtils sharedInstance] gaSendEventWithCategory:@"wholikethisitem_view" action:@"unfollowliker_no" label:nil value:nil];
                break;
            }
        }
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)configCell:(NSObject*)object{
    ItemLikers*likers= (ItemLikers*)object;
    
    self.userName.text = likers.nickname;
    
    self.avatar.frame = CGRectMake(10, 4, 36  ,36);
    
    int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatar.frame.size.width*2];
    NSString* image_url = [[HGUtils sharedInstance] cloudinaryLink:likers.portrait width:suggestWidth height:suggestWidth];
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    self.avatar.layer.cornerRadius = self.avatar.frame.size.width/2;
    self.avatar.layer.borderWidth = 1.0;
    self.contentMode = UIViewContentModeScaleAspectFill;
    self.avatar.layer.borderColor = SYSTEM_MY_ORANGE.CGColor;
    self.avatar.clipsToBounds = YES;
    
    if (likers.verified.intValue == 1) {
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    self.uid = likers.uid;
    
    
    
    self.following = likers.following;
    if (!likers.following) {
        
        self.followButton.backgroundColor = [UIColor whiteColor];
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon1"];
        
    }
    else{
        self.followButton.backgroundColor = FANCY_COLOR(@"ff8830");
        self.followIcon.image = [UIImage imageNamed:@"Follower_icon2"];
    }
    
    if([self.uid isEqualToString:[HGUserData sharedInstance].fmUserID])
    {
        self.followButton.hidden = YES;
        self.followIcon.hidden = YES;
    }
    else{
        self.followIcon.hidden = self.followButton.hidden = NO;
    }
}
@end

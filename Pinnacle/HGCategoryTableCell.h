//
//  HGCategoryTableCell.h
//  Pinnacle
//
//  Created by Alex on 15-3-16.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGCategoryTableCell : UITableViewCell

@property (strong, nonatomic)  UIImageView *imgIcon;
@property (strong, nonatomic)  UILabel *lbTitle;
//@property (nonatomic, strong) UIImageView* accView;




@property (nonatomic, strong) UIView* seperatorView;
@end

//
//  HGChatMapController.m
//  Pinnacle
//
//  Created by Alex on 15/5/6.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatMapController.h"
#import "HGConstant.h"
#import "HGAppData.h"
#import "HGChatMapSearchResultVC.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGChatMapPOICell.h"
#import "HGChatMapPOITopCell.h"
#import "HGChatMapWhereToMeetVC.h"
#import "PXAlertView+Customization.h"
#import "HGZipCodeVC.h"
#import <SVProgressHUD.h>

@interface HGChatMapController () <MKMapViewDelegate>
@property (nonatomic, assign) int zoomLevel;
@property (nonatomic, strong) NSMutableArray* dataArray;

@property (nonatomic, strong)UISearchBar* searchBar;
@property (nonatomic, strong)UIButton* btnSend;

@property (nonatomic, strong) MKPointAnnotation * itemLocation;
@property (nonatomic, strong) MKPointAnnotation * userLocation;

@property (nonatomic, strong) UIView* mapBottomMaskView;
@end

@implementation HGChatMapController


- (int)getZoomLevel:(MKMapView*)inputMapview {
    
    return 21-round(log2(inputMapview.region.span.longitudeDelta * MERCATOR_RADIUS * M_PI / (180.0 * inputMapview.bounds.size.width)));
    
}
-(MKCoordinateRegion)makeViewRegionWithUserlat:(double)templat withUserLon:(double)templon
                              withTheOtherLat:(double)theOtherLat withTheOtherLon:(double)theOtheLon
{
    CLLocationCoordinate2D zoomCenter;
    zoomCenter.latitude = 0.0;
    zoomCenter.longitude = 0.0;
    
    CLLocationDistance distanceMeters= 0;
    if (fabs(templat - 0.0) <  0.000001 && fabs(templon - 0.0) < 0.000001) {
        distanceMeters = 50000;
        if (distanceMeters < MIN_RANGE ) {
            distanceMeters = MIN_RANGE;
        }
    }
    else{
        zoomCenter.latitude = (templat + theOtherLat)/2;
        zoomCenter.longitude = (templon + theOtheLon)/2;
        CLLocation *orig=[[CLLocation alloc] initWithLatitude:theOtherLat longitude:theOtheLon];
        CLLocation* dist = [[CLLocation alloc] initWithLatitude:templat longitude:templon];
        
        distanceMeters =[orig distanceFromLocation:dist];
        if (distanceMeters < MIN_RANGE ) {
            distanceMeters = MIN_RANGE;
        }
    }
    
    
    
    
    MKCoordinateRegion viewRegion;
    if (distanceMeters == 0) {
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, 1.0*METERS_PER_MILE, 1.0*METERS_PER_MILE);
    }
    else{
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, distanceMeters*2, distanceMeters*2);
    }
    
    if (viewRegion.span.latitudeDelta > 180 || viewRegion.span.longitudeDelta > 180) {
        if (viewRegion.span.latitudeDelta > 180) {
            viewRegion.span.latitudeDelta = 180;
        }
        if (viewRegion.span.longitudeDelta > 180) {
            viewRegion.span.longitudeDelta = 180;
        }
    }
    if(viewRegion.span.latitudeDelta < 0 || viewRegion.span.longitudeDelta < 0)
    {
        viewRegion.span.longitudeDelta = 180;
        viewRegion.span.latitudeDelta = 180;
    }
    if (viewRegion.span.latitudeDelta > 120 || viewRegion.span.longitudeDelta > 120) {
        viewRegion.center.latitude = templat;
        viewRegion.center.longitude = templon;
    }
    
    return viewRegion;

}



-(void)addMapBottomMaskView
{
    if (self.mapBottomMaskView == nil) {
        self.mapBottomMaskView = [[UIView alloc] initWithFrame:CGRectMake(0, self.mapView.frame.size.height-5, self.view.frame.size.width, 5)];
        [self.mapView addSubview:self.mapBottomMaskView];
        self.mapBottomMaskView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chatMap_shadow"]];
       
        
        
    }
}
-(void)showMyLocation
{
    
    self.mapView.showsUserLocation = NO;
    self.mapView.delegate = self;
    [self addMapBottomMaskView];
    double templat = [[HGUtils sharedInstance] getPreferLat];
    double templon = [[HGUtils sharedInstance] getPreferLon];
    
   
    
    
    // 1
    CLLocationCoordinate2D zoomCenter;
    zoomCenter.latitude = templat;
    zoomCenter.longitude= templon;
    
    
 
    
    
    ///if user have selected prefer address, search POI near the address.
    if([HGAppData sharedInstance].chatMapUserSelectAddrLat.length > 0 &&
       [HGAppData sharedInstance].chatMapUserSelectAddrLon.length > 0)
    {
       
        templat = [HGAppData sharedInstance].chatMapUserSelectAddrLat.doubleValue;
        templon =[HGAppData sharedInstance].chatMapUserSelectAddrLon.doubleValue;
        zoomCenter.latitude = templat;
        zoomCenter.longitude = templon;
        //make viewRegion
        MKCoordinateRegion viewRegion = [self makeViewRegionWithUserlat:templat withUserLon:templon withTheOtherLat:self.theOtherUserCoord.latitude withTheOtherLon:self.theOtherUserCoord.longitude];
        // 3
        [self.mapView setRegion:viewRegion animated:YES];
    }
    else{
        //make viewRegion
        MKCoordinateRegion viewRegion = [self makeViewRegionWithUserlat:templat withUserLon:templon withTheOtherLat:self.theOtherUserCoord.latitude withTheOtherLon:self.theOtherUserCoord.longitude];
        // 3
        [self.mapView setRegion:viewRegion animated:YES];
    }
    self.zoomLevel = [self getZoomLevel:self.mapView];
    
   
    [self getLocalPOI:zoomCenter];
    [self addAnnotation];
}
-(void)customizeBackButton
{
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    self.navigationItem.backBarButtonItem = backbutton;
}
- (void)viewDidLayoutSubviews{
    // Adjust the element in profile summary area.
    
    [super viewDidLayoutSubviews];
    
  if (CGRectGetHeight(self.view.bounds) > 480) {
        self.mapView.frame = CGRectMake(0,CGRectGetMaxY(self.searchBar.frame) , self.view.frame.size.width, self.mapView.frame.size.height);
        self.tableview.frame = CGRectMake(0, CGRectGetMaxY(self.mapView.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.mapView.frame));
    }
    else{
        ////iphone4, map height = 374.
        self.mapView.frame = CGRectMake(0,CGRectGetMaxY(self.searchBar.frame) , self.view.frame.size.width, 374/2);
        self.tableview.frame = CGRectMake(0, CGRectGetMaxY(self.mapView.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.mapView.frame));
    }
  
    self.mapView.delegate = self;
    //error defense:
    self.navigationController.navigationBar.hidden = NO;
}


-(void)customSearchBar
{
    // Do any additional setup after loading the view.
    UIView* searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 34)];
    [self.view addSubview:searchView];
    
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 34)];;
    
    
    
    self.searchBar.barStyle = UIBarStyleDefault;
    self.searchBar.placeholder = NSLocalizedString(@"Search or enter an address", nil);
    self.searchBar.delegate = self;
    self.searchBar.tintColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.searchBar.barTintColor = [UIColor whiteColor];
    self.searchBar.userInteractionEnabled = NO;
    for (UIView *subView in self.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                searchBarTextField.backgroundColor = FANCY_COLOR(@"ededed");
            }
        }
    }
    
    
    [searchView addSubview:self.searchBar];
     UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startMapSearchResult)];
    searchView.userInteractionEnabled = YES;
    [searchView addGestureRecognizer:singleTap];
    
    
    //temp solution , cover the border of searchbar, if we find better solution later...
    UIView* tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 5)];
    tempView.backgroundColor = [UIColor whiteColor];
    
    
    UIView* bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, 4.5, self.view.frame.size.width, 0.5)];
    UIView* tempView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, 5)];
    tempView2.backgroundColor = [UIColor whiteColor];
    [tempView2 addSubview:bottomLine];
    bottomLine.backgroundColor = FANCY_COLOR(@"b2b2b2");
    
    [searchView addSubview:tempView];
    [searchView addSubview:tempView2];
    
    
    
    
}

-(void)dealloc
{
    [HGAppData sharedInstance].chatMapUserSelectSearchResultItem = nil;
}
- (void)viewDidLoad {
    
    
    [[HGUtils sharedInstance] gaTrackViewName:@"selectlocation"];
    [super viewDidLoad];
    [HGAppData sharedInstance].chatMapSearchKeyString= @"";
    [HGAppData sharedInstance].chatMapUserSelectAddrLat = @"";
    [HGAppData sharedInstance].chatMapUserSelectAddrLon = @"";
    [HGAppData sharedInstance].chatMapUserSelectSearchResultItem = nil;
    
    [self customSearchBar];
    [self customizeBackButton];
    [self addRightButton];
    [self addLeftButton];
    
    self.dataArray = [[NSMutableArray alloc] init];
    self.annotionArray = [[NSMutableArray alloc] init];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.tableview];
    
    
    self.zoomLevel = DEFAULT_ZOOM_LEVEL;
    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    self.navigationItem.title = NSLocalizedString(@"Select location", nil);
    
    ////do not have GPS access right, remind user using Zipcode
    {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        // If the status is denied or only granted for when in use, display an alert
        if (status == kCLAuthorizationStatusDenied && [HGUserData sharedInstance].zipcodeString.length == 0)
        {
            HGZipCodeVC* vc= [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"ZipCode"];
            vc.currentViewType = zipViewTypeUserRegister;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.dataArray.count > 0) {
        [self.dataArray removeAllObjects];
    }
    [self.tableview reloadData];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.searchBar.showsCancelButton = NO;
    [self.searchBar resignFirstResponder];
    
    
    ////do not have GPS access right, remind user using Zipcode
    {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        // If the status is denied or only granted for when in use, display an alert
        if (status == kCLAuthorizationStatusDenied && [HGUserData sharedInstance].zipcodeString.length == 0)
        {
            HGZipCodeVC* vc= [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"ZipCode"];
            vc.currentViewType = zipViewTypeUserRegister;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
    [self showMyLocation];
    
}


-(void)sortDataArray:(NSMutableArray*)POIArray
{
    NSComparator cmptr = ^(id obj1, id obj2){
        MKMapItem* poi1 = (MKMapItem*)obj1;
        MKMapItem* poi2 = (MKMapItem*)obj2;
    
        CLLocation* poi1Loc = [[CLLocation alloc] initWithLatitude:poi1.placemark.coordinate.latitude longitude:poi1.placemark.coordinate.longitude];
        CLLocation* poi2Loc = [[CLLocation alloc] initWithLatitude:poi2.placemark.coordinate.latitude longitude:poi2.placemark.coordinate.longitude];
        CLLocation* baseLoc = [[CLLocation alloc] initWithLatitude:[[HGUtils sharedInstance]  getPreferLat] longitude:[[HGUtils sharedInstance] getPreferLon]];
        
        if ([HGAppData sharedInstance].chatMapUserSelectSearchResultItem!=nil) {
            ///if user select some POI, using this poi as the base Loc.not user's location
             baseLoc = [[CLLocation alloc] initWithLatitude:[HGAppData sharedInstance].chatMapUserSelectAddrLat.floatValue longitude:[HGAppData sharedInstance].chatMapUserSelectAddrLon.floatValue];
        }
        
        
        
        double distance1 = [baseLoc distanceFromLocation:poi1Loc];
        double distance2 = [baseLoc distanceFromLocation:poi2Loc];
        
        if (distance1 > distance2) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if (distance1 < distance2) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    NSArray* sortedArray = [POIArray sortedArrayUsingComparator:cmptr];
    [POIArray removeAllObjects];
    [POIArray addObjectsFromArray:sortedArray];
    
}
//static int indexOfPOI= 0;
-(void)getLocalPOI:(CLLocationCoordinate2D)currentLocationCoordinate
{
    MKCoordinateSpan span = {0.005,0.005};
    MKCoordinateRegion region = {currentLocationCoordinate,span};
    
    MKLocalSearchRequest* localSearchRequest = [[MKLocalSearchRequest alloc] init] ;
    localSearchRequest.region = region;
    
    
    
    
    NSArray* keysString = [[HGAppData sharedInstance].chatMapSysteSuggestKeys componentsSeparatedByString:@","];
    
    self.defaultSelectedPOIindex= 0;
    
    [SVProgressHUD show];
    for (int i = 0; i<keysString.count; i++) {
        NSString* key = [keysString objectAtIndex:i];
        localSearchRequest.naturalLanguageQuery = key;
        
        
        
        MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:localSearchRequest];
        
        [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error)
         {
             //经测试，这里每次最多只有10条记录
             if (error) {
                 [SVProgressHUD dismiss];
                 NSLog(@"error info is：%@",error);
             }else{
                 int nCount = 0;
                 
                 for (MKMapItem *mapItem in response.mapItems) {
                     nCount ++;
                     if (nCount > 4) {
                         ///每个关键字只取最多4个结果
                         break;
                     }
                    
                     
                     [self.dataArray addObject:mapItem];
                     NSLog(@"the title POI is %@", mapItem.name);
                     NSLog(@"the place mark is %@", mapItem.placemark);
                     NSLog(@"the mapItem is %@", mapItem);
                 }
                
                 if (i==keysString.count/2 || i==keysString.count-1) {
                     //the last one search results return.
                     
                     
                     NSMutableArray* sortedMapItems = self.dataArray ;
                     if (i==keysString.count-1 && [HGAppData sharedInstance].chatMapUserSelectSearchResultItem!=nil) {
                         //remove the user select before sort,
                         [self.dataArray removeObjectAtIndex:0];
                     }
                     [self sortDataArray:sortedMapItems];
                     if ( (i==keysString.count-1 || i==keysString.count/2) && [HGAppData sharedInstance].chatMapUserSelectSearchResultItem !=nil)
                     {
                         [self.dataArray insertObject:[HGAppData sharedInstance].chatMapUserSelectSearchResultItem atIndex:0];
                     }
                     
                     
                     [SVProgressHUD dismiss];
                    {
                        MKMapItem *mapItem = [self.dataArray objectAtIndex:0];
                        MKPointAnnotation * annotation = [[MKPointAnnotation alloc] init];
                        annotation.coordinate = mapItem.placemark.location.coordinate;
                        annotation.title = mapItem.name;
                         [self configAnnotation: annotation.coordinate];
                         ///change the mapview region
                         MKCoordinateRegion viewRegion = [self makeViewRegionWithUserlat:mapItem.placemark.coordinate.latitude  withUserLon:mapItem.placemark.coordinate.longitude withTheOtherLat:[[HGUtils sharedInstance]getPreferLat ] withTheOtherLon:[[HGUtils sharedInstance] getPreferLon]];
                         
                         [self.mapView setRegion:viewRegion animated:YES];
                     }


                 }///end for final sort the results.
                  [self.tableview reloadData];
                
             }
         }];

    }//end of for
   
   
    
}


#pragma mark - UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.dataArray.count == 0) {
        return 0;
    }
    
    else{
        ///we have a extra top cell;
        return self.dataArray.count + 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 52;
    }
    else{
        return 70;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        HGChatMapPOITopCell * cell = [tableView dequeueReusableCellWithIdentifier:@"chatMapPOITopCell" forIndexPath:indexPath];
        
        
        ///if user have selected prefer address, search POI near the address.
        if([HGAppData sharedInstance].chatMapUserSelectAddrLat.length > 0 &&
           [HGAppData sharedInstance].chatMapUserSelectAddrLon.length > 0)
        {
            [cell configCell:NSLocalizedString(@"The following location has been selected according to your search.", nil)];
        }
        else{
            [cell configCell:NSLocalizedString(@"5miles has chosen some locations to safely meet up and exchange in-person.", nil)];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else{
        
        
        if ([HGAppData sharedInstance].chatMapUserSelectAddrLat > 0 && [HGAppData sharedInstance].chatMapUserSelectAddrLon > 0) {
             [[HGUtils sharedInstance] gaSendEventWithCategory:@"selectlocation_view" action:@"change_seachlocation" label:nil value:nil];
        }
        else{
             [[HGUtils sharedInstance] gaSendEventWithCategory:@"selectlocation_view" action:@"changesuggestlocation" label:nil value:nil];
        }
        HGChatMapPOICell *cell = [tableView dequeueReusableCellWithIdentifier:@"chatMapPOICell" forIndexPath:indexPath];
        
        int reallyIndex = (int)(indexPath.row - 1);
        MKMapItem* mapitem = [self.dataArray objectAtIndex:reallyIndex];
        
        BOOL bSelected = NO;
        if (self.defaultSelectedPOIindex == reallyIndex) {
            bSelected = YES;
        }
        [cell configCell:mapitem bShowSelectedIcon:bSelected];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
      
         return cell;
    }
    
    
   
    
   
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    else{
        int reallyIndex = (int)(indexPath.row - 1);
        MKMapItem* mapitem = [self.dataArray objectAtIndex:reallyIndex];
        [self configAnnotation:mapitem.placemark.coordinate];
        
        self.defaultSelectedPOIindex = reallyIndex;
        
        
        
        [self.tableview reloadData];
        ///change the mapview region
        MKCoordinateRegion viewRegion = [self makeViewRegionWithUserlat:mapitem.placemark.coordinate.latitude  withUserLon:mapitem.placemark.coordinate.longitude withTheOtherLat:[[HGUtils sharedInstance] getPreferLat] withTheOtherLon:[[HGUtils sharedInstance] getPreferLon ]];
        
        [self.mapView setRegion:viewRegion animated:YES];
    }
    
    
}
#pragma mark - MapView Delegate

#define ITEM_LOCATION_TITLE @"itemLocation"
#define USER_LOCATION_TITLE @"userLocation"
-(void)addAnnotation
{
    if(self.itemLocation == nil )
    {
        self.itemLocation = [[MKPointAnnotation alloc] init];
        self.itemLocation.title = ITEM_LOCATION_TITLE;
        self.userLocation = [[MKPointAnnotation alloc] init];
        self.userLocation.title = USER_LOCATION_TITLE;
        [self.mapView addAnnotation:self.itemLocation];
        
        CLLocationCoordinate2D userCood =(CLLocationCoordinate2D){[[HGUtils sharedInstance] getPreferLat],[[HGUtils sharedInstance] getPreferLon]} ;
        self.userLocation.coordinate = userCood;
        [self.mapView addAnnotation:self.userLocation];
    }
   
}

-(void)configAnnotation:(CLLocationCoordinate2D)coord2d
{
    self.itemLocation.coordinate = coord2d;
    CLLocationCoordinate2D userCood =(CLLocationCoordinate2D){[[HGUtils sharedInstance] getPreferLat],[[HGUtils sharedInstance] getPreferLon]} ;
    self.userLocation.coordinate = userCood;
    
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"Annotation";
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        
        MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView == nil)
        {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            MKPointAnnotation* mk = (MKPointAnnotation*)annotation;
            if ([mk.title isEqualToString:ITEM_LOCATION_TITLE] ) {
                annotationView.image = [UIImage imageNamed:@"chatMapPOIPoint"];
            }
            if ([mk.title isEqualToString:USER_LOCATION_TITLE]) {
                annotationView.image = [UIImage imageNamed:@"map_user_location_big"];
            }
            
        }
        else
        {
            annotationView.annotation = annotation;
            //[self changeAnnotationFrame:annotationView];
        }
        
        return annotationView;
    }
    else if([annotation isKindOfClass:[MKUserLocation class]]){
        MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:@"userlocation"];
        if(annotation==mapView.userLocation)
        {
            if (annotationView == nil) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"userlocation"];
                //annotationView.canShowCallout = YES;
            }
            annotationView.image = [UIImage imageNamed:@"map_user_location_big"];
            
            //hidden the user current location.GPS.
            return nil;
        }
    }
    
    return nil;
}
#pragma mark - SearchBar Delegate
-(void)startMapSearchResult
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"select_searchlocation" action:@"clicksearchlocation" label:nil value:nil];
    HGChatMapSearchResultVC *vcChat = (HGChatMapSearchResultVC *)[[UIStoryboard messageStoryboard]instantiateViewControllerWithIdentifier:@"chatMapSearch"];
    [self.navigationController pushViewController:vcChat animated:YES];
}


-(void)backButton
{
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"selectlocation_view" action:@"location_cancel" label:nil value:nil];
    [self dismissViewControllerAnimated:YES completion:^(){}];

}
-(void)addLeftButton
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(backButton) ];
}


-(void)onTouchSend
{
    
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"selectlocation_view" action:@"location_send" label:nil value:nil];
    if (self.dataArray.count == 0) {
        return;
    }
    [self disableSendButton];
    
    
    MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
    MKCoordinateRegion viewRegion;
    MKMapItem* mapitem = [self.dataArray objectAtIndex:self.defaultSelectedPOIindex];
    NSString* poiTitle = mapitem.placemark.name;

    NSString* poiAddress = mapitem.placemark.title;
    NSNumber* latNumber = [[NSNumber alloc] initWithDouble:mapitem.placemark.coordinate.latitude];
    NSNumber* lonNumber = [[NSNumber alloc] initWithDouble:mapitem.placemark.coordinate.longitude];
   
    CLLocationCoordinate2D zoomCenter;
    zoomCenter.latitude = latNumber.doubleValue;
    zoomCenter.longitude = lonNumber.doubleValue;

    //sending out a default size 2km*2km map pictures.
    viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, 2000, 2000);
   
    
    options.region = viewRegion;
    options.scale = [UIScreen mainScreen].scale;
    options.size = self.mapView.frame.size;
    
   
    
     __weak HGChatMapController* weakSelf = self;
    MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
    [snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        // get the image associated with the snapshot
        UIImage *image = snapshot.image;
        // Get the size of the final image
        CGRect finalImageRect = CGRectMake(0, 0, image.size.width, image.size.height);
        
        
        // ok, let's start to create our final image
        UIGraphicsBeginImageContextWithOptions(image.size, YES, image.scale);
        // first, draw the image from the snapshotter
        [image drawAtPoint:CGPointMake(0, 0)];
        // now, let's iterate through the annotations and draw them, too
        
        static NSString *identifier = @"Annotation";
        
        for (id<MKAnnotation>annotation in self.mapView.annotations)
        {
            MKPointAnnotation* mk = (MKPointAnnotation*)annotation;
            MKPinAnnotationView* annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            if ([mk.title isEqualToString:ITEM_LOCATION_TITLE] || [mk.title isEqualToString:USER_LOCATION_TITLE]) {
                CGPoint point = [snapshot pointForCoordinate:annotation.coordinate];
                if (CGRectContainsPoint(finalImageRect, point))
                {
                    CGPoint pinCenterOffset = annotationView.centerOffset;
                    point.x -= annotationView.bounds.size.width / 2.0;
                    point.y -= annotationView.bounds.size.height / 2.0;
                    point.x += pinCenterOffset.x;
                    point.y += pinCenterOffset.y;
                    
                    
                    if ([mk.title isEqualToString:ITEM_LOCATION_TITLE] ) {
                        annotationView.image = [UIImage imageNamed:@"chatMapPOIPoint"];
                         [annotationView.image drawAtPoint:point];
                    }
                   
                }

            }
        }
        
        // grab the final image
        
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(chatMapContrller:startSendingLocation:contentDictionary:)]) {
            NSDictionary*dict = @{@"place_name":poiTitle,@"address_name":poiAddress,@"lat":latNumber ,@"lon":lonNumber};
            [weakSelf.delegate chatMapContrller:weakSelf startSendingLocation:finalImage contentDictionary:dict];
        }
    }];
}
-(void)addRightButton
{
    
     UIView* rightBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    
    self.btnSend = [[UIButton alloc] initWithFrame:CGRectMake(13, 0, 65, 30)];
    self.btnSend.clipsToBounds = YES;
    self.btnSend.layer.cornerRadius = 3;
    self.btnSend.layer.borderWidth = 1;
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnSend.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.btnSend setTitle:NSLocalizedString(@"Send",nil) forState:UIControlStateNormal];
    [self.btnSend setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [self.btnSend setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.btnSend addTarget:self action:@selector(onTouchSend) forControlEvents:UIControlEventTouchUpInside];
    
    [rightBackView addSubview:self.btnSend];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBackView];
    
    [self enableSendButton];
    
}
- (void)enableSendButton{
    self.btnSend.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}
-(void)disableSendButton
{
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnSend.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}


-(float)getDistanceString:(CLLocationCoordinate2D)coor2d
{
    float distance = 0;
    
    double userLat = [[HGUtils sharedInstance] getPreferLat];
    double userlon = [[HGUtils sharedInstance] getPreferLon];
    CLLocation* userLoc = [[CLLocation alloc] initWithLatitude:userLat longitude:userlon];
    
    
    CLLocation * itemLoc = [[CLLocation alloc] initWithLatitude:coor2d.latitude longitude:coor2d.longitude];
    if (fabs(coor2d.latitude - 0.0) < 0.000001
        && fabs(coor2d.longitude - 0.0) < 0.000001) {
        distance = -1.0;;
    }
    else if(fabs(userLat - 0.0)< 0.000001
            && fabs(userlon - 0.0) < 0.000001)
    {
        distance = - 1.0;
    }
    else{
        distance = [itemLoc distanceFromLocation:userLoc] * 0.001;
    }
    
    return distance;
    
}

- (void)mapView:(MKMapView *)inputMapView regionDidChangeAnimated:(BOOL)animated {
    int newZoomLevel = [self getZoomLevel:inputMapView];
    if (newZoomLevel > self.zoomLevel) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"wheretomeet_view" action:@"mapzoomin" label:nil value:nil];
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"wheretomeet_view" action:@"mapzoomout" label:nil value:nil];
    }
    
    self.zoomLevel = newZoomLevel;
}
@end

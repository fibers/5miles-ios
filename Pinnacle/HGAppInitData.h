//
//  HGAppInitData.h
//  Pinnacle
//
//  Created by Alex on 15/5/4.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HGCategory.h"
@interface HGAppInitData : NSObject


@property (nonatomic, strong) NSArray * currencies;
@property (nonatomic, strong) NSDictionary* currenciesSymbolDict;
@property (nonatomic, strong) NSArray* default_placehold_colors;

@property (nonatomic, strong) NSMutableArray* categoryArray_c1;
@property (nonatomic ,strong) NSMutableArray* category_level1_localIcon;

+ (HGAppInitData *)sharedInstance;
-(int)getParentCategoryID:(NSString*)subCategoryID;
@end

//
//  HGVoiceRecordController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-18.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGVoiceRecordController.h"
#import "HGUtils.h"
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import "BDKNotifyHUD.h"
@interface HGVoiceRecordController ()

@property (nonatomic, strong) NSTimer * timerForPitch;
@property (nonatomic, strong) BDKNotifyHUD* voiceStatusView;

@property(nonatomic, assign) long long int startSecond;
@property(nonatomic, assign) long long int endSecond;

@end

@implementation HGVoiceRecordController

- (id)initWithGaugeView:(UIView *)gaugeView
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
-(void)setRecordVoiceStatusView:(UIView*)view
{
    self.voiceStatusView = (BDKNotifyHUD*)view;
}
- (void)startRecording
{
    NSLog(@"startRecording");
    
    
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    long long int date = (long long int)time;
    self.startSecond = date;
    self.endSecond = date;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:nil];
    
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] initWithCapacity:10];
//    [recordSettings setObject:[NSNumber numberWithInt: kAudioFormatAppleLossless] forKey: AVFormatIDKey];
//    [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
//    [recordSettings setObject:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];

    [recordSettings setObject:[NSNumber numberWithInt: kAudioFormatMPEG4AAC] forKey: AVFormatIDKey];
    [recordSettings setObject:[NSNumber numberWithFloat:16000.0] forKey: AVSampleRateKey];
    [recordSettings setObject:[NSNumber numberWithInt:32000] forKey:AVEncoderBitRateKey];
    [recordSettings setObject:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
    
//    [recordSettings setObject:[NSNumber numberWithInt: kAudioFormatAMR] forKey: AVFormatIDKey];
//    [recordSettings setObject:[NSNumber numberWithFloat:8000.0] forKey: AVSampleRateKey];
//    [recordSettings setObject:[NSNumber numberWithInt:12200] forKey:AVEncoderBitRateKey];
//    [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
//    [recordSettings setObject:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
    [recordSettings setObject:[NSNumber numberWithInt: AVAudioQualityMedium] forKey: AVEncoderAudioQualityKey];

    NSString *soundFilePath = [NSTemporaryDirectory()
                               stringByAppendingPathComponent:@"recordTest.m4a"];
    
    self.soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    
    NSError *error = nil;
    self.audioRecorder = [[ AVAudioRecorder alloc] initWithURL:self.soundFileURL settings:recordSettings error:&error];
    self.audioRecorder.meteringEnabled = YES;
    if ([self.audioRecorder prepareToRecord] == YES){
        self.audioRecorder.meteringEnabled = YES;
        [self.audioRecorder record];
        self.timerForPitch =[NSTimer scheduledTimerWithTimeInterval: 0.01 target: self selector: @selector(levelTimerCallback:) userInfo: nil repeats: YES];
    }else {
        NSLog(@"Start recording error: %@)" , error);
    }
}

- (void)stopRecording
{
    NSLog(@"stopRecording");
    [self.audioRecorder stop];
    NSLog(@"stopped");
    
    [self.timerForPitch invalidate];
    self.timerForPitch = nil;
}

- (void)playRecording
{
    NSLog(@"playRecording");
   
    // Init audio with playback capability
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    
    NSError *error;
    /*NSDictionary * attribs = [[NSFileManager defaultManager] attributesOfItemAtPath:self.soundFileURL.path error:&error];
    NSNumber * filesize = [attribs objectForKey:NSFileSize];
    NSLog(@"sound file size : %@", filesize);*/

    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.soundFileURL error:&error];
    self.audioPlayer.numberOfLoops = 0;
    NSLog(@"sound file duration: %f", self.audioPlayer.duration);
    [self.audioPlayer play];
    NSLog(@"playing");
}

- (void)stopPlaying
{
    NSLog(@"stopPlaying");
    [self.audioPlayer stop];
    NSLog(@"stopped");
}

- (void)pausePlaying
{
    
    [self.audioPlayer pause];
}

- (void)resumePlaying
{
    [self.audioPlayer play];
}

#pragma mark - Helpers 

- (void)levelTimerCallback:(NSTimer *)timer
{
	[self.audioRecorder updateMeters];
    
//    float linear = pow (10, [self.audioRecorder peakPowerForChannel:0] / 20);
    float linear1 = pow (10, [self.audioRecorder averagePowerForChannel:0] / 20);
    
    CGFloat pitch = 0.0;
    if (linear1 > 0.03) {
        pitch = linear1+.20;//pow (10, [audioRecorder averagePowerForChannel:0] / 20);//[audioRecorder peakPowerForChannel:0];
    }
    
    //self.barGauge.value = pitch;//linear1+.30;
    //NSLog(@"the voice status value is %f", pitch);
    
    int index = pitch / 0.1 + 1;
    if (index >= 8) {
        index = 8;
    }
    NSString* imagename = [@"voice_status" stringByAppendingFormat:@"%d", index];
    [self.voiceStatusView updateImage:[UIImage imageNamed:imagename]];
    
    
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    long long int date = (long long int)time;
    self.endSecond = date;
    self.voiceDuration = (int)(self.endSecond - self.startSecond);
    if (self.endSecond - self.startSecond > 60) {
        NSLog(@"should stop recording");
        PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"Max record time 60s reach",nil) cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            
        }];
        [view useDefaultIOS7Style];
       [self stopRecording];
    }
    
}


@end

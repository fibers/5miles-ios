//
//  HGAvatarView.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-31.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGAvatarView.h"
#import "HGUtils.h"
@implementation HGAvatarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.clipsToBounds = YES;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 2;
        self.layer.cornerRadius = CGRectGetWidth(frame) * 0.5;
        self.contentMode = UIViewContentModeScaleAspectFill;
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame imageLink:(NSString *)imageLink
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.layer.borderWidth = 2;
        self.layer.cornerRadius = CGRectGetWidth(frame) * 0.5;

        int suggestAvatarSize = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.frame.size.width*2];
         NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:imageLink width:suggestAvatarSize height:suggestAvatarSize];
        
        [self sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  HGFBUtils.m
//  Pinnacle
//
//  Created by shengyuhong on 15/4/1.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGFBUtils.h"
#import "HGAppData.h"
#import "SVProgressHUD.h"
#import "HGAppDelegate.h"
#import <Branch/Branch.h>

#define USER_PAGE_URL_FORMAT @"https://5milesapp.com/person/%@"
#define ITEM_PAGE_URL_FORMAT @"https://5milesapp.com/item/%@"

@implementation HGFBUtils

+ (HGFBUtils *)sharedInstance{
    
    static HGFBUtils *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [HGFBUtils new];
    });
    
    return _sharedInstance;
}

- (BOOL)shouldRespondWithNotification:(NSNotification *)notification{
    NSString *notificationName = [notification name];
    if([notificationName isEqualToString:FBSDKAccessTokenDidChangeNotification]){
        if([[HGUserData sharedInstance] isLoggedIn] && [HGUserData sharedInstance].bUserFBVerified ){
            return YES;
        }
    }else if([notificationName isEqualToString:FBSDKProfileDidChangeNotification]){
        if([[HGUserData sharedInstance] isLoggedIn] && [HGUserData sharedInstance].bUserFBVerified){
            return YES;
        }
    }
    
    return NO;
}

- (void)notificationDidChangeAccessToken:(NSNotification *)notification{
    
    if([self shouldRespondWithNotification:notification]){
        
        NSDictionary *userInfo = notification.userInfo;
        FBSDKAccessToken *fbNewAccessToken = [userInfo objectForKey:FBSDKAccessTokenChangeNewKey];
        
        if(![self isValidAccessToken:fbNewAccessToken]){
            return;
        }
        
        BOOL isUserIDChanged = [[userInfo objectForKey:FBSDKAccessTokenDidChangeUserID] boolValue];
        
        if([[HGUserData sharedInstance] isLoggedIn]){
            if( isUserIDChanged && [HGUserData sharedInstance].accountType == HGAccountTypeFacebook){
                // The user logged in with facebook will be logged out due to the change of the facebook id
                [(HGAppDelegate *)[UIApplication sharedApplication].delegate logUserOut];
            }else{
                // If the facebook id does't change or the user is a email account who have linked with facebook.
                [[HGUserData sharedInstance] updateFacebookToken:fbNewAccessToken];
                
                [self linkFacebookAccountWithSuccess:^(NSDictionary *reponse) {} withFailure:^(NSError *error) {}];
            }

        }
    }
    
}


- (void)requestPermissions:(BOOL)isReadPermission withPermissions:(NSArray *)arrayPermissions completion:(FacebookPermissionsCompletionBlock)completionBlock{
    
    NSMutableSet *setPermissions = [[NSMutableSet alloc] initWithArray:arrayPermissions];
    
    FBSDKAccessToken *fbAccessToken = [FBSDKAccessToken currentAccessToken];
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    
    // If the facebook access token exists, check the granted permissions.
    if([self isValidAccessToken: fbAccessToken] && [setPermissions isSubsetOfSet:[fbAccessToken permissions]]){
        
        [[HGUserData sharedInstance] updateFacebookToken:fbAccessToken];
        
        if( [[HGUserData sharedInstance] isLoggedIn] && [HGUserData sharedInstance].accountType == HGAccountTypeEmail && ![HGUserData sharedInstance].bUserFBVerified){
        
            [self linkFacebookAccountWithSuccess:^(NSDictionary *reponse) {
                completionBlock(nil);
            } withFailure:^(NSError *errorLinkFB) {
                completionBlock(errorLinkFB);
            }];
            
        }else{
            completionBlock(nil);
        }
        
    }else{
        
        if( isReadPermission ){
            [loginManager logInWithReadPermissions:arrayPermissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *errorRequestReadPermission) {
                
                if(errorRequestReadPermission){
                    completionBlock(errorRequestReadPermission);
                } else if (result.isCancelled || [result.declinedPermissions intersectsSet:setPermissions]) {
                    
                    NSError *permissionError = [NSError errorWithDomain:ErrorDomainFacebook code:HGErrorFacebookRequestPermission userInfo:@{NSLocalizedDescriptionKey:@"Require facebook permision"}];
                    completionBlock(permissionError);
                    
                }else{
                    
                    [[HGUserData sharedInstance] updateFacebookToken:result.token];
                    
                    if( [[HGUserData sharedInstance] isLoggedIn] && [HGUserData sharedInstance].accountType == HGAccountTypeEmail && ![HGUserData sharedInstance].bUserFBVerified){
                        
                        [self linkFacebookAccountWithSuccess:^(NSDictionary *reponse) {
                            completionBlock(nil);
                        } withFailure:^(NSError *errorLinkFB) {
                            completionBlock(errorLinkFB);
                        }];
                        
                    }else{
                        completionBlock(nil);
                    }
                }
            }];
            
        }else{
            [loginManager logInWithPublishPermissions:arrayPermissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *errorRequestPublishPermission) {
                
                if(errorRequestPublishPermission){
                    
                    completionBlock(errorRequestPublishPermission);
                    
                } else if (result.isCancelled || [result.declinedPermissions intersectsSet:setPermissions]) {
                    
                    NSError *permissionError = [NSError errorWithDomain:ErrorDomainFacebook code:HGErrorFacebookRequestPermission userInfo:@{NSLocalizedDescriptionKey:@"Require facebook permision"}];
                    completionBlock(permissionError);
                    
                }else{
                    
                    [[HGUserData sharedInstance] updateFacebookToken:result.token];
                    
                    if( [[HGUserData sharedInstance] isLoggedIn] && [HGUserData sharedInstance].accountType == HGAccountTypeEmail && ![HGUserData sharedInstance].bUserFBVerified){
                        
                        [self linkFacebookAccountWithSuccess:^(NSDictionary *reponse) {
                            completionBlock(nil);
                        } withFailure:^(NSError *errorLinkFB) {
                            completionBlock(errorLinkFB);
                        }];
                        
                    }else{
                        completionBlock(nil);
                    }
                }
            }];
        }
    }
    
}

- (BOOL)isValidAccessToken:(FBSDKAccessToken *)fbAccessToken{
    return fbAccessToken && [fbAccessToken.userID length] > 0 && [fbAccessToken.tokenString length] > 0 && fbAccessToken.expirationDate != nil && [fbAccessToken.expirationDate compare:[NSDate date]] == NSOrderedDescending;
}

- (BOOL)isValidProfile:(FBSDKProfile *)fbProfile{
    return fbProfile && [fbProfile.userID length] > 0 && [fbProfile.name length] > 0 && fbProfile.linkURL != nil;
}


- (void)loginAccountWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure{
 
    [[HGFBUtils sharedInstance] requestPermissions:YES withPermissions:@[@"public_profile", @"email", @"user_friends"] completion:^(NSError *errorRequestPermission){
        
        if(errorRequestPermission){
            
            failure(errorRequestPermission);
            
        }else{
            
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *errorGraph) {
                 
                 if(errorGraph){
                     failure(errorGraph);
                 }else{
                     [[HGUserData sharedInstance] updateFacebookProfile:result];
                     
                     NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
                     
                     NSString* ipString = [[HGUtils sharedInstance] getIPAddress];
                     
                     [params setObject:ipString forKey:@"REMOTE_ADDR"];
                     
                     [params setObject:[HGUserData sharedInstance].facebookUserID forKey:@"fb_userid"];
                     [params setObject:[HGUserData sharedInstance].userDisplayName forKey:@"fb_username"];
                     [params setObject:[HGUserData sharedInstance].facebookAccessToken forKey:@"fb_token"];
                     [params setObject:[HGUserData sharedInstance].userEmail forKey:@"fb_email"];
                     [params setObject:[HGUserData sharedInstance].userAvatarLink forKey:@"fb_headimage"];
                     
                     long timeSince1970 = (long)[[HGUserData sharedInstance].facebookTokenExpirationDate timeIntervalSince1970];
                     NSNumber *facebookTokenExpires  =  [NSNumber numberWithLong: timeSince1970];
                     [params setObject:facebookTokenExpires forKey:@"fb_token_expires"];
                     
                     [params setObject:[[HGUtils sharedInstance] advertisingIdentifier] forKey:@"appsflyer_user_id"];
                     
                     [params setObject:[NSNumber numberWithDouble:[HGAppData sharedInstance].GPSlat] forKey:@"lat"];
                     [params setObject:[NSNumber numberWithDouble:[HGAppData sharedInstance].GPSlon] forKey:@"lon"];
                     
                     [params setObject:DEVICE_UUID forKey:@"device_id"];
                     [params setObject:[HGUserData sharedInstance].fmUserID forKey:@"anonymous_user_id"];
                     
                     
                     
                     NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
                   
                     [params setObject:[timeZoneLocal name] forKey:@"timezone"];
                     
                     
                     
                     [[HGAppData sharedInstance].guestApiClient FIVEMILES_POST:@"login_facebook/" parameters:params success:^(NSURLSessionDataTask* task, id responseObject) {
                         
                         [[HGUserData sharedInstance] updateUserLoggedInfo:responseObject withAnonymousSignup:NO withAccountType:HGAccountTypeFacebook];
#ifndef DEBUG
                         [[HGAppData sharedInstance].chatApiClient FIVEMILES_POST:@"register/" parameters:@{@"device_id":[HGAppData sharedInstance].deviceToken} success:nil failure:nil];
#endif
                         
                         success(responseObject);
                     } failure:^(NSURLSessionDataTask* task, NSError* errorLoginFB) {
                         
                         failure(errorLoginFB);
                     }];

                 }
                 
             }];
        }
        
    }];
}

- (void)verifyAccountWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure{
    
    [self requestPermissions:YES withPermissions:@[@"public_profile", @"email", @"user_friends"] completion:^(NSError *errorRequestPermission){
        
        if(errorRequestPermission){
            failure(errorRequestPermission);
        }else{
            [self linkFacebookAccountWithSuccess:success withFailure:failure];
        }
       
    }];
}

- (void)unverifyAccountWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure{
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unlink_facebook/" parameters:nil
    success:^(NSURLSessionDataTask *task, id responseObject) {
        [HGUserData sharedInstance].bUserFBVerified = NO;
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)shareShopWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure{
    
    [self requestPermissions:NO withPermissions:@[@"publish_actions"] completion:^(NSError *errorRequestPermission){
        
        if(errorRequestPermission){
            failure(errorRequestPermission);
        }else{
            NSString *link = [NSString stringWithFormat:USER_PAGE_URL_FORMAT, [HGUserData sharedInstance].fmUserID];
            NSString *name = [HGUserData sharedInstance].userDisplayName;
            NSString *caption = @"5milesapp.com";
            NSString *description = [NSString stringWithFormat:@"%@'s marketplace in 5miles.", [HGUserData sharedInstance].userDisplayName];
            NSString *message = NSLocalizedString(@"Check out my mobile marketplace on the #5milesapp!", nil);
            
            NSDictionary *params =  @{@"link":link,
                                      @"name": name,
                                      @"caption": caption,
                                      @"description": description,
                                      @"message": message};
            
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/feed" parameters:params HTTPMethod:@"POST"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *errorPostFeed) {
                
                if (errorPostFeed){
                    failure(errorPostFeed);
                }else{
                    success(result);
                }
            }];
        }
    }];
    
}

- (void)shareItem:(NSString *)itemID title:(NSString *)title price:(float)price withSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure{
    
    [self requestPermissions:NO withPermissions:@[@"publish_actions"] completion:^(NSError *errorRequestPermission){
    
        if(errorRequestPermission){
            failure(errorRequestPermission);
        }else{
            NSString *userID = [HGUserData sharedInstance].fmUserID;
            
            if(!userID.length || !itemID.length || !title.length){
                
                NSError *errorParams = [NSError errorWithDomain:ErrorDomainFacebook code:HGErrorFacebookShareItem userInfo:@{NSLocalizedDescriptionKey:@"Share to facebook unsuccessfully."}];
                failure(errorParams);
                
            }else{
                NSString *shareType = @"item";
                
                NSString *itemURL = [NSString stringWithFormat:ITEM_PAGE_URL_FORMAT, itemID];
                NSString *aliasStr = [NSString stringWithFormat:@"%@%@%@", userID, shareType, itemID];
                
                NSString *alias = [NSString stringWithFormat:@"5miles-v2-%@", [[[HGUtils sharedInstance] md5HexDigest:aliasStr] substringToIndex:6]];
                
                NSDictionary *dictData = @{
                                           @"userId"        : userID,
                                           @"shareType"     : shareType,
                                           @"eventId"       : itemID,
                                           @"userid"        : userID,
                                           @"itemid"        : itemID,
                                           @"user_id"       : userID,
                                           @"share_type"    : shareType,
                                           @"event_id"      : itemID,
                                           @"$desktop_url"  : itemURL,
                                           @"$og_url"       : itemURL
                                           };
                
                [[Branch getInstance] getShortURLWithParams:dictData andChannel:nil andFeature:nil andStage:nil andAlias:alias andCallback:^(NSString *url, NSError *errorBranch){
                    
                    if(errorBranch){
                        failure(errorBranch);
                    }else{
                        NSString *localPrice = [NSString stringWithFormat:@"%0.2f%@", price, [HGUserData sharedInstance].preferredCurrency];
                        NSString *message = [NSString stringWithFormat: NSLocalizedString(@"Check out my mobile marketplace on the #5milesapp - I'm selling a %1$@ for %2$@. More details here: %3$@",nil), title, localPrice, url];
                        
                        NSDictionary *params = @{
                                                 @"message"     : message,
                                                 @"link"        : url
                                                 };
                        
                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/feed" parameters:params HTTPMethod:@"POST"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *errorPostFeed) {
                            if (errorPostFeed){
                                failure(errorPostFeed);
                            }else{
                                success(result);
                            }
                        }];
                    }
                }];

            }
        }
       
    }];
}
- (void)shareWebLink:(NSString *)linkString  withSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure{
    
    [self requestPermissions:NO withPermissions:@[@"publish_actions"] completion:^(NSError *errorRequestPermission){
        
        if(errorRequestPermission){
            failure(errorRequestPermission);
        }else{
            NSDictionary *params = @{
                                    @"message"     : @"What 5miles say",
                                    @"link"        : linkString
                                    };
            
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/feed" parameters:params HTTPMethod:@"POST"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *errorPostFeed) {
                            if (errorPostFeed){
                                failure(errorPostFeed);
                            }else{
                                success(result);
                            }
        }];
       
        }
        
    }];
}

- (void)addObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationDidChangeAccessToken:) name:FBSDKAccessTokenDidChangeNotification object:[FBSDKAccessToken currentAccessToken]];
}

- (void)removeObserver{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)linkFacebookAccountWithSuccess:(FacebookSuccessBlock)success withFailure:(FacebookFailureBlock)failure{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"linkfacebookaccount" label:nil value:nil];
    
    long time = (long)[[HGUserData sharedInstance].facebookTokenExpirationDate timeIntervalSince1970];
    NSNumber* expirationDate = [[NSNumber alloc] initWithLong:time];
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"link_facebook/" parameters:@{
                                @"fb_token":[HGUserData sharedInstance].facebookAccessToken,
                                @"fb_userid":[HGUserData sharedInstance].facebookUserID,
                                @"fb_expire":expirationDate}
     success:^(NSURLSessionDataTask *task, id responseObject) {
         [HGUserData sharedInstance].bUserFBVerified = YES;
         success(responseObject);
     } failure:^(NSURLSessionDataTask *task, NSError *error) {
         failure(error);
     }];
}

@end

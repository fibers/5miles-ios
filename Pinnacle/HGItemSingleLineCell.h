//
//  HGItemSingleLineCell.h
//  Pinnacle
//
//  Created by Alex on 15/7/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGShopItem.h"
#import "StrikeThroughLabel.h"

@interface HGItemSingleLineCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView * imageView;
@property (nonatomic, strong) UIImageView * soldIconView;
@property (nonatomic, strong) UIImageView * locationIcon;

@property (nonatomic, strong) UILabel* itemTitle;

@property (nonatomic, strong) UILabel * lbDistance;
@property (nonatomic, strong) UILabel * lbPrice;
@property (nonatomic, strong) StrikeThroughLabel * originalPrice;


@property (nonatomic, strong) UIView* bottomShadow;


@property (nonatomic, strong) UIImageView* avatarView;
@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;
@property (nonatomic, strong) UIImageView* UserRatingView;


@property (nonatomic, strong) UIImageView* ItemNewIcon;
- (void)configWithItem:(HGShopItem *)item;
@end

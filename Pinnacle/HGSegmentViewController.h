//
//  HGSegmentViewController.h
//  LxJSDemo
//
//  Created by mumuhou on 15/7/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGSegmentedControl.h"


@protocol HGSegmentSubViewControllerProtocol <NSObject>

- (void)segmentedPageShown:(BOOL)shown;

@end

@interface HGSegmentViewController : UIViewController

@property (nonatomic, strong) UIScrollView *contentView;
@property (nonatomic, strong) HGSegmentedControl *segmentedControl;

@property (nonatomic, strong, readonly) UIViewController *currentController;

// step1
- (void)setPageTitles:(NSArray *)pageTitles;

// step2
@property (nonatomic, strong) NSArray *pageControllers;

- (void)setTitleNormalAttributes:(NSDictionary *)normalAttributes titleSelectedAttributes:(NSDictionary *)selectedAttributes;

- (void)segmentedSelectedPageShown:(NSInteger)index;

- (void)onSegmentedSelectedChanged:(HGSegmentedControl *)segmentedControl;

@end

//
//  HGChatMessageCell.h
//  Pinnacle
//
//  Created by shengyuhong on 15/1/23.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

#define PADDING_SYSTEM_RECOMMENDED 8
#define X_PADDING_BUBBLE 8
#define Y_PADDING_BUBBLE 6
//See the discussion of boundingRectWithSize
#define WIDTH_TEXT_SUPPLEMENT 2
#define HEIGHT_TEXT_SUPPLEMENT 2

#define WIDTH_BUBBLE_TAIL 6
#define Y_OFFSET_BUBBLE_RELATE_AVATAR 4
#define Y_MARGIN_BUBBLE 18

#define FONT_SIZE_TIME 12.0f
#define X_PADDING_TIME 10
#define Y_PADDING_TIME 6
#define HEIGHT_TIME 20

#define FONT_SIZE_INFO 12.0f
#define TOP_MARGIN_INFO 14
#define BOTTOM_MARGIN_INFO 20
#define Y_PADDING_INFO 16
#define WIDTH_INFO 264


#define FONT_SIZE_BUBBLE    15.0f


@class HGOffer, HGOfferLineMeta, HGUser;

@protocol HGChatMessageCellDelegate <NSObject>

- (void)onTouchChatAvatar:(HGOffer *)offer;

@end

@interface HGChatMessageCell : UITableViewCell

@property (nonatomic, strong) TTTAttributedLabel *lbMessage;
@property (nonatomic, strong) HGOffer *offer;
@property (nonatomic, assign) id<HGChatMessageCellDelegate> delegate;

- (void)configWithOffer:(HGOffer *)offer withOfferLineMeta:(HGOfferLineMeta *)offerlineMeta;

@end

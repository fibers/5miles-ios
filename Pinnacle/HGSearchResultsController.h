//
//  HGSearchResultsController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGSearchResultFilter.h"
#import "HGUITextField.h"
typedef NS_ENUM(NSUInteger, HGSearchMode) {
    HGSearchModeKeyword,
    HGSearchModeCategory,
    HGSearchModeBrandID,
};

@interface HGSearchResultsController : UICollectionViewController<UITextFieldDelegate, HGSearchFilterDelegate, UIScrollViewDelegate>


@property (nonatomic, strong)NSString* userNewSearchString;

@property (nonatomic, strong)NSMutableDictionary* httpQuestDict;

@property (nonatomic, strong) HGSearchResultFilter* filter;
@property (nonatomic, assign) HGSearchMode searchMode;
@property (nonatomic, strong) NSDictionary * searchPayload;

@property (nonatomic, strong) HGUITextField* searchTitleView;
@property (nonatomic, strong) NSString* bannerCid;

@property (nonatomic, strong) NSString* rf_tag_from_push_payload;

@end

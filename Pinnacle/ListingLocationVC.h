//
//  ListingLocationVC.h
//  Pinnacle
//
//  Created by Alex on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"
@interface ListingLocationVC : UIViewController<UITextFieldDelegate>



@property (nonatomic, strong)NSString* sellItem_CountryCode;
@property (nonatomic, strong)NSString* sellItem_CityCode;
@property (nonatomic, strong)NSString* sellItem_RegionCode;
@property (nonatomic, strong)NSString* sellItem_lat;
@property (nonatomic, strong)NSString* sellItem_lon;
@end

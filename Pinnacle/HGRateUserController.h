//
//  HGRateUserController.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-1.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SZTextView/SZTextView.h>

@interface HGRateUserController : UIViewController<UITextViewDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) NSString* reivewID;
@property (nonatomic, strong) NSString * itemID;
@property (nonatomic, assign) BOOL success;

@property (nonatomic, strong) NSString* userID;
@property (nonatomic, strong) NSString* userName;



@property (nonatomic, assign) int direction;  // 0: buyer rate seller
                                              // 1: seller rate buyer.


//@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) UIView* backgroundBoard1;
@property (nonatomic, strong) UIView* backgroundBoard2;

@end

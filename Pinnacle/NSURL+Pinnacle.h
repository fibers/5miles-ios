//
//  NSURL+Pinnacle.h
//  Pinnacle
//
//  Created by shengyuhong on 15/6/11.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Pinnacle)

- (NSDictionary *)queryAsDictionary;

@end

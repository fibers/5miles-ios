//
//  HGFeedbackController.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-15.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGFeedbackController.h"
#import "HGAppData.h"

#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <SVProgressHUD.h>

@interface HGFeedbackController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonSend;


@end

@implementation HGFeedbackController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"setting_view" action:@"leavefeedback" label:nil value:nil];
    
    self.barButtonSend.enabled = NO;
    self.fakePlaceHolder = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 300, 25)];
    self.fakePlaceHolder.textColor = FANCY_COLOR(@"b9b9b9");
    self.fakePlaceHolder.numberOfLines = 1;
    self.fakePlaceHolder.font = [UIFont systemFontOfSize:12];
    self.fakePlaceHolder.text = NSLocalizedString(@"How can we make 5miles better for you?",nil);
    [self.textView addSubview:self.fakePlaceHolder];
    
    
    
    [self customizeLeftButton];
}
-(void)ClickNaviBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)customizeLeftButton
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame=CGRectMake(0, 0, 41/2, 34/2);
    [button setBackgroundImage:[UIImage imageNamed:@"naviBack_icon2"] forState:UIControlStateNormal];
    [button setTitle:@" " forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:14];
    
    button.backgroundColor=[UIColor clearColor];
    [button addTarget:self action:@selector(ClickNaviBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem=leftButton;
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.textView becomeFirstResponder];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.textView endEditing:YES];
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextView Delegate

- (void)textViewDidChange:(UITextView *)textView
{
    self.barButtonSend.enabled = textView.text.length > 0;
    if (self.barButtonSend.enabled) {
        self.fakePlaceHolder.hidden = YES;
    }
    else{
        self.fakePlaceHolder.hidden = NO;
    }
}

- (IBAction)onTouchCancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onTouchSendButton:(id)sender {
    [SVProgressHUD show];
    
    NSString * versionBuildString = [[HGAppData sharedInstance] versionBuild];
    NSString *deviceType = [UIDevice currentDevice].model;
    NSString * osString= @"IOS";
    NSString* osVersion = [UIDevice currentDevice].systemVersion;
    
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"feedback/" parameters:@{@"text": self.textView.text,
                                                                             @"app_version":versionBuildString,
                                                                             @"device":
                                                                                 deviceType,
                                                                             @"os":osString,
                                                                             @"os_version":osVersion
                                                                             
                                                                             } success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        PXAlertView* view = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Thank you", nil) message:NSLocalizedString(@"We have received your feedback. Thanks", nil) cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];[view useDefaultIOS7Style];
       
                                                                                 
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"post feedback failed: %@", error);
    }];
}


@end

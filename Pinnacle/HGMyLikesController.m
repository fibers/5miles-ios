//
//  HGMyLikesController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGMyLikesController.h"
#import "HGAppData.h"
#import "HGItemDetailController.h"
#import "HGUtils.h"
#import "HGPurchaseItemCell.h"
#import <SVProgressHUD.h>
#import "UIStoryboard+Pinnacle.h"

@interface HGMyLikesController ()

@property (nonatomic, strong) NSMutableArray * items;
@property (nonatomic, strong) NSString * nextLink;
@property (nonatomic, strong) UIView* noMoreHintView;
@property (nonatomic, assign) BOOL bHasUserClickActionBeforeReturn;
@end

@implementation HGMyLikesController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)customizeBackButton
{
    
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
}

-(void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_USER_ITEMS_UNLIKE object:nil];
}
- (void)viewDidLoad
{
    
    [[HGUtils sharedInstance] gaTrackViewName:@"mylikes_view"];
    self.bHasUserClickActionBeforeReturn = NO;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self customizeBackButton];
    self.items = [@[] mutableCopy];
    self.nextLink = @"";
    [self addNoMoreHintView];
    
    self.navigationItem.title = NSLocalizedString(@"My Likes", nil);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMyItemDetailViewUnlike:) name:NOTIFICATION_USER_ITEMS_UNLIKE object:nil];
   
    [self getMylikeItems];
}
-(void)handleMyItemDetailViewUnlike:(NSNotification*)notification
{
    NSDictionary* dict =  [notification object];
    NSString* itemid = [dict objectForKey:@"item_id"];
    
    for (int i = 0; i<self.items.count; i++) {
        HGShopItem* item = [self.items objectAtIndex:i];
        if ([item.uid isEqualToString:itemid]) {
            [self.items removeObjectAtIndex:i];
            [self.tableView reloadData];
        }
    }
    
}

-(void)addDefaultEmptyView
{
    int iconYoffset = 144;
    int lineOffset = 10;
    self.view.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];

    CGRect rx = [ UIScreen mainScreen ].bounds;
    int iphone4_Y_offset_config_Value = 0;
    if (rx.size.height <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    CGPoint iconCenter  = CGPointMake(rx.size.width/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [self.view addSubview:icon];
    
    // +10 for empty description which occupy two lines.
    CGPoint centerPoint = CGPointMake(rx.size.width/2, CGRectGetMaxY(icon.frame)+ lineOffset + 40/2);
    
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 40)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"Go treasure hunting in 5miles and heart cool listings!", nil);
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [self.view addSubview:textLabel];
    
    CGPoint buttonCenter = CGPointMake(rx.size.width/2, CGRectGetMaxY(textLabel.frame)+lineOffset+ 36/2);
    
    UIButton* button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 200, 36)];
    button.layer.cornerRadius = 4.0;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = DEFAULT_EMPTY_BOARDER_COLOR;
    UILabel* buttonTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    buttonTitle.textAlignment = NSTextAlignmentCenter;
    buttonTitle.text = NSLocalizedString(@"Go exploring!", nil);
    buttonTitle.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [button addSubview:buttonTitle];
    button.center = buttonCenter;
    [self.view addSubview:button];
    [button addTarget:self action:@selector(defaultEmptyAction) forControlEvents:UIControlEventTouchUpInside];
}
-(void)defaultEmptyAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"mylikes_view" action:@"likes_empty" label:nil value:nil];

    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    {
        [self.navigationController popViewControllerAnimated:YES];
        [UIView animateWithDuration:1.0 animations:^(){
            self.view.alpha = 0;
        } completion:^(BOOL bResult){
            [appDelegate.tabController button1pressed];
        }];
 
    }
    
}

-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    [self.tableView addSubview:self.noMoreHintView];
    [self.noMoreHintView setHidden:YES];
}

-(void)getMylikeItems
{
    __weak HGMyLikesController * weakSelf = self;
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"user_likes/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        
        
        [self.items removeAllObjects];
        for (id dictObj in results) {
            NSError * error = nil;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                [self.items addObject:item];
            } else {
                NSLog(@"shop item creation error: %@", error);
            }
        }
        
        if (self.items.count == 0) {
            [self addDefaultEmptyView];
        }
        else{
            [self.tableView reloadData];
            
        }
        
        if (![self.nextLink isEqual:[NSNull null]]) {
            [self.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf _loadNextPage];
            }];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"get me(%@) likes failed:%@", [HGUserData sharedInstance].fmUserID, error.userInfo);
    }];
    

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.tabController.tabBar.hidden =YES;
     self.tabBarController.tabBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    if (!self.bHasUserClickActionBeforeReturn) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"mylikes_view" action:@"mylikes_back" label:nil value:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Datasource & Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGPurchaseItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PurchaseItemCell" forIndexPath:indexPath];
    
    // Configure the cell...
    HGShopItem * item = [self.items objectAtIndex:indexPath.row];
    [cell configWithEntity:item];
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"mylikes_view" action:@"mylikes_product" label:nil value:nil];
    self.bHasUserClickActionBeforeReturn = YES;
    
    //crash defense. #501.
    if (self.items == nil || self.items.count == 0) {
        return;
    }
    
    HGShopItem * item = [self.items objectAtIndex:indexPath.row];
    
    HGItemDetailController *vcItemDetail = (HGItemDetailController *)[[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:@"VCItemDetail"];
    vcItemDetail.item = item;
    vcItemDetail.trackstring = item.rf_tag;
    [self.navigationController pushViewController:vcItemDetail animated:YES];
}


#pragma mark - Helpers

- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.tableView.infiniteScrollingView stopAnimating];
        self.tableView.showsInfiniteScrolling = NO;
        [self.noMoreHintView setFrame:CGRectMake(0, self.tableView.contentSize.height, self.tableView.frame.size.width, 40)];
        
        if(self.items.count != 0)
        {
            [self.noMoreHintView setHidden:NO];
        }
        
    } else {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"mylikes_view" action:@"mylikes_loadmore" label:nil value:nil];
        [SVProgressHUD show];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[NSString stringWithFormat:@"user_likes/%@", self.nextLink] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.items addObject:item];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            [self.tableView reloadData];
            [self.tableView.infiniteScrollingView stopAnimating];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [SVProgressHUD dismiss];
            [self.tableView.infiniteScrollingView stopAnimating];
            NSLog(@"get home items failed: %@", error.localizedDescription);
        }];
        
    }
}

@end

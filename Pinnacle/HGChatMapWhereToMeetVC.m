//
//  HGChatMapWhereToMeetVC.m
//  Pinnacle
//
//  Created by Alex on 15/5/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatMapWhereToMeetVC.h"
#import "HGConstant.h"
#import "HGAppData.h"
#import "HGZipCodeVC.h"
#import "UIStoryboard+Pinnacle.h"
@interface HGChatMapWhereToMeetVC ()<MKMapViewDelegate>

@property (nonatomic, strong) UIView* topBoard;
@property (nonatomic, strong) UILabel* POITitle;
@property (nonatomic, strong) UILabel* POIDistance;
@property (nonatomic, strong) UILabel* POIAddress;
@property (nonatomic, strong) UIImageView* POIDirectionIcon;

@property (nonatomic, assign) int zoomLevel;
@end

@implementation HGChatMapWhereToMeetVC


- (int)getZoomLevel:(MKMapView*)inputMapview {
    
    return 21-round(log2(inputMapview.region.span.longitudeDelta * MERCATOR_RADIUS * M_PI / (180.0 * inputMapview.bounds.size.width)));
    
}
-(float)getDistanceString:(CLLocationCoordinate2D)coor2d
{
    float distance = 0;
    float preferlat= [[HGUtils sharedInstance] getPreferLat];
    float preferlon =[[HGUtils sharedInstance] getPreferLon];
    
    
    CLLocation * itemLoc = [[CLLocation alloc] initWithLatitude:coor2d.latitude longitude:coor2d.longitude];
    if (fabs(coor2d.latitude - 0.0) < 0.000001
        && fabs(coor2d.longitude - 0.0) < 0.000001) {
        distance = -1.0;;
    }
    else if(fabs(preferlat - 0.0)< 0.000001
            && fabs(preferlon - 0.0) < 0.000001)
    {
        distance = - 1.0;
    }
    else{
        CLLocation* perferlocation = [[CLLocation alloc] initWithLatitude:preferlat longitude:preferlon];
        distance = [itemLoc distanceFromLocation:perferlocation] * 0.001;
    }
    
    return distance;
    
}

#define  TOP_BOARD_HEIGHT 68
static int extend_height = 0;
-(float)GetAddressLineNumber;
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12.0], NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    
    
    NSString* description = self.POIAddressString;
    CGRect rect = [description boundingRectWithSize:CGSizeMake(239,CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attributes context:nil];
    CGRect rectSingleline = [@"this is a line" boundingRectWithSize:CGSizeMake( 239 ,CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attributes context:nil];
    
    float lineCount = 0;
    if (rect.size.height == rectSingleline.size.height ) {
        lineCount = 1;
    }
    else
    {
        lineCount = 2;
    }
    return lineCount;

}
-(void)addTopBoard
{
    float lineCount = [self GetAddressLineNumber];
    if (lineCount == 2) {
        extend_height = 15;
    }
    
    
    self.topBoard = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, TOP_BOARD_HEIGHT + extend_height)];
    self.topBoard.backgroundColor = FANCY_COLOR(@"ffffff80");
    [self.view addSubview:self.topBoard];
    
    self.POITitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, self.view.frame.size.width - 81, 20)];
    [self.topBoard addSubview:self.POITitle];
    self.POITitle.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.POITitle.font  = [UIFont boldSystemFontOfSize:14];
    
    self.POIDistance = [[UILabel alloc] initWithFrame:CGRectMake(10, 10+20+2, self.view.frame.size.width - 81, 15)];
    [self.topBoard addSubview:self.POIDistance];
    self.POIDistance.font = [UIFont systemFontOfSize:12];
    self.POIDistance.textColor = FANCY_COLOR(@"818181");
    
    self.POIAddress = [[UILabel alloc] initWithFrame:CGRectMake(10, 10+20+2+15+2, self.view.frame.size.width - 81, 15+extend_height)];
    [self.topBoard addSubview:self.POIAddress];
    self.POIAddress.font = [UIFont systemFontOfSize:12];
    self.POIAddress.numberOfLines = 0;
    self.POIAddress.textColor = FANCY_COLOR(@"818181");
    
    
    self.POITitle.text = self.POITitleString;
    
    float distance = [self getDistanceString:self.POICoor];
    NSString* distanceString = [[HGUtils sharedInstance] getDistanceMiles:distance];
    self.POIDistance.text = distanceString;
    self.POIAddress.text = self.POIAddressString;
    
    
    self.POIDirectionIcon = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-51, 10+extend_height/2, 32, 32)];
    [self.topBoard addSubview:self.POIDirectionIcon];
    self.POIDirectionIcon.image = [UIImage imageNamed:@"chatMapWhereToMeetIcon"];
    self.POIDirectionIcon.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchDirectionIcon)];
    [self.POIDirectionIcon addGestureRecognizer:singleTap];
    
    
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    UIView* seperatorLine = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 71, 10, fSeperatorHeight, 46+extend_height)];
    [self.topBoard addSubview:seperatorLine];
    seperatorLine.backgroundColor = FANCY_COLOR(@"cccccc");
    
    UILabel* directionHint = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 61, 10+32+2+extend_height/2, 60, 20)];
    [self.topBoard addSubview:directionHint];
    directionHint.font = [UIFont systemFontOfSize:12];
    directionHint.textColor = FANCY_COLOR(@"ff8830");
    directionHint.text = NSLocalizedString(@"Directions", nil);
    
}
- (void)viewDidLoad {
    
    [[HGUtils sharedInstance] gaTrackViewName:@"wheretomeet_view"];
    [super viewDidLoad];
    [self addTopBoard];
    self.mapView.delegate = self;
    self.navigationItem.title = NSLocalizedString(@"Where to meet", nil);
    [self showMyLocation];
    [self addAnnotation];
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    ////do not have GPS access right, remind user using Zipcode
    {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        // If the status is denied or only granted for when in use, display an alert
        if (status == kCLAuthorizationStatusDenied && [HGUserData sharedInstance].zipcodeString.length == 0)
        {
            HGZipCodeVC* vc= [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"ZipCode"];
            vc.currentViewType = zipViewTypeUserRegister;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
}
-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}



-(void)startNavigation:(CLLocationCoordinate2D)endCoor
{
    MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:endCoor addressDictionary:nil]];
    toLocation.name = @"to name";
    
    [MKMapItem openMapsWithItems:@[currentLocation, toLocation]
                   launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving,MKLaunchOptionsShowsTrafficKey: [NSNumber numberWithBool:YES]}];
    
}
-(void)copyAddress
{
    [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Address has been Copied", nil) onView:self.view];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.POIAddress.text;
}
-(void)touchDirectionIcon
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"wheretomeet_view" action:@"directions" label:nil value:nil];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Use Map to Directions", nil),NSLocalizedString(@"Copy the Address", nil), nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"wheretomeet_view" action:@"usemaptodirect" label:nil value:nil];
            [self startNavigation:self.POICoor];
            break;
        }
        case 1:
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"wheretomeet_view" action:@"copyaddress" label:nil value:nil];
            [self copyAddress];
            break;
        }
        default:
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"wheretomeet_view" action:@"directioncancel" label:nil value:nil];
            break;
        }
    }
}


#define ITEM_LOCATION_TITLE @"itemLocation"
#define USER_LOCATION_TITLE @"userLocation"
-(void)addAnnotation
{
    MKPointAnnotation* itemLocation = [[MKPointAnnotation alloc] init];
    itemLocation.coordinate = self.POICoor;
    itemLocation.title = ITEM_LOCATION_TITLE;
    
    [self.mapView addAnnotation:itemLocation];
    
    
    MKPointAnnotation* userLocation = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D userCood =(CLLocationCoordinate2D){[[HGUtils sharedInstance] getPreferLat],[[HGUtils sharedInstance] getPreferLon]} ;
    userLocation.coordinate = userCood;
    userLocation.title = USER_LOCATION_TITLE;
    [self.mapView addAnnotation:userLocation];
}
-(void)showMyLocation
{
    
    self.mapView.showsUserLocation = NO;
    double templat = [[HGUtils sharedInstance] getPreferLat];
    double templon = [[HGUtils sharedInstance] getPreferLon];
    // 1
    CLLocationCoordinate2D zoomCenter;
    zoomCenter.latitude = templat;
    zoomCenter.longitude= templon;
    
    
    CLLocationDistance distanceMeters= 0;
    if (fabs(templat-0)< 0.000001 && fabs(templon - 0)< 0.000001) {
        distanceMeters = 50000;
        if (distanceMeters < MIN_RANGE ) {
            distanceMeters = MIN_RANGE;
        }
    }
    else{
        zoomCenter.latitude = (templat + self.POICoor.latitude)/2;
        zoomCenter.longitude = (templon + self.POICoor.longitude)/2;
        CLLocation *orig=[[CLLocation alloc] initWithLatitude:self.POICoor.latitude longitude:self.POICoor.longitude];
        CLLocation* dist = [[CLLocation alloc] initWithLatitude:templat longitude:templon];
        
        distanceMeters =[orig distanceFromLocation:dist];
        if (distanceMeters < MIN_RANGE ) {
            distanceMeters = MIN_RANGE;
        }
    }

    
    
    
    MKCoordinateRegion viewRegion;
    if (distanceMeters == 0) {
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, 1.0*METERS_PER_MILE, 1.0*METERS_PER_MILE);
    }
    else{
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, distanceMeters*2, distanceMeters*2);
    }
    
    if (viewRegion.span.latitudeDelta > 180 || viewRegion.span.longitudeDelta > 180) {
        if (viewRegion.span.latitudeDelta > 180) {
            viewRegion.span.latitudeDelta = 180;
        }
        if (viewRegion.span.longitudeDelta > 180) {
            viewRegion.span.longitudeDelta = 180;
        }
    }
    if(viewRegion.span.latitudeDelta < 0 || viewRegion.span.longitudeDelta < 0)
    {
        viewRegion.span.longitudeDelta = 180;
        viewRegion.span.latitudeDelta = 180;
    }
    if (viewRegion.span.latitudeDelta > 120 || viewRegion.span.longitudeDelta > 120) {
        viewRegion.center.latitude = templat;
        viewRegion.center.longitude = templon;
    }
    // 3
    [self.mapView setRegion:viewRegion animated:YES];
    
    self.zoomLevel = [self getZoomLevel:self.mapView];
    //[self addAnnotation];
}

- (void)mapView:(MKMapView *)inputMapView regionDidChangeAnimated:(BOOL)animated {
    int newZoomLevel = [self getZoomLevel:inputMapView];
    if (newZoomLevel > self.zoomLevel) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"wheretomeet_view" action:@"mapzoomin" label:nil value:nil];
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"wheretomeet_view" action:@"mapzoomout" label:nil value:nil];
    }
    
    self.zoomLevel = newZoomLevel;
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"Annotation";
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        
        MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView == nil)
        {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            MKPointAnnotation* mk = (MKPointAnnotation*)annotation;
            if ([mk.title isEqualToString:ITEM_LOCATION_TITLE] ) {
                annotationView.image = [UIImage imageNamed:@"chatMapPOIPoint"];
            }
            if ([mk.title isEqualToString:USER_LOCATION_TITLE]) {
                annotationView.image = [UIImage imageNamed:@"map_user_location_big"];
            }
        }
        else
        {
            annotationView.annotation = annotation;
            //[self changeAnnotationFrame:annotationView];
        }
        
        return annotationView;
    }
    else if([annotation isKindOfClass:[MKUserLocation class]]){
        MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:@"userlocation"];
        if(annotation==mapView.userLocation)
        {
            if (annotationView == nil) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"userlocation"];
                //annotationView.canShowCallout = YES;
            }
            annotationView.image = [UIImage imageNamed:@"map_user_location_big"];
            return annotationView;
        }
    }
    
    return nil;
}
@end

//
//  HGChatPhotoMediaItem.h
//  Pinnacle
//
//  Created by shengyuhong on 15/5/25.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "JSQPhotoMediaItem.h"

@interface HGChatPhotoMediaItem : JSQPhotoMediaItem

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) NSURL *imageURL;

- (void)addLoadingMask;
- (void)addFailedIcon;
- (void)removeLoadingMask;
- (void)updateProgress:(CGFloat)progress;

@end

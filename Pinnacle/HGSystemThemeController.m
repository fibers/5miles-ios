//
//  HGSystemThemeController.m
//  Pinnacle
//
//  Created by Alex on 14-12-16.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSystemThemeController.h"
#import "HGConstant.h"
#import "HGAppData.h"
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import "HGAppDelegate.h"
#import "HGSystemThemeItemCell.h"
#import "HGItemDetailController.h"

@interface HGSystemThemeController ()
@property (nonatomic, strong) NSMutableArray* items;


@property (nonatomic, strong)UIImageView* emptyIcon;
@property (nonatomic, strong)UILabel* emptyLabel;
@end


//test items:
// 2LYq1WJNxdPO6edo,  1236ZlJGL2rp5MRG
@implementation HGSystemThemeController

-(void)customizeBackButton
{
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    self.navigationItem.backBarButtonItem = backbutton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    dataContentGet = 0;
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage-title"]];
    [[HGUtils sharedInstance] gaTrackViewName:@"producttopic_view"];
    
    [self addBackButton];
    [self customizeBackButton];
    self.items = [[NSMutableArray alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
   
    [self getThemeDetailInformation];
}

static int dataContentGet = 0;

-(void)getThemeDetailInformation
{
    int themeID = [HGAppData sharedInstance].recommend_themeID;
    NSNumber* idNumber = [[NSNumber alloc] initWithInt:themeID];
    
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"featured_items/" parameters:@{@"featured_items_id": idNumber} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError * error;
        dataContentGet = 1;
        
        @try{
            NSArray* itemsResult = [responseObject objectForKey:@"items"];
            for (id dictObj in itemsResult) {
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.items addObject:item];
                } else {
                    NSLog(@"shop item %@ creation error: %@", [dictObj objectForKey:@"id"], error);
                }
            }
            
            NSString* banner_url = [responseObject objectForKey:@"banner_url"];
            self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage-title"]];
            
            UIColor* color = [[HGAppData sharedInstance].AppInitDataSingle.default_placehold_colors objectAtIndex:rand()%24];
            UIImage*fancyImage = [UIImage imageNamed:@"icon-empty"];
            self.head_theme_image.backgroundColor = color;
            [self.head_theme_image sd_setImageWithURL:[NSURL URLWithString:banner_url] placeholderImage:fancyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error) {
                    NSLog(@"load image error: %@", error);
                }
            }];

        }
        @catch(NSException *exception) {
             [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];

            [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Sorry, 5miles is experiencing a technical issue. Please try again later.", nil)  withTitle:NSLocalizedString(@"Oops!", nil) completion:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        
        
        [self.tableView reloadData];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get item detail failed: %@", error);
    }];
}

-(void)ClickNaviBack
{
    [self dismissViewControllerAnimated:YES completion:^(){
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 100)];
    tableHeaderView.backgroundColor = [UIColor yellowColor];
    return tableHeaderView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (self.items.count > 0) {
        return self.items.count + 1;
    }
    else{
        return 1+1;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.items.count == 0 && indexPath.row == 1) {
        return 200;
    }
    if (indexPath.row == 0) {
        return 103+13;
    }
    return 270+66+13;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"default_first_cell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"default_first_cell"];
            self.head_theme_image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 103)];
            cell.backgroundColor = [UIColor whiteColor];
            [cell addSubview:self.head_theme_image];
        }
        
        return cell;

    }
    else{
        
        if (self.items.count > 0) {
            NSString* cell_keystring = @"HGSystemThemeItemCell";
            
            HGSystemThemeItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_keystring];
            if (cell == nil) {
                cell = [[HGSystemThemeItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HGSystemThemeItemCell"];
            }
            
            HGShopItem * itemShop = [self.items objectAtIndex:indexPath.row -1];
            [cell configCell:itemShop];
            return cell;
        }
        else{
            if (indexPath.row == 1)
            {
                NSString* cell_keystring = @"defaultCell";
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_keystring];
                
                if (cell == nil) {
                    cell = [[UITableViewCell alloc]
                            initWithStyle:UITableViewCellStyleDefault
                            reuseIdentifier:cell_keystring];
                    self.emptyIcon = [[UIImageView alloc] initWithFrame:CGRectMake(136, 84, 94, 68)];
                    self.emptyIcon.image = [UIImage imageNamed:@"system-theme-empty-icon"];
                    self.emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(116, 84+47+7, 220, 20)];
                    self.emptyLabel.textColor = FANCY_COLOR(@"818181");
                    self.emptyLabel.textAlignment = NSTextAlignmentCenter;
                    self.emptyLabel.font = [UIFont systemFontOfSize:14];
                    
                    CGPoint centerPoint1 =  cell.center;
                    centerPoint1.y = self.emptyIcon.frame.origin.y;
                    self.emptyIcon.center = centerPoint1;
                    
                    CGPoint centerPoint2 =  cell.center;
                    centerPoint2.y = self.emptyLabel.frame.origin.y;
                    self.emptyLabel.center = centerPoint2;
                    

                    [cell addSubview:self.emptyIcon];
                    [cell addSubview:self.emptyLabel];
                }
                
                if (dataContentGet == 1) {
                    self.emptyLabel.text = NSLocalizedString(@"These listings have all been sold.",nil);
                    self.emptyIcon.hidden = NO;
                }else if(dataContentGet == 0)
                {
                    self.emptyIcon.hidden = YES;
                    self.emptyLabel.text = NSLocalizedString(@"Loading...",nil);
                }
                
                
                return cell;
            }
        }
        
    }
    
    return nil;
  
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.items.count == 0) {
        return;
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"producttopic_view" action:@"recommendproduct" label:nil value:nil];
        if (indexPath.row == 0) {
            return;
        }
        else{
            HGShopItem * item = [self.items objectAtIndex:indexPath.row -1];
            [self performSegueWithIdentifier:@"ShowItemPage" sender:item];
        }
    }
   

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowItemPage"]) {
        HGItemDetailController * vcDest = (HGItemDetailController *)segue.destinationViewController;
        vcDest.item = sender;
    }
   }


-(void)addBackButton
{
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame=CGRectMake(0, 0, 41/2, 34/2);
    [button setBackgroundImage:[UIImage imageNamed:@"naviBack"] forState:UIControlStateNormal];
    [button setTitle:@" " forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont systemFontOfSize:14];

    button.backgroundColor=[UIColor clearColor];
    [button addTarget:self action:@selector(ClickNaviBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem=leftButton;
    
   
}
@end

//
//  HGTabBarViewController.h
//  Pinnacle
//
//  Created by Alex on 14-10-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PulsingHaloLayer.h"





//////iphone 6: 102.
////old iphone5: 96
// using suggestedTabbarHeight instead;
// #define TABBAR_HEIGHT 51



#define TABBAR_LAB_Y_OFFSET 15
#define Label_height 12

@interface HGTabBarViewController : UITabBarController

@property (nonatomic, strong) PulsingHaloLayer *halo;

@property (nonatomic, assign)int suggestedTabbarHeight;

@property (nonatomic, strong) UIView* fmTabbarView;
@property (nonatomic, strong) UIButton* button1;
@property (nonatomic, strong) UIButton* button2;
@property (nonatomic, strong) UIButton* button3;
@property (nonatomic, strong) UIButton* button4;
@property (nonatomic, strong) UIButton* button5;

@property (nonatomic, strong) UILabel* label1;
@property (nonatomic, strong) UILabel* label2;
@property (nonatomic, strong) UILabel* label3;
@property (nonatomic, strong) UILabel* label4;
@property (nonatomic, strong) UILabel* label5;

@property (nonatomic, strong) UIImageView* unreadmessage_mark;
@property (nonatomic, strong) UILabel* unreadMessageCount;


////using for sell view return to restore the origin selected tab button
@property (nonatomic, assign) int lastSelectedButtonNumber;

- (UINavigationController *)setTab1SelectedStyle;
- (UINavigationController *)setTab2SelectedStyle;
- (UINavigationController *)setTab3SelectedStyle;
- (UINavigationController *)setTab4SelectedStyle;
- (UINavigationController *)setTab5SelectedStyle;

- (void)button1pressed;
- (void)button2pressed;
- (void)button3pressed;
- (void)button4pressed;
- (void)button5pressed;


- (void)showMessageInstruction;
-(void)setLongUnReadBadget;
-(void)setShortUnReadBadget;
@end

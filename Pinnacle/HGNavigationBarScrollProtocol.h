//
//  HGNavigationBarScrollProtocol.h
//  LxSegmented
//
//  Created by mumuhou on 15/7/24.
//  Copyright (c) 2015年 mumuhou. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HGNavigationBarScrollProtocol <NSObject>

@required

- (void)viewController:(UIViewController *)viewController navigationBarTransformProgress:(CGFloat)progress;

- (CGFloat)headerViewOffset;

@end
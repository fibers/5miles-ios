//
//  HGAddPhotoCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-16.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGAddPhotoCell.h"

@implementation HGAddPhotoCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    CGFloat k90DegreesClockwiseAngle = (CGFloat) (90 * M_PI / 180.0);    
    self.transform = CGAffineTransformRotate(CGAffineTransformIdentity, k90DegreesClockwiseAngle);

    self.iconView.frame = CGRectMake(0, 0, 58/2, 43/2);
    self.iconView.center = self.center;
    
    self.iconView.image = [UIImage imageNamed:@"camera"];
    self.myview.layer.cornerRadius = 2.5;
    self.myview.clipsToBounds = YES;
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  HGPurchaseItemCell.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
@class HGShopItem;

typedef NS_ENUM(NSInteger, HGItemCellType) {
    HGItemCellType_Default = 0,
    HGItemCellType_MyItem = 1

};


@interface HGPurchaseItemCell : MGSwipeTableCell

@property (weak, nonatomic) IBOutlet UIImageView *coverImgView;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbState;

@property (nonatomic, strong) UIView* SeperatorView;


@property (nonatomic, strong) UIButton* statusButton;

// 1, for my selling and purchase list ..
@property  (nonatomic, assign) HGItemCellType CellType;

- (void)configWithEntity:(HGShopItem *)item;

@end

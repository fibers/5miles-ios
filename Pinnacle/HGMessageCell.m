//
//  HGMessageCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-1.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGMessageCell.h"
#import "HGShopItem.h"
#import "HGMessage.h"
#import "HGUser.h"
#import "HGUtils.h"
#import "UIImage+pureColorImage.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGAppData.h"

@implementation HGMessageCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self _setup];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    [self _setup];
}

- (void)_setup
{
    [self.avatarView setVerifySize:CGSizeMake(12, 12)];
    self.avatarView.coverImageView.image = [UIImage imageNamed:@"avatar_circle_cover"];
    self.avatarView.coverImageView.highlightedImage = [UIImage imageNamed:@"avatar_circle_cover_hl"];
    
    self.avatarView.verifyImageView.image = [UIImage imageNamed:@"verify_user_not_verify"];
    
    self.itemView.coverImageView.image = [UIImage imageNamed:@"item_cover"];
    self.itemView.coverImageView.highlightedImage = [UIImage imageNamed:@"item_cover_hl"];
    self.itemView.backgroundColor = FANCY_COLOR(@"eaeaea");
    
//    self.avatarView.clipsToBounds = YES;
//    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
//    self.avatarView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.avatarView.layer.borderWidth = 0.5;
//    self.avatarView.layer.cornerRadius = CGRectGetWidth(self.avatarView.bounds) * 0.5;
//    
//    self.avatarCover = [[UIImageView alloc] initWithFrame:self.avatarView.frame];
//    self.avatarCover.image = [UIImage imageNamed:@"avatar-cover"];
//    self.avatarCover.center = self.avatarView.center;
//    [self addSubview:self.avatarCover];
    
    
//    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.avatarView.frame.size.width/3, self.avatarView.frame.size.width/3)];
//    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
//    CGPoint centerPoint = self.avatarView.center;
//    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
//    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
//    self.userAvatar_verifyMark.center = centerPoint;
//    [self addSubview:self.userAvatar_verifyMark];
    
//    self.itemView.layer.borderWidth = 0;
//    self.itemView.layer.cornerRadius = 3;
//    self.itemView.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
//    self.itemView.contentMode = UIViewContentModeScaleAspectFill;
    
//    NSLayoutConstraint* itemViewRight = [NSLayoutConstraint constraintWithItem:self.itemView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:-12.0];
//    [self addConstraint:itemViewRight];
//    NSLayoutConstraint* lbTimeRight = [NSLayoutConstraint constraintWithItem:self.lbDate attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.itemView attribute:NSLayoutAttributeLeading multiplier:1.0f constant:-12.0];
//    [self addConstraint:lbTimeRight];
    
    
    self.UnreadMark = [[UIImageView alloc] initWithFrame:CGRectMake(self.avatarView.frame.origin.x + self.avatarView.frame.size.width -7 , self.avatarView.frame.origin.y, 29/2, 28/2)];
    self.UnreadMark.image = [UIImage imageNamed:@"message-dot"];
    [self addSubview:self.UnreadMark];
    
    self.UnreadNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    self.UnreadNumber.textAlignment = NSTextAlignmentCenter;
    self.UnreadNumber.textColor = [UIColor whiteColor];
    self.UnreadNumber.font = [UIFont systemFontOfSize:10];
   
    self.UnreadNumber.center =  CGPointMake(self.UnreadMark.frame.size.width/2, self.UnreadMark.frame.size.height/2);;
   
    [self.UnreadMark addSubview:self.UnreadNumber];
    
    
    self.SeperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, self.frame.size.height-1, CGRectGetWidth(self.bounds), 0.5)];
    self.SeperatorView.backgroundColor = [UIColor colorWithRed:0xcc/255.0 green:0xcc/255.0 blue:0xcc/255.0 alpha:1.0];
    [self addSubview:self.SeperatorView];
    
    self.lbMessage = [[UILabel alloc] initWithFrame:CGRectMake(54, CGRectGetMaxY(self.lbName.frame) + 4, SCREEN_WIDTH- 117, 15)];
    self.lbMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.lbMessage.font = [UIFont systemFontOfSize:15];
    self.lbMessage.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    [self addSubview:self.lbMessage];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    self.avatarView.coverImageView.highlighted = (highlighted || self.isSelected);
    self.itemView.coverImageView.highlighted = (highlighted || self.isSelected);
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.SeperatorView.frame = CGRectMake(10, self.contentView.frame.size.height-1, CGRectGetWidth(self.bounds), 0.5);
    
    [self adjustLBMessage];
}
-(void)adjustLBMessage
{
    if (self.message.text.length > 0) {
        
        self.lbMessage.numberOfLines = 3;
        CGFloat kWidth = SCREEN_WIDTH - 117;
        CGSize fitSize = [self.lbMessage sizeThatFits:CGSizeMake(kWidth, 0)];
        CGPoint originalPoint = CGPointMake(self.lbMessage.frame.origin.x, self.lbMessage.frame.origin.y);
        self.lbMessage.frame = CGRectMake(originalPoint.x, originalPoint.y, kWidth, fitSize.height);
    }
}

- (void)configWithEntity:(HGMessage *)message
{
    
    //[HGUserData sharedInstance].fmUserID
    HGUser* theOtherUser;
    if ([message.from.uid isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        theOtherUser = message.to;
    }
    else{
        theOtherUser = message.from;
    }
    
    
    self.message = message;
    self.lbName.text = theOtherUser.displayName;
    
    self.lbName.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.lbMessage.text = message.text;
    
    self.lbMessage.textColor = FANCY_COLOR(@"818181");
    self.lbMessage.numberOfLines = 0;
    [self adjustLBMessage];

    self.lbDate.text = [message prettyDate];
    
    int sugggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatarView.frame.size.width*2];
    NSString* avatarLink = [[HGUtils sharedInstance] cloudinaryLink:theOtherUser.portraitLink width:sugggestWidth height:sugggestWidth];
    
    [self.avatarView setImageWithURL:[NSURL URLWithString:avatarLink] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    
//    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:avatarLink] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    
    if (message.badgeCount.intValue > 99) {
        self.UnreadNumber.text = @"99";
    }
    else
    {
        self.UnreadNumber.text = message.badgeCount;
    }
    if (!message.unread || [self.UnreadNumber.text integerValue] == 0) {
        [self.UnreadMark setHidden:YES];
        [self.UnreadNumber setHidden:YES];
    }
    else{
        [self.UnreadNumber setHidden:NO];
        [self.UnreadMark setHidden:NO];
    }

    int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.itemView.bounds.size.width * 2];
    int suggestHeight = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.itemView.bounds.size.height * 2];
    
    NSString * itemImageLink = [[HGUtils sharedInstance] cloudinaryLink:message.itemImage.imageLink width:suggestWidth height:suggestHeight];
    
    [self.itemView setImageWithURL:[NSURL URLWithString:itemImageLink] placeholderImage:nil];
    
//    [self.itemView sd_setImageWithURL:[NSURL URLWithString:itemImageLink]  placeholderImage:[UIImage imageWithColor:FANCY_COLOR(@"eaeaea")]];
    
    
    if (message.from.verified.intValue == 1) {
        self.avatarView.verifyImageView.image = [UIImage imageNamed:@"verify_user_verify"];
//        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        self.avatarView.verifyImageView.image = [UIImage imageNamed:@"verify_user_not_verify"];
//        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }

    self.avatarView.coverImageView.highlighted = (self.isHighlighted || self.isSelected);
    self.itemView.coverImageView.highlighted = (self.isHighlighted || self.isSelected);
}

@end

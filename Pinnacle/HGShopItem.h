//
//  HGShopItem.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "HGOfferLine.h"

@class HGUser;

@interface HGGeoLocation : JSONModel

@property (nonatomic, assign) float lat;
@property (nonatomic, assign) float lon;

@end

@protocol HGItemImage <NSObject>

@end

@interface HGItemImage : JSONModel

@property (nonatomic, strong) NSString * imageLink;
@property (nonatomic, assign) int width;
@property (nonatomic, assign) int height;

@end

typedef NS_ENUM(NSInteger, HGItemState) {
    HGItemStateListing = 0,
    HGItemStateOfferAccepted =1,
    HGItemStateDoneDelivery = 2,
    HGItemStateRatedSeller = 3,
    HGItemStateUnapproved = 4,
    HGItemStateUnavailable = 5,
    
    ITEM_STATE_LISTING = 0,
    ITEM_STATE_SOLD = 1     ,                       // # 新状态：用户标记为卖出
    ITEM_STATE_UNAPPROVED = 4,                      /// # 新状态：运营下架
    ITEM_STATE_UNAVAILABLE = 5,                      //# 新状态：用户删除
    ITEM_STATE_UNLISTED = 6 ,                     //# 新状态：用户下架
    ITEM_STATE_PENDING = 7,
};

@interface HGShopItem : JSONModel

// Notice: You should not use "localPrice" anymore, it just exists for compatible.

@property (nonatomic, assign) BOOL bBlockedOwner;
@property (nonatomic, assign) HGItemState state;
@property (nonatomic, assign) NSInteger created_at;
@property (nonatomic, assign) NSInteger deletable;
@property (nonatomic, assign) NSInteger is_new;
@property (nonatomic, assign) NSInteger renew_ttl;
@property (nonatomic, assign) NSInteger updated_at;
@property (nonatomic, assign) float distance;
@property (nonatomic, assign) float originalPrice;
@property (nonatomic, assign) float price;
@property (nonatomic, assign) int rootCategoryId;
@property (nonatomic, assign) int categoryIndex;
@property (nonatomic, assign) int shipping_method;
@property (nonatomic, strong) HGGeoLocation *geoLocation;
@property (nonatomic, strong) HGUser *seller;
@property (nonatomic, strong) NSMutableArray<HGItemImage> *images;
@property (nonatomic, strong) NSString *brand_name;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *currencyUnit;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *mediaLink;
@property (nonatomic, strong) NSString *region;
@property (nonatomic, strong) NSString *rf_tag;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *uid;


- (CGSize)sizeForBoundingWidth:(CGFloat)width;
- (NSString *)formatAddress;
- (NSString *)formatDistance;
- (NSString *)currencyForDisplay;
- (NSString *)originalPriceForDisplay;
- (NSString *)priceForDisplay;
- (NSURL *)coverImageUrlWithWidth:(CGFloat)width height:(CGFloat)height;
@end


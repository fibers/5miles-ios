//
//  HGChatCollectionViewController.h
//  Pinnacle
//
//  Created by shengyuhong on 15/5/13.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "JSQMessages.h"
#import "HGChatViewController.h"
#import <CTAssetsPickerController.h>

@class HGShopItem, HGUser, HGOfferLineMeta;

@interface HGChatCollectionViewController : JSQMessagesViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CTAssetsPickerControllerDelegate, UITextViewDelegate>


@property (nonatomic, assign) BOOL bBuyer;
@property (nonatomic, assign) BOOL bFromItemDetailPage;
@property (nonatomic, assign) NSTimeInterval latestTimestamp;
@property (nonatomic, assign) id<HGChatViewControllerDelegate> delegate;
@property (nonatomic, strong) HGShopItem *item;
@property (nonatomic, strong) HGUser *toUser;
@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) NSMutableDictionary *dictMessageIDs;
@property (nonatomic, strong) NSString *offerLineID;

@property (nonatomic, strong) NSString* rf_tag;

- (instancetype)initFromChatVC:(HGChatViewController *)vcChat;
- (void)refreshDataFromChatVC:(HGChatViewController *)vcChat;
- (void)refreshMessagesWithOffers:(NSArray *)offers;
- (void)updateMessagesWithNewOffers:(NSArray *)newOffers;

@end

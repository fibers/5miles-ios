//
//  HGItemLocation.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-19.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MapKit/MapKit.h>

@class HGShopItem;

@interface HGItemLocation : NSObject <MKAnnotation>

- (id)initWithItem:(HGShopItem *)item;
//- (MKMapItem *)mapItem;

@end

//
//  HGChatViewController.h
//  Pinnacle
//
//  Created by shengyuhong on 15/1/22.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HPGrowingTextView.h>
#import "HGChatMessageCell.h"
#import "TTTAttributedLabel.h"

@class HGShopItem, HGUser;

@protocol HGChatViewControllerDelegate <NSObject>

- (void) didFinishChatWithNewMessage;

@end


@interface HGChatViewController : UIViewController<UIScrollViewDelegate, UIActionSheetDelegate, HPGrowingTextViewDelegate, TTTAttributedLabelDelegate>

@property (nonatomic, strong) NSString *offerLineID;
@property (nonatomic, strong) HGShopItem *item;
@property (nonatomic, strong) HGUser *toUser;

@property (nonatomic, assign) BOOL bFromItemDetailPage;
@property (nonatomic, assign) BOOL bBuyer;
@property (nonatomic, assign) BOOL bBlockedUser;

@property (nonatomic, assign) id<HGChatViewControllerDelegate> delegate;

@property (nonatomic, assign) BOOL reviewAleadyExist;
@property (nonatomic, strong) NSString* rf_tag;

-(void)setRf_tag:(NSString *)rf_tag;

- (IBAction)chatMoreAction:(id)sender;




@end

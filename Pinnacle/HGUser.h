//
//  HGUser.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "HGShopItem.h"

@interface HGUser : JSONModel

@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * displayName;
@property (nonatomic, strong) NSString * portraitLink;

@property (nonatomic, strong) NSNumber* verified;
@property (nonatomic, strong) NSNumber* facebook_verified;
@property (nonatomic, strong) NSNumber* email_verified;
@property (nonatomic, strong) NSNumber* mobile_phone_verified;

@property (nonatomic, strong) NSString* region;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* city;

@property (nonatomic, strong) HGGeoLocation *geoLocation;
@property (nonatomic, strong) NSString *place;
@property (nonatomic, assign) float distance;
@property (nonatomic, assign) BOOL following;


@property (nonatomic, strong) NSString* review_num;
@property (nonatomic, strong) NSString* review_score;

- (BOOL)isMe;
- (NSString *)formatDistance;

@end

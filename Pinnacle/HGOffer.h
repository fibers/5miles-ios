//
//  HGOffer.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

typedef NS_ENUM(NSInteger, HGOfferType) {
    HGOfferTypeOffer,
    HGOfferTypeAcceptOffer,
    HGOfferTypeChat,
    HGOfferTypeConfirmDelivery,
    HGOfferTypeRatedSeller
};

typedef NS_ENUM(NSInteger, HGOfferMessageType) {
    HGOfferMessageTypeText = 0,
    HGOfferMessageTypePhoto,
    HGOfferMessageTypeLocation
};



@interface HGOffer : JSONModel

@property (nonatomic, strong) NSString * uid;
@property (nonatomic, assign) BOOL fromBuyer;
@property (nonatomic, assign) BOOL fromMe;
@property (nonatomic, assign) CGFloat price;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, assign) HGOfferType type;
@property (nonatomic, strong) NSDate * timestamp;
@property (nonatomic, assign) int bShowTimestamp;
@property (nonatomic, assign) HGOfferMessageType messageType;
@property (nonatomic, strong) NSDictionary *payload;

@end

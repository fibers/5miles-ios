//
//  HGRoundCornerImageView.h
//  Pinnacle
//
//  Created by zhenyonghou on 15/6/26.
//  Copyright © 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGRoundCornerImageView : UIImageView

@property (nonatomic, strong, readonly) UIImageView *coverImageView;

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage;

@end


@interface HGAvararImageView : HGRoundCornerImageView

- (id)initWithFrame:(CGRect)frame verifySize:(CGSize)verifySize;

- (void)setVerifySize:(CGSize)verifySize;

@property (nonatomic, strong, readonly) UIImageView *verifyImageView;

@end


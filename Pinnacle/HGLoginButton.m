//
//  HGLoginButton.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGLoginButton.h"
#import "HGConstant.h"
@implementation HGLoginButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)_setup
{
    self.layer.cornerRadius = 4;
    self.layer.borderWidth = 1;
    self.layer.borderColor = FANCY_COLOR(@"242424").CGColor;
    self.clipsToBounds = YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  HGCategory.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface HGCategory : JSONModel

@property (nonatomic, assign) int index;  //uid....
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString* icon;
@property (nonatomic, strong) NSString* hover_icon;

@property (nonatomic, strong) NSMutableArray* subCategories;
@property (nonatomic, strong) NSString* parent_id;
-(instancetype)commonSetup:(int)index withTitle:(NSString*)titleString withIconName:(NSString*)iconNameString;
@end

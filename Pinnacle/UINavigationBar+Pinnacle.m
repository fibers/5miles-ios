//
//  UINavigationBar+Pinnacle.m
//  Pinnacle
//
//  Created by mumuhou on 15/7/23.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "UINavigationBar+Pinnacle.h"
#import <objc/runtime.h>

@implementation UINavigationBar(Pinnacle)

static char overlayKey;
static char emptyImageKey;

- (UIView *)overlay
{
    return objc_getAssociatedObject(self, &overlayKey);
}

- (void)setOverlay:(UIView *)overlay
{
    objc_setAssociatedObject(self, &overlayKey, overlay, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIImage *)emptyImage
{
    return objc_getAssociatedObject(self, &emptyImageKey);
}

- (void)setEmptyImage:(UIImage *)image
{
    objc_setAssociatedObject(self, &emptyImageKey, image, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)lt_setBackgroundColor:(UIColor *)backgroundColor
{
    if (!self.overlay) {
        [self setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, -20, [UIScreen mainScreen].bounds.size.width, CGRectGetHeight(self.bounds) + 20)];
        self.overlay.userInteractionEnabled = NO;
        self.overlay.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self insertSubview:self.overlay atIndex:0];
    }
    self.overlay.backgroundColor = backgroundColor;
}

- (void)lt_setTranslationY:(CGFloat)translationY
{
    self.transform = CGAffineTransformMakeTranslation(0, translationY);
}

- (void)lt_setContentAlpha:(CGFloat)alpha
{
    if (!self.overlay) {
        [self lt_setBackgroundColor:self.barTintColor];
    }
    [self setAlpha:alpha forSubviewsOfView:self];
    if (alpha == 1) {
        if (!self.emptyImage) {
            self.emptyImage = [UIImage new];
        }
        self.backIndicatorImage = self.emptyImage;
    }
}

// 如果UIBarButtonItem设置的title,还是不能隐藏

- (void)setAlpha:(CGFloat)alpha forSubviewsOfView:(UIView *)view
{
//    for (UIBarButtonItem *buttonItem in self.topItem.leftBarButtonItems) {
//        buttonItem.customView.alpha = alpha;
//    }
//    
//    self.topItem.titleView.alpha = alpha;
//    
//    for (UIBarButtonItem *buttonItem in self.topItem.rightBarButtonItems) {
//        buttonItem.customView.alpha = alpha;
//    }
    
    // _UINavigationBarBackIndicatorView问题
    for (UIView *subview in view.subviews) {
        if (subview == self.overlay) {
            continue;
        }
        
        if ([NSStringFromClass([subview class]) isEqualToString:@"_UINavigationBarBackIndicatorView"]) {
            continue;
        }
        
        subview.alpha = alpha;
        [self setAlpha:alpha forSubviewsOfView:subview];
    }
}

- (void)lt_reset
{
    self.transform = CGAffineTransformIdentity;
    [self lt_setContentAlpha:1.0];
    
//    [self setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//    [self.overlay removeFromSuperview];
//    self.overlay = nil;
}

@end

//
//  ListingLocationVC.m
//  Pinnacle
//
//  Created by Alex on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "ListingLocationVC.h"
#import "HGConstant.h"
#import "HGUserData.h"
#import "HGAppData.h"
#import "HGUITextField.h"
#import <SVProgressHUD.h>
#import "PXAlertView+Customization.h"
#import "HGUtils.h"
#import "NSString+EmailAddresses.h"

@interface ListingLocationVC ()
@property (nonatomic, strong)HGUITextField* zipCodeField;
@property (nonatomic, strong)UILabel* hintLabel;
@property (nonatomic, strong)UILabel* hintLabel2;
@property (nonatomic, strong)UIButton* useGPSButton;


@property (nonatomic, strong)UIButton* btnSend;


@property (nonatomic, strong)NSString* deviceCountryCode;

@property (nonatomic, assign)BOOL anylocationChange ;


@end

@implementation ListingLocationVC



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    ////TextField has a strange bug, from 8.3..view re show will clear the content.
    self.zipCodeField = [[HGUITextField alloc] initWithFrame:CGRectMake(0, 70, self.view.frame.size.width,40)];
    [self.view addSubview:self.zipCodeField];
    self.zipCodeField.font = [UIFont systemFontOfSize:15];
    self.zipCodeField.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.zipCodeField.delegate = self;
    self.zipCodeField.placeholder = NSLocalizedString(@"Zipcode", nil);
    self.zipCodeField.backgroundColor = [UIColor whiteColor];
    self.zipCodeField.edgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    
    if (self.zipCodeField.text.length > 0) {
        [self enableSendButton];
    }
    else{
        [self disableSendButton];
    }
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[HGUtils sharedInstance] gaTrackViewName:@"listinglocationview"];
     [self addRightButton];
    
    NSLocale *currentLocale = [NSLocale currentLocale];
    
    self.deviceCountryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    self.navigationItem.title = NSLocalizedString(@"Listing location",nil);
    
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    
    
    
    self.hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 70+44+5, self.view.frame.size.width-30, 50)];
    [self.view addSubview:self.hintLabel];
    self.hintLabel.textColor = FANCY_COLOR(@"818181");
    self.hintLabel.numberOfLines = 2;
    self.hintLabel.font = [UIFont systemFontOfSize:14];
    
    self.hintLabel.text = NSLocalizedString(@"Your zipcode will be used in confirm your location and your items' location", nil);
    
    
    self.hintLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(15,  CGRectGetMaxY(self.hintLabel.frame) , self.view.frame.size.width-30, 20)];
    self.hintLabel2.textColor = FANCY_COLOR(@"818181");
    self.hintLabel2.numberOfLines = 2;
    self.hintLabel2.font = [UIFont systemFontOfSize:14];
    self.hintLabel2.text = NSLocalizedString(@"Or", nil);
    self.hintLabel2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.hintLabel2];
    
    
    self.useGPSButton = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(self.hintLabel.frame) + 30, self.view.frame.size.width -30, 36)];
    self.useGPSButton.layer.cornerRadius = 4;
    self.useGPSButton.backgroundColor = FANCY_COLOR(@"cccccc");
    self.useGPSButton.titleLabel.textColor = [UIColor whiteColor];
    [self.useGPSButton setTitle:NSLocalizedString(@"Current location", nil) forState:UIControlStateNormal];
    
    [self.useGPSButton addTarget:self action:@selector(onTouchUseGPS) forControlEvents:UIControlEventTouchUpInside];
    [self enableGPSButton];
    [self.view addSubview:self.useGPSButton];
    
   
}

-(void)GPSFailHandle
{
    if (IOS_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        PXAlertView *alert = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Please go to phone setting to open app GPS access first", nil) messageWithAttribute:nil cancelTitle:NSLocalizedString(@"Cancel", nil) otherTitle:NSLocalizedString(@"Go", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            if(!cancelled){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
            else{
                [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Without access to your current location, you must use the zipcode.", nil)];
            }
        }];
        
        
        [alert useDefaultIOS7Style];
        
    }
    else{
        PXAlertView *alert = [PXAlertView showAlertWithTitle:NSLocalizedString(@"Please go to phone setting to open app GPS access first", nil) messageWithAttribute:nil cancelTitle:NSLocalizedString(@"OK", nil) otherTitle:nil completion:^(BOOL cancelled, NSInteger buttonIndex) {
        }];
        
        [alert useDefaultIOS7Style];
    }
}
-(void)onTouchUseGPS
{
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusDenied && [HGUserData sharedInstance].zipcodeString.length == 0)
    {
        [self GPSFailHandle];
    }
    else{
        self.anylocationChange = YES;
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"listinglocationview" action:@"listings_gps" label:nil value:nil];
        self.sellItem_CityCode = [HGAppData sharedInstance].cityString;
        self.sellItem_RegionCode = [HGAppData sharedInstance].regionString;
        self.sellItem_CountryCode = [HGAppData sharedInstance].countryString;
        
        self.sellItem_lat = [@"" stringByAppendingFormat:@"%f",[HGAppData sharedInstance].GPSlat];
        self.sellItem_lon = [@"" stringByAppendingFormat:@"%f",[HGAppData sharedInstance].GPSlon];
            
        [self performSegueWithIdentifier:@"doneListingLocationZip" sender:self];
    }
    
}

-(void)enableGPSButton
{
    self.useGPSButton.userInteractionEnabled = YES;
    self.useGPSButton.backgroundColor = FANCY_COLOR(@"ff8830");
}
-(void)disableGPSButton
{
    self.useGPSButton.userInteractionEnabled = NO;
    self.useGPSButton.backgroundColor = FANCY_COLOR(@"cccccc");
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.zipCodeField becomeFirstResponder];
}


////email max length limit 100;;
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (newString.length==0) {
        [self disableSendButton];
        [self enableGPSButton];
    }
    else{
        [self enableSendButton];
        [self disableGPSButton];
    }
    
    
    return newString.length > 100 ? NO:YES;
    
}
- (void)enableSendButton{
    self.btnSend.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.btnSend.titleLabel.textColor = FANCY_COLOR(@"ff8830");
    [self.btnSend setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    self.btnSend.userInteractionEnabled = YES;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)disableSendButton{
    self.btnSend.layer.borderColor = FANCY_COLOR(@"cccccc").CGColor;
    self.btnSend.titleLabel.textColor = FANCY_COLOR(@"cccccc");
    
    [self.btnSend setTitleColor:FANCY_COLOR(@"cccccc") forState:UIControlStateNormal];
    self.btnSend.userInteractionEnabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}
-(void)onTouchSend
{

    NSLog(@"touch send");
    
    [self getZipCodeAddress:self.zipCodeField.text];
}
-(void)addRightButton
{
    UIView* rightBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    
    self.btnSend = [[UIButton alloc] initWithFrame:CGRectMake(13, 0, 65, 30)];
    self.btnSend.clipsToBounds = YES;
    self.btnSend.layer.cornerRadius = 3;
    self.btnSend.layer.borderWidth = 1;
    self.btnSend.layer.borderColor = [UIColor grayColor].CGColor;
    self.btnSend.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.btnSend setTitle:NSLocalizedString(@"Save",nil) forState:UIControlStateNormal];
   
    [self.btnSend addTarget:self action:@selector(onTouchSend) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [rightBackView addSubview:self.btnSend];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBackView];
    
    [self disableSendButton];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    if (!self.anylocationChange) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"listinglocationview" action:@"cancel" label:nil value:nil];
    }
}



-(void)getZipCodeAddress:(NSString*)zipcode
{
    
    [SVProgressHUD show];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    
    NSString* baseUrl = [@"http://maps.google.com/maps/api/geocode/json?components=postal_code:" stringByAppendingFormat:@"%@", zipcode] ;
    baseUrl = [baseUrl stringByAppendingString:@"&language=en&sensor=false"];
    
    [request setURL:[NSURL URLWithString:baseUrl]];
    
    //Send an asynchronous request so it doesn't pause the main thread
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        
        NSHTTPURLResponse *responseCode = (NSHTTPURLResponse *)response;
        if ([responseCode statusCode] == 200)
        {
            if (data == nil) {
                return;
            }
            
            
            self.anylocationChange = YES;
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"listinglocationview" action:@"listgings_zipcodesave" label:nil value:nil];
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"the zipcode address dict is %@", dict);
            
            if (![[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
                [SVProgressHUD dismiss];
                [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Sorry, we can't recognize this zipcode.", nil)];
                
                return;
            }
            NSArray* addressArray = [dict objectForKey:@"results"];
            
            int nBestAddressIndex =[self getBestMatchAddressIndex:addressArray];
            
            NSDictionary* topAddress = [addressArray objectAtIndex:nBestAddressIndex];
            
            NSArray* address_components =[topAddress objectForKey:@"address_components"];
            
            
            for (int i =0; i<address_components.count; i++) {
                NSDictionary* address_component = [address_components objectAtIndex:i];
                NSArray* types = [address_component objectForKey:@"types"];
                NSString* topType = [types objectAtIndex:0];
                if ([topType isEqualToString:@"country"]) {
                    self.sellItem_CountryCode = [address_component objectForKey:@"long_name"]!=nil?[address_component objectForKey:@"long_name"]:@"" ;
                }
                if ([topType isEqualToString:@"locality"]) {
                    self.sellItem_CityCode = [address_component objectForKey:@"short_name"]!=nil?[address_component objectForKey:@"short_name"]:@"";
                }
                if ([topType isEqualToString:@"administrative_area_level_1"]) {
                    self.sellItem_RegionCode = [address_component objectForKey:@"short_name"]!=nil? [address_component objectForKey:@"short_name"]:@"";
                }
            }
            
            NSDictionary* geometry = [topAddress objectForKey:@"geometry"];
            NSDictionary* location = [geometry objectForKey:@"location"];
            self.sellItem_lat = [(NSNumber*)[location objectForKey:@"lat"] stringValue];
            self.sellItem_lon = [(NSNumber*)[location objectForKey:@"lng"] stringValue];
            
            [self performSegueWithIdentifier:@"doneListingLocationZip" sender:self];
            
        }
        else{
            [SVProgressHUD dismiss];
        }
        
        
        
    }];
    
    
}

-(int)getBestMatchAddressIndex:(NSArray*)resutls
{
    int nBestIndex = 0;
    //int USAIndex = 0;
    for (int i = 0; i< resutls.count; i++) {
        NSDictionary* address = [resutls objectAtIndex:i];
        NSArray* address_components =[address objectForKey:@"address_components"];
        
        for (int j =0; j<address_components.count; j++) {
            NSDictionary* address_component = [address_components objectAtIndex:j];
            NSArray* types = [address_component objectForKey:@"types"];
            NSString* topType = [types objectAtIndex:0];
            if ([topType isEqualToString:@"country"]) {
                NSString* countryShortName  = [address_component objectForKey:@"short_name"]!=nil?[address_component objectForKey:@"short_name"]:@"" ;
                if ([countryShortName isEqualToString:self.deviceCountryCode]) {
                    if (nBestIndex==0) {
                        //only using the first country match.
                        nBestIndex = i;
                    }
                }
            }
        }
        
    }
    
    
    if (!(nBestIndex<resutls.count)) {
        nBestIndex = 0;
    }
    return nBestIndex;
}


@end

//
//  ItemLikersDetailViewController.m
//  Pinnacle
//
//  Created by Alex on 14-11-19.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "ItemLikersDetailViewController.h"
#import "HGUtils.h"
#import "HGAppData.h"
#import "ItemLikers.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGUser.h"
#import "HGUserViewController.h"
#import "HGAppDelegate.h"
#import "HGSearchViewController.h"
#import "HGFollowerCell.h"
#import "ItemLikersMoreCell.h"
#import <SVProgressHUD.h>
#import "SVPullToRefresh/SVPullToRefresh.h"

@interface ItemLikersDetailViewController ()

@property (nonatomic, strong) UIView* noMoreHintView;


@end

@implementation ItemLikersDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate=self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self getItemLikers];
    [self addNoMoreHintView];
    
    [[HGUtils sharedInstance] gaTrackViewName:@"wholikethisitem_view"];
    
    self.likersArray = [[NSMutableArray alloc] init];
    self.navigationItem.title = NSLocalizedString(@"Who Likes this listing", nil);
    //self.tableView set
}
-(void)getItemLikers
{
    if (self.itemIdString==nil) {
        return;
    }
    __weak ItemLikersDetailViewController * weakSelf = self;
     NSNumber* limitnumber = [[NSNumber alloc] initWithInt:15];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"item_likers/" parameters:@{@"item_id": self.itemIdString,@"limit":limitnumber} success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError * error;
        [self.likersArray removeAllObjects];
        self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        NSArray* likerArray = [responseObject objectForKey:@"objects"];
        if (likerArray != nil && likerArray.count > 0) {
            for (NSDictionary * dictObj in likerArray) {
                ItemLikers * user = [[ItemLikers alloc] initWithDictionary:dictObj error:&error];
                if(user!=nil)
                {
                    [self.likersArray addObject:user];
                }
                
            }
        }
        if (![self.nextLink isEqual:[NSNull null]]) {
            [self.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf _loadNextPage];
            }];
        }
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get item likers failed: %@", error);
    }];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.likersArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ItemLikersMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             @"ItemLikersMoreCell"];
    if (cell == nil) {
        cell = [[ItemLikersMoreCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:@"ItemLikersMoreCell"];
    
    }
    
    
    
    cell.userName.textColor  = SYSTEM_DEFAULT_FONT_COLOR_1;
    cell.userName.font = [UIFont systemFontOfSize:14];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ItemLikers*likers = [self.likersArray objectAtIndex:indexPath.row];
    
    [cell configCell:likers];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"wholikethisitem_view" action:@"peoplelikesitem" label:nil value:nil];

    ItemLikers*likers = [self.likersArray objectAtIndex:indexPath.row];
    
    HGUser* user = [[HGUser alloc] init];
    user.uid = likers.uid;
    user.displayName = likers.nickname;
    user.portraitLink = likers.portrait;
    /*HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.tabController setSelectedIndex:0];
    UINavigationController* Navi = appDelegate.tabController.viewControllers[0];
    HGSearchViewController* searchViewController= Navi.viewControllers[0];
    [searchViewController performSegue:user withSegueName:@"ShowUserDetail"];*/
    [self performSegueWithIdentifier:@"ShowUserDetail" sender:user ];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowUserDetail"]) {
        HGUserViewController * vcDest = segue.destinationViewController;
        vcDest.user = sender;
    }
}

#pragma mark - Helpers

- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.tableView.infiniteScrollingView stopAnimating];
        self.tableView.showsInfiniteScrolling = NO;
        [self.noMoreHintView setFrame:CGRectMake(0, self.tableView.contentSize.height, self.tableView.frame.size.width, 40)];
        
        [self.noMoreHintView setHidden:NO];
        
    } else {
        [SVProgressHUD show];
         NSNumber* limitnumber = [[NSNumber alloc] initWithInt:15];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[NSString stringWithFormat:@"item_likers/%@", self.nextLink] parameters:@{@"item_id": self.itemIdString,@"limit":limitnumber} success:^(NSURLSessionDataTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            for (id dictObj in results) {
                NSError * error = nil;
                ItemLikers * user = [[ItemLikers alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.likersArray addObject:user];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            [self.tableView reloadData];
            [self.tableView.infiniteScrollingView stopAnimating];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [SVProgressHUD dismiss];
            [self.tableView.infiniteScrollingView stopAnimating];
            NSLog(@"get item likers failed: %@", error.localizedDescription);
        }];
        
    }
}

-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    [self.tableView addSubview:self.noMoreHintView];
    [self.noMoreHintView setHidden:YES];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}

@end

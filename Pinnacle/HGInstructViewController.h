//
//  HGInstructViewController.h
//  Pinnacle
//
//  Created by Alex on 14-10-21.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    HGInstructViewType_VerifyIntro = 1,
    HGInstructViewType_SellerChatIntro = 2,
    HGInstructViewType_BuyerChatIntro = 3,
    
} HGInstructViewType;

@protocol HGInstructDelegate <NSObject>

@optional
-(void)onStartVerifyAction:(id)sender;
- (void)onClickGotIt;
-(void)onClickOutOfButton:(id)sender;
@end

@interface HGInstructViewController : UIViewController

@property (nonatomic, assign) HGInstructViewType AdditionalViewType;
@property (nonatomic, assign) id<HGInstructDelegate> delegate;

- (void)enableBtnGotIt;

@end

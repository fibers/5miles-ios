//
//  HGNoTouchActionView.m
//  Pinnacle
//
//  Created by Alex on 14-10-9.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGNoTouchActionView.h"

@implementation HGNoTouchActionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    // UIView will be "transparent" for touch events if we return NO
    return NO;//(point.y < MIDDLE_Y1 || point.y > MIDDLE_Y2);
}
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *hitView = [super hitTest:point withEvent:event];
    
    // If the hitView is THIS view, return the view that you want to receive the touch instead:
    if (hitView == self) {
        return self.responseView;
    }
    // Else return the hitView (as it could be one of this view's buttons):
    return [super hitTest:point withEvent:event];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (1) {
        //always forward touches action
        [self.responseView touchesBegan:touches withEvent:event];
    }
    else {
        // Do whatever A does with touches.
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.responseView touchesMoved:touches withEvent:event];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.responseView touchesEnded:touches withEvent:event];
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.responseView touchesCancelled:touches withEvent:event];
}
@end

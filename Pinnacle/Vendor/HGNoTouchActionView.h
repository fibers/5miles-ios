//
//  HGNoTouchActionView.h
//  Pinnacle
//
//  Created by Alex on 14-10-9.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGNoTouchActionView : UIView
@property (nonatomic, assign) UIView * responseView;
@end

#import <UIKit/UIKit.h>

static float kBDKNotifyHUDDefaultWidth = 130.0f;
static float kBDKNotifyHUDDefaultHeight = 100.0f;


static float userDefineWith = 0;
static float userDefineHeight = 0;

@interface BDKNotifyHUD : UIView

@property (nonatomic) CGFloat destinationOpacity;
@property (nonatomic) CGFloat currentOpacity;
@property (nonatomic) UIView *iconView;
@property (nonatomic) CGFloat roundness;
@property (nonatomic) BOOL bordered;
@property (nonatomic) BOOL isAnimating;

@property (strong, nonatomic) UIColor *borderColor;
@property (strong, nonatomic) NSString *text;





+ (id)notifyHUDWithView:(UIView *)view text:(NSString *)text;
+ (id)notifyHUDWithImage:(UIImage *)image text:(NSString *)text;
+(id)notifyHUDWithView:(UIView *)view text:(NSString *)text withFrameWith:(float)width withFrameHeight:(float)height;



- (id)initWithView:(UIView *)view text:(NSString *)text;
- (id)initWithImage:(UIImage *)image text:(NSString *)text;


- (void)setCurrentOpacity:(CGFloat)currentOpacity;
- (void)setImage:(UIImage *)image;
-(void)updateImage:(UIImage*)image;


- (void)presentWithDuration:(CGFloat)duration speed:(CGFloat)speed inView:(UIView *)view completion:(void (^)(void))completion;

@end

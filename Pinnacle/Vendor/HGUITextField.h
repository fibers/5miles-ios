//
//  HGUITextField.h
//  Pinnacle
//
//  Created by Alex on 15-3-9.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGUITextField : UITextField

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end

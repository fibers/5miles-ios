//
//  HGGoodSellerTopCell.m
//  Pinnacle
//
//  Created by Alex on 15-4-8.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGGoodSellerTopCell.h"
#import "HGConstant.h"
@implementation HGGoodSellerTopCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self commonSetup];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self commonSetup];
}


-(void)commonSetup
{
    self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(64, 0, self.frame.size.width-64-6, 76-6)];
    self.descriptionLabel.textColor = FANCY_COLOR(@"818181");
    self.descriptionLabel.font = [UIFont systemFontOfSize:13];
    self.descriptionLabel.numberOfLines = 0;
    self.descriptionLabel.textAlignment = NSTextAlignmentLeft;
    
    
    [self addSubview:self.descriptionLabel];
    
    
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height -14, self.frame.size.width, 14)];
    
    self.bottomView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    [self addSubview: self.bottomView];
    
    
    //self.descriptionLabel.backgroundColor = [UIColor grayColor];
}

-(void)layoutSubviews{
    if (self.CellType==0) {
        self.descriptionLabel.frame = CGRectMake(64, 2, self.frame.size.width-64-6, 70);
    }
    else{
        self.descriptionLabel.frame = CGRectMake(64, 2, self.frame.size.width-64-6, 94-6);
    }
}
-(void)configCellWithSufficientHint:(NSString *)sufficientHint insufficientHint:(NSString *)insufficientHint
{
    if (self.CellType==0) {
        self.descriptionLabel.text = sufficientHint;
    }else {
        self.descriptionLabel.text = insufficientHint;
    }
    self.bottomView.frame = CGRectMake(0, self.frame.size.height -14, self.frame.size.width, 14);
}

@end

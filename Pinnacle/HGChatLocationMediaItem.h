//
//  HGChatLocationMediaItem.h
//  Pinnacle
//
//  Created by shengyuhong on 15/5/25.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "JSQLocationMediaItem.h"
#import "TTTAttributedLabel.h"

@interface HGChatLocationMediaItem : JSQLocationMediaItem

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) TTTAttributedLabel* addressLabel;
@property (nonatomic, strong) TTTAttributedLabel* titleLabel;

@property (nonatomic, strong) NSString* poiTitle;
@property (nonatomic, strong) NSString* poiAddress;

@property (nonatomic, strong) NSString* latString;
@property (nonatomic, strong) NSString* lonString;

- (void)addLoadingMask;
- (void)addFailedIcon;
- (void)removeLoadingMask;
- (void)updateProgress:(CGFloat)progress;


-(void)setLeftSpeakerInsets;
-(void)setRightSpeakerInsets;
@end

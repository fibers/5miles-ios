//
//  HGChatSupplementaryReusableView.h
//  Pinnacle
//
//  Created by shengyuhong on 15/5/19.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSInteger const TopConstraintDescription;
extern NSInteger const TrailingConstraintDescription;
extern NSInteger const BottomConstraintDescription;
extern NSInteger const LeadingConstraintDescription;

extern NSInteger const TopPaddingDescription;
extern NSInteger const BottomPaddingDescription;


@interface HGChatSupplementaryReusableView : UICollectionReusableView

- (void)configText:(NSString *)text style:(NSParagraphStyle *)style boldFirstSentence:(BOOL)bold;

@end

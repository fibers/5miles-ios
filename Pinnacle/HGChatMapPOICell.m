//
//  HGChatMapPOICell.m
//  Pinnacle
//
//  Created by Alex on 15/5/13.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatMapPOICell.h"
#import "HGConstant.h"
//#import <AddressBook/AddressBook.h>
//#import <CoreLocation/CoreLocation.h>
#import "HGUserData.h"
#import "HGAppData.h"

@import AddressBookUI;
@import CoreLocation;
@implementation HGChatMapPOICell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonSetup];
    }
    return self;
}
- (void)awakeFromNib {
    [self commonSetup];
}
-(void)updateConstraints
{
    
}

-(void)commonSetup
{
    CGRect rx = [ UIScreen mainScreen ].bounds;
    self.POIName = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, rx.size.width-20, 20)];
    [self addSubview:self.POIName];
    self.POIName.font = [UIFont boldSystemFontOfSize:15];
    self.POIName.textColor = FANCY_COLOR(@"242424");
    self.POIName.text = @"Citadel outlets";
    
    self.distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 8+20+2, rx.size.width-20, 15)];
    [self addSubview:self.distanceLabel];
    self.distanceLabel.font = [UIFont systemFontOfSize:13];
    self.distanceLabel.textColor = FANCY_COLOR(@"818181");
    self.distanceLabel.text = @"3 miles away";
    
    self.POIaddress = [[UILabel alloc] initWithFrame:CGRectMake(8, 8+20+2+15+2, rx.size.width-20, 15)];
    [self addSubview: self.POIaddress];
    self.POIaddress.font = [UIFont systemFontOfSize:13];
    self.POIaddress.textColor = FANCY_COLOR(@"818181");
    self.POIaddress.text = @"641 N.HignLand Avenue, los angeles";
 
    
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    self.seperatorLine = [[UIView alloc] initWithFrame:CGRectMake(8, 0, rx.size.width-8, fSeperatorHeight)];
    [self addSubview: self.seperatorLine];
    self.seperatorLine.backgroundColor = FANCY_COLOR(@"cccccc");
    
    
    
    self.selectedIcon = [[UIImageView alloc] initWithFrame:CGRectMake(rx.size.width-40, 29, 65/4, 51/4)];
    self.selectedIcon.image = [UIImage imageNamed:@"cell-selected-icon"];
    [self addSubview:self.selectedIcon];
}


-(void)configCell:(MKMapItem*)item bShowSelectedIcon:(BOOL)showSelectedIcon
{
    if (showSelectedIcon) {
        self.selectedIcon.hidden = NO;
    }
    else{
        self.selectedIcon.hidden = YES;
    }
    
    float distance = [self getDistanceString:item.placemark.coordinate];
    NSString* distanceString = [[HGUtils sharedInstance] getDistanceMiles:distance];
    
    self.POIName.text = item.placemark.name;
    self.distanceLabel.text = distanceString;
    self.POIaddress.text = item.placemark.title;
    
}


-(float)getDistanceString:(CLLocationCoordinate2D)coor2d
{
    float distance = 0;
    
    double userLat = [[HGUtils sharedInstance] getPreferLat];
    double userlon = [[HGUtils sharedInstance] getPreferLon];
    CLLocation* userLoc = [[CLLocation alloc] initWithLatitude:userLat longitude:userlon];
    
    
    CLLocation * itemLoc = [[CLLocation alloc] initWithLatitude:coor2d.latitude longitude:coor2d.longitude];
    if (fabs(coor2d.latitude - 0.0) < 0.000001
        && fabs(coor2d.longitude - 0.0) < 0.000001) {
        distance = -1.0;;
    }
    else if(fabs(userLat - 0.0)< 0.000001
            && fabs(userlon - 0.0) < 0.000001)
    {
        distance = - 1.0;
    }
    else{
        distance = [itemLoc distanceFromLocation:userLoc] * 0.001;
    }
    
    return distance;

}




@end

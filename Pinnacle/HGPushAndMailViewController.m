//
//  HGPushAndMailViewController.m
//  Pinnacle
//
//  Created by Alex on 14-11-3.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGPushAndMailViewController.h"
#import "HGAppData.h"
#import "HGUtils.h"

@interface HGPushAndMailViewController ()

@end

@implementation HGPushAndMailViewController
//todo， 需要做国际化支持
//- (void)viewDidLoad {
//    
//    [[HGUtils sharedInstance] gaSendEventWithCategory:@"setting_view" action:@"set_notifications" label:nil value:nil];
//    [[HGUtils sharedInstance] gaTrackViewName:@"notifications_view"];
//    
//    
//    [super viewDidLoad];
//    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
//    
//    self.navigationItem.title = NSLocalizedString(@"Notifications",nil);
//    
//    [self getRemoteNotificationStatus];
//}
//
//
//
//-(void)getRemoteNotificationStatus
//{
//    
//    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"user_subscriptions/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        
//        NSArray * results = responseObject;
//        NSMutableArray *tempState = [[HGAppData sharedInstance].notificationSwitchStatus mutableCopy];
//        
//        @try {
//            for (int i = 0 ; i< results.count; i++) {
//                NSDictionary * currentRecord = [results objectAtIndex: i];
//                NSString* methodString = [currentRecord valueForKey:@"method"];
//                NSString* typeString = [currentRecord valueForKey:@"type"];
//                
//                NSInteger switchIndex = (methodString.integerValue -1)*3 + typeString.integerValue -1;
//                NSNumber *state = [currentRecord valueForKey:@"state"];
//                
//                [tempState replaceObjectAtIndex:switchIndex withObject:state];
//            }
//            
//            NSLog(@"%d", [[HGAppData sharedInstance].notificationSwitchStatus isEqual: tempState]);
//                        NSLog(@"%d", [[HGAppData sharedInstance].notificationSwitchStatus isEqualToArray: tempState]);
//            [HGAppData sharedInstance].notificationSwitchStatus = tempState;
//        }
//       
//        @catch (NSException *exception) {
//            [Flurry logError: [@"" stringByAppendingFormat:@"%s ,%d, %@", __FILE__,__LINE__,results] message:@"Exception" exception:exception] ;
//            
//            NSLog(@"remove observer exception: %@", exception);
//        }
//        
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"get remote notifications status fail");
//    }];
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 2;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return 3;
//}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
//        [self addSwitch:indexPath withCell:cell];
//    }
//    
//    if (indexPath.section == 0) {
//        switch (indexPath.row) {
//            case 0:
//                cell.textLabel.text = NSLocalizedString(@"New Followers",nil);
//                break;
//            case 1:
//                cell.textLabel.text = NSLocalizedString(@"Recommendations",nil);
//                break;
//            case 2:
//                cell.textLabel.text = NSLocalizedString(@"Marketing Activities",nil);
//                break;
//                
//            default:
//                break;
//        }
//    }
//    else if(indexPath.section == 1)
//    {
//        
//        //////??????????? text bug????
//        switch (indexPath.row) {
//            case 0:
//                cell.textLabel.text = NSLocalizedString(@"New Followers",nil);
//                break;
//            case 1:
//                cell.textLabel.text = NSLocalizedString(@"Recommendations",nil);
//                break;
//            case 2:
//                cell.textLabel.text = NSLocalizedString(@"Marketing Activities",nil);
//                break;
//                
//            default:
//                break;
//        }
//        
//    }
//    
//    return cell;
//}
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"table view cell click");
//    
//}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    if (section == 0) {
//        return NSLocalizedString(@"Push Notificaitons",nil);
//    }
//    if  (section == 1)
//    {
//        return NSLocalizedString(@"Email Notifications",nil);
//    }
//    return @"5miles";
//    
//}
//
//#define SWITCH_X_OFF 240
//#define SWITCH_Y_OFF 7
//#define SWITCH_WITH 60
//#define SWITCH_HEIGHT 30
//
//-(void)addSwitch:(NSIndexPath *)indexPath withCell:(UITableViewCell*)cell;
//{
//    UISwitch* currentSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SWITCH_X_OFF, SWITCH_Y_OFF, SWITCH_WITH, SWITCH_HEIGHT)];
//    long switchIndex = indexPath.section * 3 + indexPath.row;
//    currentSwitch.tag = switchIndex;
//                               
//    BOOL switchStatus = [[[HGAppData sharedInstance].notificationSwitchStatus objectAtIndex:switchIndex] boolValue];
//    [currentSwitch setOn:switchStatus];
//    
//    [cell addSubview: currentSwitch];
//    [currentSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
//    
//}
//
//-(void)switchAction:(id)sender
//{
//    
//    UISwitch *switchButton = (UISwitch*)sender;
//    BOOL isButtonOn = [switchButton isOn];
//    long switchIndex = switchButton.tag;
//    
//    NSNumber *methodNum = [NSNumber numberWithInt:(int)(switchIndex / 3 + 1)];
//    NSNumber *typeNum = [NSNumber numberWithInt:switchIndex % 3 + 1];
//    NSNumber *valueNum = [NSNumber numberWithBool:isButtonOn];
//    
//    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"subscribe/" parameters:@{@"method":methodNum, @"type":typeNum,@"action":valueNum} success:^(NSURLSessionDataTask *task, id responseObject){
//        
//        NSMutableArray *tempState = [[HGAppData sharedInstance].notificationSwitchStatus mutableCopy];
//        [tempState replaceObjectAtIndex:switchIndex withObject:valueNum];
//        [HGAppData sharedInstance].notificationSwitchStatus = tempState;
//
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"get remote notifications status fail");
//    }];
//   
//}
@end

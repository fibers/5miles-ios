//
//  HGFivemilesClient.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-13.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface HGFivemilesClient : AFHTTPSessionManager

@property(nonatomic, strong)AFHTTPRequestSerializer*  serializer;
+ (instancetype)guestClient;
+ (instancetype)userClient;
+ (instancetype)chatClient;
+ (void)resignClientServer;

////////////////////////////////////////////////////
- (BOOL)checkNetworkStatus;
- (NSURLSessionDataTask *)FIVEMILES_GET:(NSString *)URLString
                             parameters:(id)parameters
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionDataTask *)FIVEMILES_POST:(NSString *)URLString
                              parameters:(id)parameters
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;



@end

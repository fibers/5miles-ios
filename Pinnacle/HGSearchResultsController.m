//
//  HGSearchResultsController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSearchResultsController.h"
#import "HGAppData.h"
#import "HGItemDetailController.h"
#import "HGHomeItemCell.h"
#import "HGShopItem.h"
#import "HGUtils.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import <JDStatusBarNotification.h>
#import "SVPullToRefresh.h"
#import <SVProgressHUD.h>
#import "UIStoryboard+Pinnacle.h"
#import "HGStartSystemStrAction.h"
#import "CollectionSingleLayout.h"
#import "HGItemSingleLineCell.h"

@interface HGSearchResultsController () <CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic, strong) NSMutableArray * shopItems;
@property (nonatomic, strong) NSString * nextLink;

@property (nonatomic, strong) UIImageView* emptyIcon;
@property (nonatomic, strong) UILabel* empty_label1;
@property (nonatomic, strong) UILabel* empty_label2;


@property (nonatomic, assign) BOOL bannerOpen;
@property (nonatomic, strong) NSString* bannerActionString;
@property (nonatomic, strong) NSString* bannerImageUrl;



@property (nonatomic, strong) UIButton * navigationRightButton;
@property (nonatomic, strong) UIButton * displayTypeSwitchButton;

@property (nonatomic, assign) BOOL bIsSearchFromFilter;

@property (nonatomic, strong) CollectionSingleLayout* singleLayout;
@property (nonatomic, strong) CHTCollectionViewWaterfallLayout* waterfallLayout;
@end

#define SEARCH_ITEM_COUNT 20

static NSString * const SectionTitleID = @"SectionTitle";
@implementation HGSearchResultsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self _setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setup];
}

- (void)_setup
{
    self.shopItems = [NSMutableArray array];
    self.nextLink = @"";
    self.userNewSearchString = @"";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.httpQuestDict = [[NSMutableDictionary alloc] init];
    [self addFilterView];
    
    if (self.searchMode == HGSearchModeKeyword) {
        [self addNavigationView];
    }
    else{
        [self addRightButton];
    }
    
    
    [self customizeBackButton];
    [self setUpCollectionView];
    [self startSearch];
    [self addDefaultEmptyView];
}

-(void)customizeBackButton
{
    
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
 }
-(void)filterTap:(UITapGestureRecognizer *)gesture
{
    [self.searchTitleView resignFirstResponder];
}

-(void)addGestureRegAction
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(filterTap:)];
    [self.view addGestureRecognizer:tapGesture];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.tabController.tabBar.hidden =YES;
}
-(void)hideEmptyView
{
    self.emptyIcon.hidden = self.empty_label1.hidden = self.empty_label2.hidden = YES;
}
-(void)showEmptyView
{
    self.emptyIcon.hidden = self.empty_label1.hidden = self.empty_label2.hidden = NO;
}
-(void)addDefaultEmptyView
{
    self.emptyIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 226/2, 298/2)];
    self.emptyIcon.image = [UIImage imageNamed:@"searchResult_empty_icon"];
    [self.view insertSubview:self.emptyIcon atIndex:1];
    CGPoint centerpoint = self.view.center;
    centerpoint.y = centerpoint.y - 40;
    self.emptyIcon.center = centerpoint;
   
    self.empty_label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 25)];
     self.empty_label1.font = [UIFont boldSystemFontOfSize:14];
     self.empty_label1.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
     self.empty_label1.text = NSLocalizedString(@"No items",nil);
    [self.view insertSubview: self.empty_label1 atIndex:1];
    centerpoint.y = CGRectGetMaxY(self.emptyIcon.frame) + 11 + 12;
     self.empty_label1.center = centerpoint;
     self.empty_label1.textAlignment = NSTextAlignmentCenter;
    
     self.empty_label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 60)];
    self.empty_label2.font = [UIFont systemFontOfSize:13];
    self.empty_label2.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.empty_label2.text = NSLocalizedString(@"Sorry, we did not find listings near you.\n 5miles is a new and growing marketplace,\n please try other keywords",nil);
    [self.view insertSubview:self.empty_label2 atIndex:1];
    centerpoint.y = CGRectGetMaxY(self.empty_label1.frame)+ 25;
    self.empty_label2.center = centerpoint;
    self.empty_label2.textAlignment = NSTextAlignmentCenter;
    self.empty_label2.numberOfLines = 3;
    
    [self hideEmptyView];
    
    
}


-(void)clearSearchField{
    [self.searchTitleView resignFirstResponder];
}
-(void)StartFilterSearch
{
    {
        if (self.filter.selectedDistanceIndex != Filter_index_user_not_selected) {
            NSNumber* disctanceFilter = [NSNumber numberWithInt:self.filter.selectedDistanceIndex];
            [self.httpQuestDict setObject:disctanceFilter forKey:@"distance"];

        }
        if (self.filter.selectedSortedIndex != Filter_index_user_not_selected) {
            NSNumber* sortFilter = [NSNumber numberWithInt:self.filter.selectedSortedIndex];
            [self.httpQuestDict setObject:sortFilter forKey:@"orderby"];
        }
        else{
            NSNumber* sortFilter = [NSNumber numberWithInt:0];
            [self.httpQuestDict setObject:sortFilter forKey:@"orderby"];
        }
        
        if (self.filter.selectedCategoryIndex != Filter_index_user_not_selected) {
            NSNumber* categoryFilter = [NSNumber numberWithInt:self.filter.selectedCategoryIndex];
            {
                [self.httpQuestDict setObject:categoryFilter forKey:@"cat_id"];
                self.navigationItem.title =self.filter.selectedCategoryString;
            }
        }
        
        NSNumber* userVerify = [NSNumber numberWithInt:self.filter.VerifiedUser];
        [self.httpQuestDict setObject:userVerify forKey:@"user_verifed"];
        
        NSNumber* lowprice =[NSNumber numberWithFloat:self.filter.priceRangelow];
        NSNumber* highprice = [NSNumber numberWithFloat:self.filter.priceRangeHigh];
        
        [self.httpQuestDict setObject:lowprice forKey:@"price_min"];
        [self.httpQuestDict setObject:highprice forKey:@"price_max"];
        [self.httpQuestDict setObject:[[NSNumber alloc] initWithInt:1] forKey:@"refind"];
        
        self.bIsSearchFromFilter = YES;
        [self startSearch];
    }
}
-(void)addFilterView
{
    self.filter = [[HGSearchResultFilter alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, buttonHeight)];
    self.filter.delegate = self;
    
    self.view.userInteractionEnabled=YES;
    self.filter.userInteractionEnabled =YES;
    [self.view addSubview:self.filter];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder]; //软键盘的撤回
    NSLog(@"begin search with text %@", textField.text);
    if (textField.text.length > 0) {
        self.userNewSearchString = textField.text;
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"searchagain" label:nil value:nil];
        [self startSearch];
    }
    return YES;
}
-(void)addRightButton
{
    self.navigationRightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self.navigationRightButton addTarget:self action:@selector(onTouchChangeDisplayType) forControlEvents:UIControlEventTouchUpInside];
    self.navigationRightButton.backgroundColor =[UIColor colorWithPatternImage: [UIImage imageNamed:@"searchFilter_display_type_1"]];
    UIBarButtonItem *followItem = [[UIBarButtonItem alloc] initWithCustomView:self.navigationRightButton];
    [self.navigationItem setRightBarButtonItem:followItem];
    self.navigationItem.rightBarButtonItem = followItem;
}
-(void)addNavigationView
{
    
    
    UIView* barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-40, 30)];
    barView.backgroundColor = [UIColor whiteColor];
    self.searchTitleView = [[HGUITextField alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width -40 - 28-40,28)];
    self.searchTitleView.layer.cornerRadius = 4;
    self.searchTitleView.backgroundColor = FANCY_COLOR(@"ededed");
    self.searchTitleView.font = [UIFont systemFontOfSize:13];
    self.searchTitleView.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.searchTitleView.edgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    self.searchTitleView.delegate = self;
    self.searchTitleView.returnKeyType =UIReturnKeySearch;
    self.searchTitleView.text = self.navigationController.title;
    self.navigationController.title = @"";
    
    
    
    UIImageView* searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7, 13, 13)];
    searchIcon.image = [UIImage imageNamed:@"searchResult_searchIcon"];
    [self.searchTitleView addSubview:searchIcon];
    [barView addSubview:self.searchTitleView];
    
    
    
    
    
    //UIImageView* icon1 = [[UIImageView alloc] initWithFrame:CGRectMake(262+17+5, 5, 34/2, 34/2)];
    //icon1.image =[UIImage imageNamed:@"searchResultBar_icon1"];
    //[barView addSubview:icon1];
    
    
    UIButton * backButton =[[UIButton alloc] initWithFrame:CGRectMake(0, 5, 41/2, 34/2)];
    //[barView addSubview:backButton];
    backButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"naviBack"]];
    [backButton addTarget:self action:@selector(navigationBack) forControlEvents:UIControlEventTouchUpInside];
    
    self.displayTypeSwitchButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.searchTitleView.frame)+20, 0, 30, 30)];
    self.displayTypeSwitchButton.backgroundColor =[UIColor colorWithPatternImage: [UIImage imageNamed:@"searchFilter_display_type_1"]];
    [barView addSubview:self.displayTypeSwitchButton];
    [self.displayTypeSwitchButton addTarget:self action:@selector(onTouchChangeDisplayType) forControlEvents:UIControlEventTouchUpInside];
    

    
    //[self.navigationItem setHidesBackButton:YES animated:YES];
    self.navigationItem.titleView = barView;
    
}
-(void)navigationBack
{
    if (self.searchMode == HGSearchModeKeyword) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"search_back" label:nil value:nil];
    }
    if (self.searchMode == HGSearchModeCategory) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"category_back" label:nil value:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onTouchChangeDisplayType
{
    if (self.singleLayout==nil) {
        self.singleLayout = [[CollectionSingleLayout alloc] init];
        
    }
    
    __weak HGSearchResultsController* weakSelf = self;
    if ([self.collectionView.collectionViewLayout isKindOfClass:[CollectionSingleLayout class]]) {
        
        if (self.displayTypeSwitchButton) {
            //current search type : search key words
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"searchresult_view" action:@"search_switchtowaterfall" label:nil value:nil];
            self.displayTypeSwitchButton.backgroundColor =[UIColor colorWithPatternImage: [UIImage imageNamed:@"searchFilter_display_type_1"]];
        }
        else{
             [[HGUtils sharedInstance] gaSendEventWithCategory:@"categoryresult_view" action:@"category_switchtowaterfall" label:nil value:nil];
            self.navigationRightButton.backgroundColor =[UIColor colorWithPatternImage: [UIImage imageNamed:@"searchFilter_display_type_1"]];
        }
        [self.collectionView setCollectionViewLayout:self.waterfallLayout animated:NO completion:^(BOOL completed){
            [weakSelf.collectionView reloadData];
        }];

    }
    else{
        
        if (self.displayTypeSwitchButton) {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"searchresult_view" action:@"search_switchtolist" label:nil value:nil];
            self.displayTypeSwitchButton.backgroundColor =[UIColor colorWithPatternImage: [UIImage imageNamed:@"searchFilter_display_type_2"]];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"categoryresult_view" action:@"category_switchtolist" label:nil value:nil];
            self.navigationRightButton.backgroundColor =[UIColor colorWithPatternImage: [UIImage imageNamed:@"searchFilter_display_type_2"]];
        }
        [self.collectionView setCollectionViewLayout:self.singleLayout animated:NO completion:^(BOOL completed){
            [weakSelf.collectionView reloadData];
        }];

    }
   
}
-(void)setUpCollectionView
{
    
    //注册headerView Nib的view需要继承UICollectionReusableView
    [self.collectionView registerNib:[UINib nibWithNibName:@"SQSupplementaryView" bundle:nil] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:SectionTitleID];
    // Custom collection view layout
    self.waterfallLayout = [[CHTCollectionViewWaterfallLayout alloc] init];
    self.waterfallLayout.sectionInset = UIEdgeInsetsMake(buttonHeight+5, 5, 5,4 );
    self.waterfallLayout.minimumColumnSpacing = 5;
    self.waterfallLayout.minimumInteritemSpacing = 7.5;
    self.collectionView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    self.collectionView.collectionViewLayout = self.waterfallLayout;

}

-(void)viewDidLayoutSubviews
{
    self.collectionView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.collectionView.frame.size.height);
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

-(NSString*)getC1TitleByID:(int)idstring
{
    switch (idstring) {
        case 1000:
            return NSLocalizedString(@"For sale", nil);
            break;
        case 1001:
            return NSLocalizedString(@"Services", nil);
            break;
        case 1002:
            return NSLocalizedString(@"Housing", nil);
            break;
        case 1003:
            return NSLocalizedString(@"Jobs", nil);
            break;
            
        default:
            return NSLocalizedString(@"For sale", nil);
            break;
    }
}
-(void)getRefindCategories:(NSMutableDictionary*)response
{
    if ([response objectForKey:@"res_ext"] != nil) {
        
        NSMutableDictionary* res_ext = [response objectForKey:@"res_ext"];
        NSArray* c1Array = [res_ext objectForKey:@"refind_category"];
        if (c1Array == nil || c1Array.count==0) {
            return;
        }
        [self.filter clearCategoriesArray];
        NSMutableArray* c1TitleArray  = [[NSMutableArray alloc] init];
        
        
        HGCategory* fakeCategoryAll = [[HGCategory alloc] init];
        fakeCategoryAll.index = -1;
        fakeCategoryAll.title = NSLocalizedString(@"All", nil);
        [c1TitleArray addObject:fakeCategoryAll];
        
        for (int i = 0; i<c1Array.count; i++) {
            NSMutableDictionary* c1Dic = [c1Array objectAtIndex:i];
            HGCategory* c1 = [[HGCategory alloc] init];
            c1.title = [c1Dic objectForKey:@"title"];
            c1.index = ((NSNumber*)[c1Dic objectForKey:@"id"]).intValue;
            
            
            NSString* currentTitle = [self getC1TitleByID:c1.index];
            c1.title = currentTitle;
            [c1TitleArray addObject:c1];
            NSArray* subcategoriesArray = [c1Dic objectForKey:@"child"];
            for (int j=0; j<subcategoriesArray.count; j++) {
                HGCategory* c2= [[HGCategory alloc] init];
                NSDictionary* c2Dic = [subcategoriesArray objectAtIndex:j];
                c2.parent_id = [@"" stringByAppendingFormat:@"%d", c1.index ];
                c2.index = ((NSNumber*)[c2Dic objectForKey:@"id"]).intValue;
                c2.title = [c2Dic objectForKey:@"title"];
                
                
                [self.filter.array2 addObject:c2];
                [self.filter.array2_apply_c1 addObject:c2];
            }
            
          
        }

        
        [self.filter reFillLeftCategoryBoardWithArray:c1TitleArray];
        
    }
}

-(void)startSearch
{
    // Hook search actoin
    NSNumber * lat = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLat]];
    NSNumber * lon = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLon]];
    
    __weak HGSearchResultsController * weakSelf = self;
    [SVProgressHUD show];
    if (self.searchMode == HGSearchModeCategory) {
       
        
        int parentCategoryID = [[HGAppInitData sharedInstance] getParentCategoryID:[self.searchPayload objectForKey:@"catID"]];
        NSString* parentCategoryIDStirng = [@"" stringByAppendingFormat:@"%d", parentCategoryID];
       
        NSMutableDictionary* params =[[NSMutableDictionary alloc] initWithDictionary: @{@"root_cat_id":parentCategoryIDStirng,@"cat_id":[self.searchPayload objectForKey:@"catID"] , @"lat": lat, @"lon": lon, @"limit":[NSNumber numberWithInt:SEARCH_ITEM_COUNT]}];
        
        if (self.bIsSearchFromFilter) {
            [params addEntriesFromDictionary:self.httpQuestDict];
        }
        
       
        
        [[HGUtils sharedInstance] gaTrackViewName:@"categoryresult_view"];
        [FBSDKAppEvents logEvent:[@"category_" stringByAppendingFormat:@"%d", ((NSNumber*)[self.searchPayload objectForKey:@"catID"]).intValue]];
        
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"search/category_new/" parameters: params success:^(NSURLSessionDataTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            
            if (self.rf_tag_from_push_payload != nil && self.rf_tag_from_push_payload.length > 0) {
                [HGTrackingData sharedInstance].searchCategory_rf_tag = self.rf_tag_from_push_payload;
            }
            else{
                [HGTrackingData sharedInstance].searchCategory_rf_tag = [responseObject objectForKey:@"rf_tag"];
            }
            
            
            if (!self.bIsSearchFromFilter) {
                 [self getRefindCategories:responseObject];
            }
            self.bIsSearchFromFilter = NO;
            
            
            [self.shopItems removeAllObjects];
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.shopItems addObject:item];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            
            if (![self.nextLink isEqual:[NSNull null]]) {
                [self.collectionView addInfiniteScrollingWithActionHandler:^{
                    [weakSelf _loadNextPage];
                }];
            }
            if (self.shopItems.count == 0) {
                [self showEmptyView];
            }
            else{
                [self hideEmptyView];
            }
            
            [self.collectionView reloadData];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"get search result items error: %@", error);
        }];
        
    } else if (self.searchMode == HGSearchModeKeyword) {
        [[HGUtils sharedInstance] gaTrackViewName:@"searchresult_view"];
        
        NSString*cid = [self.searchPayload objectForKey:@"cid"];
        //for test : cid =@"1";
        if (cid!=nil && cid.length > 0) {
            [self getBannerInformation:cid];
        }
        
        
        NSString* keyword = [self.searchPayload objectForKey:@"keyword"];
        if (self.userNewSearchString.length > 0) {
            keyword = self.userNewSearchString;
        }
        ///error defense
        if (keyword == nil) {
            keyword = @"";
        }
        
        NSMutableDictionary*  params =[[NSMutableDictionary alloc] initWithDictionary: @{@"q":keyword , @"lat": lat, @"lon": lon,@"limit":[NSNumber numberWithInt:SEARCH_ITEM_COUNT]}];
        
        if (self.bIsSearchFromFilter) {
            [params addEntriesFromDictionary:self.httpQuestDict];
        }
        
        
        self.searchTitleView.text = keyword;
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"search_new/" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            if (self.rf_tag_from_push_payload != nil && self.rf_tag_from_push_payload.length > 0) {
                [HGTrackingData sharedInstance].search_rf_tag = self.rf_tag_from_push_payload;
            }
            else{
                [HGTrackingData sharedInstance].search_rf_tag = [responseObject objectForKey:@"rf_tag"];
            }
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            if (!self.bIsSearchFromFilter) {
                [self getRefindCategories:responseObject];
            }
            self.bIsSearchFromFilter = NO;

            
            [self.shopItems removeAllObjects];
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.shopItems addObject:item];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            
            
            if (![self.nextLink isEqual:[NSNull null]]) {
                [self.collectionView addInfiniteScrollingWithActionHandler:^{
                    [weakSelf _loadNextPage];
                }];
            }
            if (self.shopItems.count == 0) {
                [self showEmptyView];
            }
            else{
                [self hideEmptyView];
            }
            [self.collectionView reloadData];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"get user selling listings error: %@", error);
        }];
    }
    else if(self.searchMode == HGSearchModeBrandID)
    {
        
        NSString* brand_idstring = [self.searchPayload objectForKey:@"brand_keyword"];
        [[HGUtils sharedInstance] gaTrackViewName:@"productbrand_view"];
        NSNumber* brand_number = [[NSNumber alloc] initWithInteger:brand_idstring.integerValue];
        
        NSMutableDictionary* params =[[NSMutableDictionary alloc] initWithDictionary:@{@"brand_id":brand_number , @"lat": lat, @"lon": lon,@"limit":[NSNumber numberWithInt:SEARCH_ITEM_COUNT]}];
        
        if(self.bIsSearchFromFilter)
        {
            [params addEntriesFromDictionary:self.httpQuestDict];
        }
        
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"brand_items/" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];

            if (self.rf_tag_from_push_payload != nil && self.rf_tag_from_push_payload.length > 0) {
                [HGTrackingData sharedInstance].brand_item_rf_tag = self.rf_tag_from_push_payload;
            }
            else{
                [HGTrackingData sharedInstance].brand_item_rf_tag = [responseObject objectForKey:@"rf_tag"] ;
            }
            if (!self.bIsSearchFromFilter) {
                [self getRefindCategories:responseObject];
            }
            self.bIsSearchFromFilter = NO;

            
            
            [self.shopItems removeAllObjects];
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.shopItems addObject:item];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            
            if (![self.nextLink isEqual:[NSNull null]]) {
                [self.collectionView addInfiniteScrollingWithActionHandler:^{
                    [weakSelf _loadNextPage];
                }];
            }
            if (self.shopItems.count == 0) {
                [self showEmptyView];
            }else{
                [self hideEmptyView];
            }
            [self.collectionView reloadData];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"get search result listings error: %@", error);
        }];
    }

}


-(void)getBannerInformation:(NSString*)cidString
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"ad_banner/" parameters: @{@"cid":cidString} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"get banner ad content %@",responseObject);
        self.bannerOpen = YES;
        self.bannerActionString = [responseObject objectForKey:@"click_action"];
        self.bannerImageUrl = [responseObject objectForKey:@"image_url"];
        [self.collectionView reloadData];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get banner ad content fail %@",error);
    }];
    
}
#pragma mark - CollectionView Datasource & Delegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
         if(self.bannerOpen )
         {
             
             return 48 ;
         }
        
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    {
        return self.shopItems.count;
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"the collection kind is %@", kind);
    if([collectionView.collectionViewLayout isKindOfClass:[CollectionSingleLayout class]]) {
        return nil;
    }
    
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
        
        
        if(self.bannerOpen)
       {
            UICollectionReusableView *view =  [collectionView dequeueReusableSupplementaryViewOfKind :kind   withReuseIdentifier:SectionTitleID   forIndexPath:indexPath];
           view.backgroundColor = [UIColor clearColor];
            UIImageView* bannerView  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 48 )];
            
           NSString* imageUrl = self.bannerImageUrl;
            NSLog(@"the home banner url is %@", imageUrl);
            [bannerView sd_setImageWithURL: [NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_home_banner"]];
            
            
            bannerView.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapHomeBanner:)];
            [bannerView addGestureRecognizer:singleTap];
            
            UIButton* bannerClose = [[UIButton alloc] initWithFrame:CGRectMake(view.frame.size.width -30, 12, 25, 25)];
            [bannerView addSubview:bannerClose];
            [bannerClose setImage:[UIImage imageNamed:@"close-button"] forState:UIControlStateNormal];
            [bannerClose addTarget:self action:@selector(closeBanner) forControlEvents:UIControlEventTouchUpInside];
            
            
            [view addSubview:bannerView];
            return view;;
            
        }
    }
    return nil;
}
-(void)closeBanner
{
    self.bannerOpen = FALSE;
    [self.collectionView reloadData];
}

-(void)handleTapHomeBanner:(id)sender
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"banneroff" label:nil value:nil];
    NSString* actionUrl = self.bannerActionString;
    [[HGStartSystemStrAction sharedInstance] startHomeBannerAction:actionUrl withController:self withActionSource:SYSTEM_ACTION_SOURCE_UNDEFINE];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView.collectionViewLayout isKindOfClass:[CHTCollectionViewWaterfallLayout class]]) {
        HGHomeItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShopItemCell" forIndexPath:indexPath];
        
        HGShopItem * item = [self.shopItems objectAtIndex:indexPath.row];
        [cell configWithItem:item];
        
        return cell;
 
    }
    else{
        HGItemSingleLineCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ItemSingleLineCell" forIndexPath:indexPath];
        
        HGShopItem * item = [self.shopItems objectAtIndex:indexPath.row];
        [cell configWithItem:item];
        
        return cell;
    }
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
     HGItemDetailController *vcItemDetail = (HGItemDetailController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCItemDetail"];
    

    HGShopItem * item = [self.shopItems objectAtIndex:indexPath.row];
    
   
    vcItemDetail.item = item;
    if(self.rf_tag_from_push_payload && self.rf_tag_from_push_payload.length > 0)
    {
        /// push rf_tag has higher priority. 
        vcItemDetail.trackstring = self.rf_tag_from_push_payload;
    }
    else
    {
        vcItemDetail.trackstring = item.rf_tag;
    }
    [self.navigationController pushViewController:vcItemDetail animated:YES];
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGShopItem * item = [self.shopItems objectAtIndex:indexPath.row];
    HGItemImage * coverImage = [item.images firstObject];
    
    ///error defense: width can not be 0
    if(coverImage.width == 0 || coverImage.height==0)
    {
        return CGSizeMake(140, 140 + WATER_FLOW_CELL_EXTEND_HEIGHT);
    }
    else{
        return CGSizeMake(140, ceil(coverImage.height * 140 / coverImage.width) + WATER_FLOW_CELL_EXTEND_HEIGHT);
    }
}

#pragma mark - Helpers

- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.collectionView.infiniteScrollingView stopAnimating];
        self.collectionView.showsInfiniteScrolling = NO;
    } else {
        NSNumber * lat = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLat]];
        NSNumber * lon = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLon]];
        
        if (self.searchMode == HGSearchModeCategory) {
            
            NSMutableDictionary* params = [[NSMutableDictionary alloc ] initWithDictionary: @{@"cat_id": [self.searchPayload objectForKey:@"catID"], @"lat": lat, @"lon": lon,@"limit":[NSNumber numberWithInt:SEARCH_ITEM_COUNT]}];
            [params addEntriesFromDictionary:self.httpQuestDict];
            
            [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[NSString stringWithFormat:@"search/category/%@", self.nextLink] parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
                self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
                
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"category_loadmore" label:nil value:nil];
                NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
                for (id dictObj in results) {
                    NSError * error = nil;
                    HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                    if (!error) {
                        [self.shopItems addObject:item];
                    } else {
                        NSLog(@"shop item creation error: %@", error);
                    }
                }
                
                [self.collectionView reloadData];
                [self.collectionView.infiniteScrollingView stopAnimating];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                [self.collectionView.infiniteScrollingView stopAnimating];
                NSLog(@"get home items failed: %@", error.localizedDescription);
            }];
        } else  if (self.searchMode == HGSearchModeKeyword)
        {
            NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithDictionary: @{@"q": [self.searchPayload objectForKey:@"keyword"], @"lat": lat, @"lon": lon,@"limit":[NSNumber numberWithInt:SEARCH_ITEM_COUNT]}];
            [params addEntriesFromDictionary:self.httpQuestDict];
            
            [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[NSString stringWithFormat:@"search/%@", self.nextLink] parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
                self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"search_loadmore" label:nil value:nil];
                NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
                for (id dictObj in results) {
                    NSError * error = nil;
                    HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                    if (!error) {
                        [self.shopItems addObject:item];
                    } else {
                        NSLog(@"shop item creation error: %@", error);
                    }
                }
                
                [self.collectionView reloadData];
                [self.collectionView.infiniteScrollingView stopAnimating];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                [self.collectionView.infiniteScrollingView stopAnimating];
                NSLog(@"get home items failed: %@", error.localizedDescription);
            }];
        }
        else if(self.searchMode == HGSearchModeBrandID)
        {
            
            NSString* brand_idstring = [self.searchPayload objectForKey:@"brand_keyword"];
            NSNumber* brand_number = [[NSNumber alloc] initWithInteger:brand_idstring.integerValue];
            
            NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithDictionary:@{@"brand_id":brand_number , @"lat": lat, @"lon": lon,@"limit":[NSNumber numberWithInt:SEARCH_ITEM_COUNT]}];
            [params addEntriesFromDictionary:self.httpQuestDict];
            
            [[HGAppData sharedInstance].userApiClient FIVEMILES_GET: [NSString stringWithFormat:@"brand_items/%@", self.nextLink] parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"discover_view" action:@"brand_loadmore" label:nil value:nil];
                
                self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
                NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
                
               
                for (id dictObj in results) {
                    NSError * error = nil;
                    HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                    if (!error) {
                        [self.shopItems addObject:item];
                    } else {
                        NSLog(@"shop item creation error: %@", error);
                    }
                }
                
                [self.collectionView.infiniteScrollingView stopAnimating];
                [self.collectionView reloadData];
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                [self.collectionView.infiniteScrollingView stopAnimating];
                NSLog(@"get search result items error: %@", error);
            }];
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    //内容（滚动视图）高度大于一定数值时
    if (textView.contentSize.height > 28 )
    {
        //删除最后一行的第一个字符，以便减少一行。
        //textView.text = [textView.text substringToIndex:[textView.text length]-1];
        return NO;
    }
    if([text isEqualToString:@"\n"])
    {
        return NO;
    }
    return YES;
}


#pragma mark - UIScrollView Delegate
static float _yCord = 0;

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.searchTitleView resignFirstResponder];
    
    NSLog(@"%f, %f", scrollView.contentOffset.y , _yCord);
    
    if(scrollView.contentOffset.y <= 0){
        [UIView animateWithDuration:0.5 animations:^{
            self.filter.frame = CGRectMake(0, 0, self.filter.frame.size.width, self.filter.frame.size.height);
        } completion:^(BOOL finished) {
           
        }];
    }else{
        if (scrollView.contentOffset.y <= _yCord) {
            [UIView animateWithDuration:0.5 animations:^{
                self.filter.frame = CGRectMake(0, 0, self.filter.frame.size.width, self.filter.frame.size.height);
                
            } completion:^(BOOL finished) {
                
            }];

        } else {
            [UIView animateWithDuration:0.5 animations:^{
                self.filter.frame = CGRectMake(0, -64, self.filter.frame.size.width, self.filter.frame.size.height);
            } completion:^(BOOL finished) {
                
            }];

        }
    }
    _yCord = scrollView.contentOffset.y;
}

@end

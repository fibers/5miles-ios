//
//  HGRoundCornerView.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-16.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGRoundCornerView.h"

@implementation HGRoundCornerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self _setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setup];
}

- (void)_setup
{
    self.layer.cornerRadius = 2;
    self.clipsToBounds = YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

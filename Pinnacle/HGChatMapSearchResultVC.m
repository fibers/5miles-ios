//
//  HGChatMapSearchResultVC.m
//  Pinnacle
//
//  Created by Alex on 15/5/7.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatMapSearchResultVC.h"
#import "HGUserData.h"
#import "HGAppData.h"
#import "HGChatMapPOICell.h"

@interface HGChatMapSearchResultVC ()
@property (nonatomic, strong) UISearchBar* searchBar;
@property (nonatomic, strong)NSMutableArray* dataArray;
@property (nonatomic, assign)BOOL bHasStartSearch;



@property (nonatomic, strong)UIView* barView;

@property (nonatomic, strong)UIImageView* emptyIcon;
@property (nonatomic, strong)UILabel* empty_label1;
@end

@implementation HGChatMapSearchResultVC

-(void)customSearchBar:(UIView*)superView;
{
    // Do any additional setup after loading the view.
    if (self.searchBar==nil) {
        self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 40)];
        //temp solution , cover the border of searchbar, if we find better solution later...
        UIView* tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 5)];
        tempView.backgroundColor = [UIColor whiteColor];
        
        UIView* tempView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 35, self.view.frame.size.width, 10)];
        tempView2.backgroundColor = [UIColor whiteColor];
        
        [superView addSubview:self.searchBar];
        
        [superView addSubview:tempView];
        [superView addSubview:tempView2];
    }
    
    
    self.searchBar.barStyle = UIBarStyleDefault;
    self.searchBar.placeholder = NSLocalizedString(@"Search or enter an address", nil);
    self.searchBar.delegate = self;
    self.searchBar.tintColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.searchBar.barTintColor = [UIColor whiteColor]; //FANCY_COLOR(@"f0f0f0");
    [self.searchBar becomeFirstResponder];
    self.searchBar.showsCancelButton = YES;
    
    for (UIView *subView in self.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            NSLog(@"the subview : %@", secondLevelSubview);
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                searchBarTextField.backgroundColor = FANCY_COLOR(@"ededed");
            }
            
        }
    }
    
  
    

}

-(void)addNavigationView
{
    
    if (self.barView == nil) {
        self.barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
        
       
        
    }
    self.navigationController.title = @"";
    [self customSearchBar:self.barView];
    
    [self.navigationController.navigationBar addSubview:self.barView];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        self.navigationItem.hidesBackButton = YES;
    }
    self.barView.hidden = NO;
   

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
     [self.barView removeFromSuperview];
    self.barView.hidden = YES;
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.bHasStartSearch = NO;
   

}

-(void)addTableView
{
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [[NSMutableArray alloc] init];
    [self addTableView];
    
    
    [self addDefaultEmptyView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    [self addNavigationView];
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.bHasStartSearch) {
        return 70;
    }
    else{
        return 44;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    int count =0;
    
    if (self.bHasStartSearch) {
        count = (int)self.dataArray.count;
        
    }
    else{
        count = (int)[HGUserData sharedInstance].userLocalMapSearchPOIs.count;
    }
    
    
    if(count==0 && self.bHasStartSearch)
    {
        [self showEmptyView];
        self.tableView.hidden = YES;
    }
    else{
        [self hideEmptyView];
        self.tableView.hidden = NO;
    }
    return count;
}

static int fakeTextLaleltag = 2424;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.bHasStartSearch)
    {
        HGChatMapPOICell *cell = [tableView dequeueReusableCellWithIdentifier:@"chatMapPOICell" forIndexPath:indexPath];
        
        MKMapItem* mapitem = [self.dataArray objectAtIndex:indexPath.row];
        [cell configCell:mapitem bShowSelectedIcon:NO];
        return cell;

    }
    else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchResultVCCell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"searchResultVCCell"];
            
            
            UILabel* fakeTextLabel = [[UILabel alloc] initWithFrame:cell.textLabel.frame];
            fakeTextLabel.font = [UIFont systemFontOfSize:15];
            fakeTextLabel.textColor = FANCY_COLOR(@"242424");
            fakeTextLabel.tag = fakeTextLaleltag;
            [cell addSubview:fakeTextLabel];
            
            UIView* sepertorLine = [[UIView alloc] initWithFrame:CGRectMake(8, cell.frame.size.height-0.5, self.view.frame.size.width-8, 0.5)];
            sepertorLine.backgroundColor = FANCY_COLOR(@"cccccc");
            [cell addSubview:sepertorLine];
        }
        
        NSString* keyString = [[HGUserData sharedInstance].userLocalMapSearchPOIs objectAtIndex:indexPath.row];
       
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textColor = FANCY_COLOR(@"242424");
        cell.textLabel.text = keyString;
        
        
        
        return cell;
    }
 
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.bHasStartSearch)
    {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"selectlocation_view" action:@"locationhistory" label:nil value:nil];
        NSString* keyString = [[HGUserData sharedInstance].userLocalMapSearchPOIs objectAtIndex:indexPath.row];
        self.searchBar.text = keyString;
        
        double lat= [[HGUtils sharedInstance] getPreferLat];
        double lon= [[HGUtils sharedInstance] getPreferLon];
        
        CLLocationCoordinate2D startPt = (CLLocationCoordinate2D){lat, lon};
        [self getLocalPOI:startPt withKeyString:keyString];
        [self.searchBar resignFirstResponder];
        [HGAppData sharedInstance].chatMapSearchKeyString = keyString;
        
        for (UIView *view in self.searchBar.subviews)
        {
            for (id subview in view.subviews)
            {
                if ( [subview isKindOfClass:[UIButton class]] )
                {
                    [subview setEnabled:YES];
                    NSLog(@"enableCancelButton");
                    //return;
                }
            }
        }
        //[self.navigationController popViewControllerAnimated:YES];
    }
    else{
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"selectlocation_view" action:@"select_searchlocation" label:nil value:nil];
        
        MKMapItem* mapitem = [self.dataArray objectAtIndex:indexPath.row];
        [HGAppData sharedInstance].chatMapUserSelectAddrLat = [@"" stringByAppendingFormat:@"%f",  mapitem.placemark.coordinate.latitude];
        [HGAppData sharedInstance].chatMapUserSelectAddrLon = [@"" stringByAppendingFormat:@"%f", mapitem.placemark.coordinate.longitude];
        [HGAppData sharedInstance].chatMapUserSelectSearchResultItem = mapitem;
        [self.navigationController popViewControllerAnimated:YES];
    }
   
}


#pragma mark - SearchBar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"selectlocation_view" action:@"searchlocation" label:nil value:nil];

    //todo: alex.
    if (searchBar.text.length > 0) {
        NSString* keyString = searchBar.text;
        
        
        //ugly way.
        NSMutableArray* tempArray = [[NSMutableArray alloc] initWithArray:[HGUserData sharedInstance].userLocalMapSearchPOIs];
        
        
        BOOL bFound = NO;
        for (int i = 0;i<tempArray.count;i++) {
            NSString* string = [tempArray objectAtIndex:i];
            if([string isEqualToString:keyString]) {
                bFound = YES;
            }
        }
        if (!bFound) {
            [tempArray addObject:keyString];
            [HGUserData sharedInstance].userLocalMapSearchPOIs = tempArray;
            
            /////report this key string to our server
            [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"search_poi/" parameters:@{ @"p":keyString} success:^(NSURLSessionDataTask *task, id responseObject) {
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
            }];
            
            //[self.tableView reloadData];
        }
        
        [HGAppData sharedInstance].chatMapSearchKeyString = keyString;
     
    }
    
    
    double lat= [[HGUtils sharedInstance] getPreferLat];
    double lon= [[HGUtils sharedInstance] getPreferLon];
    
    CLLocationCoordinate2D startPt = (CLLocationCoordinate2D){lat, lon};
    [self getLocalPOI:startPt withKeyString:searchBar.text];
    
    [searchBar resignFirstResponder];
    for (UIView *view in searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                NSLog(@"enableCancelButton");
                //return;
            }
        }
    }
    
    
  }

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"selectlocation_view" action:@"searchlocation_cancel" label:nil value:nil];
    searchBar.text = @"";
    searchBar.showsCancelButton = YES;
    [searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getLocalPOI:(CLLocationCoordinate2D)currentLocationCoordinate withKeyString:(NSString*)keyString
{
    MKCoordinateSpan span = {0.005,0.005};
    MKCoordinateRegion region = {currentLocationCoordinate,span};
    
    MKLocalSearchRequest* localSearchRequest = [[MKLocalSearchRequest alloc] init] ;
    localSearchRequest.region = region;
    localSearchRequest.naturalLanguageQuery = keyString;
    
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:localSearchRequest];
    
    
    self.bHasStartSearch = YES;
    [self.dataArray removeAllObjects];
    
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        NSLog(@"the response's count is:%lu",(unsigned long)response.mapItems.count);        //经测试，这里每次最多只有10条记录
        
        if (error) {
            [self.tableView reloadData];
            NSLog(@"error info is：%@",error);
        }else{
            [self.dataArray removeAllObjects];
            for (MKMapItem *mapItem in response.mapItems) {
                MKPointAnnotation * annotation = [[MKPointAnnotation alloc] init];
                annotation.coordinate = mapItem.placemark.location.coordinate;
                annotation.title = mapItem.name;
               
                
                
                [self.dataArray addObject:mapItem];
                NSLog(@"the title POI is %@", mapItem.name);
                NSLog(@"the place mark is %@", mapItem.placemark);
                NSLog(@"the mapItem is %@", mapItem);
                
            }
            [self.tableView reloadData];
        }
        
        
    }];
}




-(void)hideEmptyView
{
    self.emptyIcon.hidden = self.empty_label1.hidden = YES;
}
-(void)showEmptyView
{
    self.emptyIcon.hidden = self.empty_label1.hidden = NO;
}
-(void)addDefaultEmptyView
{
    
    self.emptyIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 226/2, 298/2)];
    self.emptyIcon.image = [UIImage imageNamed:@"searchResult_empty_icon"];
    [self.view insertSubview:self.emptyIcon atIndex:1];
    CGPoint centerpoint = self.view.center;
    centerpoint.y = centerpoint.y - 40;
    self.emptyIcon.center = centerpoint;
    
    self.empty_label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 25)];
    self.empty_label1.font = [UIFont boldSystemFontOfSize:14];
    self.empty_label1.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.empty_label1.text = NSLocalizedString(@"No location found",nil);
    [self.view insertSubview: self.empty_label1 atIndex:1];
    centerpoint.y = CGRectGetMaxY(self.emptyIcon.frame) + 11 + 12;
    self.empty_label1.center = centerpoint;
    self.empty_label1.textAlignment = NSTextAlignmentCenter;
    
    [self hideEmptyView];
    
    
}
@end

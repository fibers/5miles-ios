//
//  HGSignInEmailController.m
//  Pinnacle
//
//  Created by Maya Game on 14-9-5.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGSignInEmailController.h"
#import "HGAppData.h"
#import "HGAppDelegate.h"
#import "HGUtils.h"
#import "HGConstant.h"
#import "NSString+EmailAddresses.h"
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <SVProgressHUD.h>
#import "HGEmailSuggestTextField.h"
#import "HGForgetPwdViewController.h"
#import "UIStoryboard+Pinnacle.h"

#define TAG_TF_PASSWORD 1001

@interface HGSignInEmailController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgCancel;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet HGEmailSuggestTextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignin;
@property (weak, nonatomic) IBOutlet UILabel *lbForgotPassword;

@property (nonatomic, assign) CGFloat yKeyboardShow;
@property (nonatomic, strong) UITapGestureRecognizer *gestTapOutOfKeyboard;

@end

@implementation HGSignInEmailController

#pragma mark Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.yKeyboardShow = self.view.frame.origin.y + Y_OFFSET_KEYBOARD_SHOW;
    
    self.imgCancel.image = [UIImage imageNamed: @"cancel.png"];
    
    UITapGestureRecognizer *cancelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapCancel:)];
    self.imgCancel.userInteractionEnabled = YES;
    [self.imgCancel addGestureRecognizer:cancelGesture];
    
    self.imgLogo.image = [UIImage imageNamed: @"sign-logo.png"];
    
    self.lbTitle.text = NSLocalizedString(@"Your Mobile Marketplace", nil);
    self.lbTitle.font = [UIFont fontWithName:@"Museo-700" size:18];
    self.lbTitle.textColor = FANCY_COLOR(@"818181");
    self.lbTitle.textAlignment = NSTextAlignmentCenter;

    NSString *domainFilePath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"email_suggest_domain.plist"];
    NSArray *domainList = [NSArray arrayWithContentsOfFile:domainFilePath];
    self.tfEmail.preferredDomainList = domainList;
    self.tfEmail.delegate = self;
    self.tfEmail.font = [UIFont systemFontOfSize:15];
    self.tfEmail.backgroundColor = FANCY_COLOR(@"f0f0f0");
    self.tfEmail.layer.cornerRadius = 3.0f;
    [self.tfEmail setPlaceholder: NSLocalizedString(@"Email address", nil)];
    [[HGUtils sharedInstance] addHeadPadding: 54.0 forTextField: self.tfEmail];
    
    UIImageView *iconEmail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, self.tfEmail.bounds.size.height)];
    iconEmail.image = [UIImage imageNamed:@"icon-signup-email"];
    iconEmail.contentMode = UIViewContentModeCenter;
    [self.tfEmail.leftView addSubview:iconEmail];

    UIView *separatorEmail = [[UIView alloc] initWithFrame:CGRectMake(iconEmail.bounds.size.width, 0, 1, self.tfEmail.bounds.size.height)];
    separatorEmail.backgroundColor = [UIColor whiteColor];
    [self.tfEmail.leftView addSubview:separatorEmail];
    
    self.tfPassword.delegate = self;
    self.tfPassword.tag = TAG_TF_PASSWORD;
    self.tfPassword.font = [UIFont systemFontOfSize:15];
    self.tfPassword.backgroundColor = FANCY_COLOR(@"f0f0f0");
    self.tfPassword.layer.cornerRadius = 3.0f;
    [self.tfPassword setPlaceholder:NSLocalizedString(@"Password (6~16 characters)", nil)];
    [[HGUtils sharedInstance] addHeadPadding: 54.0 forTextField: self.tfPassword];
    
    UIImageView *iconPassword = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, self.tfPassword.bounds.size.height)];
    iconPassword.image = [UIImage imageNamed:@"icon-signup-password"];
    iconPassword.contentMode = UIViewContentModeCenter;
    [self.tfPassword.leftView addSubview:iconPassword];
    
    UIView *separatorPassword = [[UIView alloc] initWithFrame:CGRectMake(iconPassword.bounds.size.width, 0, 1, self.tfPassword.bounds.size.height)];
    separatorPassword.backgroundColor = [UIColor whiteColor];
    [self.tfPassword.leftView addSubview:separatorPassword];

    [self.btnSignin setTitle:NSLocalizedString(@"Sign in", nil) forState:UIControlStateNormal];
    self.btnSignin.backgroundColor = FANCY_COLOR(@"ff8830");
    [self.btnSignin setTitleColor:FANCY_COLOR(@"ffffff") forState:UIControlStateNormal];
    self.btnSignin.layer.cornerRadius = 3.0;
    self.btnSignin.titleLabel.font = [UIFont systemFontOfSize:18];
    
    self.lbForgotPassword.font = [UIFont systemFontOfSize:13];
    self.lbForgotPassword.textAlignment = NSTextAlignmentRight;
    self.lbForgotPassword.text = NSLocalizedString(@"Forgot Password?", nil);
    
    UITapGestureRecognizer *forgotGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapForgotPassword:)];
    self.lbForgotPassword.userInteractionEnabled = YES;
    [self.lbForgotPassword addGestureRecognizer:forgotGesture];

}

- (void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
    [super viewWillDisappear:animated];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:NULL];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"signin_view"];
    [[AppsFlyerTracker sharedTracker] trackEvent:@"signin_confirm" withValue:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTouchButtonSignin:(id)sender {
    [self _doLogin];
}

#pragma mark - UITextfield Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.tfPassword) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _doLogin];
        });
    } else {
        [self.tfPassword becomeFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(textField.tag == TAG_TF_PASSWORD){
        return newString.length > 16 ? NO:YES;
    }
    
    return YES;
}


#pragma mark - Helpers

- (void) keyboardWillShow: (NSNotification *)notification{
    
    if (IPHONE4X) {
        [UIView animateWithDuration:0.3f animations:^{
            CGSize viewSize = self.view.bounds.size;
            self.view.frame = CGRectMake(0, -120, viewSize.width, viewSize.height);
        }];
    } else {
        if( self.view.frame.origin.y > self.yKeyboardShow){
            [UIView animateWithDuration:0.3f animations:^{
                CGRect rect = self.view.frame;
                rect.origin.y = self.yKeyboardShow;
                self.view.frame = rect;
            }];
        }
    }
    
    if(IPHONE4X){
        self.imgCancel.hidden = YES;
        self.imgLogo.hidden = YES;
        self.lbTitle.hidden = YES;
    }
    
    self.gestTapOutOfKeyboard = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapOutOfKeyboard:)];
    [self.view addGestureRecognizer:self.gestTapOutOfKeyboard];
   
}

- (void) keyboardWillHide: (NSNotification *)notification{
    
    if (IPHONE4X) {
        [UIView animateWithDuration:0.3f animations:^{
            CGSize viewSize = self.view.bounds.size;
            self.view.frame = CGRectMake(0, 0, viewSize.width, viewSize.height);
        }];
    } else {
        if( self.view.frame.origin.y == self.yKeyboardShow){
            [UIView animateWithDuration:0.3f animations:^{
                CGRect rect = self.view.frame;
                rect.origin.y = self.yKeyboardShow - Y_OFFSET_KEYBOARD_SHOW;
                self.view.frame = rect;
            }];
        }
    }
    
    self.imgCancel.hidden = NO;
    self.imgLogo.hidden = NO;
    self.lbTitle.hidden = NO;

    [self.view removeGestureRecognizer:self.gestTapOutOfKeyboard];
}


- (void) onTapOutOfKeyboard: (UITapGestureRecognizer *)gesture{

    [self.view endEditing:YES];
}


- (void) onTapCancel: (UITapGestureRecognizer* )gesture{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) tapForgotPassword: (UITapGestureRecognizer* )gesture{
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"signin_view" action:@"forgotpassword" label:nil value:nil];
    [self performSegueWithIdentifier:@"SegueToForgetPassword" sender:self];
}


- (void)_doLogin
{
    if (![[HGAppData sharedInstance].guestApiClient checkNetworkStatus]) {
        return;
    }
    
    NSString * email = self.tfEmail.text;
    NSString * password = self.tfPassword.text;
    
    if (![email isValidEmailAddress]) {
              PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"This email doesn't seem to work.", nil) cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            [self.tfEmail becomeFirstResponder];
        }];
        [view useDefaultIOS7Style];
        return;
    }
    
    if (password.length < 6 || password.length > 16) {
        PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"Your password should contain 6~16 characters.", nil) cancelTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            [self.tfPassword becomeFirstResponder];
        }];
        [view useDefaultIOS7Style];
        return;
    }
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"signin_view" action:@"signin_confirm" label:nil value:nil];
    email = [email stringByCorrectingEmailTypos];
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:email forKey:@"email"];
    [params setObject:password forKey:@"password"];
    [params setObject:[[HGUtils sharedInstance] advertisingIdentifier] forKey:@"appsflyer_user_id"];
    
    [params setObject:[NSNumber numberWithDouble:[HGAppData sharedInstance].GPSlat] forKey:@"lat"];
    [params setObject:[NSNumber numberWithDouble:[HGAppData sharedInstance].GPSlon] forKey:@"lon"];
    
    [params setObject:DEVICE_UUID forKey:@"device_id"];
    [params setObject:@"" forKey:@"anonymous_user_id"];
    
    [SVProgressHUD show];
    [[HGAppData sharedInstance].guestApiClient FIVEMILES_POST:@"login_email/" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        [[HGUserData sharedInstance] updateUserLoggedInfo:responseObject withAnonymousSignup:NO withAccountType:HGAccountTypeEmail];
        
#ifndef DEBUG
        [[HGAppData sharedInstance].chatApiClient FIVEMILES_POST:@"register/" parameters:@{@"device_id":    [HGAppData sharedInstance].deviceToken} success:nil failure:nil];
#endif
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"email login failed: %@", [error.userInfo objectForKey:@"body"]);
    }];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"SegueToForgetPassword"]) {

        HGForgetPwdViewController * vcDest = segue.destinationViewController;
        vcDest.suggestUserEmail = self.tfEmail.text;
    }
    
}


@end

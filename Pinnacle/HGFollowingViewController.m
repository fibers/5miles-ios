//
//  HGFollowingViewController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGFollowingViewController.h"
#import "HGFollowingCell.h"
#import "HGUtils.h"
#import "HGAppDelegate.h"
#import "HGUserViewController.h"
#import "HGAppData.h"
#import "UIStoryboard+Pinnacle.h"
#import <SVProgressHUD.h>
#import "HGGoodSellerViewController.h"
@interface HGFollowingViewController ()


@property(nonatomic, strong)UIView* emptyViewBoard;
@property(nonatomic, assign)BOOL isMyFollowing;
@property(nonatomic, assign)BOOL bHasUserActionBeforeReturn;

@end

@implementation HGFollowingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)commonSetup
{
    [super commonSetup];
    self.endpoint = @"user_following/";

}

- (void)viewDidLoad
{
    self.bHasUserActionBeforeReturn = NO;
    self.params = @{@"user_id": self.userID,@"limit":[[NSNumber alloc] initWithInt:15]};

    [super viewDidLoad];
    
    if ([self.userID isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        self.isMyFollowing = TRUE;
    }
    else
    {
        self.isMyFollowing = FALSE;
    }
    
    self.navigationItem.title = NSLocalizedString(@"Following", nil);
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    self.emptyViewBoard = [[UIView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:self.emptyViewBoard];
    self.emptyViewBoard.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}




- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   self.tabBarController.tabBar.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addOtherDefaultEmptyView
{
    UIImageView * emptyDefault = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 600/4, 313/4)];
   
    [self.view addSubview:emptyDefault];
    
    
    int iconYoffset = 144;
    int lineOffset = 10;
    
    
    int iphone4_Y_offset_config_Value = 0;
    if (SCREEN_HEIGHT <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    CGPoint iconCenter  = CGPointMake(SCREEN_WIDTH/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [emptyDefault addSubview:icon];
    
    CGPoint centerPoint = CGPointMake(SCREEN_WIDTH/2, CGRectGetMaxY(icon.frame)+ lineOffset + 60/2);
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 60)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"No following right now", nil);
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [emptyDefault addSubview:textLabel];

}
-(void)addDefaultEmptyView:(UIView*)emptyViewBoard
{
    int iconYoffset = 144;
    int lineOffset = 10;
   

    CGRect rx = [ UIScreen mainScreen ].bounds;
    int iphone4_Y_offset_config_Value = 0;
    if (rx.size.height <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    
    CGPoint iconCenter  = CGPointMake(rx.size.width/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [emptyViewBoard addSubview:icon];
    
    CGPoint centerPoint = CGPointMake(rx.size.width/2, CGRectGetMaxY(icon.frame)+ lineOffset + 60/2);
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 60)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"Follow your first 5miler to discover interesting people around you! Go to home page and follow them now.", nil);
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [emptyViewBoard addSubview:textLabel];
    
    CGPoint buttonCenter = CGPointMake(rx.size.width/2, CGRectGetMaxY(textLabel.frame)+lineOffset+ 36/2);
    UIButton* button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 200, 36)];
    button.layer.cornerRadius = 4.0;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = DEFAULT_EMPTY_BOARDER_COLOR;
    UILabel* buttonTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    buttonTitle.textAlignment = NSTextAlignmentCenter;
    buttonTitle.text = NSLocalizedString(@"Go exploring!", nil);
    buttonTitle.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [button addSubview:buttonTitle];
     button.center = buttonCenter;
    [emptyViewBoard addSubview:button];
    [button addTarget:self action:@selector(defaultEmptyAction) forControlEvents:UIControlEventTouchUpInside];
}
-(void)defaultEmptyAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowing_view" action:@"myfollowing_empty" label:nil value:nil];
    UIStoryboard * storyBoard = [UIStoryboard mainStoryboard];
    HGGoodSellerViewController * goodSeller = [storyBoard instantiateViewControllerWithIdentifier:@"goodSellerView"];
    goodSeller.leftNavigationType = 0;
    [self.navigationController pushViewController:goodSeller animated:YES];
    

}
#pragma mark - Overrides

- (void)fillObjectsWithServerDataset:(NSArray *)dataset
{
    for (NSDictionary * dictObj in dataset) {
        NSError * error;
        HGUser * user = [[HGUser alloc] initWithDictionary:dictObj error:&error];
        if (nil == error) {
            [self.objects addObject:user];
        }
    }
    if (self.objects.count == 0 && [self.userID isEqualToString:[HGUserData sharedInstance].fmUserID]) {
        self.emptyViewBoard.hidden = NO;
        [self addDefaultEmptyView:self.emptyViewBoard];
    }
    else if(self.objects.count == 0)
    {
        [self addOtherDefaultEmptyView];
    }
    else{
        self.emptyViewBoard.hidden = YES;
    }
}




#pragma mark - UITableView Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGFollowingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FollowingCell" forIndexPath:indexPath];
    cell.delegate = self;
    HGUser * user = [self.objects objectAtIndex:indexPath.row];
    [cell configWithObject:user withIsMyFollowing:self.isMyFollowing];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.bHasUserActionBeforeReturn = YES;
    if (self.isMyFollowing) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowing_view" action:@"myfollowingperson" label:nil value:nil];
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowing_view" action:@"sellerfollowingperson" label:nil value:nil];
    }
    HGUser * user = [self.objects objectAtIndex:indexPath.row];
    
    HGUserViewController *vcUser = (HGUserViewController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCUser"];
    vcUser.user = user;
    
    [self.navigationController pushViewController:vcUser animated:YES];

}

#pragma makr -  HGFollowing delegate
-(void)onTouchUnfollowing:(NSString *)uidString
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"unfollow/" parameters:@{@"user_id": uidString} success:^(NSURLSessionDataTask *task, id responseObject) {
       
        if (self.isMyFollowing) {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowing_view" action:@"myfollowing_unfollow" label:nil value:nil];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowing_view" action:@"sellerfollowing_unfollow" label:nil value:nil];
        }
        [self getServerData];
        [self.tableView reloadData];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
       
    }];
}

-(void)onTouchFollowing:(NSString *)uidString
{
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"follow/" parameters:@{@"user_id": uidString} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if (self.isMyFollowing) {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowing_view" action:@"myfollowing_follow" label:nil value:nil];
        }
        else{
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellerfollowing_view" action:@"sellerfollowing_follow" label:nil value:nil];
        }
        [self getServerData];
        [self.tableView reloadData];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.isMyFollowing && !self.bHasUserActionBeforeReturn) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myfollowing_view" action:@"myfollowing_back" label:nil value:nil];
    }
}

@end

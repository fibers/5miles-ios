//
//  HGGoodUserNearBy.m
//  Pinnacle
//
//  Created by Alex on 15-4-13.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGGoodUserNearBy.h"

@implementation HGGoodUserNearBy

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid"}];
}
- (instancetype) initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err{
    self = [super initWithDictionary:dict error:err];
    
    return self;
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[@"uid", @"is_robot", @"item_num", @"nickname", @"place",@"portrait",@"verified", @"item_images",@"location", @"userPreferFollow"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    
    return NO;
}

@end

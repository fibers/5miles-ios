//
//  HGItemDetailController.h
//  Pinnacle
//
//  Created by Maya Game on 14-9-20.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGAppData.h"
#import "HGItemDetailLayout.h"
#import "HGItemHeaderCell.h"
#import "HGSectionTitleReusableView.h"
#import "HGItemOfferCell.h"
#import "HGHomeItemCell.h"
#import "HGItemMapController.h"
#import "HGChatViewController.h"
#import "HGOfferViewController.h"
#import "HGUserViewController.h"
#import "HGOfferLine.h"
#import "HGShopItem.h"
#import "HGUser.h"
#import "HGUtils.h"
#import "HGAppDelegate.h"
#import <SVProgressHUD.h>
#import <PXAlertView+Customization.h>
#import <UIColor+FlatColors.h>
#import <CoreMotion/CoreMotion.h>
#import <AudioToolbox/AudioToolbox.h>
#import "BDKNotifyHUD.h"
#import "HGInstructViewController.h"
#import <MapKit/MapKit.h>
#import "IBActionSheet.h"
#import "ItemDetailLikersCell.h"
#import "StrikeThroughLabel.h"
#import "HGItemDetail.h"
#import "X4ImageViewer.h"

@class HGShopItem;

@interface HGItemDetailController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,UIAccelerometerDelegate, UIScrollViewDelegate, HGItemDetailLayoutDelegate, HGItemHeaderActionDelegate,  HGChatViewControllerDelegate,  UIActionSheetDelegate ,ItemDetailLikeUserClickAction, MKMapViewDelegate, HGInstructDelegate, IBActionSheetDelegate, X4ImageViewerDelegate>


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) UISegmentedControl* suggestItemsSegment;
@property (nonatomic, strong) UIButton* reporterButton;
@property (nonatomic, strong) UIView* reportView;
@property (nonatomic, weak) HGItemHeaderCell * headcell;

@property (nonatomic, assign) BOOL bFromSellingPage;

@property (nonatomic, strong) HGItemDetail * itemDetail;
@property (nonatomic, strong) NSMutableArray * similarItems;
@property (nonatomic, strong) NSMutableArray * offerLines;

@property (nonatomic, strong) HGShopItem * item;
@property (nonatomic, strong) NSMutableArray* sellingItems;
@property (nonatomic, strong) NSString* item_SellerUserID;
@property (nonatomic, strong) NSTimer* updateButtonTimer;


///////////flying user action view /////////////
@property (nonatomic, strong) UIView* flyingUserActionView;
@property (nonatomic, strong) UIButton* button_like;
@property (nonatomic, strong) UIButton* button_buy;
@property (nonatomic, strong) UIButton* button_ask;
@property (nonatomic, strong) UIImageView* button_askIcon;
@property (nonatomic, strong) UIView* SeperatorView;
//////////end of flying user action view//////


@property (nonatomic, strong) NSMutableArray* likersArray;
@property (nonatomic, assign) int likerTotalcount ;

@property (nonatomic, assign) BOOL bBlockedOwner;
@property (nonatomic, strong) NSString* trackstring;

//////////////////////MD5 String ////////////////
@property (nonatomic, strong) NSString* MD5_recommendItems;
@property (nonatomic, strong) NSString* MD5_ItemDetail;

@property (nonatomic, strong) NSString* MD5_SellerOtherItems;
/////////////////////end MD5 String//////////////
@end

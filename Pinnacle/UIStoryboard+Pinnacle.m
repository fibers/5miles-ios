//
//  UIStoryboard+Pinnacle.h
//  Pinnacle
//
//  Created by shengyuhong on 15/2/26.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "UIStoryboard+Pinnacle.h"

@implementation UIStoryboard (Pinnacle)

+ (UIStoryboard *)mainStoryboard{
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

+ (UIStoryboard *)loginStoryboard{
    return [UIStoryboard storyboardWithName:@"Login" bundle:nil];
}

+ (UIStoryboard *)searchStoryboard{
    return [UIStoryboard storyboardWithName:@"Search" bundle:nil];
}

+ (UIStoryboard *)uploadStoryboard{
    return [UIStoryboard storyboardWithName:@"Upload" bundle:nil];
}

+ (UIStoryboard *)messageStoryboard{
    return [UIStoryboard storyboardWithName:@"Message" bundle:nil];
}

+ (UIStoryboard *)profileStoryboard{
    return [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
}


@end

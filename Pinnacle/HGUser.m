//
//  HGUser.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGUser.h"
#import "HGAppData.h"

@implementation HGUser

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid", @"nickname":@"displayName", @"portrait":@"portraitLink",  @"location":@"geoLocation"}];
}

- (instancetype) initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err{
    self = [super initWithDictionary:dict error:err];
    if (self) {
        if ([dict objectForKey:@"distance"] == nil) {
            CLLocation * itemLoc = [[CLLocation alloc] initWithLatitude:self.geoLocation.lat longitude:self.geoLocation.lon];
            if (fabs(self.geoLocation.lat - 0.0) < 0.000001
                && fabs(self.geoLocation.lon - 0.0) < 0.000001) {
                self.distance = -1.0;;
            }
            else{
                CLLocation* perferLocation = [[CLLocation alloc] initWithLatitude:[[HGUtils sharedInstance] getPreferLat] longitude:[[HGUtils sharedInstance] getPreferLon]];
                self.distance = [itemLoc distanceFromLocation:perferLocation] * 0.001;
            }
            
        }
    }
    return self;
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    NSArray * names = @[@"displayName", @"portraitLink", @"verified", @"facebook_verified", @"email_verified",@"mobile_phone_verified",@"region",@"country",@"city",@"distance",@"geoLocation",@"place", @"following", @"review_score", @"review_num"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }

    return NO;
}

- (BOOL)isMe
{
    return [[HGUserData sharedInstance].fmUserID isEqualToString:self.uid];
}

- (NSString *)formatDistance
{
    if (![CLLocationManager locationServicesEnabled]) {
        return @"";
    }
    
    NSString * distString = @"";
    
    NSString* preferredLang = [[HGUtils sharedInstance] getPreferLanguage];
    
    if ([preferredLang isEqualToString:@"zh-Hans"]) {
        //Chinese
        distString = [[HGUtils sharedInstance]getDistanceKm:self.distance];
        
    }else
    {
        distString = [[HGUtils sharedInstance]getDistanceMiles:self.distance];
    }
    
    return distString;
}

@end

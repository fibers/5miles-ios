//
//  HGOfferViewController.h
//  Pinnacle
//
//  Created by shengyuhong on 15/2/3.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HGShopItem;

@interface HGOfferViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, strong) HGShopItem *item;
@property (nonatomic, strong) NSString *offerLineID;

@property (nonatomic, strong) NSString* rf_tag;

@end

//
//  HGMessageCenterController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-25.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGMessageCenterController.h"
#import "HGAppData.h"
#import "HGAppDelegate.h"
#import "HGShopItem.h"
#import "HGUser.h"
#import "HGMessage.h"
#import "HGSysMessage.h"
#import "HGMessageCell.h"
#import "HGSysMessageCell.h"
#import "HGChatViewController.h"
#import "HGUtils.h"
#import "HGUserViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SVProgressHUD.h>
#import "SVPullToRefresh.h"

#import "HGWebContentController.h"
#import "HGSearchViewController.h"
#import "HGItemDetailController.h"
#import "HGSystemThemeController.h"
#import "JSONKit.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGDataCacheManager.h"
#import "HGSearchResultsController.h"
#import "HGRatingsController.h"
#import "HGStartSystemStrAction.h"
#import "UIApplication+Pinnacle.h"

@interface HGMessageCenterController ()<UIAlertViewDelegate>


@property (nonatomic, strong) UISegmentedControl * segmentedCtrl;
@property (nonatomic, strong) NSMutableArray* msgArray_buying;
@property (nonatomic, strong) NSMutableArray* msgArray_selling;
@property (nonatomic, strong) NSMutableArray* msgArray_system;

@property (nonatomic, strong) NSArray * msgTypes;

@property (nonatomic, strong) NSMutableDictionary* messageAlreadyReceived;


@property (nonatomic, assign) BOOL pageRead;
@property (nonatomic, strong) NSString* nextLink_system;
@property (nonatomic, strong) NSString* nextLink_buyingMessage;
@property (nonatomic, strong) NSString* nextLink_sellingMessage;

@property (nonatomic, strong) UIButton *statusBarBG;

@property (nonatomic, strong) UIView* noMoreHintView;
@property (nonatomic, assign) int showSellNoMoreHint;
@property (nonatomic, assign) int showBuyNoMoreHint;
@property (nonatomic, assign) int showNotificationNoMoreHint;

@property (nonatomic, strong) RACDisposable *Count_buying_subsNeedDisposed;
@property (nonatomic, strong) RACDisposable *Count_selling_subsNeedDisposed;
@property (nonatomic, strong) RACDisposable *Count_system_subsNeedDisposed;

@property (nonatomic, assign) BOOL isViewAppear;


@end


@implementation HGMessageCenterController

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setup];
}

const CGFloat kSegmentParentViewHeight = 44;
const CGFloat kSegmentControlHeight = 30;

-(void)initHeadview
{
   
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    
    UIView* seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0,self.segmentParentView.frame.size.height-fSeperatorHeight, self.segmentParentView.frame.size.width, fSeperatorHeight)];
    seperatorView.backgroundColor = FANCY_COLOR(@"d5d5d5");
    [self.segmentParentView addSubview:seperatorView];
    
    float singleSegmentWidth = self.segmentParentView.frame.size.width/3;
    self.dotImage1_buy = [[UIImageView alloc] initWithFrame:CGRectMake(singleSegmentWidth-16, 10, 19/3, 19/3)];
    self.dotImage1_buy.image = [UIImage imageNamed:@"message-dot"];
    [self.segmentParentView addSubview:self.dotImage1_buy];
    
    self.dotImage2_sell = [[UIImageView alloc] initWithFrame:CGRectMake(2*singleSegmentWidth-16, 10, 19/3, 19/3)];
    self.dotImage2_sell.image = [UIImage imageNamed:@"message-dot"];
    [self.segmentParentView addSubview:self.dotImage2_sell];
    
    self.dotImage3_notification = [[UIImageView alloc] initWithFrame:CGRectMake(3*singleSegmentWidth-16, 10, 19/3, 19/3)];
    self.dotImage3_notification.image = [UIImage imageNamed:@"message-dot"];
    [self.segmentParentView addSubview:self.dotImage3_notification];
    
    self.dotImage1_buy.hidden = self.dotImage2_sell.hidden = self.dotImage3_notification.hidden = YES;
}

-(void)addEmptyMessageHintView
{
    
    self.emptyMessageHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.emptyMessageHintView.backgroundColor = [UIColor whiteColor];
    
    self.emptyBuyIcon = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 205)/2,
                                                                      (SCREEN_HEIGHT - 64 - 184 - 40 - 20) / 2,
                                                                      410/2,
                                                                      313/2)];
    
    self.emptyBuyIcon.image = [UIImage imageNamed:@"message_empty_buy"];
    [self.emptyMessageHintView addSubview:self.emptyBuyIcon];
    self.emptyBuyLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, CGRectGetMaxY(self.emptyBuyIcon.frame)+20, SCREEN_WIDTH - 35 * 2, 40)];
    self.emptyBuyLabel.font = [UIFont systemFontOfSize:14];
    self.emptyBuyLabel.textColor = FANCY_COLOR(@"818181");
    self.emptyBuyLabel.textAlignment = NSTextAlignmentCenter;
    self.emptyBuyLabel.numberOfLines = 2;
    self.emptyBuyLabel.text = NSLocalizedString(@"Conversations with sellers can be found here.", nil);
    [self.emptyMessageHintView addSubview:self.emptyBuyLabel];
    self.emptyBuyLabel.hidden = self.emptyBuyIcon.hidden = YES;
    
    
    self.emptySellIcon = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 205)/2,
                                                                       (SCREEN_HEIGHT - 64 - 184 - 60 - 20) / 2,
                                                                       410/2,
                                                                       313/2)];
    self.emptySellIcon.image = [UIImage imageNamed:@"message_empty_sell"];
    [self.emptyMessageHintView addSubview:self.emptySellIcon];
    self.emptySellLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, CGRectGetMaxY(self.emptySellIcon.frame)+20, SCREEN_WIDTH - 35 * 2, 60)];
    self.emptySellLabel.font = [UIFont systemFontOfSize:14];
    self.emptySellLabel.textColor = FANCY_COLOR(@"818181");
    self.emptySellLabel.textAlignment = NSTextAlignmentCenter;
    self.emptySellLabel.numberOfLines = 3;
    self.emptySellLabel.text = NSLocalizedString(@"Conversations with buyers can be found here. Reply quickly and your listings will be sold fast.", nil);
    [self.emptyMessageHintView addSubview:self.emptySellLabel];
    self.emptySellIcon.hidden = self.emptySellLabel.hidden = YES;
    
    self.emptyNotificationIcon = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 205)/2,
                                                                               (SCREEN_HEIGHT - 64 - 257 - 60 - 20) / 2,
                                                                               410/2,
                                                                               358/2)];
    self.emptyNotificationIcon.image = [UIImage imageNamed:@"message_empty_notification"];
    [self.emptyMessageHintView addSubview:self.emptyNotificationIcon];
    self.emptyNotificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,
                                                                            CGRectGetMaxY(self.emptyNotificationIcon.frame)+20,
                                                                            SCREEN_WIDTH - 35 * 2,
                                                                            60)];
    self.emptyNotificationLabel.font = [UIFont systemFontOfSize:14];
    self.emptyNotificationLabel.textColor = FANCY_COLOR(@"818181");
    self.emptyNotificationLabel.textAlignment = NSTextAlignmentCenter;
    self.emptyNotificationLabel.numberOfLines = 3;
    self.emptyNotificationLabel.text = NSLocalizedString(@"Notifications about new followers, likes, listings from users you follow, and reviews can be found here.", nil);
    [self.emptyMessageHintView addSubview:self.emptyNotificationLabel];
    self.emptyNotificationIcon.hidden = self.emptyNotificationLabel.hidden = YES;
    
    
    self.emptyMessageHintView.hidden = YES;
    [self.view addSubview:self.emptyMessageHintView];
    
}

-(void)showEmptyBuy
{
    self.emptyBuyLabel.hidden = self.emptyBuyIcon.hidden = NO;
    self.emptySellLabel.hidden=self.emptySellIcon.hidden = YES;
    self.emptyNotificationLabel.hidden = self.emptyNotificationIcon.hidden = YES;
    self.emptyMessageHintView.hidden = NO;
}

-(void)showEmptySell
{
    self.emptySellLabel.hidden=self.emptySellIcon.hidden = NO;
    self.emptyBuyLabel.hidden = self.emptyBuyIcon.hidden = YES;
    self.emptyNotificationLabel.hidden = self.emptyNotificationIcon.hidden = YES;
    self.emptyMessageHintView.hidden = NO;
}
-(void)showEmptyNotification
{
    self.emptyNotificationLabel.hidden = self.emptyNotificationIcon.hidden = NO;
    self.emptyBuyLabel.hidden = self.emptyBuyIcon.hidden = YES;
    self.emptySellLabel.hidden=self.emptySellIcon.hidden = YES;
    self.emptyMessageHintView.hidden = NO;
}

-(void)hiddenEmptyView
{
    self.emptyNotificationLabel.hidden = self.emptyNotificationIcon.hidden = YES;
    self.emptyBuyLabel.hidden = self.emptyBuyIcon.hidden = YES;
    self.emptySellLabel.hidden=self.emptySellIcon.hidden = YES;
    self.emptyMessageHintView.hidden = YES;
}


-(void)addNewSegmentcontroller
{
    self.segmentParentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kSegmentParentViewHeight)];
   
    [self.view addSubview:self.segmentParentView];
    self.segmentParentView.backgroundColor = [UIColor whiteColor];
    
    self.segmentedCtrl=[[UISegmentedControl alloc] initWithFrame:CGRectMake(5.0f,
                                                                            (kSegmentParentViewHeight - kSegmentControlHeight) / 2,
                                                                            self.view.frame.size.width-10,
                                                                            kSegmentControlHeight) ];
    
    NSString* segString1 =  NSLocalizedString(@"Buy", nil);
    NSString* segString2 = NSLocalizedString(@"Sell", nil);
    NSString* segString3 = NSLocalizedString(@"Notifications", nil);
    
    [self.segmentedCtrl insertSegmentWithTitle:segString1 atIndex:0 animated:YES];
    [self.segmentedCtrl insertSegmentWithTitle:segString2 atIndex:1 animated:YES];
    [self.segmentedCtrl insertSegmentWithTitle:segString3 atIndex:2 animated:YES];
    self.segmentedCtrl.momentary = NO;
    self.segmentedCtrl.tintColor = FANCY_COLOR(@"242424");
    self.segmentedCtrl.selectedSegmentIndex = 0;
    self.segmentedCtrl.layer.borderWidth = 1.0;
    self.segmentedCtrl.layer.borderColor = FANCY_COLOR(@"242424").CGColor;
    self.segmentedCtrl.layer.cornerRadius = 6.0;
    self.segmentedCtrl.clipsToBounds = YES;
    self.segmentedCtrl.backgroundColor = [UIColor whiteColor];
    
    int suggestFontSize = 13;
    if(IPHONE6PLUS)
    {
        suggestFontSize = 15;
    }
    else
    {
        suggestFontSize = 13;
    }
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:FANCY_COLOR(@"242424"),NSForegroundColorAttributeName,  [UIFont fontWithName:@"Museo-300" size:suggestFontSize],NSFontAttributeName,nil];
    [self.segmentedCtrl setTitleTextAttributes:dic forState:UIControlStateNormal];
    [self.segmentedCtrl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.segmentParentView addSubview:self.segmentedCtrl];
   
    [self initHeadview];
}
-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    self.tableView.tableFooterView = self.noMoreHintView;
    [self.noMoreHintView setHidden:YES];
}
-(void)initCacheData
{
    NSString* jsonString_system = [[HGDataCacheManager sharedInstance] getCacheData:MSG_SYSTEM_KEY];
    if (jsonString_system != nil && jsonString_system.length > 0) {
        NSError * error = nil;
        NSDictionary * responseDic = (NSDictionary*)[jsonString_system objectFromJSONString];
        HGSysMessageResponse * currentSystemMessageRes = [[HGSysMessageResponse alloc] initWithDictionary:responseDic error:&error];
        self.nextLink_system = [[responseDic objectForKey:@"meta"] objectForKey:@"next"];
        [self InfiniteScrollingHandle:self.nextLink_system];
        
        for (int i = (int)currentSystemMessageRes.objects.count -1; i>=0; i--) {
            HGSysMessage* CurrentSystemMessage = [currentSystemMessageRes.objects objectAtIndex:i];
            NSString* valuestring = [self.messageAlreadyReceived valueForKey:CurrentSystemMessage.uid];
            if (valuestring == nil || valuestring.length == 0) {
                [self.msgArray_system insertObject:CurrentSystemMessage atIndex:0];
                [self.messageAlreadyReceived setObject:@"1" forKey:CurrentSystemMessage.uid];
            }
        }

    }
    
    NSString* jsonString_buy = [[HGDataCacheManager sharedInstance] getCacheData:MSG_BUYING_KEY];
    if (jsonString_buy != nil && jsonString_buy.length > 0) {
        NSDictionary * responseDic = (NSDictionary*)[jsonString_buy objectFromJSONString];
        NSArray * objects = [responseDic objectForKey:@"objects"];
        self.nextLink_buyingMessage = [[responseDic objectForKey:@"meta"] objectForKey:@"next"];
        [self InfiniteScrollingHandle:self.nextLink_buyingMessage];
        
        for (int i = (int)objects.count-1;i>=0 ; i--) {
            NSDictionary * dictObj = [objects objectAtIndex:i];
            NSError * error = nil;
            HGMessage * msg = [[HGMessage alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                NSString* valuestring = [self.messageAlreadyReceived valueForKey:msg.uid];
                if (valuestring == nil || valuestring.length == 0) {
                    [self.msgArray_buying addObject:msg];
                    [self.messageAlreadyReceived setObject:@"1" forKey:msg.uid];
                }
            }
        }
    }
    NSString* jsonString_selling = [[HGDataCacheManager sharedInstance] getCacheData:MSG_SELLING_KEY];
    if (jsonString_selling != nil && jsonString_selling.length > 0) {
        NSDictionary * responseDic = (NSDictionary*)[jsonString_selling objectFromJSONString];
        self.nextLink_sellingMessage = [[responseDic objectForKey:@"meta"] objectForKey:@"next"];
        [self InfiniteScrollingHandle:self.nextLink_sellingMessage];
        
        
        NSArray * objects = [responseDic objectForKey:@"objects"];
        
        for (int i = (int)objects.count-1;i>=0 ; i--) {
            NSDictionary * dictObj = [objects objectAtIndex:i];
            NSError * error = nil;
            HGMessage * msg = [[HGMessage alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                NSString* valuestring = [self.messageAlreadyReceived valueForKey:msg.uid];
                if (valuestring == nil || valuestring.length == 0) {
                    [self.msgArray_selling addObject:msg];
                    [self.messageAlreadyReceived setObject:@"1" forKey:msg.uid];
                }
            }
        }
    }
    [self sortDataArray:self.msgArray_selling];
    [self sortDataArray:self.msgArray_buying];
    
}
- (void)_setup
{
    self.msgArray_buying = [[NSMutableArray alloc] init];
    self.msgArray_selling = [[NSMutableArray alloc] init];
    self.msgArray_system = [[NSMutableArray alloc] init];
     self.messageAlreadyReceived = [[NSMutableDictionary alloc] init];
   
    
    self.msgTypes = @[NSLocalizedString(@"buying",nil), NSLocalizedString(@"selling",nil), NSLocalizedString(@"system",nil)];
   
    
    self.nextLink_system = @"";
    self.nextLink_sellingMessage = @"";
    self.nextLink_buyingMessage = @"";
    
    self.lastestMD5_buy = @"";
    self.lastestMD5_sell = @"";
    self.lastestMD5_notification = @"";
    self.count_buying = self.count_selling = self.count_system = [[NSNumber alloc] initWithInt:0];

    self.navigationItem.title = NSLocalizedString(@"Messages",nil);
    
    [self addNoMoreHintView];
    
    self.statusBarBG = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    self.statusBarBG.hidden = YES;
    self.statusBarBG.backgroundColor = [UIColor whiteColor];
    self.statusBarBG.userInteractionEnabled = YES;
    [self.view addSubview:self.statusBarBG];
    
    [self addNewSegmentcontroller];

    @weakify(self);
    [[RACObserve([HGUserData sharedInstance], fmUserID) ignore:nil] subscribeNext:^(id x) {
        
        @strongify(self);
        [self.msgArray_buying removeAllObjects];
        [self.msgArray_selling removeAllObjects];
        [self.msgArray_system removeAllObjects];
        [self.messageAlreadyReceived removeAllObjects];
        [self initCacheData];
        [self.tableView reloadData];
        
        self.currentType = HGMessageTypeBuying;
        self.segmentedCtrl.selectedSegmentIndex = 0;
        self.pageRead = NO;
        if (self.messageTimer == nil) {
            self.messageTimer =  [NSTimer scheduledTimerWithTimeInterval:MESSAGE_FRESH_TIME target:self selector:@selector(freshMessage) userInfo:nil repeats:YES];
            //每10秒运行一次function方法。
            simply_lock = 0;
        }
        else{
            [self.messageTimer invalidate];
            self.messageTimer = nil;
            simply_lock = 0;
            self.messageTimer = [NSTimer scheduledTimerWithTimeInterval:MESSAGE_FRESH_TIME target:self selector:@selector(freshMessage) userInfo:nil repeats:YES];
        }
        [self _fetchNewMessageCountWithCompletion:nil];
        
        
        //todo : need add load more function.
        if (1) {
            if (![self.nextLink_buyingMessage isEqual:[NSNull null]]) {
                [self.tableView addInfiniteScrollingWithActionHandler:^{
                    @strongify(self);
                    [self loadNextPage:self.nextLink_buyingMessage];
                }];
            }

        }
    }];
}

-(void)loadNextPage:(NSString*)nextUrlString
{
    
    switch (self.currentType) {
        case HGMessageTypeBuying:
             [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"message_buy_loadmore" label:nil value:nil];
            nextUrlString = self.nextLink_buyingMessage;
            break;
        case HGMessageTypeSelling:
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"message_sell_loadmore" label:nil value:nil];
            nextUrlString = self.nextLink_sellingMessage;
            break;
        case HGMessageTypeSystem:
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"message_system_loadmore" label:nil value:nil];
            nextUrlString = self.nextLink_system;
            break;
            
        default:
            break;
    }
    
    if ([nextUrlString isEqual:[NSNull null]] || nextUrlString.length == 0 ) {
        [self.tableView.infiniteScrollingView stopAnimating];
        self.tableView.showsInfiniteScrolling = NO;
         [self.noMoreHintView setFrame:CGRectMake(0, self.tableView.contentSize.height, self.tableView.frame.size.width, 40)];
        switch (self.currentType) {
            case HGMessageTypeBuying:
            {
                self.showBuyNoMoreHint = 1;
                break;
            }
            case HGMessageTypeSelling:
            {
                self.showSellNoMoreHint = 1;
                break;
            }
            case HGMessageTypeSystem:
            {
                self.showNotificationNoMoreHint = 1;
                break;
            }
                
            default:
                break;
        }
        
        self.noMoreHintView.hidden = NO;
      
    } else {
        self.noMoreHintView.hidden = YES;
        if (self.currentType == HGMessageTypeSystem) {
            [self getSystemNextPageRequest:nextUrlString];
        }
        else{
            [self getUserMessageNextPageRequest:nextUrlString];
        }
    }

}
-(void)getSystemNextPageRequest:(NSString*)nextUrl
{
    NSString* url = [@"my_messages/" stringByAppendingString:nextUrl];
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:url parameters:@{@"type":@"system"} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self.count_buying = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"buying"];
        self.count_selling = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"selling"];
        self.count_system = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"system"];
        
        [self _customBadgeDot];

        self.nextLink_system = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        
        NSLog(@"the system next url is %@", self.nextLink_system);
        NSError * error = nil;
        HGSysMessageResponse * currentSystemMessageRes = [[HGSysMessageResponse alloc] initWithDictionary:responseObject error:&error];
        
        for (int i = 0; i<currentSystemMessageRes.objects.count; i++) {
            HGSysMessage* CurrentSystemMessage = [currentSystemMessageRes.objects objectAtIndex:i];
            NSString* valuestring = [self.messageAlreadyReceived valueForKey:CurrentSystemMessage.uid];
            if (valuestring == nil || valuestring.length == 0) {
                [self.msgArray_system addObject:CurrentSystemMessage];
                [self.messageAlreadyReceived setObject:@"1" forKey:CurrentSystemMessage.uid];
            }
            else{
                //already in array .   need to replace
                BOOL bMessageFound = NO;
                for (int j = 0; j<self.msgArray_system.count; j++) {
                    HGSysMessage* oldSystemMessage  = [self.msgArray_system objectAtIndex:j];
                    if ([CurrentSystemMessage.uid isEqualToString:oldSystemMessage.uid]) {
                        bMessageFound = YES;
                        [self.msgArray_system replaceObjectAtIndex:j withObject:CurrentSystemMessage];
                    }
                }
                
                //force to add a message
                if (!bMessageFound) {
                    [self.msgArray_system insertObject:CurrentSystemMessage atIndex:0];
                }
            }
            
        }

       
        [self.tableView reloadData];
        [self.tableView.infiniteScrollingView stopAnimating];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.tableView.infiniteScrollingView stopAnimating];
        NSLog(@"fetch system messages failed: %@", error.userInfo);
    }];

}

-(void)getUserMessageNextPageRequest:(NSString*)nextUrl
{
    NSString* url = [@"my_messages/" stringByAppendingString:nextUrl];
    NSString * msgType = [self.msgTypes objectAtIndex:self.currentType];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:url parameters:@{@"type": msgType, @"limit":[[NSNumber alloc] initWithInt:MESSAGE_GET_LIMIT_COUNT]} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self.count_buying = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"buying"];
        self.count_selling = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"selling"];
        self.count_system = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"system"];
        
        [self _customBadgeDot];
        
        NSString * msgTypelatest = [self.msgTypes objectAtIndex:self.currentType];
        if (![msgTypelatest isEqualToString:msgType]) {
            return;
        }
        
        
        NSMutableArray * messages;

        if (self.currentType == HGMessageTypeBuying) {
            self.nextLink_buyingMessage = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            messages = self.msgArray_buying;
        }
        if (self.currentType == HGMessageTypeSelling) {
            self.nextLink_sellingMessage = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            messages = self.msgArray_selling;
        }
        NSArray * objects = [responseObject objectForKey:@"objects"];
        
        
        for (int i = 0;i<objects.count ; i++) {
            NSDictionary * dictObj  = [objects objectAtIndex:i];
            NSError * error = nil;
            HGMessage * msg = [[HGMessage alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                NSString* valuestring = [self.messageAlreadyReceived valueForKey:msg.uid];
                if (valuestring == nil || valuestring.length == 0) {
                    [messages addObject:msg];
                    [self.messageAlreadyReceived setObject:@"1" forKey:msg.uid];
                }
                else{
                    
                    BOOL bMessageFound = NO;
                    //already in array .   need to replace
                    for (int j = 0; j<messages.count; j++) {
                        HGMessage* currentMessage  = [messages objectAtIndex:j];
                        if ([currentMessage.uid isEqualToString:msg.uid]) {
                            bMessageFound = YES;
                            [messages replaceObjectAtIndex:j withObject:currentMessage];
                        }
                    }
                    
                    if (!bMessageFound) {
                        [messages insertObject:msg atIndex:0];
                    }
                }

            } else {
                NSLog(@"HGMessage object init error: %@", error);
            }
        }
        
        [self.tableView reloadData];
        [self.tableView.infiniteScrollingView stopAnimating];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.tableView.infiniteScrollingView stopAnimating];
        NSLog(@"fetch messages failed: %@", error.userInfo);
    }];

}


-(void)freshMessage
{
     [self _fetchNewMessageCountWithCompletion:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.isViewAppear = NO;
    
    [SVProgressHUD dismiss];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)ResetContentOffset
{
    self.tableView.contentOffset = CGPointMake(0, 0);
}
-(void)customizeBackButton
{
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    self.navigationItem.backBarButtonItem = backbutton;
}
-(void)AddRACforMessageCount
{
    @weakify(self);
    self.Count_buying_subsNeedDisposed = [[RACObserve(self, count_buying) distinctUntilChanged] subscribeNext:^(NSNumber* x) {
    
        @strongify(self);
        if (self.dotImage1_buy) {
            if (x.intValue == 0) {
                self.dotImage1_buy.hidden = YES;
            }
            else{
                self.dotImage1_buy.hidden = NO;
            }
        }
        [self.segmentedCtrl setNeedsDisplay];
        [self _customBadgeDot];
       
    }];
    
    self.Count_selling_subsNeedDisposed = [[RACObserve(self, count_selling) distinctUntilChanged] subscribeNext:^(NSNumber* x) {
        
        @strongify(self);
        if (self.dotImage2_sell) {
            if (x.intValue == 0) {
                self.dotImage2_sell.hidden = YES;
            }
            else{
                self.dotImage2_sell.hidden = NO;
            }
            [self.segmentedCtrl setNeedsDisplay];
            [self _customBadgeDot];

        }
        
    }];
    
    self.Count_system_subsNeedDisposed = [[RACObserve(self, count_system) distinctUntilChanged] subscribeNext:^(NSNumber* x) {
        
        @strongify(self);
        if (self.dotImage3_notification) {
            if (x.intValue == 0) {
                self.dotImage3_notification.hidden = YES;
            }
            else{
                self.dotImage3_notification.hidden = NO;
            }
            [self.segmentedCtrl setNeedsDisplay];
            [self _customBadgeDot];

        }
        
    }];
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeLeft|UIRectEdgeRight|UIRectEdgeBottom;
    
    [self customizeBackButton];
    [self AddRACforMessageCount];
    self.navigationController.delegate = self;
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 20)];
    footView.autoresizesSubviews = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.tableFooterView = footView;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePushNotificationPayload) name:@"Notification.Payload.offer" object:nil];
    
   
    [self addEmptyMessageHintView];
}

- (void)viewDidLayoutSubviews{
    self.tableView.frame = CGRectMake(0, kSegmentParentViewHeight, self.view.bounds.size.width, self.view.bounds.size.height - kSegmentParentViewHeight - 49);
}

static int autoSwitchSetSystemOn = 0;
-(void)autoSwitchUnReadSegment
{
    if(self.bNeedSegmentAutoChange) {
        self.bNeedSegmentAutoChange = NO;
        if (self.count_buying.intValue > 0) {
            [self.segmentedCtrl setSelectedSegmentIndex:0];
        }
        else if (self.count_selling.intValue > 0) {
            [self.segmentedCtrl setSelectedSegmentIndex:1];
        }
        else if (self.count_system.intValue > 0) {
            [self.segmentedCtrl setSelectedSegmentIndex:2];
            autoSwitchSetSystemOn = 1;
        }

    }
    self.currentType = self.segmentedCtrl.selectedSegmentIndex;
    
    
    [self.tableView reloadData];
    
}
-(void)CheckShowNotificationSegment
{
    if (self.bShowNotificationSegment) {
        self.bShowNotificationSegment = FALSE;
         [self.segmentedCtrl setSelectedSegmentIndex:2];
        self.currentType = self.segmentedCtrl.selectedSegmentIndex;
        [self.tableView reloadData];
    }
}

-(void)handlePushNotificationPayload
{
    
    self.segmentedCtrl.selectedSegmentIndex = HGMessageTypeSelling;
    [self _fetchMesssagesWithCurrentType:1];
}


- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning  in HGSellView view Controller");
    [super didReceiveMemoryWarning];
    [[SDImageCache sharedImageCache] clearMemory];
}

#pragma mark - UI Control Actions
-(void)InfiniteScrollingHandle:(NSString*)nextUrl
{
     __weak HGMessageCenterController * weakSelf = self;
    if ([nextUrl isEqual:[NSNull null]] || nextUrl.length == 0) {
        [self.tableView.infiniteScrollingView stopAnimating];
        self.tableView.showsInfiniteScrolling = NO;
       
    }
    else{
        self.tableView.showsInfiniteScrolling = YES;
        [self.tableView addInfiniteScrollingWithActionHandler:^{
            
            [weakSelf loadNextPage:nextUrl];
        }];

    }
    
}
-(NSMutableArray*)getCurrentMsgArray:(NSInteger)msgType
{
    if (msgType == HGMessageTypeBuying) {
        return self.msgArray_buying;
    }
    if (msgType == HGMessageTypeSelling) {
        return self.msgArray_selling;
    }
    if(msgType == HGMessageTypeSystem) {
        return self.msgArray_system;
    }
    return nil;
}

- (void)killScroll {
    [self.tableView setContentOffset:self.tableView.contentOffset animated:NO];
}

- (void)segmentedControlChangedValue:(UISegmentedControl *)sender
{
    [self killScroll];
    
    self.currentType = sender.selectedSegmentIndex;
    [self.tableView reloadData];
   
    if (autoSwitchSetSystemOn == 1) {
        if (self.currentType != HGMessageTypeSystem) {
            self.dotImage3_notification.hidden = YES;
        }
    }
    switch (self.currentType) {
        case HGMessageTypeBuying:
        {
            
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"tab_buy" label:nil value:nil];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [self InfiniteScrollingHandle:self.nextLink_buyingMessage];
            NSMutableArray* msgArray = [self getCurrentMsgArray: self.currentType];
            self.count_buying = [[NSNumber alloc] initWithInt: [self updateMessagesArrayUnread:msgArray]];
           
            break;
        }
           
        case HGMessageTypeSelling:
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"tab_sell" label:nil value:nil];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [self InfiniteScrollingHandle:self.nextLink_sellingMessage];
            NSMutableArray* msgArray = [self getCurrentMsgArray:self.currentType];
            self.count_selling = [[NSNumber alloc] initWithInt:[self updateMessagesArrayUnread:msgArray]];
           
            break;
        }
           
        case HGMessageTypeSystem:
        {
            [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"tab_system" label:nil value:nil];
            self.count_system = [[NSNumber alloc] initWithInt:0];
            
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
            [self InfiniteScrollingHandle:self.nextLink_system];
            break;

        }
            
        default:
            break;
    }
    
    [self handleEndOfRoadAppear];
    [self _customBadgeDot];
    [self _fetchMesssagesWithCurrentType:1];
}

//todo, not define .when the message count < screen display size.
-(void)handleEndOfRoadAppear
{
    switch (self.currentType) {
        case HGMessageTypeBuying:
        {
            if (self.showBuyNoMoreHint == 1) {
                self.noMoreHintView.hidden = NO;
            }
            else{
                self.noMoreHintView.hidden = YES;
            }
            break;
        }
        case  HGMessageTypeSelling:
        {
            if(self.showSellNoMoreHint == 1)
            {
                self.noMoreHintView.hidden = NO;
            }
            else{
                self.noMoreHintView.hidden = YES;
            }
            break;
        }
            
        case HGMessageTypeSystem:
        {
            if(self.showNotificationNoMoreHint == 1)
            {
                self.noMoreHintView.hidden = NO;
            }
            else{
                self.noMoreHintView.hidden = YES;
            }
            break;
        }
            
    }
    
}
-(int)updateMessagesArrayUnread:(NSArray*)MessageArray
{
    int unReadCount = 0;
    for (int i = 0; i<MessageArray.count; i++) {
        HGMessage * msg = [MessageArray objectAtIndex:i];
        unReadCount =(int)( unReadCount + msg.badgeCount.integerValue);
    }
    return unReadCount;
    
}



#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int nCount = 0;
    if (self.currentType == HGMessageTypeBuying) {
       
        self.count_buying = [[NSNumber alloc] initWithInt: [self updateMessagesArrayUnread:self.msgArray_buying]];
        nCount = (int)self.msgArray_buying.count;
        if (nCount == 0)
        {
            [self showEmptyBuy];
        }
    }
    if (self.currentType == HGMessageTypeSelling) {
        self.count_selling = [[NSNumber alloc] initWithInt: [self updateMessagesArrayUnread:self.msgArray_selling]];

        nCount = (int)self.msgArray_selling.count;
        if(nCount==0)
        {
            [self showEmptySell];
        }
    }
    if (self.currentType == HGMessageTypeSystem) {
        nCount = (int)self.msgArray_system.count;
        if(nCount == 0)
        {
            [self showEmptyNotification];
        }
    }
    
    if (nCount!=0) {
        [self hiddenEmptyView];
    }
    return nCount;
}

//static int ratingResultIconHeight = 24;
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentType == HGMessageTypeSystem) {
        HGSysMessage * message = [self.msgArray_system objectAtIndex:indexPath.row];
        CGRect rect = [message.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.view.bounds) - 60, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0]} context:nil];
        
        float height = CGRectGetHeight(rect) + 36.0;
        if (height < 60.0) {
            height = 60.0;
        }
      
        return height;
    } else {
        return 60;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentType == HGMessageTypeSystem) {
        HGSysMessage * message = [self.msgArray_system objectAtIndex:indexPath.row];
        
        if(message.text==nil)
        {
            message.text = @"";
        }
        if (message.desc == nil) {
            message.desc = @"";
        }
        CGRect rect1 = [message.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.view.bounds) - 60, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0]} context:nil];
        
        CGRect rect2 = [message.desc boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.view.bounds) - 60, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0]} context:nil];
        float height = CGRectGetHeight(rect1) + CGRectGetHeight(rect2) + 30;
        if (height < 44.0) {
            height = 44.0;
        }
        if (message.image.length > 0 ) {
            height = height + SYSTEM_MESSAGE_IMAGE_HEIGHT;
        }
        
        if (message.smType == HGSysMessageTypeReview && [HGAppData sharedInstance].bOpenReviewFunctions) {
            //type of review.
                height = height + 18;
        }
        
        return height;
    }
    else
    {
        HGMessage * message = [[self getCurrentMsgArray:self.currentType] objectAtIndex:indexPath.row];
        
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        tempLabel.numberOfLines = 3;
        tempLabel.lineBreakMode = NSLineBreakByWordWrapping;
        tempLabel.text = message.text;
        tempLabel.font = [UIFont systemFontOfSize:15];
        CGSize fitSize = [tempLabel sizeThatFits:CGSizeMake(SCREEN_WIDTH - 117, 0)];
        
        return (fitSize.height + 45);
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentType != HGMessageTypeSystem) {
        HGMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
        
        // Configure the cell...
        HGMessage * message = [[self getCurrentMsgArray:self.currentType] objectAtIndex:indexPath.row];
        [cell configWithEntity:message];
        
        
        return cell;
    } else {
        HGSysMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SysMessageCell" forIndexPath:indexPath];
        
        HGSysMessage * message = [self.msgArray_system objectAtIndex:indexPath.row];
        [cell configWithSysMessage:message];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //[cell sizeToFit];
        cell.canvasView.delegate = self;
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentType == HGMessageTypeSystem) {
        
        HGSysMessage* message =[self.msgArray_system objectAtIndex:indexPath.row];
        if(message.action != nil && message.action.length > 0)
        [[HGStartSystemStrAction sharedInstance] startHomeBannerAction:message.action withController:self withActionSource:SYSTEM_ACTION_SOURCE_MESSAGECENTER];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        return;
    }
    else{
        [self HandleMessageAction:indexPath];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        return;
    }
    
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //todo. alex.json数组需要替换掉， 还是要上sqlite进行本地持久化。3.2 之后，3.2如果有删除操作 暂时丢掉被污染的本地数据。
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSString* messageIdString = @"";
        if (self.currentType == HGMessageTypeBuying || self.currentType == HGMessageTypeSelling) {
            HGMessage * message = [[self getCurrentMsgArray:self.currentType] objectAtIndex:indexPath.row];
            messageIdString = message.uid;
            
            if (self.currentType == HGMessageTypeBuying ) {
                self.count_buying = [[NSNumber alloc] initWithInt: self.count_buying.intValue - message.badgeCount.intValue ];
                 [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"buy_delete" label:nil value:nil];
                [[HGDataCacheManager sharedInstance] updateCacheData:MSG_BUYING_KEY withValueString:@""];
               
            }
            if (self.currentType == HGMessageTypeSelling ) {
                 [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"sell_delete" label:nil value:nil];
                self.count_selling = [[NSNumber alloc] initWithInt: self.count_selling.intValue - message.badgeCount.intValue ];
                [[HGDataCacheManager sharedInstance] updateCacheData:MSG_SELLING_KEY withValueString:@""];

            }

            
            [[self getCurrentMsgArray:self.currentType] removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            
        }
        else{
            
            [[HGDataCacheManager sharedInstance] updateCacheData:MSG_SYSTEM_KEY withValueString:@""];

             [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"system_delete" label:nil value:nil];
            HGSysMessage* message =[self.msgArray_system objectAtIndex:indexPath.row];
            messageIdString = message.uid;
            [self.msgArray_system removeObjectAtIndex:indexPath.row];
           
            
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            
        }
        
       
        [self.messageAlreadyReceived removeObjectForKey:messageIdString];
        NSString* typeString = [@"" stringByAppendingFormat:@"%ld", (long)self.currentType];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"delete_message/" parameters:@{@"message_id":messageIdString, @"type": typeString} success:^(NSURLSessionDataTask *task, id responseObject) {
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"delete messga fail");
        }];
        
    }
}

#pragma mark- UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    static BOOL isAnimating = NO;
    
    if ([scrollView isEqual:self.tableView] && self.isViewAppear && !isAnimating) {
        
        if (scrollView.contentOffset.y <= 0) {
            if (self.navigationController.navigationBarHidden) {
                isAnimating = YES;
                [self.navigationController setNavigationBarHidden:NO animated:YES];
                
                [UIView animateWithDuration:0.3 animations:^{
                    self.segmentParentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.segmentParentView.frame.size.height);
                    self.statusBarBG.hidden = YES;
                } completion:^(BOOL finished) {
                    isAnimating = NO;
                }];
            }
        } else {
            if (!self.navigationController.navigationBarHidden) {
                isAnimating = YES;
                [self.navigationController setNavigationBarHidden:YES animated:YES];
                
                [UIView animateWithDuration:0.3 animations:^{
                    self.segmentParentView.frame = CGRectMake(0, 20, self.view.frame.size.width, self.segmentParentView.frame.size.height);
                    self.statusBarBG.hidden = NO;
                } completion:^(BOOL finished) {
                    isAnimating = NO;
                }];
            }
        }
    }
}

-(void)HandleMessageAction:(NSIndexPath *)indexPath
{
    NSMutableArray* dataArray = [self getCurrentMsgArray:self.currentType];
    if (dataArray == nil || dataArray.count == 0) {
        return;
    }
    HGMessage * message = [dataArray objectAtIndex:indexPath.row];
   
    if (message.uid == nil) {
        return;
    }
    NSDictionary* parameters =@{@"message_id":message.uid};
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"read_message/" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
       
        
        if (self.currentType == HGMessageTypeBuying ) {
             [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"message_buy" label:nil value:nil];
            self.count_buying = [[NSNumber alloc] initWithInt:self.count_buying.intValue - message.badgeCount.intValue ];
            
        }
        if (self.currentType == HGMessageTypeSelling ) {
             [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"message_sell" label:nil value:nil];
            self.count_selling = [[NSNumber alloc] initWithInt:self.count_selling.intValue - message.badgeCount.intValue];
        }
         message.unread = NO;
        @try {
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView reloadData];
        }
        @catch (NSException *exception) {
             [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];

            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"mark message read failed: %@", error.userInfo);
    }];
    
    [self performSegueWithIdentifier:@"MessageCenterToChat" sender:message];
}

-(void)systemMessageAction
{
    //using current user temply
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"message_view" action:@"message_system" label:nil value:nil];
    HGUser* user = [[HGUser alloc] init];
    user.portraitLink = [HGUserData sharedInstance].userAvatarLink;
    user.uid = [HGUserData sharedInstance].fmUserID;
    user.displayName = [HGUserData sharedInstance].userDisplayName;
    
    HGUserViewController *vcUser = (HGUserViewController *)[[UIStoryboard mainStoryboard]instantiateViewControllerWithIdentifier:@"VCUser"];
    vcUser.user = user;
    
    [self.navigationController pushViewController:vcUser animated:YES];
}


#pragma mark - Helpers

-(void)sortDataArray:(NSMutableArray*)messageArray
{
    NSComparator cmptr = ^(id obj1, id obj2){
        HGMessage* message1 = (HGMessage*)obj1;
        HGMessage* message2 = (HGMessage*)obj2;
        
        if (message1.timestamp < message2.timestamp) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if (message1.timestamp > message2.timestamp) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    NSArray* sortedArray = [messageArray sortedArrayUsingComparator:cmptr];
    [messageArray removeAllObjects];
    [messageArray addObjectsFromArray:sortedArray];
    
}

///////todo...need to rewrite..
- (void)_fetchMesssagesWithCurrentType:(int)needProgressHUD
{
    
    NSLog(@"message center timer fetch message working");
    
    ///networking status check.
    if (![[HGAppData sharedInstance].userApiClient checkNetworkStatus]) {
        //return;
    }
    //
    

    if (self.currentType != HGMessageTypeSystem) {
        NSString * msgType = [self.msgTypes objectAtIndex:self.currentType];
        
      
        
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"my_messages/" parameters:@{@"type": msgType, @"limit":[[NSNumber alloc] initWithInt:MESSAGE_GET_LIMIT_COUNT]} success:^(NSURLSessionDataTask *task, id responseObject) {
            
            self.count_buying = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"buying"];
            self.count_selling = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"selling"];
            self.count_system = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"system"];
            
            [self _customBadgeDot];
            
            NSString * msgTypelatest = [self.msgTypes objectAtIndex:self.currentType];
            
            NSString *jsonString = [[NSString alloc] initWithData:[responseObject JSONData] encoding:NSUTF8StringEncoding];
            NSString* currentMD5 = [[HGUtils sharedInstance] md5HexDigest:jsonString];
            
            if (needProgressHUD == 1) {
                [SVProgressHUD dismiss];
            }
            if (![msgType isEqualToString:msgTypelatest]) {
                return;
            }
            if (self.currentType == HGMessageTypeBuying) {
            
                if ([self.lastestMD5_buy isEqualToString:currentMD5] && self.msgArray_buying.count != 0) {
                    //the same with latest result, do nothing
                    [self.tableView reloadData];
                    return;
                }
                else{
                    ////new message coming
                    //AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                    self.lastestMD5_buy = currentMD5;
                }

                //warning, only update the empty next link, if the next link have value, we can not cover the originl value.
                if ([self.nextLink_buyingMessage isEqual:[NSNull null]]) {
                    //skip , the end of message
                    
                }
                else if([self.nextLink_buyingMessage isEqualToString:@""]) {
                    self.nextLink_buyingMessage = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
                  
                }
                [self InfiniteScrollingHandle:self.nextLink_buyingMessage];
                [[HGDataCacheManager sharedInstance] updateCacheData:MSG_BUYING_KEY withValueString:jsonString];
                
            }
            if (self.currentType == HGMessageTypeSelling) {
                if ([self.lastestMD5_sell isEqualToString:currentMD5] && self.msgArray_selling.count != 0) {
                    //the same with latest result, do nothing
                    [self.tableView reloadData];
                    return;
                }
                else{
                    //new message.
                    //AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                    self.lastestMD5_sell = currentMD5;
                }
                //warning, only update the empty next link, if the next link have value, we can not cover the originl value.
                if ([self.nextLink_sellingMessage isEqual:[NSNull null]]) {
                    //skip the end of message..
                    //todo . handle dynamic message number increase? how?
                }
                else if ([self.nextLink_sellingMessage isEqualToString:@""]) {
                    self.nextLink_sellingMessage = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
                 
                }
                [self InfiniteScrollingHandle:self.nextLink_sellingMessage];
                [[HGDataCacheManager sharedInstance] updateCacheData:MSG_SELLING_KEY withValueString:jsonString];
            }

            NSArray * objects = [responseObject objectForKey:@"objects"];
            
            NSMutableArray * messages = [self getCurrentMsgArray:self.currentType];
            //[messages removeAllObjects];
            int messageCount= (int)messages.count;
            for (int i = (int)objects.count-1;i>=0 ; i--) {
                NSDictionary * dictObj = [objects objectAtIndex:i];
                if (![[dictObj objectForKey:@"type"] isEqualToString:msgType]) {
                    //not current msg type, skip to avoid content mix bug.
                    continue;
                }
                NSError * error = nil;
                HGMessage * msg = [[HGMessage alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    NSString* valuestring = [self.messageAlreadyReceived valueForKey:msg.uid];
                    if (valuestring == nil || valuestring.length == 0) {
                        [messages insertObject:msg atIndex:messageCount];
                        [self.messageAlreadyReceived setObject:@"1" forKey:msg.uid];
                    }
                    else
                    {
                        BOOL bMessageFound = NO;
                        //already in array .   need to replace
                        for (int j = 0; j<messages.count; j++)
                        {
                            HGMessage* currentMessage  = [messages objectAtIndex:j];
                            
                            
                            if ([currentMessage.uid isEqualToString:msg.uid]) {
                                bMessageFound = YES;
                                [messages removeObjectAtIndex:j];
                                [messages insertObject:msg atIndex:0];
                                break;
                            }
                           
                        }
                        if (!bMessageFound) {
                            [messages insertObject:msg atIndex:0];
                        }
                    }
                }
            }
            //sort the message
            [self sortDataArray:messages];
            [self.tableView reloadData];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            if (needProgressHUD == 1) {
                [SVProgressHUD dismiss];
            }
            NSLog(@"fetch messages failed: %@", error.userInfo);
        }];
    } else {
        [self _fetchSysMessages];
    }
}

- (void)_fetchSysMessages
{
    
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"my_messages/" parameters:@{@"type":@"system"} success:^(NSURLSessionDataTask *task, id responseObject) {

        self.count_buying = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"buying"];
        self.count_selling = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"selling"];
        self.count_system = [[[responseObject objectForKey:@"meta"] objectForKey:@"res_ext"] objectForKey:@"system"];
        ////// auto mark as read immediatly.
        self.count_system = 0;
        
        [self _customBadgeDot];
        
        NSString *jsonString = [[NSString alloc] initWithData:[responseObject JSONData] encoding:NSUTF8StringEncoding];
        [[HGDataCacheManager sharedInstance] updateCacheData:MSG_SYSTEM_KEY withValueString:jsonString];
        
        NSString* currentMD5 = [[HGUtils sharedInstance] md5HexDigest:jsonString];
        if ([self.lastestMD5_notification isEqualToString:currentMD5] && self.msgArray_system.count != 0) {
            //the same with latest result, do nothing
            //return;
        }
        else{
            //new system message
            //AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            self.lastestMD5_notification = currentMD5;
        }
        
        self.nextLink_system = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        [self InfiniteScrollingHandle:self.nextLink_system];
        NSError * error = nil;
        HGSysMessageResponse * currentSystemMessageRes = [[HGSysMessageResponse alloc] initWithDictionary:responseObject error:&error];
        
        for (int i = (int)currentSystemMessageRes.objects.count -1; i>=0; i--) {
            HGSysMessage* CurrentSystemMessage = [currentSystemMessageRes.objects objectAtIndex:i];
            NSString* valuestring = [self.messageAlreadyReceived valueForKey:CurrentSystemMessage.uid];
            if (valuestring == nil || valuestring.length == 0) {
                [self.msgArray_system insertObject:CurrentSystemMessage atIndex:0];
                [self.messageAlreadyReceived setObject:@"1" forKey:CurrentSystemMessage.uid];
            }
            else{
                BOOL bMessageFound = NO;
                //already in array .   need to replace
                for (int j = 0; j<self.msgArray_system.count; j++) {
                    HGSysMessage* oldSystemMessage  = [self.msgArray_system objectAtIndex:j];
                    if ([CurrentSystemMessage.uid isEqualToString:oldSystemMessage.uid]) {
                        bMessageFound = YES;
                        [self.msgArray_system removeObjectAtIndex:j];
                        [self.msgArray_system insertObject:CurrentSystemMessage atIndex:0];
                    }
                }
                
                if (!bMessageFound) {
                    [self.msgArray_system insertObject:CurrentSystemMessage atIndex:0];
                }
            }
            
        }

        
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"fetch system messages failed: %@", error.userInfo);
    }];
}


static int simply_lock = 0;
- (void)_fetchNewMessageCountWithCompletion:(void (^)())completion
{
    NSLog(@"fetchNewMessageCount with Completion");
    if (![[HGUserData sharedInstance] isLoggedIn]) {
        //not login,
        simply_lock = 0;
        return;
    }
    
    
    if (simply_lock == 1) {
        //already a request for message update in process.
        return;
    }
    else
    {
        //add a lock
        simply_lock = 1;
    }
    
    
    
    ////new_messge_count will update meaning for total unread message count from v3.1.
    ////
    NSString* userToken = [[HGAppData sharedInstance].userApiClient.requestSerializer.HTTPRequestHeaders valueForKey:@"X-FIVEMILES-USER-TOKEN"];

    if (userToken== nil || userToken.length == 0 ) {
        return;
    }
    
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"new_message_count/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self.count_buying = [responseObject objectForKey:@"buying"];
        self.count_selling = [responseObject objectForKey:@"selling"];
        self.count_system = [responseObject objectForKey:@"system"];

       

        
        
        [self _customBadgeDot];
        [self _fetchMesssagesWithCurrentType:0];
        if (completion) {
            completion();
        }
        //unlock
        simply_lock = 0;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"get new message count failed: %@", error.userInfo);
        //unlock
        simply_lock = 0;
    }];
}

- (void)_customBadgeDot
{
    if (self.count_buying.intValue > 0 || self.count_selling.intValue > 0 || self.count_system.intValue > 0) {
        
        if (![UIApplication isEnableRemoteNotification]) {
            
            NSString* const kShowTurnOnNotificationKey = @"ShowTurnOnNotifications";
            
            if (![[NSUserDefaults standardUserDefaults] boolForKey:kShowTurnOnNotificationKey]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Turn on notifications", nil)
                                                                message:NSLocalizedString(@"This way, you will see new messages on your iPhone instantly. And you can close some types of notifications in Profile>Setting", nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                      otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                [alert show];
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kShowTurnOnNotificationKey];
            }
        }
        
        UIImage * imageIcon = [UIImage imageNamed:@"msgcenter-unread"];
        imageIcon = [imageIcon imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.navigationController.tabBarItem.image = imageIcon;
         HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.tabController.unreadmessage_mark.hidden = NO;
        int totalUnReadCount= self.count_buying.intValue + self.count_selling.intValue + self.count_system.intValue;
        if (totalUnReadCount>99) {
            totalUnReadCount = 99;
        }
        
        if (totalUnReadCount>9) {
            [appDelegate.tabController setLongUnReadBadget];
        }
        else
        {
            [appDelegate.tabController setShortUnReadBadget];
        }
        
        appDelegate.tabController.unreadMessageCount.text = [@"" stringByAppendingFormat:@"%d",totalUnReadCount];
        [HGAppData sharedInstance].bHaveNewMessage = YES;
        
    } else {
          HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.tabController.unreadmessage_mark.hidden = YES;
        appDelegate.tabController.unreadMessageCount.text = @"";
        [self.segmentedCtrl setNeedsDisplay];
    }
}

#pragma mark- UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (1 == buttonIndex) {
        [UIApplication gotoSystemSetting];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"MessageCenterToChat"]) {
        HGChatViewController *vcDest = (HGChatViewController *)segue.destinationViewController;
        HGMessage * message = sender;
        HGUser *toUser = [message fromMe] ? message.to : message.from;
        
        vcDest.bFromItemDetailPage = NO;
        vcDest.offerLineID = message.threadID;
        vcDest.toUser = toUser;
        
    }
}


#pragma mark -
#pragma mark Dealloc

-(void)dealloc
{
    //[super dealloc];
    //取消定时器
    [self.messageTimer invalidate];
    self.messageTimer = nil;
    simply_lock = 0;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Notification.Payload.offer" object:nil];
    
    if (self.Count_system_subsNeedDisposed && self.Count_selling_subsNeedDisposed && self.Count_buying_subsNeedDisposed) {
        [self.Count_buying_subsNeedDisposed dispose];
        [self.Count_selling_subsNeedDisposed dispose];
        [self.Count_system_subsNeedDisposed dispose];

    }
}


//页面将要进入前台，开启定时器
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;

    self.tabBarController.tabBar.hidden = YES;
    [[HGUtils sharedInstance] gaTrackViewName:@"message_view"];
    [FBSDKAppEvents logEvent:@"message_view"];
    
    if (!self.pageRead) {
        self.count_buying = [[NSNumber alloc] initWithInt:  0];
        [self _customBadgeDot];
        
        self.pageRead = YES;
    }
    [self _fetchMesssagesWithCurrentType:0];
    //开启定时器
    [self.messageTimer setFireDate:[NSDate distantPast]];
    simply_lock = 0;
    [self autoSwitchUnReadSegment];
    [self CheckShowNotificationSegment];

    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.segmentParentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.segmentParentView.frame.size.height);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.isViewAppear = YES;
}


//页面消失，进入后台不显示该页面，关闭定时器
-(void)viewDidDisappear:(BOOL)animated
{
    //关闭定时器
    //[self.messageTimer setFireDate:[NSDate distantFuture]];
    [super viewDidDisappear:animated];
}



- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
    NSLog(@"navigationcontroller will show view in Message center");
    HGAppDelegate* myappdelegate = (HGAppDelegate*)[UIApplication sharedApplication].delegate;
    UIViewController* homeview =  [navController.viewControllers objectAtIndex:0];
    
    if (viewController == homeview) {
        myappdelegate.tabController.fmTabbarView.hidden = NO;
    }
    else{
        myappdelegate.tabController.fmTabbarView.hidden = YES;
    }
    
}


-(void)clickUrlAction:(NSURL *)url
{
    HGWebContentController *vcWebContent = (HGWebContentController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCWebContent"];
    vcWebContent.contentLink = [url absoluteString];
    [self.navigationController pushViewController:vcWebContent animated:YES];
}

@end

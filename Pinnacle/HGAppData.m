//
//  HGAppData.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-29.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

// Notice: Once you add the a global variable in the class.
// You should add the read and write logic in the following method
// setGlobalAppDataDefaultValue
// setLocalAppDataDefaultValue
// initAppDataFromDisk
// saveAppDataToDisk
// addRACOnAppData

#import "HGAppData.h"
#import <TargetConditionals.h>
#import "HMFJSONResponseSerializerWithData.h"
#import "HGConstant.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "JSONKit.h"
#import "HGReportReason.h"

@implementation HGAppData

+ (HGAppData *)sharedInstance
{
    static HGAppData * _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[HGAppData alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [HGAppInitData sharedInstance];
        self.AppInitDataSingle = [HGAppInitData sharedInstance];
        
               
        [self setGlobalAppDataDefaultValue];
        [self initAppDataFromDisk];
        [self addRACOnAppData];

        self.guestApiClient = [HGFivemilesClient guestClient];
        self.userApiClient = [HGFivemilesClient userClient];
        self.chatApiClient = [HGFivemilesClient chatClient];
        
        HMFJSONResponseSerializerWithData *responseSerializer = [HMFJSONResponseSerializerWithData serializer];
        responseSerializer.removesKeysWithNullValues = YES;
        self.guestApiClient.responseSerializer = responseSerializer;
        
        responseSerializer = [HMFJSONResponseSerializerWithData serializer];
        responseSerializer.removesKeysWithNullValues = YES;
        self.userApiClient.responseSerializer = responseSerializer;
        
        responseSerializer = [HMFJSONResponseSerializerWithData serializer];
        responseSerializer.removesKeysWithNullValues = YES;
        self.chatApiClient.responseSerializer = responseSerializer;
        
        [self.guestApiClient.requestSerializer setValue:[[NSLocale preferredLanguages] firstObject] forHTTPHeaderField:@"Accept-Language"];
        [self.userApiClient.requestSerializer setValue:[[NSLocale preferredLanguages] firstObject] forHTTPHeaderField:@"Accept-Language"];
        [self.chatApiClient.requestSerializer setValue:[[NSLocale preferredLanguages] firstObject] forHTTPHeaderField:@"Accept-Language"];
        
        self.countryString = @"";
        self.regionString = @"";
        self.cityString = @"";
        
        self.bHasItemValue = 0;
        self.phoneNumber = @"";
        self.phoneNumber_countryCode = @"+1";
        self.phoneNumber_CountryName = @"United States";
        self.selfIntroduct = @"";
        self.sellingItemBrandString = @"";
        self.sellingItemDeliveryString = @"";
        self.sellingItemDeliveryType = -1;
        self.homeBanner = nil;
        self.homeBanner_userClose = NO;
        
        self.deviceToken = @"this_is_a_default_device_token";
        
        self.chatMapSearchKeyString = @"";
        self.chatMapUserSelectAddrLat = @"";
        self.chatMapUserSelectAddrLon = @"";
        self.chatMapSysteSuggestKeys = @"Walmart,Starbucks,Parking lot,Cafe,Coffee shop,Police,Store,Mall,McDonalds,Fast Food,Community Center,College,Office,Payground";
    }
    return self;
}

- (void)prefetchConfiguration{
    
    [self.guestApiClient FIVEMILES_GET:@"report_reasons/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSArray *arrayReasons = (NSArray *)responseObject;
        for(NSDictionary *reasons in arrayReasons){
            NSNumber *type = [reasons objectForKey:@"type"];
            ReportReasonType reasonType = (ReportReasonType)[type intValue];
            NSArray *arrayReasons = [HGReportReason arrayOfModelsFromDictionaries:[reasons objectForKey:@"objects"]];
            
            switch (reasonType) {
                case ReportReasonTypeItem:
                    self.reportItemReasons = arrayReasons;
                    break;
                case ReportReasonTypeSeller:
                    self.reportSellerReasons = arrayReasons;
                    break;
                case ReportReasonTypeReview:
                    self.reportReviewReasons = arrayReasons;
                    break;
 
                default:
                    break;
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
    
}

- (NSString *)localUserKey:(NSString *)key
{
    return key;
}

- (void)addRACOnAppData{
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];

    
    [[RACObserve(self, startADCampaign) distinctUntilChanged] subscribeNext:^(HGCampaignsObject *x) {
        [standardUserDefaults setObject:x ? [x toDictionary]:@{} forKey:@"startADCampaign"];
        [standardUserDefaults synchronize];
    }];
    
    [[RACObserve(self, bOpenReviewFunctions) distinctUntilChanged] subscribeNext:^(NSNumber *x) {
        [standardUserDefaults setBool:x ? [x boolValue]:YES forKey:@"bOpenReviewFunctions"];
        [standardUserDefaults synchronize];
    }];
    
    [[RACObserve(self, reportItemReasons) distinctUntilChanged] subscribeNext:^(NSArray *x) {
        [standardUserDefaults setObject:x ? [HGReportReason arrayOfDictionariesFromModels:x]:@[] forKey:@"reportItemReasons"];
        [standardUserDefaults synchronize];
    }];
    
    [[RACObserve(self, reportSellerReasons) distinctUntilChanged] subscribeNext:^(NSArray *x) {
        [standardUserDefaults setObject:x ? [HGReportReason arrayOfDictionariesFromModels:x]:@[] forKey:@"reportSellerReasons"];
        [standardUserDefaults synchronize];
    }];
    
    [[RACObserve(self, reportReviewReasons) distinctUntilChanged] subscribeNext:^(NSArray *x) {
        [standardUserDefaults setObject:x ? [HGReportReason arrayOfDictionariesFromModels:x]:@[] forKey:@"reportReviewReasons"];
        [standardUserDefaults synchronize];
    }];


}



- (void)setGlobalAppDataDefaultValue{
    NSDictionary *userDefaultsDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                          @{}, @"startADCampaign",
                                          @[], @"reportItemReasons",
                                          @[], @"reportSellerReasons",
                                          @[], @"reportReviewReasons",
                                          @(YES), @"bOpenReviewFunctions",
                                          nil];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:userDefaultsDefaults];
}

- (void)initAppDataFromDisk{
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];

    NSDictionary* dictObj =  [standardUserDefaults dictionaryForKey:@"startADCampaign"];
    if (dictObj != nil) {
        self.startADCampaign
        = [[HGCampaignsObject alloc] initWithDictionary:dictObj error:nil];
    }
    
    
    self.bOpenReviewFunctions = [standardUserDefaults boolForKey:@"bOpenReviewFunctions"];
    
    NSArray *itemReasons = [standardUserDefaults arrayForKey:@"reportItemReasons"];
    self.reportItemReasons = [HGReportReason arrayOfModelsFromDictionaries:itemReasons];
    
    NSArray *sellerReasons = [standardUserDefaults arrayForKey:@"reportSellerReasons"];
    self.reportSellerReasons = [HGReportReason arrayOfModelsFromDictionaries:sellerReasons];
    
    NSArray *reviewReasons = [standardUserDefaults arrayForKey:@"reportReviewReasons"];
    self.reportReviewReasons = [HGReportReason arrayOfModelsFromDictionaries:reviewReasons];
}

- (void)saveAppDataToDisk
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardUserDefaults setObject:[self.startADCampaign toDictionary] forKey:@"startADCampaign"];
    
    [standardUserDefaults setBool:self.bOpenReviewFunctions forKey:@"bOpenReviewFunctions"];
    
    [standardUserDefaults setObject:[HGReportReason arrayOfDictionariesFromModels:self.reportItemReasons] forKey:@"reportItemReasons"];
    [standardUserDefaults setObject:[HGReportReason arrayOfDictionariesFromModels:self.reportSellerReasons] forKey:@"reportSellerReasons"];
    [standardUserDefaults setObject:[HGReportReason arrayOfDictionariesFromModels:self.reportReviewReasons] forKey:@"reportReviewReasons"];
    
    [standardUserDefaults synchronize];
}




-(NSString *) appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

- (NSString *) build
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

- (NSString *) versionBuild
{
    NSString * version = [self appVersion];
    NSString * build = [self build];
    
    NSString * versionBuild = [NSString stringWithFormat: @"v%@", version];
    
    if (![version isEqualToString: build]) {
        versionBuild = [NSString stringWithFormat: @"%@(%@)", versionBuild, build];
    }
    
    return versionBuild;
}




@end

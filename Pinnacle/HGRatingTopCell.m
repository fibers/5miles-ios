//
//  HGRatingTopCell.m
//  Pinnacle
//
//  Created by Alex on 15/5/6.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGRatingTopCell.h"

@implementation HGRatingTopCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    [self _commonSetup];
}

- (void)_commonSetup
{
    
}

@end

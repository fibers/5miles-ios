//
//  HGBackDoorViewController.m
//  Pinnacle
//
//  Created by Alex on 14-10-22.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGBackDoorViewController.h"
#import "HGFivemilesClient.h"
#import "HGAppData.h"
#import "HMFJSONResponseSerializerWithData.h"
#import "HGAppDelegate.h"
#import <PXAlertView/PXAlertView+Customization.h>

@interface HGBackDoorViewController ()

@end

@implementation HGBackDoorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)changeServer:(id)sender {
    [HGFivemilesClient resignClientServer];
    [HGAppData sharedInstance].guestApiClient = [HGFivemilesClient guestClient];
    [HGAppData sharedInstance].userApiClient = [HGFivemilesClient userClient];
    [HGAppData sharedInstance].guestApiClient.responseSerializer = [HMFJSONResponseSerializerWithData serializer];
    [HGAppData sharedInstance].userApiClient.responseSerializer = [HMFJSONResponseSerializerWithData serializer];
    
    if ([HGUserData sharedInstance].fmUserID.length > 0) {
        [[HGAppData sharedInstance].userApiClient.requestSerializer setValue:[HGUserData sharedInstance].fmUserID forHTTPHeaderField:@"X-FIVEMILES-USER-ID"];
    }
    
    if ([HGUserData sharedInstance].fmUserToken.length > 0) {
        [[HGAppData sharedInstance].userApiClient.requestSerializer setValue:[HGUserData sharedInstance].fmUserToken forHTTPHeaderField:@"X-FIVEMILES-USER-TOKEN"];
    }

}

- (IBAction)testFunction:(id)sender {
    
    PXAlertView * view = [PXAlertView showAlertWithTitle:@"1"                                                 message: @"2"
                                             cancelTitle:@"cancle"                                              otherTitles:@[@"test",@"test2"]
                                              completion:nil];
    [view useDefaultIOS7Style];
    
}


@end

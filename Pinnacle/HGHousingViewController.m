//
//  HGHousingViewController.m
//  Pinnacle
//
//  Created by mumuhou on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGHousingViewController.h"
#import "HGUtils.h"
@interface HGHousingViewController ()

@end

@implementation HGHousingViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self loadUrl:@"https://m.5milesapp.com/app/house"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"tab_housing" label:nil value:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  HGHomeViewController.m
//  Pinnacle
//
//  Created by mumuhou on 15/7/15.
//  copy from HGHomePageViewController
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGHomeViewController.h"
#import "HGForSaleViewController.h"
#import "HGServiceViewController.h"
#import "HGHousingViewController.h"
#import "HGJobsViewController.h"
#import "HGHomeFollowingViewController.h"
#import "HGHomeChildViewControllerProtocol.h"

#import "HGItemDetailController.h"
#import "HGZipCodeVC.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGGoodSellerViewController.h"
#import "HGMessage.h"
#import "HGWebContentController.h"
#import "UINavigationBar+Pinnacle.h"
#import "HGNavigationBarScrollProtocol.h"

typedef NS_ENUM(NSInteger, SegmentedItem) {
    kSegmentedItemForSave = 0,
    kSegmentedItemService,
    kSegmentedItemHousing,
    kSegmentedItemJobs,
    kSegmentedFollowing
};

@interface HGHomeViewController () <UINavigationControllerDelegate, HGNavigationBarScrollProtocol>

@property (nonatomic, strong) UIButton*statusBarBG;

@property (nonatomic, assign) BOOL isNaviBarAnimating;

@property (nonatomic, strong) HGForSaleViewController *forSaveViewController;
@property (nonatomic, strong) HGServiceViewController *serviceViewController;
@property (nonatomic, strong) HGHousingViewController *housingViewController;
@property (nonatomic, strong) HGJobsViewController *jobsViewController;
@property (nonatomic, strong) HGHomeFollowingViewController *followingViewController;

@property (nonatomic, strong) HGAppDelegate* myappdelegate;

@end

@implementation HGHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setPageTitles:@[@"For Sale", @"Services", @"Housing", @"Jobs", @"Following"]];
    
    self.forSaveViewController = [[HGForSaleViewController alloc] init];
    self.forSaveViewController.delegate = self;
    self.serviceViewController = [[HGServiceViewController alloc] init];
    self.serviceViewController.delegate = self;
    self.housingViewController = [[HGHousingViewController alloc] init];
    self.housingViewController.delegate = self;
    self.jobsViewController = [[HGJobsViewController alloc] init];
    self.jobsViewController.delegate = self;
    self.followingViewController = [[HGHomeFollowingViewController alloc] init];
    self.followingViewController.delegate = self;

    self.pageControllers = @[self.forSaveViewController,
                             self.serviceViewController,
                             self.housingViewController,
                             self.jobsViewController,
                             self.followingViewController];

    CTFontRef normalFont = [[self class] ctFontRefFromUIFont:[UIFont systemFontOfSize:14]];
    CGColorRef normalColor = [UIColor blackColor].CGColor;
    CGColorRef selectedColor = FANCY_COLOR(@"ff8830").CGColor;
    CTFontRef selectedFont = [[self class] ctFontRefFromUIFont:[UIFont systemFontOfSize:14]];
    
    NSDictionary *normalAttributes = @{NSFontAttributeName : (__bridge id)normalFont, NSForegroundColorAttributeName : (__bridge id)normalColor};
    NSDictionary *selectedAttributes = @{NSFontAttributeName : (__bridge id)selectedFont, NSForegroundColorAttributeName : (__bridge id)selectedColor};
    [self setTitleNormalAttributes:normalAttributes titleSelectedAttributes:selectedAttributes];
    
    self.segmentedControl.bottomLineColor = [UIColor lightGrayColor];
    self.segmentedControl.selectionIndicatorColor = FANCY_COLOR(@"ff8830");
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage-title"]];
    self.view.backgroundColor = FANCY_COLOR(@"f0f0f0");

    //navigation right search temply hidden.
    [self customizeBackButton];
    [self addRightBarButton_search];
    [self addleftBarButton];
    self.navigationController.delegate = self;
    self.myappdelegate = (HGAppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.statusBarBG = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
    self.statusBarBG.hidden = YES;
    self.statusBarBG.backgroundColor = [UIColor whiteColor];
    self.statusBarBG.userInteractionEnabled = YES;
    
    [self.view addSubview:self.statusBarBG];
}

+ (CTFontRef)ctFontRefFromUIFont:(UIFont *)font {
    CTFontRef ctfont = CTFontCreateWithName((__bridge CFStringRef)font.fontName, font.pointSize, NULL);
    return CFAutorelease(ctfont);
}

-(void)customizeBackButton
{
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
    NSLog(@"the navigation back button frame is %@", backbutton);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        self.navigationItem.backBarButtonItem = backbutton;
        //using the customize back  image;
        // naviBack_icon2 naviBack
        UIImage *backButtonImage = [[UIImage imageNamed:@"naviBack"]resizableImageWithCapInsets:UIEdgeInsetsMake(0, 18, 0, 21)];
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    }
    else{
        //todo for ios7.0
        [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"naviBack"]];
        [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"naviBack"]];
    }
}

-(void)addRightBarButton_search
{
    UIBarButtonItem* rightBarItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"homeRightItem"] style:UIBarButtonItemStylePlain target:self action:@selector(searchAction)];
    self.navigationItem.rightBarButtonItem = rightBarItem;
}

-(void)addleftBarButton
{
    UIBarButtonItem* leftBarItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"homeLeftItem"] style:UIBarButtonItemStylePlain target:self action:@selector(startShowUserGoodReviews)];
    self.navigationItem.leftBarButtonItem = leftBarItem;
}

-(void)searchAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"search_top" label:nil value:nil];
    [HGAppData sharedInstance].jumpToSearchFromHomeRightButton = YES;
    [self.myappdelegate.tabController button2pressed];
}
-(void)startShowUserGoodReviews
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"reviewtopic" label:nil value:nil];
    
    
    HGWebContentController *vcWebContent = (HGWebContentController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCWebContent"];
    vcWebContent.contentLink = @"https://m.5milesapp.com/app/reviewTopic";
    vcWebContent.bShowRightShareButton = YES;
    [self.navigationController pushViewController:vcWebContent animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    HGAppDelegate* appDelegate = (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.tabController.tabBar.hidden =YES;
    appDelegate.tabController.fmTabbarView.hidden = NO;

    //self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
//    [UIView animateWithDuration:0.1 animations:^(){
//        self.segmentHeadView.frame = CGRectMake(0, 0, self.view.frame.size.width,self.segmentHeadView.frame.size.height);
//    }];
//    
//    if ([HGAppData sharedInstance].bHomeSwitchToFollowingNeeded) {
//        [HGAppData sharedInstance].bHomeSwitchToFollowingNeeded = NO;
//        [self showFollowingView];
//    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self setNavigationBarTransformProgress:0];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self checkFBtokenExpire];
    
   {
        if(![[HGUserData sharedInstance] isLoggedIn]){
            return;
        }
    }
    
    [[HGUtils sharedInstance] gaTrackViewName:@"home_view"];
    [FBSDKAppEvents logEvent:@"home_view"];
    
    BOOL bshowZipVC = NO;
    ////do not have GPS access right, remind user using Zipcode
    {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        // If the status is denied or only granted for when in use, display an alert
        BOOL bUserForceCloseZipView = [HGUserData sharedInstance].bUserForceCloseZipView;
        if (status == kCLAuthorizationStatusDenied && [HGUserData sharedInstance].zipcodeString.length == 0 && !bUserForceCloseZipView)
        {
            bshowZipVC = YES;
            HGZipCodeVC* vc= [[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"ZipCode"];
            vc.currentViewType = zipViewTypeUserRegister;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
    if(!bshowZipVC)
    {
        if ([HGUserData sharedInstance].bHomeShowUserNearby) {
            [HGUserData sharedInstance].bHomeShowUserNearby = NO;
            [self startUserNearBy];
        }else{
            if ([HGUserData sharedInstance].bHomeShowFollowInstruction){
                
                [HGUserData sharedInstance].bHomeShowFollowInstruction = NO;
//                [[HGUtils sharedInstance] showTipsWithMessage:NSLocalizedString(@"View the listings from the people you're following here. You can manage who you follow in your profile.",nil) atView:self.segmentedControl inView:self.view animated:YES withOffset:CGPointMake(80, -5) withSize:CGSizeMake(186, 100) withDirection:PointDirectionUp];
            }
        }
    }
}

-(void)startUserNearBy
{
    UIStoryboard * storyBoard = [UIStoryboard mainStoryboard];
    HGGoodSellerViewController * goodSeller = [storyBoard instantiateViewControllerWithIdentifier:@"goodSellerView"];
    [self.navigationController pushViewController:goodSeller animated:YES];
}

- (void)freshHomeItem
{
    if (self.segmentedControl.selectedSegmentIndex >= 0 && self.segmentedControl.selectedSegmentIndex < self.pageControllers.count) {
        id<HGHomeChildViewControllerProtocol> childPage = self.pageControllers[self.segmentedControl.selectedSegmentIndex];
        [childPage refreshContent];
    }
}

- (void)freshHomeItemSilent
{
    if (self.segmentedControl.selectedSegmentIndex >= 0 && self.segmentedControl.selectedSegmentIndex < self.pageControllers.count) {
        id<HGHomeChildViewControllerProtocol> childPage = self.pageControllers[self.segmentedControl.selectedSegmentIndex];
        [childPage refreshContentSilent];
    }
}

- (void)showNearbyView
{
    self.segmentedControl.selectedSegmentIndex = 0;
}

-(void)performSegue:(id)sender withSegueName:(NSString*)segueName
{
    if ([segueName isEqualToString:@"ShowItemPage"]) {
        HGShopItem* item = (HGShopItem*)sender;
        [self performSegueWithIdentifier:@"ShowItemPage" sender:item];
    }
    if ([segueName isEqualToString:@"MessageCenterToChat"])
    {
        HGMessage * message =(HGMessage*)sender;
        [self performSegueWithIdentifier:@"MessageCenterToChat" sender:message];
    }
    if ([segueName isEqualToString:@"ShowUserDetail"]) {
        [self performSegueWithIdentifier:@"ShowUserDetail" sender:sender];
    }
}


#pragma mark - UIActionsheet Delegate
-(void)checkAnonymousUser
{
    {
        if (![[HGUserData sharedInstance] isLoggedIn]) {
            [(HGAppDelegate *)[[UIApplication sharedApplication] delegate] logUserOut];
        }
    }
}
-(void)checkFBtokenExpire
{
    //check facebook token expire time
    if ([HGUserData sharedInstance].accountType == HGAccountTypeFacebook) {
        NSDate* expirationDate = [HGUserData sharedInstance].facebookTokenExpirationDate;
        if (expirationDate!=nil) {
            
            NSDate* currentDate = [NSDate date];
            if ([currentDate timeIntervalSinceReferenceDate] > [expirationDate timeIntervalSinceReferenceDate]) {
                //token out of time , need re login.
                [(HGAppDelegate *)[[UIApplication sharedApplication] delegate] logUserOut];
                [[HGUtils sharedInstance] presentLoginGuide:self];
            }
        }
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowItemPage"]) {
        HGItemDetailController * vcDest = (HGItemDetailController *)segue.destinationViewController;
        vcDest.item = sender;
        vcDest.trackstring = [HGTrackingData sharedInstance].home_rf_tag;
    }
    if ([segue.identifier isEqualToString:@"ShowUserDetail"]) {
        HGUserViewController* vcDest =  (HGUserViewController *)segue.destinationViewController;
        vcDest.user = sender;
    }
}

- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSLog(@"navigtion will show view in home view");
    
    UIViewController* homeview =  [navController.viewControllers objectAtIndex:0];
    
    if (viewController == homeview) {
        self.myappdelegate.tabController.fmTabbarView.hidden = NO;
    }
    else{
        self.myappdelegate.tabController.fmTabbarView.hidden = YES;
    }
    //appDelegate.tabController.fmTabbarView.hidden = YES;
}

#pragma mark- HGNavigationBarScrollProtocol

- (void)viewController:(UIViewController *)viewController navigationBarTransformProgress:(CGFloat)progress
{
    if ([viewController isEqual:self.currentController]) {
        [self setNavigationBarTransformProgress:progress];
    }
}

- (void)setNavigationBarTransformProgress:(CGFloat)progress
{
    [self.navigationController.navigationBar lt_setTranslationY:(-44 * progress)];
    [self.navigationController.navigationBar lt_setContentAlpha:(1 - progress)];
    
    CGSize segmentedSize = self.segmentedControl.bounds.size;
    self.segmentedControl.frame = CGRectMake(0, 64 - 44 * progress, segmentedSize.width, segmentedSize.height);
}

- (CGFloat)headerViewOffset {
    return 64 - self.segmentedControl.frame.origin.y;
}

@end

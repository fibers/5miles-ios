//
//  HGHomeFollowingViewController.m
//  Pinnacle
//  copy from HomePageViewController
//  Created by mumuhou on 15/7/15.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGHomeFollowingViewController.h"
#import "HGHomeViewController.h"
#import "HGUtils.h"
#import "HGHomeChildViewControllerProtocol.h"
#import "HGTrackingData.h"
#import "HGFivemilesClient.h"
#import "HGHomeItemCell.h"
#import "HGItemDetailController.h"
#import "HGShopItem.h"
#import "HGUtils.h"
#import "HGMessage.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "SVPullToRefresh.h"
#import <SVProgressHUD.h>
#import "UIColor+FlatColors.h"
#import "HGCampaignsObject.h"
#import "HGWebContentController.h"
#import "JSONKit.h"
#import "HGSearchViewController.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGGoodSellerViewController.h"
#import "InstructionView.h"
#import "HGStartSystemStrAction.h"
#import "CMPopTipView.h"
#import "SectionFooterCollectionReusableView.h"
#import "HGUserData.h"
#import <ReactiveCocoa.h>
#import "HGZipCodeVC.h"
#import "HGSegmentViewController.h"

@interface HGHomeFollowingViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, CHTCollectionViewDelegateWaterfallLayout, HGHomeChildViewControllerProtocol, HGSegmentSubViewControllerProtocol>


@property (nonatomic, strong) UIView* noMoreHintView;

@property(nonatomic, strong) NSString* latestMD5string;

@property (nonatomic, strong) NSMutableArray * followings_shopItems;

@property (nonatomic, strong) UICollectionView* followingCollectionView;
@property (nonatomic, strong) UIView* followingEmptyView;

@property (nonatomic, strong) NSString * nextLink;
@property (nonatomic, strong) NSString* followingsNextLink;

@end

static NSInteger kScrollViewTopMargin = 64 - 20;

static NSString * const SectionTitleID = @"SectionTitle";
static NSString * const SimilarSectionFooter =@"noMoreFootSection";


@implementation HGHomeFollowingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self _setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setup];
}

- (id)initWithParentViewController:(HGHomeViewController *)homeViewController {
    if (self = [super init]) {
        [self _setup];
    }
    
    return self;
}

-(UIView*)createRemindView
{
    InstructionView* v = [[InstructionView alloc] initWithFrame:CGRectMake(0, 500, 50, 50)];
    v.TriangleType = 1;
    [v startInitView];
    HGAppDelegate* d =  (HGAppDelegate*)[UIApplication sharedApplication].delegate;
    [d.window addSubview:v];
    return v;
}

- (void)_setup
{
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.followings_shopItems = [NSMutableArray array];
    self.title = NSLocalizedString(@"Home", nil);
    self.nextLink = @"";
    self.followingsNextLink = @"";
    self.latestMD5string = @"";
    
    [self addFollowingEmptyView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"tab_following" label:nil value:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //hide progresshud is view change.
    [SVProgressHUD dismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self addFollowingCollectionView];
    
    [self startLoadingFollowings];
}

-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    
    [self.noMoreHintView setHidden:YES];
}

-(void)addFollowingCollectionView
{
    [self addNoMoreHintView];
    
//    UICollectionViewFlowLayout *templayout=[[UICollectionViewFlowLayout alloc] init];
    
    // Custom collection view layout
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 0, 4 );
    layout.minimumColumnSpacing = 5;
    layout.minimumInteritemSpacing = 7.5;

    self.followingCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    self.followingCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.followingCollectionView.contentInset = UIEdgeInsetsMake(kScrollViewTopMargin, 0, 0, 0);
    self.followingCollectionView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    [self.followingCollectionView registerClass:[HGHomeItemCell class] forCellWithReuseIdentifier:@"HomeItemCell"];
    self.followingCollectionView.delegate = self;
    self.followingCollectionView.dataSource = self;
    
    //注册headerView Nib的view需要继承UICollectionReusableView
    [self.followingCollectionView registerNib:[UINib nibWithNibName:@"SQSupplementaryView" bundle:nil] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:SectionTitleID];
    
    
    [self.followingCollectionView registerClass:[SectionFooterCollectionReusableView class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter withReuseIdentifier:SimilarSectionFooter];
    
    
//    self.followingCollectionView.collectionViewLayout = layout;
    
    
    // Hook pull down to refresh
    __weak __typeof(self) weakSelf = self;
    
    [self.followingCollectionView addPullToRefreshWithActionHandler:^{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"followingtop_refresh" label:nil value:nil];
        [weakSelf.followingCollectionView.pullToRefreshView stopAnimating];
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
        [weakSelf startLoadingFollowings];
        
    }];
    
    
    //Hook pull up to load more
    [self.followingCollectionView addInfiniteScrollingWithActionHandler:^{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"followingbottom_load" label:nil value:nil];
        [weakSelf _loadNextFollowingPage];
    } withAddtionnalOffset:1000];
    
    //self.followingCollectionView
    [self.view addSubview:self.followingCollectionView];
    
    self.followingCollectionView.scrollsToTop = NO;
}

- (void)startLoadingFollowings
{
    self.followingsNextLink = @"";
    __weak __typeof(self) weakSelf = self;
    
    if (self.view.window && [self isViewLoaded]) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
    }
    
    
    [self loadFollowingsNextPageItemsWithSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        bStartFollowingSearch = YES;
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        weakSelf.followingsNextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        
        [weakSelf.followings_shopItems removeAllObjects];
        
        for (id dictObj in results) {
            NSError * error = nil;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            
            if (!error) {
                [weakSelf.followings_shopItems addObject:item];
            } else {
                NSLog(@"shop item %@ creation error: %@", [dictObj objectForKey:@"id"], error);
            }
        }
        
        [weakSelf.followingCollectionView reloadData];
        [SVProgressHUD dismiss];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        bStartFollowingSearch = YES;
        [SVProgressHUD dismiss];
    }];
}

-(void)_loadNextFollowingPage
{
    if ([self.followingsNextLink isEqual:[NSNull null]]) {
        [self.followingCollectionView.infiniteScrollingView stopAnimating];
        [self.noMoreHintView setHidden:NO];
        
    } else {
        [self.noMoreHintView setHidden:YES];
        [self loadFollowingsNextPageItemsWithSuccess:^(NSURLSessionDataTask *task, id responseObject) {
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            
            self.followingsNextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    BOOL bDataReapted = NO;
                    for (int i = 0; i<self.followings_shopItems.count; i++) {
                        HGShopItem * currentItem = [self.followings_shopItems objectAtIndex:i];
                        if ([currentItem.uid isEqualToString:item.uid]) {
                            bDataReapted = YES;
                        }
                    }
                    if (!bDataReapted) {
                        [self.followings_shopItems addObject:item];
                    }
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            
            [self.followingCollectionView reloadData];
            [self.followingCollectionView.infiniteScrollingView stopAnimating];
            
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self.followingCollectionView.infiniteScrollingView stopAnimating];
        }];
    }
}

#define  HOME_ITME_COUNT 50
- (void)loadFollowingsNextPageItemsWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    ///networking status check.
//    if (![[HGAppData sharedInstance].userApiClient checkNetworkStatus]) {
//        [SVProgressHUD dismiss];
//        return;
//    }
    
    NSString* userToken = [[HGAppData sharedInstance].userApiClient.requestSerializer.HTTPRequestHeaders valueForKey:@"X-FIVEMILES-USER-TOKEN"];
    if (userToken== nil || userToken.length == 0 ) {
        return;
    }
    
    NSNumber * lat = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLat]];
    NSNumber * lon = [NSNumber numberWithDouble:[[HGUtils sharedInstance] getPreferLon]];
    if ([self.followingsNextLink isEqual:[NSNull null]] || self.followingsNextLink == nil) {
        self.followingsNextLink = @"";
    }
    NSString * apiEndpoint = [NSString stringWithFormat:@"followings_items/%@", self.followingsNextLink];
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:apiEndpoint parameters:@{@"lat": lat, @"lon": lon, @"limit":[NSNumber numberWithInt:HOME_ITME_COUNT]} success:success failure:failure];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView Datasource & Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

static BOOL bStartFollowingSearch = NO;
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    int count = (int)self.followings_shopItems.count;
    if (count == 0 && bStartFollowingSearch) {
        self.followingEmptyView.hidden = NO;
    }
    else{
        self.followingEmptyView.hidden = YES;
        
    }
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* keyCellString = @"HomeItemCell" ;
    
    
    
    HGShopItem * item;
    HGHomeItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:keyCellString forIndexPath:indexPath];
    item = [self.followings_shopItems objectAtIndex:indexPath.row];
    
    [cell configWithItem:item];
    
    
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    HGShopItem * item;
    item = [self.followings_shopItems objectAtIndex:indexPath.row];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"following_product" label:nil value:nil];
    [HGTrackingData sharedInstance].home_rf_tag = item.rf_tag;
    
    [self.parentViewController performSegueWithIdentifier:@"ShowItemPage" sender:item];
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section
{
    //todo,,only when there is no more: alex
    if (section == 0) {
        
        return 40;
    }
    return 0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"the collection kind is %@", kind);
    if ([kind isEqualToString:CHTCollectionElementKindSectionFooter]) {
        if (indexPath.section == 0) {
            SectionFooterCollectionReusableView * titleView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:SimilarSectionFooter forIndexPath:indexPath];
            
            
            [titleView addSubview: self.noMoreHintView];
            //self.noMoreHintView.hidden = NO;
            
            return titleView;
        }
    }
    return nil;
}


-(void)closeBanner
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"banner" label:nil value:nil];
    [HGAppData sharedInstance].homeBanner_userClose = YES;
    [self.followingCollectionView reloadData];
}

-(void)handleTapHomeBanner:(id)sender
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"home_view" action:@"banneroff" label:nil value:nil];
    NSString* actionUrl = [HGAppData sharedInstance].homeBanner.click_action;
    
    [[HGStartSystemStrAction sharedInstance] startHomeBannerAction:actionUrl withController:self withActionSource:0];
}

#pragma mark- empty view

-(void)defaultEmptyAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"sellersnearby" action:@"explore_seller" label:nil value:nil];
    UIStoryboard * storyBoard = [UIStoryboard mainStoryboard];
    HGGoodSellerViewController * goodSeller = [storyBoard instantiateViewControllerWithIdentifier:@"goodSellerView"];
    goodSeller.leftNavigationType = 1;
    [self.navigationController pushViewController:goodSeller animated:YES];
    
}

-(void)addEmptyContent:(UIView*)view
{
    int iconYoffset = 144;
    int lineOffset = 10;
    
    view.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];
    
    int iphone4_Y_offset_config_Value = 0;
    if (SCREEN_HEIGHT <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    CGPoint iconCenter  = CGPointMake(SCREEN_WIDTH/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [view addSubview:icon];
    
    
    CGPoint centerPoint = CGPointMake(SCREEN_WIDTH/2, CGRectGetMaxY(icon.frame)+lineOffset + 60/2);
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 60)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"No listings can be found! Follow more people now to fill this page with listings.", nil);
    textLabel.font = [UIFont systemFontOfSize:13];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [view addSubview:textLabel];
    [textLabel sizeToFit];
    
    
    CGPoint buttonCenter = CGPointMake(SCREEN_WIDTH/2, CGRectGetMaxY(textLabel.frame)+lineOffset + 36/2);
    UIButton* button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 200, 36)];
    button.layer.cornerRadius = 4.0;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = DEFAULT_EMPTY_BOARDER_COLOR;
    UILabel* buttonTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    buttonTitle.textAlignment = NSTextAlignmentCenter;
    buttonTitle.text = NSLocalizedString(@"Go exploring!", nil);
    buttonTitle.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [button addSubview:buttonTitle];
    button.center = buttonCenter;
    [view addSubview:button];
    [button addTarget:self action:@selector(defaultEmptyAction) forControlEvents:UIControlEventTouchUpInside];
}

-(void)addFollowingEmptyView
{
    self.followingEmptyView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
    self.followingEmptyView.backgroundColor = FANCY_COLOR(@"f0f0f0");
    
    
    [self addEmptyContent:self.followingEmptyView];
    
    [self.view addSubview:self.followingEmptyView];
    self.followingEmptyView.hidden = YES;
    
    //self.followingCollectionView.hidden = YES;
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HGShopItem * item;
    item = [self.followings_shopItems objectAtIndex:indexPath.row];
    
    HGItemImage * coverImage = [item.images firstObject];
    CGFloat wd = CGRectGetWidth(self.view.frame) * 0.5 - 20;
    CGSize cellSize = CGSizeMake(wd, ceil(coverImage.height * wd / coverImage.width) +WATER_FLOW_CELL_EXTEND_HEIGHT);
    
    return cellSize;
}

#pragma mark- HGHomeChildViewControllerProtocol

- (void)refreshContent
{
//    [self.followingCollectionView setContentOffset:CGPointZero animated:YES];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
    [self startLoadingFollowings];
}

- (void)refreshContentSilent
{
//    [self.followingCollectionView setContentOffset:CGPointZero animated:YES];
    [self startLoadingFollowings];
}

#pragma mark- HGSegmentSubViewControllerProtocol

- (void)segmentedPageShown:(BOOL)shown
{
    self.followingCollectionView.scrollsToTop = shown;
    
    CGFloat headerOffsetY = [self.delegate headerViewOffset];
    [self.followingCollectionView setContentOffset:CGPointMake(0, headerOffsetY - kScrollViewTopMargin) animated:NO];
    
    if (shown && self.followings_shopItems.count == 0) {
        [self refreshContentSilent];
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    [self notifyHomePageForContentOffsetDidChange:offsetY];
}

- (void)notifyHomePageForContentOffsetDidChange:(CGFloat)offsetY
{
    NSLog(@"offsetY = %f", offsetY);
    offsetY += kScrollViewTopMargin;
    if (offsetY > 0) {
        if (offsetY > 44) {
            [self.delegate viewController:self navigationBarTransformProgress:1];
            
        } else {
            [self.delegate viewController:self navigationBarTransformProgress:offsetY / 44];
        }
    } else {
        [self.delegate viewController:self navigationBarTransformProgress:0];
    }
}


@end

//
//  HGAppDelegate.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-10.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGAppDelegate.h"
#import "HGAppData.h"
#import "HGGetuiManager.h"
#import <AdSupport/AdSupport.h>
#import "HGUtils.h"
#import <Parse/Parse.h>
#import "ACTReporter.h"
#import "UIColor+FlatColors.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "JSONKit.h"
#import <QuartzCore/QuartzCore.h>
#import "HGMessage.h"
#import "HGUser.h"
#import "HGHomeViewController.h"
#import "HGMessageCenterController.h"
#import "HGSearchViewController.h"
#import "KGStatusBar.h"
#import "HGCampaignsObject.h"
#import <Branch.h>
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/SDWebImagePrefetcher.h>
#import <Crashlytics/Crashlytics.h>
#import "UIStoryboard+Pinnacle.h"
#import "HGWebContentController.h"
#import "HGChatViewController.h"
#import "HGWelcomePageController.h"
#import "HGFBUtils.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "HGMyProfileViewController.h"
#import <Bolts/Bolts.h>
#import "HGSearchResultsController.h"
#import "HGItemDetailController.h"
#import "RavenClient.h"


@interface HGAppDelegate ()

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation HGAppDelegate 


void uncaughtExceptionHandler(NSException*exception){
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@",[exception callStackSymbols]);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
   
#ifdef DEBUG
     NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
#endif
    
    
    ////sentry crash catch recorder.
    RavenClient *client = [RavenClient clientWithDSN:@"https://13c87b26a5364ca29e6ae097375b5644:495bf44e02164eddad06cdf9ca70e76a@app.getsentry.com/44080"];
    [RavenClient setSharedClient:client];
    
    
    // Init the app data global object.
    [HGTrackingData sharedInstance];
    [HGUserData sharedInstance];

    [HGAppData sharedInstance];
    
    [HGAppData sharedInstance].bShowStartAD = YES;
    [HGUserData sharedInstance].userLaunchCounter++;
    [[HGAppData sharedInstance] prefetchConfiguration];
    
    
    
    
    
    
    [HGAppData sharedInstance].bOpenReviewFunctions = YES;
    
    // Override point for customization after application launch.
    [[UINavigationBar appearance] setTintColor:SYSTEM_DEFAULT_FONT_COLOR_1];
    [[UINavigationBar appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : SYSTEM_DEFAULT_FONT_COLOR_1 }];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    [SDImageCache sharedImageCache].maxCacheSize = 1024 * 1024 * 64; //64m disk cache
    [SDImageCache sharedImageCache].maxMemoryCost = 1024 * 1024 * 16; //16m memory cache
    
    // Parse Tracking Integration
    [Parse setApplicationId:@"bhhhEaxN6BOZIx46FpglH1ywSEPqH3lff6aeMxU1"
                  clientKey:@"8DNG6RPt7tr1Llk0etPZ2DZM5dpoUtD1oRq16lH7"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    // Google Analytics Tracking Integration
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 30;
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelNone];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-51595858-3"];
    
    // Google Conversion Tracking Integration
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"959032051"];
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"946158234"];
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"948571033"];
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"950327123"];
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"956801077"];
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"948163045"];
    
    
    [ACTConversionReporter reportWithConversionID:@"959032051" label:@"J1Q0CKeAllkQ89WmyQM" value:@"2.00" isRepeatable:NO];
    [ACTConversionReporter reportWithConversionID:@"946158234" label:@"R5OlCP7y5lwQmvWUwwM" value:@"0.00" isRepeatable:NO];
    [ACTConversionReporter reportWithConversionID:@"948571033" label:@"zafqCKa3nl0QmZeoxAM" value:@"0" isRepeatable:NO];
    [ACTConversionReporter reportWithConversionID:@"950327123" label:@"dBrMCOi6nl0Q066TxQM" value:@"0" isRepeatable:NO];
    [ACTConversionReporter reportWithConversionID:@"956801077" label:@"SDg1CIDDoV0QtcCeyAM" value:@"0" isRepeatable:NO];
    [ACTConversionReporter reportWithConversionID:@"948163045" label:@"KGsQCMTCoV0Q5aOPxAM" value:@"0" isRepeatable:NO];
    
    
    
    // AppsFlyer Integration
    if ([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
        [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"GH9moFNSa7tT3AkrV4Uend";
        [AppsFlyerTracker sharedTracker].appleAppID = @"917554930";
        [AppsFlyerTracker sharedTracker].customerUserID = [[HGUtils sharedInstance] advertisingIdentifier];
    }
    
    // Enable FBSDKProfile to automatically track the currentAccessToken:
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    /* Getui SDK Integeration */
    // [1]:使用APPID/APPKEY/APPSECRENT创建个推实例
    [[HGGetuiManager sharedManager] startSdk];
    
    // Register parse
    if (application.applicationState != UIApplicationStateBackground) {
        // Track an app open here if we launch with a push, unless
        // "content_available" was used to trigger a background push (introduced
        // in iOS 7). In that case, we skip tracking here to avoid double
        // counting the app-open.
        BOOL preBackgroundPush = ![application respondsToSelector:@selector(backgroundRefreshStatus)];
        BOOL oldPushHandlerOnly = ![self respondsToSelector:@selector(application:didReceiveRemoteNotification:fetchCompletionHandler:)];
        BOOL noPushPayload = ![launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (preBackgroundPush || oldPushHandlerOnly || noPushPayload) {
            [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
        }
    }
    
    // [2]:注册APNS
    [self registerRemoteNotification];
    
    
    // [2-EXT]: 获取启动时收到的APN
    NSDictionary* message = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (message) {
        NSString *payloadMsg = [message objectForKey:@"payload"];
        NSString *record = [NSString stringWithFormat:@"[APN]%@, %@", [NSDate date], payloadMsg];
        NSDictionary* payloadDict = [payloadMsg objectFromJSONString];
        
        [[[RACObserve(self, tabController) ignore:nil] take:1] subscribeNext:^(id x){
            [self offlineStatusHandlePayloadMsg:payloadDict];
        }];
        
        if (record.length > 0 ) {
            NSLog(@"received msg from getui while starting app: %@", record);
        }
    }
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
    [[Branch getInstance] initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
        if (!error) {
            NSLog(@"deep branch link data: %@", [params description]);
            [self branchActionHandle:params];
        }
    }];
    
    
    
  
    [self setFB_advertisingID];
    
    
    if( [CLLocationManager locationServicesEnabled] ){
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = 500;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        [self.locationManager startUpdatingLocation];
    }else{
        NSLog(@"Cannot Starting CLLocationManager!");
    }
    
    
    

    
    RACSignal *signalGetuiInstalltionID = [[[RACObserve([HGGetuiManager sharedManager], clientId) ignore:nil] ignore:@""] distinctUntilChanged];
    RACSignal *signalParseInstallationID = [[[RACObserve([PFInstallation currentInstallation], installationId) ignore:nil] ignore:@""] distinctUntilChanged];
    RACSignal *signalDeviceUUID = [[[RACObserve([[UIDevice currentDevice] identifierForVendor], UUIDString) ignore:nil] ignore:@""] distinctUntilChanged];
    RACSignal *signalUserID = [[[RACObserve([HGUserData sharedInstance], fmUserID) ignore:nil] ignore:@""] distinctUntilChanged];
    RACSignal *signalUserToken = [[[RACObserve([HGUserData sharedInstance], fmUserToken) ignore:nil] ignore:@""] distinctUntilChanged];
    
    
    [[[RACSignal combineLatest:@[signalGetuiInstalltionID, signalDeviceUUID, signalUserID, signalUserToken]] delay:5]
                subscribeNext:^(RACTuple *racTuple) {
                    
                    NSString *installtionID = [racTuple first];
                    NSString *deviceUUID = [racTuple second];
                    NSString *userID = [racTuple third];
             
                    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"register_device/" parameters:@{
                                   @"provider":@"getui",
                                   @"installation_id":installtionID,
                                   @"device_id":deviceUUID,
                                   @"push_token":@""
                    } success:^(NSURLSessionDataTask *task, id responseObject) {
                        NSLog(@"[Getui] Bind client result: %@, client id: (%@), device uuid: (%@), user id: (%@)", [responseObject objectForKey:@"message"], installtionID, deviceUUID, userID);
                    } failure:^(NSURLSessionDataTask *task, NSError *error) {
                        NSLog(@"[Getui] Bind client failed: %@, client id: (%@), device uuid: (%@), user id: (%@)", error.userInfo, installtionID, deviceUUID, userID);
                    }];
     }];
    
    [[[RACSignal combineLatest:@[signalParseInstallationID, signalDeviceUUID, signalUserID, signalUserToken]] delay:5]
                subscribeNext:^(RACTuple *racTuple) {
                    
                    NSString *installtionID = [racTuple first];
                    NSString *deviceUUID = [racTuple second];
                    NSString *userID = [racTuple third];
                    
                    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"register_device/" parameters:@{
                             @"provider":@"parse",
                             @"installation_id":installtionID,
                             @"device_id":deviceUUID,
                             @"push_token":@""
                     } success:^(NSURLSessionDataTask *task, id responseObject) {
                         NSLog(@"[Parse] Bind client result: %@, client id: (%@), device uuid: (%@), user id: (%@)", [responseObject objectForKey:@"message"], installtionID, deviceUUID, userID);
                     } failure:^(NSURLSessionDataTask *task, NSError *error) {
                         NSLog(@"[Parse] Bind client failed: %@, client id: (%@), device uuid: (%@), user id: (%@)", error.userInfo, installtionID, deviceUUID, userID);
                     }];
     }];

    if (launchOptions[UIApplicationLaunchOptionsURLKey] == nil) {
        [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
            if (error) {
                NSLog(@"Received error while fetching deferred app link %@", error);
            }
            if (url) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }];
    }
    if([launchOptions count] > 0){
        [self application:application openURL:[launchOptions objectForKey:UIApplicationLaunchOptionsURLKey] sourceApplication:[launchOptions objectForKey:UIApplicationLaunchOptionsSourceApplicationKey] annotation:nil];
    }
    
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

-(void)setFB_advertisingID
{
    
    NSString *idfaString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    int advertisingValue = [[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]?1:0;
    
    
    [[HGAppData sharedInstance].userApiClient.requestSerializer setValue:idfaString forHTTPHeaderField:@"advertiser_id"];
    [[HGAppData sharedInstance].userApiClient.requestSerializer setValue:@"" forHTTPHeaderField:@"attribution"];
    [[HGAppData sharedInstance].userApiClient.requestSerializer setValue:[@"" stringByAppendingFormat:@"%d",advertisingValue] forHTTPHeaderField:@"advertiser_tracking_enabled"];
    [[HGAppData sharedInstance].userApiClient.requestSerializer setValue:@"1" forHTTPHeaderField:@"application_tracking_enabled"];
    
}



-(void)getApplicationServerSuggestion
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    
    if([HGUserData sharedInstance].fmUserToken != nil && [HGUserData sharedInstance].fmUserToken.length > 0)
    {
        [request setValue:[HGUserData sharedInstance].fmUserToken forHTTPHeaderField:@"X-FIVEMILES-USER-TOKEN"];
        
    }
    
    if ([HGUserData sharedInstance].fmUserID != nil && [HGUserData sharedInstance].fmUserID.length > 0) {
        [request setValue:[HGUserData sharedInstance].fmUserID forHTTPHeaderField:@"X-FIVEMILES-USER-ID"];
    }
    {
        //add user Agent
        NSString * versionBuildString = [NSString stringWithFormat: @"%@(%@)",
                                         [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"],
                                         [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey]];
        NSString *deviceType = [[HGUtils sharedInstance] deviceString];
        NSString *osVersion = [UIDevice currentDevice].systemVersion;
        NSString *preferredLanguages = [[NSLocale preferredLanguages] firstObject];
        
        NSString *userAgentString = [@"" stringByAppendingFormat:@"os:ios,os_version:%@,app_version:%@,device:%@,Accept-Language:%@",osVersion,versionBuildString,deviceType,preferredLanguages];
        [request setValue:userAgentString forHTTPHeaderField:@"User-Agent"];
    }
    
    
    
    NSString* baseUrl = [[FMBizAPIBaseURLString componentsSeparatedByString:@"/v"] objectAtIndex:0];
    baseUrl = [baseUrl stringByAppendingString:@"/init/"];
    
    [request setURL:[NSURL URLWithString:baseUrl]];
     
    //Send an asynchronous request so it doesn't pause the main thread
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSHTTPURLResponse *responseCode = (NSHTTPURLResponse *)response;
        if ([responseCode statusCode] == 200)
        {
            if (data == nil) {
                return;
            }
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingAllowFragments error:nil];
            NSDictionary* switcherDict = [dict objectForKey:@"switchers"];
            NSString* reviewValue = [switcherDict objectForKey:@"review"];
            if (reviewValue != nil) {
                if(reviewValue.intValue == 1)
                {
                    [HGAppData sharedInstance].bOpenReviewFunctions = 1;
                }
                else
                {
                    [HGAppData sharedInstance].bOpenReviewFunctions = 0;
                }
            }
            
            
            NSArray* searchMenus = [dict objectForKey:@"categories"];
          

            if(![searchMenus isEqual:[NSNull null]] && searchMenus != nil && searchMenus.count > 0)
            {
                NSString* categoryJsonString = [searchMenus JSONString];
                [[NSUserDefaults standardUserDefaults] setValue:categoryJsonString forKey:SERVER_CATEGORY_LIST_V3];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                if(searchMenus.count > 0)
                {
                    [[HGAppInitData sharedInstance].categoryArray_c1 removeAllObjects];
                }
                
                for (int i = 0; i<searchMenus.count; i++) {
                    NSDictionary* searchSingleMenu = [searchMenus objectAtIndex:i];
                    NSError * error;
                    HGCategory* c = [[HGCategory alloc] initWithDictionary:searchSingleMenu error:&error];
                    if ([searchSingleMenu valueForKey:@"child"]) {
                        NSArray* subCategories = [searchSingleMenu valueForKey:@"child"];
                    
                        for (int j = 0 ; j< subCategories.count; j++) {
                            NSDictionary* subCategoryDic =  [subCategories objectAtIndex:j];
                             HGCategory* c_level2 = [[HGCategory alloc] initWithDictionary:subCategoryDic error:&error];
                            [c.subCategories addObject:c_level2];
                        }
                        
                    }
                    [[HGAppInitData sharedInstance].categoryArray_c1 addObject:c];
                }
            }
            
            
        }
        
    }];

}
-(void)getCampaignsObjects
{
    //get system AD informations
    {
        NSNumber * typeNumber = [[NSNumber alloc] initWithInt:1];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"campaigns/" parameters:@{@"type":typeNumber} success:^(NSURLSessionDataTask *task, id responseObject) {
            
            
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            if (results.count > 0)
            {
                NSDictionary* dictObj = [results objectAtIndex:0];
                NSError * error = nil;
                [HGAppData sharedInstance].startADCampaign = [[HGCampaignsObject alloc] initWithDictionary:dictObj error:&error];
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"fail to get campaign information");
        }];

    }
    
    //
    {
        NSNumber * typeNumber2 = [[NSNumber alloc] initWithInt:2];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"campaigns/" parameters:@{@"type":typeNumber2} success:^(NSURLSessionDataTask *task, id responseObject) {
            
            
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            
            for (id dictObj in results) {
                NSError * error = nil;
                [HGAppData sharedInstance].homeBanner = [[HGCampaignsObject alloc] initWithDictionary:dictObj error:&error];
                if (!error ) {
                    
                } else {
                    
                }
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"fail to get campaign information");
        }];
    }
   

}


-(void)branchActionHandle:(NSDictionary*)params
{
    
    
    if ([params valueForKey:@"share_type"] != nil) {
        //using new standard... share_type : invite, item, shop, unknown
        if ([[params valueForKey:@"share_type"] isEqualToString:@"item"]) {
            NSString* valueString = [params valueForKey:@"event_id"];
            [self startItemPage:valueString];
        }
        if ([[params valueForKey:@"share_type"] isEqualToString:@"shop"]) {
            NSString* valueString = [params valueForKey:@"event_id"];
            [self startUserPage:valueString];
        }
        if ([[params valueForKey:@"share_type"] isEqualToString:@"invite"]) {
            NSString* valueString = [params valueForKey:@"event_id"];
            [self startUserPage:valueString];
        }
    }
    

}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    NSLog(@"Opened app by url : %@", [url absoluteString]);
    
    BFURL *parsedUrl = [BFURL URLWithInboundURL:url sourceApplication:sourceApplication];
    
    if ([[Branch getInstance] handleDeepLink:url]) {
        return YES;
    }else if ([parsedUrl appLinkData]) {
        [self handleFBDeeplinkAction: [parsedUrl targetQueryParameters]];
        return YES;
    }else if ([[url scheme] isEqualToString:@"fivemiles"]) {
        [self handleAppSchemeAction: [url absoluteString]];
        return YES;
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
    
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[HGGetuiManager sharedManager] stopSdk];
    [self adaptWithStatusBarHeight];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self adaptWithStatusBarHeight];
    
    [[HGFBUtils sharedInstance] removeObserver];
}

- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame{
    [self adaptWithStatusBarHeight];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    [[HGGetuiManager sharedManager] startSdk];
    
    [self getApplicationServerSuggestion];
    [self getCampaignsObjects];
    
    [FBSDKAppEvents activateApp];
    [[HGFBUtils sharedInstance] addObserver];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [self.locationManager stopUpdatingLocation];
    
    [[HGAppData sharedInstance] saveAppDataToDisk];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"privacylimit" action:@"allownotification" label:nil value:nil];
    
    NSLog(@"deviceToken:%@", deviceToken);
    
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (token==nil) {
        //to do , replace the token with self define id ..
        token = @"this_device_token_is_nil";
    }
    // Register device token on 5miles app server
    [HGAppData sharedInstance].deviceToken = token;
    
    // [3]:向个推服务器注册deviceToken
    [[HGGetuiManager sharedManager] setDeviceToken:token];
    
    // Store the deviceToken in the current Installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceToken:token];
    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!succeeded) {
            NSException* ex = [[NSException alloc] initWithName:@"ExceptionName"                                 reason:@"parser Installation id save fail"
                userInfo:nil];
            
            [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",ex]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];

            
            //retry to save installation id.
            PFInstallation *currentInstallation = [PFInstallation currentInstallation];
            [currentInstallation setDeviceToken:token];
            [currentInstallation saveInBackground];
        }
    }];    

    [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(checkPushNotificationStatus) userInfo:nil repeats:NO];
}


-(int)checkPushNotificationStatus
{
    int pushEnabled=0 ;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
            pushEnabled=1;
        }
        else
            pushEnabled=0;
    }
    else
    {
        UIRemoteNotificationType types = [[UIApplication sharedApplication]        enabledRemoteNotificationTypes];
        if (types & UIRemoteNotificationTypeAlert)
            pushEnabled=1;
        else
            pushEnabled=0;
    }

    NSLog(@"devic push notification status is %d",pushEnabled );
    return pushEnabled;
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"remote notification registration failed: %@", error);
    
    [HGAppData sharedInstance].deviceToken = @"";
    [[HGGetuiManager sharedManager] setDeviceToken:@""];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
   
    if ( application.applicationState == UIApplicationStateActive )
    {
          // app was already in the foreground
        NSString *payloadMsg = [userInfo objectForKey:@"payload"];
        NSString *record = [NSString stringWithFormat:@"[APN]%@, %@", [NSDate date], payloadMsg];
        if(record != nil )
        {
            NSLog(@"received remote notification: %@", record);
        }
        NSDictionary* payloadDict = [payloadMsg objectFromJSONString];
        [self RunningStatusHandlePayloadMsg:payloadDict];

    }
  
    else
    {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
        // app was just brought from background to foreground
        // [4-EXT]:处理APN
        NSString *payloadMsg = [userInfo objectForKey:@"payload"];
        NSString *record = [NSString stringWithFormat:@"[APN]%@, %@", [NSDate date], payloadMsg];
        NSDictionary* payloadDict = [payloadMsg objectFromJSONString];
        [self offlineStatusHandlePayloadMsg:payloadDict];
        if(record.length > 0 )
        {
            NSLog(@"received remote notification: %@", record);
        }

    }
}


#pragma mark - CLLocationManagerDelegate


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation * location = [locations lastObject];
    
    [HGAppData sharedInstance].currentLocation = location;
    [HGAppData sharedInstance].GPSlat = location.coordinate.latitude;
    [HGAppData sharedInstance].GPSlon = location.coordinate.longitude;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if(status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse){
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"privacylimit" action:@"allowgps" label:nil value:nil];
    }else if(status == kCLAuthorizationStatusDenied){
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"privacylimit" action:@"notallowgps" label:nil value:nil];
    }
}

#pragma mark - Tabbar Controller Delegate


static int lastSelectedIndex = -1;
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    NSLog(@"tab bar controller did select view ");
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        [(UINavigationController *)viewController popToRootViewControllerAnimated:NO];
    }
    
    if (lastSelectedIndex == 0) {
        if (tabBarController.selectedIndex == 0) {
            //when it is already in home view controller , start to fresh home
            UINavigationController* Navi = tabBarController.viewControllers[0];
            HGHomeViewController* homeViewcontroller= Navi.viewControllers[0];
            [homeViewcontroller freshHomeItem];
        }
    }
    
    lastSelectedIndex = (int)tabBarController.selectedIndex;

}



- (void)logUserOut
{
    UINavigationController *naviHome = (UINavigationController *)[self.tabController setTab1SelectedStyle];
    HGHomeViewController *vcHome = (HGHomeViewController *)[naviHome.viewControllers firstObject];
    [vcHome showNearbyView];
    
    //clear branch account
    [[Branch getInstance] logout];
    
    // Clear logged info
    [[HGUserData sharedInstance] clearUserInfo];

#ifndef DEBUG
    ////for hu gang's server
    [[HGAppData sharedInstance].chatApiClient FIVEMILES_POST:@"unregister/" parameters:@{@"device_id":    [HGAppData sharedInstance].deviceToken} success:nil failure:nil];
#endif
    
    //for longzhi's server.
    
    NSString* installId = [PFInstallation currentInstallation].installationId;
    if (installId==nil) {
        installId = @"";
    }
    [[HGAppData sharedInstance].chatApiClient FIVEMILES_POST:@"unregister_device/" parameters:@{@"provider":    @"parse", @"installation_id":installId} success:nil failure:nil];
    
    NSString* clientid = [HGGetuiManager sharedManager].clientId;
    if (clientid==nil) {
        clientid = @"";
    }
    [[HGAppData sharedInstance].chatApiClient FIVEMILES_POST:@"unregister_device/" parameters:@{@"provider": @"getui", @"installation_id":clientid} success:nil failure:nil];
    
    
    
    {
        UINavigationController * navWelcome = (UINavigationController *)[[UIStoryboard loginStoryboard] instantiateViewControllerWithIdentifier:@"NavWelcome"];
        
        [self.tabController presentViewController:navWelcome animated:YES completion:nil];
    }
    
}

#pragma mark - Helpers

- (void)adaptWithStatusBarHeight{
    CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;

    if( statusBarHeight == 40){
        self.tabController.fmTabbarView.frame = CGRectMake(0, SCREEN_HEIGHT - self.tabController.suggestedTabbarHeight - 20,self.tabController.view.frame.size.width,self.tabController.suggestedTabbarHeight);
    }else if(statusBarHeight == 20){
        self.tabController.fmTabbarView.frame = CGRectMake(0, SCREEN_HEIGHT - self.tabController.suggestedTabbarHeight,self.tabController.view.frame.size.width,self.tabController.suggestedTabbarHeight);
    }
}


- (void)registerRemoteNotification
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        NSLog(@"using iphone 8.0 push register function");
        UIUserNotificationSettings *uns = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:uns];
        [[UIApplication sharedApplication] registerForRemoteNotifications];

    }
    else {
        //ios 7 do not respondsto registerForRemoteNotificationTypes?  ---> todo.
        NSLog(@"using iphone older(before 8.0) push register function");
        UIRemoteNotificationType apn_type = (UIRemoteNotificationType)(UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge);
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:apn_type];
    }
}

-(void)RunningStatusHandlePayloadMsg:(NSDictionary*)msgDict
{
    /*
    NSString* msgType = [msgDict objectForKey:@"t"];
    if ([msgType isEqualToString:@"o"])
    {
         [KGStatusBar showSystemNotification:@"New Chat Message!"];
    }
    if ([msgType isEqualToString:@"s"]) {
         [KGStatusBar showSystemNotification:@"New notification!"];
    }
     */
}


-(void)offlineStatusHandlePayloadMsg:(NSDictionary*)msgDict
{
    
    NSString* msgType = [msgDict objectForKey:@"t"];
    NSString* rf_tag =[msgDict objectForKey:@"r"];
    if (rf_tag != nil && rf_tag.length >0) {
        
    }
    else{
        rf_tag = @"";
    }
    
    if ([msgType isEqualToString:@"o"])
    {
        //offer or chat.
        NSString* offerlineId = [msgDict objectForKey:@"olid"];
        
        HGMessage * message = [[HGMessage alloc] init];
        message.threadID = offerlineId;
        
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"push" action:@"offer_open" label:nil value:nil];
        HGChatViewController *vcChat = (HGChatViewController *)[[UIStoryboard messageStoryboard]instantiateViewControllerWithIdentifier:@"VCChat"];
        vcChat.bFromItemDetailPage = NO;
        vcChat.offerLineID = offerlineId;
        UINavigationController *naviHome = (UINavigationController *)[self.tabController setTab1SelectedStyle];
        [naviHome pushViewController:vcChat animated:YES];
    }
       
    if ([msgType isEqualToString:@"s"]) {
        //system
        NSString* actionString = [msgDict objectForKey:@"a"];
        
        [self recordPushGAInformation:actionString];
        
        if ([actionString isEqualToString:@"h"]) {

            UINavigationController* naviHome = [self.tabController setTab1SelectedStyle];
            [naviHome popToRootViewControllerAnimated:YES];
        }
        if ([actionString isEqualToString:@"m"]) {
            
            UINavigationController* naviMessageCenter = [self.tabController setTab4SelectedStyle];
            [naviMessageCenter popToRootViewControllerAnimated:YES];
        }
        if ([actionString isEqualToString:@"c"]) {
            
            UINavigationController* naviSearch = [self.tabController setTab2SelectedStyle];
            [naviSearch popToRootViewControllerAnimated:YES];
        }
        if ([actionString hasPrefix:@"n"]) {
            //push to message center, with notification segment selected.
            UINavigationController* naviMessageCenter = [self.tabController setTab4SelectedStyle];
            
            HGMessageCenterController* messageController =  naviMessageCenter.viewControllers[0];
            messageController.bShowNotificationSegment = YES;
            
            [messageController CheckShowNotificationSegment];
            [messageController ResetContentOffset];
            [naviMessageCenter popToRootViewControllerAnimated:YES];
        }
        if ([actionString hasPrefix:@"r"]) {
            //push to review list controller.
            
            UINavigationController* naviSearch = [self.tabController setTab5SelectedStyle];
            HGMyProfileViewController* myprofile= naviSearch.viewControllers[0];
            [myprofile performSegueWithIdentifier:@"ShowSellerRatings" sender:nil];
             
            
        }
        
        
        if ([actionString hasPrefix:@"c:"]) {
            //category
            NSString* valueString = [actionString substringFromIndex:2];
           
            
            NSDictionary* dict = @{@"categoryKey":valueString, @"rf_tag": rf_tag};
            UINavigationController* naviSearch = [self.tabController setTab2SelectedStyle];
            HGSearchViewController* searchViewController= naviSearch.viewControllers[0];
            [searchViewController performSegue:dict withSegueName:@"ShowCategorySearchResults"];
        }
        if ([actionString hasPrefix:@"s:"]) {
            //s:#{keyword}
            NSString* keywordString = [actionString substringFromIndex:2];
            
            NSDictionary* dict = @{@"keyword":keywordString, @"rf_tag":rf_tag};
            
            UINavigationController* naviSearch = [self.tabController setTab2SelectedStyle];
            HGSearchViewController* searchViewController= naviSearch.viewControllers[0];
            [searchViewController performSegue:dict withSegueName:@"ShowKeywordSearchResult"];
        }
        if ([actionString hasPrefix:@"s2:"]) {
            //s2:#{keyword},cid:#{campain_id}
            NSArray* strings = [actionString componentsSeparatedByString:@","];
            NSString* keyString = [[strings objectAtIndex:0] substringFromIndex:3];
            NSString* cidString = [[strings objectAtIndex:1] substringFromIndex:4];
            
            NSDictionary* dict = @{@"keyword":keyString,@"cid":cidString, @"rf_tag":rf_tag};

            UINavigationController* naviSearch = [self.tabController setTab2SelectedStyle];
            HGSearchViewController* searchViewController= naviSearch.viewControllers[0];
            
            [searchViewController performSegue:dict withSegueName:@"ShowKeywordSearchResult"];
        }
        
        if ([actionString hasPrefix:@"p:"]) {
            //p:#{user_id}
            
            NSString* valueString = [actionString substringFromIndex:2];
            if (valueString != nil) {
                [self startUserPage:valueString];
            }
        }
        
        if ([actionString hasPrefix:@"i:"]) {
            //i:#{item_id}
           NSString* valueString = [actionString substringFromIndex:2];
           
            [self startItemPage:valueString];
            
        }
        if ([actionString hasPrefix:@"u:"]) {
            //u:#{url}
            NSString* valueString = [actionString substringFromIndex:2];
            
            UINavigationController *naviMessageCenter = [self.tabController setTab4SelectedStyle];

            HGWebContentController *vcWebContent = (HGWebContentController *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithIdentifier:@"VCWebContent"];
            vcWebContent.contentLink = valueString;
            
            [naviMessageCenter pushViewController:vcWebContent animated:YES];
        }
        if ([actionString hasPrefix:@"f:"]) {
            //f:fid{ system recommend id}
            NSString* valueString = [actionString substringFromIndex:2];
            [self startSystemThemeItem:valueString];
        }
        
        
    }
}
-(void)recordPushGAInformation:(NSString*)actionString
{
    if ([actionString hasPrefix:@"r"] || [actionString hasPrefix:@"p:"] || [actionString hasPrefix:@"i:"]) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"push" action:@"businessnotification_open" label:nil value:nil];
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"push" action:@"marketnotification_open" label:nil value:nil];
        
    }
    
}

-(void)startSystemThemeItem:(NSString*)valueString
{
    
    [HGAppData sharedInstance].recommend_themeID = valueString.intValue;
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController * sysThemeNavi = [storyBoard instantiateViewControllerWithIdentifier:@"systemThemeNavi"];
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    sysThemeNavi.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    [appDelegate.tabController presentViewController:sysThemeNavi animated:YES completion:nil];
    

}

-(void)startItemPage:(NSString*)valueString
{
    [[RACSignal combineLatest:@[[[RACObserve(self, tabController) ignore:nil] take:1], [[[RACObserve([HGUserData sharedInstance], fmUserID) ignore:nil] ignore:@""] take:1]]] subscribeNext:^(id x) {
        HGShopItem* item = [[HGShopItem alloc] init];
        item.uid = valueString;
        
        UINavigationController* naviHome = [self.tabController setTab1SelectedStyle];
        HGHomeViewController* vcHome= [naviHome.viewControllers firstObject];
        [vcHome performSegue:item withSegueName:@"ShowItemPage"];
    }];
}

-(void)startUserPage:(NSString*)valueString
{
    [[RACSignal combineLatest:@[[[RACObserve(self, tabController) ignore:nil] take:1], [[[RACObserve([HGUserData sharedInstance], fmUserID) ignore:nil] ignore:@""] take:1]]] subscribeNext:^(id x) {
        HGUser* user = [[HGUser alloc] init];
        user.uid = valueString;
        user.displayName = @"";
        user.portraitLink = @"";
        
        UINavigationController *naviHome = [self.tabController setTab1SelectedStyle];
        HGHomeViewController* vcHome= [naviHome.viewControllers firstObject];
        [vcHome performSegue:user withSegueName:@"ShowUserDetail"];
    }];
}

- (void)startSearchPage:(NSString *)valueString{

    [[RACSignal combineLatest:@[[[RACObserve(self, tabController) ignore:nil] take:1], [[[RACObserve([HGUserData sharedInstance], fmUserID) ignore:nil] ignore:@""] take:1]]] subscribeNext:^(id x) {
        
        UINavigationController *naviSearch = (UINavigationController *)[self.tabController setTab2SelectedStyle];
        HGSearchViewController *vcSearch= [naviSearch.viewControllers firstObject];
        
        HGSearchResultsController *vcSearchResults = (HGSearchResultsController *)[[UIStoryboard searchStoryboard]instantiateViewControllerWithIdentifier:@"VCSearchResults"];
        vcSearchResults.searchMode = HGSearchModeKeyword;
        vcSearchResults.searchPayload = @{@"keyword":valueString};
        vcSearchResults.title = valueString;
        
        [vcSearch.navigationController pushViewController:vcSearchResults animated:YES];
    }];
}

-(void)handleAppSchemeAction:(NSString*)actionString
{
    
    //todo
    if([actionString rangeOfString:@"item/"].location != NSNotFound){
        NSRange range = [actionString rangeOfString:@"item/"];
        NSString* valueString = [actionString substringFromIndex:range.location + range.length ];
        if (valueString == nil) {
            return;
        }
        [self startItemPage:valueString];

    }else if ([actionString rangeOfString:@"person/"].location != NSNotFound) {
        NSRange range = [actionString rangeOfString:@"person/"];
        NSString* valueString = [actionString substringFromIndex:range.location + range.length ];
        if (valueString == nil) {
            return;
        }
        [self startUserPage:valueString];
    }else if ([actionString rangeOfString:@"search/"].location != NSNotFound) {
        NSRange range = [actionString rangeOfString:@"search/"];
        NSString* valueString = [actionString substringFromIndex:range.location + range.length ];
        if (valueString == nil) {
            return;
        }
        [self startSearchPage:valueString];
    }

}

-(void)handleFBDeeplinkAction:(NSDictionary *)queryParatmers{
    
    if([queryParatmers objectForKey:@"item"]){
        NSString *itemID = [queryParatmers objectForKey:@"item"];
        [self startItemPage:itemID];
    }else if([queryParatmers objectForKey:@"person"]){
        NSString *userID = [queryParatmers objectForKey:@"person"];
        [self startUserPage:userID];
    }else if([queryParatmers objectForKey:@"search"]){
        NSString *keyword = [queryParatmers objectForKey:@"search"];
        [self startSearchPage:keyword];
    }
}


@end

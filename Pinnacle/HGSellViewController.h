//
//  HGSellViewController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-15.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "HGPlayButton.h"
//#import "F3BarGauge.h"
#import "HGRoundCornerView.h"
#import "BDKNotifyHUD.h"
#import <SZTextView.h>
#import "HGAppData.h"
#import "HGCategoryPickController.h"
#import "HGCurrencySelectController.h"
#import "HGHorizontalTableView.h"
#import "HGPhotoSlotsController.h"
#import "HGVoiceRecordController.h"
#import "HGUtils.h"
#import "UIImage+Scale.h"
#import <Cloudinary/Cloudinary.h>
#import <CTAssetsPickerController.h>
#import <PXAlertView.h>
#import <PXAlertView+Customization.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SVProgressHUD.h>
#import "HGItemBrandViewController.h"
#import "HGShopItem.h"
#import "HGDeliveryTableViewController.h"
#import "HGConstant.h"
#import "HGAppData.h"
#import "HGUtils.h"
#import "HGAppDelegate.h"
#import <SDWebImage/SDImageCache.h>
#import "HGCategory.h"
#import "HGFBUtils.h"
#import "HGSellPageInputDataManager.h"
#import "HGAssetsHelper.h"
#import "UIStoryboard+Pinnacle.h"


@interface HGSellViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITableViewCell *titleCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *category_cell;
@property (weak, nonatomic) IBOutlet UITableViewCell *listingPrice_cell;
@property (weak, nonatomic) IBOutlet UITableViewCell *locationCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *descriptionCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *voiceRecordCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *originPriceCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *brandCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *delivery_cell;

@property (weak, nonatomic) IBOutlet UITableViewCell *ShareOnFaceBookCell;


@property (weak, nonatomic) IBOutlet HGRoundCornerView *voiceCellRoundCornerView;
@property (weak, nonatomic) IBOutlet UIImageView *iconMicrophone;
@property (nonatomic, strong) BDKNotifyHUD* voiceStatusHud;

@property (weak, nonatomic) IBOutlet UILabel *locationTitle;

///audio slide view
@property (nonatomic, strong)UIView* audio_slideView;
@property (nonatomic, strong)UILabel* audio_label1;
@property (nonatomic, strong)UILabel* audio_label2;
@property (strong, nonatomic) UISlider *audio_slide;
@property (strong, nonatomic) NSTimer* audioStatusUpdateTimer;
@property (strong, nonatomic) UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UILabel *audio_userHintLabel;

//end of audio slide view


@property (weak, nonatomic) IBOutlet UIButton *btnRecord;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;

@property (weak, nonatomic) IBOutlet UISwitch *shareOnfacebookSwitch;

@property (weak, nonatomic) IBOutlet UITextField *shareOnFacebookLabel;






@property (nonatomic, assign) int ItemDeliveryType;

@property (nonatomic, assign) int EditModel;





@end

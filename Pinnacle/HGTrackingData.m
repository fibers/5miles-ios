//
//  HGTrackingData.m
//  Pinnacle
//
//  Created by Alex on 15-4-20.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGTrackingData.h"

@implementation HGTrackingData

+ (HGTrackingData *)sharedInstance
{
    static HGTrackingData * _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[HGTrackingData alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.home_rf_tag = @"";
        self.search_rf_tag = @"";
        self.searchCategory_rf_tag = @"";
        self.suggest_rf_tag = @"";
        self.brand_item_rf_tag = @"";
        self.search_items_rf_tag = @"";
        self.user_likes_rf_tag = @"";
        
    }
    return self;
}

@end

//
//  HGUserData.m
//  Pinnacle
//
//  Created by Alex on 15/5/4.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGUserData.h"
#import "HGAppData.h"
#import <objc/runtime.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation HGUserData

+ (HGUserData *)sharedInstance{
    static HGUserData *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[HGUserData alloc] init];
    });

    return instance;
}

- (instancetype) init{
    self = [super init];
    if(self){
        
        [self setDefaultValues];
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        unsigned int numberOfProperties = 0;
        objc_property_t *propertyArray = class_copyPropertyList([self class], &numberOfProperties);
        
        for (NSUInteger i=0; i<numberOfProperties; i++)
        {
            BOOL bObjectType = NO;
            
            objc_property_t property = propertyArray[i];
            NSString *propertyName = [[NSString alloc] initWithUTF8String:property_getName(property)];
            NSString *type = [[NSString alloc] initWithUTF8String:property_copyAttributeValue(property, "T")];
            
            if([type hasPrefix:@"@"]){
                bObjectType = YES;
            }
            
            type = [type stringByReplacingOccurrencesOfString:@"@" withString:@""];
            type = [type stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            
            id value = [standardUserDefaults objectForKey:[self generateKey:propertyName]];
            
            // If there is a initliaze action. Otherwise set the value directly.
            NSString *initAction = [propertyName stringByAppendingString:@"Init"];
            if(!value){
                if([self respondsToSelector:NSSelectorFromString(initAction)]){
                    SEL selector = NSSelectorFromString(initAction);
                  
                    [self performSelector:selector withObject:nil afterDelay:0];
                }else{
                    id defaultEmptyValue = [self getDefaultEmptyValue:type];
                    [self setValue:defaultEmptyValue forKey:propertyName];
                }
            }else{
                [self setValue:value forKey:propertyName];
            }
            
            @weakify(self);
            [[[self rac_valuesForKeyPath:propertyName observer:self] distinctUntilChanged] subscribeNext:^(id x) {
                @strongify(self);
                
                if(!x || (bObjectType && ![x isKindOfClass:NSClassFromString(type)])){
                    id defaultEmptyValue = [self getDefaultEmptyValue:type];
                    [self setValue:defaultEmptyValue forKey:propertyName];
                }else{
                    
                    [[NSUserDefaults standardUserDefaults] setObject:x forKey:[self generateKey:propertyName]];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSString *valueChangedAction = [propertyName stringByAppendingString:@"ChangedAction:"];
                    
                    if([self respondsToSelector:NSSelectorFromString(valueChangedAction)]){
                        NSLog(@"Perform value changed action (%@) for (%@).", valueChangedAction, propertyName);
                        [self performSelector:NSSelectorFromString(valueChangedAction) withObject:x afterDelay:0];
                    }
                }
            }];
        }
    }
    return self;
}


#pragma mark - Setup
- (NSDictionary *)defaultValues{
    return @{
            /************** Global Data ***************/
            @"fmUserID" : @"",
            @"fmUserToken" : @"",
            @"userDisplayName" : @"",
            @"userAvatarLink" : @"",
            @"userEmail" : @"",
            @"zipcodeString":@"",
            @"zipLat":@"",
            @"zipLon":@"",
            @"zipCountry":@"",
            @"zipRegion":@"",
            @"zipCity":@"",
            
            @"bLoggedIn" : @(NO),
            @"bAnonymous" : @(NO),
            @"bUserAvatarVerified" : @(NO),
            @"bUserEmailVerified" : @(NO),
            @"bUserFBVerified" : @(NO),
            @"bUserPhoneVerified" : @(NO),
            @"bUserForceCloseZipView":@(NO),
            
            @"accountType" : @(0),
            @"facebookUserID" : @"",
            @"facebookToken" : @"",
            @"facebookTokenExpirationDate" : [NSDate distantFuture],

            /************** Local Data ***************/
            @"bShowVerifyRemind" : @(NO),
            @"bSellingFirstTime" : @(YES),
            @"serviceSellingFirstTime" : @(YES),
            @"firstClosedProfilePhonePage" : @(YES),
            @"showServiceVerifyRemind" : @(NO),
            @"bNeedProfileIntro" : @(YES),
            @"bNeedShakingIntro" : @(YES),
            @"bHomeShowFBLike" : @(YES),
            @"bHomeShowRateApp" : @(YES),
            @"bHomeShowUserNearby" : @(YES),
            @"bHomeShowFollowInstruction" : @(YES),
            @"bOtherUserViewShowRatingInstruction" : @(YES),
            @"bMessageTabShowInstruction" : @(YES),
            @"bChatShowReviewTip" : @(YES),
            @"bChatShowSendMediaTip" : @(YES),
            @"bShowRatingAppRemind_MarkAsSold" : @(YES),
            @"bShowRatingAppRemind_FirstReview5star" : @(YES),
            @"bSellingFBShare" : @(YES),
            @"bPromptFBShare" : @(YES),
            @"enterChatCounter" : @(0),
            @"userLaunchCounter" : @(0),
            @"userLocalProfile" : @{},
            @"userLocalBrand" : @[],
            @"userLocalMapSearchPOIs":@[]
            };
}

- (NSArray *)globalData{
    return @[
             @"fmUserID",
             @"fmUserToken",
             @"userDisplayName",
             @"userAvatarLink",
             @"userEmail",
             
             @"bShowVerifyRemind",
             @"bSellingFirstTime",
             @"serviceSellingFirstTime",
             @"firstClosedProfilePhonePage",
             @"showServiceVerifyRemind",
             @"bNeedProfileIntro",
             @"bNeedShakingIntro",
             @"bHomeShowFBLike",
             @"bHomeShowRateApp",
             @"bOtherUserViewShowRatingInstruction",
             @"bMessageTabShowInstruction",
             @"bChatShowReviewTip",
             @"bChatShowSendMediaTip",
             @"bShowRatingAppRemind_MarkAsSold",
             @"bShowRatingAppRemind_FirstReview5star",
             @"bSellingFBShare",
             @"bPromptFBShare",
             @"enterChatCounter",

             @"userLaunchCounter",

             @"bLoggedIn",
             @"bAnonymous",
             @"bUserAvatarVerified",
             @"bUserEmailVerified",
             @"bUserFBVerified",
             @"bUserPhoneVerified",
             @"bUserForceCloseZipView",
             @"accountType",
             @"facebookUserID",
             @"facebookToken",
             @"facebookTokenExpirationDate"
             ];
}

- (NSArray *)localData{
    return @[
             @"zipcodeString",
             @"zipLat",
             @"zipLon",
             @"zipCountry",
             @"zipRegion",
             @"zipCity",
             
             @"bHomeShowUserNearby",
             @"bHomeShowFollowInstruction",
             
             @"userLocalProfile",
             @"userLocalBrand",
             @"userLocalMapSearchPOIs",
             @"preferredCurrency"
             ];
}


#pragma mark - Public
-(BOOL)isLoggedIn
{
    return (self.fmUserID.length > 0
            && self.fmUserToken.length > 0
            && self.bLoggedIn) ? YES : NO;
}


- (void)clearUserInfo{
    self.fmUserID = self.fmUserToken = self.userDisplayName = self.userEmail = self.userAvatarLink = @"";
    self.bLoggedIn = self.bAnonymous = NO;
    self.facebookUserID = self.facebookAccessToken = @"";
    self.facebookTokenExpirationDate = [NSDate distantFuture];
    self.bUserAvatarVerified = self.bUserEmailVerified = self.bUserFBVerified = NO;
}

- (void)updateUserLoggedInfo:(NSDictionary *)info withAnonymousSignup:(BOOL)isAnonymousSignup withAccountType:(HGAccountType)accountType {
    
    self.fmUserID = [info objectForKey:@"id"];
    self.fmUserToken = [info objectForKey:@"token"];
    self.userDisplayName = [info objectForKey:@"nickname"];
    self.userEmail = [info objectForKey:@"email"];
    self.userAvatarLink = [info objectForKey:@"portrait"];
    self.accountType = (NSInteger)accountType;
    
    self.bLoggedIn = !isAnonymousSignup;
    self.bAnonymous = [[info objectForKey:@"is_anonymous"] boolValue];
    self.bUserAvatarVerified = [[info objectForKey:@"verified"] boolValue];
    self.bUserEmailVerified = [[info objectForKey:@"email_verified"] boolValue];
    self.bUserFBVerified = [[info objectForKey:@"facebook_verified"] boolValue];
    self.bUserPhoneVerified = [[info objectForKey:@"mobile_phone_verified"] boolValue];
    
    
    
    self.facebookUserID = [info objectForKey:@"fb_user_id"];
    if([info objectForKey:@"fb_token_expires"]){
        NSNumber *seconds = [info objectForKey:@"fb_token_expires"];
        self.facebookTokenExpirationDate = [NSDate dateWithTimeIntervalSince1970:[seconds doubleValue]];
    }else{
        self.facebookTokenExpirationDate = [NSDate distantFuture];
    }
    
    
    @try {
        self.zipcodeString = [info objectForKey:@"zipcode"];
        self.zipCity = [info objectForKey:@"city"];
        self.zipCountry = [info objectForKey:@"country"];
        self.zipRegion = [info objectForKey:@"region"];
        if (self.zipcodeString != nil && self.zipcodeString.length > 0) {
            self.zipLat = [info objectForKey:@"lat"];
            self.zipLon = [info objectForKey:@"lon"];
        }
    }
    @catch (NSException *exception) {
        [[RavenClient sharedClient] captureMessage:[@"" stringByAppendingFormat:@"%@",exception]   level:kRavenLogLevelDebugError method:__FUNCTION__ file:__FILE__ line:__LINE__];
    }
   
    
}


- (void)updateFacebookToken:(FBSDKAccessToken *)fbAccessToken{
    self.facebookUserID = fbAccessToken.userID;
    self.facebookAccessToken = fbAccessToken.tokenString;
    self.facebookTokenExpirationDate = fbAccessToken.expirationDate;
}

- (void)updateFacebookProfile:(NSDictionary *)response{
    
    self.userDisplayName = [response objectForKey:@"name"];
    self.facebookUserID = [response objectForKey:@"id"];
    self.userEmail = [response objectForKey:@"email"];
    
    if([self.facebookUserID length] > 0){
        self.userAvatarLink = [NSString stringWithFormat:@"http://res.cloudinary.com/fivemiles/image/facebook/%@.jpg", self.facebookUserID];
    }else{
        self.userAvatarLink = @"";
    }
}

#pragma mark - Helper

- (HGUser *)currentUser{
    HGUser *user = [[HGUser alloc] init];
    user.uid = self.fmUserID;
    user.displayName = self.userDisplayName;
    user.portraitLink = self.userAvatarLink;
    user.verified = @(self.bUserAvatarVerified);
    user.facebook_verified = @(self.bUserFBVerified);
    user.email_verified = @(self.bUserEmailVerified);
    user.mobile_phone_verified = @(self.bUserPhoneVerified);
    
    return user;
}

- (void)setDefaultValues{
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *defaultValues = [self defaultValues];
    NSMutableDictionary *defaultValuesWithKey = [@{} mutableCopy];
    
    [defaultValues enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        [defaultValuesWithKey setObject:obj forKey:[self generateKey:key]];
    }];
    
    [standardUserDefaults registerDefaults:defaultValuesWithKey];
}

- (void)reloadLocalData{
    
    [self setDefaultValues];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    for(NSString *propertyName in [self localData]){
        objc_property_t property = class_getProperty([self class], [propertyName UTF8String] );
        if(!property){
            NSLog(@"Can not find the property : %@", propertyName);
            continue;
        }
        NSString *type = [[NSString alloc] initWithUTF8String:property_copyAttributeValue(property, "T")];
        
        type = [type stringByReplacingOccurrencesOfString:@"@" withString:@""];
        type = [type stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        id value = [standardUserDefaults objectForKey:[self generateKey:propertyName]];
        
        NSString *initAction = [propertyName stringByAppendingString:@"Init"];
        if(!value){
            if([self respondsToSelector:NSSelectorFromString(initAction)]){
                [self performSelector:NSSelectorFromString(initAction) withObject:nil afterDelay:0];
            }else{
                id defaultEmptyValue = [self getDefaultEmptyValue:type];
                [self setValue:defaultEmptyValue forKey:propertyName];
            }
        }else{
            [self setValue:value forKey:propertyName];
        }

    }
    
}

- (NSString *)generateKey:(NSString *)propertyName{
    
    BOOL isGlobalData = [[self globalData] containsObject:propertyName];
    BOOL isLocalData = [[self localData] containsObject:propertyName];
    
    if(isLocalData && [self.fmUserID length] > 0){
        return [NSString stringWithFormat:@"%@%@", self.fmUserID, propertyName];
    }else if(isGlobalData){
        return propertyName;
    }else{
        return propertyName;
    }
}


- (id)getDefaultEmptyValue:(NSString *)type{
    if(!type){
        return nil;
    }
    
    if([type isEqualToString:@"NSArray"]){
        return @[];
    }else if([type isEqualToString:@"NSMutableArray"]){
        return [@{} mutableCopy];
    }else if([type isEqualToString:@"NSDictionary"]){
        return @{};
    }else if([type isEqualToString:@"NSMutableDictionary"]){
        return [@{} mutableCopy];
    }else if([type isEqualToString:@"NSString"]){
        return @"";
    }else if([type isEqualToString:@"NSDate"]){
        return [NSDate distantFuture];
    }else if( [type hasPrefix:@"i"]
             || [type hasPrefix:@"s"]
             || [type hasPrefix:@"l"]
             || [type hasPrefix:@"q"]
             || [type hasPrefix:@"I"]
             || [type hasPrefix:@"S"]
             || [type hasPrefix:@"L"]
             || [type hasPrefix:@"Q"]
             || [type hasPrefix:@"f"]
             || [type hasPrefix:@"d"] ){
        return @(0);
    }else if([type hasPrefix:@"B"]){
        return @(NO);
    }else{
        return nil;
    }
}

#pragma mark - Initialize action
- (void)preferredCurrencyInit{
    if( [[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode] ){
        self.preferredCurrency = [[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode];
    }else{
        self.preferredCurrency = [[HGAppInitData sharedInstance].currencies firstObject];
    }
}


#pragma mark - Value changed action
- (void)fmUserIDChangedAction:(NSString *)newValue{
    
    if([self.fmUserID length] > 0){
        [[HGAppData sharedInstance].userApiClient.requestSerializer setValue:newValue forHTTPHeaderField:@"X-FIVEMILES-USER-ID"];
        [[HGAppData sharedInstance].chatApiClient.requestSerializer setValue:newValue forHTTPHeaderField:@"X-FIVEMILES-USER-ID"];
        
        [self reloadLocalData];
    }
}

- (void)fmUserTokenChangedAction:(NSString *)newValue{
    if (self.fmUserToken.length > 0) {
        [[HGAppData sharedInstance].userApiClient.requestSerializer setValue:newValue forHTTPHeaderField:@"X-FIVEMILES-USER-TOKEN"];
        [[HGAppData sharedInstance].chatApiClient.requestSerializer setValue:newValue forHTTPHeaderField:@"X-FIVEMILES-USER-TOKEN"];
    }
}

- (void)userLocalProfileChangedAction:(NSDictionary *)newValue{
    
    self.userDisplayName = [newValue objectForKey:@"nickname"];
    self.userEmail = [newValue objectForKey:@"email"];
    self.userAvatarLink = [newValue objectForKey:@"portrait"];
    
    self.bUserEmailVerified = [[newValue objectForKey:@"email_verified"] boolValue];
    self.bUserFBVerified = [[newValue objectForKey:@"facebook_verified"] boolValue];
    self.bUserPhoneVerified = [[newValue objectForKey:@"mobile_phone_verified"] boolValue];
    self.bUserAvatarVerified = [[newValue objectForKey:@"verified"] boolValue];
    
}



@end

//
//  HGOfferLine.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@class HGUser, HGShopItem;

@protocol HGOfferLine <NSObject>

@end

@interface HGOfferLine : JSONModel

@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) HGUser * buyer;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, assign) BOOL diggable;

@end

@interface HGOfferLineMeta : JSONModel

@property (nonatomic, strong) HGUser * buyer;
@property (nonatomic, strong) HGShopItem * item;
@property (nonatomic, assign) NSTimeInterval latestTimestamp;
@property (nonatomic, assign) BOOL soldToOthers;

@end

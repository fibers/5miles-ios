//
//  HGCategory.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGCategory.h"

@implementation HGCategory

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"index"}];
}
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    
    
    NSArray * names = @[@"icon", @"index",@"title", @"hover_icon", @"subCategories", @"parent_id"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    
    return NO;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.subCategories = [[NSMutableArray alloc] init];
    }
    return self;
}

-(instancetype)commonSetup:(int)index withTitle:(NSString*)titleString withIconName:(NSString*)iconNameString
{
    self.index = index;
    self.title = titleString;
    self.icon = iconNameString;
    return self;
}
@end

//
//  NSURL+Pinnacle.m
//  Pinnacle
//
//  Created by shengyuhong on 15/6/11.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "NSURL+Pinnacle.h"

@implementation NSURL (Pinnacle)

- (NSDictionary *)queryAsDictionary{
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSString *queryString = self.query;

    if([queryString length] > 0){
        
        NSArray *queryArray = [queryString componentsSeparatedByString:@"&"];
        for(NSString *queryPair in queryArray){
            NSArray *pair = [queryPair componentsSeparatedByString:@"="];
            if([pair count] == 2){
                [dictionary setObject:[pair lastObject] forKey:[pair firstObject]];
            }
        }
    }
    
    return dictionary;
}


@end

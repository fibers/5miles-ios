//
//  ItemDetailLikersCell.m
//  Pinnacle
//
//  Created by Alex on 14-11-17.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "ItemDetailLikersCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGUtils.h"
#import "ItemLikers.h"
@interface ItemDetailLikersCell ()

@property (nonatomic, strong) UIImageView* imageView;
@property (nonatomic, strong) NSMutableArray* userArray;


@end
@implementation ItemDetailLikersCell



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        self.userArray = [[NSMutableArray alloc] init];
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

static int likeCountTag = 100001;
static int likeCountIconTag= 100002;
-(void)setup
{

    
    self.likersIcon = [[UIImageView alloc] initWithFrame:CGRectMake(13, 15, 26/2, 22/2)];
    self.likersIcon.image = [UIImage imageNamed:@"item_detail_likers"];
    [self.contentView addSubview: self.likersIcon];
    
    
    self.likersCount = [[UILabel alloc] initWithFrame:CGRectMake(4, 15 + 13 , 30 , 16)];
    self.likersCount.textAlignment = NSTextAlignmentCenter;
    
    self.likersCount.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:self.likersCount];
    
    self.likersCount.tag = likeCountTag;
    self.likersIcon.tag = likeCountIconTag;

    
}
- (void)layoutSubviews
{
    
    
       //self.contentView.backgroundColor = [UIColor blueColor];
}

-(void)pushUserProfile:(id)sender
{
    [self.actionDelegate onTouchAvatarForUserView:sender];
}
-(void)pushMoreLikers:(id)sender
{
    [self.actionDelegate onTouchLikerMore:sender];
}
-(void)startLikerDetailViewController
{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
}

-(void)configCell:(NSArray*)likersArray withTotalLikerCount:(int)totalCount;
{
    if (likersArray ==nil || likersArray.count == 0) {
        return;
    }
    
    for (UIView *subviews in [self.contentView subviews]) {
        if (subviews.tag == likeCountIconTag || subviews.tag == likeCountTag) {
            continue;
        }else{
            [subviews removeFromSuperview];
        }
    }
    for(UIView*subviews in [self subviews])
    {
        if (subviews.tag == 5678) {
            [subviews removeFromSuperview];

        }
    }
    
      
     self.likersCount.text = [@"" stringByAppendingFormat:@"%d", totalCount];
    
    int i = 0;
    for (i = 0; i<likersArray.count; i++) {
        int xIndex = i % 7;
        int YIndex = i / 7;
        UIImageView* currentView = [[UIImageView alloc] initWithFrame:CGRectMake(60-24 + xIndex * (32 + 6) , 11 + YIndex * (32 + 7), 32, 32)];
        
        currentView.layer.borderColor = SYSTEM_MY_ORANGE.CGColor;
        currentView.layer.borderWidth = 1;
        [self.contentView addSubview:currentView];
        
        UIImageView* userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, currentView.frame.size.width/3, currentView.frame.size.width/3)];
        
        
        ItemLikers * liker = [likersArray objectAtIndex:i];
        if (liker.verified.intValue == 1) {
            userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
        }
        else{
            userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
        }
        CGPoint centerPoint = currentView.center;
        centerPoint.x = centerPoint.x + currentView.frame.size.width/2 -userAvatar_verifyMark.frame.size.width/2;
        centerPoint.y = centerPoint.y + currentView.frame.size.width/2 -userAvatar_verifyMark.frame.size.width/2;
        userAvatar_verifyMark.center = centerPoint;
        userAvatar_verifyMark.tag = 5678;
        [self addSubview:userAvatar_verifyMark];

        
        
        
        
        
        int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:currentView.frame.size.width*2];
        
         NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:liker.portrait width:suggestWidth height:suggestWidth];
        
        [currentView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
        
        currentView.layer.cornerRadius = currentView.frame.size.width/2;
        currentView.clipsToBounds = YES;
        currentView.contentMode = UIViewContentModeScaleAspectFill;
        //currentView.layer.borderColor = FANCY_COLOR(@"BC8E70").CGColor;
        //currentView.layer.borderWidth = 1;
        currentView.userInteractionEnabled = YES;
        currentView.tag = i;
        UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushUserProfile:)];
        
        [currentView addGestureRecognizer:singleTap1];
    }

    //add more button
    //default 13
    if(likersArray.count >= 13)
    {
        int xIndex = 6;
        int YIndex = 1;
        UIImageView* currentView = [[UIImageView alloc] initWithFrame:CGRectMake(60-24 + xIndex * (32 + 6) , 11 + YIndex * (32 + 7), 32, 32)];
        [self.contentView addSubview:currentView];
        currentView.clipsToBounds = YES;
        currentView.userInteractionEnabled = YES;
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        [currentView addSubview:label];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"...";
        
        UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushMoreLikers:)];
        
        [currentView addGestureRecognizer:singleTap1];
    }
}

@end

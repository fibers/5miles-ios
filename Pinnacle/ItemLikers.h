//
//  ItemLikers.h
//  Pinnacle
//
//  Created by Alex on 14-11-23.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface ItemLikers : JSONModel

@property (nonatomic, strong) NSString* uid;
@property (nonatomic, strong) NSString* nickname;
@property (nonatomic, strong) NSString* portrait;
@property (nonatomic, strong) NSNumber* verified;
@property (nonatomic, assign) BOOL following;
@end

//
//  HGItemMapController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-19.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGItemMapController.h"
#import "HGShopItem.h"
#import "HGItemDetail.h"
#import "HGUser.h"
#import "HGUtils.h"
#import "HGAppData.h"
#import <MapKit/MapKit.h>
@interface HGFrameSize : NSObject
@property (nonatomic, assign) float width;
@property (nonatomic, assign) float height;

@end
@implementation HGFrameSize
-(void)test
{
    
}

@end

@interface HGItemMapController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, assign) int zoomLevel;
@property (nonatomic, retain) NSMutableArray * annotationFrameSize;

@end




@implementation HGItemMapController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //default map  zoom level 15 [1-19];
        self.mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
       
    }
    return self;
}
- (int)getZoomLevel:(MKMapView*)inputMapview {
    
    return 21-round(log2(inputMapview.region.span.longitudeDelta * MERCATOR_RADIUS * M_PI / (180.0 * inputMapview.bounds.size.width)));
    
}

- (void)viewDidLayoutSubviews{
    // Adjust the element in profile summary area.
    
    [super viewDidLayoutSubviews];
     self.mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //error defense:
    self.navigationController.navigationBar.hidden = NO;
}
-(void)showMyLocation
{
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"map_view" action:@"showmylocation" label:nil value:nil];
    
    //self.mapView.showsUserLocation = YES;

    // 1
    CLLocationCoordinate2D zoomCenter;
    zoomCenter.latitude = self.itemDetail.geoLocation.lat;
    zoomCenter.longitude= self.itemDetail.geoLocation.lon;
    
    double templat = [[HGUtils sharedInstance] getPreferLat];
    double templon = [[HGUtils sharedInstance] getPreferLon];
    
    CLLocationDistance distanceMeters= 0;
    if (fabs(templat - 0) < 0.000001 && fabs(templon - 0) < 0.000001) {
        zoomCenter.latitude = (self.itemDetail.geoLocation.lat + templat)/2;
        zoomCenter.longitude= (self.itemDetail.geoLocation.lon + templon)/2;
        
        CLLocation *orig=[[CLLocation alloc] initWithLatitude:self.itemDetail.geoLocation.lat longitude:self.itemDetail.geoLocation.lon];
        CLLocation* dist = [[CLLocation alloc] initWithLatitude:templat longitude:templon];
                          
        distanceMeters =[orig distanceFromLocation:dist];
        if (distanceMeters < METERS_PER_MILE ) {
            distanceMeters = METERS_PER_MILE;
        }
    }
    else{
        zoomCenter.latitude = (templat + self.itemDetail.geoLocation.lat)/2;
        zoomCenter.longitude = (templon + self.itemDetail.geoLocation.lon)/2;
        CLLocation *orig=[[CLLocation alloc] initWithLatitude:self.itemDetail.geoLocation.lat longitude:self.itemDetail.geoLocation.lon];
        CLLocation* dist = [[CLLocation alloc] initWithLatitude:templat longitude:templon];
        
        distanceMeters =[orig distanceFromLocation:dist];
        if (distanceMeters < METERS_PER_MILE ) {
            distanceMeters = METERS_PER_MILE;
        }
    }
    
    MKCoordinateRegion viewRegion;
    if (distanceMeters == 0) {
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, 1.0*METERS_PER_MILE, 1.0*METERS_PER_MILE);
    }
    else{
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, distanceMeters*2, distanceMeters*2);
    }
    
    if (viewRegion.span.latitudeDelta > 180 || viewRegion.span.longitudeDelta > 180) {
        if (viewRegion.span.latitudeDelta > 180) {
            viewRegion.span.latitudeDelta = 180;
        }
        if (viewRegion.span.longitudeDelta > 180) {
            viewRegion.span.longitudeDelta = 180;
        }
    }
    if(viewRegion.span.latitudeDelta < 0 || viewRegion.span.longitudeDelta < 0)
    {
        viewRegion.span.longitudeDelta = 180;
        viewRegion.span.latitudeDelta = 180;
    }
    if (viewRegion.span.latitudeDelta > 120 || viewRegion.span.longitudeDelta > 120) {
        viewRegion.center.latitude = templat;
        viewRegion.center.longitude = templon;
    }
    // 3
    [self.mapView setRegion:viewRegion animated:YES];
    
}
-(void)addMylocationButton
{

    self.showMylocationButton = [[UIButton alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height - 40 -10 -60, self.view.frame.size.width - 20, 40)];
    self.showMylocationButton.layer.cornerRadius = 3;
    self.showMylocationButton.backgroundColor = FANCY_COLOR(@"ff8830");
    self.showMylocationButton.titleLabel.textColor = [UIColor whiteColor];
    [self.showMylocationButton setTitle: [@"    " stringByAppendingString:NSLocalizedString(@"Show my location",nil)]   forState:UIControlStateNormal];
    //[self.showMylocationButton setTitle: @"Show my location" forState:UIControlStateHighlighted];
    
    self.showMylocationButton.layer.shadowColor = FANCY_COLOR(@"000000").CGColor;
    self.showMylocationButton.layer.shadowOpacity = 0.5;
    self.showMylocationButton.layer.shadowOffset = CGSizeMake(4,4);
    self.showMylocationButton.layer.shadowRadius = 6;
    
    self.showMylocationButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    
    UIImageView* naviIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 34/2, 36/2)];
    
    CGPoint center = CGPointMake(self.showMylocationButton.center.x - 90, 13+9);
    naviIcon.center = center;
    naviIcon.image = [UIImage imageNamed:@"map-navi-icon"];
   
    [self.showMylocationButton addSubview:naviIcon];
    
    [self.view addSubview: self.showMylocationButton];
    [self.showMylocationButton addTarget:self action:@selector(showMyLocation) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
     [[HGUtils sharedInstance] gaTrackViewName:@"map_view"];
    self.mapView.frame = self.view.frame;
    [self addMylocationButton];
    
    self.annotationFrameSize = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"Map", nil);
    self.navigationController.navigationBar.hidden = NO;
    // 1
    CLLocationCoordinate2D zoomCenter;
    zoomCenter.latitude = self.itemDetail.geoLocation.lat;
    zoomCenter.longitude= self.itemDetail.geoLocation.lon;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomCenter, 1.0*METERS_PER_MILE, 1.0*METERS_PER_MILE);
    
    // 3
    [self.mapView setRegion:viewRegion animated:YES];
    self.mapView.showsUserLocation = NO;
 
    self.zoomLevel = [self getZoomLevel:self.mapView];
    //MKCircle
    //4 add annotation or mkcircle
    //[self addAnnotation];
    [self addMKcircle];
    [self addUserAnnotation];
}


-(void)addAnnotation
{
    // Pin the item on the map
    //    HGItemLocation * itemLocation = [[HGItemLocation alloc] initWithItem:self.item];
    MKPointAnnotation * itemLocation = [[MKPointAnnotation alloc] init];
    itemLocation.title = self.item ? self.item.title : self.itemDetail.title;
    itemLocation.subtitle = self.item ? self.item.seller.displayName : self.itemDetail.seller.displayName;
    itemLocation.coordinate = CLLocationCoordinate2DMake(self.item ? self.item.geoLocation.lat : self.itemDetail.geoLocation.lat, self.item ? self.item.geoLocation.lon : self.itemDetail.geoLocation.lon);
    
    
    if (fabs(itemLocation.coordinate.latitude - 0.0) < 0.000001
        && fabs(itemLocation.coordinate.longitude-0.0) < 0.000001)  {
        //item location fail
        return;
    }
    else
    {
        itemLocation.coordinate = [self makePositionRandomOffset:itemLocation.coordinate];
        itemLocation.title = @"itemLocation";
    
        [self.mapView addAnnotation:itemLocation];
    }
}
#define USER_LOCATION_TITLE @"userLocation"
-(void)addUserAnnotation
{
     MKPointAnnotation * userLocation = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D userCood =(CLLocationCoordinate2D){[[HGUtils sharedInstance] getPreferLat],[[HGUtils sharedInstance] getPreferLon]} ;
    userLocation.coordinate = userCood;
    userLocation.title = USER_LOCATION_TITLE;
    [self.mapView addAnnotation:userLocation];
}

-(CLLocationCoordinate2D)makePositionRandomOffset:(CLLocationCoordinate2D)coordinate
{
    float x_rand = 0.0001* (rand()%50);
    float y_rand = 0.0001* (rand()%50);
    
    coordinate.latitude = coordinate.latitude + x_rand;
    coordinate.longitude = coordinate.longitude + y_rand;
    return coordinate;
    
}
#define MKCIRCLERADIUS  400
-(void)addMKcircle
{
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.item ? self.item.geoLocation.lat : self.itemDetail.geoLocation.lat, self.item ? self.item.geoLocation.lon : self.itemDetail.geoLocation.lon);
    
    if ( fabs(coordinate.latitude -0)<0.000001
        && fabs(coordinate.longitude-0)<0.000001) {
        return;
    }
    
    coordinate = [self makePositionRandomOffset:coordinate];
    self.circle=[MKCircle circleWithCenterCoordinate:coordinate radius:MKCIRCLERADIUS];
   
    [self.mapView addOverlay:self.circle];
}
-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    MKOverlayView *view=nil;
    if ([overlay isKindOfClass:[MKCircle class]]) {
        MKCircleView *cirView =[[MKCircleView alloc] initWithCircle:(MKCircle*)overlay];
        cirView.fillColor=[UIColor colorWithRed:0x5f/255.0 green:0x78/255.0 blue:0xf7/255.0 alpha:0.5];
        cirView.strokeColor= [UIColor colorWithRed:0x5f/255.0 green:0x78/255.0 blue:0xf7/255.0 alpha:0.5];
        cirView.alpha=1;
        cirView.lineWidth=2.0;
        view=cirView;        
    }
    return view;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"ItemMap"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - MapView Delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"Annotation";
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        
        MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView == nil)
        {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            MKPointAnnotation* mk = (MKPointAnnotation*)annotation;
            if ([mk.title isEqualToString:USER_LOCATION_TITLE]) {
                annotationView.image = [UIImage imageNamed:@"map_user_location_big"];
            }
            
        }
        else
        {
            annotationView.annotation = annotation;
            //[self changeAnnotationFrame:annotationView];
        }
        
        return annotationView;
    }
    else if([annotation isKindOfClass:[MKUserLocation class]]){
         MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:@"userlocation"];
        if(annotation==mapView.userLocation)
        {
            if (annotationView == nil) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"userlocation"];
                //annotationView.canShowCallout = YES;
            }
            annotationView.image = [UIImage imageNamed:@"map_user_location_big"];
            return annotationView;
        }
    }
    
    return nil;
}
- (void)mapView:(MKMapView *)inputMapView regionDidChangeAnimated:(BOOL)animated {
    
    int newZoomLevel = [self getZoomLevel:inputMapView];
    
    if (newZoomLevel > self.zoomLevel) {
        ///zoom range become smaller.
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"map_view" action:@"productmapzoomin" label:nil value:nil];
    }
    else{
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"map_view" action:@"productmapzoomout" label:nil value:nil];
    }
    self.zoomLevel = newZoomLevel;
    
    
    NSLog(@"zoom level %d", [self getZoomLevel:inputMapView]);
    for (id<MKAnnotation> annotation in inputMapView.annotations)
    {
        [inputMapView removeAnnotation:annotation];
        // change coordinates etc
        [inputMapView addAnnotation:annotation];
    }
    
    if(self.zoomLevel <= 15)
    {
        //if 
        CLLocationCoordinate2D temp = self.circle.coordinate;
        [self.mapView removeOverlay:self.circle];
        int zoomLeveldelta = DEFAULT_ZOOM_LEVEL - self.zoomLevel ;
        double  numberExtendTime = pow(1.9, zoomLeveldelta);
        self.circle=[MKCircle circleWithCenterCoordinate:temp radius:MKCIRCLERADIUS*numberExtendTime];
        
        [self.mapView addOverlay:self.circle];
    }
    
}

-(void)changeAnnotationFrame:(MKAnnotationView*)annotationView
{
    CGRect containerFrame = annotationView.frame;
    HGFrameSize* expectedsize = (HGFrameSize*)self.annotationFrameSize[self.zoomLevel];
    
    if (expectedsize.width <= 40 || expectedsize.height <= 40) {
        // the smallest size
         annotationView.frame = containerFrame;
    }
    else{
        containerFrame.size.width = expectedsize.width;
        containerFrame.size.height = expectedsize.height;
        annotationView.frame = containerFrame;
    }
}

-(void)initAnnotationZoomFrame:(MKAnnotationView*)annotationView
{
    if (self.annotationFrameSize.count == 0) {
        for(int i = 0;i <= 19;i++)
        {
            HGFrameSize* framesize = [[HGFrameSize alloc] init];
            if (i<15) {
                framesize.width = annotationView.frame.size.width;
                framesize.height = annotationView.frame.size.height;
                [self.annotationFrameSize addObject:framesize];
            }
            else{
                int powNumber = i - DEFAULT_ZOOM_LEVEL;
                powNumber++;
                int ExtensionSize = pow(2, powNumber);
                framesize.width = annotationView.frame.size.width * ExtensionSize;
                framesize.height = annotationView.frame.size.height*ExtensionSize;
                [self.annotationFrameSize addObject:framesize];
            }
        }
    }
    NSLog(@"the annotation size is %ld", (unsigned long)self.annotationFrameSize.count);
}


@end

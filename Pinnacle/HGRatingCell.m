//
//  HGRatingCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGRatingCell.h"
#import "HGRating.h"
#import "HGUser.h"
#import "HGUtils.h"
#import "HGUserData.h"
#import<CoreText/CoreText.h>
#import <MHPrettyDate.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "HGConstant.h"
#define PADDING_LEFT 54
#define PADDING_RIGHT 10

#define RATING_IMAGE_HEIGHT 10

@interface HGRatingCell ()

@property (nonatomic, strong) UIImageView * avatarView;
@property (nonatomic, strong) HGRatingCellDrawingView * drawingView;
@property (nonatomic, strong) UIImageView* scoreStarImage;
@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;

@property (nonatomic, strong) UIImageView* reportIcon;
@property (nonatomic, strong) UILabel* reportLabel;


@property (nonatomic, strong) UIView* dividerline;
@property (nonatomic, strong) UIImageView* replyIcon;
@property (nonatomic, strong) UILabel* replyLabel;



@end

@implementation HGRatingCell

#define MAX_COMMENT_DISPLAY_LINE 5
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self _commonSetup];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    [self _commonSetup];
}

- (void)_commonSetup
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
   
    self.drawingView = [[HGRatingCellDrawingView alloc] initWithFrame:self.bounds ownerCell:self];
    [self.contentView addSubview:self.drawingView];
    
    self.avatarView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 8, 32, 32)];
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.borderColor = SYSTEM_MY_ORANGE.CGColor ;//[UIColor whiteColor].CGColor;
    self.avatarView.layer.borderWidth = 1;
    self.avatarView.layer.cornerRadius = CGRectGetWidth(self.avatarView.bounds) * 0.5;
    [self.contentView addSubview:self.avatarView];
    
    self.ratingUserName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.avatarView.frame)+10, CGRectGetMinY(self.avatarView.frame), 100, 20)];
    self.ratingUserName.textAlignment = NSTextAlignmentLeft;
    self.ratingUserName.textColor= FANCY_COLOR(@"c95500");
    self.ratingUserName.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:self.ratingUserName];
    
    
    
    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.avatarView.frame.size.width/3, self.avatarView.frame.size.width/3)];
    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    CGPoint centerPoint = self.avatarView.center;
    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -self.userAvatar_verifyMark.frame.size.width/2;
    self.userAvatar_verifyMark.center = centerPoint;
    [self.contentView addSubview:self.userAvatar_verifyMark];
    
    
    
    self.scoreStarImage = [[UIImageView alloc] initWithFrame:CGRectMake(PADDING_LEFT, 6 + CGRectGetMaxY(self.ratingUserName.frame), 144/2, RATING_IMAGE_HEIGHT)];
    [self.contentView addSubview:self.scoreStarImage];
   
    
    
    
    self.SeperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width, 0.5)];
    [self.contentView addSubview: self.SeperatorView];
    self.SeperatorView.backgroundColor = [UIColor colorWithRed:0xcc/255.0 green:0xcc/255.0 blue:0xcc/255.0 alpha:1.0];
    [self addReport];
    [self addReply];
}
-(void)addReport
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapReportIcon:)];
    
    self.reportIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    [self.contentView addSubview:self.reportIcon];
    self.reportIcon.image = [UIImage imageNamed:@"rating_Report"];
    self.reportIcon.userInteractionEnabled = YES;
    [self.reportIcon addGestureRecognizer:tapGesture];
    
    self.reportLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 20)];
    [self.contentView addSubview:self.reportLabel];
    self.reportLabel.font = [UIFont systemFontOfSize:13];
    self.reportLabel.textColor = FANCY_COLOR(@"818181");
    self.reportLabel.text = NSLocalizedString(@"Report",nil);
    
    self.reportLabel.userInteractionEnabled = YES;
    [self.reportLabel addGestureRecognizer:tapGesture];
    
}

-(void)addReply
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapReplyIcon:)];
    
    self.dividerline = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 15)];
    self.dividerline.backgroundColor = FANCY_COLOR(@"818181");
    [self.contentView addSubview:self.dividerline];
    
    self.replyIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
    [self.contentView addSubview:self.replyIcon];
    self.replyIcon.image = [UIImage imageNamed:@"rating_Reply"];
    self.replyIcon.userInteractionEnabled = YES;
    [self.replyIcon addGestureRecognizer:tapGesture];
    
    self.replyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 20)];
    [self.contentView addSubview:self.replyLabel];
    self.replyLabel.font = [UIFont systemFontOfSize:13];
    self.replyLabel.textColor = FANCY_COLOR(@"818181");
    self.replyLabel.text = NSLocalizedString(@"Reply",nil);
    
    self.replyLabel.userInteractionEnabled = YES;
    [self.replyLabel addGestureRecognizer:tapGesture];
}

- (void)onTapReportIcon:(UITapGestureRecognizer *)tapGesture{
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onTapReportIcon:)]){
        [self.delegate onTapReportIcon:self.rating];
    }
}

-(void)onTapReplyIcon:(UITapGestureRecognizer *)tapGesture{
    
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(onTapReplyIcon:)]){
        [self.delegate onTapReplyIcon:self.rating];
    }
}

- (void)layoutSubviews
{
    self.contentView.frame = self.bounds;
    self.SeperatorView.frame = CGRectMake(0, self.frame.size.height-0.5, self.frame.size.width-8, 0.5);
    
    
    float replyHeight = 0;
    if (self.rating && self.rating.replyHeight > 0) {
        replyHeight = self.rating.replyHeight + 15;
    }
    
    float xOffset = 0;
    if (self.rating && self.rating.reported) {
        xOffset = 10;
    }
    self.reportIcon.frame = CGRectMake(self.frame.size.width-68 - 75 - xOffset, self.frame.size.height - 20 - replyHeight, 14, 14);
    self.reportLabel.frame = CGRectMake(self.frame.size.width-52 -75 -xOffset, self.frame.size.height - 25 - replyHeight, 60, 20);
    
    
    self.replyIcon.frame = CGRectMake(self.frame.size.width-68 , self.frame.size.height - 20 - replyHeight, 14, 14);
    self.replyLabel.frame = CGRectMake(self.frame.size.width-52 , self.frame.size.height - 25 - replyHeight, 60, 20);
    
    self.dividerline.frame = CGRectMake(CGRectGetMinX(self.replyIcon.frame)-8, self.frame.size.height - 22 - replyHeight, self.dividerline.frame.size.width, self.dividerline.frame.size.height);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithEntity:(HGRating *)rating isMyRatingView:(BOOL)bIsMyRatingView
{
    self.rating = rating;
    
    self.ratingUserName.text = self.rating.reviewer.nickname;
    
    int suggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatarView.frame.size.width*2];
    NSString * image_url = [[HGUtils sharedInstance] cloudinaryLink:rating.reviewer.portrait width:suggestWidth height:suggestWidth];
    
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"default-avatar"]];
    
    NSString* image_index = [[HGUtils sharedInstance] getReviewImageNameByScore:rating.score];
    
    self.scoreStarImage.image = [UIImage imageNamed:image_index];
    
    if (rating.reviewer.verified) {
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    
    
    if (bIsMyRatingView) {
        self.reportLabel.hidden = self.reportIcon.hidden = NO;
        self.replyLabel.hidden = self.replyIcon.hidden =  NO;
        self.dividerline.hidden = NO;
    }
    else{
        self.reportLabel.hidden = self.reportIcon.hidden = YES;
        self.replyLabel.hidden = self.replyIcon.hidden = YES;
        self.dividerline.hidden = YES;
    }
    
    if (self.rating.reported) {
        self.reportLabel.text = NSLocalizedString(@"Reported", nil);
    }
    else{
        self.reportLabel.text = NSLocalizedString(@"Report", nil);
    }
    
    [self.drawingView setNeedsDisplay];
}

#pragma mark - Class Level Methods

+ (UIFont *)titleFont
{
    UIFont * font = [UIFont boldSystemFontOfSize:14.0];
    return font;
}

+ (UIFont *)timeFont
{
    UIFont * font = [UIFont systemFontOfSize:13.0];
    return font;
}

+ (UIFont *)commentFont
{
    UIFont * font = [UIFont systemFontOfSize:13.0];
    return font;
}

+ (UIFont *)replyFont
{
    UIFont * font = [UIFont systemFontOfSize:13.0];
    return font;
}

+ (CGFloat)cellHeightForRating:(HGRating *)rating boundingWidth:(CGFloat)width
{
    CGFloat height = RATING_IMAGE_HEIGHT + 10 + 20;
   
    
    CGRect rect = [rating.comment boundingRectWithSize:CGSizeMake(width - PADDING_LEFT - PADDING_RIGHT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [HGRatingCell commentFont]} context:nil];
    height += rect.size.height + 10;
    rating.commentHeight = rect.size.height;
    height += [HGRatingCell timeFont].lineHeight + 20;
    
    if (rating.reply_content && rating.reply_content.length > 0) {
        NSString* displayContent = [[HGUserData sharedInstance].userDisplayName stringByAppendingFormat:@" Reply: %@",rating.reply_content];
        
        CGRect replyRect = [displayContent boundingRectWithSize:CGSizeMake(width - PADDING_LEFT - PADDING_RIGHT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [HGRatingCell replyFont]} context:nil];
        rating.replyHeight = replyRect.size.height;
        height += replyRect.size.height + 10;
    }
    return height;
}

@end

@implementation HGRatingCellDrawingView

- (id)initWithFrame:(CGRect)frame ownerCell:(HGRatingCell *)cell
{
    self = [super initWithFrame:frame];
    if (self) {
        self.ownerCell = cell;
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    CGPoint ptCommentLeftUp = CGPointMake(PADDING_LEFT, RATING_IMAGE_HEIGHT + 20 + 20 + 5);
    
    CGRect commentRect = CGRectMake(ptCommentLeftUp.x,
                                    ptCommentLeftUp.y,
                                    CGRectGetWidth(rect) - PADDING_LEFT - PADDING_RIGHT,
                                    ceil(self.ownerCell.rating.commentHeight));
    [self.ownerCell.rating.comment drawWithRect:commentRect options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [HGRatingCell commentFont]} context:nil];
    
    NSDate * ts = [self.ownerCell.rating timestamp];
    NSString * timeString = [MHPrettyDate prettyDateFromDate:ts withFormat:MHPrettyDateFormatTodayTimeOnly];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MM/dd/yyyy"];
    if ([timeString rangeOfString:@"/"].location != NSNotFound) {
        timeString=[fmt stringFromDate:ts];
    }
    
    
    float replyHeight = 0;
    if (self.ownerCell.rating.reply_content && self.ownerCell.rating.reply_content.length > 0) {
        
        
        
        replyHeight=  self.ownerCell.rating.replyHeight + 10;
        CGRect ReplyRect = CGRectMake(ptCommentLeftUp.x,
                                        CGRectGetMaxY(commentRect)+25,
                                        CGRectGetWidth(rect) - PADDING_LEFT - PADDING_RIGHT,
                                        ceil(self.ownerCell.rating.replyHeight));
        
        
        
        //创建路径并获取句柄
        CGMutablePathRef path = CGPathCreateMutable();
        CGRect replyBackGround = CGRectMake(ReplyRect.origin.x - 8, ReplyRect.origin.y-5, ReplyRect.size.width+6, ReplyRect.size.height+10);
        CGPathAddRect(path,NULL,replyBackGround);
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        CGContextAddPath(currentContext, path);
        [FANCY_COLOR(@"EFF0F1") setFill];
        [[UIColor clearColor] setStroke];
        CGContextDrawPath(currentContext, kCGPathFillStroke);
        CGPathRelease(path);
        
        
        /////绘制小三角
        CGContextSetLineWidth(currentContext, 1.0);
        CGPoint sPoints[3];//坐标点
        sPoints[0] =CGPointMake(replyBackGround.origin.x+8, replyBackGround.origin.y);//坐标1
        sPoints[1] =CGPointMake(replyBackGround.origin.x+12, replyBackGround.origin.y-6);//坐标2
        sPoints[2] =CGPointMake(replyBackGround.origin.x+16, replyBackGround.origin.y);//坐标3
        CGContextAddLines(currentContext, sPoints, 3);//添加线
        CGContextClosePath(currentContext);//封起来
        CGContextDrawPath(currentContext, kCGPathFillStroke); //根据坐标绘制路径
        
         NSString* displayContent = [self.ownerCell.nickname stringByAppendingFormat:@" Reply: %@",self.ownerCell.rating.reply_content];
       
        [displayContent drawWithRect:ReplyRect options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [HGRatingCell replyFont]} context:nil];
        
        
        
        
        
       

        
    }
    CGPoint ptTimeString = CGPointMake(SCREEN_WIDTH-75 ,20);
    [timeString drawAtPoint:ptTimeString withAttributes:@{NSFontAttributeName: [HGRatingCell timeFont], NSForegroundColorAttributeName:FANCY_COLOR(@"818181")}];
    

}


@end

//
//  HGChatLocationMediaItem.m
//  Pinnacle
//
//  Created by shengyuhong on 15/5/25.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatLocationMediaItem.h"
#import "JSQMessagesMediaViewBubbleImageMasker.h"
#import "JSQMessagesBubbleImageFactory.h"
#import "HGConstant.h"

static const NSUInteger SizeActivityIndicator = 20;
static const NSUInteger HeightProgress = 20;
static const NSUInteger AddressLabelHeight = 40;
static const NSUInteger WidthTail = 2;

@interface HGChatLocationMediaItem ()

@property (nonatomic, strong) UIView *loadingMask;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorLoading;
@property (nonatomic, strong) CATextLayer *layerProgress;
@property (nonatomic, strong) JSQMessagesMediaViewBubbleImageMasker *masker;

@end


@implementation HGChatLocationMediaItem


- (instancetype)init
{
    self = [super init];
    if(self){
        [self setup];
    }
    
    return self;
}
-(void)setLeftSpeakerInsets
{
     self.titleLabel.textInsets =  UIEdgeInsetsMake(5, 16, 0, 6);
    self.addressLabel.textInsets =  UIEdgeInsetsMake(-2, 16, 0, 6);
   
}
-(void)setRightSpeakerInsets
{
    self.titleLabel.textInsets =  UIEdgeInsetsMake(5, 8, 0, 6);
    self.addressLabel.textInsets =  UIEdgeInsetsMake(-2, 8, 0, 6);
}
- (void)setup{
    
    JSQMessagesBubbleImageFactory *factory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage imageNamed:@"bubble_self_defined"] capInsets:UIEdgeInsetsZero];
    
    self.masker = [[JSQMessagesMediaViewBubbleImageMasker alloc] initWithBubbleImageFactory:factory];
    
    self.poiAddress = @"";
    self.poiTitle = @"";
    self.latString= @"";
    self.lonString = @"";

    self.imageView = [[UIImageView alloc] init];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = YES;
    
    self.addressLabel = [[TTTAttributedLabel alloc] init];
    self.addressLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    self.addressLabel.font = [UIFont systemFontOfSize:12];
    self.addressLabel.textColor = [UIColor whiteColor];
    self.addressLabel.numberOfLines = 1;
    self.addressLabel.textAlignment = NSTextAlignmentLeft;
    self.addressLabel.textInsets = UIEdgeInsetsMake(0, 6, 0, 6);
    [self.imageView addSubview:self.addressLabel];
    
    self.titleLabel = [[TTTAttributedLabel alloc] init];
    self.titleLabel.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    self.titleLabel.font = [UIFont systemFontOfSize:12];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.textInsets = UIEdgeInsetsMake(5, 6, 0, 6);
    [self.imageView addSubview:self.titleLabel];
    
    

    self.loadingMask = [[UIView alloc] init];
    self.loadingMask.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    self.activityIndicatorLoading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicatorLoading.userInteractionEnabled = NO;
    [self.activityIndicatorLoading startAnimating];
    [self.loadingMask addSubview:self.activityIndicatorLoading];
    
    self.layerProgress = [CATextLayer layer];
    self.layerProgress.string = @"0%";
    self.layerProgress.alignmentMode = kCAAlignmentCenter;
    self.layerProgress.backgroundColor = [UIColor clearColor].CGColor;
    self.layerProgress.wrapped = YES;
    self.layerProgress.contentsScale = [UIScreen mainScreen].scale;
    
    UIFont *font = [UIFont systemFontOfSize:14];
    CFStringRef fontName = (__bridge CFStringRef)font.fontName;
    CGFontRef fontRef = CGFontCreateWithFontName(fontName);
    self.layerProgress.font = fontRef;
    self.layerProgress.fontSize = font.pointSize;
    CGFontRelease(fontRef);
    
    [self.loadingMask.layer addSublayer:self.layerProgress];
    
}

- (CGSize)mediaViewDisplaySize{
    if(IPHONE6PLUS)
    {
        return CGSizeMake((210 + WidthTail)*1.2,120*1.2);
    }
    
    return CGSizeMake(210 + WidthTail,120);
}


- (UIView *)mediaView{
    
    CGSize size = [self mediaViewDisplaySize];
    self.imageView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
    
     self.titleLabel.frame = CGRectMake(0, size.height-AddressLabelHeight, size.width-WidthTail, AddressLabelHeight/2);
    self.addressLabel.frame = CGRectMake(0, size.height - AddressLabelHeight/2  , size.width - WidthTail, AddressLabelHeight/2);
   
    
    [self updateLoadingMaskSize:size];
    
    if(self.appliesMediaViewMaskAsOutgoing){
        [self.masker applyOutgoingBubbleImageMaskToMediaView:self.imageView];
    }else{
        [self.masker applyIncomingBubbleImageMaskToMediaView:self.imageView];
    }
    
    return self.imageView;
}


- (void)addLoadingMask{
    if(![self.loadingMask superview]){
        [self.imageView addSubview:self.loadingMask];
    }
    CGSize size = [self mediaViewDisplaySize];
    [self updateLoadingMaskSize:size];
    [self.activityIndicatorLoading startAnimating];
    [self updateProgress:0];
}

- (void)removeLoadingMask{
    [self.loadingMask removeFromSuperview];
    [self.activityIndicatorLoading stopAnimating];
    [self updateProgress:0];
}

- (void)updateProgress:(CGFloat)progress{
    self.layerProgress.string = [[HGUtils sharedInstance] decimalToPercentage:fabs(progress)];
}

- (void)addFailedIcon{
    
    CGRect rectFailedIcon = self.imageView.bounds;
    rectFailedIcon.size.width = rectFailedIcon.size.width - WidthTail;
    UIImageView *ivFailedIcon = [[UIImageView alloc] initWithFrame:rectFailedIcon];
    ivFailedIcon.contentMode = UIViewContentModeScaleAspectFill;
    ivFailedIcon.image = [UIImage imageNamed:@"icon_image_receive_failed"];
    
    [self.imageView addSubview:ivFailedIcon];
}

- (void)updateLoadingMaskSize:(CGSize)size{
    self.loadingMask.frame = CGRectMake(0, 0, size.width, size.height);
    
    if(self.appliesMediaViewMaskAsOutgoing){
        self.activityIndicatorLoading.frame = CGRectMake((size.width - 2 * WidthTail - SizeActivityIndicator) / 2, 34, SizeActivityIndicator, SizeActivityIndicator);
        self.layerProgress.frame = CGRectMake(0, 60, size.width - 2 * WidthTail, HeightProgress);
    }else{
        self.activityIndicatorLoading.frame = CGRectMake((size.width - 2 * WidthTail - SizeActivityIndicator) / 2, 34, SizeActivityIndicator, SizeActivityIndicator);
        self.layerProgress.frame = CGRectMake(0, 60, size.width - 2 * WidthTail, HeightProgress);
    }

   
}



@end

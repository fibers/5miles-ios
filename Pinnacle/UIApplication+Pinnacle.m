//
//  UIApplication+Pinnacle.m
//  Pinnacle
//
//  Created by zhenyonghou on 15/6/30.
//  Copyright © 2015年 The Third Rock Ltd. All rights reserved.
//

#import "UIApplication+Pinnacle.h"

@implementation UIApplication(Pinnacle)

+ (BOOL)isEnableRemoteNotification
{
    BOOL isEnable = YES;
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        UIUserNotificationSettings *settings = [UIApplication sharedApplication].currentUserNotificationSettings;
        if (settings.types == UIUserNotificationTypeNone) {
            isEnable = NO;
        }
    } else {
        if (UIRemoteNotificationTypeNone == [UIApplication sharedApplication].enabledRemoteNotificationTypes) {
            isEnable = NO;
        }
    }
    
    return isEnable;
}

+ (void)gotoSystemSetting {
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=NOTIFICATIONS_ID"]];       // 设置URL Types :prefs
    }
}


@end

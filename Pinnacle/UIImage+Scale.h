//
//  UIImage+Scale.h
//  Pinnacle
//
//  Created by Maya Game on 14-7-30.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Scale)

-(UIImage*)scaleToSize:(CGSize)size;
-(UIImage*)scropeImageFromSize:(CGSize)bigSize toSize:(CGSize)smallSize;



-(UIImage*)resizeImage:(UIImage*)originImage withTargetSize:(CGSize)targetSize;
@end

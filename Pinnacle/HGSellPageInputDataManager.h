//
//  HGSellPageInputDataManager.h
//  Pinnacle
//
//  Created by zhenyonghou on 15/7/3.
//  Copyright © 2015年 The Third Rock Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGSellPageInputData: NSObject <NSCoding>

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *listingPrice;

@property (nonatomic, copy) NSString *brand;

@property (nonatomic, copy) NSString *category;

@property (nonatomic, copy) NSString *delivery;

@property (nonatomic, copy) NSString *originalPrice;

@property (nonatomic, copy) NSString *itemDescription;

@property (nonatomic, assign) NSInteger categoryId;

@property (nonatomic, assign) NSInteger deliveryType;

@property (nonatomic, strong) NSArray *selectedAssetUrls;

@property (nonatomic, copy) NSString *mediaLink;

@property (nonatomic, assign) NSInteger rootCategoryId;

@property (nonatomic, copy) NSString *country;

@property (nonatomic, copy) NSString *city;

@property (nonatomic, copy) NSString *region;


@end


@interface HGSellPageInputDataManager : NSObject

+ (HGSellPageInputData *)loadFromLocal;

+ (void)saveToLocalWithInputData:(HGSellPageInputData *)inputData;

+ (void)removeData;



@end

//
//  HGItemDescView.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-1.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrikeThroughLabel.h"
@class HGShopItem;

@interface HGItemDescView : UIView

@property (nonatomic, strong) UILabel * lbTitle;
@property (nonatomic, strong) UILabel * lbPrice;
@property (nonatomic, strong) StrikeThroughLabel * originalPrice;
@property (nonatomic, strong) UILabel * lbDesc;
@property (nonatomic, strong) UIButton * btnLocation;
@property (nonatomic, strong) UIButton * btnPlaySound;

- (id)initWithFrame:(CGRect)frame item:(HGShopItem *)item;

@end

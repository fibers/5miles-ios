//
//  HGMessageCenterController.h
//  Pinnacle
//
//  Created by Maya Game on 14-8-25.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGSysMessageCell.h"
#import "MessageCenterHelper.h"

@interface HGMessageCenterController : UIViewController<UITableViewDataSource,UITableViewDelegate, UINavigationControllerDelegate, HGSysMessageDrawingViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, assign) NSInteger currentType;
@property (nonatomic, strong) NSTimer* messageTimer;
@property (nonatomic, strong) UIImageView* dotImage1_buy;
@property (nonatomic, strong) UIImageView* dotImage2_sell;
@property (nonatomic, strong) UIImageView* dotImage3_notification;

@property (nonatomic, assign) BOOL bNeedSegmentAutoChange;;
@property (nonatomic, assign) BOOL bShowNotificationSegment;

@property (nonatomic, strong) NSString* lastestMD5_buy;
@property (nonatomic, strong) NSString* lastestMD5_sell;
@property (nonatomic, strong) NSString* lastestMD5_notification;


@property (nonatomic, strong) NSNumber* count_buying;
@property (nonatomic, strong) NSNumber* count_selling;
@property (nonatomic, strong) NSNumber* count_system;


@property (nonatomic, strong) UIView* segmentParentView;
@property (nonatomic, strong) UIView* emptyMessageHintView;
@property (nonatomic, strong) UIImageView* emptyBuyIcon;
@property (nonatomic, strong) UIImageView* emptySellIcon;
@property (nonatomic, strong) UIImageView* emptyNotificationIcon;

@property (nonatomic, strong) UILabel* emptyBuyLabel;
@property (nonatomic, strong) UILabel* emptySellLabel;
@property (nonatomic, strong) UILabel* emptyNotificationLabel;



//////////////////////////////////////////////////




-(void)CheckShowNotificationSegment;

-(void)ResetContentOffset;


@end

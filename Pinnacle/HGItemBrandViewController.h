//
//  HGItemBrandViewController.h
//  Pinnacle
//
//  Created by Alex on 14-9-30.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGItemBrandViewController : UITableViewController<UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UISearchBar *mySearchBar;
@property (strong, nonatomic)UISearchDisplayController* mySearchDisplayController;

@property (nonatomic, strong) NSMutableArray *allItems;
@property (nonatomic, strong) NSMutableArray *searchResults;

@end

//
//  HGPurchasesController.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-26.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGPurchasesController.h"
#import "HGAppData.h"
#import "HGShopItem.h"
#import "HGItemDetailController.h"
#import "HGPurchaseItemCell.h"
#import "HGUtils.h"
#import <JDStatusBarNotification.h>
#import "SVPullToRefresh.h"
#import <SVProgressHUD.h>
#import "UIStoryboard+Pinnacle.h"


@interface HGPurchasesController ()

@property (nonatomic, strong) NSMutableArray * items;
@property (nonatomic, strong) NSString * nextLink;
@property (nonatomic, strong) UIView* noMoreHintView;
@property (nonatomic, assign) BOOL bHasUserClickActionBeforeReturn;
@end

@implementation HGPurchasesController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)customizeBackButton
{
    
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.bHasUserClickActionBeforeReturn = NO;
    [self customizeBackButton];
    self.items = [@[] mutableCopy];
    [self addNoMoreHintView];
    
    
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 20)];
    footView.autoresizesSubviews = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.tableFooterView = footView;
    
    __weak HGPurchasesController * weakSelf = self;
    [SVProgressHUD show];
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"my_purchases/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
        NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
        
        [self.items removeAllObjects];
        for (id dictObj in results) {
            NSError * error = nil;
            HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
            if (!error) {
                [self.items addObject:item];
            } else {
                NSLog(@"shop item creation error: %@", error);
            }
        }
        
        if (self.items.count == 0) {
            [self addDefaultEmptyView];
        }
        else{
            [self.tableView reloadData];
        }
        
        if (![self.nextLink isEqual:[NSNull null]]) {
            [self.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf _loadNextPage];
            }];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"fetch my purchases failed: %@", error.userInfo);
    }];
}

-(void)addDefaultEmptyView
{
    int iconYoffset = 144;
    int lineOffset = 10;
    self.view.backgroundColor = [UIColor colorWithRed:0xf0/255.0 green:0xf0/255.0 blue:0xf0/255.0 alpha:1.0];

    CGRect rx = [ UIScreen mainScreen ].bounds;
    int iphone4_Y_offset_config_Value = 0;
    if (rx.size.height <= 480) {
        iphone4_Y_offset_config_Value = 44;
    }
    if(SCREEN_HEIGHT == 667)
    {
        iconYoffset = 144+ (667-568)/2;
    }
    if (SCREEN_HEIGHT==736) {
        iconYoffset = 144 + (736-568)/2;
    }
    
    CGPoint iconCenter  = CGPointMake(rx.size.width/2, iconYoffset - iphone4_Y_offset_config_Value);
    UIImageView* icon =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 434/2, 72/2)];
    icon.image = [UIImage imageNamed:@"defaultEmpty-icon"];
    icon.center = iconCenter;
    [self.view addSubview:icon];
    
    // +10 for empty description which occupy two lines.
    CGPoint centerPoint = CGPointMake(rx.size.width/2, CGRectGetMaxY(icon.frame)+ lineOffset + 40/2);
    
    
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 40)];
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.numberOfLines = 0;
    textLabel.center = centerPoint;
    textLabel.text = NSLocalizedString(@"Time to make your first purchase! See what listings are cool and grab a bargain. ", nil);
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [self.view addSubview:textLabel];
    
    CGPoint buttonCenter = CGPointMake(rx.size.width/2, CGRectGetMaxY(textLabel.frame)+lineOffset+ 36/2);
    
    UIButton* button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 200, 36)];
    button.layer.cornerRadius = 4.0;
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = DEFAULT_EMPTY_BOARDER_COLOR;
    UILabel* buttonTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    buttonTitle.textAlignment = NSTextAlignmentCenter;
    buttonTitle.text = NSLocalizedString(@"Go shopping!", nil);
    buttonTitle.textColor = DEFAULT_EMPTY_FONT_COLOR;
    [button addSubview:buttonTitle];
    button.center = buttonCenter;
    [self.view addSubview:button];
    [button addTarget:self action:@selector(defaultEmptyAction) forControlEvents:UIControlEventTouchUpInside];
}
-(void)defaultEmptyAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"mybuying_view" action:@"buying_empty" label:nil value:nil];
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    {
        [self.navigationController popViewControllerAnimated:YES];
        [UIView animateWithDuration:1.0 animations:^(){
            self.view.alpha = 0;
        } completion:^(BOOL bResult){
            [appDelegate.tabController button1pressed];
        }];
    }

}



-(void)addNoMoreHintView
{
    self.noMoreHintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 200, 30)];
    hintLabel.text = NSLocalizedString(@"No more", nil);
    hintLabel.textColor = FANCY_COLOR(@"cccccc");
    hintLabel.textAlignment = NSTextAlignmentCenter;
    hintLabel.center = self.noMoreHintView.center;
    [self.noMoreHintView addSubview:hintLabel];
    [self.tableView addSubview:self.noMoreHintView];
    [self.noMoreHintView setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[HGUtils sharedInstance] gaTrackViewName:@"mybuying_view"];
    HGAppDelegate* appDelegate =     (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.tabController.tabBar.hidden =YES;
     self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [SVProgressHUD dismiss];
    [super viewWillDisappear:animated];
    if (!self.bHasUserClickActionBeforeReturn) {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"mybuying_view" action:@"mybuying_back" label:nil value:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGPurchaseItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PurchaseItemCell" forIndexPath:indexPath];
    
    // Configure the cell...
    HGShopItem * item = [self.items objectAtIndex:indexPath.row];
    //this is selling/purchase cell type;
    //cell.CellType = 1;
    [cell configWithEntity:item];
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [[HGUtils sharedInstance] gaSendEventWithCategory:@"mybuying_view" action:@"mybuying_product" label:nil value:nil];
    self.bHasUserClickActionBeforeReturn = YES;
    HGShopItem * item = [self.items objectAtIndex:indexPath.row];
    
    HGItemDetailController *vcItemDetail = (HGItemDetailController *)[[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:@"VCItemDetail"];
    vcItemDetail.item = item;
    
    [self.navigationController pushViewController:vcItemDetail animated:YES];
}


#pragma mark - Helpers

- (void)_loadNextPage
{
    if ([self.nextLink isEqual:[NSNull null]] || self.nextLink == nil) {
        [self.tableView.infiniteScrollingView stopAnimating];
        self.tableView.showsInfiniteScrolling = NO;
        [self.noMoreHintView setFrame:CGRectMake(0, self.tableView.contentSize.height, self.tableView.frame.size.width, 40)];
        
        if(self.items.count != 0)
        {
            [self.noMoreHintView setHidden:NO];
        }
        
    } else {
        [SVProgressHUD show];
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"mybuying_view" action:@"mybuying_loadmore" label:nil value:nil];
        [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:[NSString stringWithFormat:@"my_purchases/%@", self.nextLink] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            self.nextLink = [[responseObject objectForKey:@"meta"] objectForKey:@"next"];
            
            NSArray * results = (NSArray *)[responseObject objectForKey:@"objects"];
            for (id dictObj in results) {
                NSError * error = nil;
                HGShopItem * item = [[HGShopItem alloc] initWithDictionary:dictObj error:&error];
                if (!error) {
                    [self.items addObject:item];
                } else {
                    NSLog(@"shop item creation error: %@", error);
                }
            }
            
            [self.tableView reloadData];
            [self.tableView.infiniteScrollingView stopAnimating];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self.tableView.infiniteScrollingView stopAnimating];
            [SVProgressHUD dismiss];
            NSLog(@"fetch my purchases failed: %@", error.userInfo);
        }];
        
    }
}

@end

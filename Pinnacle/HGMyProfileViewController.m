//
//  HGMyProfileViewController.m
//  Pinnacle
//
//  Created by Maya Game on 14-7-27.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGMyProfileViewController.h"
#import "HGMyProfile.h"
#import "HGAppData.h"
#import "HGAppDelegate.h"
#import "HGFollowersViewController.h"
#import "HGFollowingViewController.h"
#import "HGMyItemsController.h"
#import "HGMyLikesController.h"
#import "HGRatingsController.h"
#import "HGUtils.h"
#import "HGProfilePhoneViewController.h"
#import <PXAlertView+Customization.h>
#import "UIColor+FlatColors.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SVProgressHUD.h>
#import <Branch.h>
#import "HGConstant.h"
#import "HGUserVerifyHintView.h"
#import "HGWebContentController.h"
#import "UIStoryboard+Pinnacle.h"
#import "HGFBUtils.h"
#import "FBlikeReminderViewController.h"

@interface HGMyProfileViewController ()<FBLikerReminderCloseDelegate>

@property (nonatomic, strong) NSNumber * numItems;
@property (nonatomic, strong) NSNumber * numFollowers;
@property (nonatomic, strong) NSNumber * numFollowing;

@property (nonatomic, strong) UIImageView* avatarCover;
@property (nonatomic, strong) NSString* myShopLink;
@property (nonatomic, strong) NSString* shopImageUrl;
@property (nonatomic, strong) NSString* shopNickName;

@property (nonatomic, strong) FBlikeReminderViewController *fblikerview;

//////////////////user verify UI /////////////
@property (weak, nonatomic) IBOutlet UIView *userGenerateInfoView;
@property (nonatomic, strong) UIImageView* userAvatar_verifyMark;

@property (nonatomic, strong) UIButton* userEmail_verifyButton;
@property (nonatomic, strong) UIButton* userPhone_verifyButton;
@property (nonatomic, strong) UIButton* userFB_verifyButton;

@property (nonatomic, strong) UIButton* user_verifyInfoButton;

@property (nonatomic, strong) HGUserVerifyHintView* userVerifyInfoView;
@property (nonatomic, strong) UIButton *btnVerifyInfoMask;


@property (nonatomic, strong) NSTimer* emailActionTimerKeeper;

@property (weak, nonatomic) IBOutlet UILabel *lbLikesDetail;
@property (weak, nonatomic) IBOutlet UILabel *lbPurchasesDetail;
@property (weak, nonatomic) IBOutlet UILabel *lbFeedbackDetail;

@property (weak, nonatomic) IBOutlet UILabel *lbLikesTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbPurchasesTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbFeedbackTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbShareShopTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbInviteFriendsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbHelpCenterTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbSettingsTitle;


@property (nonatomic, strong) UIButton* rightButton;
@property (nonatomic, assign) float extraXoffset_Iphone6;
@end

@implementation HGMyProfileViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)addAvatarCover
{
    
    
    self.avatarCover = [[UIImageView alloc] initWithFrame:self.avatarView.frame];
    self.avatarCover.image = [UIImage imageNamed:@"avatar-cover"];
    self.avatarCover.center = self.avatarView.center;
    [self.view addSubview:self.avatarCover];
    
    
    self.userAvatar_verifyMark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    CGPoint centerPoint = self.avatarView.center;
    centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -10;
    centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -10;
    self.userAvatar_verifyMark.center = centerPoint;
    [self.view addSubview:self.userAvatar_verifyMark];

}
-(void)avartarConfig
{
    self.avatarView.frame = CGRectMake(self.avatarView.frame.origin.x, self.avatarView.frame.origin.y, 58, 58);
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.cornerRadius = CGRectGetHeight(self.avatarView.bounds) * 0.5;
    self.avatarView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushMyProfile)];
    [self.avatarView addGestureRecognizer:singleTap1];
    
}
-(void)setCellAccessIcon:(UITableViewCell*)cell
{
    UIImageView*accView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 17/2, 30/2)];
    accView.image = [UIImage imageNamed:@"uitableview_cell_access"];
    cell.accessoryView = accView;

}
-(void)setAllCell
{
    [self setCellAccessIcon:self.likesCell];
    [self setCellAccessIcon:self.purchase_bury_cell];
    [self setCellAccessIcon:self.feedback_cell];
    [self setCellAccessIcon:self.inviteFriend_cell];
    [self setCellAccessIcon:self.setting_cell];
    [self setCellAccessIcon:self.shareMyShop_cell];
    [self setCellAccessIcon:self.helpCenter_cell];
}



-(void)setTextStyle
{
    self.lbNumItems.textColor  = self.lbNumFollowers.textColor = self.lbNumFollowing.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.lbNumItems.font = self.lbNumFollowers.font = self.lbNumFollowing.font = [UIFont fontWithName:@"Helvetica" size:17];
    
    self.itemUnit.textColor = self.followerUnit.textColor = self.followingUnit.textColor=FANCY_COLOR(@"818181");
    self.itemUnit.font = self.followerUnit.font = self.followingUnit.font = [UIFont fontWithName:@"Helvetica" size:13];
    
    self.lbHelpCenterTitle.textColor = self.lbLikesTitle.textColor = self.lbPurchasesTitle.textColor = self.lbFeedbackTitle.textColor = self.lbShareShopTitle.textColor = self.lbInviteFriendsTitle.textColor = self.lbSettingsTitle.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    self.lbHelpCenterTitle.font =  self.lbLikesTitle.font = self.lbPurchasesTitle.font = self.lbFeedbackTitle.font = self.lbShareShopTitle.font  =self.lbInviteFriendsTitle.font = self.lbSettingsTitle.font = [UIFont fontWithName:@"Helvetica" size:15];
    
    self.lbLikesDetail.textColor = self.lbPurchasesDetail.textColor = self.lbFeedbackDetail.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    [self.lbFeedbackTitle sizeToFit];
}

- (void)setText{
    self.lbNumItems.text = @"0";
    self.lbNumFollowers.text =@"0";
    self.lbNumFollowing.text = @"0";
    self.itemUnit.text = NSLocalizedString(@"Listings", nil);
    self.followerUnit.text = NSLocalizedString(@"Followers", nil);
    self.followingUnit.text = NSLocalizedString(@"Following", nil);

    self.lbLikesTitle.text = NSLocalizedString(@"Likes", nil);
    self.lbPurchasesTitle.text = NSLocalizedString(@"Buying/Purchased", nil);
    self.lbFeedbackTitle.text = NSLocalizedString(@"Reviews", nil);
    self.lbShareShopTitle.text = NSLocalizedString(@"Share my shop to Facebook", nil);
    self.lbInviteFriendsTitle.text = NSLocalizedString(@"Invite my friends", nil);
    self.lbHelpCenterTitle.text = NSLocalizedString(@"Help Center", nil);
    self.lbSettingsTitle.text = NSLocalizedString(@"Settings", nil);
}

-(void)customizeBackButton
{
    
    //remove the title text;
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] init];
    backbutton.title = @"";
    
    self.navigationItem.backBarButtonItem = backbutton;
    
}
-(void)addRightButton
{
    UIView* rightBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 30)];
    
    //rightBackView.backgroundColor = [UIColor redColor];
    
    self.rightButton = [[UIButton alloc] initWithFrame:CGRectMake(14, 0, 84, 30)];
    self.rightButton.clipsToBounds = YES;
    self.rightButton.layer.cornerRadius = 3;
    self.rightButton.layer.borderWidth = 1;
    self.rightButton.layer.borderColor = FANCY_COLOR(@"ff8830").CGColor;
    self.rightButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.rightButton setTitle:NSLocalizedString(@"Edit profile",nil) forState:UIControlStateNormal];
    [self.rightButton setTitleColor:FANCY_COLOR(@"ff8830") forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(onTouchEditProfileButton) forControlEvents:UIControlEventTouchUpInside];
   
    [rightBackView addSubview:self.rightButton];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBackView ];
    
    
    
    
    
    
}
- (void)onTouchEditProfileButton {
    //[self pushMyProfile];
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"editprofile" label:nil value:nil];
    
    [self performSegueWithIdentifier:@"PushMyProfile" sender:self];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.extraXoffset_Iphone6 = 0;
    if (SCREEN_WIDTH==320) {
        //iphone4. iphone5
        self.extraXoffset_Iphone6 = 0;
    }
    else if(SCREEN_WIDTH == 375){
        //iphone6; UI improve
        self.extraXoffset_Iphone6 = 30 ;
    }
    else
    {
        self.extraXoffset_Iphone6 = 50;
    }

   
    self.lastAvatarUrlString = @"";
 
    [self customizeBackButton];
    [self avartarConfig];
    [self addAvatarCover];
    
    self.navigationController.delegate = self;
    self.view.backgroundColor = SYSTEM_DEFAULT_TABLE_BACKGROUND_COLOR;
    [self setTextStyle];
    [self setText];
    [self setAllCell];
    
    [self configUI:[HGUserData sharedInstance].userLocalProfile];
    //[self addNeedUpdate_mark];
    [self customizeBackButton];
    [self addVerifyUIButton];
    [self addRightButton];
    
    
    
    self.feedback_score = [[UIImageView alloc] initWithFrame:CGRectMake(178, 17, 71, 10)];
    [self.feedback_cell addSubview:self.feedback_score];
    
    
      

}
static int verifyUI_y_offset = 60;
-(void)addVerifyUIButton
{
    
    
    UILabel* verificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(100+self.extraXoffset_Iphone6, verifyUI_y_offset -5, 80, 30)];
    verificationLabel.text = NSLocalizedString(@"Verification:",nil);
    verificationLabel.font = [UIFont systemFontOfSize:15];
    verificationLabel.textColor = SYSTEM_DEFAULT_FONT_COLOR_1;
    [self.userGenerateInfoView addSubview:verificationLabel];
    
    self.userEmail_verifyButton = [[UIButton alloc] initWithFrame:CGRectMake(185+self.extraXoffset_Iphone6, verifyUI_y_offset , 21, 21)];
    [self.userEmail_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_email_not_verify"] forState:UIControlStateNormal];
    [self.userGenerateInfoView addSubview:self.userEmail_verifyButton];
    
    self.userFB_verifyButton = [[UIButton alloc] initWithFrame:CGRectMake(185 +21 +10+self.extraXoffset_Iphone6, verifyUI_y_offset , 21, 21)];
    [self.userFB_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_fb_not_verify"] forState:UIControlStateNormal];
    [self.userGenerateInfoView addSubview:self.userFB_verifyButton];
   
    self.userPhone_verifyButton = [[UIButton alloc] initWithFrame:CGRectMake(185+ 21*2+10*2 +self.extraXoffset_Iphone6, verifyUI_y_offset  , 21, 21)];
    
    [self.userPhone_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_phone_not_verify"] forState:UIControlStateNormal];
    [self.userGenerateInfoView addSubview:self.userPhone_verifyButton];

    self.user_verifyInfoButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userPhone_verifyButton.frame)+15, verifyUI_y_offset - 5, 32, 32)];
    [self.user_verifyInfoButton setImage:[UIImage imageNamed:@"verify_info_icon"] forState:UIControlStateNormal];
    [self.user_verifyInfoButton addTarget:self action:@selector(showUserVerifyInfoView) forControlEvents:UIControlEventTouchUpInside];
    [self.userGenerateInfoView addSubview:self.user_verifyInfoButton];
    
    self.userVerifyInfoView = [[HGUserVerifyHintView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.user_verifyInfoButton.frame)-201, 86, (452+20)/2, (130+74*2+186)/2)];
    self.userVerifyInfoView.isCurrentUser = 1;
    [self.userVerifyInfoView configContentText];
    [self.userVerifyInfoView configGetVerifiedButton];
    self.userVerifyInfoView.delegate = self;
    
    self.btnVerifyInfoMask = [[UIButton alloc] initWithFrame:self.view.frame];
    self.btnVerifyInfoMask.layer.opaque = YES;
    self.btnVerifyInfoMask.hidden = YES;
    [self.btnVerifyInfoMask addTarget:self action:@selector(hideUserVerifyInfoView) forControlEvents:UIControlEventTouchUpInside];
    [self.btnVerifyInfoMask addSubview:self.userVerifyInfoView];

    [self.view addSubview:self.btnVerifyInfoMask];
}

  

-(void)configVerifyInfo:(NSDictionary*)responseObject
{
    /////avatar/////////////

    if ([HGUserData sharedInstance].bUserAvatarVerified){
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_verify"];
    }
    else{
        self.userAvatar_verifyMark.image = [UIImage imageNamed:@"verify_user_not_verify"];
    }
    
    NSInteger verifiedCount = 0;
    /////email////////////////
    if ([HGUserData sharedInstance].bUserEmailVerified){
        verifiedCount++;
        self.userEmail_verifyButton.userInteractionEnabled = NO;
        [self.userEmail_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_email_verify"] forState:UIControlStateNormal];
    }
    else{
        self.userEmail_verifyButton.userInteractionEnabled = YES;
        [self.userEmail_verifyButton addTarget:self action:@selector(verifyEmailAction) forControlEvents:UIControlEventTouchUpInside];
        [self.userEmail_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_email_not_verify"] forState:UIControlStateNormal];
    }
    
    ////facebook/////////////////////
    if ([HGUserData sharedInstance].bUserFBVerified) {
        verifiedCount++;
        self.userFB_verifyButton.userInteractionEnabled = NO;
        [self.userFB_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_fb_verify"] forState:UIControlStateNormal];
    }
    else{
        self.userFB_verifyButton.userInteractionEnabled = YES;
        [self.userFB_verifyButton addTarget:self action:@selector(verifyFBverifyAction) forControlEvents:UIControlEventTouchUpInside];
        [self.userFB_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_fb_not_verify"] forState:UIControlStateNormal];
    }
    
    
    /////////phone///////////////////
    if([HGUserData sharedInstance].bUserPhoneVerified) {
        verifiedCount++;
        self.userPhone_verifyButton.userInteractionEnabled = NO;
        [self.userPhone_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_phone_verify"] forState:UIControlStateNormal];
    }else{
        self.userPhone_verifyButton.userInteractionEnabled = YES;
        [self.userPhone_verifyButton addTarget:self action:@selector(startPhoneVerifyAction) forControlEvents:UIControlEventTouchUpInside];
        [self.userPhone_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_phone_not_verify"] forState:UIControlStateNormal];
    }
    //////phone number string/////
    NSString* phoneNumber = [responseObject objectForKey:@"mobile_phone"];
    if (phoneNumber != nil && phoneNumber.length > 0) {
        [HGAppData sharedInstance].userVerifiedPhonenumberString = phoneNumber;
    }
    
    [self.userVerifyInfoView removeVerfiedButton:(verifiedCount == 3)];
    
}

-(void)verifyEmailAction
{
    if (durationCount > 0) {
        [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Please wait 60s to resend.",nil)];
        return;
    }
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"verifyemailicon" label:nil value:nil];

    [[HGAppData sharedInstance].userApiClient FIVEMILES_POST:@"send_verify_email/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
         [[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"If you still can't see the email, check the SPAM folder and add 5miles to your white list. ",nil) withTitle:NSLocalizedString(@"We have sent you an email verification",nil)];
        
        self.emailActionTimerKeeper = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownDuration) userInfo:nil repeats:YES];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
         /*[[HGUtils sharedInstance] showSimpleAlert:NSLocalizedString(@"Sorry, please try again later.",nil)];
          */
    }];
    
}
-(void)startPhoneVerifyAction
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"verifyphoneicon" label:nil value:nil];

    HGProfilePhoneViewController *viewcontroller = [[HGProfilePhoneViewController alloc] initWithNibName:@"HGProfilePhoneViewController" bundle:nil];
    
    [self.navigationController pushViewController:viewcontroller animated:YES];
}
-(void)verifyFBverifyAction
{
    if( ![HGUserData sharedInstance].bUserFBVerified){
        
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"verifyfacebookicon" label:nil value:nil];
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
        [[HGFBUtils sharedInstance] verifyAccountWithSuccess:^(NSDictionary *reponse) {
            [SVProgressHUD dismiss];
            self.userFB_verifyButton.userInteractionEnabled = NO;
            [self.userFB_verifyButton setBackgroundImage:[UIImage imageNamed:@"verify_fb_verify"] forState:UIControlStateNormal];
            [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Link facebook account successfully.",nil) onView:self.view];
        } withFailure:^(NSError *error) {
            [SVProgressHUD dismiss];
            [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"There was an error linking your Facebook account. Please try again later.",nil) onView:self.view];
        }];
    }

}

static int durationCount = 0;
-(void)countDownDuration
{
    durationCount ++;
    if (durationCount < 60) {
       
    }
    else{
        [self.emailActionTimerKeeper invalidate];
        self.emailActionTimerKeeper = nil;
        durationCount = 0;
    }
}
//static int TouchVerifyInfoCounter =0;
-(void)showUserVerifyInfoView
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"myverifyinfoicon" label:nil value:nil];
    self.btnVerifyInfoMask.hidden = NO;
}
-(void)hideUserVerifyInfoView
{
    self.btnVerifyInfoMask.hidden = YES;
}
-(void)clickGetVerifyButtion
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"mygetverified" label:nil value:nil];
    self.btnVerifyInfoMask.hidden = YES;
    [self pushMyProfile];
}


-(void)pushMyProfile
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"headphoto" label:nil value:nil];
    [self performSegueWithIdentifier:@"PushMyProfile" sender:self];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
       
    self.tabBarController.tabBar.hidden = YES;
    
    self.btnVerifyInfoMask.hidden = YES;
    
    [[HGUtils sharedInstance] gaTrackViewName:@"myProfile_view"];
    [self updateUserProfile];
    if(![HGAppData sharedInstance].bOpenReviewFunctions)
    {
        self.feedback_cell.hidden = YES;
    }
    else
    {
        self.feedback_cell.hidden = NO;
    }
    
}
-(void)updateUserProfile
{
    ///networking status check.
    if (![[HGAppData sharedInstance].userApiClient checkNetworkStatus]) {
        return;
    }
    
    [[HGAppData sharedInstance].userApiClient FIVEMILES_GET:@"me/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [HGUserData sharedInstance].userLocalProfile = responseObject;
        [self configUI:[HGUserData sharedInstance].userLocalProfile];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* statusCodeString = [error.userInfo valueForKey:@"statusCode"];
        //401 for user token out of time, log out.
        //400 not exit user
        if(statusCodeString.integerValue == 400)
        {
            [(HGAppDelegate *)[[UIApplication sharedApplication] delegate] logUserOut];
        }
        NSLog(@"get me detail failed: %@", error.userInfo);
    }];

}
- (void)viewDidLayoutSubviews{
    // Adjust the element in profile summary area.
    [super viewDidLayoutSubviews];
    
   
    
    [self userSummaryViewInit:self.extraXoffset_Iphone6];
}
-(void)setDivider:(float)extraXoffset withExtraSpace:(float)extraSpace{
    
    
    self.leftDivider = [[UIImageView alloc] initWithFrame: CGRectMake(156+extraXoffset + extraSpace/2, 27 -8, 1, 30)];
    self.leftDivider.backgroundColor = PROFILE_DIVIDER_COLOR;
    [self.view addSubview:self.leftDivider];
    
    self.rightDivider = [[UIImageView alloc] initWithFrame: CGRectMake(233+extraXoffset + (extraSpace/2)*3, 27 -8, 1, 30)];
    self.rightDivider.backgroundColor = PROFILE_DIVIDER_COLOR;
    [self.view addSubview:self.rightDivider];
}
-(void)userSummaryViewInit:(float)extraXoffset
{
    
    
    int extraSpaceBetweenEach = 0;
    if (extraXoffset>0) {
        extraSpaceBetweenEach = 10;
    }
    [self setDivider:extraXoffset withExtraSpace: extraSpaceBetweenEach];
    
    self.lbNumItems.center = CGPointMake(120+extraXoffset, 34 - 8);
    self.itemUnit.center = CGPointMake(120+extraXoffset, 52 - 8);
    
    self.lbNumFollowers.center = CGPointMake(195+extraXoffset + extraSpaceBetweenEach,34 - 8);
    self.followerUnit.center = CGPointMake(195+extraXoffset + extraSpaceBetweenEach, 52 - 8);
    
    self.lbNumFollowing.center = CGPointMake(271+extraXoffset + extraSpaceBetweenEach*2,34 - 8);
    self.followingUnit.center = CGPointMake(271+extraXoffset+extraSpaceBetweenEach*2, 52 - 8);
    
    if(SCREEN_WIDTH > 320)
    {
        self.avatarView.frame = CGRectMake(22 +10, 11, 58, 58);
        self.avatarCover.center = self.avatarView.center;
        CGPoint centerPoint = self.avatarView.center;
        centerPoint.x = centerPoint.x + self.avatarView.frame.size.width/2 -10;
        centerPoint.y = centerPoint.y + self.avatarView.frame.size.width/2 -10;
        self.userAvatar_verifyMark.center = centerPoint;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
    if ([HGUserData sharedInstance].bNeedProfileIntro) {
        [HGUserData sharedInstance].bNeedProfileIntro = NO;
//        [self startProfileIntroduce];
        self.btnVerifyInfoMask.hidden = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

-(void)configUI:(NSDictionary*)responseObject
{
    
    [self configVerifyInfo:responseObject];
    
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    [attributes setObject:[UIFont boldSystemFontOfSize:17] forKey:NSFontAttributeName];
    [self.navigationController.navigationBar setTitleTextAttributes:attributes];
    self.navigationItem.title = [HGUserData sharedInstance].userDisplayName;
    
    
    
    if ([[HGUserData sharedInstance].userAvatarLink length] > 0) {
        
        int sugggestWidth = [[HGUtils sharedInstance] getImageSuggestEdgeLength:self.avatarView.frame.size.width*2];
         NSString* image_url = [[HGUtils sharedInstance] cloudinaryLink:[HGUserData sharedInstance].userAvatarLink  width:sugggestWidth height:sugggestWidth];
        
        
        if ([self.lastAvatarUrlString isEqualToString:image_url] && self.lastAvatarUrlString.length > 0) {
            //lastAvatar is already the same with current, not need to update.
        }
        else{
            self.lastAvatarUrlString = image_url;
            [self.avatarView sd_setImageWithURL:[NSURL URLWithString:image_url]placeholderImage:[UIImage imageNamed:@"default-avatar"]];
        }
        
    }
    self.numItems = [responseObject objectForKey:@"items_count"];
    self.numFollowers = [responseObject objectForKey:@"followers_count"];
    self.numFollowing = [responseObject objectForKey:@"following_count"];
    
    self.lbNumItems.text = [NSString stringWithFormat:@"%@", [[HGUtils sharedInstance] formatInteger:self.numItems.integerValue]];
    self.lbNumFollowers.text = [NSString stringWithFormat:@"%@", [[HGUtils sharedInstance] formatInteger:self.numFollowers.integerValue]];
    self.lbNumFollowing.text = [NSString stringWithFormat:@"%@", [[HGUtils sharedInstance] formatInteger:self.numFollowing.integerValue]];
    
    self.itemUnit.text = NSLocalizedString(@"Listings", nil);
    self.followerUnit.text = NSLocalizedString(@"Followers", nil);
    self.followingUnit.text = NSLocalizedString(@"Following", nil);
    
    
    
    
    self.lbLikesDetail.text = [NSString stringWithFormat:@"%d", [[responseObject objectForKey:@"likes_count"] intValue]];
    self.lbPurchasesDetail.text = [NSString stringWithFormat:@"%d", [[responseObject objectForKey:@"purchases_count"] intValue]];
    self.lbFeedbackDetail.text = [NSString stringWithFormat:@"%d", [[responseObject objectForKey:@"review_num"] intValue]];
    
    
    
    self.feedback_score.frame = CGRectMake(self.view.frame.size.width-80-self.feedback_score.frame.size.width, self.feedback_score.frame.origin.y, self.feedback_score.frame.size.width, self.feedback_score.frame.size.height);
    
    
    CGFloat seller_score = [[responseObject objectForKey:@"review_score"] floatValue];
    NSMutableAttributedString * label = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Reviews", nil) attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0]}];
    [label appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
    self.lbFeedbackTitle.attributedText = label;
    NSString* image_index = [[HGUtils sharedInstance] getReviewImageNameByScore:seller_score];
    
    self.feedback_score.image = [UIImage imageNamed:image_index];
    
    
    if(![HGAppData sharedInstance].bOpenReviewFunctions)
    {
        self.feedback_score.hidden = self.lbFeedbackTitle.hidden = YES;
    }
    else{
        [UIView animateWithDuration:1 animations:^{
            self.feedback_score.hidden = NO;
            self.lbFeedbackTitle.hidden = NO;
        } completion:^(BOOL finished) {
            
        }];
    }

    
    
    
    
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UI Control Actions

- (void)LogoutCurrentUser {
    [(HGAppDelegate *)[[UIApplication sharedApplication] delegate] logUserOut];
}

- (IBAction)onTouchItemsNum:(id)sender {
   [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"myitem" label:nil value:nil];
    [self performSegueWithIdentifier:@"ShowMyItems" sender:self];
}

- (IBAction)onTouchFollowersNum:(id)sender {
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"myfollowers" label:nil value:nil];
    [self performSegueWithIdentifier:@"MyProfileToFollowers" sender:self];
}

- (IBAction)onTouchFollowingNum:(id)sender {
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"myfollowing" label:nil value:nil];
    [self performSegueWithIdentifier:@"MyProfileToFollowing" sender:self];
}


- (void)onTouchInviteFriend {
    if ([HGUserData sharedInstance].fmUserID == nil) {
        return;
    }
    
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"branchview" action:@"branchinvite" label:nil value:nil];
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:@"https://5milesapp.com/" forKey:@"$desktop_url"];
    NSString* featureString = [@"" stringByAppendingFormat:@"%@invite", [HGUserData sharedInstance].fmUserID];
    NSString* md5string = [[HGUtils sharedInstance] md5HexDigest:featureString];
    md5string = [md5string substringToIndex:6];
    [params setObject:md5string forKey:@"md5string"];
    [params setObject:@"invite" forKey:@"share_type"];
    [params setObject:[HGUserData sharedInstance].fmUserID forKey:@"event_id"];
    [params setObject:[HGUserData sharedInstance].fmUserID forKey:@"user_id"];
    
    NSString* branchUrl = [[HGUtils sharedInstance] getBranchUrl:params];
    
    
    NSString * template = NSLocalizedString(@"Join me and create your own mobile marketplace on the #5milesapp! Download it here.", nil);
    
    
    NSURL *urlToShare = [NSURL URLWithString:branchUrl];
    
    
    NSString* shareString = [@"" stringByAppendingString:template];
    
    UIActivityViewController * vcActivity = [[UIActivityViewController alloc] initWithActivityItems:@[shareString, urlToShare] applicationActivities:nil];
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        vcActivity.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
            if (completed && !activityError) {
                if ([HGUserData sharedInstance].bHomeShowFBLike)
                {
                    [self addFBlikerViewWithTitle:NSLocalizedString(@"Share listing successfully", nil)];
                }
            }
        };
    } else {
        vcActivity.completionHandler = ^(NSString *activityType, BOOL completed){
            if (completed) {
                if ([HGUserData sharedInstance].bHomeShowFBLike)
                {
                    [self addFBlikerViewWithTitle:NSLocalizedString(@"Thank you for inviting friends", nil)];
                }
            }
        };
    }
    
    [self presentViewController:vcActivity animated:YES completion:nil];
}


#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![HGAppData sharedInstance].bOpenReviewFunctions && (indexPath.section == 0 && indexPath.row == 2))
    {
        return 0;
    }
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0) {
        //start share my shop to Facebook
        [self shareMyShop];
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        //link with facebook account
        //
    }
    if (indexPath.section == 2 && indexPath.row == 0) {
        //invite friends
        [self onTouchInviteFriend];
    }
    if (indexPath.section == 2 && indexPath.row == 1) {
        //[self performSegueWithIdentifier:@"showHelpCenter" sender:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //error defense.
    if ([HGUserData sharedInstance].fmUserID == nil) {
        return;
    }
    if ([segue.identifier isEqualToString:@"MyProfileToFollowers"]) {
        [[HGUtils sharedInstance] gaTrackViewName:@"myfollowers_view"];
        
        HGFollowersViewController * vcDest = segue.destinationViewController;
        vcDest.userID = [HGUserData sharedInstance].fmUserID;
    } else if ([segue.identifier isEqualToString:@"MyProfileToFollowing"]) {
        
        [[HGUtils sharedInstance] gaTrackViewName:@"myfollowing_view"];
        HGFollowingViewController * vcDest = segue.destinationViewController;
        vcDest.userID = [HGUserData sharedInstance].fmUserID;
    } else if ([segue.identifier isEqualToString:@"ShowSellerRatings"]) {
        HGRatingsController * vcDest = segue.destinationViewController;
         [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"myreview" label:nil value:nil];
        vcDest.uid = [HGUserData sharedInstance].fmUserID;
        
    }else if ([segue.identifier isEqualToString:@"ShowMyLikes"])
    {
         [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"mylikes" label:nil value:nil];
    }
    else if ([segue.identifier isEqualToString:@"ShowMyPurchases"])
    {
         [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"mybuying" label:nil value:nil];
    }
    else if ([segue.identifier isEqualToString:@"ShowSettings"])
    {
         [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"setting" label:nil value:nil];
    }
    else if([segue.identifier isEqualToString:@"showHelpCenter"])
    {
        [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"helpcenter" label:nil value:nil];

        
       
        NSString* preferredLang = [[HGUtils sharedInstance] getPreferLanguage];
        HGWebContentController* vcDest = segue.destinationViewController;
        vcDest.contentLink = [@"https://5milesapp.com/mobile/helpCenter" stringByAppendingFormat:@"/?email=%@&name=%@&loc=%@&uid=%@", [HGUserData sharedInstance].userEmail, [HGUserData sharedInstance].userDisplayName, preferredLang,[HGUserData sharedInstance].fmUserID ];
    }
}




-(void)shareMyShop
{
    
    if ([HGUserData sharedInstance].fmUserID == nil) {
        [self LogoutCurrentUser];
        return;
    }
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"sharemyshop" label:nil value:nil];
    
    
    if (self.numItems.integerValue == 0) {
        ///alert to list more items
        PXAlertView* view = [PXAlertView showAlertWithTitle:nil message:NSLocalizedString(@"Publish a listing first before you share.", nil) cancelTitle:NSLocalizedString(@"Cancel", nil)  otherTitle:NSLocalizedString(@"OK", nil) completion:^(BOOL cancelled, NSInteger buttonIndex) {
            
            if (!cancelled) {
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"shareshoplistyes" label:nil value:nil];

                HGAppDelegate* appDelegate = (HGAppDelegate *)[[UIApplication sharedApplication] delegate];
                UINavigationController *navUpload = (UINavigationController *)[[UIStoryboard uploadStoryboard] instantiateViewControllerWithIdentifier:@"NavUpload"];
                [appDelegate.tabController presentViewController:navUpload animated:YES completion:nil];
            }
            else{
                [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"shareshoplistno" label:nil value:nil];
            }
            
        }];
        [view useDefaultIOS7Style];
        
    }else{
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        
        [[HGFBUtils sharedInstance] shareShopWithSuccess:^(NSDictionary *reponse) {
            [SVProgressHUD dismiss];

            if ([HGUserData sharedInstance].bHomeShowFBLike)
            {
                [self addFBlikerViewWithTitle:NSLocalizedString(@"Share my shop to Facebook successfully", nil)];
            } else {
                [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Share my shop to Facebook successfully.",nil) onView: self.view];
            }
            
        } withFailure:^(NSError *error) {
            [SVProgressHUD dismiss];
            [[HGUtils sharedInstance] showSimpleNotify:NSLocalizedString(@"Failed to share your shop. Please try again later.", nil) onView: self.view];
            NSLog(@"Error: %@", error);
        }];
    }
}

-(void)addFBlikerViewWithTitle:(NSString *)title
{
    [[HGUtils sharedInstance] gaSendEventWithCategory:@"myProfile_view" action:@"likefacebookpage_open" label:nil value:nil];
    self.fblikerview = [[FBlikeReminderViewController alloc] initWithNibName:@"FBlikeReminderViewController" bundle:nil];
    self.fblikerview.alertTitle = title;
    self.fblikerview.delegate = self;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        //ios 8
        self.fblikerview.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    else{
        //
        HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
        
        application.tabController .modalPresentationStyle = UIModalPresentationCurrentContext;
        self.modalPresentationStyle = UIModalPresentationFormSheet;
        
    }
    
    HGAppDelegate *application = (HGAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [application.tabController presentViewController:self.fblikerview animated:NO completion:nil];
}

- (void)touchedCloseButtonWithFblikeReminder:(FBlikeReminderViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchedLikeButtonWithFblikeReminder:(FBlikeReminderViewController *)viewController
{
    [HGUserData sharedInstance].bHomeShowFBLike = NO;
}

- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    NSLog(@"navigation will show view in profile");
    HGAppDelegate* myappdelegate = (HGAppDelegate*)[UIApplication sharedApplication].delegate;
    UIViewController* homeview =  [navController.viewControllers objectAtIndex:0];
    
    if (viewController == homeview) {
        myappdelegate.tabController.fmTabbarView.hidden = NO;
    }
    else{
        myappdelegate.tabController.fmTabbarView.hidden = YES;
    }
}
//
//#pragma mark- HGProfileIntroViewControllerDelegate
//
//- (void)profileIntroViewWillDismiss
//{
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        self.btnVerifyInfoMask.hidden = NO;
//    });
//}

@end

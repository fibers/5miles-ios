//
//  HGCategoryCell.m
//  Pinnacle
//
//  Created by Maya Game on 14-8-24.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import "HGCategoryCell.h"
#import "HGConstant.h"
#import "HGUtils.h"
#import "HGCategory.h"
#import "HGAppData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+pureColorImage.h"
@interface HGCategoryCell()

@property (nonatomic, strong) UIImageView* accView;


@end

@implementation HGCategoryCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commonSetup];
    
    }
    return self;
}

-(id)init
{
    self = [super init];
    if (self) {
        [self commonSetup];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        [self commonSetup];
    }
    return self;
}
-(void)commonSetup
{
   
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    self.seperatorView = [[UIView alloc] initWithFrame:CGRectMake(60, self.frame.size.height-fSeperatorHeight, self.frame.size.width, fSeperatorHeight)];
    self.seperatorView.backgroundColor =FANCY_COLOR(@"cccccc");
    [self addSubview:self.seperatorView];
    
    
    self.lbTitle.font = [UIFont systemFontOfSize:15];
    self.lbTitle.textColor = FANCY_COLOR(@"242424");
}

-(void)configCategoryCell:(HGCategory*)category
{
    
    self.lbTitle.text = category.title;
    self.lbTitle.textAlignment = NSTextAlignmentLeft;
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    self.seperatorView.frame = CGRectMake(50, self.contentView.frame.size.height-fSeperatorHeight, SCREEN_WIDTH-11, fSeperatorHeight);
    
    
   
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
    float fSeperatorHeight = [[HGUtils sharedInstance] getSuggestSeperatorLineHeight];
    self.seperatorView.frame = CGRectMake(60, self.contentView.frame.size.height-fSeperatorHeight, SCREEN_WIDTH-11, fSeperatorHeight);
}



@end

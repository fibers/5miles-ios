//
//  HGChatMapPOICell.h
//  Pinnacle
//
//  Created by Alex on 15/5/13.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface HGChatMapPOICell : UITableViewCell

@property (nonatomic, strong) UILabel* POIName;
@property (nonatomic, strong) UILabel* distanceLabel;
@property (nonatomic, strong) UILabel* POIaddress;
@property (nonatomic, strong) UIImageView* selectedIcon;


@property (nonatomic, strong) UIView* seperatorLine;

-(void)configCell:(MKMapItem*)item bShowSelectedIcon:(BOOL)selected;
@end

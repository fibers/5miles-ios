//
//  HGChatMapPOITopCell.m
//  Pinnacle
//
//  Created by Alex on 15/5/14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGChatMapPOITopCell.h"
#import "HGConstant.h"

@implementation HGChatMapPOITopCell



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonSetup];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [self commonSetup];
}



-(void)commonSetup
{
    self.titleText = [[UILabel alloc] initWithFrame:CGRectMake(8, 5, 310, 42)];
    [self addSubview: self.titleText];
    self.titleText.font = [UIFont systemFontOfSize:13];
    self.titleText.textColor = FANCY_COLOR(@"818181");
    self.titleText.numberOfLines = 2;
}


-(void)configCell:(NSString*)titleString
{
    self.titleText.text = titleString;
    
    
    //self.
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

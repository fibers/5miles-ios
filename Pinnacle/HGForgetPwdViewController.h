//
//  HGForgetPwdViewController.h
//  Pinnacle
//
//  Created by Alex on 14-9-30.
//  Copyright (c) 2014年 The Third Rock Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HGForgetPwdViewController : UIViewController

@property(nonatomic, strong) NSString* suggestUserEmail;
@end

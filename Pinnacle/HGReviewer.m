//
//  HGReviewer.m
//  Pinnacle
//
//  Created by Alex on 15-4-14.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "HGReviewer.h"

@implementation HGReviewer
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"uid"}];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    
    
    NSArray * names = @[@"uid", @"is_robot",@"location",@"nickname",@"place",@"portrait",@"verified"];
    if ([names indexOfObject:propertyName] != NSNotFound) {
        return YES;
    }
    
    return NO;
}

@end
